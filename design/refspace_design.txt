Obsidian reference space design:

Author: Joseph Kinzel

- High level overview of goals and functionality:
  + The reference space is essentially a traversalable, permissioned namespace for a variety of abstract objects and attributes within the operating system.
  + Multiple classes of objects:
    - Containers that simply hold and organize other objects
    - Channels that abstract and facilitate communication between executable components (drivers, userspace software, etc.)
    - Hardware devices
    - Block devices, partitions and filesystems
    - Links of any of the above objects
    - Attributes and resources provided by any of the above objects
  + Multiple, heirarchical, separators types indicating relationships between objects
    - '\' general container or parent object relationship
    - '.' Object attributes
    - '>' Object resources
    - ':' Object/Resource divisions and partitions
  + Topological hardware or parent-child relationships should form primary objects
  + Links can be used for groupings and classifications
    - e.g: A graphic card's frame buffer will have its primary object at \sys\pci_root0\bridge1\gfx0>mem1 but also \sys\gfx\fb0
  + High degree of permission granularity that extends down to attributes
    - e.g: \sys\gfx\fb0.mem_size could be user readable but \sys\gfx\fb0.phys_addr would not
  + Class and path specific schema will be defined in separate specifications
  + Shorthand prefixes for common paths:
    - Unix style notation for a primary filesystem with backslashes as separators
    - Label based block device shorthand with $
      + e.g: $OBS_LIVE$/drivers/usb/ehci/ehci.so

- Potential/in flux features
  + Virtual reference space containers and objects provided by the kernel and applications
   - Sould the kernel be able to convey per-app /proc style information?
   - Should applications be able to publish IPC channels?
   - How would such systems interact with a POSIX model?
   - Is a per process, read only, system information and call table more efficient/performant?


Milestones/Feature timeline:
 1: Kernel side basic refspace object classes:
    - Containers
    - Channels
    - Hardware devices
 2: Kernel side refspace search feature based on '\' separator
 3: Userspace side refspace search and connection API
    - Refspace traversal
    - Refspace enumeration
    - Refspace object querying
    - Refspace object binding (e.g mapping memory from a framebuffer)
    - Cleanup on (in)voluntary process termination
 4: Object attributes and resources
 5: Additional separator support
    - General multipass and subpath parsing support

 6: Links
 7: Shorthand support
 8: Filesystem support

Expected generational release schedule

Inital: Milestones 1 + 2
 - Kernel and driver APIs require alteration
 - Driver database requires alteration
Initial userspace: Milestones 3 + 4
 - Contingent on inital native library and system calls
 - Drivers will require updates to support new functionality
 - Additional class specifications defining schema and naming conventions for attributes and resources
Full traversal: Milestones 5 + 6
 - Primarily parsing changes to APIs both in kernel and userspace
 - Kernel side functionality for links
Integrated FS: Milestones 7 + 8
 - Start with existing INITRD filesystem
 - Heuristic for naming based on label, topology and initialization order
