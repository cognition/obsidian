/* USB Hub descriptor parsing and retrival functions
 *
 * Author: Joseph Kinzel */

#include <driverapi.h>
#include "uhub_struct.h"
#include "uhub_desc.h"
#include "uhub_ops.h"
#include "uhub_log.h"

/* Descriptor type values */
#define USB_HUB_DESC_TYPE          0x29U
#define USB_ESS_HUB_DESC_TYPE      0x2AU

/* Hub characteristics fields extraction macros */
#define UHUB_CHARS_GET_PORT_PWR(x)       ((x) & 0x03U)
#define UHUB_CHARS_GET_COMPDEV(x)        (((x)>>2) & 0x01U)
#define UHUB_CHARS_GET_OC_PORT(x)        (((x)>>3) & 0x03U)
#define UHUB_CHARS_GET_TT(x)             (((x)>>5) & 0x03U)
#define UHUB_CHARS_GET_PORT_IND(x)       (((x)>>7) & 0x01U)

/* Power switching values */
#define UHUB_POWER_GANGED                0
#define UHUB_POWER_INDIVIDUAL            1
#define UHUB_POWER_NONE(x)               ((x) & 0x2U)

/* Over current protection values */
#define UHUB_OC_GLOBAL                   0
#define UHUB_OC_INDIVIDUAL               1
#define UHUB_OC_NONE(x)                  ((x) & 0x2U)

#define SSP_DC_GET_AC(x)             ((x) & 0x1FU)
#define SSP_DC_GET_IDC(x)            (((x) >> 5) & 0xFU)

#define SSP_DC_BM_GET_ID(x)          ((x) & 0xFU)
#define SSP_DC_BM_GET_LSE(x)         (((x) >> 4) & 0x3U)
#define SSP_DC_BM_GET_PROTO(x)       (((x) >> 14) & 0x3U)
#define SSP_DC_BM_GET_MANTISSA(x)    ((x) >> 16)


/*! Extracts shared fields from the hub descriptor
 *
 *  \param hc Hub context instance
 *  \param desc Hub descriptor
 */
void usb_hub_extract_common_desc_fields(usb_hub_context *hc, volatile usbHubDescriptor *desc)
{
    uint8_t hub_power, hub_oc;
    uint32_t remove_mask_size, power_on_time;
    hc->port_count = desc->bNumPorts;

    uhub_log(UHUB_LOG_INFO, "Hub at @ %X possess %u ports.\n", hc->full_addr, desc->bNumPorts);

    hub_power = UHUB_CHARS_GET_PORT_PWR(desc->wHubCharacteristics);
    hub_oc = UHUB_CHARS_GET_OC_PORT(desc->wHubCharacteristics);
    power_on_time = desc->bPwerOn2PwerGood;
    power_on_time *= 2;

    switch(hub_power)
    {
        case UHUB_POWER_GANGED:
            break;
        case UHUB_POWER_INDIVIDUAL:
            hc->flags |= UHUB_FLAGS_INV_PP;
            break;
        default:
            break;
    }

    switch(hub_oc)
    {
        case UHUB_OC_GLOBAL:
            hc->flags |= UHUB_FLAGS_OC_PROT;
            break;
        case UHUB_OC_INDIVIDUAL:
            hc->flags |= UHUB_FLAGS_PORT_OC;
            break;
        default:
            break;
    }

    if( UHUB_CHARS_GET_COMPDEV(desc->wHubCharacteristics) )
        hc->flags |= UHUB_FLAGS_COMPOUND;

    if( UHUB_CHARS_GET_PORT_IND(desc->wHubCharacteristics) )
        hc->flags |= UHUB_FLAGS_PORT_IND;

    /* Calculate the size of the removable/port status maps */
    if(hc->flags & UHUB_FLAGS_SUPER_SPEED)
    {
            remove_mask_size = 2;
    }
    else
    {
        remove_mask_size = (hc->port_count + 1) + 0x07U;
        remove_mask_size /= 8;
    }

    hc->removable_mask = (uint8_t *)calloc(remove_mask_size, 1);
    hc->status_msg_size = remove_mask_size;
    hc->power_on_time = power_on_time;
}

driverOperationResult uhub_parse_dc_ssp(usb_hub_context *hc, uint8_t *desc_bytes, const int bytes_rem)
{
    usbBOSSuperspeedPlusDesc *ssp_desc;
    laneSublinkSpeed *speeds;
    int attribute_count;

    ssp_desc = (usbBOSSuperspeedPlusDesc *)desc_bytes;

    if(bytes_rem < sizeof(usbBOSSuperspeedPlusDesc) )
        return driverOpBadState;

    attribute_count = SSP_DC_GET_AC(ssp_desc->bmAttributes);

    if(bytes_rem < (sizeof(usbBOSSuperspeedPlusDesc) + attribute_count*4) )
        return driverOpBadState;

    speeds = (laneSublinkSpeed *)calloc(sizeof(laneSublinkSpeed), attribute_count );
    hc->ssp_speed_count = attribute_count;
    hc->ssp_speeds = speeds;

    for(int speed_attr_index = 0; speed_attr_index < attribute_count; speed_attr_index++)
    {
        laneSublinkSpeed *entry;
        int lse;
        uint32_t bitmap_value;

        bitmap_value = ssp_desc->bmSublinkSpeedAttr[speed_attr_index];
        entry = &speeds[speed_attr_index];

        lse = SSP_DC_BM_GET_LSE(bitmap_value);

        entry->id = SSP_DC_BM_GET_ID(bitmap_value);
        entry->protocol = SSP_DC_BM_GET_PROTO(bitmap_value);
        entry->mantissa = SSP_DC_BM_GET_MANTISSA(bitmap_value);
        entry->lse_value = 1;

        while(lse)
        {
            entry->lse_value *= 1000;
            lse--;
        }
    }

    return driverOpSuccess;
}

/*! Parses the binary object store
 *
 *  \param hc Hub context pointer
 *  \param full_desc Pointer to the Binary Object Store descriptor
 *
 */
driverOperationResult uhub_parse_bos(usb_hub_context *hc, usbBOSDesc *full_desc)
{
    driverOperationResult status;
    uint16_t rem_bytes;
    int offset = 0;

    rem_bytes = full_desc->wTotalLength - sizeof(usbBOSDesc);
    status = driverOpBadState;


    for(int cap_index = 0; cap_index < full_desc->bNumDeviceCaps; cap_index++)
    {
        int entry_length = full_desc->entries[offset + USB_DESC_FIELD_LENGTH_OFF];

        if( entry_length < sizeof(usbDevCapDesc) )
            goto bos_parse_end;

        if(full_desc->entries[offset + USB_DESC_FIELD_TYPE_OFF] != usbDescDeviceCapability)
            goto bos_parse_end;

        switch(full_desc->entries[offset+USB_DESC_DEV_CAP_TYPE])
        {
            case devQualSuperspeedPlus:
                status = uhub_parse_dc_ssp(hc, &full_desc->entries[offset], rem_bytes);
                break;
        }

        offset += entry_length;
        rem_bytes -= entry_length;
    }

    if(!rem_bytes)
        status = driverOpSuccess;

bos_parse_end:
    free(full_desc);
    return status;
}

/*! Attempts to retrieve and then parse the BOS descriptor
 *
 *  \param hc Hub context structure
 *  \param wait_obj Signal object for the transaction
 *  \return driverOpBusError if the BOS descriptor request fails or the result of the BOS descriptor parsing
 *  \sa uhub_parse_bos
 */
driverOperationResult uhub_get_bos_desc(usb_hub_context *hc, sync_object *wait_obj)
{
    busMsgResponse status;
    usb_device_get_descriptor desc_req;
    uint32_t response_size;
    usbBOSDesc *init_desc, *full_desc;

    response_size = sizeof(void *);

    desc_req.desc_index = 0;
    desc_req.desc_type = usbDescBOS;
    desc_req.requestor = hc->bus_spec;
    desc_req.request_type = USB_REQ_TYPE_DEVICE;
    desc_req.expected_size = 5;
    desc_req.sync = wait_obj;

    status = busMsg(&init_desc, &response_size, BUS_ID_USB, hc->full_addr, &desc_req, sizeof(usb_device_get_descriptor), usbCoordGetDescriptor, hc->dh);
    if(status != busMsgAcknowledged)
    {
        return driverOpBusError;
    }

    desc_req.expected_size = init_desc->wTotalLength;
    free(init_desc);

    status = busMsg(&full_desc, &response_size, BUS_ID_USB, hc->full_addr, &desc_req, sizeof(usb_device_get_descriptor), usbCoordGetDescriptor, hc->dh);
    if(status != busMsgAcknowledged)
    {
        return driverOpBusError;
    }

    sync_object_wait(wait_obj, 5000);

    if( !sync_object_get_status(wait_obj) )
        return driverOpBusError;


    return uhub_parse_bos(hc, full_desc);
}

/*! Retrieves the hub descriptor and parses it
 *
 *  \param hc Hub context instance
 *  \param sync Signal/Wait synchronization object for the function to utilize
 *  \return driverOpSuccess if successful or an error indicating the nature of failure
 */
driverOperationResult usb_hub_process_descriptor(usb_hub_context *hc, sync_object *sync)
{
    busMsgResponse status;
    volatile usbHubDescriptor *hub_desc;
    usb_device_get_descriptor desc_request;
    uint32_t res_size;

    res_size = sizeof(volatile usbHubDescriptor *);
    desc_request.requestor = hc->bus_spec;
    desc_request.desc_type = USB_HUB_DESC_TYPE;
    desc_request.desc_index = 0;
    desc_request.request_type = USB_BMRT_CLASS;
    desc_request.expected_size = sizeof(usbHubDescriptor);
    desc_request.sync = sync;

    status = busMsg(&hub_desc, &res_size, BUS_ID_USB, hc->full_addr, &desc_request, sizeof(usb_device_get_descriptor), usbCoordGetDescriptor, hc->dh);
    if(status != busMsgAffirmative)
    {
        uhub_log(UHUB_LOG_ERRORS, "Error - Failed to retrieve hub descriptor for hub @ %X Status: %u\n", hc->full_addr, status);
        return driverOpBusError;
    }

    /* Extract think time values */
    if(hc->flags & UHUB_FLAGS_HIGH_SPEED)
    {
        hc->tt_time = 8*(UHUB_CHARS_GET_TT(hub_desc->wHubCharacteristics)+1);
    }
    else
        hc->tt_time = 0;

    usb_hub_extract_common_desc_fields(hc, hub_desc);

    if( sizeof(usbHubDescriptor) > hub_desc->bDescLength)
    {
        uhub_log(UHUB_LOG_ERRORS, "Error - Hub descriptor for device @ %X size mismatch - Only %u bytes\n", hc->full_addr, hub_desc->bDescLength);
        return driverOpBadState;
    }

    memcpy(hc->removable_mask, (void *)hub_desc->data, hc->status_msg_size);

    free((void *)hub_desc);
    return driverOpSuccess;
}
