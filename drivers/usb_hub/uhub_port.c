/* USB Hub driver port information processing and management header
 *
 *  Author: Joseph Kinzel */

#include "uhub_port.h"
#include "uhub_ops.h"
#include "uhub_log.h"
#include <driverapi.h>

#define UHUB_EXT_STATUS_RX_ID(x)               ((x) & 0x0FU)
#define UHUB_EXT_STATUS_TX_ID(x)               (((x)>>4) & 0x0FU)
#define UHUB_EXT_STATUS_RX_LANE_COUNT(x)       (((x)>>8) & 0x0FU)
#define UHUB_EXT_STATUS_TX_LANE_COUNT(x)       (((x)>>12) & 0x0FU)

#define USB_DC_SSP_PROTO_SUPER_SPEED           0
#define USB_DC_SSP_PROTO_SUPER_SPEED_PLUS      1

#define USB_ESS_5G                             5000000000UL
#define USB_ESS_10G                            10000000000UL


/*! Decodes an extended port status value into a USB device speed
 *
 *  \param hc Hub context pointer
 *  \param ext_status Extended port status value
 *
 *  \return A USB device speed, or zero if the device speed could not be determined
 */
static int uhub_decode_ext_status_speed(usb_hub_context *hc, const uint32_t ext_status)
{
    uint64_t lanes;
    uint32_t speed_id;
    laneSublinkSpeed *sublink_info;

    speed_id = UHUB_EXTS_GET_RX_ID(ext_status);
    lanes = UHUB_EXTS_GET_RX_LC(ext_status);

    sublink_info = hc->ssp_speeds;

    /* Attempt to find a matching speed id value in the cached speeds from the SSP capability */
    for(int speed_index = 0; speed_index < hc->ssp_speed_count; speed_index++, sublink_info++)
    {
        if(sublink_info->id == speed_id)
        {
            uint32_t mbps;
            uhub_log(UHUB_LOG_INFO, "Found matching speed id of %u with LSE: %u and mantissa: %u\n", speed_id, sublink_info->lse_value, sublink_info->mantissa );
            if(sublink_info->lse_value < 1000000)
                return 0;
            mbps = sublink_info->lse_value;
            mbps /= 1000000;

            mbps *= sublink_info->mantissa;

            switch(mbps)
            {
                case 5000:
                    /* Protocol value of zero indicates a USB3.0 SuperSpeed device */
                    if( sublink_info->protocol == 0)
                        return USB_SPEED_SUPER;
                    else if(lanes == 1)
                        return USB_SPEED_ESUPER_1X1;
                    else if(lanes == 2)
                        return USB_SPEED_ESUPER_1X2;
                    break;
                case 10000:
                    if(lanes == 1)
                        return USB_SPEED_ESUPER_2X1;
                    else if(lanes == 2)
                        return USB_SPEED_ESUPER_2X2;
                    break;
                default:
                    break;
            }
        }
    }


    uhub_log(UHUB_LOG_ERRORS, "Unable to match speed id %u\n", speed_id);
    return 0;
}


/*! Messages the USB bus coordinator to inform it of a newly connected device
 *
 *  \param hc Hub context pointer
 *  \param port Port number
 *  \param port_status Status value for the port
 *  \param wait_obj Synchronization object to trigger
 *
 *  \return busMsgAcknowledged if successful, result from the coordinator or busMsgNoAck
 */
busMsgResponse uhub_register_port_connection(usb_hub_context *hc, const uint8_t port, const uint16_t port_status, sync_object *wait_obj)
{
    usb_port_connect_msg *pc_msg;
    busMsgResponse result;
    uint32_t res_size;


    uhub_log(UHUB_LOG_INTERNAL, "Attempting to initialize device @ %X:%u\n", hc->full_addr, port);

    pc_msg = (usb_port_connect_msg *)calloc(sizeof(usb_port_connect_msg), 1);
    pc_msg->controller_id = hc->full_addr/USB_MAX_ADDRESS;
    pc_msg->parent_address = USB_GET_LOCAL_ADDRESS(hc->full_addr);
    pc_msg->port_num = port;
    pc_msg->parent_handle = hc->bus_spec;
    pc_msg->connect_wait = wait_obj;

    /* Attempt to determine the speed of the device */
    if( (hc->flags & UHUB_FLAGS_SUPER_SPEED) )
    {
        int speed;
        if( hc->flags & UHUB_FLAGS_SUPER_SPEED_PLUS)
        {
            speed = uhub_decode_ext_status_speed(hc, hc->ext_port_status_cache[port-1]);
            if(!speed)
            {
                free(pc_msg);
                return busMsgNoAck;
            }
        }
        else
            speed = USB_SPEED_SUPER;

        pc_msg->dev_speed = speed;
    }
    else
    {
        if(port_status & UHUB_PMASK_LOW_SPEED)
            pc_msg->dev_speed = USB_SPEED_LOW;
        else if(port_status & UHUB_PMASK_HIGH_SPEED)
            pc_msg->dev_speed = USB_SPEED_HIGH;
        else
            pc_msg->dev_speed= USB_SPEED_FULL;

    }

    sync_object_reset(wait_obj);

    uhub_log(UHUB_LOG_INTERNAL, "Dispatching coordinator connection request for device @ %X:%u\n", hc->full_addr, port);
    result = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, pc_msg, sizeof(usb_port_connect_msg), usbCoordPortConnect, hc->dh);
    if( result != busMsgAcknowledged)
    {
        uhub_log(UHUB_LOG_ERRORS, "Failed to complete device connection on hub @ %X\n", hc->full_addr);
        free(pc_msg);
        return result;
    }

    return busMsgAcknowledged;
}

/*! Task function for new port connections
 *
 *  \param context uhub_port_connect_context structure describing the new connection
 */
static void uhub_port_connected_task(void *context)
{
    uhub_root_complex *rc;
    usb_hub_context *hc;
    default_addr_queue_entry *q_entry;
    int q_empty, queue_count, status;
    uhub_port_connect_context *pcc;
    sync_object *wait_obj;

    pcc = (uhub_port_connect_context *)context;
    hc = pcc->hc;
    rc = hc->root_complex;
    queue_count = 0;

    /* Queue up a request for the default address pipe on the root complex */
    dapi_lock_acquire(rc->default_addr_lock);

    q_empty = dapi_queue_empty(rc->default_addr_queue);

    q_entry = dapi_queue_push(rc->default_addr_queue);

    sync_create_signal_obj(&wait_obj, 0, 0);

    q_entry->wait_obj = wait_obj;
    q_entry->hc = pcc->hc;
    q_entry->port_index = pcc->port_index;

    dapi_lock_release(rc->default_addr_lock);

    uhub_log(UHUB_LOG_INTERNAL, "Port @ %X:%u - Default address queue for RC @ %p empty: %u\n", hc->full_addr, pcc->port_index, rc, q_empty);

    /* Wait until this request has ownership over the default USB address */
    while(!q_empty)
    {
        sync_object_wait(wait_obj, 250);
        q_empty = sync_object_get_status(wait_obj);
    }

    uhub_log(UHUB_LOG_INTERNAL, "Port @ %X:%u has acquired default address lock!\n", hc->full_addr, pcc->port_index);

    sync_object_reset(wait_obj);

    if( !(hc->flags & UHUB_FLAGS_SUPER_SPEED) )
    {
        /* Pre-super speed ports need to be reset to actually enter the enabled state and convey their connection speed information.
           That's also the extent of what needs to be done here as the port change handler will finish the process once the port enters the enabled state. */
        uhub_reset_port(hc, pcc->port_index, &queue_count);
        uhub_control_run(hc, USB_TRANSACTION_IN);
    }
    else
    {
        /* Super Speed will enter the enabled state on their own but have a more specific link specific indicator */
        usbHubPortStatus *ps;
        busMsgResponse result;
        uint32_t link_state;
        ps = &hc->port_status_cache[pcc->port_index - 1];



        do
        {
            link_state = UHUB_GET_LINK_STATE(ps->port_status);
            /* TODO: Handle waiting for the connection to complete or transition failures */
        } while(link_state != UHUB_LINK_STATE_U0);


        result = uhub_register_port_connection(hc, pcc->port_index, ps->port_status, wait_obj);
        if(result != busMsgAcknowledged)
        {
            uhub_log(UHUB_LOG_ERRORS, "Unable to register port @ %X:%u\n", hc->full_addr, pcc->port_index);
        }

    }

    /* Wait for the USB coordinator to signal that the device is addressed */
    do
    {
        sync_object_wait(wait_obj, 250);
        status = sync_object_get_status(wait_obj);
    } while(!status);

    if(status == busOpSuccess)
    {
        uhub_log(UHUB_LOG_INTERNAL, "Device on hub @ %X Port: %u was successfully addressed\n", hc->full_addr, pcc->port_index);
    }
    else
    {
        uhub_log(UHUB_LOG_ERRORS, "Final status for device @ %X:%u was %u\n", hc->full_addr, pcc->port_index, status);
    }


    /* Release ownership of the default address and signal the next request's owner */
    dapi_lock_acquire(rc->default_addr_lock);

    dapi_queue_pop(rc->default_addr_queue, NULL);

    uhub_log(UHUB_LOG_INTERNAL, "Port @ %X:%u has released default address lock!\n", hc->full_addr, pcc->port_index);
    if(!dapi_queue_empty(rc->default_addr_queue))
    {
        default_addr_queue_entry *next_entry;

        next_entry = dapi_queue_peek(rc->default_addr_queue);
        dapi_lock_release(rc->default_addr_lock);
        sync_object_signal(next_entry->wait_obj, 1);

    }
    else
        dapi_lock_release(rc->default_addr_lock);




    sync_destroy_signal_obj(wait_obj);
    free(pcc);
}

/*! Wrapper for the dispatch of a new port connection request task
 *
 *  \param hc Hub context pointer
 *  \param port_index Port number where a device has been connected
 *  \param ps Port status values
 */
void uhub_process_port_connected(usb_hub_context *hc, const uint8_t port_index, usbHubPortStatus *ps)
{
    uhub_port_connect_context *pcc;

    pcc = (uhub_port_connect_context *)calloc(sizeof(uhub_port_connect_context), 1);

    pcc->hc = hc;
    pcc->port_index = port_index;
    pcc->status = ps;

    dapi_spawn_task( &uhub_port_connected_task, pcc);
}

/*! Determines what attributes of a port have changed
 *
 *  \param hc Hub context instance
 *  \param port Port to analyze
 */
int uhub_handle_port_status_change(usb_hub_context *hc, const uint8_t port, int *queue_count)
{
    uint16_t port_status, status_change;

    usbHubPortStatus *ps;

    ps = &hc->port_status_cache[port - 1];
    port_status = ps->port_status;
    status_change = ps->port_change;

    uhub_log(UHUB_LOG_INTERNAL, "Port status change handler invoked for port @ %X:%u Status: %X Change: %X\n", hc->full_addr, port, port_status, status_change);

    /* Check for an overcurrent condition immediately */
    if(hc->flags & UHUB_FLAGS_PORT_OC)
    {
        if(status_change & UHUB_PC_OVER_CURRENT)
        {
            /* Overcurrent condition, kill power to the port */
            return uhub_queue_port_depower(hc, port, queue_count);
        }
    }

    /* Check if the port has been disabled */
    if(status_change & UHUB_PC_ENABLE)
    {
        if(hc->hub_port_state[port-1] != uhub_port_disabling)
        {
            if( !(port_status & UHUB_PS_POWER) )
            {
                if(hc->hub_port_state[port-1] != uhub_port_depowered)
                {
                    /* Over current condition triggered on either this port or another in its gang */

                }
            }
            else
            {
                /* Hardware error on the bus forced the port into the disabled state, notify the bus controller */
                //! TODO: Message the bus coordinator to notify it of the error
            }

        }

        hc->hub_port_state[port-1] = uhub_port_disabled;
        return busOpSuccess;
    }

    /* Check for a change in connection status */
    if(status_change & UHUB_PC_CONNECTION)
    {
        if( (port_status & UHUB_PS_CONNECTION)  && (hc->hub_port_state[port-1] == uhub_port_disconnected) )
        {
            /* New device connected, queue up a request for the default address and then reset the port so speed determination can take place */
            hc->hub_port_state[port-1] = uhub_port_connected;
            uhub_process_port_connected(hc, port, ps);
        }
        else
        {
            /* Device disconnected */
            hc->hub_port_state[port-1] = uhub_port_disconnected;
        }
        uhub_queue_clear_port_feature(hc, port, UHUB_PF_C_CONNECTION, NULL, queue_count);
    }

    /* Port has been reset and entered the enabled state */
    if(status_change & UHUB_PC_RESET)
    {
        default_addr_queue_entry *qe;
        sync_object *wait_obj;
        uhub_root_complex *rc;

        uhub_log(UHUB_LOG_INTERNAL, "Device @ %X:%u has been successfully reset to the enabled state! Status: %X\n", hc->full_addr, port, port_status);
        rc = hc->root_complex;
        /* A reset request was completed and the port should be in the enabled state with the port speed bits set.
           This is the point from where speed determination and controller side configuration should proceed from */
        hc->hub_port_state[port-1] = uhub_port_active;
        uhub_queue_clear_port_feature(hc, port, UHUB_PF_C_RESET, NULL, queue_count);

        dapi_lock_acquire(rc->default_addr_lock);
        if( !dapi_queue_empty(rc->default_addr_queue) )
        {
            qe = dapi_queue_peek(rc->default_addr_queue);
            dapi_lock_release(rc->default_addr_lock);

            if((qe->hc == hc) && (qe->port_index == port))
            {
                wait_obj = qe->wait_obj;
                uhub_register_port_connection(hc, port, port_status, wait_obj);
            }

        }
        else
            dapi_lock_release(rc->default_addr_lock);

    }

    if(status_change & UHUB_PC_SUSPEND)
    {
        /* The port has woken up */
    }

    return busOpSuccess;
}

/*! Determines if a device on a specific hub port is a compound device
 *
 *  \param hc Hub context instance
 *  \param port The port number of the device
 *
 *  \return USB_INIT_FLAGS_COMPOUND if the device is part of the parent hub or zero if it is not
 */
static COMPILER_UNUSED uint32_t uhub_port_is_compound(usb_hub_context *hc, const uint8_t port)
{
    if(hc->flags & UHUB_FLAGS_COMPOUND)
    {
        uint8_t mask, mask_index;

        mask = 1;
        mask_index = port/8;
        mask <<= port%8;

        if(hc->removable_mask[mask_index] & mask)
            return USB_INIT_FLAGS_COMPOUND;
    }

    return 0;
}

/*! Issues a reset request to a specific downstream port on the hub
 *
 *  \param hc Hub context instance
 *  \param port Downstream port number
 *  \param queue_count Pointer to the command counter
 */
busMsgResponse uhub_reset_port(usb_hub_context *hc, const uint8_t port, int *queue_count)
{
    busMsgResponse status;
    uhub_log(UHUB_LOG_INTERNAL, "Resetting port %X:%u\n", hc->full_addr, port);
    status = uhub_queue_set_port_feature(hc, port, UHUB_PF_RESET, NULL, queue_count);

    (*queue_count)++;
    return status;
}

/*! Clears a feature/status field for the port
 *
 *  \param hc Hub context instance
 *  \param port Port number
 *  \param selector Feature field selector as defined by the USB specification
 */
busMsgResponse uhub_queue_clear_port_feature(usb_hub_context *hc, const uint8_t port, const uint8_t selector, sync_object *sync, int *queue_count)
{
    (*queue_count)++;
    return uhub_single_control_queue(hc, USB_REQ_TYPE_HOST_TO_DEV | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_OTHER, USB_CONTROL_REQ_CLEAR_FEATURE,
                                     port, selector, 0, NULL, sync);
}

/*! Sets a feature/status field for the port
 *
 *  \param hc Hub context pointer
 *  \param port Port number
 *  \param selector Port feature selector to set
 *  \param sync Synchronization object to trigger upon completion of the request
 *  \param queue_count Pointer to a transaction request counter
 *
 *  \return Result of the queue control transaction request to the bus coordinator
 */
busMsgResponse uhub_queue_set_port_feature(usb_hub_context *hc, const uint8_t port, const uint8_t selector, sync_object *sync, int *queue_count)
{
    (*queue_count)++;
    return uhub_single_control_queue(hc, USB_REQ_TYPE_HOST_TO_DEV | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_OTHER, USB_CONTROL_REQ_SET_FEATURE,
                                     port, selector, 0, NULL, sync);
}

/*! Attempts to retrieve an updated port status value
 *
 *  \param hc Hub context pointer
 *  \param port Port number
 *  \param port_status_out Target buffer for the port's status and change status values
 *  \param sync Synchronization object to trigger upon completion of the request
 *  \param queue_count Pointer to a transaction request counter
 *
 *  \return Result of the queue control transaction request to the bus coordinator
 */
busMsgResponse uhub_get_port_status(usb_hub_context *hc, const uint8_t port_index, usbHubPortStatus *port_status_out, sync_object *sync, int *queue_count)
{
    if(hc->flags & UHUB_FLAGS_SUPER_SPEED_PLUS)
    {
        /* Get the extend status as well if this is a SSP hub */
        busMsgResponse status;

        status = uhub_single_control_queue(hc, USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_OTHER, USB_CONTROL_REQ_GET_STATUS,
                                     port_index, UHUB_PORT_EXT_STATUS, 4, &hc->ext_port_status_cache[port_index - 1], NULL);
        if(status != busMsgAcknowledged)
            return status;
        *queue_count += 2;
    }
    else
        (*queue_count)++;


    return uhub_single_control_queue(hc, USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_OTHER, USB_CONTROL_REQ_GET_STATUS,
                                     port_index, UHUB_PORT_STATUS, 4, port_status_out, sync);

}


/*! Powers off a port
 *
 *  \param hc Hub context instance
 *  \param port Port number
 *
 *  \return busOpSuccess if successful
 */
busMsgResponse uhub_queue_port_depower(usb_hub_context *hc, uint8_t port, int *queue_count)
{
    hc->hub_port_state[port-1] = uhub_port_depowered;
    return uhub_queue_clear_port_feature(hc, port, UHUB_PF_POWER, NULL, queue_count);
}

/*! Decodes the speed ID value and lane count into a general USB speed value
 *
 *  \param hc Hub controller context instance
 *  \param speed_id Identifier for the speed value
 *  \param lane_count Number of lanes devoted to the device
 *
 *  \return A valid USB speed for the device or zero if the device speed could not be determined
 */
int uhub_decode_ess_speed_fields(usb_hub_context *hc, const int speed_id, const int lane_count )
{
    for(int speed_entry_index = 0; speed_entry_index < hc->ssp_speed_count; speed_entry_index++)
    {
        laneSublinkSpeed *entry;
        uint64_t full_lane_speed;

        entry = &hc->ssp_speeds[speed_entry_index];
        full_lane_speed = entry->lse_value;
        full_lane_speed *= (uint64_t)entry->mantissa;

        if(entry->id == speed_id)
        {
            switch(entry->protocol)
            {
                case USB_DC_SSP_PROTO_SUPER_SPEED:
                    if((full_lane_speed == USB_ESS_5G) && (lane_count == 1))
                        return USB_SPEED_ESUPER_1X1;
                    else
                        return 0;
                    break;
                case USB_DC_SSP_PROTO_SUPER_SPEED_PLUS:
                    if(full_lane_speed == USB_ESS_5G)
                    {
                        if(lane_count == 2)
                            return USB_SPEED_ESUPER_1X2;
                    }
                    else if(full_lane_speed == USB_ESS_10G)
                    {
                        if(lane_count == 1)
                            return USB_SPEED_ESUPER_2X1;
                        else if(lane_count == 2)
                            return USB_SPEED_ESUPER_2X2;
                    }
                    return 0;
                    break;
                default:
                    return 0;
                    break;
            }
        }
    }

    return 0;
}

/*! Determines the port speed for an enhanced super speed hub
 *
 *  \param hc Hub context instance
 *  \param port_status Port status field value
 *  \param ext_port_status Extended port status field value
 *
 *  \return An appropriate USB Driver API speed value or zero if the status field contained an invalid state
 */
int uhub_get_ps_ess(usb_hub_context *hc, uint16_t port_status, const uint16_t ext_port_status)
{
    if(hc->usb_ver_minor == 0)
    {
        if( UHUB_PS_ESS_GET_SPEED(port_status) == UHUB_ESS_PS_1X1  )
            return USB_SPEED_ESUPER_1X1;
        else
            return 0;
    }
    else
        return uhub_decode_ess_speed_fields(hc, UHUB_EXT_STATUS_RX_ID(ext_port_status), UHUB_EXT_STATUS_RX_LANE_COUNT(ext_port_status) );
}

/*! Determines the port speed for a high speed hub
 *
 *  \param hc Hub context instance
 *  \param port_status Port status field value
 *
 *  \return An appropriate USB API speed value or zero if the status field contained an invalid state
 */
int usb_hub_get_ps_hs(usb_hub_context *hc, uint16_t port_status)
{
    if(port_status & UHUB_PS_LOW_SPEED)
    {
        if(port_status & UHUB_PS_HIGH_SPEED)
            return 0;
        else
            return USB_SPEED_LOW;
    }
    else if(port_status & UHUB_PS_HIGH_SPEED)
        return USB_SPEED_HIGH;
    else
        return USB_SPEED_FULL;
}

/*! Determines the port speed of a full speed hub
 *
 *  \param hc Hub context instance
 *  \param port_status Port status field value
 *
 *  \return An appropriate USB API speed value
 */
int usb_hub_get_ps_fs(usb_hub_context *hc, uint16_t port_status)
{
    if(port_status & UHUB_PS_LOW_SPEED)
        return USB_SPEED_LOW;
    else
        return USB_SPEED_FULL;
}

