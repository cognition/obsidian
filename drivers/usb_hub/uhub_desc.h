/* USB Hub descriptor access and processing support functions
 *
 *
 * Author: Joseph Kinzel */

#ifndef UHUB_DESC_H_

#include "uhub_struct.h"
#include <usb/usb_desc.h>

void usb_hub_extract_common_desc_fields(usb_hub_context *hc, volatile usbHubDescriptor *desc);
driverOperationResult uhub_get_bos_desc(usb_hub_context *hc, sync_object *wait_obj);
driverOperationResult usb_hub_process_descriptor(usb_hub_context *hc, sync_object *sync);

#endif // UHUB_DESC_H_
