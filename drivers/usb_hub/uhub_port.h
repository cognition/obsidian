/* USB Hub port status and control header
 *
 * Author: Joseph Kinzel */

#ifndef UHUB_PORT_H_
#define UHUB_PORT_H_

#include <dapi_types.h>
#include "uhub_struct.h"

/* Port status bits */
#define UHUB_PS_CONNECTION         0x0001U
#define UHUB_PS_ENABLE             0x0002U
#define UHUB_PS_SUSPEND            0x0004U
#define UHUB_PS_OVER_CURRENT       0x0008U
#define UHUB_PS_RESET              0x0010U
#define UHUB_PS_POWER              0x0100U
#define UHUB_PS_LOW_SPEED          0x0200U
#define UHUB_PS_HIGH_SPEED         0x0400U
#define UHUB_PS_TEST               0x0800U
#define UHUB_PS_INDICATOR          0x1000U

#define UHUB_PS_ESS_GET_SPEED(x)   (((x) >> 10) & 0x7U)

/* USB 3.0 port status speed field values */
#define UHUB_ESS_PS_1X1            0
/* USB 3.1+ port status speed field values */
#define UHUB_ESS_PS_ESS            0

/* Port change bits */
#define UHUB_PC_CONNECTION         0x0001U

#define UHUB_PC_ENABLE             0x0002U
#define UHUB_PC_SUSPEND            0x0004U

#define UHUB_PC_OVER_CURRENT       0x0008U
#define UHUB_PC_RESET              0x0010U

#define UHUB_PC_BH_RESET           0x0020U
#define UHUB_PC_PLS                0x0040U
#define UHUB_PC_CONFIG_ERROR       0x0080U

#define UHUB_PC_ESS_VALID_BITS     (UHUB_PC_CONNECTION | UHUB_PC_OVER_CURRENT | UHUB_PC_RESET | UHUB_PC_BH_RESET | UHUB_PC_PLS | UHUB_PC_CONFIG_ERROR)
#define UHUB_PC_VALID_BITS         (UHUB_PC_CONNECTION | UHUB_PC_ENABLE | UHUB_PC_SUSPEND | UHUB_PC_OVER_CURRENT | UHUB_PC_RESET)


/* Port feature selectors */
#define UHUB_PF_CONNECTION         0
#define UHUB_PF_ENABLE             1
#define UHUB_PF_SUSPEND            2
#define UHUB_PF_OVER_CURRENT       3
#define UHUB_PF_RESET              4
#define UHUB_PF_POWER              8
#define UHUB_PF_LOW_SPEED          9

/* Port feature selector for change indicators */
#define UHUB_PF_C_CONNECTION       16
#define UHUB_PF_C_ENABLE           17
#define UHUB_PF_C_SUSPEND          18
#define UHUB_PF_C_OVER_CURRENT     19
#define UHUB_PF_C_RESET            20

#define UHUB_PF_TEST               21
#define UHUB_PF_INDICATOR          22

#define UHUB_PF_C_LINK_STATE       25
#define UHUB_PF_C_CONFIG_ERROR     26
#define UHUB_PF_C_BH_RESET         29

/* Extended port status field extraction macros */
#define UHUB_EXTS_GET_RX_ID(x)     ((x) & 0xF)
#define UHUB_EXTS_GET_TX_ID(x)     (((x)>>4) & 0xF)
#define UHUB_EXTS_GET_RX_LC(x)     (((x)>>8) & 0xF)
#define UHUB_EXTS_GET_TX_LC(x)     (((x)>>12) & 0xF)

typedef struct
{
    usb_hub_context *hc;
    uint8_t port_index;
    usbHubPortStatus *status;
} uhub_port_connect_context;

/* Port connection processing */
void uhub_process_port_connected(usb_hub_context *hc, const uint8_t port_index, usbHubPortStatus *ps);
busMsgResponse uhub_register_port_connection(usb_hub_context *hc, const uint8_t port, const uint16_t port_status, sync_object *wait_obj);

int usb_hub_get_port_speed(usb_hub_context *hc, const uint16_t port_status);

/* Abstract port feature manipulation */
busMsgResponse uhub_queue_clear_port_feature(usb_hub_context *hc, const uint8_t port, const uint8_t selector, sync_object *sync, int *queue_count);
busMsgResponse uhub_queue_set_port_feature(usb_hub_context *hc, const uint8_t port, const uint8_t selector, sync_object *sync, int *queue_count);

/* Specific port feature operations */
busMsgResponse uhub_queue_port_depower(usb_hub_context *hc, uint8_t port, int *queue_count);
busMsgResponse uhub_reset_port(usb_hub_context *hc, const uint8_t port, int *queue_count);

int uhub_handle_port_status_change(usb_hub_context *hc, const uint8_t port, int *queue_count);
busMsgResponse uhub_get_port_status(usb_hub_context *hc, const uint8_t port_index, usbHubPortStatus *port_status_out, sync_object *sync, int *queue_count);


#endif // UHUB_PORT_H_
