/* USB Hub driver structures
 *
 * Author: Joseph Kinzel */

#ifndef UHUB_STRUCT_H_
#define UHUB_STRUCT_H_

#include <dapi_types.h>
#include <dapi_container.h>
#include <usb.h>

/* Hub context flags */
#define UHUB_FLAGS_BASE_SPEED            0x0001U
#define UHUB_FLAGS_HIGH_SPEED            0x0002U
#define UHUB_FLAGS_SUPER_SPEED           0x0004U
#define UHUB_FLAGS_SUPER_SPEED_PLUS      0x0008U

#define UHUB_FLAGS_PORT_IND              0x0100U
#define UHUB_FLAGS_INV_PP                0x0200U
#define UHUB_FLAGS_OC_PROT               0x0400U
#define UHUB_FLAGS_PORT_OC               0x0800U

#define UHUB_FLAGS_MTT                   0x1000U
#define UHUB_FLAGS_COMPOUND              0x2000U

/* Port status flags */
#define UHUB_PMASK_CONNECT               0x0001U
#define UHUB_PMASK_ENABLE                0x0002U
#define UHUB_PMASK_SUSPEND               0x0004U
#define UHUB_PMASK_OVER_CURRENT          0x0008U
#define UHUB_PMASK_RESET                 0x0010U

#define UHUB_PMASK_POWER                 0x0100U
#define UHUB_PMASK_LOW_SPEED             0x0200U
#define UHUB_PMASK_HIGH_SPEED            0x0400U
#define UHUB_PMASK_PORT_TEST_MODE        0x0800U

#define UHUB_PF_INDICATOR_SOFTWARE       0x1000U

#define UHUB_GET_LINK_STATE(x)           (((x) >> 5) & 0xF)

#define UHUB_LINK_STATE_U0               0
#define UHUB_LINK_STATE_U1               1
#define UHUB_LINK_STATE_U2               2
#define UHUB_LINK_STATE_U3               3
#define UHUB_LINK_STATE_DISABLE          4
#define UHUB_LINK_STATE_DETECT           5
#define UHUB_LINK_STATE_INACTIVE         6
#define UHUB_LINK_STATE_POLLING          7
#define UHUB_LINK_STATE_RECOVERY         8
#define UHUB_LINK_STATE_HOT_RESET        9
#define UHUB_LINK_STATE_COMPLIANCE       10
#define UHUB_LINK_STATE_LOOPBACK         11

/* Port status values for SuperSpeedPlus devices */
#define UHUB_PORT_STATUS                 0x0000U
#define UHUB_PORT_EXT_STATUS             0x0004U

/* Port states and transitions */
typedef enum
{
    uhub_port_disconnected,
    uhub_port_disabled,
    uhub_port_disabling,
    uhub_port_enabled,
    uhub_port_enabling,
    uhub_port_connected,
    uhub_port_depowered,
    uhub_port_powered,
    uhub_port_resetting,
    uhub_port_active,
    uhub_port_suspended
} uhub_port_logical_state;


/* Pre-3.0 and ESS+ Hub descriptors */
typedef struct COMPILER_PACKED
{
    uint8_t bDescLength, bDescriptorType, bNumPorts;
    uint16_t wHubCharacteristics;
    uint8_t bPwerOn2PwerGood, bHubContrCurrent;
    uint8_t data[];
} usbHubDescriptor;

typedef struct COMPILER_PACKEd
{
    uint8_t bDescLength, bDescriptorType, bNumPorts;
    uint16_t wHubCharacteristics;
    uint8_t bPwerOn2PwerGood, bHubContrCurrent, bHubHdrDecLat;
    uint16_t wHubDelay, DeviceRemovable;
} usbESSHubDescriptor;

typedef struct COMPILER_PACKED
{
    uint16_t hub_status;
    uint16_t hub_status_change;
} usbHubStatus;


typedef struct COMPILER_PACKED
{
    uint16_t port_status;
    uint16_t port_change;
} usbHubPortStatus;

/* ESS lane speed information */
typedef struct
{
    uint8_t id, protocol;
    uint16_t mantissa;
    uint32_t lse_value;
} laneSublinkSpeed;

typedef struct
{
    void *default_addr_queue;
    void *default_addr_lock;
} uhub_root_complex;

typedef struct
{
    struct _usb_hub_context *hc;
    uint8_t port_index;
} uhub_port_status_change_context;

/* Hub context structure */
typedef struct _usb_hub_context
{
	DEVICE_HANDLE dh;
	BUS_HANDLE bus_spec;
	uint64_t full_addr;
	uint8_t usb_ver_major, usb_ver_minor, port_count, tt_time, status_ep;
	uint32_t flags, power_on_time, status_msg_size, max_port_power, hub_power_req;
	uint8_t *removable_mask;

	uint8_t ssp_speed_count;
	laneSublinkSpeed *ssp_speeds;
	/*! Buffer for status change updates */
	volatile uint8_t *status_change_buffer;
    /*! Callback for handling status change notifications */
    sync_object *status_cb;
    /*! Transaction requests */
	usbDevTransaction dt;
	usbDevRunTransactionsRequest rt;
	/* Cached hub status field */
	usbHubStatus last_hub_status;
	usbHubStatus pending_hub_status;
	sync_object *hub_status_cb;
	/* Cached port status fields */
	usbHubPortStatus *port_status_cache;
	uint32_t *ext_port_status_cache;
    uhub_port_status_change_context *ps_contexts;
	sync_object **port_status_cbs;
	uhub_port_logical_state *hub_port_state;
	uhub_root_complex *root_complex;
} usb_hub_context;

typedef struct COMPILER_PACKED
{
    sync_object *wait_obj;
    usb_hub_context *hc;
    uint8_t port_index;
} default_addr_queue_entry;

#endif // UHUB_STRUCT_H_
