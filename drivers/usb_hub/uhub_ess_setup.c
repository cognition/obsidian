/* USB Hub (Enhanced) SuperSpeed related functionality
 *
 *  Author: Joseph Kinzel */

#include <driverapi.h>
#include <usb.h>
#include "uhub_ops.h"
#include "uhub_log.h"
#include "uhub_port.h"
#include "uhub_ess_setup.h"
#include "uhub_desc.h"

#define USB_ESS_HUB_DESC_TYPE      0x2AU

/*! Interprets an SuperSpeed or higher hub descriptor
 *
 *  \param hc Hub context instance
 *  \param sync Wait/Signal synchronization object for the function to utilize
 *  \return driverOpSuccess if successful.  driverOpBadState if a problem was encountered.  driverOpBusError if an error was encountered in retrieving the hub descriptor.
 */
driverOperationResult usb_hub_process_ess_descriptor(usb_hub_context *hc, sync_object *sync)
{
    busMsgResponse status;
    volatile usbESSHubDescriptor *hub_desc;
    usb_device_get_descriptor desc_request;
    uint32_t res_size;

    res_size = sizeof(volatile usbESSHubDescriptor *);

    /* Retrieve the hub descriptor */
    desc_request.requestor = hc->bus_spec;
    desc_request.desc_type = USB_ESS_HUB_DESC_TYPE;
    desc_request.desc_index = 0;
    desc_request.request_type = USB_BMRT_CLASS;
    desc_request.expected_size = sizeof(usbESSHubDescriptor);
    desc_request.sync = sync;

    status = busMsg(&hub_desc, &res_size, BUS_ID_USB, hc->full_addr, &desc_request, sizeof(usb_device_get_descriptor), usbCoordGetDescriptor, hc->dh);
    if(status != busMsgAffirmative)
    {
        uhub_log(UHUB_LOG_ERRORS, "Error - Unable to get ESS Hub descriptor for device @ %X Status: %u\n", hc->full_addr, status);
        return driverOpBusError;
    }

    usb_hub_extract_common_desc_fields(hc, (volatile usbHubDescriptor *)hub_desc);

    if( (sizeof(usbHubDescriptor)) > hub_desc->bDescLength)
    {
        uhub_log(UHUB_LOG_ERRORS, "Error - Hub ESS descriptor for device @ %X size mismatch - Only %u bytes\n", hc->full_addr, hub_desc->bDescLength);

        return driverOpBadState;
    }

    memcpy(hc->removable_mask, (void *)&hub_desc->DeviceRemovable, hc->status_msg_size);

    if(hc->flags & UHUB_FLAGS_SUPER_SPEED_PLUS)
        hc->ext_port_status_cache = (uint32_t *)calloc(hc->port_count, 4);

    free((void *)hub_desc);
    return driverOpSuccess;
}

