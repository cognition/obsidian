/* USB Hub operations header
 *
 * Author: Joseph Kinzel */

#ifndef UHUB_OPS_H_
#define UHUB_OPS_H_

#include "uhub_struct.h"

#define UHUB_REQ_SET_DEPTH            12


busMsgResponse uhub_control_run(usb_hub_context *hc, const uint8_t direction);

busMsgResponse uhub_single_control_queue(usb_hub_context *hc, const uint8_t bmrt, const uint8_t request, const uint16_t w_index, const uint16_t w_value,
                                  const uint16_t w_length, volatile void *data_ptr, sync_object *sync);

busMsgResponse uhub_perform_single_control(usb_hub_context *hc, const uint8_t bmrt, const uint8_t request, const uint16_t w_index, const uint16_t w_value,
    const uint16_t w_length, volatile void *data_ptr, sync_object *sync);


#endif // UHUB_OPS_H_
