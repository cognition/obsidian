/* USB Hub driver logging interface
 *
 * Author: Joseph Kinzel */

#ifndef USB_HUB_LOG_H_
#define USB_HUB_LOG_H_

#define UHUB_LOG_ERRORS                           0x0001U
#define UHUB_LOG_INFO                             0x0002U
#define UHUB_LOG_MILESTONE                        0x0004U

#define UHUB_LOG_INTERNAL                         0x0100U

/* Logging conditions bitmask */
#define UHUB_LOG_CONDITIONS                       (UHUB_LOG_ERRORS | UHUB_LOG_INFO)

#define uhub_log(condition, ...)                  if(condition & UHUB_LOG_CONDITIONS) \
                                                  	printf("[UHUB]: " __VA_ARGS__);


#endif
