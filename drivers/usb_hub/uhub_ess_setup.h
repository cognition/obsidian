/* USB Hub Enchanced Super Speed Setup support fuctions
 *
 *
 * Author: Joseph Kinzel */

#ifndef UHUB_ESS_SETUP_H_

#include <dapi_types.h>
#include "uhub_struct.h"

driverOperationResult usb_hub_process_ess_descriptor(usb_hub_context *hc, sync_object *sync);
int uhub_decode_ext_status_speed(usb_hub_context *hc, const uint32_t ext_status);

#endif // UHUB_ESS_SETUP_H_
