/* USB Hub operations implementation
 *
 * Implements common operations for the USB bus
 *
 * Author: Joseph Kinzel */

#include <driverapi.h>
#include "uhub_ops.h"
#include "uhub_log.h"

busMsgResponse uhub_control_run(usb_hub_context *hc, const uint8_t direction)
{
    busMsgResponse status;
    usbDevRunTransactionsRequest rt;
    uint32_t res_size = 0;

    rt.requestor = hc->bus_spec;
    rt.endpoint = 0;
    rt.direction = direction;

    status = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, &rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, hc->dh);
    if(status != busMsgAcknowledged)
        uhub_log(UHUB_LOG_ERRORS, "Failed to run single transaction for device @ %X\n", hc->full_addr);


    return status;
}

busMsgResponse uhub_single_control_queue(usb_hub_context *hc, const uint8_t bmrt, const uint8_t request, const uint16_t w_index, const uint16_t w_value,
                                  const uint16_t w_length, volatile void *data_ptr, sync_object *sync) {

    busMsgResponse status;
    uint32_t res_size;
    usbDevTransaction dt;

    dt.type = usbTransactionControl;
    dt.flags = 0;
    dt.direction = bmrt & USB_REQ_TYPE_DEV_TO_HOST ? USB_TRANSACTION_IN:USB_TRANSACTION_OUT;
    dt.endpoint = 0;
    dt.control.bm_request_type = bmrt;
    dt.control.b_request = request;
    dt.control.w_index = w_index;
    dt.control.w_value = w_value;
    dt.control.w_length = w_length;
    dt.control.data_ptr = data_ptr;
    dt.sync = sync;
    dt.requestor = hc->bus_spec;

    res_size = 0;

    status = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, &dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, hc->dh );
    if(status != busMsgAcknowledged)
        uhub_log(UHUB_LOG_ERRORS, "Failed to queue single transaction for device @ %X - Request: %X BMRT: %X\n", hc->full_addr, request, bmrt);


    return status;
}

busMsgResponse uhub_perform_single_control(usb_hub_context *hc, const uint8_t bmrt, const uint8_t request, const uint16_t w_index, const uint16_t w_value,
    const uint16_t w_length, volatile void *data_ptr, sync_object *sync) {

    busMsgResponse status;
    int direction;

    direction = bmrt & USB_REQ_TYPE_DEV_TO_HOST ? USB_TRANSACTION_IN:USB_TRANSACTION_OUT;

    status = uhub_single_control_queue(hc, bmrt, request, w_index, w_value, w_length, data_ptr, sync);
    if(status != busMsgAcknowledged)
        return status;

    status = uhub_control_run(hc, direction);

    return status;
}
