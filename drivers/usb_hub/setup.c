/* Setup and registration code for the USB Hub driver
 *
 * Author: Joseph Kinzel */

#include <driverapi.h>
#include <usb.h>
#include "uhub_ops.h"
#include "uhub_log.h"
#include "uhub_port.h"
#include "uhub_ess_setup.h"
#include "uhub_desc.h"

#define UHUB_REQ_RESET_TT            9


/* Prototypes for module table */
driverOperationResult usb_hub_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet);
driverOperationResult usb_hub_deinit(void *context);
driverOperationResult usb_hub_enum(void *context, void *identifyFunc);

/* Module export table */
COMPILER_MODINFO_SECTION const modExportTable moduleInfoTable = {&usb_hub_init, &usb_hub_deinit, &usb_hub_enum};

static void *uhub_root_complex_map = NULL;
static void *uhub_rc_map_lock = NULL;

/*! Indicates the speed of the hub
 *
 *  \param hc Hub context instance
 *  \param dev_desc USB device descriptor for the hub
 *  \return driverOpSuccess if the speed could be ascertained or driverOpBadStatus if the hub's speed was unrecognized
 */
driverOperationResult usb_hub_determine_speed(usb_hub_context *hc, const usbDeviceDescriptor *dev_desc, const uint8_t dev_speed )
{
    uhub_log(UHUB_LOG_INFO, "Determining version for hub @ %X with major:minor - %X:%X\n", hc->full_addr, hc->usb_ver_major, hc->usb_ver_minor);
	switch(dev_speed)
	{
        case USB_SPEED_LOW:
        case USB_SPEED_FULL:
            if(dev_desc->bDeviceProtocol > 1)
                return driverOpBadState;

            hc->flags |= UHUB_FLAGS_BASE_SPEED;
            uhub_log(UHUB_LOG_INFO, "Found USB1.x hub @ %X\n", hc->full_addr);
            break;
        case USB_SPEED_HIGH:
			if( hc->usb_ver_major < 0x02U )
				return driverOpBadState;
            switch(dev_desc->bDeviceProtocol)
            {
                case 1:
                    break;
                case 2:
                    hc->flags |= UHUB_FLAGS_MTT;
                    break;
                default:
                    return driverOpBadState;
                    break;
            }
            hc->flags |= UHUB_FLAGS_HIGH_SPEED;
			uhub_log(UHUB_LOG_INFO, "Found USB2.x hub @ %X\n", hc->full_addr);
            break;
        case USB_SPEED_ESUPER_1X2:
        case USB_SPEED_ESUPER_2X2:
            if( (hc->usb_ver_major == 0x03) && (hc->usb_ver_minor < 0x20))
                return driverOpBadState;
        case USB_SPEED_ESUPER_1X1:
        case USB_SPEED_ESUPER_2X1:
            if( (hc->usb_ver_major == 0x03) && (hc->usb_ver_minor < 0x10))
                return driverOpBadState;
            hc->flags |= UHUB_FLAGS_SUPER_SPEED_PLUS | UHUB_FLAGS_SUPER_SPEED;
            break;
        case USB_SPEED_SUPER:
            if( (hc->usb_ver_major < 0x03) || (dev_desc->bDeviceProtocol < 3))
                return driverOpBadState;
            hc->flags |= UHUB_FLAGS_SUPER_SPEED;
            break;
	}

	return driverOpSuccess;
}


/*! Attempts to transition the hub to the configured state
 *
 *  \param hc USB Hub controller context instance
 *  \param max_bus_power Maximum power available from vbus for the device
 *  \param num_configurations Number of potential device configurations available
 *
 */
driverOperationResult usb_hub_configure(usb_hub_context *hc, const uint32_t max_bus_power, const uint32_t num_configurations)
{
    usb_request_config_msg config_request;
    usb_decode_ep_request decode_ep_request;
    usb_decode_ep_response ep_parse_res;
    volatile usbConfigDescriptor *config_desc;
    volatile usbInterfaceDescriptor *interface_desc;
    volatile uint8_t *desc_data;
    uint32_t res_size, config_desc_offset;
    busMsgResponse status;
    usbConfigureEndpointFields ep_info;

    res_size = sizeof(uintptr_t);

    /* Request that bus coordinator driver select it's best guess at the appropriate configuration */

    config_request.requestor = hc->bus_spec;
    config_request.descriptor_index = -1;

    status = busMsg(&config_desc, &res_size, BUS_ID_USB, hc->full_addr, &config_request, sizeof(usb_request_config_msg), usbCoordRequestConfig, hc->dh);

    if(status != busMsgAcknowledged)
    {
        uhub_log(UHUB_LOG_ERRORS, "Error - configuration failed for device @ %X Value: %u\n", hc->full_addr, status);
        return driverOpBusError;
    }
    else
        uhub_log(UHUB_LOG_INTERNAL, "Retrieved full configuration descriptor for device @ %X\n", hc->full_addr);


    /* USB Hub specification requires only a single interface on the device */
    if(config_desc->bNumInterfaces != 1)
    {
        uhub_log(UHUB_LOG_ERRORS, "Error - Hub @ %X reports more than one interface!\n", hc->full_addr);
        return driverOpBadState;
    }

    desc_data = (volatile uint8_t *)config_desc;
    config_desc_offset = config_desc->bLength;

    if(config_desc->wTotalLength < (config_desc_offset + sizeof(usbEndpointDescriptor) + sizeof(usbInterfaceDescriptor)) )
    {

        uhub_log(UHUB_LOG_ERRORS, "Error - Hub @ %X had configuration descriptor that was smaller than expected %u bytes.\n", config_desc->wTotalLength);
        return driverOpBadState;
    }


    if(hc->flags & UHUB_FLAGS_MTT)
    {
        usb_request_alt_interfaces *alt_iface_req;
        int last_desc_size = 0;

        interface_desc = NULL;
        uhub_log(UHUB_LOG_INFO, "Searching for MTT interface on hub @ %X Configuration descriptor @ %p Size: %u CO: %u\n", hc->full_addr, desc_data, config_desc->wTotalLength, config_desc_offset);
        for(int desc_offset = config_desc_offset; desc_offset < config_desc->wTotalLength; desc_offset += last_desc_size)
        {
            last_desc_size = desc_data[desc_offset + USB_DESC_FIELD_LENGTH_OFF];
            if((desc_offset + sizeof(usbInterfaceDescriptor) + sizeof(usbEndpointDescriptor)) <= config_desc->wTotalLength)
            {
                if(desc_data[desc_offset + USB_DESC_FIELD_TYPE_OFF] == usbDescInterface)
                {
                    volatile usbInterfaceDescriptor *tmp_interface;
                    tmp_interface = (volatile usbInterfaceDescriptor *)&desc_data[desc_offset];
                    uhub_log(UHUB_LOG_INFO, "Found candidate interface #%u Class: %u Protocol: %u EP#:%u Alt: %u\n", tmp_interface->bInterfaceNumber,
                             tmp_interface->bInterfaceClass, tmp_interface->bInterfaceProtocol, tmp_interface->bNumEndpoints, tmp_interface->bAlternateSettings);
                    if( (tmp_interface->bInterfaceClass == USB_CLASS_HUB) && (tmp_interface->bInterfaceProtocol == 2) && (tmp_interface->bNumEndpoints == 1))
                    {
                        uhub_log(UHUB_LOG_INFO, "Found MTT interface @ offset %u\n", desc_offset);
                        interface_desc = tmp_interface;
                        config_desc_offset = desc_offset + last_desc_size;
                        break;
                    }
                }
            }
        }

        /* We need at least one viable MTT interface on an MTT hub */
        if(!interface_desc)
            return driverOpHardwareError;

        uhub_log(UHUB_LOG_INFO, "Found MTT interface #%u Alt value: %u for device @ %X\n", interface_desc->bInterfaceNumber, interface_desc->bAlternateSettings, hc->full_addr);
        if(interface_desc->bAlternateSettings)
        {
            busMsgResponse b_result;
            uint32_t res_size, req_size;

            /* Enable the alternate interface */
            req_size = sizeof(usb_request_alt_interfaces);

            alt_iface_req = (usb_request_alt_interfaces *)calloc(req_size, 1);
            alt_iface_req->requestor = hc->bus_spec;
            alt_iface_req->interface_mask = 1<<interface_desc->bInterfaceNumber;
            alt_iface_req->interface_indices[interface_desc->bInterfaceNumber] = interface_desc->bAlternateSettings;

            res_size = 0;
            b_result = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, alt_iface_req, req_size, usbCoordRequestAltInterface, hc->dh );

            if(b_result != busMsgAcknowledged)
            {
                free(alt_iface_req);
                free((void *)desc_data);

                return driverOpBusError;
            }

            free(alt_iface_req);
        }
    }
    else
    {
        interface_desc = (volatile usbInterfaceDescriptor *)&desc_data[config_desc_offset];


        /* Hubs should only have a single endpoint and no alternate settings */
        if( (interface_desc->bNumEndpoints != 1) || interface_desc->bAlternateSettings )
        {
            uhub_log(UHUB_LOG_ERRORS, "Error - Hub @ %X had %u endpoints and a %u alternate setting value!\n", hc->full_addr, interface_desc->bNumEndpoints, interface_desc->bAlternateSettings);
            return driverOpHardwareError;
        }

        config_desc_offset += interface_desc->bLength;

    }
    /* Interpret the status input endpoint descriptor */

    decode_ep_request.requestor = hc->bus_spec;
    decode_ep_request.ep_fields = &ep_info;
    decode_ep_request.ep_desc = (const uint8_t *)&desc_data[config_desc_offset];
    decode_ep_request.max_desc_bytes = config_desc->wTotalLength - config_desc_offset;

    uhub_log(UHUB_LOG_INTERNAL, "Attempting to decode status input endpoint descriptor for device @ %X\n", hc->full_addr);

    res_size = sizeof(usb_decode_ep_response);
    status = busMsg(&ep_parse_res, &res_size, BUS_ID_USB, hc->full_addr, &decode_ep_request, sizeof(usb_decode_ep_request), usbCoordDecodeEndpointDesc, hc->dh);
    if(status != busMsgAcknowledged)
    {

        uhub_log(UHUB_LOG_ERRORS, "Error - Failed to parse endpoint descriptor for device @ %X\n", hc->full_addr);
        return driverOpBadState;
    }

    if( (ep_info.ep_type != USB_EP_TYPE_INTERRUPT) || !ep_info.direction )
    {
        uhub_log(UHUB_LOG_ERRORS, "Error - Hub @ had an endpoint mismatch type: %u direction: %u\n", hc->full_addr, ep_info.ep_type, ep_info.direction);
        return driverOpBadState;
    }

    hc->status_ep = ep_info.ep_num;

    uhub_log(UHUB_LOG_INTERNAL, "Successfully configured device @ %X\n", hc->full_addr );
    return driverOpSuccess;
}

/*! Powers on ports so that connection status can be determined
 *
 *  \param hc Hub context pointer
 *
 *  \return busMsgAcknowledged if successful, busMsgNegative or a transaction related error code otherwise
 */
static busMsgResponse uhub_standard_port_enum(usb_hub_context *hc)
{
    busMsgResponse status;
    int transaction_count;
    uint32_t wait_time;
    sync_object *enum_wait;
    uint64_t response;

    transaction_count = 0;

    sync_create_signal_obj(&enum_wait, 0, 0);

    /*
    if(hc->flags & UHUB_FLAGS_HIGH_SPEED)
    {

        if(hc->flags & UHUB_FLAGS_MTT)
        {
            for(int port_index = 1; port_index <= hc->port_count; port_index++ )
            {
                uhub_single_control_queue(hc, USB_REQ_TYPE_OTHER | USB_REQ_TYPE_CLASS, UHUB_REQ_RESET_TT, port_index, 0, 0, NULL, NULL);
            }
        }
        else
            uhub_single_control_queue(hc, USB_REQ_TYPE_OTHER | USB_REQ_TYPE_CLASS, UHUB_REQ_RESET_TT, 1, 0, 0, NULL, NULL);


        uhub_control_run(hc, USB_TRANSACTION_IN);

        wait(50);

    }
    */

    /* By this point the hub must be configured and should have been reset along with either the parent hub or host controller.
     * That means the downstream ports should be in the 'Powered off' state.  They'll need to be powered on so that connection status can be polled. */
    for(int port_num = 1; port_num <= hc->port_count; port_num++)
    {
        uhub_log(UHUB_LOG_INTERNAL, "Powering USB2.x port @ %X:%u\n", hc->full_addr, port_num);
        hc->hub_port_state[port_num-1] = uhub_port_disconnected;
        status = uhub_queue_set_port_feature(hc, port_num, UHUB_PF_POWER, (port_num == hc->port_count) ? enum_wait:NULL , &transaction_count);
        if(status != busMsgAcknowledged)
            return status;
    }

    /* Just run the transactions and let the state change interrupt pipe catch and deal with any changes in connection state */
    uhub_control_run(hc, USB_TRANSACTION_IN);

    wait_time = transaction_count*50;
    if(wait_time < (hc->power_on_time))
        wait_time = hc->power_on_time;
    sync_object_wait(enum_wait, wait_time );

    response = sync_object_get_status(enum_wait);
    if(response != usb_op_success )
    {
        uhub_log(UHUB_LOG_ERRORS, "Received unexpected response during standard hub enumeration: %u\n", response);
        return busMsgNegative;
    }

    sync_destroy_signal_obj(enum_wait);
    return busMsgAcknowledged;
}


/*! Retrieves the initial state of the hub into the structures of the hub cache
 *
 *  \param hc Hub context pointer
 *  \param topo_string Topology string containing the depth of the hub
 *  \param wait_obj Signal synchronization object
 *
 *  \return busMsgAcknowledged if successful, various others depending on nature of the error encountered
 */
busMsgResponse usb_hub_get_init_state(usb_hub_context *hc, const uint32_t topo_string, sync_object *wait_obj)
{
    busMsgResponse result;
    int queue_count;

    queue_count = 0;

    if(hc->flags & UHUB_FLAGS_SUPER_SPEED)
    {
        result = uhub_single_control_queue(hc, USB_REQ_TYPE_CLASS | USB_REQ_TYPE_HOST_TO_DEV | USB_REQ_TYPE_DEVICE, UHUB_REQ_SET_DEPTH, 0, USB_TOPO_GET_DEPTH(topo_string), 0, NULL, NULL );
        if( result != busMsgAcknowledged )
        {
            uhub_log(UHUB_LOG_ERRORS, "Failed to dispatch SET_HUB_DEPTH request for hub @ %X\n", hc->full_addr);
            return result;
        }
    }

    result = uhub_single_control_queue(hc, USB_REQ_TYPE_CLASS | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_DEVICE, USB_CONTROL_REQ_GET_STATUS, 0, 0, 4, &hc->last_hub_status, NULL);

    if( result != busMsgAcknowledged )
    {
        uhub_log(UHUB_LOG_ERRORS, "Failed to queue initial hub status request for device @ %X\n", hc->full_addr);
        return result;
    }

    /* Note: We're going to be queueing a lot of transactions so there's no run request yet and no synchronization object attached */
    for(int port_index = 1; port_index <= hc->port_count; port_index++)
    {
        result = uhub_get_port_status(hc, port_index, &hc->port_status_cache[port_index-1],
                                      (port_index == hc->port_count) ? wait_obj:NULL, &queue_count);

        if( result != busMsgAcknowledged )
        {
            uhub_log(UHUB_LOG_ERRORS, "Failed to queue initial port status request for port %u on hub %u @ %X\n", port_index, hc->full_addr);
            return result;
        }
    }

    /* Run all the GET_STATUS transactions and wait for the successful completion of all of them */
    result = uhub_control_run(hc, USB_TRANSACTION_IN);
    if( result != busMsgAcknowledged )
    {
        uhub_log(UHUB_LOG_ERRORS, "Failed to run initial hub and port status requests for hub @ %X\n", hc->full_addr);
        return result;
    }

    sync_object_wait( wait_obj, hc->port_count*1000 );
    if( sync_object_get_status(wait_obj) != usb_op_success )
    {
        uhub_log(UHUB_LOG_ERRORS, "Initial status requests for hub timed out for hub @ %X\n", hc->full_addr);
        return busMsgNoAck;
    }
    else
        uhub_log(UHUB_LOG_INTERNAL, "Initial state retrieved for hub @ %X\n", hc->full_addr);

    for(int port_index = 0; port_index < hc->port_count; port_index++)
    {
        uhub_log(UHUB_LOG_INTERNAL, "Port @ %X:%u initial status: %X\n", hc->full_addr, port_index+1, hc->port_status_cache[port_index]);
    }

    if( !(hc->flags & UHUB_FLAGS_SUPER_SPEED) )
        return uhub_standard_port_enum(hc);
    else
        return busMsgAcknowledged;
}

void uhub_process_hub_status(void *context, uint64_t bytes_transferred)
{
    usb_hub_context *hc;

    hc = (usb_hub_context *)context;

    uhub_log(UHUB_LOG_INTERNAL, "Hub controller status update triggered for hub @ %X New status: %X Change: %X\n", hc->full_addr, hc->pending_hub_status.hub_status, hc->pending_hub_status.hub_status_change);
}

/*! Processes a change in the overall status of a hub
 *
 *  \param hc Hub context instance
 *  \param queued Pointer to the queued transaction counter
 */
void usb_hub_status_change(usb_hub_context *hc, int *queued)
{
    uhub_single_control_queue(hc, USB_REQ_TYPE_CLASS | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_DEVICE, USB_CONTROL_REQ_GET_STATUS, 0, 0, 4, &hc->pending_hub_status, hc->hub_status_cb);
    (*queued)++;
}

/*! Reissues a single status input transaction on the status endpoint of a hub
 *
 *  \param hc Hub context pointer
 *
 *  \return driverOpSuccess if successful, driverOpBusError is the transaction or run command fails
 */
driverOperationResult usb_hub_issue_status_transaction(usb_hub_context *hc)
{
    busMsgResponse status;
    usbDevRunTransactionsRequest *rt;
    usbDevTransaction *dt;
    uint32_t res_size;

    dt = &hc->dt;
    rt = &hc->rt;

    res_size = 0;

    /* Initialize the interrupt in endpoint transaction information for the status change endpoint */
    dt->type = usbTransactionInterrupt;
    dt->direction = USB_TRANSACTION_IN;
    dt->flags = USB_TRANSACTION_IN;
    dt->sync = hc->status_cb;
    dt->requestor = hc->bus_spec;
    dt->endpoint = hc->status_ep;
    dt->transfer.data = hc->status_change_buffer;
    dt->transfer.length = hc->status_msg_size;

    rt->direction = USB_TRANSACTION_IN;
    rt->endpoint = hc->status_ep;
    rt->requestor = hc->bus_spec;

    status = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, hc->dh );
    if(status != busMsgAcknowledged)
    {
        uhub_log(UHUB_LOG_ERRORS, "Failed to queue status change transaction for device @ %X Status: %u\n", hc->full_addr, status);
        return driverOpBusError;
    }

    status = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, hc->dh);
    if(status != busMsgAcknowledged)
    {
        uhub_log(UHUB_LOG_ERRORS, "Failed to run status change transaction for device @ %X Status: %u\n", hc->full_addr, status);
        return driverOpBusError;
    }

    return driverOpSuccess;
}


/*! Status change pipe callback
 *
 *  \param context USB Hub context instance
 *  \param bytes_transferred Number of bytes transferred the transaction
 */
void usb_hub_handle_status_change(void *context, uint64_t bytes_transferred)
{
    usb_hub_context *hc;
    uint8_t mask;
    int buffer_index;
    int queued = 0;

    hc = (usb_hub_context *)context;
    buffer_index = 0;

    mask = 1;
    /* Check for a change in the hub status */
    if(hc->status_change_buffer[0] & mask)
        usb_hub_status_change(hc, &queued);

    mask <<= 1;

    for(int port_index = 1; port_index <= hc->port_count; port_index++, mask <<= 1)
    {
        if(!mask)
        {
            mask = 1;
            buffer_index++;
        }

        if(hc->status_change_buffer[buffer_index] & mask)
        {
            busMsgResponse result;

            result = uhub_get_port_status(hc, port_index, &hc->port_status_cache[port_index-1], hc->port_status_cbs[port_index-1], &queued);
            if(result != busMsgAcknowledged)
                uhub_log(UHUB_LOG_ERRORS, "Error failed to dispatch port GET_STATUS request on status update for port @ %X:%u\n", hc->full_addr, port_index);
        }
    }

    if(queued)
        uhub_control_run(hc, USB_TRANSACTION_IN);

    usb_hub_issue_status_transaction(hc);
}

/*! Dispatches the initial of interrupt pipe transactions
 *
 *  \param hc Hub context instance
 *
 *  \return driverOpSuccess if successful or driverOpBusError if the transactions could not be scheduled
 */
driverOperationResult usb_hub_init_status_transactions(usb_hub_context *hc)
{
    busMsgResponse status;
    usbDevRunTransactionsRequest *rt;
    usbDevTransaction *dt;
    uint32_t res_size;

    dt = &hc->dt;
    rt = &hc->rt;

    res_size = 0;

    sync_create_callback_obj(&hc->status_cb, hc, &usb_hub_handle_status_change);
    sync_create_callback_obj(&hc->hub_status_cb, hc, &uhub_process_hub_status);

    /* Initialize the interrupt in endpoint transaction information for the status change endpoint */
    dt->type = usbTransactionInterrupt;
    dt->direction = USB_TRANSACTION_IN;
    dt->flags = USB_TRANSACTION_IN;
    dt->sync = hc->status_cb;
    dt->requestor = hc->bus_spec;
    dt->endpoint = hc->status_ep;
    dt->transfer.data = hc->status_change_buffer;
    dt->transfer.length = hc->status_msg_size;

    rt->direction = USB_TRANSACTION_IN;
    rt->endpoint = hc->status_ep;
    rt->requestor = hc->bus_spec;

    for(int status_transaction_count = 0; status_transaction_count < 4; status_transaction_count++)
    {
        status = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, hc->dh );
        if(status != busMsgAcknowledged)
        {
            uhub_log(UHUB_LOG_ERRORS, "Failed to queue status change transactions for device @ %X Status: %u\n", hc->full_addr, status);
            return driverOpBusError;
        }
    }

    status = busMsg(NULL, &res_size, BUS_ID_USB, hc->full_addr, rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, hc->dh);
    if(status != busMsgAcknowledged)
    {
        uhub_log(UHUB_LOG_ERRORS, "Failed to run status change transactions for device @ %X Status: %u\n", hc->full_addr, status);
        return driverOpBusError;
    }

    return driverOpSuccess;
}

/*! Destroys a hub context and frees all memory associated with it
 *
 *  \param hc Hub context instance to destroy
 */
void usb_hub_destroy_context(usb_hub_context *hc)
{
    if(hc->status_cb)
        sync_destroy_signal_obj(hc->status_cb);
    if(hc->port_status_cache)
        free(hc->port_status_cache);
    if(hc->removable_mask)
        free(hc->removable_mask);

    free(hc);
}

/*! Creates and initializes a new root complex object
 *
 *  \return A pointer to a new and valid root complex structure
 */
static uhub_root_complex *uhub_create_root_complex(void)
{
    uhub_root_complex *rc;

    rc = (uhub_root_complex *)calloc(sizeof(uhub_root_complex), 1);

    rc->default_addr_queue = dapi_queue_create(sizeof(default_addr_queue_entry));
    rc->default_addr_lock = dapi_lock_create();

    return rc;
}

/*! Locates or creates a root complex structure for the provided address
 *
 *  \param addr Address of the hub device
 *
 *  \return A pointer the root complex corresponding to the controller that the hub is attached to
 */
static uhub_root_complex *uhub_locate_root_complex(const uint64_t addr)
{
    uint32_t id;
    uhub_root_complex *rc;

    id = USB_GET_CONTROLLER_ID(addr);
    dapi_lock_acquire(uhub_rc_map_lock);

    rc = (uhub_root_complex *)dapi_map_get_mut(uhub_root_complex_map, id);
    if(!rc)
    {
        rc = uhub_create_root_complex();
        dapi_map_insert(uhub_root_complex_map, id, rc);
    }

    dapi_lock_release(uhub_rc_map_lock);

    return rc;
}

/*! Callback function wrapper for handling port status changes
 *  \note Connected to a port targeted GET_STATUS transaction scheduled when the hub detects a port status change on the status endpoint
 *
 *  \param context Port status change context provided by the callback object
 *  \param status_value Value signaled by the signaling procedure, should be translatable to a usb_hw_op_result value
 *
 */
static void uhub_handle_ps_update(void *context, const uint64_t status_value)
{
    uhub_port_status_change_context *psc;
    int queue_count;

    if(status_value != usb_op_success)
    {
        /* TODO: Reissue transaction or handle failure */
    }

    queue_count = 0;
    psc = (uhub_port_status_change_context *)context;

    uhub_handle_port_status_change(psc->hc, psc->port_index, &queue_count);
    if(queue_count)
        uhub_control_run(psc->hc, USB_TRANSACTION_IN);
}

/*! Initialization function */
driverOperationResult usb_hub_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet)
{
	driverOperationResult status;
	busMsgResponse msg_status;
	usbDeviceDescriptor *dev_desc;
	usb_device_entry_context *dev_desc_info;
	usb_hub_context *hc;
	sync_object *wait_obj;
	dev_desc_info = (usb_device_entry_context *)input_context;

	if(dev_desc_info->length < sizeof(usbDeviceDescriptor))
		return driverOpBadState;

	dev_desc = (usbDeviceDescriptor *)dev_desc_info->desc_buffer;

	/* Allocate and populate the hub context instance */
	hc = (usb_hub_context *)calloc(1, sizeof(usb_hub_context));
	hc->full_addr = desc->address;
	hc->dh = dev;
	hc->bus_spec = bus_spec;

	hc->usb_ver_major = dev_desc->bcdVerMajor;
	hc->usb_ver_minor = dev_desc->bcdVerMinor;

	if(!uhub_root_complex_map)
    {
        uhub_rc_map_lock = dapi_lock_create();
        uhub_root_complex_map = dapi_map_create();
    }

	hc->root_complex =  uhub_locate_root_complex(desc->address);

	/* Figure out the hub's speed and get it into the configured state */
	status = usb_hub_determine_speed(hc, dev_desc, dev_desc_info->dev_speed);
	if(status != driverOpSuccess)
    {
		uhub_log(UHUB_LOG_ERRORS, "Error determining speed for device @ %X\n", hc->full_addr);
		free(hc);
		return driverOpVersionMismatch;
    }

    sync_create_signal_obj(&wait_obj, 0, 0);

    /* ESS Hubs will require some extra descriptors */
    if(hc->flags & UHUB_FLAGS_SUPER_SPEED)
        uhub_get_bos_desc(hc, wait_obj);


    status = usb_hub_configure(hc, dev_desc_info->bus_power_available, dev_desc->bNumConfigurations );
    if(status != driverOpSuccess)
        goto init_fail_cleanup;

    uhub_log(UHUB_LOG_MILESTONE, "Hub SET_CONFIGURATION was successful for device @ %X\n", hc->full_addr);
    /* Process the hub descriptor based on the speed of the hub */
    if(hc->flags & (UHUB_FLAGS_BASE_SPEED | UHUB_FLAGS_HIGH_SPEED) )
        status = usb_hub_process_descriptor(hc, wait_obj);
    else if(hc->flags & UHUB_FLAGS_SUPER_SPEED)
        status = usb_hub_process_ess_descriptor(hc, wait_obj);
    else
        return driverOpVersionMismatch;

    uhub_log(UHUB_LOG_MILESTONE, "Hub descriptor parsing was successful for device @ %X\n", hc->full_addr);

    if(status != driverOpSuccess)
        goto init_fail_cleanup;

    /* Allocate the buffers for the various per port structures */
    hc->status_change_buffer = (volatile uint8_t *)calloc(hc->status_msg_size, 1);
    hc->port_status_cache = (usbHubPortStatus *)calloc(hc->port_count, sizeof(usbHubPortStatus));
    hc->ps_contexts = (uhub_port_status_change_context *)calloc( sizeof(uhub_port_status_change_context), hc->port_count);
    hc->port_status_cbs = (sync_object **)calloc(sizeof(sync_object *), hc->port_count);
    hc->hub_port_state = (uhub_port_logical_state *)calloc(sizeof(uhub_port_logical_state), hc->port_count);

    /* Create the call back objects for port status changes */
    for(int port_index = 0; port_index < hc->port_count; port_index++)
    {
        uhub_port_status_change_context *ps = &hc->ps_contexts[port_index];

        ps->hc = hc;
        ps->port_index = port_index + 1;

        sync_create_callback_obj(&hc->port_status_cbs[port_index], ps, &uhub_handle_ps_update);
    }

    uhub_log(UHUB_LOG_MILESTONE, "Hub configured @ %X on route %X with %u downstream ports and status endpoint %u\n", hc->full_addr, dev_desc_info->topo_string, hc->port_count, hc->status_ep);
	*contextRet = hc;


	/* Get the initial status of the hub controller and ports, will also power on USB2 or earlier ports */
	msg_status = usb_hub_get_init_state(hc, dev_desc_info->topo_string, wait_obj);
	if(msg_status != busMsgAcknowledged)
    {
        status = driverOpBusError;
        uhub_log(UHUB_LOG_ERRORS, "Hub @ %X failed to retrieve initial state!\n", hc->full_addr);
        goto init_fail_cleanup;
    }

    /* Issue the initial batch of input transaction on the status endpoint */
    usb_hub_init_status_transactions(hc);

	uhub_log(UHUB_LOG_INFO, "USB Hub driver active for device @ %X\n", desc->address);
    goto init_cleanup;

init_fail_cleanup:
    uhub_log(UHUB_LOG_ERRORS, "Hub configuration failed with status: %u\n", status);
    usb_hub_destroy_context(hc);
init_cleanup:
	sync_destroy_signal_obj(wait_obj);
	return status;
}

/*! De-initialization function */
driverOperationResult usb_hub_deinit(void *context)
{
	return driverOpSuccess;
}

/*! Enumerate function */
driverOperationResult usb_hub_enum(void *context, void *identifyFunc)
{
	return driverOpSuccess;
}

