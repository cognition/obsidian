/* USB Descriptors and descriptor specific fields
 *
 * Author: Joseph Kinzel */

#ifndef USB_DESC_H_
#define USB_DESC_H_

#include <dapi_types.h>

/* Spec. defined misc. values */
#define USB_DEFAULT_CONTROL_EP                    0

/* Standard descriptor field offsets */
#define USB_DESC_FIELD_LENGTH_OFF                 0
#define USB_DESC_FIELD_TYPE_OFF                   1
#define USB_DESC_FIELD_INTERFACE_ALT              3

#define USB_DESC_DEV_CAP_TYPE                     2

/* Descriptor request types */
#define USB_DESCRIPTOR_DEVICE                     (1<<8)
#define USB_DESCRIPTOR_CONFIG                     (2<<8)
#define USB_DESCRIPTOR_STRING                     (3<<8)
#define USB_DESCRIPTOR_INTERFACE                  (4<<8)
#define USB_DESCRIPTOR_ENDPOINT                   (5<<8)
#define USB_DESCRIPTOR_DEV_QUAL                   (6<<8)
#define USB_DESCRIPTOR_SCONFIG                    (7<<8)
#define USB_DESCRIPTOR_IPOWER                     (8<<8)

/* Configuration descriptor attribute fields */
#define USB_CONFIG_DESC_ATTR_SELF_POWERED         0x40U
#define USB_CONFIG_DESC_ATTR_REMOTE_WAKEUP        0x20U

/* Endpoint descriptor address bitfields */
#define USB_EP_GET_NUM(x)                         (x & 0x0FU)
#define USB_EP_GET_DIR(x)                         (x & 0x80U)

#define USB_EP_DIR_OUT                            0x00U
#define USB_EP_DIR_IN                             0x80U

/* Endpoint descriptor attribute bitfields */
#define USB_EP_GET_TYPE(x)                        (x & 0x03U)
#define USB_EP_GET_SYNC(x)                        ((x >> 2) & 0x03U)
#define USB_EP_GET_USAGE(x)                       ((x >> 4) & 0x03U)

#define USB_EP_TYPE_CONTROL                       0x00U
#define USB_EP_TYPE_ISOCHRON                      0x01U
#define USB_EP_TYPE_BULK                          0x02U
#define USB_EP_TYPE_INTERRUPT                     0x03U

#define USB_EP_SYNC_NONE                          0x00U
#define USB_EP_SYNC_ASYNC                         0x01U
#define USB_EP_SYNC_ADAPTIVE                      0x02U
#define USB_EP_SYNC_SYNC                          0x03U

#define USB_EP_USAGE_DATA                         0x00U
#define USB_EP_USAGE_FEEDBACK                     0x01U
#define USB_EP_USAGE_IMP_FB                       0x02U

/* Mask versions of the endpoint type fields */
#define USB_TYPE_FIELD_CONTROL                    0x1U
#define USB_TYPE_FIELD_ISOCHRON                   0x2U
#define USB_TYPE_FIELD_BULK                       0x4U
#define USB_TYPE_FIELD_INTERRUPT                  0x8U

/* Endpoint descriptor packet size extraction */
#define USB2_EP_GET_PSIZE(x)                      (x & 0x7FFU)
#define USB_EP_GET_AT(x)                          ((x >> 11) & 0x03U)

#define USB_EP_AT_NONE                            0x00U
#define USB_EP_AT_ONE                             0x01U
#define USB_EP_AT_TWO                             0x02U

/* Superspeed Endpoint Companion values and macros */
#define USB_SS_EPC_ISOC                           0x80U

#define USB_SS_GET_STREAMS(x)                     ((x) & 0x1FU)
#define USB_SS_GET_MULT(x)                        ((x) & 0x03U)

/* Hub attributes */
#define UHUB_ATTRIBUTES_OCP_PER_PORT              0x0008U

/* USB Descriptor Types */
typedef enum
{
    usbDescDevice = 1,
    usbDescConfig = 2,
    usbDescString = 3,
    usbDescInterface = 4,
    usbDescEndpoint = 5,
    usbDescDevQual = 6,
    usbDescOtherSpeed = 7,
    usbDescInterfacePower = 8,
    usbDescBOS = 15,
    usbDescDeviceCapability = 16,
    usbDescHub = 0x29,
    usbDescESSHub = 0x2A,
    usbDescSSEC = 48,
    usbDescSSISOEC = 49
} usbDescType;


typedef enum
{
    devQualWirelessUSB       = 0x01U,
    devQualUsb2Ext           = 0x02U,
    devQualSuperSpeedUSB     = 0x03U,
    devQualContainerId       = 0x04U,
    devQualPlatform          = 0x05U,
    devQualPowerDelivery     = 0x06U,
    devQualBatteryInfo       = 0x07U,
    devQualPDConsumer        = 0x08U,
    devQualPDProvider        = 0x09U,
    devQualSuperspeedPlus    = 0x0AU,
    devQualPTM               = 0x0BU,
    devQualWirelessUSBExt    = 0x0CU,
    devQualBillboard         = 0x0DU,
    devQualAuthentication    = 0x0EU,
    devQualBillboardEx       = 0x0FU,
    devQualConfigSummary     = 0x10U
} usbDevQualType;

/* USB Standard defined descriptors */
typedef struct COMPILER_PACKED
{
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint8_t bcdVerMinor, bcdVerMajor;
    uint8_t bDeviceClass, bDeviceSubClass;
    uint8_t bDeviceProtocol;
    uint8_t bMaxPacketSize;
    uint16_t idVendor, idProduct;
    uint8_t bcdDevice[2];
    uint8_t iManufacturer, iProduct, iSerialNumber, bNumConfigurations;
} usbDeviceDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint16_t wTotalLength;
    uint8_t bNumInterfaces;
    uint8_t bConfigurationValue;
    uint8_t iConfiguration;
    uint8_t bmAttributes, bMaxPower;
} usbConfigDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint8_t bInterfaceNumber, bAlternateSettings;
    uint8_t bNumEndpoints;
    uint8_t bInterfaceClass, bInterfaceSubClass, bInterfaceProtocol;
    uint8_t iInterface;
} usbInterfaceDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint8_t bEndpointAddress;
    uint8_t bmAttributes;
    uint16_t wMaxPacketSize;
    uint8_t bInterval;
} usbEndpointDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint8_t bcdVersionMinor, bcdVersionMajor;
    uint8_t bDeviceClass, bDeviceSubClass, bDeviceProtocol;
    uint8_t bMaxPacketSize;
    uint8_t bNumConfigurations, bReserved;
} usbDevQualDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint8_t bMaxBurst, bmAttributes;
    uint16_t wBytesPerInterval;
} usbSSECDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint16_t wReserved;
    uint32_t dwBytesPerInterval;
} usbSSIsoECDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint16_t wTotalLength;
    uint8_t bNumDeviceCaps, entries[];
} usbBOSDesc;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint8_t bDevCapabilityType, data[];
} usbDevCapDesc;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType, bDevCapabilityType, bReserved;
    uint32_t bmAttributes;
    uint16_t wFunctionalitySupport, wReserved;
    uint32_t bmSublinkSpeedAttr[];
} usbBOSSuperspeedPlusDesc;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType, bNbrPorts;
    uint16_t wHubCharacteristics;
    uint8_t bPwrOn2PwrGood, bHubContrCurrent, masks[];
} usb2HubDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType, bNbrPorts;
    uint16_t wHubCharacteristics;
    uint8_t bPwrOn2PwrGood, bHubContrCurrent, bHubHdrDecLat;
    uint16_t wHubDelay;
    uint16_t deviceRemovable;
} usb3HubDescriptor;

#endif

