#ifndef DAPI_THREAD_H_
#define DAPI_THREAD_H_

#include <compiler.h>

/* Channel flag subfield extraction macros */
#define DAPI_CHANNEL_EXTRACT_TYPE(x)               ((x)>>8)
#define DAPI_CHANNEL_EXTRACT_TRIGGER(x)            ((x) & 0xFFU)


/* Channel trigger behaviors */
#define DAPI_CHANNEL_TRIGGER_FULL                  0x0000U
#define DAPI_CHANNEL_TRIGGER_PUBLISH               0x0001U

/* Channel types */
typedef enum
{
    DAPI_CHANNEL_TYPE_SINGLEP_SINGLER = 0x01U,
    DAPI_CHANNEL_TYPE_SINGLEP_MULTIR  = 0x02U,
    DAPI_CHANNEL_TYPE_MULTIP_SINGLER  = 0x03U,
    DAPI_CHANNEL_TYPE_MULTIP_MULTIR   = 0x04U
} dapi_channel_type;


typedef struct COMPILER_PACKED
{
    void *internal_obj;
    uint32_t flags;
    uint16_t current_slot;
    dapi_channel_type type;
} dapi_channel_ref;


typedef uint32_t pid_t;

typedef void (*taskFunc)(void *);
typedef void (*callbackFunc)(void *, const uint64_t status);

typedef enum
{
    syncObjCallback,
    syncObjFuture,
    syncObjWait
} sync_object_type;

typedef struct
{
    sync_object_type obj_type;
    union
    {
        callbackFunc func;
        void *wait;
        void *future;
    };

    union
    {
        uint64_t flags;
        void *context;
        uint64_t max_wait;
    };
    volatile uint32_t sync_flags;
    volatile uint32_t attachments;
} sync_object;

pid_t dapi_spawn_task(taskFunc func, void *context);

int sync_attach(sync_object *sync);
void sync_detach(sync_object *sync);
int sync_create_future_obj(sync_object **object, const uint64_t default_value, const uint32_t max_wait);
int sync_create_signal_obj(sync_object **object, const uint64_t default_value, const uint32_t flags);
int sync_create_callback_obj(sync_object **object, void *context, callbackFunc cb);
int sync_destroy_signal_obj(sync_object *obj);

int sync_object_wait(sync_object *obj, uint32_t timeout);
int sync_object_signal(sync_object *obj, uint64_t value);
uint64_t sync_object_get_status(sync_object *obj);
void sync_object_reset(sync_object *obj);

void *sync_monitor_create();
int sync_monitor_acquire(void *monitor, const uint32_t timeout );
void sync_monitor_release(void *monitor);
void sync_monitor_destroy(void *monitor);

dapi_channel_ref *sync_channel_create(const uint32_t buffer_size, const uint32_t slot_count, const dapi_channel_type chan_type);
dapi_channel_ref *sync_channel_find(const char *name);
int sync_channel_publish(dapi_channel_ref *channel, void *msg, const uint32_t msg_size);
dapi_channel_ref *sync_channel_subscribe(dapi_channel_ref *channel, sync_object *cb_object, const uint32_t trigger_type);
uint32_t sync_channel_read(dapi_channel_ref *channel, void *target_buffer, const uint32_t buffer_size);

#endif // DAPI_THREAD_H_
