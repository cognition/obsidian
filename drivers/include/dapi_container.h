/* Wrapper API for kernel data structures
 *
 * Author: Joseph Kinzel */

#ifndef DAPI_CONTAINER_H_
#define DAPI_CONTAINER_H_

/* Key-value tree based map */
void *dapi_map_create();
void dapi_map_destroy(void *map);
void dapi_map_insert(void *map, const uint64_t key, void *value);
const void *dapi_map_get(void *map, const uint64_t key);
void *dapi_map_get_mut(void *map, const uint64_t key);

/* General hash table functions */
void *dapi_ht_create(const uint32_t init_size);
void dapi_ht_insert(void *ht, int64_t key, void *value);
void *dapi_ht_get_entry( void *ht, int64_t key);
void dapi_ht_destroy(void *ht);

/* General radix map functions */
void *dapi_radix_map_create(void);
void dapi_radix_map_destroy(void *rmap);
int dapi_radix_map_insert(void *rmap, const uint64_t key, void *value);
const void *dapi_radix_map_find(void *rmap, const uint64_t key);

/* Queue functions */
void *dapi_queue_create(const uint32_t datum_size);
void *dapi_queue_push(void *queue);
int dapi_queue_pop(void *queue, void *out_buffer);
void *dapi_queue_peek(void *queue);
int dapi_queue_empty(void *queue);
void dapi_queue_destroy(void *queue);

#endif // DAPI_CONTAINER_H_
