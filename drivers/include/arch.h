#ifndef DRIVER_ARCH_H_
#define DRIVER_ARCH_H_

#ifdef __x86_64
	#define ARCH_PAGE_SIZE             0x1000UL
	#define ARCH_PAGE_OFFSET            0xFFFUL
#else
	#error "Unknown CPU architecture!"
#endif

#endif

