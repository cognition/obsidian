/* Obsidian Driver API Reference Space header
 *
 *
 * Author: Joseph Kinzel */


#ifndef DAPI_REFSPACE_H_
#define DAPI_REFSPACE_H_

#include "dapi_types.h"

driverOperationResult refspace_channel_find(const char *channel_name, void **channel);

#endif // DAPI_REFSPACE_H_
