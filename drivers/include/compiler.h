#ifndef DAPI_COMPILER_H_
#define DAPI_COMPILER_H_

#ifndef _OBSIDIAN_KERNEL_BUILD_
#ifdef __GNUC__

#define COMPILER_MODINFO_SECTION __attribute__ ((section(".modinfo")))
#define COMPILER_PACKED __attribute__ ((packed))
#define COMPILER_UNUSED __attribute__ ((unused))
#define COMPILER_ARCH_ALIGN __attribute ((aligned(16)))

typedef __INT8_TYPE__   int8_t;
typedef __INT16_TYPE__  int16_t;
typedef __INT32_TYPE__  int32_t;
typedef __INT64_TYPE__  int64_t;
typedef __UINT8_TYPE__  uint8_t;
typedef __UINT16_TYPE__ uint16_t;
typedef __UINT32_TYPE__ uint32_t;
typedef __UINT64_TYPE__ uint64_t;
typedef __UINTPTR_TYPE__ uintptr_t;
typedef __INTPTR_TYPE__ intptr_t;
typedef __SIZE_TYPE__   size_t;

#define getLsb64(x)             (__builtin_ffsl(x) - 1)
#define getLsb32(x)             (__builtin_ffs(x) - 1)

#define getMsb64(x)             (63 - __builtin_clzl(x) )
#define getMsb32(x)             (31 - __builtin_clz(x) )

#define NULL                    0

#define atomic_inc_pre(x)           (__sync_add_and_fetch((x), 1))
#define atomic_dec_pre(x)           (__sync_sub_and_fetch((x), 1))

#define atomic_add(x, y)            __sync_fetch_and_add((x), (y))
#define atomic_sub(x, y)            __sync_fetch_and_sub((x), (y))

#define atomic_sub_pre(x, y)        (__sync_sub_and_fetch((x), (y)))

#define atomic_or(x, y)             (__sync_or_and_fetch((x), (y)))

#endif

#endif

#endif
