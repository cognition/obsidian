#ifndef DRIVER_API_H_
#define DRIVER_API_H_

#include "dapi_types.h"

#define PCI_ADDR_GET_FUNCTION(x)          (x & 0x7U)

typedef enum
{
	busRegisterApproved,
	busRegisterBlocked
} busRegisterResponse;

/* Function pointer types */
typedef int (*read64Func)(uint64_t *dest, void *context, const uint64_t address, const uint32_t offset);
typedef int (*read32Func)(uint32_t *dest, void *context, const uint64_t address, const uint32_t offset);
typedef int (*read16Func)(uint16_t *dest, void *context, const uint64_t address, const uint32_t offset);
typedef int (*read8Func)(uint8_t *dest, void *context, const uint64_t address, const uint32_t offset);
typedef int (*readPacketFunc)(void *dest, void *context, const uint64_t control, const uint32_t offset, const uint32_t *bufferSize);

typedef int (*write64Func)(void *context, const uint64_t address, const uint32_t offset, const uint64_t value);
typedef int (*write32Func)(void *context, const uint64_t address, const uint32_t offset, const uint32_t value);
typedef int (*write16Func)(void *context, const uint64_t address, const uint32_t offset, const uint16_t value);
typedef int (*write8Func)(void *context, const uint64_t address, const uint32_t offset, const uint8_t value);
typedef int (*writePacketFunc)(void *context, const uint64_t address, const uint32_t control, void *packet, const uint32_t packLength);

typedef int (*addressConfigFunc)(void *context, const uint64_t address, void *config_info, const uint32_t config_length, sync_object *sync);

typedef int (*queueTransactionFunc)(void *context, const uint64_t address, void *t_info, const uint32_t t_info_length, sync_object *sync);
typedef int (*runTransactionsFunc)(void *context, const uint64_t address, sync_object *sync);

typedef int (*interruptFunc)(void *context, const int index);

typedef busMsgResponse (*busMsgRecvFunc)(const uint64_t addr, void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType, const int64_t unique_key);
typedef busRegisterResponse (*busRegisterNotifyFunc)(const uint64_t baseAddress, const uint64_t spanLength, BUS_HANDLE range_handle, const int64_t unique_key, void **retContext);

/* Bus IDs */
#define BUS_ID_PCI                 1
#define BUS_ID_IO                  2
#define BUS_ID_MEMORY              3
#define BUS_ID_USB                 4
#define BUS_ID_SATA                5
#define BUS_ID_SCSI                6
#define BUS_ID_EC                  7
#define BUS_ID_SMBUS               8
#define BUS_ID_ETHERNET            9

/* Bus registration capabilities */
#define BUS_CAPS_READ_BYTE         0x1
#define BUS_CAPS_READ_WORD         0x2
#define BUS_CAPS_READ_DWORD        0x4
#define BUS_CAPS_READ_QWORD        0x8
#define BUS_CAPS_READ_PACKET       0x10
#define BUS_CAPS_READ_MQWORD       0x20
#define BUS_CAPS_READ_MDWORD       0x40
#define BUS_CAPS_READ_MWORD        0x80

#define BUS_CAPS_WRITE_BYTE        0x100
#define BUS_CAPS_WRITE_WORD        0x200
#define BUS_CAPS_WRITE_DWORD       0x400
#define BUS_CAPS_WRITE_QWORD       0x800
#define BUS_CAPS_WRITE_PACKET      0x1000
#define BUS_CAPS_WRITE_MQWORD      0x2000
#define BUS_CAPS_WRITE_MDWORD      0x4000
#define BUS_CAPS_WRITE_MWORD       0x8000

#define BUS_CAPS_TRANSACTIONS      0x10000
#define BUS_CAPS_CONFIG_ADDR       0x20000

typedef struct
{
	uint64_t address;
	int busId;
} busAddressDesc;

typedef struct
{
	uint64_t base, length;
	int busId, busSpecific;
} resourceEntry;

typedef struct
{
    int eventCount;
} multi_event_group;

/* Bus registration descriptor */
typedef struct
{
	int busId;
	uint32_t capabilities;
	uint64_t baseAddress, length;
	/* Read functions */
	read64Func readQWord;
	read32Func readDWord;
	read16Func readWord;
	read8Func readByte;
	readPacketFunc readPacket;
    /* Write functions */
	write64Func writeQWord;
	write32Func writeDWord;
	write16Func writeWord;
	write8Func writeByte;
	writePacketFunc writePacket;
	/* Required transaction functions */
	queueTransactionFunc queueTransaction;
	runTransactionsFunc runTransactions;
    /* Internal addressing related functions */
    addressConfigFunc updateConfig;

	void *contextPtr;
} busRegDescriptor;

/* Bus manager descriptor */
typedef struct
{
    int busId;
    uint32_t coord_only_mask;
    busMsgRecvFunc recv;
    busRegisterNotifyFunc notify;
} busManagerDesc;

typedef driverOperationResult (*initFunc)(DEVICE_HANDLE dev_spec, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet);
typedef driverOperationResult (*deinitFunc)(void *context);
typedef driverOperationResult (*enumerateFunc)(void *context, void *identifyFunc);

typedef struct COMPILER_PACKED
{
	initFunc init;
	deinitFunc deinit;
	enumerateFunc enumerate;
} modExportTable;

/* Memory management functions and flags */
#define ALLOC_USE32           0x01U      /* Allocate a region completely within 32-bit space */

void *malloc(size_t size);
int free(void *ptr);
void *calloc(size_t num_items, size_t item_size);
void memset(void *dest, const uint8_t value, size_t length);
void memcpy(void *dest, const void *src, size_t length);
uint64_t getPhysBase(uintptr_t addr);


int allocatePhysRegion(uintptr_t *vBase, uintptr_t *physBase, size_t contiguousSize, uintptr_t align, int flags, DEVICE_HANDLE dHandle);
int freePhysRegion(uintptr_t vBase, DEVICE_HANDLE dHandle);

/* PCI Bus functions */
int pciReadByte(uint8_t *dest, const uint32_t busAddress, const uint16_t offset);
int pciReadWord(uint16_t *dest, const uint32_t busAddress, const uint16_t offset);
int pciReadDWord(uint32_t *dest, const uint32_t busAddress, const uint16_t offset);
int pciReadQWord(uint64_t *dest, const uint32_t busAddress, const uint16_t offset);

int pciWriteByte(const uint32_t busAddress, const uint16_t offset, const uint8_t value);
int pciWriteWord(const uint32_t busAddress, const uint16_t offset, const uint16_t value);
int pciWriteDWord(const uint32_t busAddress, const uint16_t offset, const uint32_t value);
int pciWriteQWord(const uint32_t busAddress, const uint16_t offset, const uint64_t value);

int pciReadBar(uint64_t *base, uint64_t *length, int *type, const uint32_t address, const uint8_t barNum);
int pciEnableBusMaster(const uint32_t address);

/* IO Bus functions */
int ioBusReadByte(uint8_t *dest, const uint16_t port);
int ioBusReadWord(uint16_t *dest, const uint16_t port);
int ioBusReadDWord(uint32_t *dest, const uint16_t port);

int ioBusWriteByte(const uint16_t port, const uint8_t value);
int ioBusWriteWord(const uint16_t port, const uint16_t value);
int ioBusWriteDWord(const uint16_t port, const uint32_t value);

/* General bus functions */
int busQueueTransaction(const int bus_id, const uint64_t address, void *t_info, const uint32_t t_info_length, sync_object *sync, DEVICE_HANDLE caller_handle);
int busRunTransactions(const int bus_id, const uint64_t address, sync_object *sync, DEVICE_HANDLE caller_handle);

int busConfigAddress(const int bus_id, const uint64_t address, void *config_info, const uint32_t config_size, sync_object *sync, DEVICE_HANDLE caller_handle);

/* Coordinator and driver functions */
busMsgResponse busMsg(void *response, uint32_t *resSize, const int busId, uint64_t busAddress, void *msg, const uint32_t msgSize, const uint32_t msgType, DEVICE_HANDLE dev);
int registerBusCoordinator(const busManagerDesc *desc, DEVICE_HANDLE device);
int registerNewDevice(const busAddressDesc *dev_addr, DEVICE_HANDLE coord_dev, BUS_HANDLE parent_handle, const uint64_t id_key, BUS_HANDLE bus_spec, void *context );
int registerDriverDatabase(const char *dbase_path, const int bus_id, DEVICE_HANDLE coord_dev);
BUS_HANDLE coordAllocBusHandle(const int bus_id, DEVICE_HANDLE coord_dev);
void coordFreeBusHandle(const int bus_id, BUS_HANDLE handle, DEVICE_HANDLE coord_dev);
BUS_HANDLE coordRegisterMultifunction(const char *name, const char *ref_space_id, const uint64_t address, BUS_HANDLE parent_handle, DEVICE_HANDLE coord_dev, const int bus_id);

/* Memory mapped I/O functions */
int registerMMIORegion(uintptr_t *mmioBase, uintptr_t physBase, size_t physLength, DEVICE_HANDLE dHandle);
int deregisterMMIORegion(uintptr_t mmioBase, DEVICE_HANDLE dHandle);

/* Interrupts */
int registerInterruptHandler(interruptFunc func, void *context, int *requestedCount, DEVICE_HANDLE dHandle);
int deregisterInterruptHandler(DEVICE_HANDLE dHandle);

/* Registration and discovery functions */
int registerBusRange(busRegDescriptor *desc, BUS_HANDLE *regHandle, DEVICE_HANDLE dHandle);
int registerBusMultiplexer(const int busId, const uint64_t addr, BUS_HANDLE *regHandle, DEVICE_HANDLE dHandle);
int deregisterBusRange(BUS_HANDLE regHandle, DEVICE_HANDLE dHandle);
int getDeviceResources(DEVICE_HANDLE dHandle, resourceEntry **resources, int *resourceCount);

/* Utility function */
int printf(const char *fmt, ...);

/* Time/control */
void wait(int ms);

/* Spinlock functions */
void *dapi_lock_create();
void dapi_lock_destroy(void *lock);
void dapi_lock_acquire(void *lock);
void dapi_lock_release(void *lock);

#endif
