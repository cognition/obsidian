#ifndef DRIVER_API_USB_H_
#define DRIVER_API_USB_H_

#include <dapi_types.h>
#include <usb/usb_desc.h>

/* USB language ids */
#define USB_LANGID_ENGLISH_US                     0x0409U

/* USB device feature selectors */
#define USB_DEV_FS_REMOTE_WAKEUP                  1
#define USB_DEV_FS_TEST_MODE                      2
#define USB_DEV_FS_U1_ENABLE                      48
#define USB_DEV_FS_U2_ENABLE                      49
#define USB_DEV_FS_LTM_ENABLE                     50

/* USB endpoint feature selectors */
#define USB_EP_FS_HALT                            0


/* USB device speeds */
#define USB_SPEED_LOW                             0x1U
#define USB_SPEED_FULL                            0x2U
#define USB_SPEED_HIGH                            0x3U
#define USB_SPEED_SUPER                           0x4U
#define USB_SPEED_ESUPER_1X1                      0x5U
#define USB_SPEED_ESUPER_2X1                      0x6U
#define USB_SPEED_ESUPER_1X2                      0x7U
#define USB_SPEED_ESUPER_2X2                      0x8U

/* Bus protocol defined values */
#define USB_MAX_ENDPOINTS                         32
#define USB_MAX_DEVICE_COUNT                      128
#define USB_MAX_ADDRESS                           (USB_MAX_ENDPOINTS*USB_MAX_DEVICE_COUNT)

#define USB_GET_CONTROLLER_ID(x)                  ( (x)/USB_MAX_ADDRESS )
#define USB_GET_LOCAL_ADDRESS(x)                  ( ((x)/USB_MAX_ENDPOINTS)%USB_MAX_DEVICE_COUNT )
#define USB_ADDR_GET_ENDPOINT(x)                  (((x)>>1) & 0xFU)
#define USB_ADDR_GET_DIR(x)                       ((x) & 0x1U)
#define USB_ADDR_FULL_DEVICE(x)                   ((x) - (x)%USB_MAX_ENDPOINTS)
#define USB_MAKE_FULl_ADDR(cid, did, ep_id)       ( ((cid)*USB_MAX_ADDRESS) | ( (did)*USB_MAX_ENDPOINTS) | (ep_id))

#define USB_ADDR_ADD_EP_INFO(addr, ep, dir)       ( (addr) | ((ep)<<1) | ((dir) & 0x1))

#define USB_INIT_PACKET_SIZE                      8

#define USB_REQUEST_TYPE_GET(x)                   (((x)>>5) & 0x03U)
#define USB_REQUEST_RECIPIENT_GET(x)              ((x) & 0x1FU)

#define USB_BMRT_STANDARD                         0
#define USB_BMRT_CLASS                            1
#define USB_BMRT_VENDOR                           2
#define USB_BMRT_RESERVED                         3

/* Transaction flags */
#define USB_TRANSACTION_IN                        0x01U
#define USB_TRANSACTION_OUT                       0x00U

/* Request type codes values */
#define USB_REQ_TYPE_DEV_TO_HOST                  0x80U
#define USB_REQ_TYPE_HOST_TO_DEV                  0x00U
#define USB_REQ_TYPE_STANDARD                     0x00U
#define USB_REQ_TYPE_CLASS                        0x20U
#define USB_REQ_TYPE_VENDOR                       0x40U
#define USB_REQ_TYPE_RESERVED                     0x60U
#define USB_REQ_TYPE_DEVICE                       0x00U
#define USB_REQ_TYPE_INTERFACE                    0x01U
#define USB_REQ_TYPE_ENDPOINT                     0x02U
#define USB_REQ_TYPE_OTHER                        0x03U

/* Standard control request codes */
#define USB_CONTROL_REQ_GET_STATUS                0
#define USB_CONTROL_REQ_CLEAR_FEATURE             1
#define USB_CONTROL_REQ_SET_FEATURE               3
#define USB_CONTROL_REQ_SET_ADDRESS               5
#define USB_CONTROL_REQ_GET_DESCRIPTOR            6
#define USB_CONTROL_REQ_SET_DESCRIPTOR            7
#define USB_CONTROL_REQ_GET_CONFIG                8
#define USB_CONTROL_REQ_SET_CONFIG                9
#define USB_CONTROL_REQ_GET_INTERFACE             10
#define USB_CONTROL_REQ_SET_INTERFACE             11
#define USB_CONTROL_REQ_SYNCH_FRAME               12

/* Device status request values */
#define USB_DEV_STATUS_REQ_STANDARD               0x00U
#define USB_DEV_STATUS_REQ_PTM                    0x01U

/* Device status fields */
#define USB_DEV_STATUS_SELF_POWERED               0x0001U
#define USB_DEV_STATUS_REMOTE_WAKEUP              0x0002U
#define USB_DEV_STATUS_U1_ENABLE                  0x0004U
#define USB_DEV_STATUS_U2_ENABLE                  0x0008U
#define USB_DEV_STATUS_LTM_ENABLE                 0x0010U

/* USB Class codes */
#define USB_CLASS_VENDOR                          0xFFU
#define USB_CLASS_PROBE_IFACE                     0x00U
#define USB_CLASS_HUB                             0x09U

#define USB_DEV_FLAG_HUB                          0x04U
#define USB_DEV_FLAG_MTT                          0x02U

/* Driver transaction flags */
#define USB_TRANSACTION_SMALLER                   0x10U

/* Controller request flags */
#define USB_CONTROLLER_FLAG_CONFIG_REQUEST        0x10000U

/* USB New connection initialization flags */
#define USB_INIT_FLAGS_COMPOUND                   0x00100U

/* Device topography information field extraction */
#define USB_TOPO_DEPTH_OFFSET           20
#define USB_TOPO_INTERFACE_OFFSET       24

#define USB_TOPO_GET_DEPTH(x)           ( ((x)>>USB_TOPO_DEPTH_OFFSET) & 0xF)
#define USB_TOPO_GET_IFACE(X)           ( ((x)>>USB_TOPO_INTERFACE_OFFSET) & 0x1F)

/* USB Hardware codes */
typedef enum
{
    usb_op_success = 1,
    usb_op_blabber,
    usb_op_setup_stall,
    usb_op_status_stall
} usb_hw_op_result;

typedef enum
{
    usbTransactionBlabber = 0,
    usbTransactionTimeout,
    usbTransactionSuccess = 0x80U,
    usbTransactionShortPacket

} usbTransactionStatus;

typedef enum
{
	usbCoordControllerIdRequest = 1,
	usbCoordControllerDereg,
	usbCoordDeviceConnect,
	usbCoordDeviceDisconnect,
	usbCoordPortConnect,
	usbCoordPortDisconnect,
	usbCoordDeviceError,
	usbCoordDecodeEndpointDesc,
	usbCoordGetStatus,
	usbCoordGetDescriptor,
	usbCoordRequestConfig,
	usbCoordRequestAltInterface,
	usbCoordTransactionQueue,
	usbCoordTransactionRun,

	usbCoordDebugRequestStart = 0x800U,
	usbCoordDebugEndpoint = 0x800U
} usbCoordMsgType;

/* Requests an ID for a given USB controller, should response with busAffirmative if the request is accepted or busNegative if it is denied */
typedef struct COMPILER_PACKED
{
	uint8_t usb_major;
	uint8_t usb_minor;
	uint8_t usb_revision;
} controller_id_req_msg;

/* Id response */
typedef struct COMPILER_PACKED
{
	uint32_t controller_id;
	uint8_t coord_usb_major, coord_usb_minor;
	uint16_t version_flags;
} controller_id_response;

/* Controller device deregister message, should respond with busAcknowledge or busNoAck depending on the validity of the request.  No response message */
typedef struct COMPILER_PACKED
{
	uint32_t reason_code, controller_id;
} controller_dereg_msg;

typedef struct COMPILER_PACKED
{
    BUS_HANDLE parent_handle;
    uint32_t controller_id, max_bus_power;
    uint8_t port_num, parent_address, dev_speed;
    sync_object *connect_wait;
} usb_port_connect_msg;

typedef struct COMPILER_PACKED
{
    sync_object *connect_wait;
    uint8_t port_num, parent_address, dev_speed;
} controller_port_connect_msg;

#define USB_NEW_CONNECT_ADDRESSED             0x0001U

/* USB device connected message, can originate from a controller or a hub. */
typedef struct COMPILER_PACKED
{
    BUS_HANDLE parent_handle;
    uint32_t controller_id, max_bus_power;
	uint8_t port_num, usb_address, parent_address, dev_speed;
	uint16_t flags;
} device_connect_msg;

/* USB device connected response */
typedef struct COMPILER_PACKED
{
} device_connect_response;

/* USB Device disconnect message, can originate from a controller or a hub. */
typedef struct COMPILER_PACKED
{
	uint32_t session_id;
	uint8_t port_num;
} device_disconnect_msg;

/* USB Device disconnect response */
typedef struct COMPILER_PACKED
{
} device_disconnect_response;

/* USB Device error message, should respond with busAcknowledge or busNoAck */
typedef struct COMPILER_PACKED
{
	uint8_t device_id, reason, action_proposal, error_count;
	uint32_t session_id;
} device_error_msg;


typedef struct COMPILER_PACKED
{
    volatile void *data_ptr;
    uint16_t w_value, w_index, w_length;
    uint8_t b_request;
} device_control_transaction_request;

typedef struct COMPILER_PACKED
{
    uint8_t descriptor_req_type, descriptor_value, descriptor_index;
} device_descriptor_request;

typedef struct COMPILER_PACKED
{
    uint16_t descriptor_size;
    void *descriptor;
} device_descriptor_response;

typedef union
{
	controller_id_response id_response;
	device_connect_response connect_response;
	device_disconnect_response disconnect_response;

} usbCoordResponse;

typedef union
{
	controller_id_req_msg controller_id_req;
	controller_dereg_msg controller_dereg;
	device_connect_msg dev_connect;
	device_disconnect_msg dev_disconnect;
	device_error_msg dev_error;
} usbCoordMsg;

/* Transactions */
typedef enum
{
    usbTransactionControl = USB_EP_TYPE_CONTROL,
    usbTransactionIsochronous = USB_EP_TYPE_ISOCHRON,
    usbTransactionBulk = USB_EP_TYPE_BULK,
    usbTransactionInterrupt = USB_EP_TYPE_INTERRUPT
} usbTransactionType;

typedef struct
{
    uint8_t bm_request_type, b_request;
    uint16_t w_value, w_index, w_length;
    union
    {
        volatile void *data_ptr;
        uint64_t data_addr;
        uint8_t *data_buffer;
    };
} usbControlTransaction;

typedef struct
{
    union
    {
        void *data;
        uintptr_t data_addr;
    };
    uint32_t length;
    uint16_t frame_id;
    uint16_t flags;
} usbIsoTransaction;

typedef struct
{
    union
    {
        uint8_t *buffer;
        volatile void *data;
        uintptr_t buffer_addr;
    };
    uint32_t length;
} usbTransferTransaction;

typedef struct
{
    usbTransactionType type;
    uint32_t flags;
    union
    {
        usbControlTransaction control;
        usbIsoTransaction isoch;
        usbTransferTransaction transfer;
    };
} usbControllerTransaction;

typedef struct
{
    sync_object *sync;
    BUS_HANDLE requestor;
    usbTransactionType type;
    uint32_t flags;
    uint8_t endpoint, direction;
    union
    {
        usbControlTransaction control;
        usbIsoTransaction isoch;
        usbTransferTransaction transfer;
    };
} usbDevTransaction;

typedef struct
{
    BUS_HANDLE requestor;
    uint8_t endpoint, direction;
} usbDevRunTransactionsRequest;

/* Device and endpoint configuration */
typedef enum
{
    usbConfigureEndpoint = 1,
    usbConfigureEndpointUpdate,
    usbConfigureDevice,
    usbAddressDevice,
    usbPrepareDevice,
    /* Debugging commands */
    usbConfigDebugCommandStart = 0x800U,
    usbConfigDebugStopEndpoint = 0x800U
} usbConfigureType;

typedef enum
{
    usbEndpointInvalid = 0,
    usbEndpointIsochOut = 1,
    usbEndpointBulkOut = 2,
    usbEndpointInterruptOut = 3,
    usbEndpointControl = 4,
    usbEndpointIsochIn = 5,
    usbEndpointBulkIn = 6,
    usbEndpointInterruptIn = 7
} usbEndpointType;

typedef struct COMPILER_PACKED
{
    uint8_t ep_num, ep_type, at_ops, interval, direction;
    uint16_t packet_size, ess_wbpi;
    uint8_t ess_max_burst, ess_attr, ess_iso_mult;
    uint32_t ess_iso_dwbpi;
} usbConfigureEndpointFields;

typedef struct COMPILER_PACKED
{
    uint8_t hub_flags;
    uint8_t num_ports;
    uint8_t tt;
} usbHubInfo;

typedef struct COMPILER_PACKED
{
    usbConfigureType command_type;
    union
    {
        struct
        {
            usbConfigureEndpointFields endpoints[USB_MAX_ENDPOINTS];
            uint8_t dev_speed, num_ep_entries, value, interface, alt_setting;
            usbHubInfo hub_info;
        } device_info;

        struct
        {
            uint8_t parent_addr, port_num, dev_speed;
        } connection_info;
    };
} usbConfigureAddress;

typedef struct COMPILER_PACKED
{
    BUS_HANDLE requestor;
    usbConfigureEndpointFields *ep_fields;
    const uint8_t *ep_desc;
    int max_desc_bytes;
} usb_decode_ep_request;

typedef struct COMPILER_PACKED
{
    int desc_bytes_traversed;
} usb_decode_ep_response;

typedef struct COMPILER_PACKED
{
    BUS_HANDLE requestor;
    uint16_t type_index;
    uint8_t status_type;
    sync_object *sync;
} usb_get_status_request;

typedef struct COMPILER_PACKED
{
    uint32_t length, flags, bus_power_available, topo_string;
    uint8_t dev_speed, desc_buffer[];
} usb_device_entry_context;

typedef struct COMPILER_PACKED
{
    BUS_HANDLE requestor;
    sync_object *sync;
    uint16_t expected_size;
    uint8_t desc_type, desc_index, request_type;
} usb_device_get_descriptor;

typedef struct COMPILER_PACKED
{
    BUS_HANDLE requestor;
    int descriptor_index;
} usb_request_config_msg;

typedef struct COMPILER_PACKED
{
    BUS_HANDLE requestor;
    uint32_t interface_mask;
    uint8_t interface_indices[32];
} usb_request_alt_interfaces;

typedef struct COMPILER_PACKED
{
    sync_object *sync;
    BUS_HANDLE requestor;
    uint32_t flags;
} usb_request_debug_ep_stop;

typedef struct COMPILER_PACKED
{
    sync_object *sync;
    BUS_HANDLE parent;

    usbControlTransaction port_enable_transaction;
    uint32_t flags;
    uint8_t port;
} usb_coord_hub_connect_request;


#endif

