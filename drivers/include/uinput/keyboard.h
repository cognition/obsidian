/* User input keyboard codes
 *
 *  Author: Joseph Kinzel */

#ifndef UINPUT_KEYBOARD_H_
#define UINPUT_KEYBOARD_H_

typedef enum
{
    /* Function/control keys */
    uinput_kb_escape_pressed = 1,
    uinput_kb_f1_pressed,
    uinput_kb_f2_pressed,
    uinput_kb_f3_pressed,
    uinput_kb_f4_pressed,
    uinput_kb_f5_pressed,
    uinput_kb_f6_pressed,
    uinput_kb_f7_pressed,
    uinput_kb_f8_pressed,
    uinput_kb_f9_pressed,
    uinput_kb_f10_pressed,
    uinput_kb_f11_pressed,
    uinput_kb_f12_pressed,

    uinput_kb_print_screen_pressed,
    uinput_kb_pause_pressed,

    /* Traversal keys */
    uinput_kb_insert_pressed,
    uinput_kb_home_pressed,
    uinput_kb_delete_pressed,
    uinput_kb_end_pressed,

    uinput_kb_page_up_pressed,
    uinput_kb_page_down_pressed,

    uinput_kb_space_pressed,
    uinput_kb_tab_pressed,
    uinput_kb_return_pressed,
    uinput_kb_backspace_pressed,

    uinput_kb_up_pressed,
    uinput_kb_down_pressed,
    uinput_kb_left_pressed,
    uinput_kb_right_pressed,

    /* Modifier keys */
    uinput_kb_left_shift_pressed,
    uinput_kb_right_shift_pressed,
    uinput_kb_left_alt_pressed,
    uinput_kb_right_alt_pressed,
    uinput_kb_left_ctrl_pressed,
    uinput_kb_right_ctrl_pressed,

    /* Specialized function keys */
    uinput_kb_left_win_pressed,
    uinput_kb_right_win_pressed,

    uinput_kb_menu_pressed,
    /* Lock keys */
    uinput_kb_caps_lock_pressed,
    uinput_kb_scroll_lock_pressed,
    uinput_kb_num_lock_pressed,

    /* Media keys */
    uinput_kb_play_pause_pressed,
    uinput_kb_stop_pressed,
    uinput_kb_prev_pressed,
    uinput_kb_next_pressed,
    uinput_kb_volume_up_pressed,
    uinput_kb_volume_down_pressed,
    uinput_kb_mute_pressed,

    /* Primary alphanumeric and symbol keys */
    uinput_kb_tilde_pressed,
    uinput_kb_one_pressed,
    uinput_kb_two_pressed,
    uinput_kb_three_pressed,
    uinput_kb_four_pressed,
    uinput_kb_five_pressed,
    uinput_kb_six_pressed,
    uinput_kb_seven_pressed,
    uinput_kb_eight_pressed,
    uinput_kb_nine_pressed,
    uinput_kb_zero_pressed,
    uinput_kb_dash_pressed,
    uinput_kb_plus_pressed,

    uinput_kb_a_pressed,
    uinput_kb_b_pressed,
    uinput_kb_c_pressed,
    uinput_kb_d_pressed,
    uinput_kb_e_pressed,
    uinput_kb_f_pressed,
    uinput_kb_g_pressed,
    uinput_kb_h_pressed,
    uinput_kb_i_pressed,
    uinput_kb_j_pressed,
    uinput_kb_k_pressed,
    uinput_kb_l_pressed,
    uinput_kb_m_pressed,
    uinput_kb_n_pressed,
    uinput_kb_o_pressed,
    uinput_kb_p_pressed,
    uinput_kb_q_pressed,
    uinput_kb_r_pressed,
    uinput_kb_s_pressed,
    uinput_kb_t_pressed,
    uinput_kb_u_pressed,
    uinput_kb_v_pressed,
    uinput_kb_w_pressed,
    uinput_kb_x_pressed,
    uinput_kb_y_pressed,
    uinput_kb_z_pressed,

    uinput_kb_left_bracket_pressed,
    uinput_kb_right_bracket_pressed,
    uinput_kb_semicolon_pressed,
    uinput_kb_quote_pressed,
    uinput_kb_comma_pressed,
    uinput_kb_period_pressed,
    uinput_kb_backslash_pressed,
    uinput_kb_slash_pressed,

    /* Numpad keys */
    uinput_kb_numpad_backslash_pressed,
    uinput_kb_numpad_asterisk_pressed,
    uinput_kb_numpad_dash_pressed,
    uinput_kb_numpad_plus_pressed,
    uinput_kb_numpad_enter_pressed,
    uinput_kb_numpad_period_pressed,
    uinput_kb_numpad_comma_pressed,

    uinput_kb_numpad_one_pressed,
    uinput_kb_numpad_two_pressed,
    uinput_kb_numpad_three_pressed,
    uinput_kb_numpad_four_pressed,
    uinput_kb_numpad_five_pressed,
    uinput_kb_numpad_six_pressed,
    uinput_kb_numpad_seven_pressed,
    uinput_kb_numpad_eight_pressed,
    uinput_kb_numpad_nine_pressed,
    uinput_kb_numpad_zero_pressed,

    /* Function/control keys */
    uinput_kb_escape_released,
    uinput_kb_f1_released,
    uinput_kb_f2_released,
    uinput_kb_f3_released,
    uinput_kb_f4_released,
    uinput_kb_f5_released,
    uinput_kb_f6_released,
    uinput_kb_f7_released,
    uinput_kb_f8_released,
    uinput_kb_f9_released,
    uinput_kb_f10_released,
    uinput_kb_f11_released,
    uinput_kb_f12_released,

    uinput_kb_print_screen_released,
    uinput_kb_pause_released,

    /* Traversal keys */
    uinput_kb_insert_released,
    uinput_kb_home_released,
    uinput_kb_delete_released,
    uinput_kb_end_released,

    uinput_kb_page_up_released,
    uinput_kb_page_down_released,

    uinput_kb_tab_released,
    uinput_kb_return_released,
    uinput_kb_backspace_released,

    uinput_kb_up_released,
    uinput_kb_down_released,
    uinput_kb_left_released,
    uinput_kb_right_released,

    /* Modifier keys */
    uinput_kb_left_shift_released,
    uinput_kb_right_shift_released,
    uinput_kb_left_alt_released,
    uinput_kb_right_alt_released,
    uinput_kb_left_ctrl_released,
    uinput_kb_right_ctrl_released,

    /* Specialized function keys */
    uinput_kb_left_win_released,
    uinput_kb_right_win_released,

    uinput_kb_menu_released,
    /* Lock keys */
    uinput_kb_caps_lock_released,
    uinput_kb_scroll_lock_released,
    uinput_kb_num_lock_released,

    /* Media keys */
    uinput_kb_play_pause_released,
    uinput_kb_stop_released,
    uinput_kb_prev_released,
    uinput_kb_next_released,
    uinput_kb_volume_up_released,
    uinput_kb_volume_down_released,
    uinput_kb_mute_released,

    /* Primary alphanumeric and symbol keys */
    uinput_kb_tilde_released,
    uinput_kb_one_released,
    uinput_kb_two_released,
    uinput_kb_three_released,
    uinput_kb_four_released,
    uinput_kb_five_released,
    uinput_kb_six_released,
    uinput_kb_seven_released,
    uinput_kb_eight_released,
    uinput_kb_nine_released,
    uinput_kb_zero_released,
    uinput_kb_dash_released,
    uinput_kb_plus_released,

    uinput_kb_a_released,
    uinput_kb_b_released,
    uinput_kb_c_released,
    uinput_kb_d_released,
    uinput_kb_e_released,
    uinput_kb_f_released,
    uinput_kb_g_released,
    uinput_kb_h_released,
    uinput_kb_i_released,
    uinput_kb_j_released,
    uinput_kb_k_released,
    uinput_kb_l_released,
    uinput_kb_m_released,
    uinput_kb_n_released,
    uinput_kb_o_released,
    uinput_kb_p_released,
    uinput_kb_q_released,
    uinput_kb_r_released,
    uinput_kb_s_released,
    uinput_kb_t_released,
    uinput_kb_u_released,
    uinput_kb_v_released,
    uinput_kb_w_released,
    uinput_kb_x_released,
    uinput_kb_y_released,
    uinput_kb_z_released,

    uinput_kb_left_bracket_released,
    uinput_kb_right_bracket_released,
    uinput_kb_semicolon_released,
    uinput_kb_quote_released,
    uinput_kb_comma_released,
    uinput_kb_period_released,
    uinput_kb_backslash_released,
    uinput_kb_slash_released,

    /* Numpad keys */
    uinput_kb_numpad_backslash_released,
    uinput_kb_numpad_asterisk_released,
    uinput_kb_numpad_dash_released,
    uinput_kb_numpad_plus_released,
    uinput_kb_numpad_enter_released,
    uinput_kb_numpad_period_released,

    uinput_kb_numpad_one_released,
    uinput_kb_numpad_two_released,
    uinput_kb_numpad_three_released,
    uinput_kb_numpad_four_released,
    uinput_kb_numpad_five_released,
    uinput_kb_numpad_six_released,
    uinput_kb_numpad_seven_released,
    uinput_kb_numpad_eight_released,
    uinput_kb_numpad_nine_released,
    uinput_kb_numpad_zero_released

} uinput_kb_values;

#endif // UINPUT_KEYBOARD_H_

