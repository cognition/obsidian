/* User input bus messages
 *
 * Author: Joseph Kinzel */

#ifndef UINPUT_MSG_H_
#define UINPUT_MSG_H_

typedef enum
{
    uinput_register_producer,
    uinput_register_consumer,

    uinput_create_pipe_val,
    uinput_destroy_pipe_val,
    uinput_listen_pipe_val,
    uinput_query_pipe_val
} uinput_control_msg_value;

/* Register producer message and response */
typedef struct COMPILER_PACKED
{
    uint64_t bus_addr;
    uint8_t bus_id;
} uinput_register_producer_msg;

typedef struct COMPILER_PACKED
{
    BUS_HANDLE producer_handle;
} uinput_register_producer_response;

/* Create pipe message and response */
typedef struct COMPILER_PACKED
{
    BUS_HANDLE producer_handle
    uint8_t pipe_type;
} uinput_create_pipe_msg;

typedef struct COMPILER_PACKED
{
    uint64_t pipe_addr;
} uinput_create_pipe_response;



#endif // UINPUT_MSG_H_
