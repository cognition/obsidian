/* Common types used in the driver API
 *
 * Author: Joseph Kinzel */

#ifndef DAPI_TYPES_H_
#define DAPI_TYPES_H_

#include "compiler.h"
#include "sync.h"

#define NULL                              0

#define DRIVER_OP_ERROR(x)        ((x) & 0xFF)

/* API specific types */
typedef uint64_t BUS_HANDLE;
typedef int64_t DEVICE_HANDLE;

typedef enum
{
	driverOpSuccess = 0x100,
	driverOpNotFound = 0x200,
	driverOpBusError = 0x01,
	driverOpNoMemory,
	driverOpResources,
	driverOpVersionMismatch,
	driverOpNoAccess,
	driverOpHardwareError,
	driverOpBadArg,
	driverOpBadState,
	driverOpSyncError
} driverOperationResult;

typedef enum
{
	busOpSuccess        = 1,
	busOpAddressInvalid = 2,
	busOpOffsetInvalid  = 3,
	busOpNotSupported   = 4,
	busOpInvalidArg     = 5,
	busOpIOError        = 6,
	busOpNoAccess       = 7,
	busOpMorePackets    = 8,
	busOpResources      = 9,
	busOpSyncError      = 10
} busOperationResult;

typedef enum
{
	busMsgAcknowledged = 1,
	busMsgNoAck        = 2,
	busMsgWait         = 3,
	busMsgAffirmative  = 4,
	busMsgNegative     = 5,
	busMsgBadFormat    = 6,
	busMsgNotSupported = 7,
	busMsgNoSpace      = 8
} busMsgResponse;

typedef enum
{
	interruptHandled = 0,
	interruptNotMe
} interruptResult;


#endif // DAPI_TYPES_H_
