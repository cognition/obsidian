#ifndef EHCI_QUEUE_H_
#define EHCI_QUEUE_H_

#include <compiler.h>

/* Most of these flags, fields and values are defined in Chapter 3 of the
   ECHI Specification */

/* Frame link field values */
#define EHCI_FRAME_TERMINAL                          0x0001U
#define EHCI_FRAME_TYPE_ITD                          0x0000U
#define EHCI_FRAME_TYPE_QH                           0x0002U
#define EHCI_FRAME_TYPE_SITD                         0x0004U
#define EHCI_FRAME_TYPE_FSTN                         0x0006U

/* iTD Buffer status and offset flags and macros */
#define EHCI_ITD_STATUS_ACTIVE                       0x80000000U
#define EHCI_ITD_STATUS_BUFFER_ERROR                 0x40000000U
#define EHCI_ITD_STATUS_BABBLE                       0x20000000U
#define EHCI_ITD_STATUS_TRANSACTION_ERROR            0x10000000U

#define EHCI_ITD_TRANSACTION_LENGTH(x)               ((x & 0xFFF) << 16)

#define EHCI_ITD_IOC                                 0x00008000U

#define EHCI_ITD_PAGE_SELECT(x)                      (x<<12)
#define EHCI_BUFFER_OFFSET(x)                        (x)

/* iTD Buffer page universal macros */
#define EHCI_ITD_BUFFER_POINTER(x)                   (x & 0xFFFFF000U)

/* iTD Buffer page 0 macros */
#define EHCI_ITD_ENDPOINT_NUM(x)                     (x<<8)
#define EHCI_ITD_DEVICE_ADDR(x)                      (x)

/* iTD Buffer page 1 macros and flags */
#define EHCI_ITD_DIRECTION_OUT                       0
#define EHCI_ITD_DIRECTION_IN                        0x00000800U

#define EHCI_ITD_MAX_PACKET_SIZE(x)                  (x)

/* iTD Buffer page 2 macros */
#define EHCI_ITD_MULTI_NUM                           (x)

/* iTD 32-bit structure */
typedef struct COMPILER_PACKED
{
  uint32_t nextLink;
  uint32_t transactions[8];
  uint32_t bufferPointers[8];
} itd32;

/* siTD Endpoint status and control flags and macros */
#define EHCI_SITD_OUT                                0
#define EHCI_SITD_IN                                 0x80U

/* siTD Transfer state flags */
#define EHCI_SITD_IOC                                0x80U
#define EHCI_SITD_PAGE_SELECT                        0x40U

/* siTD buffer Page 1 fields and macros */
#define EHCI_SITD_TRANSACTION_COUNT(x)               (x)
#define EHCI_SITD_TRANSACTION_ALL                    0x0000U
#define EHCI_SITD_TRANSACTION_BEGIN                  0x0080U
#define EHCI_SITD_TRANSACTION_MID                    0x0100U
#define EHCI_SITD_TRANSACTION_END                    0x0180U

/* siTD 32-bit structure */
typedef struct COMPILER_PACKED
{
  uint32_t nextLink;
  /* Endpoint status and control fields */
  uint8_t  deviceAddress, endPointerNum, hubAddress, portNumber;
  /* uFrame schedule control fields */
  uint8_t splitStartMask, splitCompleteMask;
  uint16_t _reserved;
  /* Transfer state */
  uint8_t status, uFrameProgress;
  uint16_t transferInfo;

  uint32_t bufferPointers[2];
  uint32_t backPointer;
} sitd32;

/* qTD Status flags */
#define EHCI_QTD_PID_OUT                             0
#define EHCI_QTD_PID_IN                              0x100U
#define EHCI_QTD_PID_SETUP                           0x200U

#define EHCI_QTD_GET_ERROR_COUNTER(x)                (x>>10)
#define EHCI_QTD_CURRENT_PAGE(x)                     (x << 12)
#define EHCI_QTD_IOC                                 0x8000U

/* qTD Transfer size flags */
#define EHCI_QTD_DATA_TOGGLE                         0x8000U

typedef struct COMPILER_PACKED
{
  uint32_t nextQtd;
  uint32_t alternateNextQtd;
  uint16_t status;
  uint16_t transferSize;
  uint32_t buffers[5];
} qtd32;

/* Queue Head Endpoint characteristics - Device */
#define EHCI_QH_INACTIVE                             0x80U

/* Queue Head Endpoint characteristics - Endpoint */
#define EHCI_QH_EPS_FULL                             0x00U
#define EHCI_QH_EPS_LOW                              0x10U
#define EHCI_QH_EPS_HIGH                             0x20U

#define EHCI_QH_DATA_TOGGLE_CONTROL                  0x40U
#define EHCI_QH_RECLAMATION_LIST                     0x80U

/* Queue Head Endpoint characteristics - Maximum Packet Length */
#define EHCI_QH_CONTROL_ENDPOINT                     0x80U
#define EHCI_QH_NAK_RELOAD(x)                        (x<<12)

/* 32-bit Queue head structure */
typedef struct COMPILER_PACKED
{
  uint32_t horizontalLinkPtr;
  /* Endpoint capabitiles/characteristics */
  uint8_t deviceAddress, endPoint;
  uint16_t maximumPacketLength;
  /* Transfer overlay - QTD Pointers */
  uint32_t currentQTD, nextQtd, alternateNextQtd;
  /* Transfer overlay - Information (Table 3-22) */
  uint8_t status, iocCounters;
  uint16_t totalBytes;
  /* Buffer pointers */
  uint32_t bufferPointers[5];
} qh32;

/* 32-bit Frame Span Traversal Node */
typedef struct COMPILER_PACKED
{
  uint32_t normalPointer, backPointer;
} fstn32;

#endif
