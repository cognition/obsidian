#ifndef FRAME_ALLOC_H_
#define FRAME_ALLOC_H_

#include <compiler.h>

/* Error values */
#define FALLOC_INVALID                            1
#define FALLOC_CORRUPTED                          2
/* Configuation parameters */
#define EHCI_BLOCK_BINS                           3

typedef struct COMPILER_PACKED _frameBlock
{
  struct _frameBlock *prev;
  uintptr_t nextLink;
  uint64_t bitmaps[2];
} frameBlock;

typedef struct COMPILER_PACKED
{
  frameBlock *blocks[EHCI_BLOCK_BINS];
  uintptr_t base;
  uint32_t align;
  frameBlock *freeBlocks;
} frameAllocatorState;

void *allocateFrame(uint16_t frameSize, frameAllocatorState *state);
int freeFrame(void *ptr, frameAllocatorState *state);
frameAllocatorState *initFrameAllocator(const uintptr_t regionBase, const uintptr_t regionSize);

#endif
