#include <driverapi.h>

#define EHCI_SBRN_OFFSET               0x60U

/* Capability register offsets */
#define EHCI_CAPLENGTH_OFFSET          0x00U
#define EHCI_HCIVERSION_OFFSET         0x02U
#define EHCI_HCSPARAMS_OFFSET          0x04U
#define EHCI_HCCPARAMS_OFFSET          0x08U
#define EHCI_HCSP_PORTROUTE_OFFSET     0x0CU

/* Host controller capability parameters register flags */
#define EHCI_HCC_ADDR64                0x0001U
#define EHCI_HCC_PROG_FRAME_LIST       0x0002U
#define EHCI_HCC_ASYNC_PARK            0x0004U

/* Host controller Structural parameters register flags */

/* Command registers offsets, added to the value of the CAPLENGTH register per EHCI spec. */
#define EHCI_OP_USBCMD_OFF             0x00U
#define EHCI_OP_USBSTS_OFF             0x04U
#define EHCI_OP_USBINTR_OFF            0x08U
#define EHCI_OP_FRINDEX_OFF            0x0CU
#define EHCI_OP_CTRLDSSEGMENT_OFF      0x10U
#define EHCI_OP_PERIODICLISTBASE_OFF   0x14U
#define EHCI_OP_ASYNCLISTADDR_OFF      0x18U
#define EHCI_OP_CONFIGFLAGS_OFF        0x40U
#define EHCI_OP_PORTSC_OFF             0x44U

/* Port status flags */
#define EHCI_PSC_CURRENT_CONNECT       0x00000001U
#define EHCI_PSC_CONNECT_CHANGE        0x00000002U
#define EHCI_PSC_PORT_ENABLE           0x00000004U
#define EHCI_PSC_ENABLE_CHANGE         0x00000008U
#define EHCI_PSC_OC_ACTIVE             0x00000010U
#define EHCI_PSC_OC_CHANGE             0x00000020U
#define EHCI_PSC_FORCE_RESUME          0x00000040U
#define EHCI_PSC_SUSPEND               0x00000080U
#define EHCI_PSC_PORT_RESET            0x00000100U
#define EHCI_PSC_PORT_POWER            0x00001000U
#define EHCI_PSC_PORT_OWNER            0x00002000U
#define EHCI_PSC_WAKE_CONNECT          0x00100000U
#define EHCI_PSC_WAKE_DISCONNECT       0x00200000U
#define EHCI_PSC_WAKE_OVERCURRENT      0x00400000U

/* Port status and control getters and setters */
#define EHCI_PSC_GET_LINE_STATUS(x)    ((x>>10) & 0x3)
#define EHCI_PSC_GET_INDICATOR(x)      ((x>>14) & 0x3)
#define EHCI_PSC_GET_TEST(x)           ((x>>16) & 0xF)
#define EHCI_PSC_SET_TEST(x, y)        ((y << 16) | x)

/* Usb command register flags */
#define EHCI_COMMAND_RUNSTOP           0x0001U
#define EHCI_COMMAND_HC_RESET          0x0002U
#define EHCI_COMMAND_PERIODIC_ENABLE   0x0010U
#define EHCI_COMMAND_ASYNC_ENABLE      0x0020U
#define EHCI_COMMAND_INTERRUPT_ASYNC   0x0040U
#define EHCI_COMMAND_LIGHT_RESET       0x0080U
#define EHCI_COMMAND_ASYNC_PARK_EN     0x0800U

/* USB status register flags */
#define EHCI_STATUS_IOC                0x00000001U
#define EHCI_STATUS_ERROR_INT          0x00000002U
#define EHCI_STATUS_PORT_CHANGE        0x00000004U
#define EHCI_STATUS_FRAME_ROLLOVER     0x00000008U
#define EHCI_STATUS_HOST_ERROR         0x00000010U
#define EHCI_STATUS_ASYNC_ADVANCE_INT  0x00000020U
#define EHCI_STATUS_HC_HALTED          0x00001000U
#define EHCI_STATUS_RECLAMATION        0x00002000U
#define EHCI_STATUS_PERIODIC           0x00004000U
#define EHCI_STATUS_ASYNC              0x00008000U

#define EHCI_STATUS_ANY_INT            0x0000003FU

/* Usb configuration register flags */
#define EHCI_CONFIG_CF                 0x0001U

/* Expected values */
#define USB_CONTROLLER_VERSION         0x20U

/* Legacy emulation support values */
#define EHCI_LEGACY_ID                 0x000000001U
#define EHCI_LEGACY_OS_SEMAPHORE       0x001000000U
#define EHCI_LEGACY_BIOS_SEMAPHORE     0x000100000U

/* Device structure flags */
#define EHCI_USE64                     0x0001U

driverOperationResult controller_init(DEVICE_HANDLE dHandle, BUS_HANDLE bus_spec, busAddressDesc *addr, void *input_context, void **context);
driverOperationResult controller_deinit(void *context);
driverOperationResult controller_enum(void *context, void *identifyFunc);

typedef struct COMPILER_PACKED
{
	volatile uint16_t capLength, hciVer;
	volatile uint32_t hcsParams, hccParams, hssParams;
	volatile uint64_t portRoute[];
} ehciCapRegs;

typedef struct COMPILER_PACKED
{
	volatile uint32_t usbCommand, usbStatus, usbIntr, frameIndex,
		segmentSelect, perdiocListBase, asyncListAddr;
	uint32_t __reserved[9];
	volatile uint32_t configFlags, portSc[];
} ehciOpRegs;

typedef struct
{
	ehciCapRegs *caps;
	ehciOpRegs *ops;
	uintptr_t commandVirtBase, commandPhysBase;
	DEVICE_HANDLE devHandle;
	uint16_t flags, ownedPorts, connected;
	uint8_t numPorts;
}  ehciDevice;

COMPILER_MODINFO_SECTION const modExportTable moduleInfoTable = {&controller_init, &controller_deinit, &controller_enum};

static void queryPort(uint8_t portNum, ehciDevice *dev)
{
	uint16_t portMask = 1;
	portMask <<= portNum;
	/* Verify that a device is currently connected */
	if(dev->ops->portSc[portNum] & EHCI_PSC_CURRENT_CONNECT)
	{
		dev->connected |= portMask;
		/* Port reset timing defined in the USB2.0 spec (10.2.8.1) */
		dev->ops->portSc[portNum] |= EHCI_PSC_PORT_RESET;
		wait(50);
		dev->ops->portSc[portNum] &= ~EHCI_PSC_PORT_RESET;
		/* Reset state transfer defined in the EHCI spec (2.3.9) */
		wait(2);
		if( !(dev->ops->portSc[portNum] & EHCI_PSC_PORT_ENABLE ))
		{
			printf("EHCI - Found non-highspeed device on port %u\n", portNum);
			/* Hand over control of the port if a non-highspeed device is connected */
			dev->ops->portSc[portNum] |= EHCI_PSC_PORT_OWNER;
			return;
		}
		else
		{
			printf("EHCI - Found highspeed device on port %u\n", portNum);
		}
	}

	dev->ops->portSc[portNum] |= EHCI_PSC_CONNECT_CHANGE;
	dev->ownedPorts |= portMask;
}

static void ehciProcessPSC(ehciDevice *dev)
{
	/* Port change, find out what was connected or disconnected and determine
	   who should own the port after the event */
	uint16_t mask = 1;
	for(int port = 0; port < dev->numPorts; port++, mask <<= 1)
	{
		if(dev->ops->portSc[port] & EHCI_PSC_CONNECT_CHANGE)
		{
			if(dev->connected & mask)
			{
				dev->connected &= ~mask;
				dev->ownedPorts |= mask;
				dev->ops->portSc[port] &= ~EHCI_PSC_PORT_OWNER;
			}
			else
			{
				queryPort(port, dev);
				/* TODO: Alert USB driver and CHC if a non-highspeed device is connected */
			}
			break;
		}
	}
}

static void ehciCheckError(ehciDevice *dev)
{
	/* Transaction errors go here */
}

static void ehciProcessInterrupt(ehciDevice *dev)
{
	/* IOC interrupts and short packets */
}

static void ehciProcessFrameRollover(ehciDevice *dev)
{
	/* Probably want to queue up more work if this occurs */
}

static void ehciProcessHostError(ehciDevice *dev)
{
	/* Pretty much all of these errors are fatal and require resetting the Host
	   controller */
}

/* EHCI controller interrupt handler */
static int ehciIntHandler(void *context, const int index)
{
	uint32_t status;
	ehciDevice *dev = (ehciDevice *)context;
	status = dev->ops->usbStatus;
	if(!(status & EHCI_STATUS_ANY_INT))
		return interruptNotMe;

	if(status & EHCI_STATUS_PORT_CHANGE)
		ehciProcessPSC(dev);
	if(status & EHCI_STATUS_FRAME_ROLLOVER)
		ehciProcessFrameRollover(dev);
	if(status & EHCI_STATUS_HOST_ERROR)
		ehciProcessHostError(dev);
	else if(status & EHCI_STATUS_ERROR_INT)
		ehciCheckError(dev);
	else if(status & EHCI_STATUS_IOC)
		ehciProcessInterrupt(dev);

	return interruptHandled;
}

static void ehciDestroyDevice(ehciDevice *dev)
{
	if(dev->caps)
	{
		uintptr_t mmioBase = (uintptr_t)dev->caps;
		deregisterMMIORegion(mmioBase, dev->devHandle);
	}
	free(dev);
}

driverOperationResult controller_init(DEVICE_HANDLE dHandle, BUS_HANDLE bus_spec, busAddressDesc *addr,void *input_context, void **context)
{
	int result, resourceCount, interrupt_count;
	resourceEntry *controllerResources, *usbRegBase;
	uintptr_t usbMmioBase;
	uint8_t usbRev, eecp;
	ehciDevice *dev;

	usbRegBase = NULL;

	printf("EHCI: Initializing...\n");

	/* Validate the controller version */
	result = pciReadByte(&usbRev, addr->address, EHCI_SBRN_OFFSET);
	if(result != busOpSuccess)
		return driverOpBusError;

	if(usbRev != USB_CONTROLLER_VERSION)
		return driverOpVersionMismatch;

	/* Find the MMIO register area */
	result = getDeviceResources(dHandle, &controllerResources, &resourceCount);
	if(result != driverOpSuccess)
		return result;

	for(int resIndex = 0; resIndex < resourceCount; resIndex++)
	{
		resourceEntry *entry = &controllerResources[resIndex];
		/* Should be a memory range @ BAR0 */
		if((entry->busId == BUS_ID_MEMORY) && (entry->busSpecific == 0))
		{
			usbRegBase = entry;
			break;
		}
	}

	/* Can't do much if the MMIO registers can't be found */
	if(!usbRegBase)
		return driverOpResources;

	result = registerMMIORegion(&usbMmioBase, usbRegBase->base, usbRegBase->length, dHandle);
	if( result != driverOpSuccess)
		return result;

	/* Allocate and initialize the device specific structure */
	dev = (ehciDevice *)malloc(sizeof(ehciDevice));

	dev->devHandle = dHandle;
	dev->caps = NULL;
	dev->ops = NULL;
	dev->flags = 0;
	dev->ownedPorts = 0;
	dev->numPorts = 0;
	dev->caps = (ehciCapRegs * volatile)usbMmioBase;
	dev->ops = (ehciOpRegs * volatile)(usbMmioBase + dev->caps->capLength);

	eecp = (dev->caps->hccParams >> 8) & 0xFFU;

	/* A non-zero value of the eecp register indicates the presence of the
		controller's extended capabilties and the need to do a handoff from the bios */
	if(eecp)
	{
		uint8_t capValue;
		uint32_t eecpRegValue;
		/* Value sanity check, must be in the capabilities area of the PCI
		   config space */
		if(eecp < 0x40)
			return driverOpHardwareError;

		result = pciReadDWord(&eecpRegValue, addr->address, eecp);
		if(result != busOpSuccess)
		{
			ehciDestroyDevice(dev);
			return driverOpBusError;
		}

		/* Validate the capability value, should indicate legacy support */
		capValue = eecpRegValue & 0xFFU;
		if(capValue != EHCI_LEGACY_ID)
		{
			ehciDestroyDevice(dev);
			return driverOpHardwareError;
		}

		/* Attempt to obtain ownership of the device from the BIOS/firmware
			See EHCI specification for details
		 */
		eecpRegValue |= EHCI_LEGACY_OS_SEMAPHORE;
		result = pciWriteDWord(addr->address, eecp, eecpRegValue);
		do {
			wait(300);
			result = pciReadDWord(&eecpRegValue, addr->address, eecp);
			if(result != busOpSuccess)
			{
				ehciDestroyDevice(dev);
				return driverOpHardwareError;
			}
		} while(eecpRegValue & EHCI_LEGACY_BIOS_SEMAPHORE);

	}

	/* At this point the OS driver should have ownership and control of the EHCI
	   controller */

	dev->numPorts = dev->caps->hcsParams & 0xF;
	dev->ops->configFlags &= ~EHCI_CONFIG_CF;
	dev->ops->usbCommand &= ~(EHCI_COMMAND_RUNSTOP | EHCI_COMMAND_ASYNC_ENABLE | EHCI_COMMAND_PERIODIC_ENABLE);

	/* Check ports to see if devices are aleady connected */
	for(uint8_t port = 0; port < dev->numPorts; port++)
		queryPort(port, dev);

	/* Allocate a contiguous region for the command frames */
	if(dev->caps->hccParams & EHCI_HCC_ADDR64)
	{
		dev->flags |= EHCI_USE64;
		result = allocatePhysRegion(&dev->commandVirtBase, &dev->commandPhysBase, 64*4096, 64*4096, 0, dHandle);
		if(result != driverOpSuccess)
		{
			ehciDestroyDevice(dev);
			return result;
		}

		/* EHCI uses a 32-bit prefix address to actually form 64-bit physical addresses */
		dev->ops->segmentSelect = dev->commandVirtBase>>32;
	}
	else
	{
		result = allocatePhysRegion(&dev->commandVirtBase, &dev->commandPhysBase, 48*4096, 4096, ALLOC_USE32, dHandle);
		if(result != driverOpSuccess)
		{
			ehciDestroyDevice(dev);
			return result;
		}
	}

	interrupt_count = 1;
	result = registerInterruptHandler(&ehciIntHandler, dev, &interrupt_count, dHandle);
	if(result != driverOpSuccess)
	{
		ehciDestroyDevice(dev);
		return result;
	}

	*context = dev;

	printf("EHCI: Controller successfully initialized!\n");
	return driverOpSuccess;
}

driverOperationResult controller_deinit(void *context)
{
	return driverOpSuccess;
}

driverOperationResult controller_enum(void *context, void *identifyFunc)
{
	return driverOpSuccess;
}
