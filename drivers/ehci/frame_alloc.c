#include "frame_alloc.h"
#include <driverapi.h>

#define FRAME_BLOCK_SIZE           4096
#define EHCI_FRAME_ALIGN           32

void *allocateFrame(uint16_t frameSize, frameAllocatorState *state)
{
  uintptr_t freeSpace;
  uint64_t mask, index;
  uint16_t bin;
  frameBlock *block;

  /* Calculate the appropriate bin index */
  bin = frameSize/EHCI_FRAME_ALIGN;
  if(frameSize%EHCI_FRAME_ALIGN)
    bin++;
  bin--;

  if(!state->blocks[bin])
  {
    /* Initialize a new block cache, if there's no partially filled caches */
    block = state->freeBlocks;
    if(!block)
      return NULL;
    state->freeBlocks = (frameBlock *)block->nextLink;
    block->nextLink = (bin+1)*EHCI_FRAME_ALIGN;
  }
  else
    block = state->blocks[bin];

  mask = 1;
  /* Find the first open entry in the bitmap */
  freeSpace = (uintptr_t)block;
  if(block->bitmaps[0])
  {
    index = getLsb64(block->bitmaps[0]);
    mask <<= index;
    block->bitmaps[0] &= ~mask;
  }
  else
  {
    index = getLsb64(block->bitmaps[1]);
    mask <<= index;
    index += 64;
    block->bitmaps[1] &= ~mask;
  }

  /* Check if the block cache is now full */
  if( !(block->bitmaps[0] | block->bitmaps[1]) )
  {
    /* Remove it from the bin list if it is */
    state->blocks[bin] = (frameBlock *)(block->nextLink &= ~0xFFUL);
    if(state->blocks[bin])
      state->blocks[bin]->prev = NULL;

    block->nextLink &= ~0xFFUL;
    block->prev = NULL;
  }

  /* Calculate the offset into the block  */
  freeSpace += sizeof(frameBlock) + index*(bin+1)*EHCI_FRAME_ALIGN;
  return (void *)freeSpace;
}

static void linkBlock(frameBlock **field, frameBlock *value)
{
  if(*field)
  {
    value->nextLink &= ~0xFFUL;
    value->nextLink |= (uintptr_t)*field;
    (*field)->prev = value;
  }

  value->prev = NULL;
  *field = value;
}

int freeFrame(void *ptr, frameAllocatorState *state)
{
  uintptr_t blockAddr, blockGranularity, frameIndex;
  uint64_t mask;
  int bin;

  frameBlock *block;
  uintptr_t addr = (uintptr_t)ptr;
  if( (addr < state->base) || ((addr + FRAME_BLOCK_SIZE) > (state->base + state->align))  )
    return FALLOC_INVALID;

  frameIndex = addr%FRAME_BLOCK_SIZE;
  blockAddr = addr - frameIndex;
  frameIndex -= sizeof(frameBlock);
  block = (frameBlock *)blockAddr;
  blockGranularity = (block->nextLink & 0xFF);
  if(frameIndex%blockGranularity )
    return FALLOC_CORRUPTED;

  frameIndex /= blockGranularity;
  bin = blockGranularity /= EHCI_FRAME_ALIGN;
  bin--;


  if( !(block->bitmaps[0] | block->bitmaps[1]) )
  {
      /* A previously full block now has some free entries */
      linkBlock(&state->blocks[bin], block);
  }
  mask = 1;
  if(frameIndex < 64)
  {
    mask <<= frameIndex;
    block->bitmaps[0] |= mask;
  }
  else
  {
    mask <<= frameIndex - 64;
    block->bitmaps[1] |= mask;
  }

  if( !(~(block->bitmaps[0] & block->bitmaps[1])) )
  {
    /* If there's no frames allocated in this block, return it to the free blocks list */
    uintptr_t nextAddr;
    nextAddr = block->nextLink & ~0xFFUL;

    /* Unlink the block */
    if(block->prev)
    {
        block->prev->nextLink &= ~0xFFUL;
        block->prev->nextLink |= nextAddr;
    }
    else
    {
      frameBlock *next = (frameBlock *)nextAddr;
      state->blocks[bin] = next;
      if(next)
        next->prev = NULL;
    }

    /* Put the block at the head of the free blocks list */
    block->nextLink = (uintptr_t)state->freeBlocks;
    if(state->freeBlocks)
      state->freeBlocks->prev = block;

    state->freeBlocks = block;
  }

  return 0;
}

frameAllocatorState *initFrameAllocator(const uintptr_t regionBase, const uintptr_t regionSize)
{
  frameBlock *lastBlock;
  frameAllocatorState *state = (frameAllocatorState *)malloc(sizeof(frameAllocatorState));

  state->base = regionBase;
  state->align = regionSize;
  for(int bin = 0; bin < EHCI_BLOCK_BINS; bin++)
    state->blocks[bin] = NULL;
  lastBlock = NULL;

  for(uintptr_t frameBlockPointer = regionBase; frameBlockPointer < (regionBase + regionSize); frameBlockPointer += FRAME_BLOCK_SIZE)
  {
    frameBlock *block = (frameBlock *)frameBlockPointer;
    /* Initialize bitmaps */
    block->bitmaps[0] = 0;
    block->bitmaps[1] = 0;

    /* Initialize linkages */
    block->prev = lastBlock;
    if(!lastBlock)
      state->freeBlocks = block;
    else
      lastBlock->nextLink = frameBlockPointer;

    lastBlock = block;
  }

  return state;
}
