#ifndef XHCI_REGS_H_
#define XHCI_REGS_H_

//PCI Configuration space registers
#define PCI_CLASS_CODE_REV          0x08U

#define USB_SBRN_REG                0x60U
#define USB_FLADJ_REG               0x61U

#define USB_VERSION_MAJOR_MASK      0xF0U
#define USB_VERSION3                0x30U

#define USB_XHCI_CLASS_CODE         0x0C0330U

#define EXCAP_USB_LEGACY            1
#define EXCAP_SUPPORTED_PROTO       2
#define EXCAP_EXT_PM                3
#define EXCAP_IO_VIRT               4
#define EXCAP_MSI                   5
#define EXCAP_LOCAL_MEM             6
#define EXCAP_USB_DEBUG             10
#define EXCAP_EXT_MSI               17

#define USB_LEGACY_OS_OWNERSHIP     (1U<<24)


//Capability parameters 1 flags
#define XHCI_HCC1_64BIT_CAPABLE         0x001U
#define XHCI_HCC1_BNC                   0x002U
#define XHCI_HCC1_CSZ                   0x004U
#define XHCI_HCC1_PPC                   0x008U
#define XHCI_HCC1_PIND                  0x010U
#define XHCI_HCC1_LHRC                  0x020U
#define XHCI_HCC1_LTC                   0x040U
#define XHCI_HCC1_NSS                   0x080U
#define XHCI_HCC1_PAE                   0x100U
#define XHCI_HCC1_SPC                   0x200U
#define XHCI_HCC1_SEC                   0x400U
#define XHCI_HCC1_CFC                   0x800U

//Capabilities parameters 2 flags
#define XHCI_HCC2_U3C                   0x001U
#define XHCI_HCC2_CMC                   0x002U
#define XHCI_HCC2_FSC                   0x004U
#define XHCI_HCC2_CTC                   0x008U
#define XHCI_HCC2_LEC                   0x010U
#define XHCI_HCC2_CIC                   0x020U
#define XHCI_HCC2_ETC                   0x040U
#define XHCI_HCC2_ETC_TSC               0x080U
#define XHCI_HCC2_GSC                   0x100U
#define XHCI_HCC2_VTC                   0x200U

//Command register flags
#define USB_COMMAND_RUNSTOP             0x00001U
#define USB_COMMAND_RESET               0x00002U
#define USB_COMMAND_INT_ENABLE          0x00004U
#define USB_COMMAND_SYSTEM_ERROR_EN     0x00008U
#define USB_COMMAND_LIGHT_RESET         0x00080U
#define USB_COMMAND_SAVE_STATE          0x00100U
#define USB_COMMAND_RESTORE_STATE       0x00200U
#define USB_COMMAND_ENABLE_WRAP_EVENT   0x00400U
#define USB_COMMAND_ENABLE_U3_MFINDEX   0x00800U
#define USB_COMMAND_CEM_ENABLE          0x02000U
#define USB_COMMAND_EXT_TBC_EN          0x04000U
#define USB_COMMAND_EXT_TBC_TRB_EN      0x08000U
#define USB_COMMAND_VTIO_EN             0x10000U

#define USB_VERSION_MAJOR               0xF0U
#define USB_VERSION3                    0x30U

//Status register flags
#define USB_STATUS_HCH                  0x0001U
#define USB_STATUS_HSE                  0x0004U
#define USB_STATUS_EINT                 0x0008U
#define USB_STATUS_PCD                  0x0010U
#define USB_STATUS_SSS                  0x0100U
#define USB_STATUS_RSS                  0x0200U
#define USB_STATUS_SRE                  0x0400U
#define USB_STATUS_CNR                  0x0800U
#define USB_STATUS_HCE                  0x1000U

//Configure register flags
#define USB_CONFIGURE_U3E               0x0100U
#define USB_CONFIGURE_CIE               0x0200U

//Interrupter register masks
#define XHCI_IMAN_IP                    0x0001U
#define XHCI_IMAN_IE                    0x0002U
#define XHCI_INTR_DQ_EHB                0x0008U

//Port status and control flags
#define XHCI_PORTSC_CCS                 0x00000001U
#define XHCI_PORTSC_PED                 0x00000002U
#define XHCI_PORTSC_OCA                 0x00000008U
#define XHCI_PORTSC_PR                  0x00000010U
#define XHCI_PORTSC_PP                  0x00000200U
#define XHCI_PORTSC_LWS                 0x00010000U
#define XHCI_PORTSC_CSC                 0x00020000U
#define XHCI_PORTSC_PEC                 0x00040000U
#define XHCI_PORTSC_WRC                 0x00080000U
#define XHCI_PORTSC_OCC                 0x00100000U
#define XHCI_PORTSC_PRC                 0x00200000U
#define XHCI_PORTSC_PLC                 0x00400000U
#define XHCI_PORTSC_CEC                 0x00800000U
#define XHCI_PORTSC_CAS                 0x01000000U
#define XHCI_PORTSC_WCE                 0x02000000U
#define XHCI_PORTSC_WDE                 0x04000000U
#define XHCI_PORTSC_WOE                 0x08000000U
#define XHCI_PORTSC_DR                  0x40000000U
#define XHCI_PORTSC_WPR                 0x80000000U

#define XHCI_PORTSC_PLS_MASK            0x000001E0U
#define XHCI_PORTSC_PIC_MASK            0x0000C000U

#define XHCI_PORTSC_CHANGE_STATUS       (XHCI_PORTSC_CSC | XHCI_PORTSC_PEC | XHCI_PORTSC_WRC | XHCI_PORTSC_OCC | XHCI_PORTSC_PRC | XHCI_PORTSC_PLC | XHCI_PORTSC_CEC)

#define XHCI_PORTSC_GET_PLS(x)          (((x)>>5) & 0xFU)
#define XHCI_PORTSC_PORTSPEED(x)        (((x)>>10) & 0xFU)
#define XHCI_PORTSC_GET_PIC(x)          (((x)>>14) & 0x3U)

#define XHCI_PORTSC_SET_PLS(x)          (((x) & 0xFU) << 5)
#define XHCI_PORTSC_SET_PIC(x)          (((x) & 0x3U) << 14)

/* Port link state field read values */
#define XHCI_PLS_READ_U0                0
#define XHCI_PLS_READ_U1                1
#define XHCI_PLS_READ_U2                2
#define XHCI_PLS_READ_U3                3
#define XHCI_PLS_READ_DISABLED          4
#define XHCI_PLS_READ_RXDETECT          5
#define XHCI_PLS_READ_INACTIVE          6
#define XHCI_PLS_READ_POLLING           7
#define XHCI_PLS_READ_RECOVERY          8
#define XHCI_PLS_READ_HOT_RESET         9
#define XHCI_PLS_READ_COMPLIANCE        10
#define XHCI_PLS_READ_TEST              11
#define XHCI_PLS_READ_RESUME            15

/* Port link state field write values */
#define XHCI_PLS_WRITE_U0               0
#define XHCI_PLS_WRITE_U1               1
#define XHCI_PLS_WRITE_U2               2
#define XHCI_PLS_WRITE_U3               3
#define XHCI_PLS_WRITE_RXDETECT         5
#define XHCI_PLS_WRITE_COMPLIANCE       10
#define XHCI_PLS_WRITE_RESUME           15

/* Port indicator values */
#define XHCI_PIC_OFF                    0
#define XHCI_PIC_AMBER                  1
#define XHCI_PIC_GREEN                  2

/* Port PMSC register values */
#define XHCI_PMSC_USB3_U1_MASK          0x0000FFU
#define XHCI_PMSC_USB3_U2_MASK          0x00FF00U

#define XHCI_PMSC_USB3_SET_U1(x)        ((x) & 0xFFU)
#define XHCI_PMSC_USB3_SET_U2(x)        ((x) << 8)


#define XHCI_PMSC_USB3_FLA              0x010000U




#define XHCI_OP_CRC_PRESERVE            0x30U
#define XHCI_INT_ERSTBA_PRESERVE        0x3FU

//! Capabilities registers defined in the XHCI specification sect 5.3
typedef struct COMPILER_PACKED
{
	volatile uint8_t capLength, _reserved;                     //Offset 00h, 001h
	volatile uint16_t hciVersion;                              //Offset 02h
	volatile uint32_t hcsparams1, hcsparams2, hcsparams3;      //Offset 04h, 08h, 0Ch
	volatile uint32_t hccparams1, dbOff, rtsOff, hccparams2;   //Offset 10h, 14h, 18h, 1Ch
	//Implementation dependent register and configuration
	volatile uint32_t virtIoOff[];
}  xhci_cap_regs;

typedef struct COMPILER_PACKED
{
	volatile uint32_t portStatusControl, portPmStatusContrl, portLinkInfo, portHwLPM;
} portReg;

//! Operational registers defined in the XHCI specification sect 5.4
typedef struct COMPILER_PACKED
{
	volatile uint32_t usbCommand;               // Offset 00h
	volatile uint32_t usbStatus;                // Offset 04h
	volatile uint32_t pageSize;                 // Offset 08h, RO
	volatile uint32_t _reserved[2];             // Offset 0Ch, 10h
	volatile uint32_t devNotificationControl;   // Offset 14h

	union                                       // Offset 18h
	{
		volatile uint64_t commandRingControl64;
		volatile uint32_t commandRingControl32;
	};

	volatile uint32_t _reserved2[4];            // Offset 20h, 24h, 28h, 2Ch

	union                                       // Offset 30h
	{
		volatile uint64_t dcbaap64;
		volatile uint32_t dcbaap32;
	};
	volatile uint32_t configure;                // Offset 38h
	volatile uint32_t _reserved3[241];
	volatile portReg portRegSet[];
} xhci_operational_regs;

typedef struct COMPILER_PACKED
{
	volatile uint32_t iman, imod, eventSegCount, _reserved;

	union
	{
		volatile uint64_t eventRingSegmentTableBase64;
		volatile uint32_t eventRingSegmentTableBase32;
	};

	union
	{
		volatile uint64_t eventRingDequeuePtr64;
		volatile uint32_t eventRingDequeuePtr32;
	};
} xhci_interrupt_reg;

typedef struct COMPILER_PACKED
{
	volatile uint32_t uFrameIndex, _reserved[7];
	volatile xhci_interrupt_reg interrupter[];
} xhci_runtime_regs;

typedef struct COMPILER_PACKED
{
	volatile uint8_t cap_id, next_cap, hc_bios_owned, hc_os_owned;
} xhci_excap_legacy;


#endif
