#include "xhci_cq.h"
#include "xhci_context.h"
#include "xhci_log.h"

#include <driverapi.h>


/*! Creates a new ringRegion object
 * \param hcState Controller state structure
 * \param newRegion Output pointer to the newly allocated linkedRegion object
 * \return driverOpSuccess if successful, allocateMappedPage failure values
 */
driverOperationResult xhci_create_ring_region(xhci_controller_state *hcState, ringRegion **new_region, const uint8_t cycle_state)
{
    driverOperationResult result;

    ringRegion *region;

    region = (ringRegion *)calloc(1, sizeof(ringRegion));
    result = allocate_mapped_page(&region->region, hcState);
    if(result != driverOpSuccess)
        return result;

    for(int trb_index = 0; trb_index < TRB_PER_PAGE; trb_index++)
    {
        region->region.commands[trb_index].noOp.cycleFlags = ((~cycle_state) & TRB_CYCLE_BIT);
    }

    region->region_dq_index = TRB_PER_PAGE;
    region->region_eq_index = 0;

    *new_region = region;
    return driverOpSuccess;
}


/*! Links a linkedRegion object in both a ring queue and an interrupter's event tree
 *  \param hcState Host controller state structure
 *  \param predecessor Previous region in the command ring, or NULL if the ring is empty
 *  \param region Region to be inserted
 *  \param ring Target ring queue
 */
void xhci_link_ring(xhci_controller_state *hcState, ringRegion *predecessor, ringRegion *region, ringQueue *ring)
{
	volatile linkTRB *rLink;
	//physInstance *segmentInstance;

	/* Setup link TRB pointer */
	rLink = &region->region.trbOther[TABLE_LAST_TRB].link;

	/* Create and initialize the tree object */
	/*
	segmentInstance = (physInstance *)malloc(sizeof(physInstance));
	memset(segmentInstance, 0, sizeof(physInstance));

	segmentInstance->region = region;
	segmentInstance->queue = ring;
    */

	if(predecessor)
	{
		/* At least one other region exists on the ring */
		volatile linkTRB *pLink;
		/* Host side structure linking */
		region->prev = predecessor;
		region->next = predecessor->next;
		region->next->prev = region;
		predecessor->next = region;

		/* Setup the predecessor link TRB pointer */
		pLink = &predecessor->region.trbOther[TABLE_LAST_TRB].link;

		/* Initialize the new link TRB */
		rLink->segmentPointer = region->next->region.physicalBase;
		rLink->trbType = WRITE_TRB_TYPE_ADJ(TRB_LINK);
		rLink->flags = ring->cycleState | ((region->next == ring->ringStem) ? LINK_TRB_TOGGLE_CYCLE:0);

		/* Link up with the prior region */
		pLink->segmentPointer = region->region.physicalBase;
        pLink->flags = ring->cycleState | ((region->next == ring->ringStem) ? LINK_TRB_TOGGLE_CYCLE:0);

	}
	else
	{
		/* First region to be added to the ring */
		region->next = region;
		region->prev = region;
		rLink->segmentPointer = region->region.physicalBase;
		rLink->flags = ring->cycleState | LINK_TRB_TOGGLE_CYCLE;
		rLink->trbType = WRITE_TRB_TYPE_ADJ(TRB_LINK);

		ring->ringStem = region;
		ring->enqueueRegion = region;
	}

	ring->regionCount++;
}

/*! Creates a new ring object that contains a single region
 *  \param hcState Host controller state structure
 *  \param ringOut Return pointer for the newly created ring
 *  \return driverOpSucces if successful
 *  \sa xhci_create_linked_region
 */
driverOperationResult xhci_create_ring(xhci_controller_state *hcState, ringQueue **ringOut )
{
	driverOperationResult result;
	ringQueue *ring;
	ringRegion *initRegion;
	/* Allocate and initialize the ring structure */
	ring = (ringQueue *)calloc(1, sizeof(ringQueue));

	ring->rqLock = dapi_lock_create();
	ring->cycleState = 0x1;

	/* Create the region and link it into the ring */
	result = xhci_create_ring_region(hcState, &initRegion, ring->cycleState);
	if(result != driverOpSuccess)
	{
		free(ring);
		return result;
	}
	xhci_link_ring(hcState, NULL, initRegion, ring);

	ring->completedRegion = initRegion;

	*ringOut = ring;

	return driverOpSuccess;
}

/*! Unlinks a region from a ring
 *  \param hcState Host controller state
 *  \param region Region object to remove
 *  \param ringHead Pointer to the ring's head field
 */
void xhci_ring_unlink(xhci_controller_state *hcState, ringRegion *region, ringRegion **ringHead)
{
	if(region->prev != region)
	{
		volatile linkTRB *rLink, *pLink;
		/* Link TRB pointers */
		pLink = &region->prev->region.trbOther[TABLE_LAST_TRB].link;
		rLink = &region->region.trbOther[TABLE_LAST_TRB].link;

		/* Update the software links */
		if(region->next != region)
			region->next->prev = region->prev;
		if(*ringHead == region)
			*ringHead = region->prev;

		region->prev->next = region->next;
		/* Update TRB links */
		pLink->segmentPointer = rLink->segmentPointer;
		pLink->flags = rLink->flags;
	}
	else
		*ringHead = NULL;
}

/*! Destroys a linked region object
 *  \param hcState Host controller state structure
 *  \param reg Region to be destroyed
 *  \return driverOpSuccess if successful
 */
driverOperationResult xhci_destroy_ring_region(xhci_controller_state *hcState, ringRegion *reg)
{
	driverOperationResult result;
	result = freePhysRegion(reg->region.virtualBase, hcState->dev);
	if(result != driverOpSuccess)
		return result;

	free(reg);

	return driverOpSuccess;
}

/*! Returns the physical address of the current dequeue pointer for a ring
 *
 *  \param ring Ring queue being queried
 *  \return Physical address of the current dequeue pointer
 */
uint64_t xhci_ring_get_dq(ringQueue *ring)
{
    uint64_t addr;

    addr = ring->completedRegion->region.physicalBase;
    addr += ring->completedRegion->region_dq_index * TRB_SIZE;

    return addr;
}

/*! Returns the physical address of the current enqueue pointer for a ring
 *
 *  \param ring Ring queue being queried
 *  \return Physical address of the current enqueue pointer
 */
uint64_t xhci_ring_get_eq(ringQueue *ring)
{
    uint64_t addr;

    addr = ring->completedRegion->region.physicalBase;
    addr += ring->completedRegion->region_eq_index * TRB_SIZE;

    return addr;
}


static volatile softwareIssuedTRB *xhci_rr_get_batch_trb(const int requested_trbs, xhci_controller_state *hcState, ringQueue *ring, ringRegion *eq_reg)
{
    volatile softwareIssuedTRB *trb_ret;
    int reg_trb_tail;

    reg_trb_tail =  (eq_reg->region_dq_index > eq_reg->region_eq_index) ? (eq_reg->region_dq_index - eq_reg->region_eq_index):(TRB_PER_PAGE - eq_reg->region_eq_index);

    trb_ret = NULL;

    if(reg_trb_tail > requested_trbs)
    {
        trb_ret = &eq_reg->region.trbs[eq_reg->region_eq_index];

        for(int trb_index = 0; trb_index < requested_trbs; trb_index++)
        {
            volatile noOpTRB *noop = &trb_ret[trb_index].command.noOp;

            noop->trbType = 0;
            noop->_reserved2 = 0;
            noop->cycleFlags = ~ring->cycleState & TRB_CYCLE_BIT;
            noop->_reserved = 0;
            noop->transferInfo = 0;
        }

        eq_reg->region_eq_index += requested_trbs;

    }

    return trb_ret;
}

driverOperationResult xhci_get_batch_trb(const int requested_trbs, xhci_controller_state *hcState, ringQueue *ring, volatile softwareIssuedTRB **trb_start, uint8_t *cycleState)
{
    ringRegion *eq_region;
    volatile softwareIssuedTRB *start_trb;

    dapi_lock_acquire(ring->rqLock);

    eq_region = ring->enqueueRegion;

    xhci_log(XHCI_LOG_INTERNAL, "Attempting to get %u TRBs on ring %p Caller: %p\n", requested_trbs, ring, __builtin_return_address(0));
    start_trb = xhci_rr_get_batch_trb(requested_trbs, hcState, ring, eq_region);

    if(!start_trb)
    {
        ringRegion *link_region;
        volatile linkTRB *link_trb;
        int next_trb_index = eq_region->region_eq_index;


        if(eq_region->next->region_dq_index >= requested_trbs)
        {
            /* An existing linked region with enough space for the allocation already exists */
            link_region = eq_region->next;
            link_region->region_eq_index = 0;
        }
        else
        {
            /* A new TRB region needs to be allocated and linked */
            driverOperationResult result;

            /* Create a new region */
            result = xhci_create_ring_region(hcState, &link_region, ring->cycleState);
            if(result != driverOpSuccess)
                return result;
            xhci_link_ring(hcState, eq_region, link_region, ring);
        }

        /* Set up the link entry to be next TRB in the current region */
        link_trb = &eq_region->region.trbs[next_trb_index].link;
        link_trb->flags = ring->cycleState | ((link_region == ring->ringStem) ? LINK_TRB_TOGGLE_CYCLE:0);
        link_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_LINK);
        link_trb->segmentPointer = link_region->region.physicalBase;

        /* Initialize the cycle state to correct value so the controller actually stops when it loops back around */
        for(int rem_trb_index = next_trb_index+1; rem_trb_index < TRB_PER_PAGE; rem_trb_index++)
        {
            eq_region->region.trbs[rem_trb_index].command.noOp.cycleFlags = ring->cycleState;
        }

        /* Update the cycle state if the ring has looped back around */
        if(link_region == ring->ringStem)
            ring->cycleState = (~(ring->cycleState) & TRB_CYCLE_BIT);

        ring->enqueueRegion = link_region;

        start_trb = xhci_rr_get_batch_trb(requested_trbs, hcState, ring, link_region);
        if(!start_trb)
            xhci_log(XHCI_LOG_ERRORS, "Error failed to get TRB despite roll over!\n");
    }

    if( (start_trb->command.noOp.cycleFlags & TRB_CYCLE_BIT) == ring->cycleState )
    {
        xhci_log(XHCI_LOG_ERRORS, "Error - Ring %p TRB allocate cycle mismatch!\n");
        return driverOpBadState;
    }

    *trb_start = start_trb;
    *cycleState = ring->cycleState;
    ring->commandsInFlight += requested_trbs;
    xhci_log(XHCI_LOG_INTERNAL, "Allocated %u TRBs @ %p from ring @ %p with cycle %u\n", requested_trbs, start_trb, ring, ring->cycleState);
    dapi_lock_release(ring->rqLock);
    return driverOpSuccess;
}

/*! Retrieves the next available TRB from a command/transfer ring
 *  \param nextTRB TRB return pointer
 *  \param cycleState State the cycle bit should be set to for the TRB to be valid
 *  \param hcState Host controller state structure
 *  \param ring Ring queue to retrieve TRB from
 */
driverOperationResult xhci_get_ring_trb(volatile softwareIssuedTRB **nextTRB, uint8_t *cycleState, xhci_controller_state *hcState, ringQueue *ring)
{
	return xhci_get_batch_trb(1, hcState, ring, nextTRB, cycleState);
}

/*! Sets the state of the cycle bit in a TRB
 *  \param trb Pointer to the TRB to update
 *  \param cycle_value Value of the cycle bit
 */
void xhci_trb_set_cycle(volatile void *trb, const uint8_t cycle_value)
{
   volatile noOpTRB *ptr = (volatile noOpTRB *)trb;
   ptr->cycleFlags &= ~TRB_CYCLE_BIT;
   ptr->cycleFlags |= cycle_value;
}

/*! Destroys a command ring queue
 *  \param hcState Host controller state structure
 *  \param ring Ring queue to destroy
 */
driverOperationResult xhci_destroy_ring(xhci_controller_state *hcState, ringQueue *ring)
{
	driverOperationResult result;
	while(ring->ringStem)
	{
		ringRegion *reg = ring->ringStem;
		xhci_ring_unlink(hcState, reg, &ring->ringStem);
		result = xhci_destroy_ring_region(hcState, reg);
		if(result != driverOpSuccess)
			return result;
	}

	free(ring);
	return driverOpSuccess;
}

/*! Retrieves a completed TRB
 *  \param trbRet Return pointer for the TRB
 *  \param trb_address Physical address of the completed TRB
 *  \param ring Command or Transfer ring to search
 *  \return Zero if the TRB address could not be matched to a TRB pointer.  Non-zero if successful.
 */
int xhci_get_completed_trb(volatile softwareIssuedTRB **trbRet, const uint64_t trb_address, ringQueue *ring)
{
	uint64_t trb_page_base;
	ringRegion *reg;

	trb_page_base = (trb_address & ~(ARCH_PAGE_OFFSET) );
	reg = ring->enqueueRegion;

	/* Loop backwards through region until a matching one is found or the initial region is encountered again */
	do
	{
		if(reg->region.physicalBase == trb_page_base)
		{
			/* Calculate the virtual address of the TRB and return */
			volatile softwareIssuedTRB *trb;

			trb = (volatile softwareIssuedTRB *)(reg->region.virtualBase + (trb_address - trb_page_base) );
			*trbRet = trb;
			return 1;
		}
		else
			reg = reg->prev;
	} while(reg != ring->enqueueRegion);

	/* TRB could not be resolved, signal an error */
	xhci_log(XHCI_LOG_ERRORS, "Could not resolve TRB @ %p\n", trb_address);
	return 0;
}

uint64_t xhci_ring_get_base(ringQueue *ring)
{
	return ring->ringStem->region.physicalBase;
}

/*! Gets an endpoint ring based on the context
 *  \param hcState XHCI Controller Instance Context
 *  \param slot Slot of the device supplying the target endpoint
 *  \param endpoint_id Device context index to the endpoint's entry
 *  \return A pointer to the ring queue controller structure or NULL if the request was invalid
 */
ringQueue *xhci_get_endpoint_ring_from_id(xhci_controller_state *hcState, const uint8_t slot, const uint8_t endpoint_id)
{
	xhci_slot_state *slot_info;
	volatile slotContext *sc;

	if(slot >= hcState->maxSlots)
    {
        xhci_log(XHCI_LOG_ERRORS, "Get RQ - Slot not in range - Slot: %u Max: %u\n", slot, hcState->maxSlots);
		return NULL;
    }

	slot_info = &hcState->slots[slot];


	if(!slot_info || !endpoint_id)
    {
        xhci_log(XHCI_LOG_ERRORS, "Get RQ - Invalid endpoint id[%u] or slot info pointer[%p]\n", endpoint_id, slot_info);
		return NULL;
    }
	sc = (volatile slotContext *)xhci_get_dev_context(hcState, slot, 0);
	if(!sc)
    {
        xhci_log(XHCI_LOG_ERRORS, "Get RQ - Unable to retrieve slot context\n");
		return NULL;
    }

	return (slot_info->endpoint_rings[endpoint_id-1]);
}

/* Updates the state of a ring after a TRB completes
 *
 *  \param rq Ring queue containing the TRB
 *  \param trb Completed TRB
 */
driverOperationResult xhci_update_ring_completed(ringQueue *rq, volatile softwareIssuedTRB *trb)
{
    uintptr_t trb_addr, trb_base;
    uint32_t old_dq;

    trb_addr = (uintptr_t)trb;
    trb_base = trb_addr - (trb_addr%ARCH_PAGE_SIZE);

    dapi_lock_acquire(rq->rqLock);

    uintptr_t region_virt_base;
    int region_rem;
    ringRegion *current_reg;

    region_rem = rq->regionCount;
    current_reg = rq->completedRegion;

    region_virt_base = current_reg->region.virtualBase;

    while(trb_base != region_virt_base)
    {
        region_rem--;
        if(!region_rem)
        {
            dapi_lock_release(rq->rqLock);
            xhci_log(XHCI_LOG_INFO, "Failed to find matching TRB region for TRB @ %p\n", trb);
            return driverOpBadState;
        }

        current_reg = current_reg->next;

        region_virt_base = current_reg->region.virtualBase;


    }

    rq->completedRegion = current_reg;

    old_dq = current_reg->region_dq_index;
    if(old_dq == TRB_PER_PAGE)
        old_dq = 0;
    current_reg->region_dq_index = ((trb_addr - region_virt_base)/TRB_SIZE);

    rq->completedPtr = trb;
    xhci_log(XHCI_LOG_INTERNAL, "Updated completed - Ptr: %p Calculated: %p\n", trb, &current_reg->region.trbs[current_reg->region_dq_index]);

    dapi_lock_release(rq->rqLock);
    return driverOpSuccess;
}

/*! Retrieves the transfer ring for a specific endpoint
 *  \param hc Host Controller Instance context
 *  \param slot Slot of the endpoint being requested
 *  \param ep Endpoint to retrieve the queue for
 *  \param in_out Input or output pipe
 *  \return A pointer to the ring queue control structure or NULL if the request was invalid
 */
ringQueue *xhci_get_endpoint_ring(xhci_controller_state *hc, const uint8_t slot, const uint8_t ep, const int in_out)
{
	xhci_slot_state *slot_info;
	uint32_t offset;

	if(slot >= hc->maxSlots)
    {
        xhci_log(XHCI_LOG_ERRORS, "Get endpoint ring - invalid slot request: %u\n", slot);
		return NULL;
    }

	slot_info = &hc->slots[slot];

	if(ep >= slot_info->ep_count)
    {
        xhci_log(XHCI_LOG_ERRORS, "Get endpoint ring - Invalid endpoint index! Requested: %u Max: %u\n", ep, slot_info->ep_count);
		return NULL;
    }
	if(ep)
	{
		offset = ep*2 -1;
		if(in_out && offset)
			offset++;
	}
	else
		offset = 0;

	return slot_info->endpoint_rings[offset];
}

/*! Traverses to the next TRB, meant to be used for completed TRBs
 *  \param rq Ring queue that the TRB resides in
 *  \param init_trb Pointer to the prior TRB
 *  \return Pointer to the next TRB or NULL if one does not exist
 */
volatile void *xhci_get_next_trb(ringQueue *rq, volatile void *init_trb)
{
	uintptr_t rem, trb_page_base;
	ringRegion *region;
	volatile commandTRB *init = (volatile commandTRB *)init_trb;

	rem = (uintptr_t)init_trb;
	trb_page_base = rem;

	rem %= ARCH_PAGE_SIZE;
	trb_page_base -= rem;

	rem = ARCH_PAGE_SIZE - rem;
	rem /= sizeof(commandTRB);
	region = rq->ringStem;

	if(rem > 1)
	{
		init++;
		return init;
	}
	else
	{
		for(int region_index = 0; region_index < rq->regionCount; region_index++)
		{
			if(region->region.virtualBase == trb_page_base )
				return region->next->region.transfers;
		}
		return NULL;
	}
}
