/* XHCI controller port related utility functions
 *
 * Author: Joseph Kinzel */

#ifndef XHCI_PORTS_H_
#define XHCI_PORTS_H_

#include <driverapi.h>
#include "xhci_controller.h"
#include "xhci_assert.h"

/* PSI Flags and field extraction macros */
#define XHCI_PSI_GET_ID(x)                    (x & 0x0FU)
#define XHCI_PSI_GET_EXPONENT(x)              (((x)>>4) & 0x3U)
#define XHCI_PSI_GET_TYPE(x)                  (((x)>>6) & 0x3U)
#define XHCI_GET_PSIC(x)                      ((x)>>12)

#define XHCI_PSI_FULL_DUPLEX                  0x100U

#define XHCI_PSI_GET_LINK(x)                  (((x)>>14) & 0x3U)
#define XHCI_PSI_GET_MANTISSA(x)              ((x)>>16)

#define XHCI_PROTO_NS_USB                     0x20425355U

#define XHCI_PSI_EX_KB                        1
#define XHCI_PSI_EX_MB                        2
#define XHCI_PSI_EX_GB                        3

#define XHCI_DEFAULT_PSI_FS                   1
#define XHCI_DEFAULT_PSI_LS                   2
#define XHCI_DEFAULT_PSI_HS                   3
#define XHCI_DEFAULT_PSI_SS1X1                4
#define XHCI_DEFAULT_PSI_SS2X1                5
#define XHCI_DEFAULT_PSI_SS1X2                6
#define XHCI_DEFAULT_PSI_SS2X2                7

/* Standard device speed bitrates */
#define XHCI_USB_LS_BITRATE                   1500
#define XHCI_USB_FS_BITRATE                   12000
#define XHCI_USB_HS_BITRATE                   480000
#define XHCI_USB_SS1X1_BITRATE                5000000
#define XHCI_USB_SS2X1_BITRATE                10000000
#define XHCI_USB_SS1X2_BITRATE                10000000
#define XHCI_USB_SS2X2_BITRATE                20000000

/* Port indicator color control values */
#define USB_PORT_INDICATOR_AUTO               0
#define USB_PORT_INDICATOR_AMBER              1
#define USB_PORT_INDICATOR_GREEN              2
#define USB_PORT_INDICATOR_OFF                3

/* Protocol speed extended capability as defined XHCI Specification section 7.2 */
typedef struct COMPILER_PACKED
{
  volatile uint8_t capability_id, next_capability_offset, revision_minor, revision_major;
  volatile uint32_t name_string;
  volatile uint8_t port_offset, port_count;
  volatile uint16_t protocol_info;
  volatile uint8_t protocol_slot_type, _reserved[3];
  volatile uint32_t protocol_speed_ids[];
} xhci_supported_protocol;

typedef enum
{
    PORT_PROTO_USB20 = 0,
    PORT_PROTO_USB30 = 1,
    PORT_PROTO_USB31 = 2,
    PORT_PROTO_USB32 = 4
} port_generation_type;

/* Protocol speed parsing and conversion functions */
driverOperationResult process_xhci_protocol_capability(xhci_controller_state *hcState, xhci_supported_protocol *cap);
driverOperationResult xhci_probe_ports(xhci_controller_state *hcState);
int xhci_get_port_speed(const xhci_port_info *port_info, const int psi_value);
int xhci_get_psi_from_speed(xhci_controller_state *hcState, const uint16_t dev_speed, const uint16_t root_port);

/* Root hub port level set_feature helper functions */
void xhci_port_set_reset(xhci_controller_state *hcState, const int port_index);
void xhci_port_set_enable(xhci_controller_state *hcState, const int port_index);
driverOperationResult xhci_port_set_suspend(xhci_controller_state *hcState, const int port_index);
driverOperationResult xhci_port_set_power(xhci_controller_state *hcState, const int port_index);
driverOperationResult xhci_port_set_indicator(xhci_controller_state *hcState, const int port_index, const uint8_t value);
void xhci_port_set_u1_timeout(xhci_controller_state *hcState, const int port_index, const uint8_t value);
void xhci_port_set_u2_timeout(xhci_controller_state *hcState, const int port_index, const uint8_t value);
driverOperationResult xhci_port_set_link_state(xhci_controller_state *hcState, const int port_index, const uint8_t value);
driverOperationResult xhci_port_set_remote_wake_mask(xhci_controller_state *hcState, const int port_index, const uint8_t value);
void xhci_port_warm_reset(xhci_controller_state *hcState, const int port_index);
void xhci_port_set_flpma(xhci_controller_state *hcState, const int port_index);

/* Root hub port level clear_feature helper functions */
void xhci_port_usb2_disable(xhci_controller_state *hcState, const int port_index);
void xhci_port_usb2_clear_suspend(xhci_controller_state *hcState, const int port_index);
driverOperationResult xhci_port_clear_power(xhci_controller_state *hcState, const int port_index);
void xhci_port_clear_change(xhci_controller_state *hcState, const int port_index, const uint32_t flag);
void xhci_port_clear_lpma(xhci_controller_state *hcState, const int port_index);

#endif
