#ifndef XHCI_TRANSACTION_H_
#define XHCI_TRANSACTION_H_

#include <driverapi.h>
#include <usb.h>
#include "xhci_trb.h"
#include "xhci_struct.h"

#define USB_SETUP_LENGTH                 8U
#define USB_REQUEST_DEV2HOST             0x80U

#define SETUP_TRT_OUT                    2
#define SETUP_TRT_IN                     3

#define XHCI_MAX_TD_SIZE                 31

#define XHCI_TRANSACTION_IN              0x00U
#define XHCI_TRANSACTION_OUT             0x01U

#define XHCI_INIT_DESC_REQ               0x0100U

typedef struct COMPILER_PACKED
{
  volatile transferTRB *first_trb;
  sync_object *sync;
  volatile void *buffer;
  uint8_t trb_count, transfer_type;
  uint32_t flags;
  uint32_t total_size;
} activeTransaction;

typedef struct
{
  uint8_t bmRequestType;
  uint8_t bRequest;
  uint16_t wValue;
  uint16_t wIndex;
  uint16_t wLength;

  uint8_t *buffer;
} controlTransaction;

typedef struct
{
  uint8_t *buffer;
  uint32_t length;
  uint8_t flags;
} bulkIntTransaction;

busOperationResult xhci_queue_control_transaction(xhci_controller_state *hc, const uint8_t slot, const uint8_t endpoint, uint8_t direction, usbControlTransaction *ct, const uint32_t flags, sync_object *sync_info);
busOperationResult xhci_queue_normal_transaction(xhci_controller_state *hc, const uint8_t slot, const uint8_t endpoint, const uint8_t direction, usbControllerTransaction *transaction, const uint32_t flags, sync_object *sync);
void xhci_process_completed_transfer(xhci_controller_state *hcState, activeTransaction *request, volatile eventTRB *event, uint32_t residual_bytes, const uint8_t slot_id, const uint8_t ep_id);

driverOperationResult xhci_ring_control_backtrace(xhci_controller_state *hcState, ringQueue *rq, volatile transferTRB *start_trb);
#endif
