#include "xhci_bus.h"
#include "xhci_transaction.h"
#include "xhci_event.h"
#include "xhci_cq.h"
#include "xhci_trb.h"
#include "xhci_log.h"
#include "xhci_event.h"
#include "sync_queue.h"
#include "xhci_root_hub.h"
#include <usb.h>

#define XHCI_ENABLE_DEBUG_COMMANDS               1


#ifdef XHCI_ENABLE_DEBUG_COMMANDS

typedef struct
{
    sync_object *sync;
    sync_object *op_cb;
    xhci_controller_state *hcState;
    uint8_t slot;
    union
    {
        uint8_t endpoint_id;
    } command_specific;
} xhci_debug_command_request;

/*! Handles a Debug: stop endpoint request
 *
 *  \param context xhci_debug_command_request for the stop endpoint request
 *  \param status Status code from the HC event processor
 */
void xhci_handle_debug_stop_completion(void *context, uint64_t status)
{
    uint8_t slot, ep_id;
    uint64_t dq_ptr, mask;
    volatile endpointContext *epc;
    xhci_debug_command_request *req;
    xhci_controller_state *hcState;
    ringQueue *rq;
    volatile softwareIssuedTRB *trb;


    req = (xhci_debug_command_request *)context;
    hcState = req->hcState;

    slot = req->slot;
    ep_id = req->command_specific.endpoint_id;

    epc = (volatile endpointContext *)xhci_get_dev_context(hcState, slot, ep_id);
    rq = xhci_get_endpoint_ring_from_id(hcState, slot, ep_id);

    mask = XHCI_EP_DCS;
    mask = ~mask;

    dq_ptr = epc->trDequeuePtr & mask;

    xhci_get_completed_trb(&trb, dq_ptr, rq);

    /* Output information about the TRB being processed or that would be processed next */
    xhci_log(XHCI_LOG_INFO, "Debug stop completed for slot %u endpoint %u DQPTR: %p\nTRB[0]: %08X TRB[1]: %08X TRB[2]: %08X TRB[3]: %08X\n", epc->trDequeuePtr,
             trb->raw_dw[0], trb->raw_dw[1], trb->raw_dw[2], trb->raw_dw[3]);

    /* Signal the attached sync object if present */
    if(req->sync)
        sync_object_signal(req->sync, 1);

    /* Clean up the objects related to this request */
    sync_destroy_signal_obj(req->op_cb);
    free(req);
}

/*! Handler for a request to perform a Debug: Stop Endpoint operation
 *
 *  \param hcState XHCI context instance
 *  \param address Full address of the device
 *  \param ca Configure address structure indicating the endpoint of interest
 *  \param sync Synchronization object to be triggered once the operation has completed
 */
static int xhci_process_debug_stop_endpoint(xhci_controller_state *hcState, const uint64_t address, usbConfigureAddress *ca, sync_object *sync)
{
    uint8_t dev_address, endpoint, direction, slot, ep_dci, ep_state;
    xhci_slot_state *slot_state;
    volatile endpointContext *epc;
    xhci_debug_command_request *req;

    dev_address = USB_GET_LOCAL_ADDRESS(address);
    endpoint = USB_ADDR_GET_ENDPOINT(address);
    direction = USB_ADDR_GET_DIR(address);

    slot = xhci_get_slot_from_address(hcState, dev_address);
    if(!slot)
        return busOpAddressInvalid;

    slot_state = &hcState->slots[slot];

    ep_dci = xhci_calc_ep_dci(endpoint, direction, ENDPOINT_GET_TYPE(slot_state->endpoint_state[endpoint]) );

    epc = (volatile endpointContext *)xhci_get_dev_context(hcState, slot, ep_dci);

    ep_state = XHCI_EPC_GET_EP_STATE(epc->epState);

    /* Form the internal request structure for the debug: stop endpoint request */
    req = (xhci_debug_command_request *)calloc(1, sizeof(xhci_debug_command_request));

    req->sync = sync;
    req->hcState = hcState;
    req->slot = slot;
    req->command_specific.endpoint_id = ep_dci;

    sync_create_callback_obj(&req->op_cb, req, &xhci_handle_debug_stop_completion);

    /* Queue the stop endpoint request and bind the callback sync for the request and handler to it */
    xhci_queue_stop_endpoint(hcState, slot, ep_dci, req->op_cb);

    xhci_hc_signal_work_pending(hcState, 1);

    xhci_log(XHCI_LOG_OPSRES, "Stopping endpoint requested for slot %u endpoint %u direction %u current state %X\n", slot, endpoint, direction, ep_state);

    return busOpSuccess;
}

/*! Processes debug command requests to the controller if that capability is enabled by the XHCI_ENABLE_DEBUG_COMMANDS flag
 *  \param hcState XHCI Context instance
 *  \param address Full address of the debug command targets
 *  \param ca Configure address structure indicating the endpoints targeted by the request and/or debug request type
 *  \param sync Synchronization object to signal on completion of the operation
 *  \return Dependent on the requested command or busOpNotSupported is an unrecognized debug operation is requested
 */
static int xhci_process_debug_command(xhci_controller_state *hcState, const uint64_t address, usbConfigureAddress *ca, sync_object *sync)
{
    switch(ca->command_type)
    {
        case usbConfigDebugStopEndpoint:
            return xhci_process_debug_stop_endpoint(hcState, address, ca, sync);
            break;
        default:
            return busOpNotSupported;
            break;
    }
}

#else

/* Simply states that debugging operations are not supported if they are not enabled */
static int xhci_process_debug_command(xhci_controller_state *hcState, const uint64_t address, usbConfigureAddress *ca, sync_object *sync)
{
    return busOpNotSupported;
}


#endif

/*! Retrives the slot value that corresponds to a given address
 *  \param hcState Host controller instance context
 *  \param device_address USB device address to retrieve a slot value for
 *  \return The slot index for the device or zero if no addressed device corresponds to the provided address
 */
uint8_t xhci_get_slot_from_address(xhci_controller_state *hcState, const uint8_t device_address)
{
    uint8_t slot;
	if(device_address > USB_MAX_DEVICE_COUNT)
		return 0;
	slot = hcState->dev_to_slot_map[device_address];
    xhci_log(XHCI_LOG_INTERNAL, "Found slot %u in entry for address %u\n", device_address, slot);

    return slot;
}

/*! Bus address configuration handler
 *  \param hcState Host controller instance context
 *  \param address Full controller + device + endpoint address
 *  \param ca Configuration request
 *  \param sync Synchronization information structure
 *  \return busOpSuccess if the request was successfully dispatched.  busOp errors describing failure otherwise
 */
int xhci_update_endpoint(xhci_controller_state *hcState, const uint64_t address, usbConfigureAddress *ca, sync_object *sync)
{
	uint32_t slot;
    uint8_t cycleState;
	uintptr_t icc_phys;
	xhci_slot_state *slot_ptr;
	uint32_t ec_bitmask;
	volatile inputControlContext *icc;
	volatile endpointContext *ec, *current_ec;
	volatile commandTRB *cmd;
	busOperationResult status;

	slot = xhci_get_slot_from_address(hcState, USB_GET_LOCAL_ADDRESS(address) );
    /* Validate input */
	if(!slot || (slot > hcState->maxSlots) )
		return busOpAddressInvalid;

	slot_ptr = &hcState->slots[slot];

	ec_bitmask = 0;
	/* Setup the input context and endpoint context */
	icc = xhci_get_icc_context(hcState, slot, 0);
    icc_phys = getPhysBase((uintptr_t)icc);

	memset((void *)icc, 0, ARCH_PAGE_SIZE);

	icc->dropContext = 0;
	icc->configValue = 0;
	icc->altSetting = 0;
	icc->interfaceNum = 0;

    for(uint32_t endpoint_entry_index = 0; endpoint_entry_index < ca->device_info.num_ep_entries; endpoint_entry_index++)
    {
        uint32_t mask;
        uint8_t endpoint, dc_index;

        usbConfigureEndpointFields *fields = &ca->device_info.endpoints[endpoint_entry_index];
        endpoint = fields->ep_num;
        dc_index = xhci_calc_ep_dci(endpoint, fields->direction, fields->ep_type);

        ec = xhci_get_icc_context(hcState, slot, dc_index + 1);
        current_ec = xhci_get_dev_context(hcState, slot, dc_index );

        memcpy( (void *)ec, (void *)current_ec, sizeof(endpointContext));

        mask = 1;
        mask <<= dc_index;
        ec_bitmask |= mask;
        ec->epState = 0;
        ec->maxPacketSize = ca->device_info.endpoints[endpoint].packet_size;
    }

    icc->addContext = ec_bitmask;


    cmd = xhci_hc_get_trb(hcState, &cycleState);

	/* Setup the evaluate context command TRB */
	cmd->evaluteContext.input_context_addr = icc_phys;
	cmd->evaluteContext.trbType = WRITE_TRB_TYPE_ADJ(TRB_EVALUATE_CONTEXT);
	cmd->evaluteContext.slotId = slot;

	status = busOpSuccess;
	/* If a sync object is provided push it on the slot queue */
	if(sync)
    {
        if(sync_attach(sync))
            sync_queue_push(&slot_ptr->command_wait, cmd, sync);
        else
        {
            status = busOpSyncError;
            cmd->noOp.trbType = WRITE_TRB_TYPE_ADJ(TRB_NO_OP2);
        }
    }

    xhci_trb_set_cycle(cmd, cycleState);
	xhci_hc_signal_work_pending(hcState, 1);
	return status;
}

/*! Configures endpoints for a device
 *  \param context Host controller instance context
 *  \param address Full address of the device
 *  \param config_address Configuration request information
 *  \param sync Synchronization primitive
 */
static int xhci_config_device(void *context, const uint64_t address, usbConfigureAddress *config_address, sync_object *sync)
{
	xhci_controller_state *hcState;
	xhci_slot_state *slot_ptr;
    volatile inputControlContext *icc;
    volatile slotContext *slot_context;
    volatile commandTRB *config_ep_trb;
    int slot, max_ep_index, trb_count;
    uint32_t ep_mask;
    uint8_t config_cycle;
    busOperationResult status;

	hcState = (xhci_controller_state *)context;

	max_ep_index = 0;

	slot = xhci_get_slot_from_address(hcState, USB_GET_LOCAL_ADDRESS(address) );
	if(slot  & XHCI_DEV_IS_RH)
    {
        int rh_index;
        xhci_rh_state *rh_state;

        rh_index = slot & ~XHCI_DEV_IS_RH;
        if(rh_index > hcState->root_hub_count)
            return busOpAddressInvalid;

        rh_state = (xhci_rh_state *)hcState->root_hub_info[rh_index];
        return xhci_rh_config_device(hcState, rh_state, config_address, sync);
    }
	else if(!slot)
        return busOpInvalidArg;

    slot_ptr = &hcState->slots[slot];
    dapi_lock_acquire(slot_ptr->slot_lock);

    icc = xhci_get_icc_context(hcState, slot, 0);
    ep_mask = 0;

    /* If extended configuration information support is supported and enabled make sure to indication the configuration value that's being set here */
    if(hcState->caps->hccparams2 & XHCI_HCC2_CIC)
    {
        if(hcState->ops->configure & USB_CONFIGURE_CIE)
        {
            icc->configValue = config_address->device_info.value;
            icc->altSetting = config_address->device_info.alt_setting;
            icc->interfaceNum = config_address->device_info.interface;
        }
    }

    slot_context = xhci_get_icc_context(hcState, slot, 1);
    /* Update the index value for the maximum endpoint context entry and hub settings.
     * This couldn't be done in the initial configuration since we had no idea of the endpoint count
     * or what class of device was connected.
     */
    slot_context->slotState = 0;
    slot_context->usbDeviceAddress = 0;
    slot_context->interrupterAndTTT |= config_address->device_info.hub_info.tt;
    slot_context->numberOfPorts = config_address->device_info.hub_info.num_ports;
        /* Issue the actual configure endpoints command to host controller's command queue */
    config_ep_trb = xhci_hc_get_trb(hcState, &config_cycle);

    if(config_ep_trb)
    {
        config_ep_trb->configureEndpoint.icc_ptr = getPhysBase((uintptr_t)icc);
        config_ep_trb->configureEndpoint.trbType = WRITE_TRB_TYPE_ADJ(TRB_CONFIGURE_ENDPOINT);
        config_ep_trb->configureEndpoint.slotId = slot;


        trb_count = 1;
    }
    else
    {

        xhci_log(XHCI_LOG_ERRORS, "Unable to allocate TRB for endpoint configuration command\n");
        dapi_lock_release(slot_ptr->slot_lock);
        return busOpResources;
    }


    /* Traverse and configure each endpoint of the device */
    for(int endpoint_entry = 0; endpoint_entry < config_address->device_info.num_ep_entries; endpoint_entry++ )
    {
        usbConfigureEndpointFields *fields;
        ringQueue *ep_queue;
        volatile endpointContext *epc;
        int mult, maxBurst;
        uint32_t maxESIT;
        uint8_t ep_type_mask;
        uint32_t ep_index, ici;

        fields = &config_address->device_info.endpoints[endpoint_entry];

        ep_index = xhci_calc_ep_dci(fields->ep_num, fields->direction, fields->ep_type);
        if(ep_index > max_ep_index)
            max_ep_index = ep_index;

        xhci_log(XHCI_LOG_INTERNAL, "Configure endpoint entry for ep %u with direction %u and DCI: %u MDCI: %u\n", fields->ep_num, fields->direction, ep_index, max_ep_index);

        ici = xhci_calc_ep_ici(fields->ep_num, fields->direction, fields->ep_type);

        ep_mask |= (1U << ep_index);

        epc = (volatile endpointContext *)xhci_get_icc_context(hcState, slot, ici);
        memset((void *)epc, 0, hcState->devContextSize);

        epc->epState = 0;
        ep_type_mask = 0x1U<<fields->ep_type;

        /* XHCI Defined max burst size value as of rev 1.2 */
        switch(config_address->device_info.dev_speed)
        {
            default:
                maxBurst = 0;
                break;
            case USB_SPEED_HIGH:
                switch(fields->ep_type)
                {
                    case USB_EP_TYPE_CONTROL:
                    case USB_EP_TYPE_BULK:
                        maxBurst = 0;
                        break;
                    default:
                        maxBurst = fields->at_ops;
                        break;
                }
                break;
                    case USB_SPEED_SUPER:
            case USB_SPEED_ESUPER_1X1:
            case USB_SPEED_ESUPER_2X1:
            case USB_SPEED_ESUPER_1X2:
            case USB_SPEED_ESUPER_2X2:
                    maxBurst = fields->ess_max_burst;
                break;
        }

        epc->maxBurstSize = maxBurst;
        epc->maxPacketSize = fields->packet_size;
        /* Multi field and ESIT value vary based upon the value of the LEC flag */
        if( hcState->caps->hccparams2 & XHCI_HCC2_LEC )
        {
            epc->streamInfo = 0;
            maxESIT = 16U<<20;
            mult = (maxESIT/fields->packet_size)/(maxBurst + 1) - 1;
            epc->streamInfo = 0;
        }
        else
        {
            if(config_address->device_info.dev_speed >= USB_SPEED_SUPER)
            {
                if(fields->ep_type == USB_EP_TYPE_ISOCHRON)
                {
                    if(fields->ess_attr & USB_SS_EPC_ISOC)
                        mult = fields->ess_iso_mult;
                    else
                        mult = fields->ess_attr & 0x3;
                    maxESIT = fields->ess_iso_dwbpi;
                }
                else
                {
                    mult = 0;
                    maxESIT = fields->ess_wbpi;
                }

            }
            else
            {

                maxESIT = fields->packet_size;
                maxESIT *= (maxBurst+1);
                mult = 0;
            }
            epc->streamInfo = mult;
        }

        if(ep_type_mask & (USB_TYPE_FIELD_INTERRUPT | USB_TYPE_FIELD_ISOCHRON))
        {
            /* Set the Max ESIT fields if we're dealing with an interrupt or isochronous endpoint */
            if(hcState->caps->hccparams2 & XHCI_HCC2_LEC)
                epc->maxEsitPayloadHi = maxESIT>>16;
            else
                epc->maxEsitPayloadHi = 0;
            epc->maxESITPayloadLo = maxESIT & 0xFFFFU;
        }
        else
        {
            epc->maxEsitPayloadHi = 0;
            epc->maxESITPayloadLo = 0;
        }

        /* Create the transfer ring for the endpoint */
        if( xhci_create_ring(hcState, &ep_queue) != driverOpSuccess)
        {
            dapi_lock_release(slot_ptr->slot_lock);
            return busOpResources;
        }

        /* Attach the transfer ring queue to the ICC endpoint context entry and slot state's endpoint ring array */
        slot_ptr->endpoint_rings[ep_index - 1] = ep_queue;
        epc->trDequeuePtr = xhci_ring_get_base( ep_queue ) | XHCI_EP_DCS;
        ep_queue->cycleState = XHCI_EP_DCS;

        /* Set endpoint type, default average TRB value and interval value (if applicable) */
        switch(fields->ep_type)
        {
            /* TODO: High speed bulk and control endpoints can technically have a NAK rate dictated by the interval field */
            case USB_EP_TYPE_BULK:
                slot_ptr->endpoint_state[ep_index-1] = ENDPOINT_SET_TYPE(usbTransactionBulk) | ENDPOINT_CONFIGURING;
                /* Specification defined sane initial value */
                epc->averageTRBLength = 1024*3;

                if( fields->direction )
                {
                    xhci_log(XHCI_LOG_INTERNAL, "Initializing bulk input endpoint #%u for device @ %X\n", fields->ep_num, address);
                    epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_BULK_IN) | XHCI_EPC_CERR(3);
                }
                else
                {
                    xhci_log(XHCI_LOG_INTERNAL, "Initializing bulk output endpoint #%u for device @ %X\n", fields->ep_num, address);
                    epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_BULK_OUT) | XHCI_EPC_CERR(3);
                }

                break;
            case USB_EP_TYPE_CONTROL:
                xhci_log(XHCI_LOG_INTERNAL, "Initializing control endpoint #%u for device @ %X\n", fields->ep_num, address);
                slot_ptr->endpoint_state[ep_index-1] = ENDPOINT_SET_TYPE(usbTransactionControl) | ENDPOINT_CONFIGURING;
                slot_ptr->endpoint_state[ep_index-2] = ENDPOINT_SET_TYPE(usbTransactionControl) | ENDPOINT_CONFIGURING;
                epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_CONTROL) | XHCI_EPC_CERR(3);
                /* Specification defined sane initial value */
                epc->averageTRBLength = 8;
                break;
                /* Interrupt and isochronous endpoints require an interval value */
            case USB_EP_TYPE_INTERRUPT:
                slot_ptr->endpoint_state[ep_index-1] = ENDPOINT_SET_TYPE(usbTransactionInterrupt) | ENDPOINT_CONFIGURING;
                /* Specification defined sane initial value */
                epc->averageTRBLength = 1024;

                if( fields->direction )
                {
                    xhci_log(XHCI_LOG_INTERNAL, "Initializing interrupt input endpoint #%u for device @ %X bInterval: %u Speed: %u MB: %u Mult: %u\n",
                             fields->ep_num, address, fields->interval, config_address->device_info.dev_speed, maxBurst, mult);
                    epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_INTERRUPT_IN) | XHCI_EPC_CERR(3);
                }
                else
                {
                    xhci_log(XHCI_LOG_INTERNAL, "Initializing interrupt output endpoint #%u for device @ %X bInterval: %u Speed: %u MB: %u Mult: %u\n",
                             fields->ep_num, address, fields->interval, config_address->device_info.dev_speed, maxBurst, mult);
                    epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_INTERRUPT_OUT) | XHCI_EPC_CERR(3);
                }

                /* Low and full speed devices handle the calculation of the interval value different than High speed and ESS+ devices */
                switch(config_address->device_info.dev_speed)
                {
                    case USB_SPEED_LOW:
                    case USB_SPEED_FULL:
                        if(!fields->interval || (fields->interval > 255) )
                        {
                            dapi_lock_release(slot_ptr->slot_lock);
                            return busOpNotSupported;
                        }
                        else
                        {
                            uint8_t reg_value;
                            uint32_t interval_units = fields->interval;
                            interval_units *= 1000;
                            interval_units /= 125;

                            reg_value = getMsb32(interval_units);
                            if(reg_value < 3)
                                reg_value = 3;
                            epc->interval = reg_value;
                        }
                        break;
                    case USB_SPEED_HIGH:
                    case USB_SPEED_SUPER:
                    case USB_SPEED_ESUPER_1X1:
                    case USB_SPEED_ESUPER_1X2:
                    case USB_SPEED_ESUPER_2X1:
                    case USB_SPEED_ESUPER_2X2:
                        if(!fields->interval || (fields->interval > 16) )
                        {
                            dapi_lock_release(slot_ptr->slot_lock);
                            return busOpNotSupported;
                        }
                        else
                        {

                            uint8_t reg_value;

                            reg_value = fields->interval - 1;
                            epc->interval = reg_value;
                        }
                        break;
                }
                break;
            case USB_EP_TYPE_ISOCHRON:
                slot_ptr->endpoint_state[ep_index-1] = ENDPOINT_SET_TYPE(usbTransactionIsochronous) | ENDPOINT_CONFIGURING;
                /* Specification defined sane initial value */
                epc->averageTRBLength = 1024*3;
                if( fields->direction )
                {
                    xhci_log(XHCI_LOG_INTERNAL, "Initializing isochronous input endpoint #%u for device @ %X\n", fields->ep_num, address);
                    epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_ISO_IN) | XHCI_EPC_CERR(0);
                }
                else
                {
                    xhci_log(XHCI_LOG_INTERNAL, "Initializing isochronous output endpoint #%u for device @ %X\n", fields->ep_num, address);
                    epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_ISO_OUT) | XHCI_EPC_CERR(0);
                }

                if(fields->ep_type == USB_SPEED_FULL)
                {
                    if(!fields->interval || (fields->interval > 16) )
                    {
                        dapi_lock_release(slot_ptr->slot_lock);
                        return busOpNotSupported;
                    }
                    epc->interval = fields->interval + 2;
                }
                else if(!fields->interval || (fields->interval > 16) )
                {
                    dapi_lock_release(slot_ptr->slot_lock);
                    return busOpNotSupported;
                }
                else
                    epc->interval = fields->interval - 1;

                break;
        }

    }

    /* Update the input control and slot context with information about the endpoint structures that were modified */
    slot_ptr->ep_count = max_ep_index;
    slot_context->contextInfo = SET_SLOT_CONTEXT_ENTRIES(max_ep_index + 1, config_address->device_info.hub_info.hub_flags);
    icc->addContext = ep_mask | 0x01U;

    status = busOpSuccess;
    /* If a sync object is provided push it on the slot queue */
    if(sync)
    {
        if( sync_attach(sync) )
            sync_queue_push(&slot_ptr->command_wait, (volatile commandTRB *)config_ep_trb, sync);
        else
        {
            config_ep_trb->noOp.trbType = WRITE_TRB_TYPE_ADJ(TRB_NO_OP2);
            status = busOpSyncError;
        }
    }

    dapi_lock_release(slot_ptr->slot_lock);

    xhci_trb_set_cycle(config_ep_trb, config_cycle);
    xhci_hc_signal_work_pending(hcState, trb_count);

    return status;
}



/*! Retrieves the root port number a device is ultimately connected do
 *
 *  \param hcState XHCI controller context pointer
 *  \param slot Starting slot to search
 *  \param port Port the device is reported to be connected to
 *  \return A valid root hub port number
 */
int xhci_get_rh_port(xhci_controller_state *hcState, int slot, const int port)
{
    int rh_index, rh_port;
    xhci_rh_state *rh_info;
    xhci_slot_state *slot_info;

    if(slot & XHCI_DEV_IS_RH)
    {
        /* Extract the root hub index */
        rh_index = slot & ~XHCI_DEV_IS_RH;
        rh_info = (xhci_rh_state *)hcState->root_hub_info[rh_index];

        if(port > rh_info->port_count)
        {
            xhci_log(XHCI_LOG_ERRORS, "Error port offset of %u exceeds root hub port count of %u\n", port, rh_info->port_count);
            return 0;
        }
        else
            return (rh_info->controller_port_offset + port);
    }

    /* Cycle until a root hub entry is found */
    do
    {
        slot_info = &hcState->slots[slot];
        slot = slot_info->parent_slot;
    } while(!(slot & XHCI_DEV_IS_RH));


    /* Extract the root hub index */
    rh_index = slot & ~XHCI_DEV_IS_RH;

    if(rh_index >= hcState->root_hub_count)
    {
        xhci_log(XHCI_LOG_ERRORS, "Error found root hub index of %u while maximum hub count is %u\n", rh_index, hcState->root_hub_count);
        return 0;
    }

    rh_info = (xhci_rh_state *)hcState->root_hub_info[rh_index];

    rh_port = rh_info->controller_port_offset;
    rh_port += slot_info->parent_port;

    return rh_port;
}

/*! Creates a USB3 style routing string from a combination of parent slot and port number
 *
 *  \param hcState XHCI Host controller context pointer
 *  \param parent_slot Slot ID of the parent device
 *  \param port_num Port number a device is connected to on the parent device
 *
 *  \return A routing string in USB3 format that can be placed in the target device's slot context
 */

uint32_t xhci_form_route_string(xhci_controller_state *hcState, const uint8_t parent_slot, const uint8_t port_num)
{
    if(parent_slot & XHCI_DEV_IS_RH)
        return 0;
    else
    {
        slotContext *sc;
        uint32_t test_mask, offset, parent_route_string, dev_route_string;

        sc = (slotContext *)xhci_get_dev_context(hcState, parent_slot, 0);

        test_mask = 0xFU;
        parent_route_string = sc->routeString[2];
        parent_route_string &= 0x0FU;
        parent_route_string <<= 8;
        parent_route_string |= sc->routeString[1];
        parent_route_string <<= 8;
        parent_route_string |= sc->routeString[0];

        for(offset = 0; offset < 5; offset++, test_mask <<= 4)
        {
            if(!(test_mask & parent_route_string))
                break;
        }

        dev_route_string = port_num;
        dev_route_string <<= offset*4;

        dev_route_string |= parent_route_string;

        return dev_route_string;
    }
}

busOperationResult xhci_init_slot(xhci_controller_state *hcState, usbConfigureAddress *ca, sync_object *sync)
{
    uint8_t rh_port, parent_slot, psi_value;
    uint32_t route_string;

    parent_slot = xhci_get_slot_from_address( hcState, ca->connection_info.parent_addr );

    xhci_log(XHCI_LOG_INTERNAL, "Attempting to acquire root hub port from slot %X and subport %u\n", parent_slot, ca->connection_info.port_num);
    rh_port = xhci_get_rh_port(hcState, parent_slot, ca->connection_info.port_num);
    route_string = xhci_form_route_string(hcState, parent_slot, ca->connection_info.port_num);

    xhci_log(XHCI_LOG_INTERNAL, "Attempting to translated USB speed %u into PSI value for port %u\n", ca->connection_info.dev_speed, rh_port);
    psi_value = xhci_get_psi_from_speed(hcState, ca->connection_info.dev_speed, rh_port);

    xhci_log(XHCI_LOG_OPSRES, "Preparing device with parent slot %u Root hub port: %u Route String: %05X PSI value: %u Port: %u\n",
             parent_slot, rh_port, route_string, psi_value, ca->connection_info.port_num);
    xhci_hc_enable_slot(hcState, ca->connection_info.port_num, rh_port, ca->connection_info.dev_speed, psi_value, route_string, parent_slot, sync);

    return busOpSuccess;
}

/*! Bus address configuration handler
 * \param context Host controller state
 * \param address Address of the device, endpoint or interface to configure
 * \param config_info Pointer to configuration information
 * \param config_length Length of the configuration information structure
 * \param sync Synchronization information structure
 */
int xhci_config_address(void *context, const uint64_t address, void *config_info, const uint32_t config_length, sync_object *sync)
{
	xhci_controller_state *hcState;
	usbConfigureAddress *ca;

	hcState = (xhci_controller_state *)context;

	/* Make sure the configuration structure is a valid size */
	if(config_length < sizeof(usbConfigureAddress))
		return busOpInvalidArg;

	ca = (usbConfigureAddress *)config_info;


    /* Process the specific command request */
	switch(ca->command_type)
	{
		case usbConfigureDevice:
		    xhci_log(XHCI_LOG_OPSRES, "Configure device @ %X\n", address);
		    return xhci_config_device(context, address, ca, sync);
			break;
		case usbConfigureEndpoint:
			break;
		case usbConfigureEndpointUpdate:
		    xhci_log(XHCI_LOG_OPSRES, "Update endpoint @ %X\n", address);
			return xhci_update_endpoint(hcState, address, ca, sync);
			break;
        case usbAddressDevice:
            break;
        case usbPrepareDevice:
            xhci_log(XHCI_LOG_OPSRES, "Preparing device @ %X\n", address);
            return xhci_init_slot(hcState, ca, sync);
            break;
        default:
            if(ca->command_type >= usbConfigDebugCommandStart)
                return xhci_process_debug_command(hcState, address, ca, sync);
            break;

	}

	return busOpSuccess;
}

/*! Bus transaction request handler
 *  \param context Host controller state
 *  \param address Address of the device being accessed
 *  \param t_info  Transaction description
 *  \param t_info_length Size of the transaction descriptor
 *  \param sync Optional synchronization parameter
 *  \return busOpSuccess if successful, busOpInvalidArg if an invalid argument or transaction description was provided, busOpResources if too many transactions have been queued
 */
static int xhci_queue_transaction(void *context, const uint64_t address, void *t_desc, const uint32_t t_info_length, sync_object *sync)
{
	uint32_t device_address, controller_id, endpoint, direction, ep_dci;
	int slot, ep_type;
	//int ep_state;
	usbControllerTransaction *transaction;
	xhci_slot_state *slot_state;
	xhci_controller_state *hcState;
	volatile endpointContext *epc;


	hcState = (xhci_controller_state *)context;
	transaction = (usbControllerTransaction *)t_desc;

	controller_id = address/USB_MAX_ADDRESS;
	device_address = USB_GET_LOCAL_ADDRESS(address);
	endpoint = USB_ADDR_GET_ENDPOINT(address);
	direction = USB_ADDR_GET_DIR(address);

	slot = xhci_get_slot_from_address(hcState, device_address);

	/* Validate the address provided */
	if(!slot || (controller_id != hcState->controller_id) )
    {
        xhci_log(XHCI_LOG_ERRORS, "Unable to validate address %X Local: %u - Found slot %u Controller id: %u\n", address, device_address, slot, controller_id);
		return busOpAddressInvalid;
    }

    if(slot & XHCI_DEV_IS_RH)
    {
        return xhci_rh_queue_transaction(hcState, slot & ~XHCI_DEV_IS_RH, endpoint, direction, transaction, t_info_length, sync);
    }


    slot_state = &hcState->slots[slot];

	ep_dci = xhci_calc_ep_dci(endpoint, direction, transaction->type);
    epc = xhci_get_dev_context(hcState, slot, ep_dci);

    //ep_state = XHCI_EPC_GET_EP_STATE(epc->epState);
    ep_type = XHCI_EPC_GET_TYPE(epc->epTypeField);

    if(!epc)
    {
        xhci_log(XHCI_LOG_ERRORS, "Failed to retrieve context for requested endpoint %u direction %u of device @ %X\n", endpoint, direction, device_address );
        return busOpAddressInvalid;
    }

    /*
    if(ep_state != XHCI_EPC_EP_STATE_RUNNING)
    {
        xhci_log(XHCI_LOG_ERRORS, "Endpoint %u:%u for device @ %X is in an unpredicted state: %u\n", endpoint, direction, device_address, ep_state);
        return busOpIOError;
    }
    */

	if(endpoint > slot_state->ep_count )
    {
        xhci_log(XHCI_LOG_ERRORS, "Failed to validate endpoint index %u - Max index: %u\n", endpoint, slot_state->ep_count);
		return busOpAddressInvalid;
    }

	/* Validate the transaction request */
	if( t_info_length < sizeof(usbControllerTransaction))
    {
        xhci_log(XHCI_LOG_ERRORS, "QT - Transaction length invalid! Found: %u Minimum: %u\n", t_info_length, sizeof(usbControllerTransaction));
		return busOpInvalidArg;
    }

	if( transaction->type != (ENDPOINT_GET_TYPE(slot_state->endpoint_state[ep_dci-1])) )
    {
        xhci_log(XHCI_LOG_ERRORS, "QT - Endpoint index %u state invalid for device %X! Expected: %u Found: %u Raw: %X\n", ep_dci, address, transaction->type,
                 ENDPOINT_GET_TYPE(slot_state->endpoint_state[ep_dci - 1]), slot_state->endpoint_state[endpoint] );
        return busOpInvalidArg;
    }

    /* Process the request based upon the type of transaction being requested */
	switch(transaction->type)
	{
		case usbTransactionControl:
            if(ep_type != XHCI_EPC_EP_CONTROL)
            {
                xhci_log(XHCI_LOG_ERRORS, "Endpoint %u:%u on device @ %X was not of the control type found type %u instead\n", endpoint, direction, device_address, ep_type);
                return busOpAddressInvalid;
            }
			return xhci_queue_control_transaction(hcState, slot, endpoint, direction, &transaction->control, transaction->flags, sync);

			break;
		case usbTransactionBulk:
		    if(direction)
            {
                if(ep_type != XHCI_EPC_EP_BULK_IN)
                {
                    xhci_log(XHCI_LOG_ERRORS, "Endpoint %u:%u on device @ %X was not of the \'bulk out\' type found type %u instead\n", endpoint, direction, device_address, ep_type);
                    return busOpAddressInvalid;
                }
            }
            else if(ep_type != XHCI_EPC_EP_BULK_OUT)
            {
                xhci_log(XHCI_LOG_ERRORS, "Endpoint %u:%u on device @ %X was not of the \'bulk in\' type found type %u instead\n", endpoint, direction, device_address, ep_type);
                return busOpAddressInvalid;
            }

		    return xhci_queue_normal_transaction(hcState, slot, endpoint, direction, transaction, transaction->flags, sync);
			break;
		case usbTransactionInterrupt:
            if(direction)
            {
                if(ep_type != XHCI_EPC_EP_INTERRUPT_IN)
                {
                    xhci_log(XHCI_LOG_ERRORS, "Endpoint %u:%u on device @ %X was not of the \'interrupt in\' type found type %u instead\n", endpoint, direction, device_address, ep_type);
                    return busOpAddressInvalid;
                }
            }
            else if(ep_type != XHCI_EPC_EP_INTERRUPT_OUT)
            {
                xhci_log(XHCI_LOG_ERRORS, "Endpoint %u:%u on device @ %X was not of the \'interrupt out\' type found type %u instead\n", endpoint, direction, device_address, ep_type);
                return busOpAddressInvalid;
            }

		    return xhci_queue_normal_transaction(hcState, slot, endpoint, direction, transaction, transaction->flags, sync);
			break;
		case usbTransactionIsochronous:
		    return busOpNotSupported;
			break;
	}

	return busOpSuccess;
}

/*! Bus execute queue handler
 *  \param context Host controller context
 *  \param address Address of the device to execute the current transfer queue for
 *  \param sync Optional control flow handling upon completion of the transfer queue
 */
static int xhci_transaction_run(void *context, const uint64_t address, sync_object *sync)
{
	uint32_t device_address, controller_id, endpoint;
	int slot, doorbell_index, dir;
	xhci_slot_state *slot_state;
    volatile endpointContext *ec;
    ringQueue *queue;

	xhci_controller_state *hcState = (xhci_controller_state *)context;

	controller_id = address/USB_MAX_ADDRESS;
	device_address = (address%USB_MAX_ADDRESS)/USB_MAX_ENDPOINTS;
	endpoint = USB_ADDR_GET_ENDPOINT(address);
	dir = USB_ADDR_GET_DIR(address);

	slot = xhci_get_slot_from_address(hcState, device_address);

	/* Validate the address provided */
	if(!slot || (controller_id != hcState->controller_id) )
		return busOpAddressInvalid;

    if(slot & XHCI_DEV_IS_RH)
        return xhci_rh_run_transactions(hcState, slot & ~XHCI_DEV_IS_RH, endpoint, dir, sync);


    slot_state = &hcState->slots[slot];
	if(endpoint)
        doorbell_index = xhci_calc_ep_dci(endpoint, dir, ENDPOINT_GET_TYPE(slot_state->endpoint_state[endpoint*2 + dir - 1]) );
    else
        doorbell_index = 1;


    if( doorbell_index > slot_state->ep_count)
    {

        xhci_log(XHCI_LOG_ERRORS, "RT - Transaction request for device @ %X provided an invalid endpoint index of %u while the highest initialized endpoint index is %u\n",
                 address, endpoint, slot_state->ep_count);
		return busOpAddressInvalid;
    }

    if(ENDPOINT_GET_STATUS(slot_state->endpoint_state[doorbell_index - 1]) < ENDPOINT_CONFIGURING)
    {
        xhci_log(XHCI_LOG_ERRORS, "RT - Transaction request for device @ %X requested a transaction on endpoint %u which had an unusable state of %u\n",
                 address, endpoint, ENDPOINT_GET_STATUS(slot_state->endpoint_state[endpoint]) );
        return busOpAddressInvalid;
    }

    /* Examine the queue for the targeted endpoint to see if there's actually work to be done and ring the doorbell if there is */
    queue = xhci_get_endpoint_ring(hcState, slot, endpoint, dir);
    if(queue)
    {
        volatile slotContext *sc;

        sc = xhci_get_dev_context(hcState, slot, 0);
        dapi_lock_acquire(queue->rqLock);
        if(queue->commandsInFlight)
        {
            if( ENDPOINT_GET_STATUS(slot_state->endpoint_state[doorbell_index - 1]) == ENDPOINT_CONFIGURING )
                return busOpSuccess;

            int port_num;
            uint64_t link_state;
            port_num = sc->rootPortNum;
            link_state = XHCI_PORTSC_GET_PLS(hcState->ops->portRegSet[port_num - 1].portStatusControl);
            //volatile endpointContext *epc;
            hcState->dbs[slot] = doorbell_index;
            ec = (volatile endpointContext *)xhci_get_dev_context(hcState, slot, doorbell_index);
            xhci_log(XHCI_LOG_INTERNAL, "Ringing slot %u doorbell with value: %u Given request for endpoint: %u direction: %u SC State: %u PLS: %01X ECS: %01X RDQ: %p\n", slot, doorbell_index, endpoint, dir, (sc->slotState>>3), link_state, XHCI_EPC_GET_EP_STATE(ec->epState), queue->completedPtr);
            dapi_lock_release(queue->rqLock);
        }
        else
        {
            dapi_lock_release(queue->rqLock);
            xhci_log(XHCI_LOG_INFO, "Slot %u endpoint %u had no pending work!\n", slot, endpoint);
            /*
            return busOpAddressInvalid;
            */
        }
    }
    else
    {
        xhci_log(XHCI_LOG_ERRORS, "Error invalid end point requested for runQueue! Full address: %X EP: %u Direction: %u\n", address, endpoint, dir);
        return busOpAddressInvalid;
    }



	return busOpSuccess;
}

/*! Controller registration logic
 *  \param hcState Host controller state
 *  \param usb_major Requested USB Major version number
 *  \param usb_minor Requested USB Minor version number
 *  \return  busRegisterApproved if successful, busRegisterBlocked if the registration was blocked by the bus coordinator
 */
busRegisterResponse xhci_register_controller(xhci_controller_state *hcState, const uint8_t usb_major, const uint8_t usb_minor)
{
	uint32_t res_size;
	controller_id_response response;
	controller_id_req_msg id_req = {usb_major, usb_minor, 0};
	busRegDescriptor busReg;
	BUS_HANDLE controller_handle;
	res_size = sizeof(controller_id_response);

	memset(&busReg, 0, sizeof(busRegDescriptor));

	/* Get a controller ID from the bus coordinator */
	if( busMsg(&response, &res_size, BUS_ID_USB, 0, &id_req, sizeof(controller_id_req_msg), usbCoordControllerIdRequest, hcState->dev ) != busMsgAffirmative )
	{
		xhci_log(XHCI_LOG_ERRORS, "Failed to acquire controller id from bus coordinator!\n");
		return busRegisterBlocked;
	}

	/* Update the controller state with id and version information */
	hcState->controller_id = response.controller_id;
	hcState->usb_major_supported = response.coord_usb_major;
	hcState->usb_minor_supported = response.coord_usb_minor;

	/* Bus controller registration descriptor */
	busReg.busId = BUS_ID_USB;
	busReg.capabilities = BUS_CAPS_TRANSACTIONS | BUS_CAPS_CONFIG_ADDR;
	busReg.baseAddress = hcState->controller_id*USB_MAX_ADDRESS;
	busReg.length = 128*USB_MAX_ENDPOINTS;
	busReg.queueTransaction = &xhci_queue_transaction;
	busReg.runTransactions = &xhci_transaction_run;
	busReg.updateConfig = &xhci_config_address;
	busReg.contextPtr = hcState;


	if( registerBusRange(&busReg, &controller_handle, hcState->dev) == busOpSuccess )
	{
		hcState->controller_handle = controller_handle;
		return busRegisterApproved;
	}
	else
	{
		xhci_log(XHCI_LOG_ERRORS, "Failed to register bus range with controller!\n");
		return busRegisterBlocked;
	}
}

