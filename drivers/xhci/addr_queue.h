/* Address request queue header
 *
 * Author: Joseph Kinzel */

#ifndef ADDR_QUEUE_H_
#define ADDR_QUEUE_H_

void addr_queue_push(addr_queue *queue, const uint8_t value);
uint8_t addr_queue_pop(addr_queue *queue);
int addr_queue_empty(addr_queue *queue);

#endif // ADDR_QUEUE_H_
