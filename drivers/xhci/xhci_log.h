/* Logging and output control for the Obsidian XHCI driver
 *
 * Author: Joseph Kinzel */

#ifndef XHCI_LOG_H_
#define XHCI_LOG_H_

#define XHCI_LOG_NONE           0
/* Enables the logging of errors */
#define XHCI_LOG_ERRORS                     0x01U
/* Enables the logging/output of basic informational events (connect/disconnect, device enumeration, controller information, etc.) */
#define XHCI_LOG_INFO                       0x02U
/* Enables the logging of any other high level operations not captured by XHCI_LOG_INFO */
#define XHCI_LOG_BASIC                      0x04U
/* Enables the logging of all operations and responses to them */
#define XHCI_LOG_OPSRES                     0x08U
/* Enables low level logging of the internal operations of the driver */
#define XHCI_LOG_INTERNAL                   0x10U

#define XHCI_LOG_WARNINGS                   0x20U

#define XHCI_LOG_ALL                        (XHCI_LOG_INFO | XHCI_LOG_ERRORS | XHCI_LOG_BASIC | XHCI_LOG_OPRES | XHCI_LOG_INTERNALS)

/* THIS ESTABLISHES LOGGING CONDITIONS */
#define XHCI_CONDITIONS                     (XHCI_LOG_ERRORS | XHCI_LOG_INFO | XHCI_LOG_WARNINGS | XHCI_LOG_BASIC)

#define xhci_log(condition, ...)            if(condition & XHCI_CONDITIONS) \
                                                printf("[XHCI]: " __VA_ARGS__);


#endif // XHCI_LOG_H_
