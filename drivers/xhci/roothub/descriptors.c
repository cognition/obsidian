/* XHCI Root hub descriptor handling functions
 *
 * Author: Joseph Kinzel
 */

#include "../xhci_log.h"
#include "../xhci_struct.h"
#include "../xhci_root_hub.h"
#include <driverapi.h>

#define XHCI_RH_STRING_MANUFACTURER           1
#define XHCI_RH_STRING_PRODUCT                2
#define XHCI_RH_STRING_SERIAL                 3

#define XHCI_RH_STRING_CONFIGURATION          4
#define XHCI_RH_STRING_INTERFACE              5

static const uint8_t xhci_rh_usb2_device_desc[0x12] =
{
    0x12,         /* bLength */
    0x01,         /* bDescriptorType */
    0x10, 0x2,    /* bcdUSB */
    0x09,         /* bDeviceClass, Hub */
    0x00,         /* bDeviceSubClass */
    0x01,         /* bDeviceProtocol, Single Transaction Translator */
    0x40,         /* bMaxPacketSize0, 64 byte maximum packet size */

    0x6b,
    0x1d,         /* Linux Foundation */

    0x03,
    0x00,         /* 3.0 Root hub */

    0x00,
    0x01,         /* Device release number in BCD */

    XHCI_RH_STRING_MANUFACTURER ,
    XHCI_RH_STRING_PRODUCT,
    XHCI_RH_STRING_SERIAL,


    0x01          /* bNumConfigurations */
};

static const uint8_t xhci_rh_usb3_device_desc[0x12] =
{
    0x12,         /* bLength */
    0x01,         /* bDescriptorType */
    0x20, 0x03,   /* bcdUSB */
    0x09,         /* bDeviceClass, Hub */
    0x00,         /* bDeviceSubClass */
    0x03,         /* bDeviceProtocol */
    0x09,         /* bMaxPacketSize0, 512 bytes */

    0x6b,
    0x1d,         /* Linux Foundation */

    0x03,
    0x00,         /* 3.0 Root hub */

    0x00,
    0x01,         /* Product version */

    XHCI_RH_STRING_MANUFACTURER ,
    XHCI_RH_STRING_PRODUCT,
    XHCI_RH_STRING_SERIAL,

    0x01         /* bNumConfigurations */
};

static const uint8_t xhci_rh_usb2_config_desc[25] =
{
    /* Configuration descriptor */

    0x09,                                /* bLength */
    0x02,                                /* bDescriptorType, Configuration Descriptor */
    (0x9 + 0x9 + 0x7), 0,                /* wTotalLength */
    0x01,                                /* bNumInterfaces */
    0x01,                                /* bConfigurationValue */
    XHCI_RH_STRING_CONFIGURATION,        /* iConfiguration */
    0x60,                                /* bmAttributes, bus powered, remote wakeup */
    0x00,                                /* bMaxPower */

    /* Hub interface descriptor */

    0x09,                                /* bLength */
    0x04,                                /* bDescriptorType, Interface Descriptor */
    0x00,                                /* bInterfaceNumber */
    0x00,                                /* bAlternateSetting */
    0x01,                                /* bNumEndpoints, one endpoint, the status endpoint */
    0x09,                                /* bInterfaceClass, Hub interface */
    0x00,                                /* bInterfaceSubClass */
    0x00,                                /* bInterfaceProtocol, Multiple transaction translators */
    XHCI_RH_STRING_INTERFACE,            /* iInterface, Index of the string descriptor describing the interface */

    /* Status endpoint descriptor */

    0x07,                                /* bLength */
    0x05,                                /* bDescriptorType, Endpoint descriptor */
    0x81,                                /* bEndpointAddress, Input endpoint #1 */
    0x03,                                /* bmAttributes, Interrupt endpoint */
    0x04, 0x00,                          /* wMaxPacketSize, 4 bytes */
    0x0C                                 /* bInterval, 12 -> 2^12 -> 4096 -> 512ms */
};

static const uint8_t xhci_rh_usb3_config_desc[31] =
{
    /* Configuration descriptor */

    0x09,                                /* bLength */
    0x02,                                /* bDescriptorType, Configuration Descriptor */
    31, 0,                               /* wTotalLength */
    0x01,                                /* bNumInterfaces */
    0x01,                                /* bConfigurationValue */
    XHCI_RH_STRING_CONFIGURATION,        /* iConfiguration */
    0x60,                                /* bmAttributes, bus powered, remote wakeup */
    0x00,                                /* bMaxPower */

    /* Hub interface descriptor */

    0x09,                                /* bLength */
    0x04,                                /* bDescriptorType, Interface Descriptor */
    0x00,                                /* bInterfaceNumber */
    0x00,                                /* bAlternateSetting */
    0x01,                                /* bNumEndpoints, one endpoint, the status endpoint */
    0x09,                                /* bInterfaceClass, Hub interface */
    0x00,                                /* bInterfaceSubClass */
    0x00,                                /* bInterfaceProtocol */
    XHCI_RH_STRING_INTERFACE,            /* iInterface, Index of the string descriptor describing the interface */

    /* Status endpoint descriptor */

    0x07,                                /* bLength */
    usbDescEndpoint,                     /* bDescriptorType, Endpoint descriptor */
    0x81,                                /* bEndpointAddress, Input endpoint #1 */
    0x13,                                /* bmAttributes, Interrupt endpoint */
    0x04, 0x00,                          /* wMaxPacketSize, 4 bytes */
    0x0C,                                /* bInterval, 12 -> 2^12 -> 4096 -> 512ms */

    /* Status endpoint SuperSpeed companion descriptor */
    0x06,                                /* bLength */
    usbDescSSEC,                         /* bDescriptorType, SuperSpeed Endpoint Companion */
    0,                                   /* bMaxBurst, only one packet burst */
    0,                                   /* bmAttributes, not used by interrupt endpoints */
    0x04,
    0x00                                 /* wBytesPerInterval */
};

static uint8_t xhci_rh_usb2_bos_desc[25] =
{
    /* BOS header */
    5,                                   /* bLength */
    usbDescBOS,                          /* bDescriptorType */
    25, 0,                               /* wTotalLength */
    1,                                   /* bNumDeviceCaps */

    /* Container ID */
    20,                                  /* bLength */
    usbDescDeviceCapability,             /* bDescriptorType */
    devQualContainerId,                  /* bDevCapabilityType */
    0,                                   /* bReserved */

    0, 0, 0, 0, 0, 0, 0, 0,              /* UUID */
    0, 0, 0, 0, 0, 0, 0, 0
};

static const int usb30_bos_length = 20 + 5 + 10;

static uint8_t xhci_rh_usb3_bos_desc[71] =
{
    /* BOS header */
    5,                                   /* bLength */
    usbDescBOS,                          /* bDescriptorType */
    71, 0,                               /* wTotalLength */
    3,                                   /* bNumDeviceCaps */

    /* Container ID */
    20,                                  /* bLength */
    usbDescDeviceCapability,             /* bDescriptorType */
    devQualContainerId,                  /* bDevCapabilityType */
    0,                                   /* bReserved */

    0, 0, 0, 0, 0, 0, 0, 0,              /* UUID */
    0, 0, 0, 0, 0, 0, 0, 0,

    /* SuperSpeed Capability */
    10,                                  /* bLength */
    usbDescDeviceCapability,             /* bDescriptorType */
    devQualSuperSpeedUSB,                /* bDevCapabilityType */
    0x02,                                /* bmAttributes */

    0x0F,                                /* wSpeedsSupported, all speeds */
    0x00,

    0x03,                                /* bFunctionalitySupport */

    0x00,                                /* bU1DevExitLat, Zero worst-case latency */

    0x00,                                /* wU2DevExitLat, Zero worst-case latency */
    0x00,

    /* SuperSpeedPlus USB Device Capability */

    36,                                  /* bLength */
    usbDescDeviceCapability,             /* bDescriptorType */
    devQualSuperspeedPlus,               /* bDevCapabilityType */
    0x00,                                /* bReserved */

    0x45,                                /* bmAttributes, 32-bit field, 6 lane attributes and 3 ids */
    0x00,
    0x00,
    0x00,

    0x00,                                /* wFunctionalitySupported,  Minimum land speed id is zero */
    0x11,                                /* wFunctionalitySupported, Minimum TX and RX lane counts of 1 */

    /* First SSA */
    0x32,                                /* ID: 0, Units: Gb/s, Symmetric, Receive Mode */
    0x00,                                /* SuperSpeed protocol */

    0x05,
    0x00,                                /* Lane speed mantissa: 5 */
    /* Second SSA */
    0xB2,                                /* ID: 0, Units: Gb/s, Symmetric, Transmit Mode */
    0x00,                                /* SuperSpeed protocol */

    0x05,
    0x00,                                /* Lane speed mantissa: 5 */


    /* Third SSA */
    0x31,                                /* ID: 1, Units: Gb/s, Symmetric, Receive Mode */
    0x40,                                /* SuperSpeedPlus protocol */

    0x05,
    0x00,                                /* Lane speed mantissa: 5 */
    /* Fouth SSA */
    0xB1,                                /* ID: 1, Units: Gb/s, Symmetric, Transmit Mode */
    0x40,                                /* SuperSpeedPlus protocol */

    0x05,
    0x00,                                /* Lane speed mantissa: 5 */
    /* Fifth SSA */
    0x32,                                /* ID: 2, Units: Gb/s, Symmetric, Receive Mode */
    0x40,                                /* SuperSpeedPlus protocol */

    0x0A,
    0x00,                                /* Lane speed mantissa: 10 */
    /* Sixth SSA */
    0xB2,                                /* ID: 2, Units: Gb/s, Symmetric, Transmit Mode */
    0x40,                                /* SuperSpeedPlus protocol */

    0x0A,
    0x00                                 /* Lane speed mantissa: 10 */

};

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint16_t w_lang_ids[];
} usbStringDesc;

/* String descriptors */
static const usbStringDesc xhci_rh_langid_desc = {4, usbDescString, {USB_LANGID_ENGLISH_US}};
static const usbStringDesc xhci_rh_manufacturer_str_desc = {2 + sizeof(u"Obsidian OS") , usbDescString, {u"Obsidian OS"}};
static const usbStringDesc xhci_rh_serial_str_desc = {2 + sizeof(u"0"), usbDescString, {u"0"}};
static const usbStringDesc xhci_rh_product_usb2_str_desc = {2 + sizeof(u"USB 2.x Root Hub"), usbDescString, {u"USB 2.x Root Hub"}};
static const usbStringDesc xhci_rh_product_usb3_str_desc = {2 + sizeof(u"USB 3.x Root Hub"), usbDescString, {u"USB 3.x Root Hub"}};
static const usbStringDesc xhci_rh_config_str_desc = {2 + sizeof(u"Default"), usbDescString, {u"Default"} };
static const usbStringDesc xhci_rh_interface_str_desc = {2 + sizeof(u"USB Hub"), usbDescString , {u"USB Hub"}};

/*! Hub class descriptor GET_DESCRIPTOR processing
 *
 *  \param rh_state Root hub context pointer
 *  \param req Control transaction request
 *
 *  \return usb_op_success or a stall if the request was in some way incorrect
 */
usb_hw_op_result xhci_rh_process_class_get_descriptor(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    uint8_t descriptor_type, descriptor_index;
    const void *desc_ptr;
    uint16_t desc_max_length;

    descriptor_type = req->w_value>>8;
    descriptor_index = req->w_value & 0xFFU;

    /* Request sanity check */
    if(req->w_index || descriptor_index)
        return usb_op_setup_stall;

    /* SuperSpeed hubs use a different descriptor format and index. */
    if(rh_state->hub_generation == PORT_PROTO_USB20)
    {
        if(descriptor_type != usbDescHub)
            return usb_op_status_stall;
    }
    else if(descriptor_type != usbDescESSHub)
        return usb_op_status_stall;

    if(rh_state->hub_generation == PORT_PROTO_USB20)
    {
        desc_ptr = (uint8_t *)rh_state->usb2_hub_desc;
        desc_max_length = rh_state->usb2_hub_desc->bLength;
    }
    else
    {
        desc_ptr = (uint8_t *)rh_state->usb3_hub_desc;
        desc_max_length = rh_state->usb3_hub_desc->bLength;
    }

    /* Copy the actual descriptor out to the target buffer */
    if(req->w_length < desc_max_length)
        memcpy((uint8_t *)req->data_ptr, desc_ptr, req->w_length);
    else
        memcpy( (uint8_t *)req->data_ptr, desc_ptr, desc_max_length);

    return usb_op_success;
}

/*! GET_DESCRIPTOR request handling
 *
 *  \param rh_state Root Hub state
 *  \param req Control transaction request
 *
 *  \return usb_op_success if the transaction succeeded and usb_op_stall on a request error
 */
usb_hw_op_result xhci_rh_device_get_descriptor(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    uint8_t descriptor_type, descriptor_index;
    const void *desc_ptr;
    uint16_t desc_max_length;

    descriptor_type = req->w_value>>8;
    descriptor_index = req->w_value & 0xFFU;

    if(!req->data_ptr || !req->w_length)
    {

        xhci_log(XHCI_LOG_ERRORS, "RH - GET_DESCRIPTOR request without a data buffer pointer or buffer length!\n");
        return usb_op_setup_stall;
    }

    switch(descriptor_type)
    {
        /* Device descriptor */
        case usbDescDevice:
            xhci_log(XHCI_LOG_INTERNAL, "RH - Attempting to retrieve root hub device descriptor! WI: %u DI: %u\n", req->w_index, descriptor_index);

            if(req->w_index || descriptor_index)
                return usb_op_setup_stall;

            switch(rh_state->hub_generation)
            {
                case PORT_PROTO_USB20:
                    desc_ptr = xhci_rh_usb2_device_desc;
                    desc_max_length = sizeof(xhci_rh_usb2_device_desc);
                    break;
                case PORT_PROTO_USB30:
                    if(req->w_length >= sizeof(xhci_rh_usb3_device_desc))
                    {
                        memcpy(req->data_buffer, xhci_rh_usb3_device_desc, 2);
                        req->data_buffer[2] = 0;
                        memcpy(&req->data_buffer[3], &xhci_rh_usb3_device_desc[3], sizeof(xhci_rh_usb3_device_desc) - 3);
                        return usb_op_success;
                    }
                    else if(req->w_length >= 3)
                    {
                        memcpy(req->data_buffer, xhci_rh_usb3_device_desc, 2);
                        req->data_buffer[2] = 0;
                        memcpy(&req->data_buffer[3], &xhci_rh_usb3_device_desc[3], req->w_length - 3);
                        return usb_op_success;
                    }
                    else
                    {
                        desc_ptr = xhci_rh_usb3_device_desc;
                        desc_max_length = sizeof(xhci_rh_usb3_device_desc);
                    }
                    break;
                case PORT_PROTO_USB31:
                    if(req->w_length >= sizeof(xhci_rh_usb3_device_desc))
                    {
                        memcpy(req->data_buffer, xhci_rh_usb3_device_desc, 2);
                        req->data_buffer[2] = 0x10;
                        memcpy(&req->data_buffer[3], &xhci_rh_usb3_device_desc[3], sizeof(xhci_rh_usb3_device_desc) - 3);
                        return usb_op_success;
                    }
                    else if(req->w_length >= 3)
                    {
                        memcpy(req->data_buffer, xhci_rh_usb3_device_desc, 2);
                        req->data_buffer[2] = 0x10;
                        memcpy(&req->data_buffer[3], &xhci_rh_usb3_device_desc[3], req->w_length - 3);
                        return usb_op_success;
                    }
                    else
                    {
                        desc_ptr = xhci_rh_usb3_device_desc;
                        desc_max_length = sizeof(xhci_rh_usb3_device_desc);
                    }
                    break;
                case PORT_PROTO_USB32:
                    desc_ptr = xhci_rh_usb3_device_desc;
                    desc_max_length = sizeof(xhci_rh_usb3_device_desc);
                    break;
                default:
                    return usb_op_status_stall;
                    break;
            }
            break;
        /* Configuration descriptor */
        case usbDescConfig:
            xhci_log(XHCI_LOG_INTERNAL, "RH - Attempting to retrieve root hub configuration descriptor!\n");

            if(req->w_index || descriptor_index)
                return usb_op_setup_stall;

            if(rh_state->hub_generation == PORT_PROTO_USB20)
            {
                desc_ptr = xhci_rh_usb2_config_desc;
                desc_max_length = sizeof(xhci_rh_usb2_config_desc);
            }
            else
            {
                desc_ptr = xhci_rh_usb3_config_desc;
                desc_max_length = sizeof(xhci_rh_usb3_config_desc);
            }
            break;
        /* String descriptors */
        case usbDescString:
            if(req->w_index)
                return usb_op_setup_stall;
            switch(descriptor_index)
            {
                case 0:
                    desc_ptr = &xhci_rh_langid_desc;
                    desc_max_length = sizeof(xhci_rh_langid_desc);
                    break;
                case XHCI_RH_STRING_MANUFACTURER:
                    desc_ptr = &xhci_rh_manufacturer_str_desc;
                    desc_max_length = sizeof(xhci_rh_manufacturer_str_desc);
                    break;
                case XHCI_RH_STRING_PRODUCT:
                    if(rh_state->hub_generation == PORT_PROTO_USB20)
                    {
                        desc_ptr = &xhci_rh_product_usb2_str_desc;
                        desc_max_length = sizeof(xhci_rh_product_usb2_str_desc);
                    }
                    else
                    {
                        desc_ptr = &xhci_rh_product_usb3_str_desc;
                        desc_max_length = sizeof(xhci_rh_product_usb3_str_desc);
                    }
                    break;
                case XHCI_RH_STRING_SERIAL:
                    desc_ptr = &xhci_rh_serial_str_desc;
                    desc_max_length = sizeof(xhci_rh_serial_str_desc);
                    break;
                case XHCI_RH_STRING_CONFIGURATION:
                    desc_ptr = &xhci_rh_config_str_desc;
                    desc_max_length = sizeof(xhci_rh_config_str_desc);
                    break;
                case XHCI_RH_STRING_INTERFACE:
                    desc_ptr = &xhci_rh_interface_str_desc;
                    desc_max_length = sizeof(xhci_rh_interface_str_desc);
                    break;
                default:
                    return usb_op_status_stall;
                    break;
            }
            break;
        /* Binary Object store */
        case usbDescBOS:
            switch(rh_state->hub_generation)
            {
                case PORT_PROTO_USB20:
                    desc_ptr = xhci_rh_usb2_bos_desc;
                    desc_max_length = sizeof(xhci_rh_usb2_bos_desc);
                    break;
                case PORT_PROTO_USB30:
                    if(req->w_length >= usb30_bos_length)
                    {
                        memcpy(req->data_buffer, xhci_rh_usb3_bos_desc, 2);
                        req->data_buffer[2] = usb30_bos_length;
                        memcpy(&req->data_buffer[3], &xhci_rh_usb3_bos_desc, usb30_bos_length - 3);
                        return usb_op_success;
                    }
                    else if(req->w_length >= 3)
                    {
                        memcpy(req->data_buffer, xhci_rh_usb3_bos_desc, 2);
                        req->data_buffer[2] = usb30_bos_length;
                        memcpy(&req->data_buffer[3], &xhci_rh_usb3_bos_desc, req->w_length - 3);
                        return usb_op_success;
                    }
                case PORT_PROTO_USB31:
                case PORT_PROTO_USB32:
                    desc_ptr = xhci_rh_usb3_bos_desc;
                    desc_max_length = sizeof(xhci_rh_usb3_bos_desc);
                    break;
                default:
                    return usb_op_status_stall;
                    break;
            }
            break;
        default:
            xhci_log(XHCI_LOG_ERRORS, "RH - Unrecognized descriptor type requested: %X\n", descriptor_type);
            return usb_op_setup_stall;
            break;
    }

    if(req->w_length < desc_max_length)
        memcpy((uint8_t *)req->data_ptr, desc_ptr, req->w_length);
    else
        memcpy( (uint8_t *)req->data_ptr, desc_ptr, desc_max_length);

    return usb_op_success;
}
