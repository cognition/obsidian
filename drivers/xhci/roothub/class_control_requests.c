/* USB Hub class specific control transaction processing functions
 *
 * Author: Joseph Kinzel */

#include "../xhci_root_hub.h"
#include "../xhci_log.h"
#include <usb.h>
#include <driverapi.h>

/* USB Hub class specific requests */
#define USB_HUB_REQ_CLEAR_TT_BUFFER           8
#define USB_HUB_REQ_RESET_TT                  9
#define USB_HUB_REQ_GET_TT_STATE              10
#define USB_HUB_REQ_STOP_TT                   11

/*! Processes a SET_FEATURE request with the hub as a target
 *
 *  \param rh_state Pointer to the root hub context
 *  \param req Pointer to the control request
 *
 *  \return usb_op_success if successful, usb_op_status_stall if the request is flawed
 */
usb_hw_op_result xhci_rh_set_hub_feature(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    //TODO:  Finish me!
    if(req->w_index)
        return usb_op_status_stall;
    switch(req->w_value)
    {
        case UHUB_HS_C_LOCAL_POWER:
            break;
        case UHUB_HS_C_OVER_CURRENT:
            break;
        default:
            return usb_op_status_stall;
            break;
    }
    return usb_op_success;
}

usb_hw_op_result xhci_rh_clear_hub_feature(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    return usb_op_success;
}

/*! Sets a root hub port specific feature
 *
 *  \param rh_state Pointer to the root hub context
 *  \param req Pointer to the control request
 */
usb_hw_op_result xhci_rh_set_port_feature(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    int value;
    xhci_controller_state *hcState;
    status_reg_type *port_status;
    int port_base = req->w_index & 0xFFU;
    int port_index = port_base;

    /* Validate the high level request format */
    if((port_index > rh_state->port_count) || !port_index)
    {
        xhci_log(XHCI_LOG_ERRORS, "RH - Set port feature - Invalid port index!  Received: %u  Max port: %u\n", port_index, rh_state->port_count);
        return usb_op_status_stall;
    }

    if(req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST)
    {
        xhci_log(XHCI_LOG_ERRORS, "RH - Set port feature - Bad request type: %X\n", req->bm_request_type);
        return usb_op_setup_stall;
    }

    hcState = (xhci_controller_state *)rh_state->hc_link;
    port_status = &rh_state->port_status[port_index-1];
    port_index += rh_state->controller_port_offset;

    value = req->w_index>>8;

    switch(req->w_value)
    {
        case UHUB_PF_LOW_SPEED:
        case UHUB_PF_ENABLE:
            /* No ops */
            break;
        case UHUB_PF_SUSPEND:
            if(rh_state->hub_generation == PORT_PROTO_USB20)
            {
                xhci_port_set_suspend(hcState, port_index);
                port_status->status |= UHUB_PS_SUSPEND;
            }
            else
                return usb_op_status_stall;
            break;
        case UHUB_PF_RESET:
            xhci_log(XHCI_LOG_INFO, "RH - Resetting port @ %X:%u Controller port: %u\n", rh_state->bus_addr, port_base, port_index);
            port_status->status |= UHUB_PS_RESET;
            xhci_port_set_reset(hcState, port_index);
            break;
        case UHUB_PF_POWER:
            xhci_port_set_power(hcState, port_index);
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                port_status->status |= UHUB_PS_USB2_POWER;
            else
                port_status->status |= UHUB_PS_USB3_POWER;
            break;
        case UHUB_PF_INDICATOR:
            /* Return a request error if this is a USB3.0+ hub which doesn't support the call
             * or if the set indicator function indicates an invalid selector was used */
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_status_stall;
            else if( xhci_port_set_indicator(hcState, port_index, value) != driverOpSuccess)
                return usb_op_status_stall;

            break;
        case UHUB_PF_TEST:
            /* TODO: Implement me! Currently disabled since port test mode can only be exited by resetting the entire host controller */
            if(rh_state->hub_generation == PORT_PROTO_USB20)
            {

            }
            else
            {

            }
            return usb_op_status_stall;
            break;
        case UHUB_PF_U1_TIMEOUT:
            if(rh_state->hub_generation >= PORT_PROTO_USB30)
                xhci_port_set_u1_timeout(hcState, port_index, value);
            break;
        case UHUB_PF_U2_TIMEOUT:
            if(rh_state->hub_generation >= PORT_PROTO_USB30)
                xhci_port_set_u2_timeout(hcState, port_index, value);
            break;
        case UHUB_PF_LINK_STATE:
            if(rh_state->hub_generation >= PORT_PROTO_USB30)
                xhci_port_set_link_state(hcState, port_index, value);
            break;
        case UHUB_PF_REMOTE_WAKE_MASK:
            if(rh_state->hub_generation >= PORT_PROTO_USB30)
                xhci_port_set_remote_wake_mask(hcState, port_index, value);
            break;
        case UHUB_PF_BH_RESET:
            if(rh_state->hub_generation >= PORT_PROTO_USB30)
                xhci_port_warm_reset(hcState, port_index);
            break;
        /* USB3.1+ selectors */
        case UHUB_PF_FORCE_LINKPM:
            if(rh_state->hub_generation >= PORT_PROTO_USB31)
                xhci_port_set_flpma(hcState, port_index);
            break;

        default:
            xhci_log(XHCI_LOG_ERRORS, "RH - Set port feature - Unrecognized request value %u\n", req->w_value);
            return usb_op_status_stall;
            break;
    }

    return usb_op_success;
}

/*! Clears a port feature
 *
 *  \param hcState Host controller context
 *  \param rh_state State of the root hub
 *  \param req Control transaction request
 *
 */
usb_hw_op_result xhci_rh_clear_port_feature(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{
    status_reg_type *port_status;
    int port_index = req->w_index & 0xFFU;

    if(!port_index || (port_index > rh_state->port_count))
        return usb_op_status_stall;
    if(req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST)
        return usb_op_setup_stall;

    port_status = &rh_state->port_status[port_index-1];
    port_index += rh_state->controller_port_offset;

    switch(req->w_value)
    {
        /* USB2.0 exclusive selectors */
        case UHUB_PF_ENABLE:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
            {
                port_status->status &= ~UHUB_PS_ENABLE;
                xhci_port_usb2_disable(hcState, port_index);
            }
            break;

        case UHUB_PF_SUSPEND:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
            {
                xhci_port_usb2_clear_suspend(hcState, port_index);
            }
            break;

        case UHUB_PF_C_ENABLE:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
            {
                port_status->change_status &= ~UHUB_PS_C_ENABLE;
                xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_PEC);
            }
            break;

        case UHUB_PF_C_SUSPEND:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
            {
                port_status->change_status &= ~UHUB_PS_C_SUSPEND;
                xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_PLC);
            }
            break;

        case UHUB_PF_INDICATOR:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
            {
                //TODO: Finish me!
                xhci_port_set_indicator(hcState, port_index, USB_PORT_INDICATOR_OFF);
            }
            break;
        /* Shared selectors */
        case UHUB_PF_POWER:
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                port_status->status &= ~UHUB_PS_USB2_POWER;
            else
                port_status->status &= ~UHUB_PS_USB3_POWER;
            xhci_port_clear_power(hcState, port_index);
            break;

        case UHUB_PF_C_CONNECTION:
            port_status->change_status &= ~UHUB_PS_CONNECTION;
            xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_CSC);
            break;

        case UHUB_PF_C_RESET:
            port_status->change_status &= ~UHUB_PS_C_RESET;
            xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_PRC);
            break;

        case UHUB_PF_C_OVER_CURRENT:
            port_status->change_status &= ~UHUB_PS_C_OC;
            xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_OCC);
            break;

        /* USB3.0+ selectors */
        case UHUB_PF_C_LINK_STATE:
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;

            port_status->change_status &= ~UHUB_PS_C_LINK_STATE;
            xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_PLC);
            break;

        case UHUB_PF_C_CONFIG_ERROR:
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;

            port_status->change_status &= ~UHUB_PS_C_CONFIG_ERR;
            xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_CEC);
            break;

        case UHUB_PF_C_BH_RESET:
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;

            port_status->change_status &= ~UHUB_PS_C_BH_RESET;
            xhci_port_clear_change(hcState, port_index, XHCI_PORTSC_WRC);
            break;

        /* USB3.1+ selectors */
        case UHUB_PF_FORCE_LINKPM:
            if(rh_state->hub_generation <= PORT_PROTO_USB30)
                return usb_op_status_stall;

            xhci_port_clear_lpma(hcState, port_index);
            break;
    }

    return usb_op_success;
}

/*! Overall function for processing hub and port specific control requests
 *
 *  \param hcState XHCI controller context pointer
 *  \param rh_state Pointer to the root hub context
 *  \param req Pointer to the control request
 *
 *  \return usb_op_success if the transaction was processed successfully or a stall value dependent on the request.
 */
usb_hw_op_result xhci_rh_process_class_control_transaction(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{
    switch(req->b_request)
    {
        case USB_CONTROL_REQ_CLEAR_FEATURE:
            /* Hub and port variants */
            if((req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST) || req->w_length || req->w_index || req->data_addr)
                return usb_op_setup_stall;

            switch( USB_REQUEST_RECIPIENT_GET(req->bm_request_type) )
            {
                case USB_REQ_TYPE_DEVICE:
                    /* Hub clear feature */
                    return xhci_rh_clear_hub_feature(rh_state, req);
                    break;
                case USB_REQ_TYPE_OTHER:
                    /* Port clear feature */
                    return xhci_rh_clear_port_feature(hcState, rh_state, req);
                    break;
                default:
                    return usb_op_setup_stall;
                    break;
            }

            break;
        case USB_CONTROL_REQ_GET_DESCRIPTOR:
            /* Hub variant */
            return xhci_rh_process_class_get_descriptor(rh_state, req);
            break;
        case USB_CONTROL_REQ_SET_FEATURE:
            /* Port and hub variants */
            if((req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST) || req->w_length || req->data_addr)
                return usb_op_setup_stall;

            switch( USB_REQUEST_RECIPIENT_GET(req->bm_request_type) )
            {
                case USB_REQ_TYPE_DEVICE:
                    /* Hub set feature */
                    return xhci_rh_set_hub_feature(rh_state, req);
                    break;
                case USB_REQ_TYPE_OTHER:
                    /* Port set feature */
                    return xhci_rh_set_port_feature(rh_state, req);
                    break;
                default:
                    return usb_op_setup_stall;
                    break;
            }

            break;
        case USB_CONTROL_REQ_GET_STATUS:

            /* Hub and port variants */
            switch( USB_REQUEST_RECIPIENT_GET(req->bm_request_type) )
            {
                case USB_REQ_TYPE_DEVICE:
                    xhci_log(XHCI_LOG_INTERNAL, "RH - Hub status request.\n");
                    /* Hub get status */
                    if(req->w_length != 4)
                        return usb_op_status_stall;

                    if(req->w_index || req->w_value)
                        return usb_op_status_stall;

                    memcpy(req->data_buffer, &rh_state->hub_status, 4);

                    return usb_op_success;
                    break;
                case USB_REQ_TYPE_OTHER:
                    /* Port get status */
                    if(req->w_index > rh_state->port_count)
                        return usb_op_status_stall;
                    if(rh_state->hub_generation <= PORT_PROTO_USB30)
                    {
                        if(req->w_length != 4)
                            return usb_op_status_stall;
                        if(req->w_value)
                            return usb_op_status_stall;
                        memcpy(req->data_buffer, &rh_state->port_status[req->w_index-1], 4);
                        return usb_op_success;
                    }
                    else
                    {
                        switch(req->w_value)
                        {
                            case USB_PSTATUS_PORT:
                                if(req->w_length != 4)
                                    return usb_op_status_stall;
                                memcpy(req->data_buffer, &rh_state->port_status[req->w_index], 4);
                                return usb_op_success;
                                break;
                            case USB_PSTATUS_PD:
                                /* TOOD: Check USB3.1 specification and USB PD specification to see what needs to be done here */
                                return usb_op_status_stall;
                                break;
                            case USB_PSTATUS_EXT:
                                if(req->w_length != 8)
                                    return usb_op_status_stall;

                                memcpy(req->data_buffer, &rh_state->port_ext_status[req->w_index], 4);
                                memset(&req->data_buffer[4], 0, 4);

                                return usb_op_success;
                                break;
                            default:
                                return usb_op_status_stall;
                                break;
                        }
                    }
                    break;
                default:
                    return usb_op_setup_stall;
                    break;
            }

        break;

        /* Class specific requests the hub responds to */
        case USB_HUB_REQ_RESET_TT:

            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_setup_stall;
            if(req->w_length || (req->w_index != 1) || req->w_value)
                return usb_op_status_stall;

            rh_state->tt_state = xhci_rh_tt_running;

            return usb_op_success;
            break;

        case USB_HUB_REQ_CLEAR_TT_BUFFER:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_setup_stall;
            if(req->w_length || (req->w_index != 1))
                return usb_op_status_stall;

            return usb_op_success;
            break;

        case USB_HUB_REQ_GET_TT_STATE:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_setup_stall;


            return usb_op_success;
            break;
        case USB_HUB_REQ_STOP_TT:
            if(rh_state->hub_generation != PORT_PROTO_USB20)
                return usb_op_setup_stall;

            if(req->w_length || (req->w_index != 1) || req->w_value)
                return usb_op_status_stall;

            rh_state->tt_state = xhci_rh_tt_stopped;

            return usb_op_success;
            break;

        default:
            return usb_op_setup_stall;
            break;
    }

}
