/* XHCI Root Hub control transaction handling
 *
 * Author: Joseph Kinzel */

#include "../xhci_log.h"
#include "../xhci_struct.h"
#include "../xhci_root_hub.h"
#include <driverapi.h>

busOperationResult xhci_rh_config_device(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbConfigureAddress *config_address, sync_object *sync)
{

    for(int endpoint_index = 0; endpoint_index < config_address->device_info.num_ep_entries; endpoint_index++)
    {
        usbConfigureEndpointFields *ep_field_ptr;

        ep_field_ptr = &config_address->device_info.endpoints[endpoint_index];

        switch(ep_field_ptr->ep_num)
        {
            case 0:
                return busOpInvalidArg;
                break;
            case XHCI_RH_STATUS_EP:
                if( (ep_field_ptr->ep_type != USB_EP_TYPE_INTERRUPT ) || (!ep_field_ptr->direction)
                   || (ep_field_ptr->packet_size != 4) || (ep_field_ptr->interval != 12)) {
                       xhci_log(XHCI_LOG_ERRORS, "RH - Invalid configuration selected for status endpoint on root hub.  Type: %X DIR: %X PS: %u Int: %u\n",
                                ep_field_ptr->ep_type, ep_field_ptr->direction, ep_field_ptr->packet_size, ep_field_ptr->interval);

                        return busOpInvalidArg;
                   }


                break;
            default:
                return busOpInvalidArg;
                break;
        }
    }

    if(sync)
        sync_object_signal(sync, 1);

    xhci_log(XHCI_LOG_INTERNAL, "RH - Successfully configured endpoints for root hub!\n");

    return busOpSuccess;
}


/*! Sets a device level feature
 *
 *  \param rh_state Root hub context
 *  \param req Control transaction information
 *
 *  \return usb_op_success if successful, an appropriate stall if the request was invalid
 */
static usb_hw_op_result xhci_rh_set_device_feature(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    if(rh_state->hub_operational_state == xhci_rh_default)
        return usb_op_status_stall;
    if(req->w_length || (req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST) || req->data_ptr)
        return usb_op_setup_stall;

    switch(req->w_value)
    {
        case USB_DEV_FS_REMOTE_WAKEUP:
            if(req->w_index)
                return usb_op_setup_stall;
            rh_state->dev_status |= USB_DEV_STATUS_REMOTE_WAKEUP;
            break;
        case USB_DEV_FS_TEST_MODE:
            /* Not applicable to a root hub */
            return usb_op_status_stall;
            break;
        case USB_DEV_FS_U1_ENABLE:
            /* Not applicable to a root hub */
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
                rh_state->dev_status |= USB_DEV_STATUS_U1_ENABLE;
            break;
        case USB_DEV_FS_U2_ENABLE:
            /* Not applicable to a root hub */
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
                rh_state->dev_status |= USB_DEV_STATUS_U2_ENABLE;
            break;
        case USB_DEV_FS_LTM_ENABLE:
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;
            else if(rh_state->rh_flags & XHCI_HCC1_LTC)
            {
                /* Only enable LTM if the host controller supports it */
                rh_state->dev_status |= USB_DEV_STATUS_LTM_ENABLE;
            }
            else
                return usb_op_status_stall;
            break;
        default:
            return usb_op_status_stall;
            break;
    }

    return usb_op_success;
}

usb_hw_op_result xhci_rh_clear_device_feature(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    switch(req->w_value)
    {
        case USB_DEV_FS_REMOTE_WAKEUP:
            if(req->w_index)
                return usb_op_setup_stall;
            rh_state->dev_status |= USB_DEV_STATUS_REMOTE_WAKEUP;
            break;
        case USB_DEV_FS_TEST_MODE:
            /* Not applicable to a root hub */
            return usb_op_status_stall;
            break;
        case USB_DEV_FS_U1_ENABLE:
            /* Not applicable to a root hub */
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
                rh_state->dev_status &= ~USB_DEV_STATUS_U1_ENABLE;
            break;
        case USB_DEV_FS_U2_ENABLE:
            /* Not applicable to a root hub */
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;
            else
                rh_state->dev_status &= ~USB_DEV_STATUS_U2_ENABLE;
            break;
        case USB_DEV_FS_LTM_ENABLE:
            if(rh_state->hub_generation == PORT_PROTO_USB20)
                return usb_op_status_stall;
            else if(rh_state->rh_flags & XHCI_HCC1_LTC)
            {
                rh_state->dev_status &= ~USB_DEV_STATUS_LTM_ENABLE;
            }
            else
                return usb_op_status_stall;
            break;
        default:
            return usb_op_status_stall;
            break;
    }


    return usb_op_success;
}

usb_hw_op_result xhci_rh_set_endpoint_feature(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    return usb_op_success;
}

usb_hw_op_result xhci_rh_clear_endpoint_feature(xhci_rh_state *rh_state, usbControlTransaction *req)
{
    return usb_op_success;
}

/*! GET_CONFIGURATION request handling
 *
 *  \param hcState Host controller state
 *  \param rh_state Root Hub state
 *  \param req Control transaction request
 *
 *  \return usb_op_success if the transaction succeeded and usb_op_stall on a request error
 */
usb_hw_op_result xhci_rh_get_configuration(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{
    volatile uint8_t *buffer;
    /* Check for request error conditions */
    if(req->w_length != 1)
        return usb_op_setup_stall;
    if(req->w_index || req->w_value || !(req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST) )
        return usb_op_setup_stall;

    buffer = (volatile uint8_t *)req->data_ptr;

    /* Request format is valid, handle based on the operational state of the root hub */
    switch(rh_state->hub_operational_state)
    {
        case xhci_rh_default:
            return usb_op_status_stall;
            break;
        case xhci_rh_addressed:
            buffer[1] = 0;
            break;
        case xhci_rh_configured:
            buffer[0] = 1;
            break;
    }

    return usb_op_success;
}

/*! SET_CONFIGURATION request handler
 *
 *  \param hcState Host controller state
 *  \param rh_state Root Hub state
 *  \param req Control transaction request
 *
 *  \return usb_op_success if the transaction succeeded and usb_op_stall on a request error
 */
usb_hw_op_result xhci_rh_set_configuration(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{
    if(req->w_length || req->w_index || (req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST))
        return usb_op_setup_stall;

    xhci_log(XHCI_LOG_INTERNAL, "RH - Attempting to set configuration for device @ %X\n", rh_state->bus_addr);

    switch(rh_state->hub_operational_state)
    {
        case xhci_rh_default:
            return usb_op_status_stall;
            break;
        case xhci_rh_addressed:

            if(req->w_value == 1)
                rh_state->hub_operational_state = xhci_rh_configured;
            else if(req->w_value > 1)
                return usb_op_setup_stall;

            return usb_op_success;
        case xhci_rh_configured:
            if(req->w_value > 1)
                return usb_op_setup_stall;
            else if(!req->w_value)
                rh_state->hub_operational_state = xhci_rh_addressed;
            return usb_op_success;
            break;
    }

}

static usb_hw_op_result xhci_rh_get_ep_status(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{
    volatile uint16_t *ret_buffer_ptr = (volatile uint16_t *)req->data_ptr;
    if(rh_state->hub_operational_state != xhci_rh_configured)
    {
        if(req->w_index)
            return usb_op_status_stall;
    }

    if(req->w_value)
    {
        if(rh_state->hub_generation > PORT_PROTO_USB30)
        {
            if( (req->w_value == USB_DEV_STATUS_REQ_PTM) && (req->w_length == 4))
            {
                volatile uint32_t *ptm_buffer_ptr = (volatile uint32_t *)req->data_ptr;

                *ptm_buffer_ptr = 0;

                return usb_op_success;
            }
        }
        return usb_op_status_stall;
    }

    if(req->w_value || (req->w_length != 2))
        return usb_op_setup_stall;

    /* Requests are only valid the device is configured or the endpoint is zero in the addressed state*/
    switch(req->w_index)
    {
        case USB_DEFAULT_CONTROL_EP:
            *ret_buffer_ptr = rh_state->control_ep_status;
            break;
        case XHCI_RH_STATUS_EP:
            *ret_buffer_ptr = rh_state->status_ep_status;
            break;
        default:
            return usb_op_status_stall;
            break;
    }

    return usb_op_success;
}

/*! Gets the status of a root hub device or the interfaces and endpoints under it
 *
 *  \param hcState Host controller state
 *  \param rh_state Root hub state
 *  \param req Control transaction information
 *
 *  \return usb_op_success if the request was valid, usb_op_stall if there is a request error or undefined behavior
 */
static usb_hw_op_result xhci_rh_process_standard_get_status(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{

    volatile uint16_t *ret_buffer_ptr;

    if( !(req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST) || (rh_state->hub_operational_state == xhci_rh_default) )
        return usb_op_setup_stall;

    ret_buffer_ptr = (volatile uint16_t *)req->data_ptr;
    switch( USB_REQUEST_RECIPIENT_GET(req->bm_request_type) )
    {
        case USB_REQ_TYPE_DEVICE:
            /* There's only a single status returned by the device */
            if(req->w_index)
                return usb_op_status_stall;
            if(req->w_value && (rh_state->hub_generation <= PORT_PROTO_USB30) )
                return usb_op_status_stall;
            else if((req->w_value == USB_DEV_STATUS_REQ_PTM) && (req->w_length == 4))
            {
                volatile uint32_t *ptm_status_ptr = (volatile uint32_t *)req->data_ptr;
                *ptm_status_ptr = 0;
                return usb_op_success;
            }

            if(req->w_length != 2)
                return usb_op_status_stall;

            *ret_buffer_ptr = rh_state->dev_status;
            break;
        case USB_REQ_TYPE_ENDPOINT:
            return xhci_rh_get_ep_status(hcState, rh_state, req);
            break;
        case USB_REQ_TYPE_INTERFACE:
            if(req->w_value || (req->w_length != 2))
                return usb_op_status_stall;
            /* There's only one interface, interface zero, for hubs all others are undefined behavior */
            if(req->w_index)
                return usb_op_status_stall;
            /* It's always reserved zero according the USB specification */
            *ret_buffer_ptr = 0;
            break;
        default:
            return usb_op_status_stall;
            break;
    }

    return usb_op_success;
}

static usb_hw_op_result xhci_rh_process_standard_control_request(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{
    switch(req->b_request)
    {
        /* Standard request values the root hub responds to or variants that share that value */
        case USB_CONTROL_REQ_CLEAR_FEATURE:
            if(req->bm_request_type & USB_REQ_TYPE_DEV_TO_HOST)
                return usb_op_setup_stall;
            else
                return xhci_rh_process_standard_get_status(hcState, rh_state, req);
            break;
        case USB_CONTROL_REQ_GET_CONFIG:
            /* Standard */
            return xhci_rh_get_configuration(hcState, rh_state, req);
            break;
        case USB_CONTROL_REQ_GET_DESCRIPTOR:
            /* Standard */
            return xhci_rh_device_get_descriptor(rh_state, req);
            break;
        case USB_CONTROL_REQ_GET_STATUS:
            return xhci_rh_process_standard_get_status(hcState, rh_state, req);
            break;
        case USB_CONTROL_REQ_SET_ADDRESS:
            /* Shouldn't really receive this directly */
            return usb_op_success;
            break;
        case USB_CONTROL_REQ_SET_CONFIG:
            return xhci_rh_set_configuration(hcState, rh_state, req);
            break;
        case USB_CONTROL_REQ_SET_FEATURE:
            /* Standard */
            return xhci_rh_set_device_feature(rh_state, req);
            break;
        default:
            return usb_op_setup_stall;
            break;
    }
}

usb_hw_op_result xhci_rh_process_control_transaction(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req)
{
    switch( USB_REQUEST_TYPE_GET(req->bm_request_type) )
    {
        case USB_BMRT_CLASS:
            return xhci_rh_process_class_control_transaction(hcState, rh_state, req);
            break;
        case USB_BMRT_STANDARD:
            return xhci_rh_process_standard_control_request(hcState, rh_state, req);
            break;
        default:
            return usb_op_setup_stall;
            break;
    }
}
