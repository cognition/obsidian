#include "xhci_controller.h"

/*! Allocates and initializes a mapped page of the architecture's default page size
 *  \param trbRegion Region output object
 *  \param hcState Host controller state object
 *  \return driverOpBadArg if the output object is null, driverOpSuccess if successful
 */
driverOperationResult allocate_mapped_page(mappedRegion *trbRegion, xhci_controller_state *hcState)
{
	driverOperationResult status;
	if(!trbRegion)
		return driverOpBadArg;
	status = allocatePhysRegion(&trbRegion->virtualBase, &trbRegion->physicalBase, ARCH_PAGE_SIZE, (hcState->pageSize == ARCH_PAGE_SIZE) ? 0:hcState->pageSize, 0, hcState->dev);
	if(status != driverOpSuccess)
		return status;

	memset((void *)trbRegion->ptrs, 0, ARCH_PAGE_SIZE);
	return driverOpSuccess;
}

static inline int max(const int first, const int second)
{
	return (first > second ? first:second);
}

static short int getHeight(physInstance *node)
{
	return max(node->lHeight, node->rHeight);
}

static short int getBalance(physInstance *node)
{
	return (node->lHeight - node->rHeight);
}

static short int rotateLeft(physInstance **entry, physInstance *p)
{
	physInstance *initR = p->right;
	p->right = initR->left;
	p->rHeight = initR->lHeight;

	initR->left = p;
	initR->lHeight = getHeight(p);
	*entry = initR;

	return getHeight(initR);
}

static short int rotateRight(physInstance **entry, physInstance *p)
{
	physInstance *initL = p->left;
	p->left = initL->right;
	p->lHeight = initL->rHeight;

	initL->right = p;
	initL->rHeight = getHeight(p);
	*entry = initL;

	return getHeight(initL);
}

static short int rotateLR(physInstance **entry, physInstance *p)
{
	p->lHeight = rotateLeft(&p->left, p->left);
	return rotateRight(entry, p);
}

static short int rotateRL(physInstance **entry, physInstance *p)
{
	p->rHeight = rotateRight(&p->right, p->right);
	return rotateLeft(entry, p);
}


static short int checkLeftBalance(short int nLeftHeight, physInstance **entry, physInstance *p)
{
	short int balance = nLeftHeight - p->rHeight;
	if(balance == 2)
	{
		short int lBalance = getBalance(p->left);
		if(lBalance == 1)
		{
			//Left Left condition
			return rotateRight(entry, p);
		}
		else
		{
			//Left Right condition
			return rotateLR(entry, p);
		}
	}
	else
	{
		p->lHeight = nLeftHeight;
		return getHeight(p);
	}
}

static short int checkRightBalance(short int nRightHeight, physInstance **entry, physInstance *p)
{
	short int balance = p->lHeight - nRightHeight;
	if(balance == -2)
	{
		short int rBalance = getBalance(p->right);
		if(rBalance == -1)
		{
			//Right right condition
			return rotateLeft(entry, p);
		}
		else
		{
			//Right Left condition
			return rotateRL(entry, p);
		}
	}
	else
	{
		p->rHeight = nRightHeight;
		return getHeight(p);
	}
}

static short int recursive_insert(physInstance **entry, physInstance *p, physInstance *node)
{
	if(!*entry)
	{
		*entry = node;
		return 1;
	}
	else if(node->region->region.physicalBase < p->region->region.physicalBase)
    {
        if(!p->left)
        {
            int height;
            p->left = node;
            p->lHeight = 1;
            height = max(1, p->rHeight) + 1;
            return height;
        }
        else
            return checkLeftBalance(recursive_insert(&p->left, p->left, node), entry, p);
    }
	else
    {
        if(!p->right)
        {
            int height;
            p->right = node;
            p->rHeight = 1;
            height = max(1, p->lHeight) + 1;
            return height;
        }
        else
            return checkRightBalance(recursive_insert(&p->right, p->right, node), entry, p);
    }
}

short int xhci_event_tree_insert(physInstance **root, physInstance *node)
{
	node->lHeight = 0;
	node->rHeight = 0;
	node->left = NULL;
	node->right = NULL;
	return recursive_insert(root, *root, node);
}

int xhci_event_tree_find(physInstance **instance, physInstance *root, const uint64_t address)
{
	physInstance *currentInstance = root;
	while(currentInstance)
	{
		mappedRegion *region = &currentInstance->region->region;
		if(address < region->physicalBase)
			currentInstance = currentInstance->left;
		else if(address >= (region->physicalBase + ARCH_PAGE_SIZE))
			currentInstance = currentInstance->right;
		else
		{
			*instance = currentInstance;
			return 1;
		}
	}

	*instance = NULL;
	return 0;
}
