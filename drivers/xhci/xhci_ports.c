/* XHCI Port parsing and support functions
 *
 * Author: Joseph Kinzel */

#include "xhci_ports.h"
#include "xhci_event.h"
#include "xhci_memory.h"
#include "xhci_log.h"
#include "xhci_root_hub.h"
#include <usb.h>

/*! Parses and analyzes the protocol capability
 *  \param hcState Host controller instance context
 *  \param cap Capability pointer
 */
driverOperationResult process_xhci_protocol_capability(xhci_controller_state *hcState, xhci_supported_protocol *cap)
{
    driverOperationResult res;
	port_generation_type port_proto;
	xhci_port_info *port_ptr;
	if(cap->name_string != XHCI_PROTO_NS_USB)
		return driverOpVersionMismatch;

	if( ((cap->port_offset + cap->port_count - 1) > hcState->maxPorts) || !cap->port_offset)
	{
		xhci_log(XHCI_LOG_ERRORS, "Protocol information contains invalid port range of %u-%u when maximum number of ports is %u\n",
		       cap->port_offset, cap->port_offset + cap->port_count - 1, hcState->maxPorts);
		return driverOpBadState;
	}

	if(cap->revision_major == 0x3U)
	{
		/* USB 3.x Port information */
		switch(cap->revision_minor)
		{
			case 0:
				port_proto = PORT_PROTO_USB30;
				/* USB 3.0 Protocol info */
				xhci_log(XHCI_LOG_BASIC, "Found USB3.0 Protocol information. For ports %u to %u and %u speeds\n", cap->port_offset, cap->port_offset+cap->port_count, XHCI_GET_PSIC(cap->protocol_info)  );
				break;
			case 0x10:
				port_proto = PORT_PROTO_USB31;
				/* USB 3.1 Protocol info */
				xhci_log(XHCI_LOG_BASIC, "Found USB3.1 Port information. For ports %u to %u and %u speeds\n", cap->port_offset, cap->port_offset+cap->port_count, XHCI_GET_PSIC(cap->protocol_info));
				break;
			case 0x20:
				port_proto = PORT_PROTO_USB32;
				/* USB 3.2 Protocol info */
				xhci_log(XHCI_LOG_BASIC, "Found USB3.2 Port information. For ports %u to %u and %u speeds\n", cap->port_offset, cap->port_offset+cap->port_count, XHCI_GET_PSIC(cap->protocol_info));
				break;
			default:
				xhci_log(XHCI_LOG_WARNINGS, "Protocol information contains unknown USB3 revision number - %u.%u\n", cap->revision_major, cap->revision_minor);
				return driverOpVersionMismatch;
				break;
		}
	}
	else if(cap->revision_major == 0x2U)
	{
		/* USB 2.0  Port information */
		xhci_log(XHCI_LOG_BASIC, "Found USB2.0 Port information. For ports %u to %u\n", cap->port_offset, cap->port_offset+cap->port_count-1);
		port_proto = PORT_PROTO_USB20;
	}
	else
	{
		xhci_log(XHCI_LOG_WARNINGS, "Protocol information contains unknown revision number: %u.%u\n", cap->revision_major, cap->revision_minor);
		return driverOpVersionMismatch;
	}

	res = xhci_rh_add_hub(hcState, cap->port_offset, cap->port_count, port_proto, cap->protocol_speed_ids, XHCI_GET_PSIC(cap->protocol_info) );
	if(res != driverOpSuccess)
    {
        xhci_log(XHCI_LOG_ERRORS, "Unable to register root hub for ports %u to %u\n", cap->port_offset, cap->port_offset + cap->port_count - 1);
        return res;
    }

	/* Populate the speed information for all ports */
	port_ptr = &hcState->port_info[cap->port_offset - 1];
	for(int port_index = 0; port_index < cap->port_count; port_ptr++, port_index++)
	{
		int count = XHCI_GET_PSIC(cap->protocol_info);
		port_ptr->protocol = port_proto;
		port_ptr->count = count;
		port_ptr->speeds = !count ? NULL:&cap->protocol_speed_ids[0];
		port_ptr->slot_type = cap->protocol_slot_type;
	}

	return driverOpSuccess;
}

/*! Translates a XHCI Supported Capability speed entry into a KB/s bandwidth value
 *
 *  \param port_speed_entry Speed entry value
 *  \return Bandwidth of the entry in Kb/s
 */
uint64_t xhci_get_psi_bandwidth(const uint32_t port_speed_entry)
{
    uint64_t bandwidth;

    bandwidth = XHCI_PSI_GET_MANTISSA(port_speed_entry);
    switch( XHCI_PSI_GET_EXPONENT(port_speed_entry) )
    {
        case 0:
            bandwidth /= 1000;
            break;
        case XHCI_PSI_EX_KB:
            bandwidth *= 1;
            break;
        case XHCI_PSI_EX_MB:
            bandwidth *= 1000;
            break;
        case XHCI_PSI_EX_GB:
            bandwidth *= 1000000;
            break;
    }
    xhci_log(XHCI_LOG_INTERNAL, "Got bandwidth of %u kbps from protocol entry %X\n", bandwidth, port_speed_entry);
    return bandwidth;
}

/*! Translates a provided USB speed type into an XHCI specification defined default value
 *
 *  \param dev_speed USB speed value to translate
 *  \return A coressponding PSI value or zero if the speed cannot be translated
 */
static int xhci_get_implied_psi_from_speed(const uint16_t dev_speed)
{
    switch(dev_speed)
    {
        case USB_SPEED_FULL:
            return 1;
            break;
        case USB_SPEED_LOW:
            return 2;
            break;
        case USB_SPEED_HIGH:
            return 3;
            break;
        case USB_SPEED_SUPER:
            return 4;
            break;
        case USB_SPEED_ESUPER_1X1:
            return 4;
            break;
        case USB_SPEED_ESUPER_2X1:
            return 5;
            break;
        case USB_SPEED_ESUPER_1X2:
            return 6;
            break;
        case USB_SPEED_ESUPER_2X2:
            return 7;
            break;
        default:
            return 0;
            break;
    }
}

/*! Gets the appropriate PSI value
 *
 *  \param hcState Host controller context pointer
 *  \param dev_speed USB device speed id
 *  \param root_port Root hub port the device is connected to
 *  \return A valid PSIV value or zero if an appropriate value could not be found
 */
int xhci_get_psi_from_speed(xhci_controller_state *hcState, const uint16_t dev_speed, const uint16_t root_port)
{
    uint64_t speed_bitrate;
    volatile uint32_t *entry_ptr;
    xhci_port_info *port_info;

    port_info = &hcState->port_info[root_port-1];

    if(!port_info->count)
        return xhci_get_implied_psi_from_speed(dev_speed);

    switch(dev_speed)
    {
        case USB_SPEED_LOW:
            speed_bitrate = XHCI_USB_LS_BITRATE;
            break;
        case USB_SPEED_FULL:
            speed_bitrate = XHCI_USB_FS_BITRATE;
            break;
        case USB_SPEED_HIGH:
            speed_bitrate = XHCI_USB_HS_BITRATE;
            break;
        case USB_SPEED_SUPER:
            speed_bitrate = XHCI_USB_SS1X1_BITRATE;
            break;
        case USB_SPEED_ESUPER_1X1:
            speed_bitrate = XHCI_USB_SS1X1_BITRATE;
            break;
        case USB_SPEED_ESUPER_1X2:
            speed_bitrate = XHCI_USB_SS1X2_BITRATE;
            break;
        case USB_SPEED_ESUPER_2X1:
            speed_bitrate = XHCI_USB_SS2X1_BITRATE;
            break;
        case USB_SPEED_ESUPER_2X2:
            speed_bitrate = XHCI_USB_SS2X2_BITRATE;
            break;
        default:
            return 0;
            break;
    }

    entry_ptr = port_info->speeds;
    xhci_log(XHCI_LOG_INFO, "Attempting to evaluate %u speeds for PSI value for root hub port %u\n", port_info->count, root_port);

    for(int entry_index = 0; entry_index < port_info->count; entry_index++, entry_ptr++)
    {
        uint64_t bandwidth;
        bandwidth = xhci_get_psi_bandwidth(*entry_ptr);
        xhci_log(XHCI_LOG_INFO, "Comparing PSI Bandwidth of %u to requested bandwidth of %u\n", bandwidth, speed_bitrate);
        if(bandwidth == speed_bitrate)
            return XHCI_PSI_GET_ID(*entry_ptr);
    }

    xhci_log(XHCI_LOG_ERRORS, "Error! Unable to decode USB speed %u Bandwidth %u to a PSI value!\n", dev_speed, speed_bitrate);
    return 0;
}

/*! Determines the speed of a device connected to a XHCI port
 *  \param port_info Port information structure
 *  \param psi_value XHCI port status and control provided psi_value
 *  \return A general USB speed class (low/full/high/esuper) or zero if the the provided PSI value was invalid
 */
int xhci_get_port_speed(const xhci_port_info *port_info, const int psi_value)
{
    if(!port_info->count)
    {
        /* No explicitly defined bandwidth fields necessitates falling back to default values */
        if(port_info->protocol == PORT_PROTO_USB20)
        {
            switch(psi_value)
            {
                case XHCI_DEFAULT_PSI_FS:
                    return USB_SPEED_FULL;
                    break;
                case XHCI_DEFAULT_PSI_LS:
                    return USB_SPEED_LOW;
                    break;
                case XHCI_DEFAULT_PSI_HS:
                    return USB_SPEED_HIGH;
                    break;
                default:
                    xhci_log(XHCI_LOG_ERRORS, "Unable to decode USB2.0 implied PSI value of %u\n", psi_value);
                    return 0;
                    break;
            }
        }
        else if(port_info->protocol == PORT_PROTO_USB30)
        {
            /* SuperSpeed literally only has a single speed available to it */
            switch(psi_value)
            {
                case XHCI_DEFAULT_PSI_SS1X1:
                    return USB_SPEED_SUPER;
                    break;
            default:
                xhci_log(XHCI_LOG_ERRORS, "Unable to decode USB3.0 implied PSI value of %u\n", psi_value);
                return 0;
                break;
            }
        }
        else
        {
            /* SuperSpeedPlus defined values as of USB 3.2 */
            switch(psi_value)
            {
                case XHCI_DEFAULT_PSI_SS1X1:
                    return USB_SPEED_ESUPER_1X1;
                    break;
                case XHCI_DEFAULT_PSI_SS2X1:
                    if(port_info->protocol >= PORT_PROTO_USB31)
                        return USB_SPEED_ESUPER_2X1;
                    else
                    {
                        xhci_log(XHCI_LOG_ERRORS, "USB protocol version mismatch expected 3.1 found 3.2 for PSI value %u\n", psi_value);
                        return 0;
                    }
                    break;
                case XHCI_DEFAULT_PSI_SS1X2:
                    if(port_info->protocol == PORT_PROTO_USB32)
                        return USB_SPEED_ESUPER_1X2;
                    else
                    {
                        xhci_log(XHCI_LOG_ERRORS, "USB protocol version mismatch expected 3.2 found 3.1 for PSI value %u\n", psi_value);


                        return 0;
                    }
                    break;
                case XHCI_DEFAULT_PSI_SS2X2:
                    if(port_info->protocol == PORT_PROTO_USB32)
                        return USB_SPEED_ESUPER_2X2;
                    else
                    {
                        xhci_log(XHCI_LOG_ERRORS, "USB protocol version mismatch expected 3.2 found 3.1 for PSI value %u\n", psi_value);


                        return 0;
                    }
                    break;
                default:
                    xhci_log(XHCI_LOG_ERRORS, "Unable to decode USB3.x implied PSI value of %u\n", psi_value);
                    return 0;
                    break;
            }
        }
    }
    else
    {
        /* Non-default value, parse the actual speed entries in the supported protocol capability */
        for(int dword_index = 0; dword_index < port_info->count; dword_index++)
        {
            uint8_t defined_value;
            uint32_t protocol_dword = port_info->speeds[dword_index];
            defined_value = XHCI_PSI_GET_ID(protocol_dword);
            if(defined_value == psi_value)
            {
                /* Determine the bandwidth of matching entry and then translate it to a USB speed value */
                uint64_t lbandwidth = xhci_get_psi_bandwidth(protocol_dword);

                if(port_info->protocol == PORT_PROTO_USB20)
                {
                    switch(lbandwidth)
                    {
                        case XHCI_USB_LS_BITRATE:
                            return USB_SPEED_LOW;
                            break;
                        case XHCI_USB_FS_BITRATE:
                            return USB_SPEED_FULL;
                            break;
                        case XHCI_USB_HS_BITRATE:
                            return USB_SPEED_HIGH;
                            break;
                        default:
                            xhci_log(XHCI_LOG_ERRORS, "Error cannot match bandwidth of %u to USB2.0 speed!\n", lbandwidth);
                            return 0;
                            break;
                    }
                }
                else
                {
                    switch(lbandwidth)
                    {
                        case XHCI_USB_SS1X1_BITRATE:
                            if( XHCI_PSI_GET_LINK(protocol_dword) == 0)
                                return USB_SPEED_SUPER;
                            else
                                return USB_SPEED_ESUPER_1X1;
                            break;
                        case XHCI_USB_SS2X1_BITRATE:
                            if(port_info->protocol == PORT_PROTO_USB31)
                                return USB_SPEED_ESUPER_2X1;
                            else
                                return USB_SPEED_ESUPER_1X2;
                            break;
                        case XHCI_USB_SS2X2_BITRATE:
                            if(port_info->protocol == PORT_PROTO_USB32)
                                return USB_SPEED_ESUPER_2X1;
                            else
                                return 0;
                            break;
                        default:
                            xhci_log(XHCI_LOG_ERRORS, "Error cannot match bandwidth of %u to USB3.x speed!\n", lbandwidth);
                            break;
                    }
                }
            }
        }
        return 0;
    }
}

/*! Signals a reset to the specified host controller port
 *  \param hcState Host controller context
 *  \param port_index Port index to reset, must be greater than zero
 *
 */
void xhci_port_set_reset(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port reset - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port reset - Invalid port index");

    volatile portReg *port_reg;
    uint32_t psc_value;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    psc_value = port_reg->portStatusControl & ~(XHCI_PORTSC_PED |XHCI_PORTSC_CHANGE_STATUS);


    psc_value |= XHCI_PORTSC_PR;

    port_reg->portStatusControl = psc_value;
}

/*! Attempts to transition the specified port to the suspended state
 *  \param hcState Host controller context
 *  \param port_index Port index to reset, must be greater than zero
 *
 */
driverOperationResult xhci_port_set_suspend(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port suspend - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port suspend - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol == PORT_PROTO_USB20, "Port suspend - Port protocol mismatch");
    volatile portReg *port_reg;
    uint32_t psc_value;

    /* Move the port into the U3 state from the U0 state */
    port_reg = &hcState->ops->portRegSet[port_index-1];
    psc_value = port_reg->portStatusControl;

    if( XHCI_PORTSC_GET_PLS(psc_value) != XHCI_PLS_READ_U0)
    {
        /* Per the USB 2.0 specification any attempt to suspend the port is a valid no-op when the state is not equivalent
         * to 'transmit' or 'enabled' (both a U0 PLS value per Section 5.4.8.1 of the XHCI specification) */
        return driverOpSuccess;
    }
    else
    {
        psc_value &= ~(XHCI_PORTSC_PLS_MASK | XHCI_PORTSC_PED);
        psc_value |= XHCI_PORTSC_SET_PLS( XHCI_PLS_WRITE_U3 );
        port_reg->portStatusControl = psc_value;
        return driverOpSuccess;
    }
}

/*! Moves a USB2.0 port out of the suspended state
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero port number
 */
void xhci_port_usb2_clear_suspend(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port clear suspend - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port clear suspend - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol == PORT_PROTO_USB20, "Port clear suspend - Port protocol mismatch");

    volatile portReg *port_reg;
    uint32_t psc_value;

    port_reg = &hcState->ops->portRegSet[port_index-1] ;

    psc_value = port_reg->portStatusControl;

    psc_value &= ~(XHCI_PORTSC_PED | XHCI_PORTSC_PLS_MASK);

    /* Move into the U0 state */
    port_reg->portStatusControl = psc_value;
}

/*! Disables a USB2.0 port on the root hub
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero port index
 */
void xhci_port_usb2_disable(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port disable - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port disable - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol == PORT_PROTO_USB20, "Port disable - Port protocol mismatch");

    volatile portReg *port_reg;
    uint32_t psc_value;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    /* Clear the enable bit in the port status and control register */
    psc_value = port_reg->portStatusControl;
    psc_value |= XHCI_PORTSC_PED;

    port_reg->portStatusControl = psc_value;
}

/*! Clears a change flag in the port status and control register
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero port number
 *  \param flag Bitmask for the flag to be cleared
 */
void xhci_port_clear_change(xhci_controller_state *hcState, const int port_index, const uint32_t flag)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port clear status change - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port clear status change - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, !(flag & ~XHCI_PORTSC_CHANGE_STATUS), "Port clear status change - Invalid bit specified");

    volatile portReg *port_reg;
    uint32_t psc_value;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    psc_value = port_reg->portStatusControl & ~(XHCI_PORTSC_PED | XHCI_PORTSC_CHANGE_STATUS);

    /* All clear flags are write 1 to change */
    psc_value |= flag;

    port_reg->portStatusControl = psc_value;

}

/*! Attempts to power the specified port
 *  \param hcState Host controller context
 *  \param port_index Port index to reset, must be greater than zero
 *
 */
driverOperationResult xhci_port_set_power(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port set power - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port set power - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol == PORT_PROTO_USB20, "Port set power - Port protocol mismatch");

    volatile portReg *port_reg;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    if(hcState->caps->hccparams1 & XHCI_HCC1_PPC)
    {
        uint32_t psc_value;

        psc_value = port_reg->portStatusControl & ~(XHCI_PORTSC_PED | XHCI_PORTSC_CHANGE_STATUS);
        psc_value |= XHCI_PORTSC_PP;
        port_reg->portStatusControl = psc_value;
        return driverOpSuccess;
    }
    else
        return driverOpNotFound;
}

/*! Clears the power flag of a specified port
 *
 *  \param hcState XHCI context pointer
 *  \param port_index Port number
 *
 *  \return driverOpNotFound is the controller does not support port level power control.  driverOp success if it does
 */
driverOperationResult xhci_port_clear_power(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port set power - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port set power - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol == PORT_PROTO_USB20, "Port set power - Port protocol mismatch");

    volatile portReg *port_reg;

    port_reg = &hcState->ops->portRegSet[port_index-1];
    if(hcState->caps->hccparams1 & XHCI_HCC1_PPC)
    {
        uint32_t psc_value;

        psc_value = port_reg->portStatusControl & ~(XHCI_PORTSC_PED | XHCI_PORTSC_CHANGE_STATUS | XHCI_PORTSC_PP);
        port_reg->portStatusControl = psc_value;
        return driverOpSuccess;
    }
    else
        return driverOpNotFound;

}


/*! Implements the functionality of the set port feature PORT_INDICATOR request
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero port index
 *  \param value Selector value representing the indicator behavior being requested
 *
 *  \return driverOpSuccess if successful or driverOpBadArg if the selector is invalid.
 */
driverOperationResult xhci_port_set_indicator(xhci_controller_state *hcState, const int port_index, const uint8_t value)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port indicator set - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port indicator set - Invalid port index");

    volatile portReg *port_reg;
    uint32_t psc_value, pic_value, pls_value;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    psc_value = port_reg->portStatusControl & ~XHCI_PORTSC_PED;
    pls_value = XHCI_PORTSC_GET_PLS(psc_value);

    switch(value)
    {
        case USB_PORT_INDICATOR_AMBER:
            pic_value = XHCI_PIC_AMBER;
            break;
        case USB_PORT_INDICATOR_GREEN:
            pic_value = XHCI_PIC_GREEN;
            break;
        case USB_PORT_INDICATOR_OFF:
            pic_value = XHCI_PIC_OFF;
            break;
        case USB_PORT_INDICATOR_AUTO:
            /* Set a value according to Table 11-6 in the USB2.0 specification */
            if(psc_value & XHCI_PORTSC_OCA)
            {
                /* Off or amber if there's an over current condition */
                pic_value = XHCI_PIC_AMBER;
            }
            else if(pls_value == XHCI_PLS_READ_U0)
            {
                /* Only green for Enabled, Transmit and TransmitR states which all correspond to a U0 PLS value */
                pic_value = XHCI_PIC_GREEN;
            }
            else
                pic_value = XHCI_PIC_OFF;
            break;
        default:
            /* Invalid color mode selector results in a USB Request Error per the USB Hub specification section 11.24.2.13.
             * Signal that to the caller by returning an appropriate error code. */
            return driverOpBadArg;
            break;
    }

    if(hcState->caps->hccparams1 & XHCI_HCC1_PIND)
    {
        /* Only attempt to set indicators if the controller actually supports the feature.  If not treat it as a successful no op */
        psc_value &= ~XHCI_PORTSC_PIC_MASK;
        psc_value |= XHCI_PORTSC_SET_PIC(pic_value);
        port_reg->portStatusControl = psc_value;
    }

    return driverOpSuccess;
}

/*! Updates the U1 timeout value for a USB3+ port
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero port index
 *  \param value New timeout value in microseconds with 0xFF being an infinite timeout
 */
void xhci_port_set_u1_timeout(xhci_controller_state *hcState, const int port_index, const uint8_t value)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port U1 timeout set - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port U1 timeout set - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol == PORT_PROTO_USB30, "Port U1 timeout set - Invalid port protocol");

    volatile portReg *port_reg;
    uint32_t pmsc_value;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    pmsc_value = port_reg->portPmStatusContrl;
    pmsc_value &= ~XHCI_PMSC_USB3_U1_MASK;
    pmsc_value |= XHCI_PMSC_USB3_SET_U1(value);

    port_reg->portPmStatusContrl |= pmsc_value;
}

/*! Updates the U2 timeout value for a USB3+ port
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero port index
 *  \param value New timeout value in units of 256 microseconds with 0xFF being an infinite timeout
 */
void xhci_port_set_u2_timeout(xhci_controller_state *hcState, const int port_index, const uint8_t value)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port U2 timeout set - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port U2 timeout set - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol >= PORT_PROTO_USB30, "Port U2 timeout set - Invalid port protocol");

    volatile portReg *port_reg;
    uint32_t pmsc_value;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    pmsc_value = port_reg->portPmStatusContrl;
    pmsc_value &= ~XHCI_PMSC_USB3_U2_MASK;
    pmsc_value |= XHCI_PMSC_USB3_SET_U2(value);

    port_reg->portPmStatusContrl |= pmsc_value;
}

/*! Attempts to transition the link state for a USB3+ port
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero port index
 *  \param value New timeout value in units of 256 microseconds with 0xFF being an infinite timeout
 */
driverOperationResult xhci_port_set_link_state(xhci_controller_state *hcState, const int port_index, const uint8_t value)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port set link state - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port set link state - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol >= PORT_PROTO_USB30, "Port set link state - Invalid port protocol");

    uint32_t psc_value, prior_state;
    xhci_port_info *port_info;
    volatile portReg *port_reg;

    port_reg = &hcState->ops->portRegSet[port_index-1];
    port_info = &hcState->port_info[port_index-1];

    psc_value = port_reg->portStatusControl;
    prior_state = XHCI_PORTSC_GET_PLS(psc_value);

    psc_value &= ~(XHCI_PORTSC_PLS_MASK | XHCI_PORTSC_PED);

    /* Valid state selectors and behavior defined in the USB3.0 specification Section 10.14.2.10 */
    switch(value)
    {
        case 0:
            /* Transfer into the U0 state from any other state, we do nothing here since the PLS value has been cleared to zero already */
            break;
        case 1:
            /* Attempt a U0 -> U1 transfer */
            if(prior_state == XHCI_PLS_READ_U0)
                psc_value |= XHCI_PORTSC_SET_PLS(XHCI_PLS_WRITE_U1);
            else
                return driverOpBadState;
            break;
        case 2:
            /* Attempt a U0 -> U2 transfer */
            if(prior_state == XHCI_PLS_READ_U0)
                psc_value |= XHCI_PORTSC_SET_PLS(XHCI_PLS_WRITE_U2);
            else
                return driverOpBadState;
            break;
        case 3:
            /* Attempt a U0 -> U3 transfer */
            if(prior_state == XHCI_PLS_READ_U0)
                psc_value |= XHCI_PORTSC_SET_PLS(XHCI_PLS_WRITE_U3);
            else
                return driverOpBadState;
            break;
        case 4:
            /* Move the port into the disabled state */
            psc_value |= XHCI_PORTSC_PED;
            break;
        case 5:
            /* Attempt a disabled -> RX.Detect state transition */
            if(prior_state == XHCI_PLS_READ_DISABLED)
                psc_value |= XHCI_PORTSC_SET_PLS(XHCI_PLS_WRITE_RXDETECT);
            else
                return driverOpBadState;
            break;
        case 10:
            /* Attempt a RxDetect -> Compliance transition */
            if(port_info->protocol == PORT_PROTO_USB30)
                return driverOpBadArg;
            if(prior_state != XHCI_PLS_READ_RXDETECT)
                return driverOpBadArg;
            psc_value |= XHCI_PORTSC_SET_PLS(XHCI_PLS_WRITE_COMPLIANCE);
            break;
        default:
            /* Any other selector values should be treated as a request error */
            return driverOpBadArg;
            break;
    }

    return driverOpSuccess;
}

/*! USB3.0+ Set remote wake mask feature
 *
 *  \param hcState Host controller context
 *  \param port_index Non-zero index of the target port
 *  \param value Remote wake condition bitmask as defined in the USB 3.0 specification
 *
 *  \return driverOpSuccess if the operation completed successfully or driverOpBadArg if an invalid mask was provided
 */
driverOperationResult xhci_port_set_remote_wake_mask(xhci_controller_state *hcState, const int port_index, const uint8_t value)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port set remote wake mask - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port set remote wake mask - Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol >= PORT_PROTO_USB30, "Port set remote wake - Invalid port protocol");

    uint32_t psc_value;
    volatile portReg *port_reg;
    port_reg = &hcState->ops->portRegSet[port_index-1];

    psc_value = port_reg->portStatusControl;

    if(value & UHUB_USB3_RWM_RESERVED)
        return driverOpBadArg;

    /* Clear all wake state masks */
    psc_value &= ~(XHCI_PORTSC_PED | XHCI_PORTSC_WCE | XHCI_PORTSC_WDE | XHCI_PORTSC_WOE);

    /* Toggle XHCI specific wake conditions based on the provided USB3 mask */
    if(value & UHUB_USB3_RWM_CONN)
        psc_value |= XHCI_PORTSC_WCE;

    if(value & UHUB_USB3_RWM_DISCONN)
        psc_value |= XHCI_PORTSC_WDE;

    if(value & UHUB_USB3_RWM_OC)
        psc_value |= XHCI_PORTSC_WOE;

    port_reg->portStatusControl = psc_value;

    return driverOpSuccess;
}

void xhci_port_warm_reset(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port warm reset - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port warm reset- Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol >= PORT_PROTO_USB30, "Port warm reset - Invalid port protocol");

    uint32_t psc_value;
    volatile portReg *port_reg;
    port_reg = &hcState->ops->portRegSet[port_index-1];

    psc_value = port_reg->portStatusControl & ~XHCI_PORTSC_PED;
    psc_value |= XHCI_PORTSC_WPR;

    port_reg->portStatusControl = psc_value;
}

void xhci_port_set_flpma(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port set force link PM accept - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port set force link PM accept- Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol >= PORT_PROTO_USB31, "Port set force link PM accept - Invalid port protocol");

    uint32_t prior_state;
    volatile portReg *port_reg;

    port_reg = &hcState->ops->portRegSet[port_index-1];
    prior_state = XHCI_PORTSC_GET_PLS(port_reg->portStatusControl);

    if(prior_state == XHCI_PLS_READ_U0)
        port_reg->portPmStatusContrl |= XHCI_PMSC_USB3_FLA;


}

void xhci_port_clear_lpma(xhci_controller_state *hcState, const int port_index)
{
    xhci_assert(XHCI_VALIDATE_PORT, port_index, "Port clear force link PM accept - Found zero port index");
    xhci_assert(XHCI_VALIDATE_PORT, port_index <= hcState->maxPorts, "Port clear force link PM accept- Invalid port index");
    xhci_assert(XHCI_VALIDATE_PORT, hcState->port_info[port_index-1].protocol >= PORT_PROTO_USB31, "Port clear force link PM accept - Invalid port protocol");

    uint32_t pmsc_value;
    volatile portReg *port_reg;

    port_reg = &hcState->ops->portRegSet[port_index-1];

    pmsc_value = port_reg->portPmStatusContrl & ~XHCI_PMSC_USB3_FLA;
    port_reg->portPmStatusContrl = pmsc_value;
}
