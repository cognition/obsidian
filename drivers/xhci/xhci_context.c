#include "xhci_cq.h"
#include "xhci_log.h"
#include "sync_queue.h"
#include "xhci_event.h"
#include <sync.h>
#include <usb.h>

/*! Triggered by a device entering the configured state
 *  \param hcState XHCI context instance
 *  \param slot_state Slot state structure for the device that has entered the configured state
 *  \param slot_num Slot number of the device
 */
void xhci_process_device_configured(xhci_controller_state *hcState, xhci_slot_state *slot_state, const int slot_num)
{
    xhci_log(XHCI_LOG_INTERNAL, "Processing configuration completion for slot %u\n", slot_num);

    dapi_lock_acquire(slot_state->slot_lock);

    /* Check all endpoints */
    for(int endpoint = 0; endpoint <= slot_state->ep_count; endpoint++)
    {
        uint8_t ep_state, ep_type;
        ep_state = slot_state->endpoint_state[endpoint];
        ep_type = ENDPOINT_GET_TYPE(ep_state);
        /* If the endpoint had it's pre-configuration setup done it could potentially have TRBs queued */
        if( ENDPOINT_GET_STATUS(ep_state) == ENDPOINT_CONFIGURING )
        {
            slot_state->endpoint_state[endpoint] = ENDPOINT_SET_TYPE(ep_type) | ENDPOINT_CONFIGURED;
            if(slot_state->endpoint_rings[endpoint]->commandsInFlight)
            {
                /* Run the transfer queue if TRBs were queued prior to the SET_CONFIGURATION command */
                xhci_log(XHCI_LOG_INFO, " Ringing doorbell for slot %u DCI %u\n", slot_num, endpoint+1);
                hcState->dbs[slot_num] = endpoint+1;

            }
        }
    }
    dapi_lock_release(slot_state->slot_lock);

}

/*! Debugging function, prints out information about an endpoint that's been stopped as part of a debug-stop operation
 *  \param slot Slot of the device providing the endpoint
 *  \param ep_dci Device context index of the endpoint that's been stopped
 *  \param epc Endpoint context of the endpoint
 */
static void xhci_internal_ep_info_out(const uint8_t slot, const uint8_t ep_dci, volatile endpointContext *epc)
{
    uint8_t ep_type, ep_state, cerr;
    int mps, mult, interval, max_burst;
    uint32_t maxESIT;

    ep_state = XHCI_EPC_GET_EP_STATE(epc->epState);
    cerr = XHCI_EPC_GET_CERR(epc->epTypeField);
    ep_type = XHCI_EPC_GET_TYPE(epc->epTypeField);

    mps = epc->maxPacketSize;
    mult = epc->streamInfo;
    interval = epc->interval;
    maxESIT = epc->maxEsitPayloadHi;
    maxESIT <<= 16;
    maxESIT |= epc->maxESITPayloadLo;
    max_burst = epc->maxBurstSize;

    printf("Slot %u EP DCI: %u - EPT: %u CErr: %u EPS: %u MPS: %u\n\tMult: %u INT: %u mESIT: %u MB: %u DQPTR: %p\n", slot, ep_dci, ep_type, cerr, ep_state, mps,
            mult, interval, maxESIT, max_burst, epc->trDequeuePtr);
}

/*! Debugging function, prints out the icc and active context information for an endpoint stopped by a debug-stop request
 *
 *  \param hcState XHCI context instance
 *  \param slot Slot of the device owning the endpoint
 *  \param ep_dci Endpoint device context index
 */
void xhci_print_ep_info(xhci_controller_state *hcState, const uint8_t slot, const uint8_t ep_dci)
{
    volatile endpointContext *epc, *init_epc;
    volatile slotContext *sc;

    sc = (volatile slotContext *)xhci_get_dev_context(hcState, slot, 0);
    epc = (volatile endpointContext *)xhci_get_dev_context(hcState, slot, ep_dci);
    init_epc = (volatile endpointContext *)xhci_get_icc_context(hcState, slot, ep_dci+1);

    /* Print the current state of the endpoint context */
    printf("Current state - ");
    xhci_internal_ep_info_out(slot, ep_dci, epc);
    /* Print the initialized state of the endpoint context from the ICC */
    printf("Requested state - ");
    xhci_internal_ep_info_out(slot, ep_dci, init_epc);

    printf("Slot context max DCI: %u Root port: %u Parent port: %u PSLOT: %u MEL: %u\n", sc->contextInfo>>3, sc->rootPortNum, sc->parentPortNumber, sc->parentSlotId, sc->maxExitLatency);
}

/*! Calculates the DCI of an endpoint
 *  \param endpoint Endpoint number
 *  \param direction Direction of the endpoint 0 = Out, 1 = In
 *  \param type Endpoint type
 *  \return Device context index of the endpoint
 */
uint32_t xhci_calc_ep_dci(const uint32_t endpoint, const uint32_t direction, const uint8_t type)
{
    uint32_t dci;
    dci = endpoint*2 + ((type == USB_EP_TYPE_CONTROL) ? 1:direction);
    return dci;
}


/*! Calculates the input context index of an endpoint
 *  \param endpoint Endpoint number
 *  \param direction Direction of the endpoint 0 = Out, 1 = In
 *  \param type Endpoint type
 *  \return Input context index of the endpoint
 */
uint32_t xhci_calc_ep_ici(const uint32_t endpoint, const uint32_t direction, const uint8_t type)
{
    uint32_t ici;
    ici = xhci_calc_ep_dci(endpoint, direction, type) + 1;
    return ici;
}

/*! Retrieves a virtual memory poitner to a specific input context field
 *  \param hcState Host controller instance context
 *  \param slot Input context target slot
 *  \param index Index of the field within the input context where 0 is the ICC itself, 1 is the slot context and 2+ are endpoint contextes
 *  \return A pointer to the requested context or null if the slot is invalid
 */
volatile void *xhci_get_icc_context(xhci_controller_state *hcState, const int slot, const int index)
{
    xhci_slot_state *slot_ptr;
    uintptr_t icc_base;
    if(slot > hcState->maxSlots)
        return NULL;

    slot_ptr = &hcState->slots[slot];
    icc_base = (uintptr_t)slot_ptr->input_context;
    icc_base += index*hcState->devContextSize;

    return (volatile void *)icc_base;
}

/*! Retrieves a virtual memory pointer to a specific context
 *  \param hcState Host controller context
 *  \param slot Slot containing the target context
 *  \param index Index of the context to be retrieved
 *  \return A pointer to the context requested or NULL if the request was invalid
 */
volatile void *xhci_get_dev_context(xhci_controller_state *hcState, const int slot, const int index)
{
    volatile slotContext *sc = &hcState->devContextRegions[slot].contexts->sc;
    uint32_t max_context_entries;
    uintptr_t offset;
    uintptr_t addr = (uintptr_t)sc;

    max_context_entries = (XHCI_SLOT_CONTEXT_ENTRIES(sc->contextInfo));
    if( max_context_entries < index)
    {
        xhci_log(XHCI_LOG_ERRORS, "Request for device context %u when maximum entry is %u\n", index, max_context_entries);
        return NULL;
    }

    offset = hcState->devContextSize;
    offset *= index;
    addr += offset;

    return (volatile void *)addr;
}

/*! Retrieves the maximum packet size of an endpoint
 *  \param hcState Host controller instance context
 *  \param slot Slot of the device containing the target endpoint
 *  \param endpoint Endpoint of the device
 *  \param direction Direction of the endpoint 0 for in and 1 for out
 *  \param type Type of endpoint
 *  \return The current maximum packet size of the endpoint or zero if the request was invalid
 */
uint32_t xhci_get_ep_mps(xhci_controller_state *hcState, const int slot, const int endpoint, const int direction, const uint8_t type)
{
    uint32_t epIndex, mps;
    endpointContext *ec;

    if(slot > hcState->maxSlots)
    {
        xhci_log(XHCI_LOG_ERRORS, "Get MPS request for slot %u is invalid, exceeds maximum slot count of %u\n", slot, hcState->maxSlots);
        return 0;
    }

    epIndex = xhci_calc_ep_dci(endpoint, direction, type);

    ec = (endpointContext *)xhci_get_dev_context(hcState, slot, epIndex);
    if(!ec)
    {
        xhci_log(XHCI_LOG_ERRORS, "Get MPS request for slot %u endpoint %u direction %u failed to return device context!\n", slot, endpoint, direction);
        return 0;
    }
    xhci_log(XHCI_LOG_INTERNAL, "Got slot %u Endpoint %u Direction: %u DCI: %u information MPS: %u Type: %X\n", slot, endpoint, direction, epIndex, ec->maxPacketSize, ec->epTypeField);
    mps = ec->maxPacketSize;
    if(!mps)
    {
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    return mps;
}

/*! Processes a successful command that might have a synchronization object attached
 *  \param hcState Host controller instance context
 *  \param trb TRB that generated the successful command completion event
 *  \param slot XHCI slot number
 */
void xhci_process_potential_command_sync(xhci_controller_state *hcState, volatile commandTRB *trb, volatile commandCompletionEventTRB *cce)
{
    volatile commandTRB *sync_cmd;
    xhci_slot_state *slot_ptr;
    int slot, completion_code, trb_type;

    slot = cce->slotId;
    slot_ptr = &hcState->slots[slot];
    completion_code = cce->commandParameter >> 24;
    trb_type = READ_TRB_TYPE_ADJ(trb->noOp.trbType);

    if(completion_code != COMP_CODE_SUCCESS)
    {
        slotContext *sc;
        sc = (slotContext *)xhci_get_dev_context(hcState, slot, 0);
        xhci_log(XHCI_LOG_ERRORS, "Command completion error - Slot %u Address: %u Controller id: %u Code: %u TRB Type: %u\n",
                 slot, sc->usbDeviceAddress, hcState->controller_id, completion_code, trb_type );
        return;
    }

    /* If there's synchronization objects waiting for command responses on this slot check them against this command */
    sync_cmd = sync_queue_peek(&slot_ptr->command_wait);
    if( sync_cmd == trb)
    {
        /* If there's a match signal the wait object */
        sync_object *obj;
        obj = sync_queue_pop(&slot_ptr->command_wait);
        xhci_log(XHCI_LOG_INTERNAL, "Activating sync object %p associated with command trb of type %u\n", obj, trb_type);
        sync_object_signal(obj, 1);
        sync_detach(obj);
    }
}

