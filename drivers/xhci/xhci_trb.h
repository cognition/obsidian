#ifndef XHCI_TRB_H_
#define XHCI_TRB_H_

#include <compiler.h>
#include <arch.h>

#define TRB_SIZE                                 (sizeof(commandTRB))
#define TRB_PER_PAGE                             (ARCH_PAGE_SIZE/TRB_SIZE)
#define TABLE_LAST_TRB                           (TRB_PER_PAGE - 1)

/* TRB type insertion and extraction */
#define READ_TRB_TYPE_ADJ(x)                     (((x)>>2) & 0x3FU)
#define WRITE_TRB_TYPE_ADJ(x)                    ((x)<<2)

/* Common TRB flags */
#define TRB_CYCLE_BIT                            0x01U
#define TRB_FLAGS_ENT                            0x02U
#define TRB_FLAGS_ISP                            0x04U
#define TRB_FLAGS_NS                             0x08U
#define TRB_FLAGS_CHAIN                          0x10U
#define TRB_FLAGS_IOC                            0x20U
#define TRB_FLAGS_IDT                            0x40U
#define TRB_FLAGS_DIR                            0x01U

/* Command specific TRB flags */
#define TRB_AD_BSR                               0x02U

/* Link specific fields */
#define LINK_TRB_TOGGLE_CYCLE                    0x02U
#define LINK_TRB_CHAIN                           0x10U

/* Transfer Event TRB specific fields */
#define TRANSFER_EVENT_ED                        0x04U
#define TRANSFER_EVENT_GET_CC(x)                 ( (x)>>24)
#define TRANSFER_EVENT_GET_BYTES(x)              ( (x) & 0xFFFFFFU)

/* Transfer TRB types */
#define TRB_NORMAL                               1
#define TRB_SETUP                                2
#define TRB_DATA                                 3
#define TRB_STATUS                               4
#define TRB_ISOCH                                5
#define TRB_LINK                                 6
#define TRB_EVENT_DATA                           7
#define TRB_NO_OP                                8

/* Command ring TRB types */
#define TRB_ENABLE_SLOT                          9
#define TRB_DISABLE_SLOT                         10
#define TRB_ADDRESS_DEVICE                       11
#define TRB_CONFIGURE_ENDPOINT                   12
#define TRB_EVALUATE_CONTEXT                     13
#define TRB_RESET_ENDPOINT                       14
#define TRB_STOP_ENDPOINT                        15
#define TRB_SET_TR_DEQUEUE                       16
#define TRB_RESET_DEVICE                         17
#define TRB_FORCE_EVENT                          18
#define TRB_NEGOTIATE_BANDWIDTH                  19
#define TRB_SET_LTV                              20
#define TRB_GET_PORT_BANDWIDTH                   21
#define TRB_FORCE_HEADER                         22
#define TRB_NO_OP2                               23
#define TRB_GET_EXT_PROP                         24
#define TRB_SET_EXT_PROP                         25

/* Event ring TRB types */
#define TRB_TRANSFER_EVENT                       32
#define TRB_CMD_COMPLETION_EVENT                 33
#define TRB_PSC_EVENT                            34
#define TRB_BANDWIDTH_REQ_EVENT                  35
#define TRB_DOORBELL_EVENT                       36
#define TRB_HOST_CONTROLLER_EVENT                37
#define TRB_DEVICE_NOTIFY_EVENT                  38
#define TRB_MFINDEX_WRAP_EVENT                   39

#define MAKE_TRANSFER_INFO(tLength, tdSize, interrupter)        (tLength | (tdSize<<17) | (interrupter<<22))
#define TRB_TRANSFER_GET_LENGTH(x)               ((x) & 0x1FFFFU)

/* Transfer ring TRBs */
typedef struct COMPILER_PACKED
{
	uint64_t eventData;
	uint8_t _reserved[3], interrupter;
	uint8_t flags, trbType, _reserved2[2];
} eventDataTRB;

typedef struct COMPILER_PACKED
{
	union
	{
		uint64_t dataBuffer;
		uint8_t immData[8];
	};
	uint32_t transferInfo;
	uint8_t cycleFlags, trbType, _reserved[2];
} normalTRB;

typedef struct COMPILER_PACKED
{
	uint8_t bmRequestType, bRequest;
	uint16_t wValue, wIndex, wLength;
	uint32_t transferInfo;
	uint8_t cycleFlags, trbType, transferType, _reserved;
} setupStageTRB;

typedef struct COMPILER_PACKED
{
	union
	{
		uint64_t dataBuffer;
		uint8_t immData[8];
	};
	uint32_t transferInfo;
	uint8_t cycleFlags, trbType, direction, _reserved;
} dataStageTRB;

typedef struct COMPILER_PACKED
{
	uint64_t _reserved;
	uint32_t transferInfo;
	uint8_t cycleFlags, trbType, direction, _reserved2;
} statusStageTRB;

typedef struct COMPILER_PACKED
{
	union
	{
		uint64_t dataBuffer;
		uint8_t immData[8];
	};
	uint32_t transferInfo;
	uint8_t cycleFlags, trbType;
	uint16_t timingInfo;
} isochTRB;

typedef struct COMPILER_PACKED
{
	uint64_t _reserved;
	uint32_t transferInfo;
	uint8_t cycleFlags, trbType;
	uint16_t _reserved2;
} noOpTRB;

typedef union
{
	normalTRB normal;
	setupStageTRB setup;
	dataStageTRB data;
	statusStageTRB status;
	isochTRB iso;
	noOpTRB noOp;
	eventDataTRB event_data;
} transferTRB;

/* Event Ring TRBs */
typedef struct COMPILER_PACKED
{
	uint64_t trbPointer;
	uint32_t trbTransferLength;
	uint8_t cycle, trbType, endpointId, slotId;
} transferEventTRB;

typedef struct COMPILER_PACKED
{
	uint64_t commandTrbPtr;
	uint32_t commandParameter;
	uint8_t cycle, trbType, vfId, slotId;
} commandCompletionEventTRB;

typedef struct COMPILER_PACKED
{
	uint8_t _reserved[3], portId, _reserved2[7], cycle, trbType, _reserved3[2];
} portStatusChangeTRB;

typedef struct COMPILER_PACKED
{
	uint8_t _reserved[11], completionCode, cycle, trbType, _reserved2, slotId;
} bandwidthRequestEventTRB;

typedef struct COMPILER_PACKED
{
	uint8_t dbReason, _reserved[10], completionCode, cycle, trbType, vfId, slotId;
} doorbellEventTRB;

typedef struct COMPILER_PACKED
{
	uint8_t _reserved[11], completionCode, cycle, trbType, _reserved2[2];
} hostControllerEventTRB;

typedef struct COMPILER_PACKED
{
	uint8_t notificationType, deviceNotificationData[7], _reserved[3];
	uint8_t completionCode, cycle, trbType, _reserved2, slotId;
} deviceNotificationEventTRB;

typedef struct COMPILER_PACKED
{
	uint8_t _reserved[11], completionCode, cycle, trbType, _reserved2[2];
} mfIndexWrapTRB;

typedef union
{
	transferEventTRB transferEvent;
	commandCompletionEventTRB commandCompletion;
	portStatusChangeTRB portStatusChange;
	bandwidthRequestEventTRB bandwidthRequest;
	doorbellEventTRB doorbellEvent;
	hostControllerEventTRB hostController;
	deviceNotificationEventTRB deviceNotification;
	mfIndexWrapTRB mfIndexWrap;
} eventTRB;

/* Command Ring TRBs */
typedef struct COMPILER_PACKED
{
	volatile uint32_t _reserved[3];
	volatile uint8_t cycle, trbType;
	volatile uint16_t slotType;
} enableSlotTRB;

typedef struct COMPILER_PACKED
{
	uint32_t _reserved[3];
	uint8_t cycle, trbType;
	uint8_t _reserved2, slotId;
} disableSlotTRB;

typedef struct COMPILER_PACKED
{
	uint64_t icPtr;
	uint32_t _reserved;
	uint8_t cycle, trbType, _reserved2, slotId;
} addressDeviceTRB;

typedef struct COMPILER_PACKED
{
	uint64_t icc_ptr;
	uint32_t _reserved;
	uint8_t cycle, trbType, _reserved2, slotId;
} configEndpointTRB;

typedef struct COMPILER_PACKED
{
	uint64_t input_context_addr;
	uint32_t _reserved;
	uint8_t cycle, trbType, _reserved2, slotId;
} evaluateContextTRB;

typedef struct COMPILER_PACKED
{
	uint32_t _reserved[3];
	uint8_t cycle, trbType, endpointId, slotId;
} resetEndpointTRB;

typedef struct COMPILER_PACKED
{
	uint32_t _reserved[3];
	uint8_t cycle, trbType, endpointId, slotId;
} stopEndpointTRB;

typedef struct COMPILER_PACKED
{
	uint32_t trLow, trHigh;
	uint16_t _reserved, streamId;
	uint8_t cycle, trbType, endpointId, slotId;
} setTRdequeuePtrTRB;

typedef struct COMPILER_PACKED
{
	uint32_t _reserved[3];
	uint8_t cycle, trbType;
	uint8_t _reserved2, slotId;
} resetDeviceTRB;

typedef struct COMPILER_PACKED
{
	uint32_t pbcLow, pbcHigh, _reserved;
	uint8_t cycle, trbType, devSpeed, hubSlotId;
} getPortBandwidthTRB;

typedef struct COMPILER_PACKED
{
	uint32_t hiLo, hiMid, hiHigh;
	uint8_t cycle, trbType, _reserved, rootHubPortNum;
} forceHeaderTRB;

typedef struct COMPILER_PACKED
{
	uint32_t epcLow, epcHigh;
	uint16_t extCapId, _reserved;
	uint8_t cycle, trbType, subTypeEp, slotId;
} getExtPropertyTRB;

typedef struct COMPILER_PACKED
{
	uint32_t epcLow, epcHigh;
	uint16_t extCapId, _reserved;
	uint8_t cycle, trbType, subTypeEp, slotId;
} setExtPropertyTRB;

typedef union
{
	noOpTRB noOp;
	enableSlotTRB enableSlot;
	disableSlotTRB disableSlot;
	addressDeviceTRB addressDevice;
	configEndpointTRB configureEndpoint;
	evaluateContextTRB evaluteContext;
	resetEndpointTRB resetEndpoint;
	stopEndpointTRB stopEndpoint;
	setTRdequeuePtrTRB setTRdequeuePtr;
	resetDeviceTRB resetDevice;
	getPortBandwidthTRB getPortBandwidth;
	forceHeaderTRB forceHeader;
	getExtPropertyTRB getExtProperty;
	setExtPropertyTRB setExtProperty;
} commandTRB;

/* Special function TRBs */
typedef struct COMPILER_PACKED
{
	uint64_t segmentPointer;
	uint8_t _reserved[3], interrupter, flags, trbType, _reserved2[2];
} linkTRB;


typedef union
{
	eventDataTRB eventData;
	linkTRB link;
} otherTRB;

/* All possible TRBs */
typedef union
{
	commandTRB command;
	transferTRB transfer;
	linkTRB link;
	eventDataTRB event_data;
	uint32_t raw_dw[4];
} softwareIssuedTRB;

#endif
