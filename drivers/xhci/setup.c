/* XHCI Initialization, interrupt handler and tear down code
 *
 *
 * Author: Joseph Kinzel */

#include "xhci_controller.h"
#include "xhci_cq.h"
#include "xhci_ports.h"
#include "xhci_event.h"
#include "xhci_bus.h"
#include "xhci_root_hub.h"
#include "xhci_log.h"
#include <usb.h>

typedef struct COMPILER_PACKED
{
	mappedRegion *commandRings;
	uint16_t endpointCount;
} slotInformation;

/* Prototypes for module table */
driverOperationResult xhci_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet);
driverOperationResult xhci_deinit(void *context);
driverOperationResult xhci_enum(void *context, void *identifyFunc);

/* Module export table */
COMPILER_MODINFO_SECTION const modExportTable moduleInfoTable = {&xhci_init, &xhci_deinit, &xhci_enum};

/* Cleanup function */
static void destroy_incomplete_hc_state(xhci_controller_state *hcState)
{
	if(hcState->port_info)
		free(hcState->port_info);

	if(hcState->devContextRegions)
		freePhysRegion(hcState->deviceContextArray.virtualBase, hcState->dev);

	if(hcState->hcCommandQueue)
		xhci_destroy_ring(hcState, hcState->hcCommandQueue);

	if(hcState->scratchPadArray.virtualBase)
		freePhysRegion(hcState->scratchPadArray.virtualBase, hcState->dev);

    if(hcState->command_queue_lock)
        dapi_lock_destroy(hcState->command_queue_lock);

    if(hcState->slot_request_lock)
        dapi_lock_destroy(hcState->slot_request_lock);

    if(hcState->slot_state_lock)
        dapi_lock_destroy(hcState->slot_state_lock);

    if(hcState->default_addr_lock)
        dapi_lock_destroy(hcState->default_addr_lock);
}

/*! Frees a control data structure
 *  \param trbRegion Mapped region to free
 *  \param dev Device handle for a specific driver instance
 */
void free_ring_or_array(mappedRegion *trbRegion, DEVICE_HANDLE dev)
{
	freePhysRegion(trbRegion->virtualBase, dev);
}

/*! Processes the extended capabilties list of an XHCI controller
 *  \param hc Host controller state structure
 *  \param exCapVal Extended capabilities entry offset
 *  \return driverOpSuccess if Successful
 */
driverOperationResult xhci_process_excaps(xhci_controller_state *hc, uint32_t exCapVal)
{
	driverOperationResult result;
	uint8_t capType;
	volatile uint32_t *capPtr;
	uint32_t capValue, currentCapPtr;
	uintptr_t vBase = (uintptr_t)hc->caps;

	/* Initialize the values for the first entry */
	currentCapPtr = exCapVal;
	capPtr = (volatile uint32_t *)vBase;

	while(currentCapPtr)
	{
		capPtr = &capPtr[currentCapPtr];
		capValue = *capPtr;
		capType = capValue & 0xFFU;
		currentCapPtr = ((capValue>>8) & 0xFFU);
		switch(capType)
		{
			case EXCAP_USB_LEGACY:
				{
					/* Legacy support handoff */
					xhci_excap_legacy *legacy = (xhci_excap_legacy *)capPtr;

					if(legacy->hc_os_owned & 0x1)
						break;

					xhci_log(XHCI_LOG_BASIC, "Performing Firmware to OS handoff...\n");
					legacy->hc_os_owned |= 1;
					int counter = 0;
					wait(500);
					while( (legacy->hc_bios_owned & 0x1U))
					{
						if(counter > 4)
						{
							xhci_log(XHCI_LOG_ERRORS, "Unable to obtain controller ownership from firmware!\n");
							return driverOpHardwareError;
						}
						wait(250);
						counter++;
					}
					xhci_log(XHCI_LOG_INFO, "OS has obtained controller ownership.\n");
				}
				break;
			case EXCAP_SUPPORTED_PROTO:
				/* Protocol/Speed information */
				xhci_log(XHCI_LOG_BASIC, "Obtaining protocol information...\n");
				result = process_xhci_protocol_capability(hc, (xhci_supported_protocol *)capPtr);
				if(result != driverOpSuccess)
					return result;
				else
					xhci_log(XHCI_LOG_BASIC, "Protocol information obtained.\n");

				break;
			default:
				xhci_log(XHCI_LOG_WARNINGS, "Unknown extended capability type: %u\n", capType);
				break;
		}
	};

	return driverOpSuccess;
}

/*! Creates a new event table for a given interrupter
 *  \param hcState Host controller state structure
 *  \param interrupter_index Interrupter for the new event table to be attached to
 *  \return driverOpSuccess if successful, driverOpBadArg if the interrupter_index value is invalid, other values from allocate_mapped_page
 *  \sa allocate_mapped_page
 */
driverOperationResult xhci_create_event_table(xhci_controller_state *hcState, int interrupter_index)
{
	driverOperationResult status;

	if(interrupter_index >= hcState->maxInterrupters)
		return driverOpBadArg;

	/* Allocate and initialize a new page */
	status = allocate_mapped_page(&hcState->eventTables[interrupter_index], hcState);
	if(status != driverOpSuccess)
		return status;

	return driverOpSuccess;
}

/* Enables the event ring of an interrupter
 * \param hcState Host controller state structure
 * \param interrupter_index Interrupter to enable the event ring on
 * \return driverOpSuccess if the completed successfully, driverOpBadArg if an invalid argument was provided
 */
driverOperationResult xhci_enable_event_ring(xhci_controller_state *hcState, int interrupter_index)
{
	volatile xhci_interrupt_reg *reg;

	if(interrupter_index >= hcState->maxInterrupters)
		return driverOpBadArg;

	reg = &hcState->rts->interrupter[interrupter_index];


	//Setting the segment table base register enables events on the ring
	if(hcState->deviceFlags & XHCI_CTRL_FLAGS_64BIT)
    {
        uint64_t new_value;
        new_value = reg->eventRingSegmentTableBase64 & XHCI_INT_ERSTBA_PRESERVE;
        new_value |= hcState->eventTables[interrupter_index].physicalBase;
		reg->eventRingSegmentTableBase64 = new_value;
    }
	else
    {
        uint32_t new_value;
        new_value = reg->eventRingSegmentTableBase32 & XHCI_INT_ERSTBA_PRESERVE;
        new_value |= hcState->eventTables[interrupter_index].physicalBase;
		reg->eventRingSegmentTableBase32 = new_value;
    }

	return driverOpSuccess;
}

/*! Enable interrupts on a given interrupter
 * \param hcState Host controller state structure
 * \param interrupter_index Interrupter to enable interrupts on
 * \return driverOpSuccess if the completed successfully, driverOpBadArg if an invalid argument was provided
 */
driverOperationResult xhci_enable_interrupter(xhci_controller_state *hcState, int interrupter_index)
{
	volatile xhci_interrupt_reg *reg;

	if(interrupter_index >= hcState->maxInterrupters)
		return driverOpBadArg;

	reg = &hcState->rts->interrupter[interrupter_index];
	reg->imod = 4000;

	reg->iman |= XHCI_IMAN_IE;

	return driverOpSuccess;
}

/*! Adds a new segment to an event table, if possible
 * \param hcState Host controller state structure
 * \param interrupter_index Interrupter who's table a segment should be added to
 * \return driverOpSuccess if the completed successfully, driverOpBadArg if an invalid argument was provided, driverOpResources if the table is full
 */
driverOperationResult xhci_add_event_table_ring(xhci_controller_state *hcState, int interrupter_index)
{
	uint16_t currentSegment;
	uint32_t segCountValue;
	mappedRegion *eventTable, *newSegment;
	volatile xhci_event_ring_segment *seg;
	volatile xhci_interrupt_reg *intReg;

	if(interrupter_index >= hcState->maxInterrupters)
		return driverOpBadArg;

	eventTable = &hcState->eventTables[interrupter_index];
	intReg = &hcState->rts->interrupter[interrupter_index];
	currentSegment = intReg->eventSegCount & 0xFFFFU;

	if(currentSegment >= XHCI_EST_ENTRIES_PER_PAGE)
		return driverOpResources;

	/* Allocate and initialize new page sized segment */
	newSegment = &hcState->tableMappings[interrupter_index*EVENT_RING_PER_PAGE + currentSegment];
	allocate_mapped_page(newSegment, hcState);

	/* Set the entry in the event table to the new segment */
	seg = &eventTable->ringSegments[currentSegment];
	seg->segmentBase = newSegment->physicalBase;
	seg->ringSegmentSize = TRB_PER_PAGE;

	/* Initialize the last event pointer if this is the first segment in the table */
	if(!currentSegment)
		hcState->eventLastPtrs[interrupter_index] = newSegment->physicalBase;

	segCountValue = (intReg->eventSegCount & 0xFFFF0000U) | (currentSegment + 1);

	/* Write the dequeue pointer for this interrupter with the base of the new segment */
	if(hcState->deviceFlags & XHCI_CTRL_FLAGS_64BIT)
    {
		intReg->eventRingDequeuePtr64 = newSegment->physicalBase;
        xhci_log(XHCI_LOG_INTERNAL, "HC Event ring base segment: %p Event ring dqptr: %p\n", newSegment->physicalBase, intReg->eventRingDequeuePtr64);
    }
	else
    {
        intReg->eventRingDequeuePtr32 = newSegment->physicalBase;
        xhci_log(XHCI_LOG_INTERNAL, "HC Event ring base segment: %p Event ring dqptr: %p\n", newSegment->physicalBase, intReg->eventRingDequeuePtr32);
    }

	intReg->eventSegCount = segCountValue;

	return driverOpSuccess;
}

/*! Clears the set one to clear bits of the status register
 *  \param hcState Controller state structure
 *  \param clearBits Bits to clear
 */
void xhci_status_clear(xhci_controller_state *hcState, uint32_t clearBits)
{
	hcState->ops->usbStatus = clearBits;
}

/*! Interrupt handling function
 *  \param context Host controller state structure pointer
 *  \return interruptNotMe if the controller did not raise the interrupt, interruptHandled if the controller dealt with the interrupt
 */
static int xhci_interrupt_func(void *context, const int interrupter_index)
{
	interruptResult result;
	uint32_t usbStatusRead;
	xhci_controller_state *hcState = (xhci_controller_state *)context;

	result = interruptNotMe;

	usbStatusRead = hcState->ops->usbStatus;

	xhci_log(XHCI_LOG_INTERNAL, "Interrupt handler entered for index %u\n", interrupter_index);
	if(usbStatusRead & USB_STATUS_HCE)
    {
        xhci_log(XHCI_LOG_ERRORS, "Host controller error encountered!\n");
    }

	if(usbStatusRead & USB_STATUS_PCD)
	{
		/* Port change related interrupt */
		xhci_log(XHCI_LOG_INTERNAL, "Port change interrupt detected!\n");

		result = interruptHandled;
		xhci_status_clear(hcState, USB_STATUS_PCD);
		usbStatusRead &= ~USB_STATUS_PCD;
	}

	if(usbStatusRead & USB_STATUS_EINT)
	{
	    usbStatusRead &= ~USB_STATUS_EINT;
		/* Event interrupt, checks event rings... */
		/* EINT flag must be cleared before any IP flags per the specification */
        xhci_log(XHCI_LOG_OPSRES, "Event interrupt!\n");
		xhci_handle_ring_events(hcState, interrupter_index);

		result = interruptHandled;
	}
    if(usbStatusRead)
    {
        xhci_log(XHCI_LOG_ERRORS, "Unhandled status conditions: %X\n", usbStatusRead);
    }
    xhci_log(XHCI_LOG_INTERNAL, "Interrupt handler exit for index %u Command reg: %X IMAN: %X\n", interrupter_index, hcState->ops->usbCommand, hcState->rts->interrupter[interrupter_index].iman );
	return result;
}

/*! Controller initialization function - Driver API required
 *  \param dev Driver hardware object handle
 *  \param desc Bus address descriptor
 *  \param contextRet Controller state instance return structure
 */
driverOperationResult xhci_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet)
{
	uint8_t usbVersion;
	int barType, counter, interrupt_count;
	driverOperationResult result;
	uint32_t fullClassCode, exCapValue, configure_value;
	uint64_t mmioBase, mmioLength, mmioVirtBase;
	xhci_controller_state *hcState;

	/* Only PCI based implmentations are supported currently */
	if(desc->busId != BUS_ID_PCI)
		return driverOpBusError;

	/* Make sure the driver matching software didn't make a mistake */
	if(pciReadDWord(&fullClassCode, desc->address, PCI_CLASS_CODE_REV) != busOpSuccess)
		return driverOpBusError;

	fullClassCode >>= 8;
	if(fullClassCode != USB_XHCI_CLASS_CODE)
	{
		xhci_log(XHCI_LOG_ERRORS, "Error - Expected class code: %X Found: %X\n", USB_XHCI_CLASS_CODE, fullClassCode);
		return driverOpHardwareError;
	}

	/* Validate the supported USB version */
	if(pciReadByte(&usbVersion, desc->address, USB_SBRN_REG) != busOpSuccess)
		return driverOpBusError;


	/*  Disabled for now, VirtualBox appears to report itself as having a USB2.0 controller
	 *  even though the XHCI specification does not recognize that value (5.2.3)

	if((usbVersion & USB_VERSION_MAJOR) != USB_VERSION3)
	{
		xhci_log(XHCI_LOG_ERRORS, "Error unknown controller version: %X\n", usbVersion & USB_VERSION_MAJOR);
		return driverOpVersionMismatch;
	}

	*/

	if( pciReadBar(&mmioBase, &mmioLength, &barType, desc->address, 0) != busOpSuccess )
		return driverOpBusError;

	result = registerMMIORegion(&mmioVirtBase, mmioBase, mmioLength, dev);

	if(result != driverOpSuccess)
	{
		xhci_log(XHCI_LOG_ERRORS, "Error registering MMIO Region!\n");
		return result;
	}

    /* Create the control structure and map the MMIO register space into virtual memory */
	hcState = (xhci_controller_state *)calloc(1, sizeof(xhci_controller_state));

    hcState->dev_to_slot_map = (uint8_t *)calloc(USB_MAX_DEVICE_COUNT, 1);

	hcState->busAddr = *desc;


	/* Setup MMIO register access pointers */
	hcState->caps = (xhci_cap_regs *)mmioVirtBase;
	xhci_log( XHCI_LOG_INTERNAL, "Operational register space starts @ %X\n", hcState->caps->capLength);
	hcState->ops = (xhci_operational_regs *)(mmioVirtBase + hcState->caps->capLength);
	hcState->dbs = (volatile uint32_t *)(mmioVirtBase + (hcState->caps->dbOff & ~0x3UL));
	hcState->rts = (xhci_runtime_regs *)(mmioVirtBase + hcState->caps->rtsOff);

	/* Get basic host controller configuration parameters */
	hcState->maxPorts = hcState->caps->hcsparams1>>24;
	hcState->maxSlots = hcState->caps->hcsparams1 & 0xFFU;
	hcState->maxRootHubAddress = USB_MAX_DEVICE_COUNT - 1;

	hcState->slots = (xhci_slot_state *)calloc(hcState->maxSlots, sizeof(xhci_slot_state));
	hcState->devContextRegions = (mappedRegion *)calloc(hcState->maxSlots, sizeof(mappedRegion) );

	/* Get maximum interrupter count and initialize the structures */
	hcState->maxInterrupters = (hcState->caps->hcsparams1 >> 8) & 0x7FFU;
	//TODO: If multiple interrupters are supported attempt to request them through the driver api to determine the maximum number of addressable interrupters

	hcState->eventLastPtrs = (uint64_t *)calloc(hcState->maxInterrupters, sizeof(uint64_t));
	hcState->eventLastTableIndices = (uint32_t *)calloc(hcState->maxInterrupters, sizeof(uint32_t));

	hcState->eventCCS = (uint8_t *)malloc(hcState->maxInterrupters);
	memset(hcState->eventCCS, 1, hcState->maxInterrupters);

	/* Get scratchpad information */
	hcState->maxScratchpads = (hcState->caps->hcsparams2 >> 21) & 0x1FU;
	hcState->maxScratchpads <<= 5;
	hcState->maxScratchpads |= (hcState->caps->hcsparams2 >> 27);

	/* Initialize the controller's information flags */
	hcState->deviceFlags = (hcState->caps->hccparams1 & XHCI_HCC1_64BIT_CAPABLE) ? XHCI_CTRL_FLAGS_64BIT:0;

	xhci_log(XHCI_LOG_INFO, "Controller has %u ports and %u slots\n", hcState->maxPorts, hcState->maxSlots);

	/* Determine if the controller supports 64 bit physical addresses and register widths */
	if(hcState->deviceFlags & XHCI_CTRL_FLAGS_64BIT)
    {
		xhci_log(XHCI_LOG_INFO, "Controller supports 64-bit addressing!\n");
    }
	else
		xhci_log(XHCI_LOG_INFO, "Controller supports 32-bit addressing!\n");

	/* Determine the context structure size */
	hcState->devContextSize = (hcState->caps->hccparams1 & XHCI_HCC1_CSZ) ? 64:32;
	hcState->dev = dev;

	/* Allocate and initialize the port information structures */
	hcState->port_info = (xhci_port_info *)calloc(sizeof(xhci_port_info), hcState->maxPorts);
	if(!hcState->port_info)
    {
        destroy_incomplete_hc_state(hcState);
        free(hcState);
		return driverOpNoMemory;
    }

	/* Translate the page size bitfield into a numerical page size (Spec 5.4.3) */
	hcState->pageSize =  1<<(getLsb64(hcState->ops->pageSize) + 12);

	/* Process any extended capabilties provided by the host controller, particularly the BIOS handoff
	* NOTE: This will also perform a BIOS -> OS handoff if supported/required by the controller
	 */
	exCapValue = hcState->caps->hccparams1>>16;

	result = xhci_process_excaps(hcState, exCapValue);
	if(result != driverOpSuccess)
	{
		free(hcState);
		return result;
	}

	/* Make sure the controller isn't processing anything before resetting it */
	while( !(hcState->ops->usbStatus & USB_STATUS_HCH))
	{
		hcState->ops->usbCommand &= ~USB_COMMAND_RUNSTOP;
		wait(16);
	}
	xhci_log(XHCI_LOG_BASIC, "Controller stopped...\n");

	/* Reset the host controller */
	if(hcState->ops->usbStatus & USB_STATUS_HCH)
		hcState->ops->usbCommand |= USB_COMMAND_RESET;
	else
	{
		xhci_log(XHCI_LOG_ERRORS, "Unable to reset controller!\n");
		free(hcState);
		return driverOpHardwareError;
	}

	/* Wait for the host controller reset to complete */
	counter = 0;
	do
	{
		if(counter > 10)
		{
			xhci_log(XHCI_LOG_ERRORS, "Unable to reset controller!\n");
			free(hcState);
			return driverOpHardwareError;
		}
		wait(50);
		counter++;
	} while(hcState->ops->usbCommand & USB_COMMAND_RESET);


	xhci_log(XHCI_LOG_BASIC, "Controller reset complete.\n");
	/* Initialize the device context array */
	result = allocate_mapped_page(&hcState->deviceContextArray, hcState);
	if(result != driverOpSuccess)
	{
		free(hcState);
		return result;
	}
	//memset( (void *)hcState->deviceContextArray.ptrs, 0, ARCH_PAGE_SIZE);

	xhci_log(XHCI_LOG_BASIC, "Device context array initialized.\n");

	if(hcState->deviceFlags & XHCI_CTRL_FLAGS_64BIT)
		hcState->ops->dcbaap64 = hcState->deviceContextArray.physicalBase;
	else
		hcState->ops->dcbaap32 = hcState->deviceContextArray.physicalBase;

	/* Enable all possible device slots */
	configure_value = hcState->ops->configure;
    configure_value &= ~0xFFU;
    configure_value |= hcState->maxSlots;

	hcState->ops->configure = configure_value;

	/* Allocate the necessary scratchpads, if any are required */
	if(hcState->maxScratchpads)
	{
		/* TODO: Adjust for controller page size requirements */
		uint32_t sbaPagesRequired;
		uint64_t pageSize = hcState->pageSize;

		sbaPagesRequired = sizeof(uint64_t)*hcState->maxScratchpads + (ARCH_PAGE_SIZE-1);
		sbaPagesRequired /= ARCH_PAGE_SIZE;

		/* Allocate and initialize scratchpads arrays */
		result = allocatePhysRegion(&hcState->scratchPadArray.virtualBase, &hcState->scratchPadArray.physicalBase, ARCH_PAGE_SIZE*sbaPagesRequired, 0, 0, dev);

		if(result != driverOpSuccess)
		{
			xhci_log(XHCI_LOG_ERRORS, "Unable to allocate scratch pad buffer array!\n");
			destroy_incomplete_hc_state(hcState);
			return result;
		}
		memset( (void *)hcState->scratchPadArray.ptrs, 0, ARCH_PAGE_SIZE*sbaPagesRequired);

		/* Allocate region mappings for the array pages */
		hcState->scratchPads = (mappedRegion *)calloc(sizeof(mappedRegion), hcState->maxScratchpads);
		if(!hcState->scratchPads)
		{
			xhci_log(XHCI_LOG_ERRORS, "Unable to allocate scratch pad mappings!\n");
			destroy_incomplete_hc_state(hcState);
			return driverOpNoMemory;
		}

		/* Allocate the scratchpad pages */
		for(int entry = 0; entry < hcState->maxScratchpads; entry++)
		{
			uint64_t virt, phys;
			result = allocatePhysRegion(&virt, &phys, pageSize, 0, 0, dev);
			if(result != driverOpSuccess)
			{
				xhci_log(XHCI_LOG_ERRORS, "Unable to allocate scratch pad entry!\n");
				destroy_incomplete_hc_state(hcState);
				return result;
			}
			hcState->scratchPads[entry].virtualBase = virt;
			hcState->scratchPads[entry].physicalBase = phys;
			memset((void *)virt, 0, pageSize);
			hcState->scratchPadArray.ptrs[entry] = phys;
		}
		hcState->deviceContextArray.ptrs[0] = hcState->scratchPadArray.physicalBase;
	}

	xhci_log(XHCI_LOG_BASIC, "Attempting to initialize command queue...\n");
	/* Initialize the command queue */
	xhci_create_ring( hcState, &hcState->hcCommandQueue );

	/* Initialize the top level locks */
	hcState->command_queue_lock = dapi_lock_create();
	hcState->slot_request_lock = dapi_lock_create();
	hcState->slot_state_lock = dapi_lock_create();
	hcState->default_addr_lock = dapi_lock_create();

	if(!hcState->command_queue_lock || !hcState->slot_request_lock || !hcState->slot_state_lock)
    {
        destroy_incomplete_hc_state(hcState);
        return driverOpNoMemory;
    }

	if(hcState->deviceFlags & XHCI_CTRL_FLAGS_64BIT)
    {
        uint64_t crc_value;
        crc_value = hcState->ops->commandRingControl64 & XHCI_OP_CRC_PRESERVE;
        crc_value |= xhci_ring_get_base(hcState->hcCommandQueue) | hcState->hcCommandQueue->cycleState;
		hcState->ops->commandRingControl64 = crc_value;
    }
	else
    {
        uint32_t crc_value;
        crc_value = hcState->ops->commandRingControl32 & XHCI_OP_CRC_PRESERVE;
        crc_value |= xhci_ring_get_base(hcState->hcCommandQueue) | hcState->hcCommandQueue->cycleState;
		hcState->ops->commandRingControl32 = crc_value;
    }

	xhci_log(XHCI_LOG_INFO, "Command queue initialized.\n");

	/* Allocate the potential event table memory mappings */
	hcState->eventTables = (mappedRegion *)calloc(sizeof(mappedRegion), hcState->maxInterrupters);
	if(!hcState->eventTables)
	{
		destroy_incomplete_hc_state(hcState);
		return driverOpNoMemory;
	}

	/* Allocate the potential event segment memory mappings */
	hcState->tableMappings = (mappedRegion *)calloc(sizeof(mappedRegion), hcState->maxInterrupters*EVENT_RING_PER_PAGE);
	if(!hcState->tableMappings)
	{
		destroy_incomplete_hc_state(hcState);
		return driverOpNoMemory;
	}

	/* Allocate and initialize the host controller's event table, segment and interrupter */
	result = xhci_create_event_table(hcState, 0);
	if(result != driverOpSuccess)
        xhci_log(XHCI_LOG_ERRORS, "Unable to create event table!\n");
	result = xhci_add_event_table_ring(hcState, 0);
	if(result != driverOpSuccess)
		xhci_log(XHCI_LOG_ERRORS, "Unable to create event ring!\n");

	result = xhci_enable_event_ring(hcState, 0);
	if(result != driverOpSuccess)
		xhci_log(XHCI_LOG_ERRORS, "Unable to enable event ring!\n");

	xhci_log(XHCI_LOG_INFO, "Controller enabled with %u ports, %u slots and %u interrupters.\n", hcState->maxPorts, hcState->maxSlots, hcState->maxInterrupters);

	/* Attempt to enable MSI interrupts */

	interrupt_count = hcState->maxInterrupters;
	result = registerInterruptHandler(&xhci_interrupt_func, hcState, &interrupt_count, dev);
	if(result != driverOpSuccess)
	{
		xhci_log(XHCI_LOG_ERRORS, "Unable to register XHCI driver interrupt handler!\n");
		return result;
	}

	hcState->activeInterrupters = interrupt_count;

	/* Make sure bus mastering is enabled */
	result = pciEnableBusMaster(desc->address);
	if(result != busOpSuccess)
	{
	    xhci_log(XHCI_LOG_ERRORS, "Failed to enable PCI Bus mastering.\n");
		free(hcState);
		return result;
	}

	/* Output controller state structure as the context parameter */
	*contextRet = hcState;

	hcState->ops->usbCommand |= USB_COMMAND_INT_ENABLE;
	wait(50);

	result = xhci_enable_interrupter(hcState, 0);
	if(result != driverOpSuccess)
		xhci_log(XHCI_LOG_ERRORS, "Unable to enable interrupter!\n");

    if( xhci_register_controller(hcState, usbVersion>>4, usbVersion & 0xF) != busRegisterApproved)
	{
		xhci_log(XHCI_LOG_ERRORS, "Failed to register controller with bus coordinator!\n");
		destroy_incomplete_hc_state(hcState);
		return driverOpNoAccess;
	}

	/* Enable the controller's ability to process TRBs, Events and issue interrupts */
	hcState->ops->usbCommand |= USB_COMMAND_RUNSTOP;
	xhci_log(XHCI_LOG_INFO, "Controller initialized!\n");
    result = xhci_rh_register_hubs(hcState);
    if(result != driverOpSuccess)
    {
        xhci_log(XHCI_LOG_ERRORS, "Failed to initialize root hubs for controller @ %X\n", desc->address);
        return result;
    }

	return driverOpSuccess;
}

driverOperationResult xhci_deinit(void *context)
{
	//xhci_controller_state *hcState = (xhci_controller_state *)context;
	return driverOpSuccess;
}

driverOperationResult xhci_enum(void *context, void *identifyFunc)
{
	//xhci_controller_state *hcState = (xhci_controller_state *)context;
	return driverOpSuccess;
}
