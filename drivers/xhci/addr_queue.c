/* Address request queue implementation
 * A simple FIFO queue that holds slot values to assign addresses
 *
 * Author: Joseph Kinzel */

#include "xhci_struct.h"

/*! Pushes a slot onto the address queue
 *  \param queue Target queue to push the value onto
 *  \param slot A non-zero slot value
 */
void addr_queue_push(addr_queue *queue, const uint8_t slot)
{
    addr_queue_link *link;

    link = (addr_queue_link *)calloc(1, sizeof(addr_queue_link));
    link->slot = slot;

    if(queue->tail)
        queue->tail->next = link;
    else
        queue->head = link;

    queue->tail = link;
}

/*! Pops a value off the address queue
 *  \param queue Source queue
 *  \return The slot requesting an address or zero if the queue is empty
 */
uint8_t addr_queue_pop(addr_queue *queue)
{
    uint8_t value;
    addr_queue_link *link;

    if(!queue->head)
        return 0;

    link = queue->head;
    queue->head = link->next;
    if(!queue->head)
        queue->tail = NULL;

    value = link->slot;
    free(link);

    return value;
}

/*! Tests if an address queue is empty or not
 *  \param queue Target queue
 *  \return A non-zero value is the queue is empty or zero if there are items in the queue
 */
int addr_queue_empty(addr_queue *queue)
{
    return (!queue->head);
}
