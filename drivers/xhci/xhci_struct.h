#ifndef XHCI_STRUCT_H_
#define XHCI_STRUCT_H_

#include <driverapi.h>
#include "xhci_regs.h"
#include "xhci_trb.h"
#include "xhci_context.h"

#define ENDPOINT_DISABLED                                  0x00U
#define ENDPOINT_DISABLING                                 0x01U
#define ENDPOINT_HALTED                                    0x02U
#define ENDPOINT_STOPPED                                   0x03U
#define ENDPOINT_CONFIGURING                               0x04U
#define ENDPOINT_CONFIGURED                                0x05U

#define ENDPOINT_GET_STATUS(x)                             ((x) & 0xF)
#define ENDPOINT_SET_TYPE(x)                               ((x) << 4)
#define ENDPOINT_GET_TYPE(x)                               ((x>>4) & 0x3)

#define XHCI_ENDPOINT_OUT_OFF                              0x00U
#define XHCI_ENDPOINT_IN_OFF                               0x01U

#define XHCI_CTRL_FLAGS_64BIT                              0x01U

#define USB_DEV_DESC_SZ                                    18U

//Segment data descriptor
typedef struct COMPILER_PACKED
{
	volatile uint64_t segmentBase;
	volatile uint16_t ringSegmentSize, _reserved[3];
} xhci_event_ring_segment;

typedef struct COMPILER_PACKED
{
	union
	{
		uint64_t virtualBase;
		volatile uint64_t *ptrs;
		volatile commandTRB *commands;
		volatile eventTRB *events;
		volatile transferTRB *transfers;
		volatile softwareIssuedTRB *trbs;
		volatile otherTRB *trbOther;
		volatile xhci_event_ring_segment *ringSegments;
		volatile devContext *contexts;
	};
	uint64_t physicalBase;
} mappedRegion;


typedef struct
{
	uint32_t resultCode, bytesTransferred, bytesRemaining, offset;
	uint64_t *pages;
	softwareIssuedTRB *last_trb_start;
} transferRequest;

#define XHCI_EST_ENTRIES_PER_PAGE      (ARCH_PAGE_SIZE/sizeof(xhci_event_ring_segment))

typedef struct _slotRequest
{
	mappedRegion icc;
	struct _slotRequest *next, *prev;
	uint32_t routeInfo;
	uint8_t rh_port, parentPort, parentSlot, port_speed, psi_value;
	sync_object *sync;
} slotRequest;

typedef struct
{
    uint8_t protocol;
	uint8_t count;
	uint8_t slot_type;
	uint8_t rh_index;
	uint32_t flags;
	volatile uint32_t *speeds;
} xhci_port_info;

typedef struct COMPILER_PACKED _linkedRegion
{
	mappedRegion region;
	struct _linkedRegion *next, *prev;
} linkedRegion;

typedef struct COMPILER_PACKED _ringRegion
{
    mappedRegion region;
    struct _ringRegion *next, *prev;
    int region_eq_index, region_dq_index;
} ringRegion;

/* TRB Ring object */
typedef struct COMPILER_PACKED COMPILER_ARCH_ALIGN
{
    void *rqLock;
	volatile softwareIssuedTRB *completedPtr;

	ringRegion *enqueueRegion, *completedRegion;
	ringRegion *ringStem;
	uint32_t commandsInFlight;
	uint16_t regionCount;
	uint8_t cycleState;
} ringQueue;

/* Address link entries */
typedef struct _addr_queue_link
{
    struct _addr_queue_link *next;
    uint8_t slot;
} addr_queue_link;

/* Address queue */
typedef struct
{
    addr_queue_link *head, *tail;
} addr_queue;

/* Command sync queue link */
typedef struct _sync_link
{
	sync_object *sync_obj;
	struct _sync_link *next;
	volatile commandTRB *target;
} sync_link;

/* Command sync queue */
typedef struct
{
	sync_link *head, *tail;
} sync_queue;

/* Memory region tree node */
typedef struct COMPILER_PACKED _physInstance
{
	linkedRegion *region;
	struct _physInstance *left, *right;
	ringQueue *queue;
	char type, lHeight, rHeight;
} physInstance;

/* Device slot state */
typedef struct _xhci_slot_state
{
    void *slot_lock;
	devContext *input_context;
	uintptr_t icc_phys_base;
	ringQueue **endpoint_rings;
	uint8_t *endpoint_state;
	sync_queue command_wait;
	uint8_t ep_count, slot_speed, parent_port, parent_slot, dev_desc[USB_DEV_DESC_SZ];
} xhci_slot_state;

/* XHCI Controller Instance Context */
typedef struct
{
	/* Registers */
	volatile xhci_cap_regs *caps;
	volatile xhci_operational_regs *ops;
	volatile xhci_runtime_regs *rts;
	volatile uint32_t *dbs;

	uint32_t hcTrbIndex;

	xhci_port_info *port_info;

	/* Host controller data structures */
	mappedRegion deviceContextArray;
	ringQueue *hcCommandQueue;
	mappedRegion scratchPadArray;
	mappedRegion *scratchPads;

	mappedRegion *devContextRegions;
    /* Command TRBs allocated but not yet ready for dispatch */
	uint32_t unfinishedCommands;
	/* Event ring related data structures */
	mappedRegion *eventTables;
	uint64_t *eventLastPtrs;
	uint32_t *eventLastTableIndices;
	uint8_t *eventCCS;
	mappedRegion *tableMappings;
    /* Host controller context locks */
	void *command_queue_lock;
	void *slot_request_lock;
	void *slot_state_lock;
    void *default_addr_lock;

	/* Slot request queue */
	slotRequest *srHead, *srTail, *srDefault;

	/* Slot state structures */
	xhci_slot_state *slots;
	uint8_t *dev_to_slot_map;
	/* Root hub state */
	void **root_hub_info;
	int root_hub_count;


	/* Device/Driver addressing */
	DEVICE_HANDLE dev;
	BUS_HANDLE controller_handle;
	uint32_t controller_id;
	busAddressDesc busAddr;
	/* Configuration parameters */
	uint64_t deviceFlags;
	uint32_t pageSize;
	uint16_t maxInterrupters, maxScratchpads, activeInterrupters;
	uint8_t maxPorts, maxSlots, enSlots, devContextSize, maxRootHubAddress;
	uint8_t usb_major_supported, usb_minor_supported;
	/* Default address structures */
	volatile uint8_t default_addr_slot;
	addr_queue default_addr_queue;

} xhci_controller_state;

#endif
