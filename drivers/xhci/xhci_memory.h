#ifndef XHCI_MEMORY_H_
#define XHCI_MEMORY_H_

#include <arch.h>
#include "xhci_controller.h"

void xhci_process_transfer_request(xhci_controller_state *hcState, transferRequest *request,
  const uint8_t completion_code, const uint32_t bytes_transferred, const uint8_t slot_id, const uint8_t ep_id);

#endif
