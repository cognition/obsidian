#include "xhci_event.h"
#include "xhci_error_handle.h"
#include "xhci_cq.h"
#include "xhci_context.h"
#include "xhci_memory.h"
#include "xhci_transaction.h"
#include "xhci_ports.h"
#include "xhci_bus.h"
#include "xhci_log.h"
#include "sync_queue.h"
#include <usb.h>
#include "addr_queue.h"
#include "xhci_root_hub.h"

void hcPrintInterrupterInfo(xhci_controller_state *hcState, const int interrupter_index)
{
	uint16_t pciStatus;
	volatile xhci_interrupt_reg *ireg;
	ireg = &hcState->rts->interrupter[interrupter_index];

	pciReadWord(&pciStatus, hcState->busAddr.address, 0x6);
    xhci_log(XHCI_LOG_INTERNAL, "INT[%u] - IMAN: %X IMOD: %X EC: %u BA: %X DQ: %X\n", interrupter_index, ireg->iman, ireg->imod, ireg->eventSegCount,
	       ireg->eventRingSegmentTableBase32, ireg->eventRingDequeuePtr32);


	xhci_log(XHCI_LOG_INTERNAL, "USBCMD: %X STATUS: %X PCI-STATUS: %X CMDRING: %X DC: %X\n", hcState->ops->usbCommand,
	       hcState->ops->usbStatus, pciStatus, hcState->ops->commandRingControl64, hcState->ops->dcbaap64);
}

/*! Retrieves a valid TRB on the host controller's command ring
 *  \param hcState Host controller's state structure
 *  \return Pointer to the newly allocated TRB
 */
volatile commandTRB *xhci_hc_get_trb(xhci_controller_state *hcState, uint8_t *cycleState)
{
	volatile softwareIssuedTRB *trb;

    dapi_lock_acquire(hcState->command_queue_lock);
	hcState->unfinishedCommands++;
	xhci_get_ring_trb(&trb, cycleState, hcState, hcState->hcCommandQueue);
    dapi_lock_release(hcState->command_queue_lock);
    xhci_log(XHCI_LOG_INTERNAL, "Retrieved HC TRB: %p TRB Cycle: %u HC cycle: %u HC Ring base: %X\n", trb, trb->command.noOp.cycleFlags, hcState->hcCommandQueue->cycleState,
             xhci_ring_get_base(hcState->hcCommandQueue));
	return &trb->command;
}

uint16_t hcGetBestInterrupter(xhci_controller_state *hcState)
{
	//TODO: Actually implement this so it works with MSI And MSI-X
	return 0;
}

/*! Queues up a slot request object
 *  \param hcState Host controller state structure
 *  \param port Root hub port the device is behind
 *  \param portSpeed Controller port speed value
 *  \param routeInfo USB3 routing information
 *  \return driverOpSuccess if successful.
 */
driverOperationResult queueSlotRequest(xhci_controller_state *hcState, slotRequest *sr)
{
	xhci_log(XHCI_LOG_INTERNAL, "Attempting to acquire slot request lock...\n ");
    dapi_lock_acquire(hcState->slot_request_lock);

	/* Queue up the request */
	if(!hcState->srHead)
		hcState->srHead = sr;
	else
		hcState->srTail->next = sr;

	sr->prev = hcState->srTail;
	hcState->srTail = sr;

	dapi_lock_release(hcState->slot_request_lock);
    xhci_log(XHCI_LOG_INTERNAL, "Slot request lock released!\n");
	return driverOpSuccess;
}


/*! Ring's the HC's command doorbell
 *  \param hcState Host controller state structure
 */
void xhci_hc_signal_work_pending(xhci_controller_state *hcState, const int units)
{
    //hcState->unfinishedCommands -= units;
    if( !atomic_sub_pre( &hcState->unfinishedCommands, units) )
    {
        /* Ring the host controller's command doorbell */
        hcState->dbs[0] = 0;
        xhci_log(XHCI_LOG_INTERNAL, "HC WP - Doorbell rung!\n");
    }
    else
    {
        xhci_log(XHCI_LOG_INTERNAL, "WP %u excess commands outstanding!\n", hcState->unfinishedCommands);
    }
}

driverOperationResult xhci_queue_address_device(xhci_controller_state *hcState, const int slot, const uint8_t bsr, volatile uint32_t *commandIssueCount)
{
    volatile commandTRB *cmd;
    uint8_t cycleState;
    xhci_slot_state *slot_state;

    slot_state = &hcState->slots[slot];

    xhci_log(XHCI_LOG_OPSRES, "Attempting to address device on slot %u with BSR value: %X\n", slot, bsr);
	cmd = xhci_hc_get_trb(hcState, &cycleState);
	if(!cmd)
		return driverOpResources;


	cmd->addressDevice.trbType = WRITE_TRB_TYPE_ADJ(TRB_ADDRESS_DEVICE);
	cmd->addressDevice.trbType |= bsr;
	cmd->addressDevice.icPtr = slot_state->icc_phys_base;
	cmd->addressDevice.slotId = slot;
	cmd->addressDevice.cycle = 0;
    xhci_trb_set_cycle(cmd, cycleState);

    (*commandIssueCount)++;

    return driverOpSuccess;
}

driverOperationResult xhci_queue_disable_slot(xhci_controller_state *hcState, const int slotNumber)
{
    uint8_t cycleState;
	volatile commandTRB *cmd = xhci_hc_get_trb(hcState, &cycleState);
	if(!cmd)
		return driverOpResources;
	cmd->disableSlot.trbType = TRB_DISABLE_SLOT<<2;
	cmd->disableSlot.slotId = slotNumber;

	xhci_trb_set_cycle(cmd, cycleState);
	return driverOpSuccess;
}

driverOperationResult xhci_queue_stop_endpoint(xhci_controller_state *hcState, const int slotNumber, const uint8_t endpoint, sync_object *sync)
{
    uint8_t cycleState;
	xhci_slot_state *slot_state;
	driverOperationResult res;

	volatile commandTRB *cmd = xhci_hc_get_trb(hcState, &cycleState);
	if(!cmd)
		return driverOpResources;
	cmd->stopEndpoint.trbType = WRITE_TRB_TYPE_ADJ(TRB_STOP_ENDPOINT);
	cmd->stopEndpoint.slotId = slotNumber;
	cmd->stopEndpoint.endpointId = endpoint;

	slot_state = &hcState->slots[slotNumber];

	dapi_lock_acquire(slot_state->slot_lock);

	res = driverOpSuccess;
	if(sync)
    {
        if(sync_attach(sync))
            sync_queue_push(&slot_state->command_wait, cmd, sync);
        else
        {
            cmd->noOp.trbType = WRITE_TRB_TYPE_ADJ(TRB_NO_OP2);
            res = driverOpSyncError;
        }
    }

    dapi_lock_release(slot_state->slot_lock);

	xhci_trb_set_cycle(cmd, cycleState);
	return res;
}

driverOperationResult xhci_disable_slot_endpoints(xhci_controller_state *hcState, const int slot, volatile uint32_t *commandIssueCount)
{
	slotContext *sc;
	uint8_t entryCount;
	sc = (slotContext *)hcState->devContextRegions[slot].virtualBase;
	entryCount = XHCI_SLOT_CONTEXT_ENTRIES(sc->contextInfo);

	for(uint8_t endpoint = 0; endpoint < entryCount; endpoint++)
	{
		driverOperationResult result = xhci_queue_stop_endpoint(hcState, slot, endpoint, NULL);
		if(result != driverOpSuccess)
			return result;
        (*commandIssueCount)++;
	}
	return driverOpSuccess;
}

int xhci_get_slot_type(xhci_controller_state *hcState, const uint8_t port, const uint8_t portSpeed)
{
	xhci_port_info *port_information;

	port_information = &hcState->port_info[port];

	return port_information->slot_type;
}

/*! Issues an enable slot command to the XHCI controller's command ring
 *  \param hcState Host controller instance
 *  \param rh_port Root hub port the device is connected to
 *  \param
 *  \param portSpeed USB speed (low, full, high, superspeed, etc.) of the device
 *  \param routeInfo USB3.0 route information
 *  \return driverOpResources if the command could not allocate a TRB or slot request, driverOpSuccess otherwise
 */
driverOperationResult xhci_hc_enable_slot(xhci_controller_state *hcState, const uint8_t port, const uint8_t rh_port, const uint8_t port_speed, const uint8_t psi_value,
                                          const uint32_t routeInfo, const uint8_t parent_slot, sync_object *sync)
{
	int slotType;
    uint8_t cycleState;
    driverOperationResult status;
	slotRequest *sr;
    driverOperationResult result;

	volatile commandTRB *cmd = xhci_hc_get_trb(hcState, &cycleState);
	if(!cmd)
		return driverOpResources;

	/* Allocate and initialize the slotRequest object */
	sr = (slotRequest *)calloc(1, sizeof(slotRequest));
	if(!sr)
		return driverOpNoMemory;

	sr->rh_port = rh_port;
	sr->port_speed = port_speed;
	sr->routeInfo = routeInfo;
	sr->psi_value = psi_value;
	sr->parentSlot = parent_slot;
	sr->parentPort = port;
	sr->sync = sync;
	sr->next = NULL;

	/* Allocate space for the ICC */
	result = allocate_mapped_page(&sr->icc, hcState);
	if(result != driverOpSuccess)
		return result;


	slotType = xhci_get_slot_type(hcState, port, port_speed);

    xhci_log(XHCI_LOG_OPSRES, "Enabling slot for device on port %u Slot type: %u Cycle state: %u\n", port, slotType, cycleState);

	cmd->enableSlot.trbType = WRITE_TRB_TYPE_ADJ(TRB_ENABLE_SLOT);
	cmd->enableSlot.slotType = slotType;
    cmd->enableSlot._reserved[0] = 0;
    cmd->enableSlot._reserved[1] = 0;
    cmd->enableSlot._reserved[2] = 0;

	status = queueSlotRequest(hcState, sr);


    xhci_trb_set_cycle(cmd, cycleState);
    xhci_hc_signal_work_pending(hcState, 1);

    return status;
}

/*! Dispatches the initial request to bind a device to a specific slot and move it into the default state
 *  \param hcState XHCI Context instance
 *  \param sr Slot request information structure
 *  \param slot Number of newly allocated slot that ready to be bound to a device
 *  \param commandIssueCount Pointer to the number of new commands that have been issued on the command queue during the current host controller event processing session
 *  \return driverOpSuccess if successful or driverOpResources if the host controller lacked the resources to issue the address command
 */
driverOperationResult xhci_address_device(xhci_controller_state *hcState, slotRequest *sr, const uint8_t slot, volatile uint32_t *commandIssueCount)
{
	inputControlContext *icc;
	slotContext *sc;
	endpointContext *epc;
	xhci_slot_state *slot_state, *parent_slot_state;
	mappedRegion *dev_context_ptr;
	int mps;

	icc = (inputControlContext *)sr->icc.virtualBase;
	sc = (slotContext *)(sr->icc.virtualBase + hcState->devContextSize);
	epc = (endpointContext *)(sr->icc.virtualBase + hcState->devContextSize*2);


	/* Initialize the interrupt control context, Section 6.2.5.1 */
	icc->addContext = ICC_FLAG_ENDPOINT(0) | ICC_FLAG_ENDPOINT(1);

	/* Initialize the slot context, Section 6.2.2.1 */
	sc->routeString[0] = sr->routeInfo & 0xFFU;
	sc->routeString[1] = (sr->routeInfo >> 8) & 0xFFU;
	sc->routeString[2] = ((sr->routeInfo >> 16) & 0x0FU);

	/* Speed value should be set when using as input for the address device command */
	sc->routeString[2] |= (sr->psi_value << 4);

	sc->contextInfo = SET_SLOT_CONTEXT_ENTRIES(1, 0);
	sc->interrupterAndTTT = SET_SLOT_CONTEXT_INTERRUPTER( hcGetBestInterrupter(hcState), 0);

    sc->rootPortNum = sr->rh_port;



	/* Extract root port information */
    if(sr->parentSlot & XHCI_DEV_IS_RH)
    {
        uint32_t sc_value;

        sc_value = hcState->ops->portRegSet[sr->rh_port].portStatusControl;

        xhci_log(XHCI_LOG_INTERNAL, "Address device - provided PSI: %u Actual port PSI: %u Route info: %X Controller version: %X\n", sr->psi_value, XHCI_PORTSC_PORTSPEED(sc_value), sr->routeInfo, hcState->caps->hciVersion );

        sc->parentSlotId = 0;
        sc->parentPortNumber = 0;
    }
    else
    {
        parent_slot_state = &hcState->slots[sr->parentSlot];

        xhci_log(XHCI_LOG_INTERNAL, "Address device - Non RH connected device on slot %u pslot speed: %u Device speed: %u Device port: %u\n", slot, parent_slot_state->slot_speed,
                 sr->port_speed, sr->parentPort);

        if( parent_slot_state->slot_speed == USB_SPEED_HIGH )
        {
            if(sr->port_speed < USB_SPEED_HIGH)
            {
                /* A slot context requires extra information if a hub transaction translator involved */
                volatile slotContext *parent_context;

                parent_context = (volatile slotContext *)xhci_get_dev_context(hcState, sr->parentSlot, 0);

                sc->parentPortNumber = sr->parentPort;
                sc->parentSlotId = sr->parentSlot;

                xhci_log(XHCI_LOG_INTERNAL, "Enabling device TT device behind hub on slot %u with parent port number %u\n", sr->parentSlot, sr->parentPort);

                /* MTT flag must be set if we have a low or full speed device behind a high speed hub using MTT */
                if(parent_context->contextInfo & XHCI_SLOT_CONTEXT_MTT)
                    sc->contextInfo |= XHCI_SLOT_CONTEXT_MTT;
            }
            else
            {
                sc->parentPortNumber = 0;
                sc->parentSlotId = 0;
            }
        }
        else if( parent_slot_state->slot_speed > sr->port_speed)
        {
            /* ESS+ Devices require the parent port and slot fields to be initialized if the device is behind a higher speed hub */
            sc->parentPortNumber = sr->parentPort;
            sc->parentSlotId = sr->parentSlot;
        }
        else
        {

            sc->parentPortNumber = 0;
            sc->parentSlotId = 0;
        }
    }

	dev_context_ptr = &hcState->devContextRegions[slot];

	/* Allocate the device context region or reinitialize it */
	if(!dev_context_ptr->physicalBase)
	{
		driverOperationResult result;

		result = allocate_mapped_page(dev_context_ptr, hcState);
		if(result != driverOpSuccess)
			return result;

		hcState->deviceContextArray.ptrs[slot] = dev_context_ptr->physicalBase;
	}
	else
		memset( (void *)dev_context_ptr->ptrs, 0, ARCH_PAGE_SIZE);

	/* Initialize the slot tracking state */
	slot_state = &hcState->slots[slot];
	memset(slot_state, 0, sizeof(xhci_slot_state));

	slot_state->endpoint_rings = (ringQueue **)calloc(USB_MAX_ENDPOINTS, sizeof(ringQueue *));

	slot_state->endpoint_state = (uint8_t *)calloc(1, USB_MAX_ENDPOINTS);

	/* Create the command ring */
	xhci_create_ring(hcState, &slot_state->endpoint_rings[0]);

	slot_state->input_context= (devContext *)icc;
	slot_state->ep_count = 1;
    slot_state->slot_speed = sr->port_speed;
    slot_state->parent_port = sr->parentPort;
    slot_state->parent_slot = sr->parentSlot;
    slot_state->slot_lock = dapi_lock_create();
    slot_state->icc_phys_base = sr->icc.physicalBase;
    slot_state->endpoint_state[0] = ENDPOINT_SET_TYPE(usbTransactionControl) | ENDPOINT_CONFIGURED;

    sync_queue_push(&slot_state->command_wait, NULL, sr->sync);

	/* Initializer the default control endpoint context, Section 6.2.3.1 */
	epc->epTypeField = XHCI_EPC_EP_TYPE(XHCI_EPC_EP_CONTROL) | XHCI_EPC_CERR(3);

	/* Set the default maximum packet size based on the attached device speed */
	switch( sr->port_speed )
	{
        case USB_SPEED_LOW:
            mps = 8;
            break;
        case USB_SPEED_FULL:
            mps = 8;
            break;
        case USB_SPEED_HIGH:
            mps = 64;
            break;
        case USB_SPEED_SUPER:
        case USB_SPEED_ESUPER_1X1:
        case USB_SPEED_ESUPER_1X2:
        case USB_SPEED_ESUPER_2X1:
        case USB_SPEED_ESUPER_2X2:
            mps = 512;
            break;
        default:
            mps = 8;
            break;
	}

	epc->maxPacketSize = mps;
	epc->maxBurstSize = 0;
	epc->interval = 0;

	epc->trDequeuePtr = xhci_ring_get_base( slot_state->endpoint_rings[0] ) | XHCI_EP_DCS;

    dapi_lock_acquire(hcState->default_addr_lock);

    /* Queue the command to move the slot/device into the default state if the default address isn't currently in use */
    if(addr_queue_empty(&hcState->default_addr_queue) && !hcState->default_addr_slot)
    {
        driverOperationResult status;

        status = xhci_queue_address_device(hcState, slot, TRB_AD_BSR, commandIssueCount);
        if(status != driverOpSuccess)
        {
            free(sr);
            return driverOpResources;
        }
    }
    else
    {
        /* The default address slot is in use, queue up an ownership request */
        addr_queue_push(&hcState->default_addr_queue, slot);
    }
    dapi_lock_release(hcState->default_addr_lock);

    xhci_log(XHCI_LOG_OPSRES, "Addressing device on slot with parent slot %u parent port %u RH port %u\n", slot, sr->parentSlot, sr->parentPort, sr->rh_port);
	/* We're done with the slot request */
	free(sr);
	return driverOpSuccess;
}

/*! Retrieves a slot request entry and uses the provided slot to address the device
 *  \param hcState Host controller context instance
 *  \param slot Slot allocated with the enable slot command that is to be initialized with the retrieve device information
 *  \return driverOpBadState if there is no slot requests queued, or a value from hcAddressDevice
 */
driverOperationResult xhci_answer_slot_req(xhci_controller_state *hcState, const uint8_t slot, volatile uint32_t *commandIssueCount)
{
    dapi_lock_acquire(hcState->slot_request_lock);
	/* Remove a request from the queue */
	slotRequest *sr = hcState->srHead;
	if(!sr)
    {
        dapi_lock_release(hcState->slot_request_lock);
		return driverOpBadState;
    }
	else
	{

		hcState->srHead = sr->next;
		if(!hcState->srHead)
			hcState->srTail = NULL;
		else
			hcState->srHead->prev = NULL;


        dapi_lock_release(hcState->slot_request_lock);
		/* Attempt to address the requested device to the newly allocated slot */
		return xhci_address_device(hcState, sr, slot, commandIssueCount);
	}
}

volatile commandTRB *xhci_hc_get_command_ptr(xhci_controller_state *hcState, volatile commandCompletionEventTRB *cce)
{
	volatile softwareIssuedTRB *trb_result;
	if(!xhci_get_completed_trb(&trb_result, cce->commandTrbPtr, hcState->hcCommandQueue))
		return NULL;
	else
		return &(trb_result->command);
}

driverOperationResult hcTestCommandNoOp(xhci_controller_state *hcState)
{
    uint8_t cycleState;
	volatile commandTRB *trb = xhci_hc_get_trb(hcState, &cycleState);
	if(!trb)
		return driverOpResources;

	memset((void *)trb, 0, sizeof(commandTRB));

	trb->noOp.trbType = WRITE_TRB_TYPE_ADJ(TRB_NO_OP2);
    xhci_trb_set_cycle(trb, cycleState);

	xhci_hc_signal_work_pending(hcState, 1);
	return driverOpSuccess;
}

/*! Registers an assigned usb device address mapping to slot mapping
 *  \param hcState Host controller state structure
 *  \param slot Host controller slot with the newly assigned address
 */
void xhci_register_device_address(xhci_controller_state *hcState, const int slot, volatile uint32_t *commandIssueCount)
{
	uint8_t addr;
	volatile slotContext *active_info;
	xhci_slot_state *slot_info;
	sync_object *coord_signal;

	slot_info = &hcState->slots[slot];
	active_info = (slotContext *)xhci_get_dev_context(hcState, slot, 0);

	addr = active_info->usbDeviceAddress;

    /* Move onto the next slot */
    dapi_lock_acquire(hcState->default_addr_lock);

    /* Clear ownership of the default address slot if there are no other requests */
    if( addr_queue_empty(&hcState->default_addr_queue) )
        hcState->default_addr_slot = 0;
    else
    {
        /* Otherwise service the next pending request for ownership by issuing an address device command */
        driverOperationResult drv_status;

        hcState->default_addr_slot = addr_queue_pop(&hcState->default_addr_queue);
        drv_status = xhci_queue_address_device(hcState, hcState->default_addr_slot, TRB_AD_BSR, commandIssueCount);
        if(drv_status != driverOpSuccess)
        {
            xhci_log(XHCI_LOG_ERRORS, "Unable to issue address device command to host controller #%u\n", hcState->controller_id);
        }
    }

    dapi_lock_release(hcState->default_addr_lock);


	dapi_lock_acquire(hcState->slot_state_lock);
	/* Register to the address to slot mapping in the local map */
	hcState->dev_to_slot_map[addr] = slot;
    dapi_lock_release(hcState->slot_state_lock);

	coord_signal = sync_queue_pop(&slot_info->command_wait);
	xhci_log(XHCI_LOG_INTERNAL, "Newly addressed device @ slot %u and address: %X Entry: %u\n", slot, addr, hcState->dev_to_slot_map[addr]);
	sync_object_signal(coord_signal, addr  );
}

/*! Obtains the first eight bytes of the USB device descriptor
 *  \param hcState Host controller state instance
 *  \param slot Controller slot of the device
 */

static driverOperationResult xhci_obtain_dev_desc(xhci_controller_state *hcState, const uint8_t slot)
{
    busOperationResult bus_result;
    usbControlTransaction ct;
    xhci_slot_state *slot_state;
    volatile slotContext *sc;
    slot_state = &hcState->slots[slot];

    sc = (volatile slotContext *)xhci_get_dev_context(hcState, slot, 0);

    /* Issue a GET_DESCRIPTOR request on the default control endpoint */
    ct.bm_request_type = USB_REQ_TYPE_DEVICE | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_STANDARD;
    ct.b_request = USB_CONTROL_REQ_GET_DESCRIPTOR;
    ct.w_value = USB_DESCRIPTOR_DEVICE;
    ct.w_index = 0;
    ct.w_length = 8;
    ct.data_ptr = &slot_state->dev_desc[0];

    xhci_log(XHCI_LOG_INTERNAL, "Obtain device descriptor - Slot state: %u\n", XHCI_SLOT_CONTEXT_STATE(sc->slotState));

    bus_result = xhci_queue_control_transaction(hcState, slot, 0, USB_TRANSACTION_IN, &ct, XHCI_INIT_DESC_REQ, NULL);
    if(bus_result != busOpSuccess)
    {
        xhci_log(XHCI_LOG_ERRORS, "Failed to queue GET_DESCRIPTOR transaction for the device descriptor of slot %u\n", slot);
        return driverOpBusError;
    }
    else
    {
        hcState->dbs[slot] = 1;
        xhci_log(XHCI_LOG_OPSRES, "Retrieving device descriptor to determine mps for slot %u\n", slot);

        return driverOpSuccess;
    }
}

/*! Requests an actual address state of a device that's currently default state and moves it into the addressed state
 *
 *  \param hcState XHCI context instance
 *  \param slot Slot number of the device
 *  \param request Transaction structure for the 8-byte GET_DESCRIPTOR request
 *  \param commandIssueCount Pointer to the count of newly issued commands for the currently HC event processing session
 *  \return driverOpSuccess if successful
 */
static driverOperationResult xhci_assign_address(xhci_controller_state *hcState, const uint8_t slot, activeTransaction *request, volatile uint32_t *commandIssueCount)
{
    volatile endpointContext *ec;
    xhci_slot_state *slot_state;
    volatile usbDeviceDescriptor *dev_desc;
    ringQueue *rq;
    int mps;

    slot_state = &hcState->slots[slot];
    rq = xhci_get_endpoint_ring(hcState, slot, 0, USB_TRANSACTION_IN);

    dev_desc = (volatile usbDeviceDescriptor *)slot_state->dev_desc;
    ec = xhci_get_icc_context(hcState, slot, 2);

    /* Maximum packet size is interpreted different for ESS+ devices */
    if(dev_desc->bcdVerMajor >= 3)
    {
        mps = 2;
        mps <<= dev_desc->bMaxPacketSize;
    }
    else
    {
        mps = dev_desc->bMaxPacketSize;
    }

    xhci_log(XHCI_LOG_OPSRES, "Device on slot %u has version of %X.%X and a maximum packet size of %u bytes\n", slot, dev_desc->bcdVerMajor, dev_desc->bcdVerMinor, mps);
    ec->maxPacketSize = mps;

    dapi_lock_acquire(rq->rqLock);
    /* Reinitialize the TRB queue */
    ec->trDequeuePtr = xhci_ring_get_eq(rq) | XHCI_EP_DCS;



    /* Free the TRBs */
	atomic_sub(&rq->commandsInFlight, request->trb_count);
	dapi_lock_release(rq->rqLock);
    free(request);

    /* Allocate and initialize the actual command TRB */
	return xhci_queue_address_device(hcState, slot, 0, commandIssueCount);
}

driverOperationResult xhci_hc_process_successful_command(xhci_controller_state *hcState, volatile commandCompletionEventTRB *cce, volatile uint32_t *commandIssueCount)
{
	uint64_t trbPhys;
	uint8_t slot, trbType;
    volatile slotContext *sc;
	trbPhys = cce->commandTrbPtr;
	slot = cce->slotId;
	volatile commandTRB *cmd;
	volatile softwareIssuedTRB *trb;
	driverOperationResult result;

	cmd = xhci_hc_get_command_ptr(hcState, cce);
	if(!cmd)
	{
		xhci_log(XHCI_LOG_ERRORS, "Unable to locate command pointer for command @ %X\n", trbPhys);
		return driverOpBadState;
	}

	trbType = READ_TRB_TYPE_ADJ(cmd->noOp.trbType);
    result = driverOpSuccess;

    trb = (volatile softwareIssuedTRB *)cmd;
    ++trb;
    xhci_update_ring_completed(hcState->hcCommandQueue, trb);

	switch(trbType)
	{
		case TRB_NO_OP2:
			xhci_log(XHCI_LOG_INFO, "Command NO OP encountered..\n");
			break;
		case TRB_ADDRESS_DEVICE:
            sc = xhci_get_dev_context(hcState, cce->slotId, 0);

            if( XHCI_SLOT_CONTEXT_STATE(sc->slotState) == XHCI_SC_STATE_ADDRESSED)
                xhci_register_device_address(hcState, slot, commandIssueCount);
            else
                result = xhci_obtain_dev_desc(hcState, slot);
            return result;
			break;
		case TRB_ENABLE_SLOT:
		    xhci_log(XHCI_LOG_OPSRES, "Enable slot command completed successfully!\n");

			result = xhci_answer_slot_req(hcState, slot, commandIssueCount);
			if(result != driverOpSuccess)
			{
				xhci_log(XHCI_LOG_ERRORS, "Failed to issued device address request! Code: %u\n", result);
				return result;
			}
			break;
		case TRB_DISABLE_SLOT:
			break;
		case TRB_STOP_ENDPOINT:
		    xhci_process_potential_command_sync(hcState, cmd, cce);
		    break;
		case TRB_CONFIGURE_ENDPOINT:
		    xhci_process_potential_command_sync(hcState, cmd, cce);
		    return driverOpSuccess;
		    break;
		case TRB_EVALUATE_CONTEXT:
		    xhci_log(XHCI_LOG_OPSRES, "Got completed command of type %u\n", trbType);
			xhci_process_potential_command_sync(hcState, cmd, cce);
			return driverOpSuccess;
			break;
		case TRB_RESET_ENDPOINT:
			break;
		case TRB_SET_TR_DEQUEUE:
			break;
		case TRB_RESET_DEVICE:
			break;
		case TRB_FORCE_EVENT:
			break;
		case TRB_NEGOTIATE_BANDWIDTH:
			break;
		case TRB_SET_LTV:
			break;
		case TRB_GET_PORT_BANDWIDTH:
			break;
		case TRB_GET_EXT_PROP:
			break;
		case TRB_SET_EXT_PROP:
			break;
	}

	return driverOpSuccess;
}

driverOperationResult xhci_process_event_error(xhci_controller_state *hcState, volatile eventTRB *event, void *context)
{
	uint8_t completion_code;

	completion_code = event->commandCompletion.commandParameter>>24;

	if(completion_code < COMP_CODE_VENDOR_INFO_START)
	{
		if(completion_code >= COMP_CODE_VENDOR_ERR_START)
			return xhci_process_undefined_err(hcState, event, context);
		else
		{
			switch(completion_code)
			{
				case COMP_CODE_INVALID:
					return xhci_process_invalid_error(hcState, event, context);
					break;
				case COMP_CODE_SUCCESS:
					break;
				case COMP_CODE_DATA_BUFFER_ERR:
					return xhci_process_dbe(hcState, event, context);
					break;
				case COMP_CODE_BABBLE_ERR:
					return xhci_process_babble(hcState, event, context);
					break;
				case COMP_CODE_USB_TRANSACTION_ERR:
					return xhci_process_bad_usb_transaction(hcState, event, context);
					break;
				case COMP_CODE_TRB_ERR:
					return xhci_process_trb_error(hcState, event, context);
					break;
				case COMP_CODE_STALL_ERR:
					return xhci_process_trb_error(hcState, event, context);
					break;
				case COMP_CODE_RESOURCE_ERR:
					return xhci_process_resource_error(hcState, event, context);
					break;
				case COMP_CODE_BANDWIDTH_ERR:
					return xhci_process_bandwidth_error(hcState, event, context);
					break;
				case COMP_CODE_NO_SLOTS_ERR:
					return xhci_process_no_slots(hcState, event, context);
					break;
				case COMP_CODE_INVALID_STREAM_ERR:
					return xhci_invalid_stream_error(hcState, event, context);
					break;
				case COMP_CODE_SLOT_NOT_ENABLE_ERR:
					return xhci_slot_not_enable_error(hcState, event, context);
					break;
				case COMP_CODE_ENDPOINT_NOT_ENABLE_ERR:
					return xhci_endpoint_not_enable_error(hcState, event, context);
					break;
				case COMP_CODE_SHORT_PACKET:
					return xhci_short_packet_cond(hcState, event, context);
					break;
				case COMP_CODE_RING_UNDERRUN:
					return xhci_ring_underrun(hcState, event, context);
					break;
				case COMP_CODE_RING_OVERRUN:
					return xhci_ring_overrun(hcState, event, context);
					break;
				case COMP_CODE_VF_EVENT_RING_FULL_ERR:
					return xhci_vf_ring_full(hcState, event, context);
					break;
				case COMP_CODE_PARAMETER_ERR:
					return xhci_parameter_error(hcState, event, context);
					break;
				case COMP_CODE_BANDWIDTH_OVERRUN_ERR:
					return xhci_bandwidth_overrun_error(hcState, event, context);
					break;
				case COMP_CODE_CONTEXT_STATE_ERR:
					return xhci_context_state_error(hcState, event, context);
					break;
				case COMP_CODE_NO_PING_ERR:
					return xhci_no_ping_error(hcState, event, context);
					break;
				case COMP_CODE_EVENT_RING_FULL_ERR:
					return xhci_event_ring_full_error(hcState, event, context);
					break;
				case COMP_CODE_INCOMPATIBLE_DEV_ERR:
					return xhci_incompatible_device_error(hcState, event, context);
					break;
				case COMP_CODE_MISSED_SERVICE_ERR:
					return xhci_missed_service_error(hcState, event, context);
					break;
				case COMP_CODE_COMMAND_RING_STOPPED:
					return xhci_command_ring_stopped(hcState, event, context);
					break;
				case COMP_CODE_COMMAND_ABORTED:
					return xhci_command_aborted(hcState, event, context);
					break;
				case COMP_CODE_STOPPED:
					return xhci_command_stopped(hcState, event, context);
					break;
				case COMP_CODE_STOPPED_LENGTH_INVALID:
					return xhci_command_stopped_length(hcState, event, context);
					break;
				case COMP_CODE_STOPPED_SHORT_PACKET:
					return xhci_command_stopped_short_packet(hcState, event, context);
					break;
				case COMP_CODE_EXIT_LATENCY_TOO_LARGER_ERR:
					return xhci_exit_latency_too_large(hcState, event, context);
					break;
				case COMP_CODE_ISOCH_BUFFER_OVERRUN:
					return xhci_isoch_buffer_overrun(hcState, event, context);
					break;
				case COMP_CODE_EVENT_LOST_ERR:
					return xhci_event_lost_error(hcState, event, context);
					break;
				case COMP_CODE_UNDEFINED_ERR:
					return xhci_process_undefined_err(hcState, event, context);
					break;
				case COMP_CODE_INVALID_STREAM_ID_ERR:
					return xhci_invalid_stream_id_error(hcState, event, context);
					break;
				case COMP_CODE_SECONDARY_BANDWIDTH_ERR:
					return xhci_secondary_bandwidth_error(hcState, event, context);
					break;
				case COMP_CODE_SPLIT_TRANSACTION_ERR:
					return xhci_split_transaction_error(hcState, event, context);
					break;
                default:
                    xhci_log(XHCI_LOG_ERRORS, "Encountered unknown CCE error: %u for slot: %u\n", completion_code, event->commandCompletion.slotId);
                    break;
			}
		}
	}
	return driverOpBadState;
}

static void xhci_process_transfer_event(xhci_controller_state *hcState, volatile transferEventTRB *event, volatile uint32_t *commandIssueCount)
{
	uint8_t completionValue;
	uint32_t transfered_bytes;

	completionValue = TRANSFER_EVENT_GET_CC(event->trbTransferLength);
	transfered_bytes = TRANSFER_EVENT_GET_BYTES(event->trbTransferLength);


	if(event->cycle & TRANSFER_EVENT_ED)
	{
		/* Transfer event, should have a software pointer associated with it instead of a physical TRB address */
		activeTransaction *request;

		request = (activeTransaction *)event->trbPointer;

		if(request->flags & ~(USB_TRANSACTION_SMALLER | XHCI_INIT_DESC_REQ | USB_CONTROLLER_FLAG_CONFIG_REQUEST | USB_TRANSACTION_IN) )
        {
            //volatile transferTRB *trb;
            volatile uint64_t *raw_req;

            raw_req = (volatile uint64_t *)event->trbPointer;

            //trb = request->first_trb;
            xhci_log(XHCI_LOG_ERRORS, "Transfer corrupted - Request: %p REQ[0]: %016X REQ[1]: %016X\n\tREQ[2]: %016X REQ[3]: %016X\n", request, raw_req[0], raw_req[1], raw_req[2], raw_req[3]);
        }

        if(completionValue == COMP_CODE_SUCCESS)
        {
            xhci_log(XHCI_LOG_OPSRES, "Completed TE @ %p transaction @ %X with flags %X and FTB: %p\n", event, request, request->flags, request->first_trb);
            if(request->flags & XHCI_INIT_DESC_REQ)
            {
                ringQueue *rq;

                rq = xhci_get_endpoint_ring_from_id(hcState, event->slotId, event->endpointId);
                if(!rq)
                {
                    xhci_log(XHCI_LOG_ERRORS, "Unable to get ringqueue for Slot %u Endpoint: %u\n", event->slotId, event->endpointId);
                    return;
                }

                xhci_log(XHCI_LOG_INTERNAL, "Initial descriptor request completed! T: %p FTRB: %p TRBC: %u TB: %u\n", request, request->first_trb, request->trb_count, transfered_bytes);
                xhci_update_ring_completed(rq, (volatile softwareIssuedTRB *)&request->first_trb[request->trb_count]);
                xhci_assign_address(hcState, event->slotId, request, commandIssueCount);
                return;
            }
            else if(request->flags & USB_CONTROLLER_FLAG_CONFIG_REQUEST)
            {
                xhci_slot_state *slot_state;

                slot_state = &hcState->slots[event->slotId];

                xhci_process_device_configured(hcState, slot_state, event->slotId);
            }

            xhci_process_completed_transfer(hcState, request, (volatile eventTRB *)event, transfered_bytes, event->slotId, event->endpointId);
        }
        else if( (completionValue == COMP_CODE_SHORT_PACKET) && (request->flags & USB_TRANSACTION_SMALLER) )
            xhci_process_completed_transfer(hcState, request, (volatile eventTRB *)event, transfered_bytes, event->slotId, event->endpointId);
        else
        {
            volatile softwareIssuedTRB *trb;


            xhci_log(XHCI_LOG_ERRORS, "Transfer error[%u] - Attempting to get RQ for Slot %u Endpoint: %u Bytes transferred: %u Transfer size: %u\n",
                     completionValue, event->slotId, event->endpointId, transfered_bytes, request->total_size);
            ringQueue *rq = xhci_get_endpoint_ring_from_id(hcState, event->slotId, event->endpointId);
            if(!rq)
            {
                xhci_log(XHCI_LOG_ERRORS, "Unable to get ringqueue for Slot %u Endpoint: %u\n", event->slotId, event->endpointId);
                return;
            }

            if( !xhci_get_completed_trb(&trb, event->trbPointer, rq) )
            {
                xhci_log(XHCI_LOG_ERRORS, "Unable to get failed transfer @ %p from slot %u endpoint: %u\n", event->trbPointer, event->slotId, event->endpointId);
                return;
            }
            xhci_log(XHCI_LOG_ERRORS, "Transfer event error! TRB: %p Code: %u TRB type: %u\n", event, completionValue, READ_TRB_TYPE_ADJ(trb->command.noOp.trbType));
        }
		return;
	}
	else
	{
		volatile softwareIssuedTRB *trb;
		volatile slotContext *sc;

        ringQueue *rq = xhci_get_endpoint_ring_from_id(hcState, event->slotId, event->endpointId);
		sc = xhci_get_dev_context(hcState, event->slotId, 0);

		if( !xhci_get_completed_trb(&trb, event->trbPointer, rq) )
		{
			xhci_log(XHCI_LOG_ERRORS, "Unable to get failed transfer @ %p from slot %u endpoint: %u\n", event->trbPointer, event->slotId, event->endpointId);
			return;
		}

		xhci_log(XHCI_LOG_ERRORS, "Unexpected transfer event encountered! Slot: %u EP: %u CC: %u RB: %u TRB type: %u TRB transfer length: %u\n", event->slotId, event->endpointId, completionValue,
		       transfered_bytes, READ_TRB_TYPE_ADJ(trb->transfer.data.trbType), TRB_TRANSFER_GET_LENGTH(trb->transfer.data.transferInfo) );
        xhci_log(XHCI_LOG_ERRORS, "Slot state: %u\n", XHCI_SLOT_CONTEXT_STATE(sc->slotState) );
        xhci_ring_control_backtrace(hcState, rq, &trb->transfer);
	}
}

static void processHcEvent(xhci_controller_state *hcState, volatile eventTRB *event, volatile uint32_t *commandIssueCount)
{
	uint16_t eventType, completion_code;
	eventType = READ_TRB_TYPE_ADJ(event->hostController.trbType);

	switch(eventType)
	{
		case TRB_TRANSFER_EVENT:
			xhci_process_transfer_event(hcState, &event->transferEvent, commandIssueCount);
			break;
		case TRB_CMD_COMPLETION_EVENT:
			completion_code = event->commandCompletion.commandParameter>>24;
			if(completion_code == COMP_CODE_SUCCESS)
				xhci_hc_process_successful_command(hcState, &event->commandCompletion, commandIssueCount);
			else
            {

                xhci_log(XHCI_LOG_ERRORS, "CCE with error code %u found!\n", completion_code);
				xhci_process_event_error(hcState, event, NULL);
            }
			break;
		case TRB_PSC_EVENT:
		    xhci_rh_read_port_status(hcState, event->portStatusChange.portId );
			break;
		case TRB_BANDWIDTH_REQ_EVENT:
			xhci_log(XHCI_LOG_BASIC, "Bandwidth request event!\n");
			break;
		case TRB_DOORBELL_EVENT:
		    xhci_log(XHCI_LOG_BASIC, "Doorbell event!\n");
			break;
		case TRB_HOST_CONTROLLER_EVENT:
		    xhci_log(XHCI_LOG_BASIC, "Host controller event!\n");
			break;
		case TRB_DEVICE_NOTIFY_EVENT:
		    xhci_log(XHCI_LOG_BASIC, "Device notify event!\n");
			break;
		case TRB_MFINDEX_WRAP_EVENT:
		    xhci_log(XHCI_LOG_BASIC, "MFINDEX WRAP event!\n");
			break;
		default:
			//TODO: Handle this better, might be some kind of corruption or controller issue that requires a reset or disabling the controller
			xhci_log(XHCI_LOG_BASIC, "Invalid event encountered! Type: %u\n", eventType);
			break;
	}
}

static void xhci_write_event_dq(xhci_controller_state *hcState, const int interrupter_index, const uint32_t segIndex, const uint64_t physEventPtr, const uint32_t ehbClear)
{
	volatile xhci_interrupt_reg *intReg;
	intReg = &hcState->rts->interrupter[interrupter_index];

	if(hcState->deviceFlags & XHCI_CTRL_FLAGS_64BIT)
		intReg->eventRingDequeuePtr64 = physEventPtr | (segIndex & 0x7) | ehbClear;
	else
		intReg->eventRingDequeuePtr32 = physEventPtr | (segIndex & 0x7) | ehbClear;
}

void xhci_handle_ring_events(xhci_controller_state *hcState, const int interrupter_index)
{
	volatile xhci_interrupt_reg *intReg;
	uint64_t physEventPtr;
	volatile eventTRB *eventPtr;
	uint32_t lastTableIndex, tableOffset;
	uint32_t handled_event_count;
	volatile uint32_t hcCommandIssueCount;

	intReg = &hcState->rts->interrupter[interrupter_index];

	physEventPtr = hcState->eventLastPtrs[interrupter_index];
	lastTableIndex = hcState->eventLastTableIndices[interrupter_index];
	tableOffset = (physEventPtr & ARCH_PAGE_OFFSET)/sizeof(eventTRB);
	eventPtr = &hcState->tableMappings[interrupter_index*EVENT_RING_PER_PAGE + lastTableIndex].events[tableOffset];

	hcCommandIssueCount = 0;
    handled_event_count = 0;

    do
    {
        xhci_status_clear(hcState, USB_STATUS_EINT);
        intReg->iman |= XHCI_IMAN_IP | XHCI_IMAN_IE;
        while( (eventPtr->transferEvent.cycle & TRB_CYCLE_BIT) == hcState->eventCCS[interrupter_index])
        {
            /* Primary interrupter, might contain host controller events */
            if(!interrupter_index)
                processHcEvent(hcState, eventPtr, &hcCommandIssueCount);
            else
            {
                /* Secondary interrupter, should not contain command completion, doorbell, MFINDEX or port status events */
                xhci_process_transfer_event(hcState, &eventPtr->transferEvent, &hcCommandIssueCount);
            }

            tableOffset++;
            if( tableOffset == EVENT_RING_PER_PAGE )
            {
                lastTableIndex++;
                if(lastTableIndex == hcState->rts->interrupter[interrupter_index].eventSegCount )
                {
                    lastTableIndex = 0;
                    /* Track HC Wrap */
                    hcState->eventCCS[interrupter_index] = (~hcState->eventCCS[interrupter_index]) & TRB_CYCLE_BIT;
                }
                physEventPtr = hcState->tableMappings[interrupter_index*EVENT_RING_PER_PAGE + lastTableIndex].physicalBase;
                eventPtr = (volatile eventTRB *)hcState->tableMappings[interrupter_index*EVENT_RING_PER_PAGE + lastTableIndex].virtualBase;
                tableOffset = 0;
            }
            else
            {
                eventPtr++;
                physEventPtr += TRB_SIZE;
            }

            handled_event_count++;
            //xhci_write_event_dq(hcState, interrupter_index, lastTableIndex, physEventPtr, 0);
        }

        hcState->eventLastPtrs[interrupter_index] = physEventPtr;
        hcState->eventLastTableIndices[interrupter_index] = lastTableIndex;

        /* Ring the command ring doorbell if commands have been issued */
        if(hcCommandIssueCount)
            xhci_hc_signal_work_pending(hcState, hcCommandIssueCount);

    } while(intReg->iman & XHCI_IMAN_IP);

	xhci_log(XHCI_LOG_INTERNAL, "Event interrupt handler has issued %u new commands.\n", hcCommandIssueCount);
    xhci_log(XHCI_LOG_INTERNAL, "Processed %u events.\n", handled_event_count);

	xhci_write_event_dq(hcState, interrupter_index, lastTableIndex, physEventPtr, XHCI_INTR_DQ_EHB);


}
