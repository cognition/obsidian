#ifndef XHCI_EVENT_RING_H_
#define XHCI_EVENT_RING_H_

#include <driverapi.h>
#include "xhci_controller.h"


#define COMP_CODE_INVALID                        0
#define COMP_CODE_SUCCESS                        1
#define COMP_CODE_DATA_BUFFER_ERR                2
#define COMP_CODE_BABBLE_ERR                     3
#define COMP_CODE_USB_TRANSACTION_ERR            4
#define COMP_CODE_TRB_ERR                        5
#define COMP_CODE_STALL_ERR                      6
#define COMP_CODE_RESOURCE_ERR                   7
#define COMP_CODE_BANDWIDTH_ERR                  8
#define COMP_CODE_NO_SLOTS_ERR                   9
#define COMP_CODE_INVALID_STREAM_ERR             10
#define COMP_CODE_SLOT_NOT_ENABLE_ERR            11
#define COMP_CODE_ENDPOINT_NOT_ENABLE_ERR        12
#define COMP_CODE_SHORT_PACKET                   13
#define COMP_CODE_RING_UNDERRUN                  14
#define COMP_CODE_RING_OVERRUN                   15
#define COMP_CODE_VF_EVENT_RING_FULL_ERR         16
#define COMP_CODE_PARAMETER_ERR                  17
#define COMP_CODE_BANDWIDTH_OVERRUN_ERR          18
#define COMP_CODE_CONTEXT_STATE_ERR              19
#define COMP_CODE_NO_PING_ERR                    20
#define COMP_CODE_EVENT_RING_FULL_ERR            21
#define COMP_CODE_INCOMPATIBLE_DEV_ERR           22
#define COMP_CODE_MISSED_SERVICE_ERR             23
#define COMP_CODE_COMMAND_RING_STOPPED           24
#define COMP_CODE_COMMAND_ABORTED                25
#define COMP_CODE_STOPPED                        26
#define COMP_CODE_STOPPED_LENGTH_INVALID         27
#define COMP_CODE_STOPPED_SHORT_PACKET           28
#define COMP_CODE_EXIT_LATENCY_TOO_LARGER_ERR    29

#define COMP_CODE_ISOCH_BUFFER_OVERRUN           31
#define COMP_CODE_EVENT_LOST_ERR                 32
#define COMP_CODE_UNDEFINED_ERR                  33
#define COMP_CODE_INVALID_STREAM_ID_ERR          34
#define COMP_CODE_SECONDARY_BANDWIDTH_ERR        35
#define COMP_CODE_SPLIT_TRANSACTION_ERR          36

#define COMP_CODE_VENDOR_ERR_START               192
#define COMP_CODE_VENDOR_INFO_START              224

#define EVENT_RING_PER_PAGE                      (ARCH_PAGE_SIZE/sizeof(xhci_event_ring_segment))

void xhci_hc_signal_work_pending(xhci_controller_state *hcState, const int units);

driverOperationResult xhci_hc_enable_slot(xhci_controller_state *hcState, const uint8_t port, const uint8_t rh_port, const uint8_t portSpeed, const uint8_t psi_value,
                                          const uint32_t routeInfo, const uint8_t parent_slot, sync_object *sync);

void hcPrintInterrupterInfo(xhci_controller_state *hcState, const int interrupter_index);
void xhci_handle_ring_events(xhci_controller_state *hcState, const int interrupter_index);
driverOperationResult hcTestCommandNoOp(xhci_controller_state *hcState);
uint16_t hcGetBestInterrupter(xhci_controller_state *hcState);
driverOperationResult xhci_process_event_error(xhci_controller_state *hcState, volatile eventTRB *event, void *context);

driverOperationResult xhci_queue_stop_endpoint(xhci_controller_state *hcState, const int slotNumber, const uint8_t endpoint, sync_object *sync);


#endif
