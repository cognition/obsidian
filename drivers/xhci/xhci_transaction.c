/* XHCI controller transaction scheduling implementation
 *
 * Author: Joseph Kinzel */
#include "xhci_transaction.h"
#include "xhci_cq.h"
#include "xhci_event.h"
#include "xhci_ports.h"
#include "xhci_error_handle.h"
#include "xhci_log.h"
#include <sync.h>

typedef struct
{
    uint32_t total_packet_count;
    uint32_t bytes_transferred;
} td_size_state;

static uint32_t calc_sg_trb_needed(const uint64_t transfer_size, const uint32_t init_page_offset)
{
    uint32_t init_page_rem;
    init_page_rem = ARCH_PAGE_SIZE - init_page_offset;
    if(transfer_size <= init_page_rem)
        return 1;
    else
    {
        uint64_t transfer_rem;
        uint32_t trb_count;

        transfer_rem = transfer_size - init_page_rem;

        if(transfer_rem%ARCH_PAGE_SIZE)
            trb_count = 2;
        else
            trb_count = 1;

        trb_count += transfer_rem/ARCH_PAGE_SIZE;
        return trb_count;
    }
}

static inline uint32_t xhci_pack_transfer_info(const uint32_t transfer_size, const uint32_t td_size, const uint32_t interrupter)
{
    uint32_t value = interrupter << 22;
    value |= td_size << 17;
    value |= transfer_size;

    return value;
}

static inline int min(int first, int second)
{
	return (first < second ? first:second);
}

static uint32_t xhci_calc_packet_count(const uint32_t transfer_size, const uint32_t mps)
{
    int packet_rem = (transfer_size%mps);
	int packet_count = (transfer_size/mps) + (!packet_rem ? 0:1);

	return packet_count;
}


static void xhci_init_tds_state(td_size_state *state, const uint32_t transfer_size, const uint32_t mps)
{
        state->bytes_transferred = 0;
        state->total_packet_count = xhci_calc_packet_count(transfer_size, mps);
}

static uint32_t xhci_calc_td_size(td_size_state *state, const uint32_t trb_size, const uint32_t mps, const uint32_t bytes_rem)
{
    uint32_t packets_transferred, packets_rem;
    /* Last TRB always has a TD Size of zero */
    if(!bytes_rem)
        return 0;

    state->bytes_transferred += trb_size;
    packets_transferred = state->bytes_transferred/mps;

    packets_rem = state->total_packet_count - packets_transferred;
    if(packets_rem > 31)
        return 31;
    else
        return packets_rem;
}

/*! Creates an event data TRB
 *  \param hc Host controller instance context
 *  \param epRing Endpoint transfer ring virtual pointer
 *  \param transaction Transaction data for the event data field
 *  \return busOpSuccess if successful or busOpResources if no TRB was available
 */
static busOperationResult xhci_create_event_data(xhci_controller_state *hc, ringQueue *epRing, volatile activeTransaction *transaction, volatile softwareIssuedTRB *trb, const uint8_t cycleState)
{
	volatile eventDataTRB *ed_trb;
    uint64_t t_addr;


	t_addr = (uint64_t)transaction;
	ed_trb = &trb->event_data;

	xhci_log(XHCI_LOG_INTERNAL, "Attached event data TRB @ %p with T: %p FTRB: %p TRBC: %u Size: %u\n",
          ed_trb, transaction, transaction->first_trb, transaction->trb_count, transaction->total_size);

	ed_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_EVENT_DATA);
	ed_trb->eventData = t_addr;
	ed_trb->interrupter = hcGetBestInterrupter(hc);
	ed_trb->flags = cycleState | TRB_FLAGS_IOC;

	return busOpSuccess;
}

busOperationResult xhci_queue_normal_sg(xhci_controller_state *hc, ringQueue *epRing, uint8_t *buffer_ptr,
                                        uint32_t buffer_rem, td_size_state *tds, const uint32_t mps,
                                        volatile activeTransaction *request, volatile softwareIssuedTRB **trb_ptr, const uint8_t cycleState)
{
	volatile softwareIssuedTRB *trb;
	uint64_t data_addr;
	uint32_t data_rem, align_rem, td_size, buffer_offset;

	buffer_offset = 0;
	data_rem = buffer_rem;
	trb = *trb_ptr;
	while(data_rem)
	{
		volatile normalTRB *normal_trb;

        ++trb;

		normal_trb = &trb->transfer.normal;
		align_rem = min(ARCH_PAGE_SIZE, data_rem);

		data_addr = getPhysBase( (uintptr_t)&buffer_ptr[buffer_offset] );
		if(!data_addr)
			return busOpInvalidArg;

		normal_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_NORMAL);
		normal_trb->dataBuffer = data_addr;

		data_rem -= align_rem;
		buffer_offset += align_rem;
		td_size = xhci_calc_td_size( tds, align_rem, mps, data_rem);
		normal_trb->transferInfo = xhci_pack_transfer_info(align_rem, td_size, hcGetBestInterrupter(hc) );

		normal_trb->cycleFlags = cycleState | TRB_FLAGS_CHAIN;
	}

	*trb_ptr = trb;
	return busOpSuccess;
}

/*! Adds a normal transaction to an endpoint's queue
 *
 *  \param hc Host controller context instance
 *  \param slot Slot of the device queue
 *  \param endpoint Target endpoint
 *  \param direction Direction of the transaction
 *  \param transaction Transaction request information
 *  \param flags Transaction behavioral flags
 *  \param sync Synchronization object That will be invoked upon completion of the transaction
 *
 *  \return busOpSuccess if successful
 */
busOperationResult xhci_queue_normal_transaction(xhci_controller_state *hc, const uint8_t slot, const uint8_t endpoint, const uint8_t direction, usbControllerTransaction *transaction,
                                                 const uint32_t flags, sync_object *sync)
{
    busOperationResult status;
	volatile softwareIssuedTRB *trb;
	volatile normalTRB *normal_trb, *first_trb;

	uint64_t data_addr;
	uint32_t data_rem, align_rem, buffer_offset, interrupter, mps, td_size, trb_needed;
	uint8_t *buffer, cycleState;
    activeTransaction *tag;
    td_size_state tds;

	ringQueue *epRing;

	interrupter = hcGetBestInterrupter(hc);



	buffer = transaction->transfer.buffer;
	epRing = xhci_get_endpoint_ring(hc, slot, endpoint, direction);
    mps = xhci_get_ep_mps(hc, slot, endpoint, direction, transaction->type);
    normal_trb = NULL;

	if(!epRing)
    {
        xhci_log(XHCI_LOG_ERRORS, "NT - Unable to get endpoint ring for slot %u endpoint: %u direction: %u\n", slot, endpoint, direction);
		return busOpInvalidArg;
    }

    trb_needed = calc_sg_trb_needed(transaction->transfer.length, transaction->transfer.buffer_addr % ARCH_PAGE_SIZE) + 1;

    if( xhci_get_batch_trb(trb_needed, hc, epRing, &trb, &cycleState) != driverOpSuccess )
    {

        xhci_log(XHCI_LOG_ERRORS, "NT - Unable to allocated TRB for slot: %u endpoint: %u direction: %u\n", slot, endpoint, direction);
		return busOpResources;
    }

	first_trb = &trb->transfer.normal;
	first_trb->cycleFlags = TRB_FLAGS_CHAIN;

	first_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_NORMAL);

	/* Set up the internal transaction information structure which will attach to the final event data TRB */
	tag = (activeTransaction *)calloc(1, sizeof(activeTransaction));
	tag->buffer = buffer;
	tag->first_trb = &trb->transfer;
    tag->flags = direction | transaction->flags;
    tag->total_size = transaction->transfer.length;
    tag->transfer_type = transaction->type;
    tag->flags = flags;
    tag->trb_count = trb_needed;    /* At least one Normal TRB + Event data TRB */

	tag->sync = sync;
	if(sync)
    {
        if(!sync_attach(sync) )
        {
            free(transaction);
            return busOpSyncError;
        }
		sync_object_reset(sync);
    }

    xhci_init_tds_state(&tds, transaction->transfer.length, mps);

	if( (tag->total_size <= 8) && (!direction) )
	{
        td_size = xhci_calc_td_size(&tds, tag->total_size, mps, 0);

	    /* If the transaction is outgoing with a small amount of data use an immediate TRB */
		memcpy( (void *)first_trb->immData, buffer, tag->total_size);
		first_trb->cycleFlags |= TRB_FLAGS_IDT | TRB_FLAGS_CHAIN;
		first_trb->transferInfo = xhci_pack_transfer_info(tag->total_size, td_size, interrupter);

	}
	else
	{
		uint32_t page_offset;

		buffer_offset = 0;

		data_rem = tag->total_size;

		/* Set up the first physical page TRB */
		data_addr = getPhysBase( (uint64_t)(&buffer[buffer_offset]) );
		if(!data_addr)
        {
            free(tag);
			return busOpInvalidArg;
        }

		page_offset = data_addr & ARCH_PAGE_OFFSET;
        align_rem = min(ARCH_PAGE_SIZE - page_offset, data_rem);
        data_rem -= align_rem;
        buffer_offset += align_rem;

		td_size = xhci_calc_td_size( &tds, align_rem, mps, data_rem);

        first_trb->transferInfo = xhci_pack_transfer_info(align_rem, td_size, interrupter);
        first_trb->dataBuffer = data_addr;

		while(data_rem)
		{
            ++trb;

            data_addr = getPhysBase( (uint64_t)(&buffer[buffer_offset]) );
            align_rem = min(ARCH_PAGE_SIZE, data_rem);
            normal_trb = &trb->transfer.normal;
            normal_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_NORMAL);
            normal_trb->cycleFlags = cycleState | TRB_FLAGS_CHAIN;
            normal_trb->dataBuffer = data_addr;
            buffer_offset += align_rem;
            data_rem -= align_rem;

            td_size = xhci_calc_td_size( &tds, align_rem, mps, data_rem);
            normal_trb->transferInfo = xhci_pack_transfer_info(align_rem, td_size, interrupter);

        }

	}

    status = xhci_create_event_data(hc, epRing, tag, ++trb, cycleState);
	if(status == busOpSuccess)
    {
        xhci_trb_set_cycle(first_trb, cycleState);
    }

    xhci_log(XHCI_LOG_OPSRES, "Normal transaction queued for slot %u endpoint %u TRB @ %X\n", slot, endpoint, first_trb);

    return status;
}

/*! Adds a control transaction to an endpoint's queue
 *
 *  \param hc Host controller context
 *  \param slot Slot of the device to schedule a transaction on
 *  \param endpoint Endpoint to issue the transaction on
 *  \param direction Direction of the transaction (in or out)
 *  \param ct Control transaction specific information structure
 *  \param flags Flags to pass to the transaction information structure that will be attached to the transaction
 *  \param sync_info Synchronization object to attach to the transaction information structure, can be NULL
 *
 *  \return busOpSuccess if successful
 */
busOperationResult xhci_queue_control_transaction(xhci_controller_state *hc, const uint8_t slot, const uint8_t endpoint, uint8_t direction,
        usbControlTransaction *ct, const uint32_t flags, sync_object *sync_info) {

    volatile setupStageTRB *setup_trb;
	volatile statusStageTRB *status_trb;
	volatile dataStageTRB *data_trb;
	volatile softwareIssuedTRB *trb;
	uint8_t *buffer_ptr, cycleState;
	uint16_t data_dir_flag, status_dir_flag, trt, trb_count;

	uint32_t data_rem, align_rem;
	ringQueue *epRing;
	busOperationResult status;
	td_size_state tds;
	activeTransaction *transaction = NULL;

	epRing = xhci_get_endpoint_ring(hc, slot, endpoint, direction);
	if(!epRing)
	{
		xhci_log(XHCI_LOG_ERRORS, "Unable to get endpoint ring for slot %u endpoint %u\n", slot, endpoint);
		return busOpInvalidArg;
	}

	buffer_ptr = (uint8_t *)ct->data_ptr;
	data_rem = ct->w_length;

    trb_count = 3;

    if(data_rem)
        trb_count += calc_sg_trb_needed(data_rem, ct->data_addr % ARCH_PAGE_SIZE);


    if( xhci_get_batch_trb(trb_count, hc, epRing, &trb, &cycleState) != driverOpSuccess )
		return busOpResources;


	if(ct->w_length && !ct->data_ptr)
    {

        xhci_log(XHCI_LOG_ERRORS, "Error control transaction requested on slot %u endpoint %u with WL: %u PTR: %p\n", slot, endpoint, ct->w_length, ct->data_ptr);
		return busOpInvalidArg;
    }

	/* See Table 4-7 in the XHCI specification for the mapping of TRT and direction flag values for each stage of the control transfer */
	if(!ct->w_length)
		trt = 0;
	else if(ct->bm_request_type & USB_REQUEST_DEV2HOST)
		trt = SETUP_TRT_IN;
	else
		trt = SETUP_TRT_OUT;

	if(ct->bm_request_type & USB_REQUEST_DEV2HOST)
		data_dir_flag = TRB_FLAGS_DIR;
	else
		data_dir_flag = 0;

	status_dir_flag = TRB_FLAGS_DIR;
	if(data_dir_flag)
		if(ct->w_length)
			status_dir_flag = 0;

	/* Setup stage */
	setup_trb = &trb->transfer.setup;

	setup_trb->bmRequestType = ct->bm_request_type;
	setup_trb->wLength = ct->w_length;
	setup_trb->bRequest = ct->b_request;
	setup_trb->wIndex = ct->w_index;
	setup_trb->wValue = ct->w_value;
	setup_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_SETUP);

	/* Transfer length is always 8 for a setup TRB and the IDT flag is always set per XHCI specification */
	setup_trb->transferInfo = xhci_pack_transfer_info( USB_SETUP_LENGTH, 0, hcGetBestInterrupter(hc) );
	setup_trb->cycleFlags = TRB_FLAGS_IDT;

	setup_trb->transferType = trt;


	transaction = (activeTransaction *)calloc(1, sizeof(activeTransaction));
	transaction->flags = data_dir_flag | flags;
	transaction->total_size = data_rem;
	transaction->buffer = ct->data_ptr;
	transaction->sync = sync_info;
    transaction->trb_count = trb_count;
	transaction->transfer_type = usbTransactionControl;
    transaction->first_trb = &trb->transfer;

	if(sync_info)
    {
        if(!sync_attach(sync_info) )
        {
            free(transaction);
            return busOpSyncError;
        }
		sync_object_reset(sync_info);
    }


	/* Data stage, if required */
	if(data_rem)
	{
		uint32_t td_size, mps;
		uint64_t data_addr, buffer_offset;

	    mps = xhci_get_ep_mps(hc, slot, endpoint>>1, transaction->flags & USB_TRANSACTION_IN, usbTransactionControl);
        xhci_init_tds_state(&tds, transaction->total_size, mps);

        ++trb;

		data_trb = &trb->transfer.data;
		data_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_DATA);
		data_trb->direction = data_dir_flag;

		uint64_t page_offset = ((uintptr_t)buffer_ptr) & ARCH_PAGE_OFFSET;

		align_rem = min(ARCH_PAGE_SIZE - page_offset, data_rem);


		data_rem -= align_rem;
		td_size = xhci_calc_td_size(&tds, align_rem, mps, data_rem);


		data_addr = getPhysBase( (uintptr_t)buffer_ptr);
		if(!data_addr)
        {
            xhci_log(XHCI_LOG_ERRORS, "CT - Unable to resolve virtual address %p into physical address!\n", buffer_ptr);

			return busOpInvalidArg;
        }

		data_trb->dataBuffer = data_addr;
		data_trb->transferInfo = xhci_pack_transfer_info(align_rem, td_size, hcGetBestInterrupter(hc) );
		data_trb->cycleFlags = cycleState | (data_rem ? (TRB_FLAGS_CHAIN):0);

		buffer_offset = align_rem;

		if(data_rem)
		{
			status = xhci_queue_normal_sg(hc, epRing, &buffer_ptr[buffer_offset], data_rem, &tds, mps, transaction, &trb, cycleState);
			if(status != busOpSuccess)
				return status;
		}

	}

	/* Status stage */
	++trb;

	status_trb = &trb->transfer.status;

	status_trb->cycleFlags = TRB_FLAGS_CHAIN | TRB_FLAGS_ENT | cycleState;
	status_trb->trbType = WRITE_TRB_TYPE_ADJ(TRB_STATUS);
	status_trb->transferInfo = xhci_pack_transfer_info( 0, 0, hcGetBestInterrupter(hc) );
	status_trb->direction = status_dir_flag;

	status = xhci_create_event_data(hc, epRing, transaction, ++trb, cycleState);
	if(status == busOpSuccess)
		xhci_trb_set_cycle(setup_trb, cycleState);

	return status;
}

/*! Process a completed control transfer
 *  \param hcState Host controller instance context
 *  \param request Control transaction request
 *  \param slot_id Slot of the device
 *  \param ep_id Endpoint ID of the TRB ring
 *  \param rq Ring queue pointer for the endpoint
 */
static void xhci_process_completed_control_transfer(xhci_controller_state *hcState, activeTransaction *request, const uint8_t slot_id, const uint8_t ep_id, ringQueue *rq)
{
	int min_trb_count, trb_type;
	volatile transferTRB *init_trb = request->first_trb;

	min_trb_count = 3;
	trb_type = READ_TRB_TYPE_ADJ(init_trb->data.trbType);
	if(request->total_size)
    {
        min_trb_count++;
        if( trb_type != TRB_SETUP)
        {
            free(request);
            xhci_log(XHCI_LOG_ERRORS, "Malformed control transaction. Slot: %u Endpoint Index: %u Found TRB Type: %u Expected: %u\n", slot_id, ep_id, trb_type, TRB_SETUP );
            return;
        }
    }
	/* Sanity check */
	if(request->trb_count < min_trb_count)
	{
		xhci_log(XHCI_LOG_ERRORS, "Malformed control transaction - TRB count %u Slot: %u Endpoint Index: %u\n", request->trb_count, slot_id, ep_id);
		free(request);
		return;
	}

	xhci_log(XHCI_LOG_OPSRES, "Completed control transaction with T: %p FTRB: %p TRBC: %u\n", request, request->first_trb, request->trb_count);

    xhci_update_ring_completed(rq, (volatile softwareIssuedTRB *)&request->first_trb[request->trb_count] );

    if(request->sync)
    {
        sync_object_signal(request->sync, usb_op_success);
        sync_detach(request->sync);
    }


	free(request);
}

/*! Processes a completed normal transfer
 *  \param hcState Host controller context instance
 *  \param request Attached transaction information structure
 *  \param slot_id XHCI slot number corresponding to the transaction
 *  \param ep_id DCI of the endpoint that completed the transfer
 *  \param rq Ringqueue of the endpoint containing the completed TRB
 *  \param transferred_bytes Number of bytes actually transferred
 */
static void xhci_process_completed_normal_transfer(xhci_controller_state *hcState, activeTransaction *request, const uint8_t slot_id, const uint8_t ep_id, ringQueue *rq, const uint32_t transferred_bytes)
{
    /* Signal the synchronization object attached to the transaction with the number of bytes transferred */
	if(request->sync)
    {

        sync_object_signal(request->sync, transferred_bytes);
        sync_detach(request->sync);
    }

    xhci_update_ring_completed(rq, (volatile softwareIssuedTRB *)&request->first_trb[request->trb_count] );

    free(request);
}

/*! Processes a completed transaction
 *  \param hcState Host controller state structure
 *  \param request Transaction request structure
 *  \param event Pointer to the transfer completion event
 *  \param transferred_bytes Number of points the transfer sent or received successfully
 *  \param slot_id Slot of the device involved in the transaction
 *  \param ep_id Endpoint ID (index into device context structure)
 */
void xhci_process_completed_transfer(xhci_controller_state *hcState, activeTransaction *request, volatile eventTRB *event,
                                     uint32_t transferred_bytes, const uint8_t slot_id, const uint8_t ep_id)
{
	ringQueue *rq;
	uint32_t completion_value, trb_count_save;

	trb_count_save = request->trb_count;
	rq = xhci_get_endpoint_ring_from_id(hcState, slot_id, ep_id);

	if(!rq)
	{
		xhci_log(XHCI_LOG_ERRORS, "Failed to locate transfer command queue for slot %u endpoint id: %u\n", slot_id, ep_id);
		return;
	}

	completion_value = event->transferEvent.trbTransferLength>>24;

	switch(completion_value)
	{
        case COMP_CODE_SHORT_PACKET:
        case COMP_CODE_SUCCESS:
            break;
        default:
            xhci_process_event_error(hcState, event, request);
            return;
            break;
	}

	dapi_lock_acquire(rq->rqLock);

    /* Free the TRBs */
	atomic_sub(&rq->commandsInFlight, trb_count_save);

	dapi_lock_release(rq->rqLock);

	switch(request->transfer_type)
	{
		case usbTransactionControl:
			xhci_process_completed_control_transfer(hcState, request, slot_id, ep_id, rq);
			break;
		case usbTransactionBulk:
		case usbTransactionInterrupt:
		    xhci_process_completed_normal_transfer(hcState, request, slot_id, ep_id, rq, transferred_bytes);
			break;
		case usbTransactionIsochronous:
			break;
	}

}

#define XHCI_DBG_HAS_SETUP       0x0001U
#define XHCI_DBG_HAS_DATA        0x0002U
#define XHCI_DBG_HAS_STATUS      0x0004U

#define XHCI_DBG_EXIT            0x8000U

static void xhci_print_raw_trb(volatile void *trb)
{
    volatile uint32_t *trb_dw = (volatile uint32_t *)trb;

    xhci_log(XHCI_LOG_ERRORS, "TRB @ %p - [+0x0]: %08X [+0x4]: %08X [+0x8]: %08X, [+0xC]: %08X\n", trb, trb_dw[0], trb_dw[1], trb_dw[2], trb_dw[3]);
}

/*! Attempts to crawl a failed control transaction and decode the individual TRBs involved
 *
 *  \param hcState Host controller state
 *  \param rq Ring queue containing the transaction
 *  \param start_trb TRB that raised an error
 *  \return driverOpSuccess if at least one TRB could be properly identified and decode driverOpBadState otherwise
 */
driverOperationResult xhci_ring_control_backtrace(xhci_controller_state *hcState, ringQueue *rq, volatile transferTRB *start_trb)
{
    ringRegion *dq_reg;
    uintptr_t trb_base;
    uint32_t transaction_state;

    dq_reg = rq->completedRegion;

    trb_base = (uintptr_t)start_trb;
    trb_base -= trb_base%ARCH_PAGE_SIZE;

    /* Find the region containing the TRB */
    while(trb_base != dq_reg->region.virtualBase)
    {
        dq_reg = dq_reg->next;
        if(dq_reg == rq->completedRegion)
        {

            xhci_log(XHCI_LOG_ERRORS, "Could not locate region containing TRB @ %p\n", start_trb);
            return driverOpBadState;
        }
    }

    transaction_state = 0;

    while(start_trb >= dq_reg->region.transfers)
    {
        int trb_type;

        trb_type = READ_TRB_TYPE_ADJ(start_trb->noOp.trbType);
        xhci_print_raw_trb(start_trb);
        switch(trb_type)
        {
            case TRB_SETUP:
                if(transaction_state & XHCI_DBG_HAS_SETUP)
                {
                    xhci_log(XHCI_LOG_ERRORS, "Duplicate setup trb found!\n");
                }
                else
                {
                    volatile setupStageTRB *setup = &start_trb->setup;
                    xhci_log(XHCI_LOG_ERRORS, "Setup TRB - BMRT: %X BREQ: %X wV: %X wI: %X wL: %X Flags: %X\n",
                             setup->bmRequestType, setup->bRequest, setup->wValue, setup->wIndex, setup->wLength, setup->cycleFlags);
                    transaction_state |= XHCI_DBG_HAS_SETUP | XHCI_DBG_EXIT;
                }
                break;
            case TRB_DATA:
                if(transaction_state & XHCI_DBG_HAS_DATA)
                {
                    xhci_log(XHCI_LOG_ERRORS, "Duplicate Data TRB found!\n");
                }
                else
                {
                    volatile dataStageTRB *data = &start_trb->data;
                    transaction_state |= XHCI_DBG_HAS_DATA;
                    xhci_log(XHCI_LOG_ERRORS, "Data TRB - TI: %X Address: %p Direction: %X Flags: %X\n", data->transferInfo, data->dataBuffer, data->direction, data->cycleFlags);
                }
                break;
            case TRB_STATUS:
                if(transaction_state & XHCI_DBG_HAS_STATUS)
                {
                    xhci_log(XHCI_LOG_ERRORS, "Duplicate status TRB found!\n");
                }
                else
                {
                    volatile statusStageTRB *status = &start_trb->status;
                    transaction_state |= XHCI_DBG_HAS_STATUS;
                    xhci_log(XHCI_LOG_ERRORS, "Status stage TRB - TI: %X Direction: %X Cycle flags: %X\n", status->transferInfo, status->direction, status->cycleFlags);
                }
                break;
            case TRB_EVENT_DATA:
                {
                    volatile eventDataTRB *event;
                    event = &start_trb->event_data;

                    xhci_log(XHCI_LOG_ERRORS, "Event Data TRB - Data: %p\n", event->eventData);
                }
                break;
            case TRB_NORMAL:
                {
                    volatile normalTRB *normal = &start_trb->normal;
                    xhci_log(XHCI_LOG_ERRORS, "Normal stage SG TRB - Buffer: %p TI: %X Flags: %X\n", normal->dataBuffer, normal->transferInfo, normal->cycleFlags);
                }
                break;
            default:
                break;
        }

        if(transaction_state & XHCI_DBG_EXIT)
            break;
        start_trb--;
    }


    return driverOpSuccess;
}
