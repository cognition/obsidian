#ifndef XHCI_ERROR_HANDLING_H_
#define XHCI_ERROR_HANDLING_H_

#include "xhci_controller.h"
#include "xhci_trb.h"

driverOperationResult xhci_process_invalid_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_dbe(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_babble(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_bad_usb_transaction(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_trb_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_resource_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_bandwidth_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_no_slots(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_invalid_stream_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_slot_not_enable_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_endpoint_not_enable_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_short_packet_cond(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_ring_underrun(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_ring_overrun(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_vf_ring_full(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_parameter_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_bandwidth_overrun_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_context_state_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_no_ping_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_event_ring_full_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_incompatible_device_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_missed_service_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_command_ring_stopped(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_command_aborted(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_command_stopped(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_command_stopped_length(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_command_stopped_short_packet(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_exit_latency_too_large(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_isoch_buffer_overrun(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_event_lost_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_process_undefined_err(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_invalid_stream_id_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_secondary_bandwidth_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);
driverOperationResult xhci_split_transaction_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context);

#endif
