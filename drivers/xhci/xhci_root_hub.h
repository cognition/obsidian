/* XHCI Root Hub header
 *
 * Author: Joseph Kinzel */

#ifndef XHCI_ROOT_HUB_H_
#define XHCI_ROOT_HUB_H_

#include <usb.h>
#include "xhci_ports.h"
#include "xhci_cq.h"

#define XHCI_DEV_IS_RH             0x80U

/* USB Hub feature selectors */
#define UHUB_HS_C_LOCAL_POWER      0x0000U
#define UHUB_HS_C_OVER_CURRENT     0x0001U

/* USB Hub port status bits */
#define UHUB_PS_CONNECTION         0x0001U
#define UHUB_PS_ENABLE             0x0002U
#define UHUB_PS_SUSPEND            0x0004U
#define UHUB_PS_OVER_CURRENT       0x0008U
#define UHUB_PS_RESET              0x0010U
#define UHUB_PS_USB2_L1            0x0020U

#define UHUB_PS_USB2_POWER         0x0100U
#define UHUB_PS_LOWSPEED           0x0200U
#define UHUB_PS_HIGHSPEED          0x0400U
#define UHUB_PS_TEST_MODE          0x0800U
#define UHUB_PS_INDICATOR          0x1000U

#define UHUB_PS_GET_LINK_STATE(x)  ((x)>>5)
#define UHUB_PS_SET_LINK_STATE(x)  ((x)<<5)
#define UHUB_PS_LINK_STATE_MASK    0x01E0U

#define UHUB_PS_USB3_POWER         0x0200U

#define UHUB_PS_GET_SPEED(x)       ((x)>>10)
#define UHUB_PS_SET_SPEED(x)       ((x)<<10)


/* USB Hub port status bit masks */
#define UHUB_PS_C_CONNECTION       0x0001U
#define UHUB_PS_C_ENABLE           0x0002U
#define UHUB_PS_C_SUSPEND          0x0004U
#define UHUB_PS_C_OC               0x0008U
#define UHUB_PS_C_RESET            0x0010U
#define UHUB_PS_C_L1               0x0020U
#define UHUB_PS_C_BH_RESET         0x0020U
#define UHUB_PS_C_LINK_STATE       0x0040U
#define UHUB_PS_C_CONFIG_ERR       0x0080U

/* Port feature selectors */
#define UHUB_PF_CONNECTION         0
#define UHUB_PF_ENABLE             1
#define UHUB_PF_SUSPEND            2
#define UHUB_PF_OVER_CURRENT       3
#define UHUB_PF_RESET              4
#define UHUB_PF_LINK_STATE         5
#define UHUB_PF_POWER              8
#define UHUB_PF_LOW_SPEED          9
#define UHUB_PF_LPM_L1             10

/* Port feature selector for change indicators */
#define UHUB_PF_C_CONNECTION       16
#define UHUB_PF_C_ENABLE           17
#define UHUB_PF_C_SUSPEND          18
#define UHUB_PF_C_OVER_CURRENT     19
#define UHUB_PF_C_RESET            20

#define UHUB_PF_TEST               21
#define UHUB_PF_INDICATOR          22

/* USB 2.0 LPM */
#define UHUB_PF_C_LPM_L1           23

/* USB 3.0+ */
#define UHUB_PF_U1_TIMEOUT         23
#define UHUB_PF_U2_TIMEOUT         24

#define UHUB_PF_C_LINK_STATE       25
#define UHUB_PF_C_CONFIG_ERROR     26

#define UHUB_PF_REMOTE_WAKE_MASK   27
#define UHUB_PF_BH_RESET           28
#define UHUB_PF_C_BH_RESET         29

#define UHUB_PF_FORCE_LINKPM       30

/* USB3.0+ Remote wake enable masks */
#define UHUB_USB3_RWM_CONN         0x01U
#define UHUB_USB3_RWM_DISCONN      0x02U
#define UHUB_USB3_RWM_OC           0x04U

#define UHUB_USB3_RWM_RESERVED     0xF8U

#define UHUB_LINK_STATE_U0         0x00U
#define UHUB_LINK_STATE_U1         0x01U
#define UHUB_LINK_STATE_U2         0x02U
#define UHUB_LINK_STATE_U3         0x03U
#define UHUB_LINK_STATE_DISABLED   0x04U
#define UHUB_LINK_STATE_RXDETECT   0x05U
#define UHUB_LINK_STATE_INACTIVE   0x06U
#define UHUB_LINK_STATE_POLLING    0x07U
#define UHUB_LINK_STATE_RECOVERY   0x08U
#define UHUB_LINK_STATE_HOT_RESET  0x09U
#define UHUB_LINK_STATE_COMPLIANCE 0x0AU
#define UHUB_LINK_STATE_LOOPBACK   0x0BU

#define UHUB_SET_LINK_STATE(x)     ((x)<<5)
#define UHUB_GET_LINK_STATE(x)     (((x)>>5) & 0xF)

#define USB_PSTATUS_PORT           0x00U
#define USB_PSTATUS_PD             0x01U
#define USB_PSTATUS_EXT            0x02U

#define MAKE_EXT_PORT_STATUS(rx_id, tx_id, rx_lc, tx_lc)         ((rx_id & 0xFU) | (tx_id<<4) | (rx_lc<<8) | (tx_lc<<12))

#define XHCI_RH_STATUS_EP                     1

#define SUPER_SPEED_LANE_SPEED          0
#define ESS_LANE_SPEED_5G_ID            1
#define ESS_LANE_SPEED_10G_ID           2


typedef struct COMPILER_PACKED _xhci_rh_control_queue_entry
{
    sync_object *sync;
    struct _xhci_rh_control_queue_entry * volatile next;
    usbControlTransaction transaction;
} xhci_rh_control_queue_entry;

typedef struct COMPILER_PACKED _xhci_rh_int_queue_entry
{
    sync_object *sync;
    struct _xhci_rh_int_queue_entry * volatile next;
    usbTransferTransaction transaction;
} xhci_rh_int_queue_entry;

typedef enum
{
    xhci_rh_default,
    xhci_rh_addressed,
    xhci_rh_configured
} xhci_rh_oper_state;

typedef enum
{
    xhci_rh_tt_running,
    xhci_rh_tt_stopped
} xhci_rh_tt_state;

typedef enum
{
    xhci_rh_ep_stopped,
    xhci_rh_ep_running,
    xhci_rh_ep_halted
} xhci_rh_endpoint_state;

typedef struct
{
    uint16_t status, change_status;
} status_reg_type;

typedef struct COMPILER_PACKED
{
    union
    {
        usb2HubDescriptor *usb2_hub_desc;
        usb3HubDescriptor *usb3_hub_desc;
    };
    /* Control pipe transaction queue */
    xhci_rh_control_queue_entry * volatile control_head, * volatile control_tail;
    /* Status interrupt pipe transaction queue */
    xhci_rh_int_queue_entry * volatile status_head, * volatile status_tail;

    uint16_t control_ep_status, status_ep_status;

    void *control_ep_lock, *status_ep_lock, *status_val_lock;

    pid_t control_pid, status_pid;

    volatile uint32_t *speeds;
    uint32_t speed_count;

    volatile uint64_t status_generation, last_pushed_status_gen, bus_addr;
    uint8_t *hub_state_bits, *port_prior_state;
    status_reg_type *port_status, hub_status;
    uint32_t *port_ext_status;
    void *hc_link;
    uint16_t dev_status, rh_flags;
    uint8_t port_count, controller_port_offset, rh_usb_addr, state_bytes_required;
    port_generation_type hub_generation;
    uint8_t hub_config;
    volatile uint8_t hub_active;
    xhci_rh_tt_state tt_state;
    xhci_rh_oper_state hub_operational_state;
    xhci_rh_endpoint_state control_ep_state, status_ep_state;
} xhci_rh_state;

void xhci_rh_read_port_status(xhci_controller_state *hcState, const int port_index);
driverOperationResult xhci_rh_register_hubs(xhci_controller_state *hcState);
driverOperationResult xhci_rh_add_hub(xhci_controller_state *hcState, const int port_start, const int port_count, const port_generation_type proto, volatile uint32_t *speed_ids, const uint8_t id_count);

busOperationResult xhci_rh_queue_transaction(xhci_controller_state *hcState, const uint8_t rh_index, const uint8_t endpoint,
    const uint8_t direction, usbControllerTransaction *transaction, const uint32_t t_info_length, sync_object *sync);
busOperationResult xhci_rh_run_transactions(xhci_controller_state *hcState, const uint8_t rh_index, const uint8_t endpoint, const uint8_t dir, sync_object *sync);
busOperationResult xhci_rh_config_device(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbConfigureAddress *config_address, sync_object *sync);

usb_hw_op_result xhci_rh_process_control_transaction(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req);
usb_hw_op_result xhci_rh_process_class_control_transaction(xhci_controller_state *hcState, xhci_rh_state *rh_state, usbControlTransaction *req);
usb_hw_op_result xhci_rh_device_get_descriptor(xhci_rh_state *rh_state, usbControlTransaction *req);
usb_hw_op_result xhci_rh_process_class_get_descriptor(xhci_rh_state *rh_state, usbControlTransaction *req);

 #endif // XHCI_ROOT_HUB_H_
