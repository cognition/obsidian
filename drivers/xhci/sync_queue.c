#include "sync_queue.h"

/*! Returns the the TRB on top of the queue
 *  \param sq Command wait queue pointer
 *  \return Target TRB if one is present, NULL otherwise
 */
volatile commandTRB *sync_queue_peek(sync_queue *sq)
{
    if(!sq->head)
        return NULL;
    else
        return sq->head->target;
}

/*! Pops the top sync item off the queue
 *  \param sq Command wait queue pointer
 *  \return Sync object pointer on the top of the queue or NULL if the queue is empty
 */
sync_object *sync_queue_pop(sync_queue *sq)
{
    if(!sq->head)
        return NULL;
    else
    {
        void *sync_obj;
        sync_link *link = sq->head;
        sync_obj = link->sync_obj;

        if(sq->head == sq->tail)
            sq->tail = NULL;

        sq->head = sq->head->next;
        free(link);
        return sync_obj;
    }
}

/*! Pushes a new item onto the command wait queue
 *  \param sq Command wait queue pointer
 *  \param target Virtual pointer to the TRB being waited on
 *  \param  sync_obj Synchronization object
 */
void sync_queue_push(sync_queue *sq, volatile commandTRB *target, sync_object *sync_obj)
{
    sync_link *link;

    link = (sync_link *)calloc(1, sizeof(sync_link));
    link->target = target;
    link->sync_obj = sync_obj;

    if(sq->tail)
        sq->tail->next = link;
    sq->tail = link;

    if(!sq->head)
        sq->head = link;

    sync_object_reset(sync_obj);
}
