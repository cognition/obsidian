#include "xhci_error_handle.h"
#include "xhci_cq.h"
#include "xhci_log.h"

driverOperationResult xhci_process_invalid_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Invalid error!\n");
	return driverOpBadArg;
}

driverOperationResult xhci_process_dbe(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.10.2.5
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Data buffer error!\n");

	return driverOpSuccess;
}

driverOperationResult xhci_process_babble(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.10.2.4
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Babble error!\n");


	return driverOpSuccess;
}

driverOperationResult xhci_process_bad_usb_transaction(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.10.2.3
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: USB transaction error!\n");

	return driverOpSuccess;
}

driverOperationResult xhci_process_trb_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
    volatile softwareIssuedTRB *srcTrb;
    volatile uint32_t *rawTrb;
    uint64_t commandPhys;
    uint8_t slot, ep;
    xhci_slot_state *src_slot;
	//Section 4.10.2.2
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: TRB error!\n");

	commandPhys = cce->commandCompletion.commandTrbPtr;
	slot = cce->commandCompletion.slotId;
    src_slot = &hcState->slots[slot];

    if(!xhci_get_completed_trb(&srcTrb, commandPhys, hcState->hcCommandQueue))
    {
        ep = 0;
        for(int epIndex = 0; epIndex < src_slot->ep_count; epIndex++)
        {
            ringQueue *rq;
            rq = xhci_get_endpoint_ring_from_id(hcState, slot, epIndex+XHCI_EP_CONTEXT_START);
            if(!rq)
                return driverOpBadState;
            if(xhci_get_completed_trb(&srcTrb, commandPhys, rq))
            {
                ep = epIndex;
                break;
            }
        }
    }
    else
        ep = 0;

    rawTrb = (volatile uint32_t *)srcTrb;

	xhci_log(XHCI_LOG_ERRORS, "TRB Error - Slot: %u EP: %u Input TRB type %u:\n"
           "\t[0]: %08X\n\t[4]: %08X\n\t[8]: %08X\n\t[12]: %0X\n", slot, ep, READ_TRB_TYPE_ADJ(srcTrb->command.noOp.trbType), rawTrb[0], rawTrb[1], rawTrb[2], rawTrb[3]);

	return driverOpSuccess;
}

driverOperationResult xhci_process_resource_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Asserted by address device or configure endpoint if HC resources are not NO_SLOT_AVAILABLE
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Resource error!\n");
	return driverOpResources;
}

driverOperationResult xhci_process_bandwidth_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Bandwidth error!\n");
	return driverOpResources;
}

driverOperationResult xhci_process_no_slots(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.6.3
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: No slots error!\n");
	return driverOpResources;
}

driverOperationResult xhci_invalid_stream_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.12.2.1
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Invalid stream type error!\n");
	return driverOpBadArg;
}

driverOperationResult xhci_slot_not_enable_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Slot not enabled error!\n");
	return driverOpBadArg;
}

driverOperationResult xhci_endpoint_not_enable_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Endpoint not enabled error!\n");
	return driverOpBadArg;
}

driverOperationResult xhci_short_packet_cond(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Short packet!\n");
	return driverOpHardwareError;
}

driverOperationResult xhci_ring_underrun(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.10.3.1
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Ring underrun!\n");
	return driverOpNoMemory;
}

driverOperationResult xhci_ring_overrun(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.10.3.1
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Ring overrun!\n");
	return driverOpNoMemory;
}

driverOperationResult xhci_vf_ring_full(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: VF Event ring full!\n");
	return driverOpNoMemory;
}

driverOperationResult xhci_parameter_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
    uint32_t cic, cie;
    cic = hcState->caps->hccparams2 & XHCI_HCC2_CIC;
    cie = hcState->ops->configure & USB_CONFIGURE_CIE;
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Context parameter error! CIC: %X CIE: %X\n", cic, cie);
	return driverOpBadArg;
}

driverOperationResult xhci_bandwidth_overrun_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Bandwidth overrun error!\n");
	return driverOpResources;
}

driverOperationResult xhci_context_state_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
    uint8_t trb_type;
    volatile commandTRB *trb_virt;
    volatile slotContext *sc;

    trb_virt = xhci_hc_get_command_ptr(hcState, &cce->commandCompletion);
    trb_type = READ_TRB_TYPE_ADJ(trb_virt->noOp.trbType);

    sc = (volatile slotContext *)xhci_get_dev_context(hcState, cce->commandCompletion.slotId, 0);

	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Context state error! TRB type: %u\n", trb_type);
	switch(trb_type)
	{
        case TRB_ADDRESS_DEVICE:
            xhci_log(XHCI_LOG_ERRORS, "\tAddress device BSR: %u Slot state: %u\n", trb_virt->addressDevice.trbType & TRB_AD_BSR, XHCI_SLOT_CONTEXT_STATE(sc->slotState) );
            break;
	}
	return driverOpBadArg;
}

driverOperationResult xhci_no_ping_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.23.5.2.1
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: No ping error!\n");
	return driverOpHardwareError;
}

driverOperationResult xhci_event_ring_full_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.9.4
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Event ring full!\n");
	return driverOpNoMemory;
}

driverOperationResult xhci_incompatible_device_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Invalid device error!\n");
	return driverOpHardwareError;
}

driverOperationResult xhci_missed_service_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Sections 4.9.4 and 4.10.3.2
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Missed Service error!\n");
	return driverOpResources;
}

driverOperationResult xhci_command_ring_stopped(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.6
	return driverOpSuccess;
}

driverOperationResult xhci_command_aborted(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.6
	return driverOpSuccess;
}

driverOperationResult xhci_command_stopped(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.6
	return driverOpSuccess;
}

driverOperationResult xhci_command_stopped_length(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.6.9
	return driverOpSuccess;
}

driverOperationResult xhci_command_stopped_short_packet(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.6.9, 4.10.1.1 and 4.11.5.2
	return driverOpSuccess;
}

driverOperationResult xhci_exit_latency_too_large(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.23.5.2.2 and 4.6.6.1
	return driverOpBadArg;
}

driverOperationResult xhci_isoch_buffer_overrun(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.14.2.1.1 and 4.14.2.1.3
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Isochronous buffer overrun error!\n");
	return driverOpNoMemory;
}

driverOperationResult xhci_event_lost_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.10.1
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Event lost error!\n");
	return driverOpResources;
}

driverOperationResult xhci_process_undefined_err(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.11.6
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Undefined error!\n");
	return driverOpHardwareError;
}

driverOperationResult xhci_invalid_stream_id_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.12.2.1
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Invalid stream id error!\n");
	return driverOpBadArg;
}

driverOperationResult xhci_secondary_bandwidth_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.16
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Secondary bandwidth error!\n");
	return driverOpResources;
}

driverOperationResult xhci_split_transaction_error(xhci_controller_state *hcState, volatile eventTRB *cce, void *context)
{
	//Section 4.10.3.3
	xhci_log(XHCI_LOG_ERRORS, "XHCI Error: Split Transaction error!\n");
	return driverOpBusError;
}
