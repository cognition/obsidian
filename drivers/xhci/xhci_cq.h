#ifndef XHCI_COMMAND_QUEUE_H_
#define XHCI_COMMAND_QUEUE_H_

#include <compiler.h>
#include "xhci_controller.h"

#define XHCI_CRCR_RCS           0x01U
#define XHCI_CRCR_CMD_STOP      0x02U
#define XHCI_CRCR_CMD_ABORT     0x04U
#define XHCI_CRCR_RUNNING       0x08U

#define XHCI_EP_CONTEXT_START   0x02U

volatile commandTRB *xhci_hc_get_trb(xhci_controller_state *hcState, uint8_t *cycleState);

driverOperationResult xhci_create_ring(xhci_controller_state *hcState, ringQueue **ringOut);
void xhci_link_ring(xhci_controller_state *hcState, ringRegion *predecessor, ringRegion *region, ringQueue *ring);
void xhci_ring_unlink(xhci_controller_state *hcState, ringRegion *region, ringRegion **ringHead);
driverOperationResult xhci_destroy_ring(xhci_controller_state *hcState, ringQueue *ring);
driverOperationResult xhci_destroy_ring_region(xhci_controller_state *hcState, ringRegion *reg);

driverOperationResult xhci_update_ring_completed(ringQueue *rq, volatile softwareIssuedTRB *trb);
driverOperationResult xhci_get_batch_trb(const int requested_trbs, xhci_controller_state *hcState, ringQueue *ring, volatile softwareIssuedTRB **trb_start, uint8_t *cycleState);
driverOperationResult xhci_get_ring_trb(volatile softwareIssuedTRB **nextTRB, uint8_t *cycleState, xhci_controller_state *hcState, ringQueue *ring);
void xhci_trb_set_cycle(volatile void *trb, const uint8_t cycle_value);
ringQueue *xhci_get_endpoint_ring_from_id(xhci_controller_state *hcState, const uint8_t slot, const uint8_t endpoint_id);
int xhci_get_completed_trb(volatile softwareIssuedTRB **trbRet, const uint64_t trb_address, ringQueue *ring);


uint64_t xhci_ring_get_base(ringQueue *ring);
uint64_t xhci_ring_get_dq(ringQueue *ring);
uint64_t xhci_ring_get_eq(ringQueue *ring);

ringQueue *xhci_get_endpoint_ring(xhci_controller_state *hc, const uint8_t slot, const uint8_t ep, const int in_out);
volatile void *xhci_get_next_trb(ringQueue *rq, volatile void *init_trb);

uint32_t xhci_calc_ep_dci(const uint32_t endpoint, const uint32_t direction, const uint8_t type);
uint32_t xhci_calc_ep_ici(const uint32_t endpoint, const uint32_t direction, const uint8_t type);

void xhci_process_device_configured(xhci_controller_state *hcState, xhci_slot_state *slot_state, const int slot_num);
volatile void *xhci_get_icc_context(xhci_controller_state *hcState, const int slot, const int index);
volatile void *xhci_get_dev_context(xhci_controller_state *hcState, const int slot, const int epIndex);
uint32_t xhci_get_ep_mps(xhci_controller_state *hcState, const int slot, const int endpoint, const int direction, const uint8_t type);
void xhci_print_ep_info(xhci_controller_state *hcState, const uint8_t slot, const uint8_t ep_dci);

void xhci_process_potential_command_sync(xhci_controller_state *hcState, volatile commandTRB *trb, volatile commandCompletionEventTRB *cce);

volatile commandTRB *xhci_hc_get_command_ptr(xhci_controller_state *hcState, volatile commandCompletionEventTRB *cce);

#endif
