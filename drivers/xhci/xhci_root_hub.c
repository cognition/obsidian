/* XHCI Root Hub implementation
 *
 * Contains the functions that emulate the USB Hub specification for an XHCI controller
 *
 * Author: Joseph Kinzel */

#include <usb.h>
#include <driverapi.h>
#include "xhci_root_hub.h"
#include "xhci_ports.h"
#include "xhci_log.h"


static void xhci_set_usb2_speed_from_psi(const xhci_port_info *port_info, xhci_rh_state *rh_state, status_reg_type *status_reg, const uint32_t psi_value)
{
    int usb_speed;

    usb_speed = xhci_get_port_speed(port_info, psi_value);

    switch(usb_speed)
    {
        case USB_SPEED_LOW:
            status_reg->status &= ~UHUB_PS_HIGHSPEED;
            status_reg->status |= UHUB_PS_LOWSPEED;
            break;
        case USB_SPEED_FULL:
            status_reg->status &= ~(UHUB_PS_LOWSPEED | UHUB_PS_HIGHSPEED);
            break;
        case USB_SPEED_HIGH:
            status_reg->status &= ~UHUB_PS_LOWSPEED;
            status_reg->status |= UHUB_PS_HIGHSPEED;
            break;
        default:
            break;
    }
}

/*! Updates the root hub's port internal extended status based on the speed of the attached device
 *
 *  \param port_info Controller's port info for the port
 *  \param rh_state Root hub context structure
 *  \param status_reg Root hub port's basic status registers
 *  \param ext_status Root hub port's extended status register
 *  \param psi_value PSI value from the controller's port status register
 */
static void xhci_set_usb3_speed_from_psi(const xhci_port_info *port_info, xhci_rh_state *rh_state, status_reg_type *status_reg, volatile uint32_t *ext_status, const uint32_t psi_value)
{
    int usb_speed;

    usb_speed = xhci_get_port_speed(port_info, psi_value);

    switch(usb_speed)
    {
        case USB_SPEED_SUPER:
            *ext_status = MAKE_EXT_PORT_STATUS(SUPER_SPEED_LANE_SPEED, SUPER_SPEED_LANE_SPEED, 1, 1);
            return;
            break;
        case USB_SPEED_ESUPER_1X1:
            if(rh_state->hub_generation > PORT_PROTO_USB30)
                *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_5G_ID, ESS_LANE_SPEED_5G_ID, 1, 1);
            return;
            break;
        case USB_SPEED_ESUPER_1X2:
            if(rh_state->hub_generation == PORT_PROTO_USB32)
            {
                *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_5G_ID, ESS_LANE_SPEED_5G_ID, 2, 2);
                return;
            }
            break;
        case USB_SPEED_ESUPER_2X1:
            if(rh_state->hub_generation >= PORT_PROTO_USB31)
            {
                *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_10G_ID, ESS_LANE_SPEED_10G_ID, 1, 1);
                return;
            }
            break;
        case USB_SPEED_ESUPER_2X2:
            if(rh_state->hub_generation == PORT_PROTO_USB32)
            {
                *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_10G_ID, ESS_LANE_SPEED_10G_ID, 2, 2);
                return;
            }
            break;
        default:
            break;
    }

    xhci_log(XHCI_LOG_ERRORS, "RH - Unable to identify device speed on root hub @ %X PSI value: %u Speed: %u\n", rh_state->bus_addr, psi_value, usb_speed);
}


/*! Reads the updated status of controller port and update their status in the root hub
 *
 *  \param hcState Host controller context pointer
 *  \param port_index Host controller port indiex
 */
void xhci_rh_read_port_status(xhci_controller_state *hcState, const int port_index)
{
    volatile portReg *pr;
    xhci_rh_state *rh_state;
    uint8_t *prior_state_ptr;
    xhci_port_info *port_info;
    status_reg_type *port_status;
    uint32_t psc_value, init_status, init_change_status, *ext_status;
    int port_rh_index;

    port_info = &hcState->port_info[port_index-1];

    rh_state = (xhci_rh_state *)hcState->root_hub_info[port_info->rh_index];

    if(!rh_state)
    {
        xhci_log(XHCI_LOG_ERRORS, "Unable to find root hub corresponding to port %u\n", port_index);
        return;
    }

    port_rh_index = port_index - rh_state->controller_port_offset;

    port_status = &rh_state->port_status[port_rh_index-1];
    prior_state_ptr = &rh_state->port_prior_state[port_rh_index-1];
    ext_status = &rh_state->port_ext_status[port_rh_index-1];


    pr = &hcState->ops->portRegSet[port_index-1];
    psc_value = pr->portStatusControl;


    dapi_lock_acquire(rh_state->status_val_lock);

    init_status = port_status->status;
    init_change_status = port_status->change_status;

    xhci_log(XHCI_LOG_INTERNAL, "RH - Port status updated for controller port %u PSC: %X Port RH Index: %u Hub base offset: %u\n\tPort status: %X Port Change: %X\n",
         port_index, psc_value, port_rh_index, rh_state->controller_port_offset, init_status, init_change_status);


    /* Over current status change */
    if(psc_value & XHCI_PORTSC_OCC)
    {
        port_status->change_status |= UHUB_PS_C_OC;
        port_status->status &= ~UHUB_PS_OVER_CURRENT;

        if(rh_state->hub_generation == PORT_PROTO_USB20)
        {
            if(psc_value & XHCI_PORTSC_PP)
                port_status->status |= UHUB_PS_USB2_POWER;
            else
                port_status->status &= ~UHUB_PS_USB2_POWER;
        }
        else
        {
            if(psc_value & XHCI_PORTSC_PP)
                port_status->status |= UHUB_PS_USB3_POWER;
            else
                port_status->status &= ~UHUB_PS_USB3_POWER;
        }

        if(psc_value & XHCI_PORTSC_OCA)
            port_status->status |= UHUB_PS_OVER_CURRENT;
    }

    /* Config error change */
    if(psc_value & XHCI_PORTSC_CEC)
        port_status->change_status |= UHUB_PS_C_CONFIG_ERR;

    /* Connect status change */
    if(psc_value & XHCI_PORTSC_CSC)
    {
        uint32_t psi_value;
        int port_speed;

        port_status->change_status |= UHUB_PS_C_CONNECTION;
        port_status->status &= ~UHUB_PS_CONNECTION;

        if(psc_value & XHCI_PORTSC_CCS)
        {
            port_status->status |= UHUB_PS_CONNECTION;

            switch(rh_state->hub_generation)
            {
                case PORT_PROTO_USB20:
                    /* PED is cleared on disconnect and the port will need to be reset before the speed field is valid */
                    port_status->status &= ~UHUB_PS_ENABLE;
                    break;
                case PORT_PROTO_USB32:
                    psi_value = XHCI_PORTSC_PORTSPEED(psc_value);
                    port_speed = xhci_get_port_speed(&hcState->port_info[port_index-1], psi_value);

                    switch(port_speed)
                    {
                        case USB_SPEED_SUPER:
                            *ext_status = MAKE_EXT_PORT_STATUS(SUPER_SPEED_LANE_SPEED, SUPER_SPEED_LANE_SPEED, 1, 1);
                            break;
                        case USB_SPEED_ESUPER_1X2:
                            *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_5G_ID, ESS_LANE_SPEED_5G_ID, 2, 2);
                            break;
                        case USB_SPEED_ESUPER_2X2:
                            *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_10G_ID, ESS_LANE_SPEED_10G_ID, 2, 2);
                            break;
                        default:
                            break;
                    }
                    break;
                case PORT_PROTO_USB31:
                    psi_value = XHCI_PORTSC_PORTSPEED(psc_value);
                    port_speed = xhci_get_port_speed(&hcState->port_info[port_index-1], psi_value);

                    switch(port_speed)
                    {
                        case USB_SPEED_SUPER:
                            *ext_status = MAKE_EXT_PORT_STATUS(SUPER_SPEED_LANE_SPEED, SUPER_SPEED_LANE_SPEED, 1, 1);
                            break;
                        case USB_SPEED_ESUPER_1X1:
                            *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_5G_ID, ESS_LANE_SPEED_5G_ID, 1, 1);
                            break;
                        case USB_SPEED_ESUPER_2X1:
                            *ext_status = MAKE_EXT_PORT_STATUS(ESS_LANE_SPEED_10G_ID, ESS_LANE_SPEED_10G_ID, 1, 1);
                            break;
                        default:
                            break;
                    }

                case PORT_PROTO_USB30:
                    psi_value = XHCI_PORTSC_PORTSPEED(psc_value);
                    port_speed = xhci_get_port_speed(&hcState->port_info[port_index-1], psi_value);

                    if(port_speed != USB_SPEED_SUPER)
                    {
                        xhci_log(XHCI_LOG_ERRORS, "RH Invalid port speed for USB3x root hub port %u\n", port_index);
                        dapi_lock_release(rh_state->status_val_lock );
                        return;
                    }
                    break;
                default:
                    xhci_log(XHCI_LOG_ERRORS, "RH Invalid port generation for root hub port %u\n", port_index);
                    dapi_lock_release(rh_state->status_val_lock );
                    return;
                    break;
            }
        }
    }

    /* Port reset status change */
    if(psc_value & XHCI_PORTSC_PRC)
    {
        port_status->change_status |= UHUB_PS_C_RESET;
        port_status->status &= ~UHUB_PS_RESET;
        xhci_log(XHCI_LOG_INTERNAL, "RH - Port reset change on port %u\n", port_index);
        if(psc_value & XHCI_PORTSC_WRC)
            port_status->change_status |= UHUB_PS_C_BH_RESET;

        if(psc_value & XHCI_PORTSC_PED)
        {
            int psi_value;

            port_status->status |= UHUB_PS_ENABLE;
            psi_value = XHCI_PORTSC_PORTSPEED(psc_value);
            if(!psi_value)
            {
                xhci_log(XHCI_LOG_ERRORS, "Invalid PSI value on port %u after reset!\n", port_index);
            }
            xhci_set_usb2_speed_from_psi(port_info, rh_state, port_status, psi_value);


        }
        else
            xhci_log(XHCI_LOG_INFO, "Port @ %X:%u is not enabled despite reset!\n", rh_state->bus_addr, port_index);


    }

    /* Port enable status change */
    if(psc_value & XHCI_PORTSC_PEC)
    {
        if(rh_state->hub_generation == PORT_PROTO_USB20)
        {
            port_status->change_status |= UHUB_PS_C_ENABLE;
            port_status->status &= ~UHUB_PS_ENABLE;
        }
    }

    /* Port link status change */
    if(psc_value & XHCI_PORTSC_PLC)
    {
        int current_state;

        current_state = XHCI_PORTSC_GET_PLS(psc_value);

        switch(current_state)
        {
            case XHCI_PLS_READ_U0:
                if(rh_state->hub_generation == PORT_PROTO_USB20)
                {
                    switch(*prior_state_ptr)
                    {
                        case XHCI_PLS_READ_U3:
                            port_status->status &= ~UHUB_PS_SUSPEND;
                            port_status->change_status |= UHUB_PS_C_SUSPEND;
                            break;
                        case XHCI_PLS_READ_RESUME:
                            /* Device is in the process of resuming or has resumed operation */
                            break;
                        case XHCI_PLS_READ_U2:
                            /* L1 Resume complete */
                            port_status->status &= ~UHUB_PS_USB2_L1;
                            port_status->change_status |= UHUB_PS_C_L1;
                            break;
                        case XHCI_PLS_READ_U0:
                            /* L1 Entry reject */
                            break;
                        default:
                            xhci_log(XHCI_LOG_ERRORS, "Root hub state mismatch prior: %u current: %u\n", *prior_state_ptr, current_state);
                            break;
                    }
                }
                else
                {
                    port_status->status &= ~UHUB_PS_LINK_STATE_MASK;
                    port_status->status |= UHUB_PS_SET_LINK_STATE( UHUB_LINK_STATE_U0 );
                    port_status->change_status |= UHUB_PS_C_LINK_STATE;
                }
                break;
            case XHCI_PLS_READ_INACTIVE:
                /* TODO: Figure out if anything needs to happen here for USB3 protocol ports */
                port_status->status &= ~UHUB_PS_LINK_STATE_MASK;
                port_status->status |= UHUB_PS_SET_LINK_STATE( UHUB_LINK_STATE_INACTIVE );
                port_status->change_status |= UHUB_PS_C_LINK_STATE;

                break;
            case XHCI_PLS_READ_RESUME:
                *prior_state_ptr = XHCI_PLS_READ_RESUME;
                break;
            case XHCI_PLS_READ_U3:
                if(rh_state->hub_generation == PORT_PROTO_USB20)
                {
                    port_status->status |= UHUB_PS_SUSPEND;
                    port_status->change_status |= UHUB_PS_C_SUSPEND;
                }
                else
                {
                    port_status->status &= ~UHUB_PS_LINK_STATE_MASK;
                    port_status->status |= UHUB_PS_SET_LINK_STATE( UHUB_LINK_STATE_U3 );
                    port_status->change_status |= UHUB_PS_C_LINK_STATE;
                }
                *prior_state_ptr = XHCI_PLS_READ_U3;
                break;
        }
    }

    psc_value &= ~XHCI_PORTSC_PED;
    psc_value |= XHCI_PORTSC_CHANGE_STATUS;

    pr->portStatusControl = psc_value;


    if( (init_status != port_status->status) || (init_change_status != port_status->change_status) )
    {
        int port_mask_index;
        uint8_t mask = 1;

        mask <<= (port_rh_index%8);
        port_mask_index = (port_rh_index/8);


        dapi_lock_acquire(rh_state->status_ep_lock);
        rh_state->status_generation++;

        rh_state->hub_state_bits[port_mask_index] |= mask;
        dapi_lock_release(rh_state->status_ep_lock);
    }

    dapi_lock_release(rh_state->status_val_lock );

}

static void xhci_rh_usb2_init_port(xhci_controller_state *hcState, xhci_rh_state *rh_state, const int port_index)
{
    uint32_t psc_value = hcState->ops->portRegSet[port_index-1].portStatusControl;

    rh_state->port_status[port_index-1].change_status = 0;
    rh_state->port_status[port_index-1].status = 0;

    /* Disable and power off the port if supported */
    hcState->ops->portRegSet[port_index-1].portStatusControl = XHCI_PORTSC_PED;

    if(psc_value & XHCI_PORTSC_DR)
    {
        int byte_index;
        uint8_t mask;

        byte_index = (port_index-1)/8;
        mask = 1;
        mask <<= (port_index-1)%8;

        rh_state->usb2_hub_desc->masks[byte_index] |= mask;
    }
}

static void xhci_rh_usb3_init_port(xhci_controller_state *hcState, xhci_rh_state *rh_state, const int port_index)
{
    uint32_t psc_value = hcState->ops->portRegSet[port_index-1].portStatusControl;

    rh_state->state_bytes_required = 2;
    rh_state->port_status[port_index-1].change_status = 0;
    rh_state->port_status[port_index-1].status = UHUB_SET_LINK_STATE( UHUB_LINK_STATE_DISABLED );
    rh_state->port_ext_status[port_index-1] = 0;
    /* Disable and power off the port if supported */
    hcState->ops->portRegSet[port_index-1].portStatusControl = XHCI_PORTSC_PED;

    if(psc_value & XHCI_PORTSC_DR)
    {
        uint16_t mask = 1;

        mask <<= (port_index - 1)%16;

        rh_state->usb3_hub_desc->deviceRemovable |= mask;
    }

}

void xhci_rh_usb2_init_desc(xhci_controller_state *hcState ,xhci_rh_state *rh_state, const int port_count)
{
    int bitmask_bytes;
    usb2HubDescriptor *desc;

    bitmask_bytes = (port_count+1)/8;
    if((port_count+1)%8)
        bitmask_bytes++;

    rh_state->state_bytes_required = bitmask_bytes;

    desc = (usb2HubDescriptor *)calloc(sizeof(usb2HubDescriptor) + bitmask_bytes*2, 1);
    rh_state->usb2_hub_desc = desc;

    desc->bLength = sizeof(usb2HubDescriptor) + bitmask_bytes*2;
    desc->bDescriptorType = usbDescHub;
    desc->bNbrPorts = port_count;

    memset(&desc->masks[bitmask_bytes], 0xFF, bitmask_bytes);

    for(int port_index = 1; port_index <= port_count; port_index++)
        xhci_rh_usb2_init_port(hcState, rh_state, port_index);


}

void xhci_rh_usb3_init_desc(xhci_controller_state *hcState, xhci_rh_state *rh_state, const int port_count)
{
    usb3HubDescriptor *desc;

    desc = (usb3HubDescriptor *)calloc(sizeof(usb3HubDescriptor), 1);
    rh_state->usb3_hub_desc = desc;

    desc->bLength = sizeof(usb3HubDescriptor);
    desc->bDescriptorType = usbDescESSHub;
    desc->bNbrPorts = port_count;

    desc->wHubCharacteristics = UHUB_ATTRIBUTES_OCP_PER_PORT;

    desc->bPwrOn2PwrGood = 5;
    desc->bHubContrCurrent = 0;
    desc->bHubHdrDecLat = 0;
    desc->wHubDelay = 0;

    xhci_log(XHCI_LOG_INFO, "RH - Initializing %u ports for USB3 hub..\n", port_count);
    for(int port_index = 1; port_index <= port_count; port_index++)
        xhci_rh_usb3_init_port(hcState, rh_state, port_index);

}

busOperationResult xhci_rh_queue_control(xhci_controller_state *hcState, xhci_rh_state *rh_state, const int ep_num, usbControllerTransaction *transaction, sync_object *sync)
{
    xhci_rh_control_queue_entry *cq_entry;


    if(ep_num)
        return busOpAddressInvalid;

    cq_entry = (xhci_rh_control_queue_entry *)calloc(sizeof(xhci_rh_control_queue_entry), 1);

    cq_entry->sync = sync;
    memcpy(&cq_entry->transaction, &transaction->control, sizeof(usbControlTransaction));

    dapi_lock_acquire(rh_state->control_ep_lock);

    cq_entry->next = NULL;

    if(rh_state->control_tail)
        rh_state->control_tail->next = cq_entry;
    else
        rh_state->control_head = cq_entry;

    rh_state->control_tail = cq_entry;

    dapi_lock_release(rh_state->control_ep_lock);

    return busOpSuccess;
}

busOperationResult xhci_rh_queue_status(xhci_controller_state *hcState, xhci_rh_state *rh_state, const int ep_num, usbControllerTransaction *transaction, sync_object *sync)
{
    xhci_rh_int_queue_entry *entry;

    if(ep_num != 1)
    {
        xhci_log(XHCI_LOG_ERRORS, "RH - Bad request for interrupt transaction on device!\n");
        return busOpAddressInvalid;
    }

    if( !(transaction->flags & USB_TRANSACTION_IN) )
    {
        xhci_log(XHCI_LOG_ERRORS, "RH - Bad request for interrupt transaction on status endpoint - Wrong direction!\n");
        return busOpNoAccess;
    }

    if(transaction->transfer.length < rh_state->state_bytes_required)
    {
        xhci_log(XHCI_LOG_ERRORS, "RH - Bad request for interrupt transaction on status endpoint - Message buffer too short expected: %u Found: %u\n", rh_state->state_bytes_required,
                 transaction->transfer.length);
        return busOpInvalidArg;
    }

    entry = (xhci_rh_int_queue_entry *)calloc(sizeof(xhci_rh_int_queue_entry), 1);

    entry->sync = sync;
    memcpy(&entry->transaction, &transaction->transfer, sizeof(usbTransferTransaction));

    dapi_lock_acquire(rh_state->status_ep_lock);

    if(rh_state->status_tail)
        rh_state->status_tail->next = entry;
    else
        rh_state->status_head = entry;

    rh_state->status_tail = entry;

    dapi_lock_release(rh_state->status_ep_lock);

    return busOpSuccess;
}

/*! Root hub control transaction processing loop
 *
 *  \param state Root hub state
 */
void xhci_rh_control_pipe_loop(void *state)
{
    xhci_rh_state *rh_state;

    rh_state = (xhci_rh_state *)state;

    /* Loop for as long as the device is active */
    do
    {
        xhci_rh_control_queue_entry *entry;


        entry = rh_state->control_head;

        while(entry)
        {
            xhci_rh_control_queue_entry *prior_entry;
            sync_object *sync;
            usb_hw_op_result control_result;

            xhci_log(XHCI_LOG_INTERNAL, "Processing control queue entry %p ADDR: %X BMRT: %X BR: %X\n", entry, rh_state->bus_addr, entry->transaction.bm_request_type, entry->transaction.b_request);
            sync = entry->sync;

            dapi_lock_acquire(rh_state->control_ep_lock);
            prior_entry = entry;
            entry = entry->next;

            rh_state->control_head = entry;
            if(!entry)
            {
                rh_state->control_tail = NULL;
            }

            dapi_lock_release(rh_state->control_ep_lock);

            control_result = xhci_rh_process_control_transaction((xhci_controller_state *)rh_state->hc_link, rh_state, &prior_entry->transaction);

            if(sync)
                sync_object_signal(sync, control_result );


            free(prior_entry);

        }

        wait(50);
    } while(rh_state->control_ep_state == xhci_rh_ep_running );

    xhci_log(XHCI_LOG_INTERNAL, "RH - Exiting control pipe loop for device @ %X\n", rh_state->bus_addr);
}

/*! Root hub status pipe event loop
 *
 *  \param state Root hub state
 */
void xhci_rh_interrupt_pipe_loop(void *state)
{
    xhci_rh_state *rh_state;

    rh_state = (xhci_rh_state *)state;

    do
    {
        xhci_rh_int_queue_entry *entry;
        sync_object *sync;
        uint64_t current_status_gen, next_status_gen;

        dapi_lock_acquire(rh_state->status_ep_lock);
        entry = rh_state->status_head;
        if(entry)
        {
            sync = entry->sync;

            current_status_gen = rh_state->status_generation;
            next_status_gen = rh_state->last_pushed_status_gen;

            if(current_status_gen > next_status_gen)
            {
                memcpy(entry->transaction.buffer, rh_state->hub_state_bits, rh_state->state_bytes_required);
                memset(rh_state->hub_state_bits, 0, rh_state->state_bytes_required);


                rh_state->last_pushed_status_gen = current_status_gen;

                rh_state->status_head = entry->next;
                if(!entry->next)
                    rh_state->status_tail = NULL;

                dapi_lock_release(rh_state->status_ep_lock);

                if(sync)
                    sync_object_signal(sync, usb_op_success);

                free(entry);
            }
            else
                dapi_lock_release(rh_state->status_ep_lock);
        }
        else
            dapi_lock_release(rh_state->status_ep_lock);


        wait(512);
    } while(rh_state->status_ep_state == xhci_rh_ep_running);


    xhci_log(XHCI_LOG_INTERNAL, "RH - Status EP task exited for device @ %X\n", rh_state->bus_addr);
    asm volatile ("cli\n"
              "hlt\n"::);

}

/*! Adds a new internal root hub device state to the XHCI controller
 *
 *  \param hcState Host controller state
 *  \param port_start Starting port index of the range of ports covered by the hub
 *  \param port_count Number of downstream ports on the root hub
 *  \param proto USB Protocol/version supported by the root hub
 *
 */
driverOperationResult xhci_rh_add_hub(xhci_controller_state *hcState, const int port_start, const int port_count, const port_generation_type proto, volatile uint32_t *speed_ids, const uint8_t id_count)
{
    xhci_rh_state *rh_state, **rh_array;
    int mask_bytes_needed;

    mask_bytes_needed = port_count/8;
    if(port_count%8)
        ++mask_bytes_needed;

    rh_state = (xhci_rh_state *)calloc(sizeof(xhci_rh_state), 1);

    /* Set the hub parameters */
    rh_state->controller_port_offset = port_start-1;
    rh_state->port_count = port_count;
    rh_state->hub_generation = proto;
    rh_state->speeds = speed_ids;
    rh_state->speed_count = id_count;

    /* Allocate the hub and port state bitmask buffers */
    rh_state->hub_state_bits = (uint8_t *)calloc(1, mask_bytes_needed);
    rh_state->port_status = (status_reg_type *)calloc(sizeof(status_reg_type), port_count);
    rh_state->port_prior_state = (uint8_t *)calloc(1, port_count);

    /* Initialize the hub state */
    rh_state->dev_status = USB_DEV_STATUS_SELF_POWERED;
    rh_state->hub_operational_state = xhci_rh_addressed;

    rh_state->control_ep_lock = dapi_lock_create();
    rh_state->status_ep_lock = dapi_lock_create();
    rh_state->status_val_lock = dapi_lock_create();

    rh_state->rh_flags = hcState->caps->hccparams1 & XHCI_HCC1_LTC;

    switch(proto)
    {
        case PORT_PROTO_USB20:
            xhci_rh_usb2_init_desc(hcState, rh_state, port_count);
            break;
        case PORT_PROTO_USB30:
            rh_state->port_ext_status = (uint32_t *)calloc(4, port_count);
            xhci_rh_usb3_init_desc( hcState, rh_state, port_count );
            break;
        case PORT_PROTO_USB31:
            rh_state->port_ext_status = (uint32_t *)calloc(4, port_count);
            xhci_rh_usb3_init_desc( hcState, rh_state, port_count );
            break;
        case PORT_PROTO_USB32:
            rh_state->port_ext_status = (uint32_t *)calloc(4, port_count);
            xhci_rh_usb3_init_desc( hcState, rh_state, port_count );
            break;
        default:
            return driverOpBadArg;
            break;
    }

    rh_state->hc_link = hcState;
    rh_state->rh_usb_addr = hcState->maxRootHubAddress--;
    hcState->dev_to_slot_map[rh_state->rh_usb_addr] = XHCI_DEV_IS_RH | hcState->root_hub_count;

    rh_array = (xhci_rh_state **)calloc(sizeof(xhci_rh_state *), hcState->root_hub_count + 1);

    if(hcState->root_hub_info)
    {
        for(int prior_rh_index = 0; prior_rh_index < hcState->root_hub_count; prior_rh_index++)
            rh_array[prior_rh_index] = hcState->root_hub_info[prior_rh_index];

        free(hcState->root_hub_info);
    }

    rh_array[hcState->root_hub_count++] = rh_state;

    hcState->root_hub_info = (void **)rh_array;

    /* Status generations */
    rh_state->last_pushed_status_gen = 0;
    rh_state->status_generation = 0;

    rh_state->hub_active = 1;

    rh_state->status_ep_state = xhci_rh_ep_running;
    rh_state->control_ep_state = xhci_rh_ep_running;

    rh_state->control_pid = dapi_spawn_task(&xhci_rh_control_pipe_loop, rh_state);
    rh_state->status_pid = dapi_spawn_task(&xhci_rh_interrupt_pipe_loop, rh_state);

    return driverOpSuccess;
}

static void xhci_rh_probe_ports(xhci_controller_state *hcState, xhci_rh_state *rh_state, int *connection_count)
{
    int sc_index, status_buffer_offset;
    uint8_t mask;

    status_buffer_offset = 0;
    mask = 2;

    sc_index = rh_state->controller_port_offset;
    for(int port_index = 0; port_index < rh_state->port_count; port_index++, mask <<= 1)
    {
        uint32_t sc_value, psi_value;
        volatile portReg *xhci_port_reg;
        xhci_port_info *port_info;
        status_reg_type *status_reg;
        volatile uint32_t *ext_status;

        xhci_port_reg = &hcState->ops->portRegSet[port_index + sc_index];
        port_info = &hcState->port_info[sc_index + port_index];
        status_reg = &rh_state->port_status[port_index];
        ext_status = &rh_state->port_ext_status[port_index];
        sc_value = xhci_port_reg->portStatusControl;

        psi_value = XHCI_PORTSC_PORTSPEED(sc_value);

        if(!mask)
        {
            mask = 1;
            status_buffer_offset++;
        }

        if(sc_value & XHCI_PORTSC_CCS)
        {
            if(sc_value & XHCI_PORTSC_PED)
            {
                if(rh_state->hub_generation != PORT_PROTO_USB20)
                {
                    if(sc_value & XHCI_PORTSC_PED)
                        status_reg->status |= UHUB_PS_ENABLE;

                    xhci_set_usb3_speed_from_psi(port_info, rh_state, status_reg, ext_status, psi_value);
                }
            }

            status_reg->status |= UHUB_PS_CONNECTION;
            status_reg->change_status |= UHUB_PS_C_CONNECTION;
            (*connection_count)++;
            rh_state->hub_state_bits[status_buffer_offset] |= mask;

        }


        if(sc_value & XHCI_PORTSC_PP)
            status_reg->status |= (rh_state->hub_generation == PORT_PROTO_USB20) ? UHUB_PS_USB2_POWER:UHUB_PS_USB3_POWER;

        if(rh_state->hub_generation != PORT_PROTO_USB20)
        {
            uint32_t usb3_link_state = UHUB_PS_SET_LINK_STATE( XHCI_PORTSC_GET_PLS(sc_value) );
            status_reg->status &= ~UHUB_PS_LINK_STATE_MASK;
            status_reg->status |= usb3_link_state;
        }
        else
        {
            /* TODO: LPM link state stuff */
        }

        xhci_log(XHCI_LOG_INTERNAL, "RH - Hub %X:%u initial port info - Status: %X Change: %X\n", rh_state->bus_addr, port_index+1, status_reg->status, status_reg->change_status);
    }
}


/*! Registers all the emulated root hub(s) on a controller with the OS and hub driver
 *
 *  \param hcState Host controller state
 */
driverOperationResult xhci_rh_register_hubs(xhci_controller_state *hcState)
{
    for(int rh_index = 0; rh_index < hcState->root_hub_count; rh_index++)
    {
        xhci_rh_state *rh_state;
        uint32_t usb_res_size;
        uint64_t bus_addr;
        uint8_t hub_usb_speed;
        int connection_count;
        device_connect_response new_dev_response;
        device_connect_msg *new_device_message;

        connection_count = 0;
        rh_state = hcState->root_hub_info[rh_index];
        new_device_message = (device_connect_msg *)calloc(1, sizeof(device_connect_msg));

        bus_addr = hcState->controller_id*USB_MAX_ADDRESS;
        bus_addr += USB_MAX_ENDPOINTS*rh_state->rh_usb_addr;
        rh_state->bus_addr = bus_addr;


        switch(rh_state->hub_generation)
        {
            case PORT_PROTO_USB20:
                hub_usb_speed = USB_SPEED_HIGH;
                break;
            case PORT_PROTO_USB30:
                hub_usb_speed = USB_SPEED_SUPER;
                break;
            case PORT_PROTO_USB31:
                hub_usb_speed = USB_SPEED_ESUPER_2X1;
                break;
            case PORT_PROTO_USB32:
                hub_usb_speed= USB_SPEED_ESUPER_2X2;
                break;
        }
        xhci_rh_probe_ports(hcState, rh_state, &connection_count);
        if(connection_count)
            rh_state->status_generation++;


        /* Setup the new USB device message */
        new_device_message->max_bus_power = 500;
        new_device_message->parent_address = 0;
        new_device_message->parent_handle = hcState->controller_handle;
        new_device_message->controller_id = hcState->controller_id;
        new_device_message->usb_address = rh_state->rh_usb_addr;
        new_device_message->port_num = 0;
        new_device_message->dev_speed = hub_usb_speed;
        new_device_message->flags = USB_NEW_CONNECT_ADDRESSED;

        usb_res_size = sizeof(device_connect_response);

        /* Register with the USB coordinator */
        if( busMsg(&new_dev_response, &usb_res_size, BUS_ID_USB, bus_addr, new_device_message, sizeof(device_connect_msg), usbCoordDeviceConnect, hcState->dev) != busMsgAcknowledged)
        {
            xhci_log(XHCI_LOG_ERRORS, "Error USB bus coordinator failed to register hub @ %X!\n", hcState->busAddr.address);
            return driverOpResources;
        }

    }

    return driverOpSuccess;
}

busOperationResult xhci_rh_queue_transaction(xhci_controller_state *hcState, const uint8_t rh_index, const uint8_t endpoint,
    const uint8_t direction, usbControllerTransaction *transaction, const uint32_t t_info_length, sync_object *sync) {

    xhci_rh_state *rh_state;

    rh_state = (xhci_rh_state *)hcState->root_hub_info[rh_index];
    switch(transaction->type)
    {
        case usbTransactionControl:
            return xhci_rh_queue_control(hcState, rh_state, endpoint, transaction, sync);
            break;
        case usbTransactionInterrupt:
            return xhci_rh_queue_status(hcState, rh_state, endpoint, transaction, sync);
            break;
        default:
            return busOpInvalidArg;
            break;
    }


    return busOpSuccess;
}

/*! Runs transactions for a root hub endpoint
 *
 *  \param hcState Host controller context
 *  \param rh_index Index of the root hub within the host controller context's array of root hubs
 *  \param endpoint Endpoint where the transaction is being requested
 *  \param sync Synchronization object
 *
 */
busOperationResult xhci_rh_run_transactions(xhci_controller_state *hcState, const uint8_t rh_index, const uint8_t endpoint, const uint8_t dir, sync_object *sync)
{
    xhci_rh_state *rh_state;

    rh_state = (xhci_rh_state *)hcState->root_hub_info[rh_index];

    switch(endpoint)
    {
        case 0:
            if(rh_state->control_ep_state != xhci_rh_ep_halted)
                rh_state->control_ep_state = xhci_rh_ep_running;
            break;
        case XHCI_RH_STATUS_EP:
            if(rh_state->hub_operational_state != xhci_rh_configured)
                return busOpIOError;
            if(rh_state->status_ep_state != xhci_rh_ep_halted)
                rh_state->status_ep_state = xhci_rh_ep_running;
            break;
        default:
            return busOpInvalidArg;
            break;
    }
    return busOpSuccess;
}
