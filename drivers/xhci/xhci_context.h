#ifndef XHCI_CONTEXT_H_
#define XHCI_CONTEXT_H_

#include <compiler.h>

#define XHCI_ENDPOINT_TO_DEVCON(x)           ((x) ? (x)*2:1)
#define XHCI_ENDPOINT_TO_ICC(x)              (XHCI_ENDPOINT_TO_DEVCON(x) + 1)

#define DC_MAX_SIZE                          0x400U
#define DEVICE_CONTEXT_PER_PAGE              (ARCH_PAGE_SIZE/DC_MAX_SIZE)

#define NO_SLOT_AVAILABLE                    -1

#define XHCI_SLOT_CONTEXT_GET_SPEED(x)       ((x)>>4)

#define XHCI_SLOT_CONTEXT_ENTRIES(x)         ((x)>>3)
#define XHCI_SLOT_CONTEXT_STATE(x)           ((x)>>3)

#define XHCI_SLOT_CONTEXT_MTT                0x2
#define XHCI_SLOT_CONTEXT_HUB                0x4

#define ICC_FLAG_ENDPOINT(x)                 (1U<<x)

#define SET_SLOT_CONTEXT_ENTRIES(x, y)       ( ((x)<<3) | (y))
#define SET_SLOT_CONTEXT_INTERRUPTER(x, y)   (((x) << 6) | y)

#define EP_TYPE_CONTROL                      (4U<<4)

#define XHCI_EP_DCS                          0x1UL

/* XHCI Specification Section 6.2.2 */
typedef struct COMPILER_PACKED
{
	//Table 6-4
	volatile uint8_t routeString[3], contextInfo;
	//Table 6-5
	volatile uint16_t maxExitLatency;
	volatile uint8_t rootPortNum, numberOfPorts;
	//Table 6-6
	volatile uint8_t parentSlotId, parentPortNumber;
	volatile uint16_t interrupterAndTTT;
	//Table 6-7
	volatile uint8_t usbDeviceAddress, _reserved[2], slotState;
} slotContext;

/* Endpoint context endpoint types */
#define XHCI_EPC_EP_ISO_OUT          1
#define XHCI_EPC_EP_BULK_OUT         2
#define XHCI_EPC_EP_INTERRUPT_OUT    3
#define XHCI_EPC_EP_CONTROL          4
#define XHCI_EPC_EP_ISO_IN           5
#define XHCI_EPC_EP_BULK_IN          6
#define XHCI_EPC_EP_INTERRUPT_IN     7

#define XHCI_EPC_EP_TYPE(x)          ((x) << 3)
#define XHCI_EPC_CERR(x)             ((x) << 1)

#define XHCI_EPC_GET_TYPE(x)         (((x) >> 3) & 0x07U)
#define XHCI_EPC_GET_CERR(x)         (((x)>>1) & 0x03U)
#define XHCI_EPC_GET_EP_STATE(x)     ((x) & 0x07U)

/* Slot context states */
#define XHCI_SC_STATE_DE             0
#define XHCI_SC_STATE_DEFAULT        1
#define XHCI_SC_STATE_ADDRESSED      2
#define XHCI_SC_STATE_CONFIGURED     3


/* Endpoint context endpoint states */
#define XHCI_EPC_EP_STATE_DISABLED   0
#define XHCI_EPC_EP_STATE_RUNNING    1
#define XHCI_EPC_EP_STATE_HALTED     2
#define XHCI_EPC_EP_STATE_STOPPED    3
#define XHCI_EPC_EP_STATE_ERROR      4


/* XHCI Specification Section 6.2.3 for more */
typedef struct COMPILER_PACKED
{
	/* Table 6-8 */
	volatile uint8_t epState, streamInfo, interval, maxEsitPayloadHi;
	/* Table 6-9 */
	volatile uint8_t epTypeField, maxBurstSize;
	volatile uint16_t maxPacketSize;
	/* Table 6-10, also contains DCS flag */
	volatile uint64_t trDequeuePtr;
	/* Table 6-11 */
	volatile uint16_t averageTRBLength, maxESITPayloadLo;
    volatile uint64_t __reserved[3];
} endpointContext;

/* XHCI Specification Section 6.2.5.1 */
typedef struct COMPILER_PACKED
{
	volatile uint32_t dropContext, addContext, _reserved[5];
	volatile uint8_t configValue, interfaceNum, altSetting, _reserved2;
} inputControlContext;

typedef union
{
	volatile slotContext sc;
	volatile endpointContext ec;
	volatile inputControlContext icc;
} devContext;


#endif
