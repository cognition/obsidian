#ifndef XHCI_BUS_H_
#define XHCI_BUS_H_

#include <driverapi.h>
#include "xhci_struct.h"

busRegisterResponse xhci_register_controller(xhci_controller_state *hcState, const uint8_t usb_major, const uint8_t usb_minor);

uint8_t xhci_get_slot_from_address(xhci_controller_state *hcState, const uint8_t device_address);
#endif // XHCI_BUS_H_
