#ifndef SYNC_QUEUE_H_
#define SYNC_QUEUE_H_

#include "xhci_struct.h"

volatile commandTRB *sync_queue_peek(sync_queue *sq);
sync_object *sync_queue_pop(sync_queue *sq);
void sync_queue_push(sync_queue *sq, volatile commandTRB *target, sync_object *sync_obj);

#endif // SYNC_QUEUE_H_
