#ifndef XHCI_CONTROLLER_H_
#define XHCI_CONTROLLER_H_

#include "xhci_struct.h"

driverOperationResult allocate_mapped_page(mappedRegion *trbRegion, xhci_controller_state *hcState);
short int xhci_event_tree_insert(physInstance **root, physInstance *node);
int xhci_event_tree_find(physInstance **instance, physInstance *root, const uint64_t address);
void xhci_status_clear(xhci_controller_state *hcState, uint32_t clearBits);

#endif
