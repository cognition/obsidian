/* XHCI Driver assert test prototypes and configuration
 *
 * Author: Joseph Kinzel */

#ifndef XHCI_ASSERT_H_
#define XHCI_ASSERT_H_

/* Component level flags */
#define XHCI_VALIDATE_PORT              0x0020U

/* Master bitmask for all active component flags */
#define XHCI_ASSERT_TRIGGERS            (XHCI_VALIDATE_PORT)


#define XHCI_STRINGIFY_DEFINE(x)             #x
/*! Assert macro
 *  \param flag The corresponding component flag which activates the assertion
 *  \param test A test condition which is expected to evaluate to true
 *  \param msg An error message to be displayed if the test condition is false
 */
#define xhci_assert(flag, test, msg)          if(XHCI_ASSERT_TRIGGERS & flag) {\
                                                 if(!(test))                   \
                                                    xhci_log(XHCI_LOG_ERRORS, msg " @ " __FILE__ ":" XHCI_STRINGIFY_DEFINE(__LINE__) "\n");\
                                              }


#endif // XHCI_ASSERT_H_
