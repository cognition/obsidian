Requirements:
 - Overall USB bus/topology driver (USBD layer per USB 2.0+ specification)
 - Communicates with controllers
 - Sets and manages the default address for freshly reset devices awaiting addressing
 - Deals with device connect/disconnect
 - Manages device status
 - Manages device identification and driver bindings
 - Manages pipes, commands and descriptor requests from clients
 - Deals with hub topology and versioning, abstracts details away from clients
Milestones:
 - Basic driver loading, likely to be dynamically triggered by the initial request on the driver API's bus interface
 - USB controller and root hub support
 - Device identifcation support
 - Hub support
 - Client level pipe, command and descriptor support via bus message interface in the driver API
