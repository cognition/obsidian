/* New port/device connection header
 *
 * Author: Joseph Kinzel */

#ifndef USB_NEW_CONNECTION_H_
#define USB_NEW_CONNECTION_H_

#include <driverapi.h>
#include "topo.h"

#define USB_ID_METHOD_VENDOR                                                0x1UL
#define USB_ID_METHOD_CLASS                                                 0x2UL
#define USB_ID_METHOD_DEVICE                                                0x3UL

#define USB_ID_PACK(method, vendClass, devSubclass, revProto)               ( ((uint64_t)(method)<<48) | ((uint64_t)(vendClass)<<32) | ((uint64_t)(devSubclass)<<16) | (revProto & 0xFFFFU) )

busMsgResponse usb_decode_endpoint_desc(void *response, uint32_t *resSize, void *msg, const uint32_t  msgSize, const int64_t unique_key);
busMsgResponse usb_process_device_connect(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
driverOperationResult usb_split_config_desc(const uint64_t full_address, volatile uint8_t *desc_buffer, const uint32_t buffer_length, const int interface_count, const usbHubInfo *hub_info, usb_handle_info *h_info, sync_object *sync);
int usb_parse_endpoint(volatile usbEndpointDescriptor *endpoint_desc, usbConfigureEndpointFields *ep_config_entry, const uint8_t dev_speed, const int bytes_rem, int *ep_skip, uint32_t *ep_mask);
void usb_device_connect_task(void *msg);
void usb_port_connect_task(void *pc_msg);

#endif // USB_NEW_CONNECTION_H_
