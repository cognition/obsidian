/* USB Coordinator driver descriptor handling functions header file
 *
 * Author: Joseph Kinzel */

#ifndef USB_DESCRIPTORS_H_
#define USB_DESCRIPTORS_H_

#include <driverapi.h>

busOperationResult usb_coord_get_desc(const uint64_t full_addr, const uint8_t desc_type, const uint8_t desc_index, const uint16_t language_id, const uint8_t req_type,
                       const uint16_t expected_size, sync_object *sync, volatile void **data);
busMsgResponse usb_get_status(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType);
busMsgResponse usb_get_desc_request(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize);
busMsgResponse usb_req_alt_iface(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t unique_key);
busOperationResult usb_coord_get_status(const uint64_t full_addr, const uint8_t status_type, const uint16_t type_index, volatile uint16_t *status_value, sync_object *sync);
busMsgResponse usb_request_config(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t unique_key);
busOperationResult usb_coord_set_config(const uint64_t full_addr, const uint8_t config_value, sync_object *sync_obj);
busOperationResult usb_get_best_config(const usb_device_info *dev_info, const uint16_t dev_status, uint8_t *config_num, uint8_t *config_desc_index, sync_object *sync, volatile usbConfigDescriptor **ret_desc);
#endif // USB_DESCRIPTORS_H_
