/* USB New device connection handling code
 *
 * Identifies and attempts to load drivers for all non-controller USB devices
 *
 *  Author: Joseph Kinzel
 */

#include "new_connect.h"
#include "transaction.h"
#include "descriptors.h"
#include "topo.h"
#include <usb.h>
#include <dapi_container.h>
#include <sync.h>

#define USB_HUB_GET_TT(x)             (((x)>>5) & 0x03U)

extern void *usb_controller_queue_map;

typedef struct COMPILER_PACKED
{
    uint8_t bLength, bDescriptorType;
    uint8_t bNbrPorts;
    uint16_t wHubCharacteristics;
    uint8_t bPwrOn2PwrGood;
    uint8_t bHubContrCurrent;
} usb_hub_descriptor;


typedef struct
{
    uint32_t ep_mask, desc_length;

    union
    {
        volatile usbInterfaceDescriptor *desc;
        volatile uint8_t *bytes;
    };
} usb_interface_entry;


extern DEVICE_HANDLE coord_dev;

COMPILER_UNUSED static busOperationResult usb_get_string_desc(const uint64_t full_address, int desc_index, int lang_id, sync_object *sync_obj, const char **str_ret )
{
    busOperationResult status;
    usbControllerTransaction ct;
    char base_buffer[8], *full_buffer, *final_buffer;
    memset(&ct, 0, sizeof(usbControllerTransaction));

    ct.type = usbTransactionControl;
    ct.control.bm_request_type = USB_REQ_TYPE_DEVICE | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_STANDARD;
    ct.control.b_request = USB_CONTROL_REQ_GET_DESCRIPTOR;
    ct.control.w_index = lang_id;
    ct.control.w_value = USB_DESCRIPTOR_STRING | desc_index;
    ct.control.w_length = 8;
    ct.control.data_ptr = base_buffer;

    status = usb_single_transaction(full_address, &ct, sync_obj);
    if(status != busOpSuccess)
    {
        return status;
    }

    sync_object_wait(sync_obj, 500);

    full_buffer = (char *)malloc( base_buffer[USB_DESC_FIELD_LENGTH_OFF]);
    ct.control.w_length = base_buffer[USB_DESC_FIELD_LENGTH_OFF];
    ct.control.data_ptr = full_buffer;

    status = usb_single_transaction(full_address, &ct, sync_obj);
    if(status != busOpSuccess)
    {
        return status;
    }

    sync_object_wait(sync_obj, 500);
    final_buffer = (char *)calloc(1, (base_buffer[USB_DESC_FIELD_LENGTH_OFF] - 2)/2 + 1);

    for(int str_index = 0; str_index < (base_buffer[USB_DESC_FIELD_LENGTH_OFF] - 2); str_index += 2)
    {
        final_buffer[str_index/2] = base_buffer[2 + str_index];
    }

    free(full_buffer);

    printf("[USB ]: Got descriptor for device %X for string %u with length %u of \"%s\"\n", full_address, desc_index, base_buffer[USB_DESC_FIELD_LENGTH_OFF], final_buffer);

    *str_ret = final_buffer;

    return status;
}

/*! Gets the full configuration data for a device
 *  \param full_address Address the device
 *  \param config_info Configuration descriptor index
 *  \param sync_info Synchronization information pointer
 *  \param config_buffer Configuration descriptor return buffer
 */
busOperationResult usb_get_configuration_desc(const uint64_t full_address, const uint8_t config_index, sync_object *sync_info, volatile uint8_t **config_buffer)
{
    usbControllerTransaction ct;
    busOperationResult result;
    volatile usbConfigDescriptor config_desc;
    volatile uint8_t *buffer;

    memset(&ct, 0, sizeof(usbControllerTransaction));

    //printf("[USB ]: Retrieving configuration descriptor for device @ %X\n", full_address);
    /* Get configuration descriptor transaction setup */
    ct.type = usbTransactionControl;
    ct.control.bm_request_type = USB_REQ_TYPE_DEVICE | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_STANDARD;
    ct.control.b_request = USB_CONTROL_REQ_GET_DESCRIPTOR;
    ct.control.w_index = config_index;
    ct.control.w_value = USB_DESCRIPTOR_CONFIG;
    ct.control.w_length = sizeof(usbConfigDescriptor);
    ct.control.data_ptr = &config_desc;
    ct.flags = USB_TRANSACTION_IN;

    /* First transaction gets the total configuration descriptor size */
    result = usb_single_transaction(full_address | USB_TRANSACTION_IN, &ct, sync_info);
    if(result != busOpSuccess)
        return result;

    buffer = (volatile uint8_t *)malloc(config_desc.wTotalLength);

    ct.control.w_length = config_desc.wTotalLength;
    ct.control.data_ptr = buffer;

    /* Second gets the complete descriptor */
    result = usb_single_transaction(full_address | USB_TRANSACTION_IN, &ct, sync_info);
    if(result != busOpSuccess)
    {
        free((void *)buffer);
        return result;
    }

    *config_buffer = buffer;

    return busOpSuccess;
}

/*! Parses an interface descriptor and attempts to load a driver for it
 *  \param interface_desc Pointer to the interface descriptor buffer
 *  \param full_address Full USB address of the device
 *  \param ep_mask Mask of endpoints accessible by this interface
 *  \param dev_speed Speed of the device
 *  \param parent_handle Handle of the device's parent
 *  \return driverOpSuccess if the interface loads a new driver for the device or no driver was found for the device, or an appropriate error code if a problem occurred during the driver loading
 */
static int usb_parse_interface(usb_handle_info *hi, BUS_HANDLE parent_handle)
{
    int status;
    uint64_t id_key, full_address;
    usb_device_entry_context *entry_context;
    busAddressDesc interface_addr_desc;
    volatile usbInterfaceDescriptor *interface_desc;
    BUS_HANDLE bh;
    usb_interface_info *iface;

    iface = &hi->iface;
    full_address = iface->parent->device.full_address;

    interface_desc = (volatile usbInterfaceDescriptor *)iface->desc_buffer;
    //printf("[USB ]: Parsing interface %u on device @ %X with endpoints: %X\n", interface_desc->bInterfaceNumber, full_address, iface->endpoints);
    /* Build the query ID and attempt to a load a driver based on the interface's class */
    id_key = USB_ID_PACK(USB_ID_METHOD_CLASS, interface_desc->bInterfaceClass, interface_desc->bInterfaceSubClass, interface_desc->bInterfaceProtocol);
    interface_addr_desc.busId = BUS_ID_USB;
    interface_addr_desc.address = full_address;

    bh = coordAllocBusHandle(BUS_ID_USB, coord_dev);

    usb_register_bus_device(hi, bh);

    /* Create the entry context and attempt to find a driver to claim the interface */
    entry_context = (usb_device_entry_context *)calloc(1, sizeof(usb_device_entry_context) + iface->desc_size);
    memcpy(entry_context->desc_buffer, (void *)iface->desc_buffer, iface->desc_size);

    entry_context->dev_speed = iface->parent->device.dev_speed;
    entry_context->length = iface->desc_size;
    entry_context->topo_string = usb_construct_topo_string(hi);

    status = registerNewDevice(&interface_addr_desc, coord_dev, parent_handle, id_key, bh, entry_context );
    if( DRIVER_OP_ERROR(status) )
    {
        coordFreeBusHandle(BUS_ID_USB, bh, coord_dev);
        printf("[USB ]: Failed to register interface @ %X interface: %u code: %u\n", full_address, interface_desc->bInterfaceNumber, status);
        return status;
    }

    return driverOpSuccess;
}

/*! Parses an endpoint descriptor and (potentially) subsequent superspeed descriptors to gather endpoint configuration information
 *  \param endpoint_desc A pointer to the endpoint descriptor buffer
 *  \param ep_config_entry Endpoint configuration entry that will receive output
 *  \param bytes_rem A value indicating the total  number of bytes remaining in the configuration buffer from the start of the endpoint descriptor pointer
 *  \param ep_skip A pointer to the 'endpoint skip' value used to skip the configuration of endpoints in alternate interfaces that are not being initialized
 *  \return The number of bytes parsed for an endpoint entry (might be more than the initial descriptor) or zero if an error was encountered
 */
int usb_parse_endpoint(volatile usbEndpointDescriptor *endpoint_desc, usbConfigureEndpointFields *ep_config_entry, const uint8_t dev_speed, const int bytes_rem, int *ep_skip, uint32_t *ep_mask)
{
    int ep_num, mps, ep_type, interval, at_ops, direction, tail_size, ess_attr, ess_max_burst, ess_iso_mult, ess_wbpi, ess_iso_dwbpi;

    volatile uint8_t *desc_ptr;
    uint32_t mask;
    int skip = *ep_skip;

    tail_size = endpoint_desc->bLength;
    desc_ptr = (volatile uint8_t *)endpoint_desc;

    ep_num = USB_EP_GET_NUM(endpoint_desc->bEndpointAddress);
    ep_type = USB_EP_GET_TYPE(endpoint_desc->bmAttributes);
    interval = endpoint_desc->bInterval;
    direction = USB_EP_GET_DIR(endpoint_desc->bEndpointAddress) ? 1:0;

    ess_attr = 0;
    ess_max_burst = 0;
    ess_iso_mult = 0;
    ess_wbpi = 0;
    ess_iso_dwbpi = 0;

    if(ep_num >= USB_MAX_ENDPOINTS)
    {
        printf("[USB ]: Found invalid endpoint descriptor for device @ %X EP: %u\n", ep_num);
        return 0;
    }

    mask = 1U;
    mask <<= ep_num;

    if(dev_speed >= USB_SPEED_SUPER)
    {
        volatile usbSSECDescriptor *ssec_desc;
        ssec_desc = (volatile usbSSECDescriptor *)&desc_ptr[tail_size];
        tail_size += sizeof(usbSSECDescriptor);

        mps = endpoint_desc->wMaxPacketSize;
        at_ops = 0;

        /* Super speed endpoints have a companion descriptor that contains additional information */
        if( (ssec_desc->bDescriptorType == usbDescSSEC) && (tail_size <= bytes_rem))
        {
            tail_size += ssec_desc->bLength - sizeof(usbSSECDescriptor);
            ess_attr = ssec_desc->bmAttributes;
            ess_max_burst = ssec_desc->bMaxBurst;
            ess_wbpi = ssec_desc->wBytesPerInterval;
            /* Isochronous ESS endpoints have yet another descriptor describing more information about the endpoint */
            if( (ep_type == USB_EP_TYPE_ISOCHRON) && (ssec_desc->bmAttributes & USB_SS_EPC_ISOC))
            {
                volatile usbSSIsoECDescriptor *ss_iso_ec_desc = (volatile usbSSIsoECDescriptor *)&desc_ptr[tail_size];
                tail_size += sizeof(usbSSIsoECDescriptor);
                if( (bytes_rem >= tail_size) && (ss_iso_ec_desc->bDescriptorType == usbDescSSISOEC) )
                {
                    tail_size += ss_iso_ec_desc->bLength - sizeof(usbSSIsoECDescriptor);
                    ess_iso_mult = ss_iso_ec_desc->dwBytesPerInterval/ssec_desc->bMaxBurst/endpoint_desc->wMaxPacketSize;
                    ess_iso_dwbpi = ss_iso_ec_desc->dwBytesPerInterval;
                }
                else
                {
                    printf("[USB ]: Endpoint %u did not have a Superspeed ISO endpoint companion descriptor!\n", ep_num);
                    return 0;
                }
            }
        }
        else
        {
            printf("[USB ]: Endpoint %us did not have a Superspeed endpoint companion descriptor!\n", ep_num);

            return 0;
        }

    }
    else
    {
        mps = USB2_EP_GET_PSIZE(endpoint_desc->wMaxPacketSize);
        at_ops = USB_EP_GET_AT(endpoint_desc->wMaxPacketSize);
    }

    /* Endpoints under alternative interfaces are skipped, but default interfaces should record the endpoint information for controller-specific configuration */
    if(!skip)
    {
        ep_config_entry->ep_num = ep_num;
        ep_config_entry->ep_type = ep_type;
        ep_config_entry->direction = ((ep_type == USB_EP_TYPE_CONTROL) ? 1:direction);
        ep_config_entry->interval = interval;
        ep_config_entry->packet_size = mps;
        ep_config_entry->at_ops = at_ops;

        ep_config_entry->ess_attr = ess_attr;
        ep_config_entry->ess_max_burst = ess_max_burst;
        ep_config_entry->ess_iso_mult = ess_iso_mult;

        ep_config_entry->ess_wbpi = ess_wbpi;
        ep_config_entry->ess_iso_dwbpi = ess_iso_dwbpi;

        *ep_mask |= mask;
    }
    else
    {
        skip--;
        *ep_skip = skip;
    }

    return tail_size;
}

/*! Processes the configuration descriptor of a device and splits its entries into individual interfaces to be activated or enumerated
 *  and issues the message for controller side configuration of the device and endpoints.
 *
 * \param full_address Full USB address of the device
 * \param desc_buffer Buffer containing the full configuration descriptor
 * \param buffer_length Length of the descriptor buffer
 * \param interface_count Number of interfaces present in the configuration descriptor
 * \param hub_info Pointer to the hub information structure
 * \param h_info Device information structure
 * \param sync Wait/Signal style synchronization object to be utilized by this function
 */
driverOperationResult usb_split_config_desc(const uint64_t full_address, volatile uint8_t *desc_buffer, const uint32_t buffer_length, const int interface_count,
                                                   const usbHubInfo *hub_info, usb_handle_info *h_info, sync_object *sync)
{
    int desc_length, ep_skip, iface_index, iface_start, ep_entry_delta;
    uint32_t *ep_mask_ptr;
    volatile usbConfigDescriptor *config_desc;
    volatile usbInterfaceDescriptor *iface_desc;
    usb_handle_info *current_info_ptr;
    usb_device_info *device_info;
    usbConfigureAddress ca;

    config_desc = (volatile usbConfigDescriptor *)desc_buffer;

    memset(&ca, 0, sizeof(usbConfigureAddress));

    /* Initialize the basics of the configure address message */
    ca.command_type = usbConfigureDevice;
    ca.device_info.dev_speed = h_info->device.dev_speed;
    ca.device_info.hub_info = *hub_info;
    ca.device_info.value = config_desc->bConfigurationValue;
    ca.device_info.num_ep_entries = 0;

    desc_length = 0;
    ep_skip = 0;
    iface_index = 0;
    iface_start = desc_buffer[USB_DESC_FIELD_LENGTH_OFF];
    ep_mask_ptr = NULL;

    /* Fill out some additional device information structure entries */
    device_info = &h_info->device;
    device_info->child_count = interface_count;
    device_info->endpoints = 0x00000001U;
    device_info->desc_buffer = desc_buffer;
    device_info->desc_size = buffer_length;
    device_info->full_address = full_address;
    device_info->children = (usb_handle_info *)calloc(interface_count, sizeof(usb_handle_info) );

    current_info_ptr = NULL;

    for(int buffer_offset = desc_buffer[USB_DESC_FIELD_LENGTH_OFF]; buffer_offset < buffer_length; buffer_offset += desc_length)
    {
        switch(desc_buffer[buffer_offset + USB_DESC_FIELD_TYPE_OFF])
        {
            case usbDescInterface:
                /* Record each interface entry for later enumeration */
                iface_desc = (volatile usbInterfaceDescriptor *)&desc_buffer[buffer_offset];
                if(!iface_desc->bAlternateSettings)
                {
                    if(iface_index >= interface_count)
                        return driverOpBadState;
                    if(current_info_ptr)
                        current_info_ptr->iface.desc_size = buffer_offset - iface_start;

                    iface_start = buffer_offset;
                    current_info_ptr = &h_info->device.children[iface_index];
                    iface_index++;

                    current_info_ptr->type = usb_interface_info_type;
                    current_info_ptr->iface.desc_buffer = &desc_buffer[buffer_offset];
                    ep_mask_ptr = &current_info_ptr->iface.endpoints;
                    *ep_mask_ptr = 0x01U;
                    current_info_ptr->iface.interface_id = iface_desc->bInterfaceNumber;
                    current_info_ptr->iface.parent = h_info;
                }
                else
                    ep_skip = iface_desc->bNumEndpoints;
                desc_length = iface_desc->bLength;
                break;
            case usbDescEndpoint:
                /* Record the endpoint fields used by each default interface so they can be passed on to the actual host controller for initialization later */
                if(!ep_mask_ptr)
                {
                    printf("[USB ]: Error endpoint descriptor encountered before interface descriptor for device @ %X!\n", full_address);
                    return driverOpBadState;
                }
                ep_entry_delta = (ep_skip ? 0:1);
                desc_length = usb_parse_endpoint( (volatile usbEndpointDescriptor *)&desc_buffer[buffer_offset], &ca.device_info.endpoints[ca.device_info.num_ep_entries], h_info->device.dev_speed, buffer_length - buffer_offset, &ep_skip, ep_mask_ptr );
                device_info->endpoints |= *ep_mask_ptr;
                //printf("[USB ]: Split config - parsed endpoint descriptor with total length: %u offset: %u\n", desc_length, buffer_offset);
                ca.device_info.num_ep_entries += ep_entry_delta;
                if(!desc_length)
                {
                    printf("[USB ]: Failed to parse endpoint entry for device @ %X\n", full_address);
                    return driverOpBadState;
                }
                break;
            default:
                desc_length = desc_buffer[buffer_offset + USB_DESC_FIELD_LENGTH_OFF];
                break;
        }
    }

    if(current_info_ptr)
        current_info_ptr->iface.desc_size = buffer_length - iface_start;

    if(busConfigAddress(BUS_ID_USB, full_address, &ca, sizeof(usbConfigureAddress), sync, coord_dev) != busOpSuccess)
    {
        printf("[USB ]: Failed to perform controller side configuration for USB device @ %X\n", full_address);
        return driverOpBusError;
    }

    sync_object_wait(sync, 1000);
    if(!sync_object_get_status(sync))
       return driverOpBusError;

    return driverOpSuccess;
}

/*! Main configuration task entry point
 *  \param msg Device connection message pointer
 */
void usb_device_connect_task(void *msg)
{
    BUS_HANDLE bh;
    uint64_t full_address;
    volatile uint8_t *desc_buffer;
    //uint16_t packet_size;
    usbControllerTransaction ct;
    volatile usbDeviceDescriptor dev_desc;
    volatile usbConfigDescriptor *config_desc;
    device_connect_msg *dc_msg;
    sync_object *sync;

    usb_handle_info *h_info, *dev_parent;

    usbConfigureAddress config_msg;
    usbHubInfo *hub_info;
    uint8_t dev_speed;
    busOperationResult status;

    memset(&config_msg, 0, sizeof(usbConfigureAddress));
    memset(&ct, 0, sizeof(usbControllerTransaction));

    dc_msg = (device_connect_msg *)msg;
    dev_speed = dc_msg->dev_speed;

    full_address = dc_msg->controller_id*USB_MAX_ADDRESS + USB_MAX_ENDPOINTS*dc_msg->usb_address;
    sync_create_signal_obj(&sync, 0, 0);

    //printf("[USB ]: Attempting to retrieve device descriptor for device @ %X\n", full_address);

    /* Retrieve the device descriptor */
    ct.type = usbTransactionControl;
    ct.control.bm_request_type = USB_REQ_TYPE_DEVICE | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_STANDARD;
    ct.control.b_request = USB_CONTROL_REQ_GET_DESCRIPTOR;
    ct.control.w_index = 0;
    ct.control.w_length = sizeof(usbDeviceDescriptor);
    ct.control.w_value = USB_DESCRIPTOR_DEVICE;
    ct.control.data_ptr = &dev_desc;

    status = usb_single_transaction(full_address | USB_TRANSACTION_IN, &ct, sync);
    if(status != busOpSuccess)
    {
        printf("[USB ]: Failed to retrieve device descriptor for device! Returned status: %u\n", status);
        return;
    }


    printf("[USB ]: New device connected on controller %u address: %u Port: %u with speed: %u PHandle: %X Version: %X.%X ID: %04X:%04X\n", dc_msg->controller_id, dc_msg->usb_address, dc_msg->port_num,
       dc_msg->dev_speed, dc_msg->parent_handle, dev_desc.bcdVerMajor, dev_desc.bcdVerMinor, dev_desc.idVendor, dev_desc.idProduct );

    /*
    printf("[USB ]: Device on controller %u address: %u Port: %u Max packet size: %u\n", dc_msg->controller_id, dc_msg->usb_address, dc_msg->port_num, dev_desc.bMaxPacketSize );
    printf("\tDescriptor length: %u Descriptor Type: %u USB Version %x.%x\n", dev_desc.bLength, dev_desc.bDescriptorType, dev_desc.bcdVerMajor, dev_desc.bcdVerMinor);
    printf("\tDevice class: %x Device sub-class: %x Device protocol: %x Max packet size: %u\n", dev_desc.bDeviceClass, dev_desc.bDeviceSubClass, dev_desc.bDeviceProtocol, dev_desc.bMaxPacketSize);
    printf("\tDevice vendor: %X Device Product: %X Number of configurations: %u\n", dev_desc.idVendor, dev_desc.idProduct, dev_desc.bNumConfigurations);
    */

    hub_info = (usbHubInfo *)calloc(1, sizeof(usbHubInfo));

    h_info = calloc(1, sizeof(usb_handle_info));
    h_info->device.config_count = dev_desc.bNumConfigurations;
    h_info->device.max_bus_power = dc_msg->max_bus_power;
    h_info->device.dev_speed = dev_speed;
    h_info->device.hub_info = hub_info;
    h_info->device.endpoints = 0x01U;
    h_info->type = usb_device_info_type;
    h_info->device.device_lock = dapi_lock_create();
    h_info->device.full_address = full_address;

    dev_parent = usb_get_mutable_hi(dc_msg->parent_handle);
    h_info->device.parent = dev_parent;

    if(dev_desc.bDeviceClass == USB_CLASS_VENDOR)
    {
        busAddressDesc desc;
        int status;
        /*! TODO: Vendor specific device and protocols, try to bounce it to a device specific driver */
        uint64_t key = USB_ID_PACK( USB_ID_METHOD_VENDOR, dev_desc.bDeviceClass, dev_desc.bDeviceSubClass, dev_desc.bDeviceProtocol );

        desc.address = full_address;
        desc.busId = BUS_ID_USB;
        bh = coordAllocBusHandle(BUS_ID_USB, coord_dev);
        printf("[USB ]: Attempting to register vendor specific device @ %X\n", full_address);
        status = registerNewDevice(&desc, coord_dev, dc_msg->parent_handle, key, bh, (void *)&dev_desc );
        if( DRIVER_OP_ERROR(status) )
        {
            coordFreeBusHandle(BUS_ID_USB, bh, coord_dev);
            printf("[USB ]: Failed to register device @ %X code: %u\n", full_address, status);
        }
        return;
    }
    else if( dev_desc.bDeviceClass == USB_CLASS_HUB )
    {
        uint64_t id_key;
        volatile usb_hub_descriptor hub_desc;
        usb_device_entry_context *entry_context;
        busAddressDesc device_addr_desc;

        /* Hubs are a bit special because of the host controller side setup needed to configure them correctly in some cases */
        hub_info->hub_flags |= USB_DEV_FLAG_HUB;
        if( dev_desc.bDeviceProtocol == 2)
            hub_info->hub_flags |= USB_DEV_FLAG_MTT;

        /* Get the hub descriptor to get some hub specific information */
        ct.control.b_request = USB_CONTROL_REQ_GET_DESCRIPTOR;
        ct.control.bm_request_type = 0xA0U;
        ct.control.w_value =  ((dev_desc.bcdVerMajor == 0x003) ? 0x2A00:0x2900U);
        ct.control.w_index = 0;
        ct.control.w_length = sizeof(usb_hub_descriptor);
        ct.control.data_ptr = &hub_desc;

        if(usb_single_transaction(full_address | USB_TRANSACTION_IN, &ct, sync) != busOpSuccess)
        {
            printf("[USB ]: Failed to retrieve hub descriptor for device!\n");
            free(hub_info);
            free(h_info);
            return;
        }

        hub_info->num_ports = hub_desc.bNbrPorts;
        hub_info->tt = (dev_speed == USB_SPEED_HIGH) ? USB_HUB_GET_TT(hub_desc.wHubCharacteristics):0;

        bh = coordAllocBusHandle(BUS_ID_USB, coord_dev);
        usb_register_bus_device(h_info, bh);

        /* The Hub class code is only valid at the device level and that's what the driver has to match */
        id_key = USB_ID_PACK(USB_ID_METHOD_DEVICE, dev_desc.bDeviceClass, dev_desc.bDeviceSubClass, dev_desc.bDeviceProtocol);

        device_addr_desc.address = full_address;
        device_addr_desc.busId = BUS_ID_USB;

        entry_context = (usb_device_entry_context *)calloc(sizeof(usb_device_entry_context) + sizeof(usbDeviceDescriptor), 1 );
        entry_context->bus_power_available = dc_msg->max_bus_power;
        entry_context->length = sizeof(usbDeviceDescriptor);
        entry_context->flags = 0;
        entry_context->dev_speed = dc_msg->dev_speed;
        entry_context->topo_string = usb_construct_topo_string(h_info);

        memcpy(&entry_context->desc_buffer[0], (void *)(&dev_desc), entry_context->length);
        printf("[USB ]: Register USB hub device @ %X with bus handle %X\n", full_address, bh);
        status = registerNewDevice(&device_addr_desc, coord_dev, dc_msg->parent_handle, id_key, bh, entry_context );
        if( DRIVER_OP_ERROR(status) )
        {
            free(entry_context);
            free(hub_info);
            coordFreeBusHandle(BUS_ID_USB, bh, coord_dev);
            printf("[USB ]: Failed to register device @ %X Code: %u\n", full_address, status);
        }
    }
    else if( dev_desc.bDeviceClass == USB_CLASS_PROBE_IFACE)
    {
        busOperationResult op_res;
        volatile uint16_t device_status;
        uint8_t config_num, config_desc_index;

        /* The device has to be enumerated at the interface level, try to find an accept configuration */
        //printf("[USB ]: Attempting to retrieve status for multifunction device @ %X\n", full_address);
        op_res = usb_coord_get_status(full_address, USB_REQ_TYPE_DEVICE, 0, &device_status, sync);
        if(op_res != busOpSuccess)
        {
            printf("[USB ]: Failed to retrieve device status for device @ %X Status: %u\n", full_address, op_res);
            return;
        }
        else
            printf("[USB ]: Multifunction device status retrieved!\n");

        op_res = usb_get_best_config( &h_info->device, device_status, &config_num, &config_desc_index, sync, &config_desc);
        if(op_res != busOpSuccess)
        {
            printf("[USB ]: Failed to retrieve acceptable configuration for device @ %X Status: %u\n", full_address, op_res);
            return;
        }

        desc_buffer = (volatile uint8_t *)config_desc;

        if( usb_split_config_desc(full_address, desc_buffer, config_desc->wTotalLength, config_desc->bNumInterfaces, hub_info, h_info,  sync) != driverOpSuccess)
        {

            printf("[USB ]: Failed to parse configuration descriptor for device @ %X\n", full_address);
            free(hub_info);
            free(h_info);
            return;
        }

        const char *device_name;
        /* Assign an appropriate name string to the overall device */
        /*
        if(dev_desc.iProduct)
        {
            if( usb_get_string_desc(full_address, dev_desc.iProduct, USB_LANGID_ENGLISH_US, sync, &device_name ) != busOpSuccess )
            {
                printf("[USB ]: Failed to retrieve device string descriptor!\n");
                free(hub_info);
                free(h_info);
                return ;
            }
        }
        else
        {
        */
            device_name = "Multifunction Device";
        //}
        /* Create an encompassing parent device in the overall device hierarchy */
        bh = coordRegisterMultifunction(device_name, "mfdev", full_address, dc_msg->parent_handle, coord_dev, BUS_ID_USB );
        if(!bh)
        {
            printf("[USB ]: Error unable to register bus handle for multifunction device: %s @ %X\n", device_name, full_address);
            free(hub_info);
            free(h_info);
            return;
        }

        usb_register_bus_device(h_info, bh);

        op_res = usb_coord_set_config(full_address, config_desc->bConfigurationValue, sync);
        h_info->device.active_config_desc = config_desc_index;

        if(op_res != busOpSuccess)
        {
            printf("[USB ]: Device @ %X failed to set configuration value of %u Status: %u\n", full_address, config_desc->bConfigurationValue, op_res);
        }

        memset(&ct, 0, sizeof(usbControllerTransaction));

        /* Enumerate the actual interface entries */
        //printf("[USB ]: Parsing %u interfaces for device @ %X\n", h_info->device.child_count, full_address);

        for(int interface_index = 0; interface_index < h_info->device.child_count; interface_index++)
        {
            status = usb_parse_interface(&h_info->device.children[interface_index], bh);
            if(status != driverOpSuccess )
            {
                printf("[USB ]: Unable to load driver for address: %X interface: %u Status: %u\n", full_address, h_info->device.children[interface_index].iface.interface_id, status);
            }
        }


    }
    sync_destroy_signal_obj(sync);
}

/*!  Processes a new port connection request
 *   \param msg Port connection message
 *
 */
void usb_port_connect_task(void *msg)
{
    sync_object *wait_obj;
    uint64_t controller_addr;
    uint64_t addr, parent_addr;
    const usb_handle_info *parent_hi;
    usbConfigureAddress ca;
    busOperationResult status;

    usb_port_connect_msg *pc_msg;
    device_connect_msg *dc_msg;

    pc_msg = (usb_port_connect_msg *)msg;

    parent_addr = usb_get_addr_from_handle(pc_msg->parent_handle, &parent_hi );

    /* Form the full default USB address */
    controller_addr = pc_msg->controller_id;
    controller_addr *= USB_MAX_ADDRESS;
    sync_create_signal_obj(&wait_obj, 0, 0);

    /* Wait 10ms in case a reset is still processing for the device */
    sync_object_wait(wait_obj, 10);
    sync_object_reset(wait_obj);

    /* Signal the controller to perform setup for a new device on its side */
    memset(&ca, 0, sizeof(usbConfigureAddress));

    ca.command_type = usbPrepareDevice;
    ca.connection_info.dev_speed = pc_msg->dev_speed;
    ca.connection_info.parent_addr = pc_msg->parent_address;
    ca.connection_info.port_num = pc_msg->port_num;

    status = busConfigAddress(BUS_ID_USB, controller_addr, &ca, sizeof(usbConfigureAddress), wait_obj, coord_dev);
    if(status != busOpSuccess)
    {
        printf("[USB ]: Controller #%u failed to configure resources for port connection!\n", pc_msg->controller_id);
        goto respond_error;
    }

    sync_object_wait(wait_obj, 500);

    addr = sync_object_get_status(wait_obj);
    if(!addr)
    {
        printf("[USB ]: Failed to complete controller side configuration for new @ %X:%u\n", parent_addr, pc_msg->port_num);
        status = busOpResources;
        goto respond_error;
    }


    /* Prepare the device connection information structure */
    dc_msg = (device_connect_msg *)calloc(sizeof(device_connect_msg), 1);
    dc_msg->controller_id = pc_msg->controller_id;
    dc_msg->dev_speed = pc_msg->dev_speed;
    dc_msg->max_bus_power = parent_hi->device.max_bus_power;
    dc_msg->parent_address = dc_msg->parent_address;
    dc_msg->parent_handle = pc_msg->parent_handle;
    dc_msg->port_num = pc_msg->port_num;
    dc_msg->usb_address = addr;
    dc_msg->flags = USB_NEW_CONNECT_ADDRESSED;

    printf("[USB ]: New device attached @ Port: %u Addr: %u\n", pc_msg->port_num, addr);


    /* Notify hub driver that the device has been addressed and that the default address is now available */
    if(pc_msg->connect_wait)
        sync_object_signal(pc_msg->connect_wait, busOpSuccess);

    sync_destroy_signal_obj(wait_obj);


    free(pc_msg);

    /* Attempting to configure new device */
    usb_device_connect_task(dc_msg);
    return;

respond_error:
    if(pc_msg->connect_wait)
        sync_object_signal(pc_msg->connect_wait, status);
    sync_destroy_signal_obj(wait_obj);
    free(pc_msg);
    return;
}

/*! Decodes an endpoint descriptor provided by a device
 *  \param response Response buffer pointer
 *  \param resSize Size of the response buffer
 *  \param msg Request message
 *  \param msgSize Size of the request message
 *  \param unique_key Unique key for the coordinator-device combo
 *  \return busMsgBadFormat if the message or response are improperly formatted.  busMsgAcknowledged if the endpoint descriptor was successfully parsed.
 */
busMsgResponse usb_decode_endpoint_desc(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    usb_decode_ep_response *response_msg;
    usb_decode_ep_request *request;
    const usb_handle_info *handle_info;
    int desc_length, dummy_skip;
    uint8_t dev_speed;
    uint32_t dummy_mask = 0;

    dummy_skip = 0;

    if(!resSize || !msg)
        return busMsgBadFormat;
    if( *resSize < sizeof(usb_decode_ep_response) )
        return busMsgBadFormat;
    if(msgSize < sizeof(usb_decode_ep_request))
        return busMsgBadFormat;

    response_msg = (usb_decode_ep_response *)response;
    request = (usb_decode_ep_request *)msg;

    handle_info = usb_get_bus_handle_info(request->requestor);
    if(!handle_info)
        return busMsgBadFormat;
    if(handle_info->type == usb_device_info_type)
        dev_speed = handle_info->device.dev_speed;
    else
        dev_speed = handle_info->iface.parent->device.dev_speed;

    desc_length = usb_parse_endpoint( (volatile usbEndpointDescriptor *)request->ep_desc, request->ep_fields, dev_speed, request->max_desc_bytes, &dummy_skip, &dummy_mask);
    if(!desc_length)
        return busMsgNegative;

    response_msg->desc_bytes_traversed = desc_length;

    return busMsgAcknowledged;

}

