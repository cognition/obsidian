/* USB Coordinator Driver topology management functions
 *
 * Author: Joseph Kinzel
 */
#ifndef USB_TOPO_H_
#define USB_TOPO_H_

#include <driverapi.h>
#include <usb.h>

typedef enum
{
    usb_controller_info_type,
    usb_device_info_type,
    usb_interface_info_type
} handle_info_type;

typedef struct
{
    struct usb_handle_info *children;
    void *controller_lock;
    uint8_t dev_speed, child_count, max_hub_depth;
} usb_controller_info;

typedef struct
{
    uint64_t full_address;
    struct _usb_handle_info *children;
    void *device_lock;
    struct _usb_handle_info *parent;
    usbHubInfo *hub_info;
    volatile uint8_t *desc_buffer;
    uint32_t endpoints, max_bus_power, power_headroom;
    uint16_t desc_size;
    uint8_t dev_speed, child_count, config_count, port, active_config_desc;
} usb_device_info;

typedef struct
{
    void *interface_lock;
    volatile uint8_t *desc_buffer;
    struct _usb_handle_info *parent;
    uint32_t endpoints;
    uint16_t desc_size;
    uint8_t interface_id;
} usb_interface_info;

typedef struct _usb_handle_info
{
    handle_info_type type;
    union
    {
        usb_controller_info controller;
        usb_device_info device;
        usb_interface_info iface;
    };
} usb_handle_info;

int usb_init_bh_table();
void usb_destroy_bh_table();
void usb_register_bus_device(usb_handle_info *h_info, BUS_HANDLE handle);
const usb_handle_info *usb_get_bus_handle_info(BUS_HANDLE handle);
usb_handle_info *usb_get_mutable_hi(BUS_HANDLE handle);
uint64_t usb_get_addr_from_handle(BUS_HANDLE handle, const usb_handle_info **hi_ret);
uint32_t usb_construct_topo_string(usb_handle_info *h_info);

#endif // USB_TOPO_H_
