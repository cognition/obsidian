/* USB Descriptor access functions
 *
 * Author: Joseph Kinzel */

#include <usb.h>
#include "topo.h"
#include "transaction.h"
#include "descriptors.h"
#include "new_connect.h"

extern DEVICE_HANDLE coord_dev;

/*! Handles a request from a driver to retrieve a descriptor for a target device
 *
 *  \param response Return pointer for a response value
 *  \param resSize Pointer to the size of the response buffer
 *  \param msg Pointer to the request message
 *  \param msgSize Size of the message in bytes
 */
busMsgResponse usb_get_desc_request(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize)
{
    uint64_t full_addr;
    const usb_handle_info *hi;
    const usb_device_get_descriptor *get_desc;

    /* Validate the response and message fields */
    if(!response)
        return busMsgBadFormat;
    if(msgSize < sizeof(usb_device_get_descriptor))
        return busMsgBadFormat;
    if(*resSize < sizeof(uintptr_t))
        return busMsgBadFormat;

    get_desc = (const usb_device_get_descriptor *)msg;

    /* Make sure the request type is valid */
    if(get_desc->request_type >= USB_BMRT_RESERVED)
    {
        printf("[USB ]: GD - Invalid request type: %u\n", get_desc->request_type);
        return busMsgBadFormat;
    }

    /* Get the full USB address of the device/interface making the request */
    full_addr = usb_get_addr_from_handle(get_desc->requestor, &hi);

    if(!full_addr)
        return busMsgBadFormat;

    /* Validate the sanity of the request */
    switch(get_desc->desc_type)
    {
        default:
        case usbDescDevice:
        case usbDescConfig:
        case usbDescBOS:
            if(hi->type != usb_device_info_type)
                return busMsgNegative;
            break;
        case usbDescEndpoint:
        case usbDescString:
        case usbDescInterface:
            if(hi->type == usb_controller_info_type)
                return busMsgNegative;
            break;
    }

    if( usb_coord_get_desc(full_addr, get_desc->desc_type, get_desc->desc_index, 0, get_desc->request_type, get_desc->expected_size, get_desc->sync, response) != busOpSuccess )
        return busMsgNegative;
    else
        return busMsgAffirmative;
}

/*! Retrieves a descriptor from a given device
 *
 *  \param full_addr Full address of the target device
 *  \param desc_type Descriptor type to retrieve
 *  \param desc_index Index of the descriptor being retrieved
 *  \param language_id Language ID of the target descriptor
 *  \param req_type Type of request (class, device, vendor)
 *  \param expected_size Expected size of the descriptor
 *  \param sync Synchornization object
 *  \param data Return pointer for the descriptor buffer
 */
busOperationResult usb_coord_get_desc(const uint64_t full_addr, const uint8_t desc_type, const uint8_t desc_index, const uint16_t language_id, const uint8_t req_type,
                       const uint16_t expected_size, sync_object *sync, volatile void **data)
{
    int status;
    uint16_t wv;
    usbControllerTransaction ct;
    volatile uint8_t *buffer;

    wv = desc_type;
    wv <<= 8;
    wv |= desc_index;

    buffer = (volatile uint8_t *)malloc(expected_size);

    /* Form the control transaction and dispatch it to the controller */
    ct.type = usbTransactionControl;
    ct.control.bm_request_type = USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_DEVICE | (req_type<<5);
    ct.control.b_request = USB_CONTROL_REQ_GET_DESCRIPTOR;
    ct.control.data_ptr = buffer;
    ct.control.w_index = language_id;
    ct.control.w_length = expected_size;
    ct.control.w_value = wv;
    ct.flags = USB_TRANSACTION_IN;

    status = usb_single_transaction(full_addr | USB_TRANSACTION_IN, &ct, sync);

    if(status != busOpSuccess)
    {
        printf("[USB ]: Get descriptor failed for address: %X WV: %X\n", full_addr, wv);
        free((void *)buffer);
    }
    else
        *data = buffer;

    return status;
}

/*! Processes a request from a device/interface to get a status value
 *
 *  \param response Return pointer for a response value
 *  \param resSize Pointer to the size of the response buffer
 *  \param msg Pointer to the request message
 *  \param msgSize Size of the message in bytes
 */
busMsgResponse usb_get_status(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType)
{
    usb_get_status_request *request;
    volatile uint16_t *status_response;
    const usb_handle_info *hinfo;
    uint32_t mask, endpoints;
    uint64_t full_address;

    /* Validate the message and response parameters */
    if(!msg || !response || !resSize)
        return busMsgBadFormat;
    if(*resSize < 2)
        return busMsgBadFormat;
    if(msgSize < sizeof(usb_get_status_request) )
        return busMsgBadFormat;

    request = (usb_get_status_request *)msg;
    status_response = (volatile uint16_t *)response;

    hinfo = usb_get_bus_handle_info(request->requestor);
    if(!hinfo)
        return busMsgBadFormat;

    if(hinfo->type == usb_device_info_type)
    {
        endpoints = hinfo->device.endpoints;
        full_address = hinfo->device.full_address;
    }
    else
    {
        endpoints = hinfo->iface.endpoints;
        full_address = hinfo->iface.parent->device.full_address;
    }

    /* Validate the request's ability to access a specific interface or endpoint */
    switch(request->status_type)
    {
        case USB_REQ_TYPE_ENDPOINT:
            mask = 1;
            mask <<= request->type_index & 0xF;
            if( !(mask & endpoints) )
                return busMsgBadFormat;
            break;
        case USB_REQ_TYPE_INTERFACE:
            if(request->type_index != hinfo->iface.interface_id)
                return busMsgBadFormat;
            break;
        case USB_REQ_TYPE_DEVICE:
            break;
        default:
            return busMsgBadFormat;
            break;
    }

    if(usb_coord_get_status(full_address, request->status_type, request->type_index, status_response, request->sync) != busOpSuccess)
    {
        return busMsgNegative;
    }


    *resSize = 2;
    return busMsgAcknowledged;
}

/*! Coordinator internal logic for issuing GET_STATUS requests
 *
 *  \param full_addr Full address of the device being targeted
 *  \param status_type Type of status to be retrieve (device, endpoint, interface)
 *  \param type_index Index of the status type being retrieve (interface or endpoint number)
 *  \param status_value Return pointer for the status value
 *  \param sync Synchronization object that will be triggered once the operation is completed
 *
 *  \return busOpSuccess if issuance of the request succeeded or an applicable error from the controller.  Operation result can be retrieved by the synchronization object.
 */
busOperationResult usb_coord_get_status(const uint64_t full_addr, const uint8_t status_type, const uint16_t type_index, volatile uint16_t *status_value, sync_object *sync)
{
    usbControllerTransaction ct;
    busOperationResult result;

    ct.type = usbTransactionControl;
    ct.control.bm_request_type = USB_REQ_TYPE_DEV_TO_HOST | (status_type & 0x03U);
    ct.control.b_request = USB_CONTROL_REQ_GET_STATUS;
    ct.control.data_ptr = status_value;
    ct.control.w_index = type_index;
    ct.control.w_length = 2;
    ct.control.w_value = 0;
    ct.flags = 0;

    result = usb_single_transaction(full_addr | USB_TRANSACTION_IN, &ct, sync);

    return result;
}

/*! Gets the 'best' usable configuration of the device based on the assumption that additional functionality will require more power
 *
 * \param dev_info Device information structure
 * \param dev_status Status value for the device
 * \param config_num Return pointer for the best configuration value
 * \param config_desc_index Index of the configuration descriptor corresponding to the best configuration value
 * \param sync Wait/signal style synchronization object for use by the function
 * \param retDesc Return pointer for a copy of the 'best' configuration descriptor
 */
busOperationResult usb_get_best_config(const usb_device_info *dev_info, const uint16_t dev_status, uint8_t *config_num, uint8_t *config_desc_index, sync_object *sync, volatile usbConfigDescriptor **ret_desc)
{
    uint16_t best_config, best_attr;
    uint8_t best_index;
    int best_diff;
    busOperationResult status;
    volatile usbConfigDescriptor *config_desc, *best_desc;

    best_diff = dev_info->max_bus_power + 1;
    best_attr = 0;
    best_config = 0;
    best_desc = NULL;

    printf("[USB ]: Attempting to find the best of %u possible configuration for device @ %X\n", dev_info->config_count, dev_info->full_address);
    for(int config_index = 0; config_index < dev_info->config_count; config_index++)
    {
        uint32_t power_diff, requested_power;

        status = usb_coord_get_desc(dev_info->full_address, usbDescConfig, config_index, 0, USB_BMRT_STANDARD, sizeof(usbConfigDescriptor), sync, (volatile void **)&config_desc);
        if(status != busOpSuccess)
        {
            printf("[USB ]: Failed to retrieve configuration descriptor %u for device %X with status code: %u\n", config_index, dev_info->full_address, status);
            return status;
        }

        requested_power = config_desc->bMaxPower;

        /* USB3 denotes power in different units than prior specifications */
        if(dev_info->dev_speed >= USB_SPEED_SUPER)
            requested_power *= 8;
        else
            requested_power *= 2;

        /* Search for configurations that can be powered */
        if(dev_info->max_bus_power >= requested_power)
        {
            power_diff = dev_info->max_bus_power - requested_power;
            if(dev_status & USB_DEV_STATUS_SELF_POWERED)
            {
                if(config_desc->bmAttributes & USB_CONFIG_DESC_ATTR_SELF_POWERED)
                {
                    /* If the device is self powered prioritize configurations that are also self powered */
                    if( !(best_attr & USB_CONFIG_DESC_ATTR_SELF_POWERED) )
                    {
                            best_index = config_index;
                            best_config = config_desc->bConfigurationValue;
                            best_diff = power_diff;
                            best_attr = config_desc->bmAttributes;
                            if(best_desc)
                                free((void *)best_desc);

                            best_desc = config_desc;
                    }
                    else if(power_diff < best_diff)
                    {
                        best_index = config_index;
                        best_config = config_desc->bConfigurationValue;
                        best_diff = power_diff;
                        best_attr = config_desc->bmAttributes;
                        if(best_desc)
                            free((void *)best_desc);

                        best_desc = config_desc;
                    }
                }
                else if( !(best_attr & USB_CONFIG_DESC_ATTR_SELF_POWERED) )
                {
                    if(power_diff < best_diff)
                    {
                        best_index = config_index;
                        best_config = config_desc->bConfigurationValue;
                        best_diff = power_diff;
                        best_attr = config_desc->bmAttributes;
                        if(best_desc)
                            free((void *)best_desc);
                        best_desc = config_desc;
                    }
                }
            }
            else if( !(config_desc->bmAttributes & USB_CONFIG_DESC_ATTR_SELF_POWERED) )
            {
                /* If the device isn't self powered any chosen configuration must be powered exclusively from the bus */
                if(power_diff < best_diff)
                {
                    best_index = config_index;
                    best_config = config_desc->bConfigurationValue;
                    best_diff = power_diff;
                    best_attr = config_desc->bmAttributes;
                    if(best_desc)
                        free((void *)best_desc);
                    best_desc = config_desc;

                }
            }
            else if( (!best_config) && requested_power )
            {
                    best_index = config_index;
                    best_config = config_desc->bConfigurationValue;
                    best_diff = power_diff;
                    best_attr = config_desc->bmAttributes;
                    if(best_desc)
                        free((void *)best_desc);
                    best_desc = config_desc;
            }
            else
            {
                printf("[USB ]: Configuration %u on device %X is unusable because it is powered while the device is not.", config_desc->bConfigurationValue, dev_info->full_address);
            }
        }
        else
        {
            printf("[USB ]: Device %X Configuration descriptor exceeds available power Requested: %u Available: %u\n",
                   dev_info->full_address, requested_power, dev_info->max_bus_power);
        }
        if(config_desc != best_desc)
            free((void *)config_desc);
    }

    if(!best_config)
    {
        printf("[USB ]: Unable to locate a valid configuration for device @ %X\n", dev_info->full_address);
        return busOpResources;
    }
    else
    {

        //printf("[USB ]: Found best configuration of %u with descriptor length of %u for device @ %X\n", best_config, best_desc->wTotalLength, dev_info->full_address);
    }

    *config_num = best_config;
    *config_desc_index = best_index;

    /* Retrieve the full configuration descriptor */
    status = usb_coord_get_desc(dev_info->full_address, usbDescConfig, best_index, 0, USB_BMRT_STANDARD, best_desc->wTotalLength, sync, (volatile void **)ret_desc);
    free((void *)best_desc);
    return status;
}

/*! Sets the configuration for a device
 *
 *  \param full_addr Full address of the target device
 *  \param config_value Configuration value to set
 *  \param sync_object Wait/signal style of sync object
 */
busOperationResult usb_coord_set_config(const uint64_t full_addr, const uint8_t config_value, sync_object *sync_obj)
{
    usbControllerTransaction ct;
    busOperationResult op_res;
    uint8_t configured_value;

    /* Form the control transaction to issue the SET_CONFIG request */
    ct.type = usbTransactionControl;
    ct.control.bm_request_type = USB_BMRT_STANDARD | USB_REQ_TYPE_HOST_TO_DEV | USB_REQ_TYPE_DEVICE;
    ct.control.b_request = USB_CONTROL_REQ_SET_CONFIG;
    ct.control.w_length = 0;
    ct.control.w_value = config_value;
    ct.control.w_index = 0;
    ct.control.data_ptr = NULL;
    ct.flags = USB_CONTROLLER_FLAG_CONFIG_REQUEST;

    op_res = usb_single_transaction(full_addr, &ct, sync_obj);
    if(op_res != busOpSuccess)
        return op_res;

    if( sync_object_get_status(sync_obj) != usb_op_success)
    {
        return busOpInvalidArg;
    }

    /* Form the control transaction to issue the GET_CONFIG request */
    ct.control.bm_request_type = USB_BMRT_STANDARD | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_DEVICE;
    ct.control.b_request = USB_CONTROL_REQ_GET_CONFIG;
    ct.control.w_length = 1;
    ct.control.data_ptr = &configured_value;
    ct.control.w_value = 0;
    ct.control.w_index = 0;
    ct.flags = USB_TRANSACTION_IN;

    op_res = usb_single_transaction(full_addr | USB_TRANSACTION_IN, &ct, sync_obj);

    if(op_res != busOpSuccess)
        return op_res;

    if( sync_object_get_status(sync_obj) != usb_op_success )
    {
        return busOpIOError;
    }

    /* Validate that the new configuration has been set */
    if(configured_value != config_value)
        return busOpIOError;
    else
        return busOpSuccess;
}

/*! Sets an alternate interface on a device
 *
 *  \param full_address Full USB address of the device
 *  \param interface_num Interface number that's being modified
 *  \param alternate_value Alternate interface value to set
 *  \param sync_obj A signal synchronization object
 */
busOperationResult usb_coord_set_interface(const uint64_t full_address, const uint8_t interface_num, const uint8_t alternate_value, sync_object *sync_obj)
{
    usbControllerTransaction ct;
    busOperationResult op_res;

    /* Set up the SET_INTERFACE control request */
    ct.type = usbTransactionControl;
    ct.control.bm_request_type = USB_BMRT_STANDARD | USB_REQ_TYPE_HOST_TO_DEV | USB_REQ_TYPE_INTERFACE;
    ct.control.b_request = USB_CONTROL_REQ_SET_INTERFACE;
    ct.control.w_length = 0;
    ct.control.data_ptr = 0;
    ct.control.w_value = alternate_value;
    ct.control.w_index = interface_num;
    ct.flags = 0;

    op_res = usb_single_transaction(full_address | USB_TRANSACTION_IN, &ct, sync_obj);

    return op_res;
}

/*! Parses an alternate interface descriptor on the current configuration
 *
 *  \param di Information for the target device
 *  \param iface_desc Target interface descriptor
 *  \param max_offset Maximum offset from the start of the interface descriptor that can be safely parsed
 *  \param sync Signal style synchronization object
 *
 *  \return Number of bytes parsed while setting
 */
static int usb_parse_and_set_interface(usb_device_info *di, volatile usbInterfaceDescriptor *iface_desc, const int max_offset, sync_object *sync)
{
    int desc_offset, desc_size, ep_skip;
    uint32_t ep_mask, ep_rem;
    volatile uint8_t *desc_buffer;
    busOperationResult b_result;
    usbConfigureAddress ca;


    desc_buffer = (volatile uint8_t *)iface_desc;
    desc_size = 0;
    ep_mask = 0;
    ep_skip = 0;
    ep_rem = iface_desc->bNumEndpoints;

    /* Setup the configure address request */
    memset(&ca, 0, sizeof(usbConfigureAddress));
    ca.command_type = usbConfigureEndpoint;
    ca.device_info.dev_speed = di->dev_speed;
    ca.device_info.hub_info = *(di->hub_info);
    ca.device_info.interface = iface_desc->bInterfaceNumber;
    ca.device_info.alt_setting = iface_desc->bAlternateSettings;

    /* TODO: Cycle through interface children to the remove any currently active configurations and their endpoints */

    /* Parse all endpoints in the interface descriptor and update the controller configure address structure with their information */
    for(desc_offset = sizeof(usbInterfaceDescriptor); desc_offset < max_offset; desc_offset += desc_size)
    {
        if(desc_buffer[desc_offset + USB_DESC_FIELD_TYPE_OFF] == usbDescEndpoint)
        {
            volatile usbEndpointDescriptor *ep_desc;

            ep_desc = (volatile usbEndpointDescriptor *)&desc_buffer[desc_offset];

            desc_size = usb_parse_endpoint(ep_desc, ca.device_info.endpoints, di->dev_speed, max_offset - desc_offset, &ep_skip, &ep_mask);
            if(!desc_size)
                return 0;
            if(!--ep_rem)
            {
                desc_offset += desc_size;
                break;
            }
        }
    }

    if(ep_rem)
        return 0;

    di->endpoints |= ep_mask;

    /* Issue the controller configuration request */
    b_result = busConfigAddress(BUS_ID_USB, di->full_address, &ca, sizeof(usbConfigureAddress), sync, coord_dev);
    if(b_result != busOpSuccess)
        return 0;

    sync_object_wait(sync, 500);
    if(sync_object_get_status(sync) != usb_op_success)
        return 0;

    /* Issue the SET_INTERFACE request */
    b_result = usb_coord_set_interface(di->full_address, iface_desc->bInterfaceNumber, iface_desc->bAlternateSettings, sync);
    if(b_result != busOpSuccess)
        return 0;

    return desc_offset;
}

/*! Retrieves the current configuration value for a device
 *
 *  \param full_address Full USB address of the device
 *  \param current_config Pointer to the return value buffer for the configuration value
 *  \param sync Signal type synchronization object
 */
COMPILER_UNUSED static busOperationResult usb_get_current_config(const uint64_t full_address, uint8_t *current_config, sync_object *sync)
{
    busOperationResult status;
    usbControllerTransaction ct;

    memset(&ct, 0, sizeof(usbControllerTransaction));

    /* Form the control transaction to issue the GET_CONFIG request */
    ct.control.bm_request_type = USB_BMRT_STANDARD | USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_DEVICE;
    ct.control.b_request = USB_CONTROL_REQ_GET_CONFIG;
    ct.control.w_length = 1;
    ct.control.data_ptr = current_config;
    ct.control.w_value = 0;
    ct.control.w_index = 0;
    ct.flags = USB_TRANSACTION_IN;

    status = usb_single_transaction(full_address | USB_TRANSACTION_IN, &ct, sync);

    return status;
}

/*! Handle device driver requests to set alternative interface values for a device
 *
 * \param response Not required
 * \param resSize Not required
 * \param msg A pointer to a usb_req_alt_interfaces message
 * \param msgSize Size of the message, base_msg_size + largest interface_index + 1
 * \param unique_key Not used
 */
busMsgResponse usb_req_alt_iface(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t unique_key)
{
    busMsgResponse result;
    usb_handle_info *hi;
    int desc_size, alt_iface_count;
    uint32_t iface_rem_mask;
    volatile uint8_t *config_desc_buffer;
    volatile usbConfigDescriptor *config_desc;
    usb_request_alt_interfaces *req;
    sync_object *sync;

    busOperationResult bus_res;
    if(!msg)
        return busMsgBadFormat;

    if(msgSize < sizeof(usb_request_alt_interfaces))
        return busMsgBadFormat;

    alt_iface_count = 0;
    req = (usb_request_alt_interfaces *)msg;

    /* Get the handle information of requestor  */
    hi = usb_get_mutable_hi(req->requestor);

    if(!hi)
    {
        printf("[USB ]: SRC - Failed to retrieve handle info for handle %X\n", req->requestor);
        return busMsgBadFormat;
    }
    if(hi->type != usb_device_info_type)
    {
        printf("[USB ]: SRC - Failed to validate bus handle type expected: %u found: %u\n", usb_device_info_type, hi->type);

        return busMsgBadFormat;
    }

    result = busMsgNoAck;

    sync_create_signal_obj(&sync, 0, 0);
    /* Get the configuration descriptor, which contains all the information about interfaces and their endpoints */
    bus_res = usb_coord_get_desc(hi->device.full_address, usbDescConfig, hi->device.active_config_desc, 0, USB_BMRT_STANDARD, 24, sync, (volatile void **)&config_desc_buffer);

    if(bus_res != busOpSuccess)
        goto alt_iface_exit;

    iface_rem_mask = req->interface_mask;
    config_desc = (volatile usbConfigDescriptor *)config_desc_buffer;

    desc_size = 0;

    /* Iterate through all interface descriptors and attempt to match all requested alternate settings */
    for(int desc_offset = sizeof(usbConfigDescriptor); desc_offset < config_desc->wTotalLength; desc_offset += desc_size)
    {
        desc_size = config_desc_buffer[desc_offset + USB_DESC_FIELD_LENGTH_OFF];

        if(config_desc_buffer[desc_offset + USB_DESC_FIELD_TYPE_OFF] == usbDescInterface)
        {
            volatile usbInterfaceDescriptor *iface_desc;

            iface_desc = (volatile usbInterfaceDescriptor *)&config_desc_buffer[desc_offset];
            if(iface_desc->bInterfaceNumber < 32)
            {
                uint32_t iface_entry_mask;

                iface_entry_mask = 1;
                iface_entry_mask <<= iface_desc->bInterfaceNumber;

                if(iface_entry_mask & iface_rem_mask)
                {
                    if(req->interface_indices[iface_desc->bInterfaceNumber] == iface_desc->bAlternateSettings )
                    {
                        desc_size = usb_parse_and_set_interface(&hi->device, iface_desc, config_desc->wTotalLength - desc_offset, sync);
                        if(!desc_size)
                        {
                            printf("[USB ]: Failed to set alternate interface %u:%u for device @ %X\n", iface_desc->bInterfaceNumber, iface_desc->bAlternateSettings, hi->device.full_address);
                            goto alt_iface_exit;
                        }
                    }

                    ++alt_iface_count;
                    iface_rem_mask &= ~iface_entry_mask;
                    if(!iface_rem_mask)
                    {
                        printf("[USB ]: Successfully set %u alternate interfaces for device @ %X\n", alt_iface_count, hi->device.full_address);
                        result = busMsgAcknowledged;
                        goto alt_iface_exit;
                    }
                }
            }
        }
    }
    printf("[USB ]: Failed to set all requested alternate interfaces! Rem req mask: %X\n", iface_rem_mask);
alt_iface_exit:
    free((void *)config_desc_buffer);
    sync_destroy_signal_obj(sync);
    return result;
}

/*! Handles device driver requests to set a specific configuration value
 *
 *  \param response Return pointer for a response value
 *  \param resSize Pointer to the size of the response buffer
 *  \param msg Pointer to the request message
 *  \param msgSize Size of the message in bytes
 */
busMsgResponse usb_request_config(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t unique_key)
{
    usb_request_config_msg *request;
    volatile usbConfigDescriptor *full_desc;
    busOperationResult op_res;
    driverOperationResult status;
    volatile usbConfigDescriptor **ret_ptr;
    volatile uint8_t *desc_buffer;
    usb_handle_info *hi;
    uint16_t device_status;
    uint8_t config_value, best_index;
    sync_object *sync;

    /* Validate the message and response parameters */
    if(!response)
        return busMsgBadFormat;

    if(*resSize < sizeof(usbConfigDescriptor *))
        return busMsgBadFormat;

    if(msgSize < sizeof(usb_request_config_msg))
        return busMsgBadFormat;

    request = (usb_request_config_msg *)msg;
    ret_ptr = (volatile usbConfigDescriptor **)response;

    hi = usb_get_mutable_hi(request->requestor);
    if(!hi)
    {
        printf("[USB ]: SRC - Failed to retrieve handle info for handle %X\n", request->requestor);
        return busMsgBadFormat;
    }
    if(hi->type != usb_device_info_type)
    {
        printf("[USB ]: SRC - Failed to validate bus handle type expected: %u found: %u\n", usb_device_info_type, hi->type);

        return busMsgBadFormat;
    }
    sync_create_signal_obj(&sync, 0, 0);

    /* Get the status of the device */
    op_res = usb_coord_get_status(hi->device.full_address, USB_REQ_TYPE_DEVICE, 0, &device_status, sync);
    if(op_res != busOpSuccess)
    {
        printf("[USB ]: SRC failed to retrieve status for device @ %X\n", hi->device.full_address);
        return busMsgNegative;
    }
    else
        printf("[USB ]: Got device status of %X for device @ %X\n", device_status, hi->device.full_address);


    /* Negative descriptor indices in the request instruct the coordinator to attempt to find the best configuration */
    if(request->descriptor_index < 0)
    {
        op_res = usb_get_best_config( &hi->device, device_status, &config_value, &best_index, sync, &full_desc);
        if(op_res != busOpSuccess)
        {
            printf("[USB ]: Failed to retrieve configurations for device @ %X Status: %u\n", hi->device.full_address, op_res);
            sync_destroy_signal_obj(sync);
            return busMsgNegative;
        }
        if(!config_value)
        {
            printf("[USB ]: Unable to locate usable configuration for device @ %X\n", hi->device.full_address);
            return busMsgNegative;
        }
        //printf("[USB ]: Request config - Found best configuration value of %u\n", config_value);

    }
    else
    {
        volatile usbConfigDescriptor *config_desc;

        uint32_t power_req;
        best_index = request->descriptor_index;
        if(request->descriptor_index >= hi->device.config_count)
        {
            sync_destroy_signal_obj(sync);
            return busMsgBadFormat;
        }
        op_res = usb_coord_get_desc(hi->device.full_address, usbDescConfig, request->descriptor_index, 0, USB_BMRT_STANDARD, sizeof(usbConfigDescriptor), sync, (volatile void **)&config_desc);
        if(op_res != busOpSuccess)
        {
            sync_destroy_signal_obj(sync);
            return busMsgNegative;
        }

        power_req = config_desc->bMaxPower;
        if(hi->device.dev_speed >= USB_SPEED_SUPER)
            power_req *= 8;
        else
            power_req *= 2;
        /* Refuse the set the configuration requested if there's not enough power to do so */
        if(power_req > hi->device.max_bus_power)
        {
            sync_destroy_signal_obj(sync);
            free((void *)config_desc);
            return busMsgNegative;
        }

        config_value = config_desc->bConfigurationValue;

        op_res = usb_coord_get_desc(hi->device.full_address, usbDescConfig, best_index, 0, USB_BMRT_STANDARD, config_desc->wTotalLength, sync, (volatile void **)&full_desc);
        if(op_res != busOpSuccess)
        {
            free((void *)config_desc);
            sync_destroy_signal_obj(sync);
            return busMsgNegative;
        }
        free((void *)config_desc);
    }

    *ret_ptr = full_desc;

    /* Do the controller level configuration first */
    desc_buffer = (volatile uint8_t *)full_desc;
    status = usb_split_config_desc(hi->device.full_address, desc_buffer, full_desc->wTotalLength, full_desc->bNumInterfaces, hi->device.hub_info, hi, sync );
    if(status != driverOpSuccess)
    {
        printf("[USB ]: Failed to do controller side device configuration for device @ %X\n", hi->device.full_address);
        sync_destroy_signal_obj(sync);
        free((void*)full_desc);
        return busMsgNegative;
    }
    else
    {
        //printf("[USB ]: Configuration descriptor successfully split!\n");
    }

    /* Dispatch the actual SET_CONFIGURATION request to the device */
    op_res = usb_coord_set_config(hi->device.full_address, config_value, sync);
    if(op_res != busOpSuccess)
    {
        printf("[USB ]: Failed to set configuration %u for device @ %X\n", config_value, hi->device.full_address);
        sync_destroy_signal_obj(sync);
        free((void *)full_desc);
        return busMsgNegative;
    }
    hi->device.active_config_desc = best_index;


    sync_destroy_signal_obj(sync);
    return busMsgAcknowledged;
}
