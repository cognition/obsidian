/* USB coordinator driver transaction support header
 *
 * Author: Joseph Kinzel */

#ifndef USB_TRANSACTION_H_
#define USB_TRANSACTION_H_

#include <driverapi.h>
#include <usb.h>

busMsgResponse usb_queue_transaction(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
busMsgResponse usb_run_transactions(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
busOperationResult usb_single_transaction(const uint64_t full_address, usbControllerTransaction *ct, sync_object *sync_info);

#endif // USB_TRANSACTION_H_
