/* USB Coordinator topology management system
 *
 * Author: Joseph Kinzel
 */

#include "topo.h"
#include <usb.h>
#include <dapi_container.h>

static void *bus_handle_map;
static void *bus_handle_lock;

/*! Constructs a topography string for a given device or interface
 *  \param h_info USB entity handle
 *
 *  \return Topography string
 */
uint32_t usb_construct_topo_string(usb_handle_info *h_info)
{
    usb_handle_info *ptr;
    uint32_t topo, route_string, depth;

    topo = 0;
    route_string = 0;
    depth = 0;
    ptr = h_info;

    while(ptr->type != usb_controller_info_type)
    {
        if(ptr->type == usb_device_info_type)
        {
            route_string <<= 4;
            route_string |= h_info->device.port & 0xF;
            ptr = ptr->device.parent;
        }
        else if(ptr->type == usb_interface_info_type)
        {
            topo = ptr->iface.interface_id;
            topo <<= USB_TOPO_INTERFACE_OFFSET;
            ptr = ptr->iface.parent;
        }
        depth++;
    }

    route_string >>= 4;
    depth <<= USB_TOPO_DEPTH_OFFSET;
    topo |= depth | route_string;

    return topo;
}

/*! Initializes the bus handle table
 *  \return driverOpSuccess if the table was created successfully, driverOpNoMemory if it failed
 */
int usb_init_bh_table()
{
    bus_handle_map = dapi_map_create();
    bus_handle_lock = dapi_lock_create();
    if(!bus_handle_map || !bus_handle_lock)
        return driverOpNoMemory;
    else
        return driverOpSuccess;
}

/*! Registers a new bus handle with information about a device or interface
 *  \param h_info Information associated with the bus handle
 *  \param handle Bus-side unique device handle
 */
void usb_register_bus_device(usb_handle_info *h_info, BUS_HANDLE handle)
{
    dapi_lock_acquire(bus_handle_lock);
    dapi_map_insert(bus_handle_map, handle, h_info);
    dapi_lock_release(bus_handle_lock);
}

/*! Gets the device or interface info that corresponds to a specific handle
 *  \param handle Bus handle of the device
 *  \return A pointer to the device or interface's information if it is found or null if it cannot be found
 */
const usb_handle_info *usb_get_bus_handle_info(BUS_HANDLE handle)
{
    const usb_handle_info *entry;

    dapi_lock_acquire(bus_handle_lock);
    entry = dapi_map_get(bus_handle_map, handle);
    dapi_lock_release(bus_handle_lock);
    return entry;
}

/*! Returns a mutable form of the handle information structure for the provided bus handle
 *  \param handle Handle to retrieve the structure for
 *  \return A mutable form of the handle information structure or NULL if the bus handle could not be resolved
 */
usb_handle_info *usb_get_mutable_hi(BUS_HANDLE handle)
{
    usb_handle_info *entry;

    dapi_lock_acquire(bus_handle_lock);
    entry = dapi_map_get_mut(bus_handle_map, handle);
    dapi_lock_release(bus_handle_lock);
    return entry;
}

/*! Retrieves the full address of a device or interface from a USB-specific BUS_HANDLE value
 *
 *  \param handle Handle to retrieve the address for
 *  \param hi_ret Return pointer the corresponding handle information structure
 *  \return The address of the device for the provided BUS_HANDLE or zero if the request could not be resolved
 */
uint64_t usb_get_addr_from_handle(BUS_HANDLE handle, const usb_handle_info **hi_ret)
{
    const usb_handle_info *entry;

    entry = usb_get_bus_handle_info(handle);
    if(!entry)
    {
        printf("[USB ]: Unable to resolve bus handle %X\n", handle);
        return 0;
    }
    switch(entry->type)
    {
        /* Interfaces propagate the address of the device that they belong to */
        case usb_interface_info_type:
            *hi_ret = entry;
            return entry->iface.parent->device.full_address;
            break;
        case usb_device_info_type:
            *hi_ret = entry;
            return entry->device.full_address;
        break;
    default:
        return 0;
    }
}

/* Destroys the bus handle map */
void usb_destroy_bh_table()
{
    dapi_map_destroy(bus_handle_map);
    dapi_lock_destroy(bus_handle_lock);
}
