/* USB coordinator driver transaction support functions
 * Allows for the queueing and dispatching of USB requests by drivers
 *
 * Author: Joseph Kinzel */

#include "transaction.h"
#include "topo.h"
#include <usb.h>

extern DEVICE_HANDLE coord_dev;

static busMsgResponse usb_queue_interrupt_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize);
static busMsgResponse usb_queue_bulk_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize);
static busMsgResponse usb_queue_control_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize);
static busMsgResponse usb_queue_iso_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize);

/*! Issues a single USB transaction and waits on it
 *  \param full_address Complete address of the device
 *  \param ct Transaction description
 *  \param sync_info Synchronization information
 */
busOperationResult usb_single_transaction(const uint64_t full_address, usbControllerTransaction *ct, sync_object *sync_info)
{
    busOperationResult status;

    sync_object_reset(sync_info);

    status = busQueueTransaction(BUS_ID_USB, full_address, ct, sizeof(usbControllerTransaction), sync_info, coord_dev );
    if(status != busOpSuccess)
    {
        printf("[USB ]: Failed to queue transaction! Code: %u\n", status);
        return status;
    }

    status = busRunTransactions(BUS_ID_USB, full_address, NULL, coord_dev);
    if(status != busOpSuccess)
    {
        printf("[USB ]: Failed to run transactions! Code: %u\n", status);
        return status;
    }

    sync_object_wait(sync_info, 5000);

    if(sync_object_get_status(sync_info) != usb_op_success)
        return busOpIOError;

    return busOpSuccess;
}

/*! Handler for all transaction queue requests from device or interface drivers
 *  \param response Not used by this function
 *  \param resSize Not used by this function
 *  \param msg Request message outlining the transaction to be conducted
 *  \param msgSize Size of the request message
 *  \param unique_key Not used by this function
 */
busMsgResponse usb_queue_transaction(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    usbDevTransaction *request;
    const usb_handle_info *handle_info;
    uint64_t full_addr, dev_addr;
    uint32_t ep_mask, endpoints;
    request = (usbDevTransaction *)msg;

    if(msgSize < sizeof(usbDevTransaction))
        return busMsgBadFormat;
    /* Get the handle information of the provided bus handled, which contains the address and endpoints the requestor can address */
    handle_info = usb_get_bus_handle_info(request->requestor);
    if(!handle_info)
    {
        printf("[USB ]: QT - Unable to resolve bus handle: %X\n", request->requestor);
        return busMsgBadFormat;
    }

    ep_mask = 1;
    ep_mask <<= request->endpoint;

    if(handle_info->type == usb_interface_info_type)
    {
        dev_addr = handle_info->iface.parent->device.full_address;
        endpoints = handle_info->iface.endpoints;
    }
    else if(handle_info->type == usb_device_info_type)
    {
        dev_addr = handle_info->device.full_address;
        endpoints = handle_info->device.endpoints;
    }
    else
    {
        printf("[USB ]: Device handle info type corrupted for handle %X\n", request->requestor);
        return busMsgBadFormat;
    }

    /* Validate that the requestor has access to the target endpoint */
    if( !(ep_mask & endpoints) )
    {
        printf("[USB ]: Device with handle %X does not have access to endpoint %u\n", request->requestor, request->endpoint);
        return busMsgNegative;
    }

    /* Form the full address request to the controller */
    full_addr = USB_ADDR_ADD_EP_INFO(dev_addr, request->endpoint, request->direction);

    switch(request->type)
    {
        case usbTransactionBulk:
            return usb_queue_bulk_transaction(full_addr, request, response, resSize);
            break;
        case usbTransactionInterrupt:
            return usb_queue_interrupt_transaction(full_addr, request, response, resSize);
            break;
        case usbTransactionControl:
            if(!request->endpoint)
            {
                /* The default control endpoint is shared among all interfaces and thus could be contended */
                busMsgResponse control_response;
                void *lock_ptr;

                if(handle_info->type == usb_interface_info_type)
                    lock_ptr = handle_info->iface.parent->device.device_lock;
                else
                    lock_ptr = handle_info->device.device_lock;

                dapi_lock_acquire(lock_ptr);
                control_response = usb_queue_control_transaction(full_addr, request, response, resSize);

                dapi_lock_release(lock_ptr);

                return control_response;
            }
            else
                return usb_queue_control_transaction(full_addr, request, response, resSize);
            break;
        case usbTransactionIsochronous:
            return usb_queue_iso_transaction(full_addr, request, response, resSize);
            break;
    }

    return busMsgAcknowledged;
}

/*! Handles a request to run queued transactions
 *  \param response Not used
 *  \param resSize Not used
 *  \param msg Run transactions request structure
 *  \param msgSize Size of the run transactions requests
 *  \param unique_key Not used
 */
busMsgResponse usb_run_transactions(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    usbDevRunTransactionsRequest *rtr;
    const usb_handle_info *hi;
    uint64_t full_addr, req_addr;
    uint32_t ep_mask, endpoints;

    rtr = (usbDevRunTransactionsRequest *)msg;
    if(msgSize < sizeof(usbDevRunTransactionsRequest) )
        return busMsgBadFormat;

    /* Use the bus handle to construct and validate a coherent request to the controller */
    hi = usb_get_bus_handle_info(rtr->requestor);
    if(hi->type == usb_device_info_type)
    {
        req_addr = hi->device.full_address;
        endpoints = hi->device.endpoints;
    }
    else
    {
        req_addr = hi->iface.parent->device.full_address;
        endpoints = hi->iface.endpoints;
    }

    full_addr = USB_ADDR_ADD_EP_INFO(req_addr, rtr->endpoint, rtr->direction);

    ep_mask = 1;
    ep_mask <<= rtr->endpoint;
    if( !(endpoints & ep_mask) )
        return busMsgNegative;

    if( busRunTransactions(BUS_ID_USB, full_addr, NULL, coord_dev ) != busOpSuccess)
        return busMsgNegative;

    return busMsgAcknowledged;
}

static busMsgResponse usb_queue_bulk_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize)
{
    usbControllerTransaction ct;

    memset(&ct, 0, sizeof(usbControllerTransaction));

    ct.type = usbTransactionBulk;
    ct.transfer.buffer = request->transfer.buffer;
    ct.transfer.length = request->transfer.length;
    ct.flags = request->flags;

    if(!request->endpoint)
        return busMsgNegative;

    if( busQueueTransaction(BUS_ID_USB, full_address, &ct, sizeof(usbControllerTransaction), request->sync, coord_dev) != busOpSuccess)
        return busMsgNegative;

    return busMsgAcknowledged;
}

/*! Sets up an interrupt transaction for a device
 *  \param full_address Full address containing the target controller, device and endpoint
 *  \param request Transaction request information structure
 *  \param response Pointer to a response structure
 *  \param resSize Response size return pointer
 *  \return busgMsgAcknowledged if the transaction request was queued successfully, busMsgNegative otherwise
 */
static busMsgResponse usb_queue_interrupt_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize)
{
    usbControllerTransaction ct;
    int status;

    /* Initialize a coressponding controller transaction structure based on the information in the device transaction request */
    memset(&ct, 0, sizeof(usbControllerTransaction));

    ct.type = usbTransactionInterrupt;
    ct.transfer.buffer = request->transfer.buffer;
    ct.transfer.length = request->transfer.length;
    ct.flags = request->flags;

    /* Endpoint zero is always a control endpoint and as such an interrupt request wouldn't be sane */
    if(!request->endpoint)
        return busMsgNegative;

    /* Dispatch the actual request */
    status = busQueueTransaction(BUS_ID_USB, full_address, &ct, sizeof(usbControllerTransaction), request->sync, coord_dev);
    if(status != busOpSuccess)
    {
        printf("[USB ]: Failed to queue transaction @ %X EP: %u Status: %X\n", full_address, request->endpoint, status);
        return busMsgNegative;
    }

    return busMsgAcknowledged;
}

/*! Sets up a control transaction for a requesting device
 *  \param full_address Full address containing the target controller, device and endpoint
 *  \param request Transaction request information structure
 *  \param response Pointer to a response structure
 *  \param resSize Response size return pointer
 *  \return busgMsgAcknowledged if the transaction request was queued successfully, busMsgBadFormat if the request , busMsgNegative otherwise
 */
static busMsgResponse usb_queue_control_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize)
{
    usbControllerTransaction ct;
    int request_target;
    int status;

    memset(&ct, 0, sizeof(usbControllerTransaction));
    ct.control = request->control;
    memcpy(&ct.control, &request->control, sizeof(usbControlTransaction) );

    ct.type = request->type;
    ct.flags = request->flags;

    request_target = USB_REQUEST_TYPE_GET(request->control.bm_request_type);
    /* Devices and interfaces can only perform class and vendor specific requests as the USB coordinator maintains control of the rest */
    switch( request_target )
    {
        case USB_BMRT_STANDARD:
            //printf("[USB ]: Processing standard request targeting class specific function for device @ %X\n", full_address);
            if( USB_REQUEST_RECIPIENT_GET(request->control.bm_request_type) == USB_REQ_TYPE_DEVICE )
                return busMsgNegative;
            else if( USB_REQUEST_RECIPIENT_GET(request->control.bm_request_type) == USB_REQ_TYPE_OTHER )
                return busMsgNegative;

        case USB_BMRT_CLASS:
            //printf("[USB ]: Processing control request targeting class specific function for device @ %X\n", full_address);
        case USB_BMRT_VENDOR:
            status = busQueueTransaction(BUS_ID_USB, full_address, &ct, sizeof(usbControllerTransaction), request->sync, coord_dev);
            if(status != busOpSuccess)
            {
                printf("Failed to queue bus transaction on device @ %X code: %u\n", full_address, status);
                return busMsgNegative;
            }
            break;
        default:
            return busMsgBadFormat;
            break;
    }

    return busMsgAcknowledged;
}

static busMsgResponse usb_queue_iso_transaction(const uint64_t full_address, usbDevTransaction *request, void *response, uint32_t *resSize)
{
    //TODO: Finish me!
    return busMsgAcknowledged;
}

