/* Author: Joseph Kinzel */

#include <driverapi.h>
#include <dapi_container.h>
#include "new_connect.h"
#include "topo.h"
#include "transaction.h"
#include "descriptors.h"
#include <usb.h>
#include <sync.h>

typedef enum
{
    ucc_id_assigned,
    ucc_activated,
    ucc_pending_dereg,
    ucc_cleanup
} usb_coord_controller_status;

typedef struct
{
    uint8_t usb_major, usb_minor;
    uint8_t status, device_count;
    uint32_t controller_id;
} usb_coord_controller_entry;

#define USB_MAJOR_VER_SUPPORTED       3
#define USB_MINOR_VER_SUPPORTED       2

/* Bus coordinator prototypes */
busMsgResponse usb_msg_recv(const uint64_t addr, void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType, const int64_t unique_key);
busRegisterResponse usb_range_notify(const uint64_t baseAddress, const uint64_t spanLength, BUS_HANDLE range_handle, const int64_t unique_key, void **retContext);

/* Prototypes for module table */
driverOperationResult usb_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet);
driverOperationResult usb_deinit(void *context);
driverOperationResult usb_enum(void *context, void *identifyFunc);

/* Module export table */
COMPILER_MODINFO_SECTION const modExportTable moduleInfoTable = {&usb_init, &usb_deinit, &usb_enum};

/* Message processing prototypes */
busMsgResponse usb_process_controller_id_req(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
busMsgResponse usb_process_controller_dereg(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
busMsgResponse usb_process_device_connect(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
busMsgResponse usb_process_device_disconnect(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
busMsgResponse usb_process_device_error(void *response, uint32_t *resSize, void *msg, const uint32_t  msgSize, const int64_t unique_key);
busMsgResponse usb_process_enable_interface(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);
busMsgResponse usb_process_port_connect(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key);

/* Controller id and state management functions */
uint32_t usb_alloc_controller_id(const int64_t unique_key, const uint8_t usb_major, const uint8_t usb_minor);

static void *usb_coord_controller_map = NULL;
static void *usb_controller_id_lock = NULL;
static uint32_t usb_coord_controller_ids = 0xFFFFFFFFU;
DEVICE_HANDLE coord_dev;

static driverOperationResult usb_register_coordinator(DEVICE_HANDLE dev)
{
	busManagerDesc usb_coord_desc = {BUS_ID_USB, 0xFFFFFFFFU, &usb_msg_recv, &usb_range_notify};
	return registerBusCoordinator(&usb_coord_desc, dev);
}

/* Module initialization function */
driverOperationResult usb_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet)
{
	driverOperationResult result;

	printf("[USB ]: Initializing USB Coordinator...\n");

    coord_dev = dev;
    usb_controller_id_lock = dapi_lock_create();

	/* Create the controller hash table */
	usb_coord_controller_map = dapi_map_create();
    if(!usb_coord_controller_map)
        return driverOpNoMemory;

    result = usb_register_coordinator(dev);
	if(result != driverOpSuccess)
		return result;


    result = registerDriverDatabase("/initrd/drivers/bus_usb.dbase", BUS_ID_USB, coord_dev);
    if(result != driverOpSuccess)
    {
        printf("[USB ]: Failed to load driver database file.  Code: %u\n", result);
        return result;
    }

    if( usb_init_bh_table() != driverOpSuccess)
    {
        printf("[USB ]: Failed allocated bus handle mapping table!\n");
        return driverOpNoMemory;
    }

    printf("[USB ]: USB Coordinator initialized!\n");
	return driverOpSuccess;
}

/* Module deinit/unload function */
driverOperationResult usb_deinit(void *context)
{
    dapi_map_destroy(usb_coord_controller_map);
    usb_destroy_bh_table();
    return driverOpSuccess;
}

/* Enumeration function */
driverOperationResult usb_enum(void *context, void *identifyFunc)
{
    return driverOpSuccess;
}

static busMsgResponse usb_debug_endpoint(const uint64_t addr, void *response, uint32_t *resSize, void *msg, const uint32_t msgSize)
{
    usb_request_debug_ep_stop *request;
    uint8_t device_endpoint;
    uint32_t ep_mask;
    uint64_t device_addr;
    const usb_handle_info *hi;
    usbConfigureAddress ca;

    if(msgSize < sizeof(usb_request_debug_ep_stop))
        return busMsgBadFormat;

    request = (usb_request_debug_ep_stop *)msg;

    ep_mask = 1;
    device_endpoint = USB_ADDR_GET_ENDPOINT(addr);
    device_addr = USB_ADDR_FULL_DEVICE(addr);

    ep_mask <<= device_endpoint;

    hi = usb_get_bus_handle_info(request->requestor);
    if(!hi)
        return busMsgBadFormat;

    switch(hi->type)
    {
        case usb_device_info_type:
            if( hi->device.full_address != device_addr)
                return busMsgBadFormat;

            if( !(hi->device.endpoints & ep_mask ))
                return busMsgBadFormat;

            break;
        case usb_interface_info_type:
            if (hi->iface.parent->device.full_address != device_addr)
                return busMsgBadFormat;
            if( !(hi->iface.endpoints & ep_mask))
                return busMsgBadFormat;
            break;
        default:
            return busMsgBadFormat;
            break;
    }

    memset(&ca, 0, sizeof(usbConfigureAddress));

    ca.command_type = usbConfigDebugStopEndpoint;
    ca.device_info.value = request->flags;

    if( busConfigAddress(BUS_ID_USB, addr, &ca, sizeof(usbConfigureAddress), request->sync, coord_dev) != busOpSuccess)
        return busMsgNegative;


    return busMsgAcknowledged;
}

static busMsgResponse usb_process_debug_request(const uint64_t addr, void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType, const int64_t unique_key)
{
    switch(msgType)
    {
        case usbCoordDebugEndpoint:
            return usb_debug_endpoint(addr, response, resSize, msg, msgSize);
            break;
        default:
            return busMsgNotSupported;
            break;
    }
}



/* Bus coordinator message receiver function */
busMsgResponse usb_msg_recv(const uint64_t addr, void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType, const int64_t unique_key)
{
	if(!msg || !resSize || !msgSize )
		return busMsgBadFormat;

	switch(msgType)
	{
		case usbCoordControllerIdRequest:
			return usb_process_controller_id_req(response, resSize, msg, msgSize, unique_key);
			break;
		case usbCoordControllerDereg:
		    return usb_process_controller_dereg(response, resSize, msg, msgSize, unique_key);
			break;
        case usbCoordPortConnect:
            return usb_process_port_connect(response, resSize, msg, msgSize, unique_key);
            break;
		case usbCoordDeviceConnect:
		    return usb_process_device_connect(response, resSize, msg, msgSize, unique_key);
			break;
		case usbCoordDeviceDisconnect:
		    return usb_process_device_disconnect(response, resSize, msg, msgSize, unique_key);
			break;
		case usbCoordDeviceError:
		    return usb_process_device_error(response, resSize, msg, msgSize, unique_key);
			break;
        case usbCoordTransactionQueue:
            return usb_queue_transaction(response, resSize, msg, msgSize, unique_key);
            break;
        case usbCoordTransactionRun:
            return usb_run_transactions(response, resSize, msg, msgSize, unique_key);
            break;
        case usbCoordDecodeEndpointDesc:
            return usb_decode_endpoint_desc(response, resSize, msg, msgSize, unique_key);
            break;
        case usbCoordGetStatus:
            return usb_get_status(response, resSize, msg, msgSize, unique_key);
            break;
        case usbCoordGetDescriptor:
            return usb_get_desc_request(response, resSize, msg, msgSize);
            break;
        case usbCoordRequestConfig:
            return usb_request_config(response, resSize, msg, msgSize, unique_key);
        case usbCoordRequestAltInterface:
            return usb_req_alt_iface(response, resSize, msg, msgSize, unique_key);
		default:
		    if(msgType >= usbCoordDebugRequestStart)
                return usb_process_debug_request(addr, response, resSize, msg, msgSize, msgType, unique_key);
		    else
                return busMsgBadFormat;
			break;
	}

	return busMsgAcknowledged;
}

/* USB Bus controller registration handler */
busRegisterResponse usb_range_notify(const uint64_t baseAddress, const uint64_t spanLength, BUS_HANDLE range_handle, const int64_t unique_key, void **retContext)
{
    usb_coord_controller_entry *controller_entry;
    usb_handle_info *h_info;

    controller_entry = (usb_coord_controller_entry *)dapi_map_get(usb_coord_controller_map, unique_key);
    if(!controller_entry || (baseAddress%USB_MAX_ADDRESS) || (spanLength > USB_MAX_ADDRESS) )
    {
        printf("[USB ]: Unable to find matching map entry for controller!\n");

        return busRegisterBlocked;
    }
    if( (baseAddress/USB_MAX_ADDRESS) != controller_entry->controller_id )
    {
        printf("[USB ]: Range allocation controller id mismatch! Expected: %X Found: %X\n", controller_entry->controller_id, baseAddress%USB_MAX_ADDRESS);
        return busRegisterBlocked;
    }

    if(controller_entry->status != ucc_id_assigned)
        return busRegisterBlocked;

    h_info = (usb_handle_info *)calloc(1, sizeof(usb_handle_info));
    h_info->type = usb_controller_info_type;

    usb_register_bus_device(h_info, range_handle);

    controller_entry->status = ucc_activated;
    *retContext = controller_entry;

	return busRegisterApproved;
}

/* Controller id request command */
busMsgResponse usb_process_controller_id_req(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    const void *existing_entry;
    controller_id_response *id_response;
    controller_id_req_msg *req = (controller_id_req_msg *)msg;

    id_response = (controller_id_response *)response;

    if(*resSize < sizeof(controller_id_response) )
    {
        *resSize = sizeof(controller_id_response);
        return busMsgNoSpace;
    }

    if(!req->usb_major)
    {
        return busMsgBadFormat;
    }

    /* Check if the bus coordinator can support the controller */
    if(req->usb_major > USB_MAJOR_VER_SUPPORTED)
        return busMsgNotSupported;
    else if(req->usb_major == USB_MAJOR_VER_SUPPORTED)
        id_response->coord_usb_minor = USB_MINOR_VER_SUPPORTED;
    else
        id_response->coord_usb_minor = req->usb_minor;

    /* The response should contain the lesser of the reported version of the controller or the maximum supported version of this driver */
    id_response->coord_usb_major = req->usb_major;

    existing_entry = dapi_map_get( usb_coord_controller_map, unique_key);
    if(existing_entry)
    {
        printf("[USB ]: Controller requested too many ids...\n");
        return busMsgNegative;
    }

    if( (id_response->controller_id = usb_alloc_controller_id(unique_key, id_response->coord_usb_major, id_response->coord_usb_minor)) )
    {

        printf("[USB ]: ID %u allocated...\n", id_response->controller_id);
        return busMsgAffirmative;
    }
    else
        return busMsgNegative;
}

busMsgResponse usb_process_controller_dereg(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    //TODO: FINISH ME!
    return busMsgNoAck;
}

busMsgResponse usb_process_device_disconnect(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    /* TODO: IMPLEMENT ME! */
    return busMsgNoAck;
}

busMsgResponse usb_process_device_error(void *response, uint32_t *resSize, void *msg, const uint32_t  msgSize, const int64_t unique_key)
{
    /* TODO: IMPLEMENT ME! */
    return busMsgNoAck;
}

/*! Allocates a new ID for a bus controller
 *  \param unique_key Unique key between the controller and bus coordinator
 *  \param usb_major USB major version supported
 *  \param usb_minor USB Minor version supported
 *  \return A new id for the controller or zero if no ID could be allocated
 */
uint32_t usb_alloc_controller_id(const int64_t unique_key, const uint8_t usb_major, const uint8_t usb_minor)
{
    usb_coord_controller_entry *entry;
    int32_t id;
    uint32_t mask;

    entry = (usb_coord_controller_entry *)malloc(sizeof(usb_coord_controller_entry));


    dapi_lock_acquire(usb_controller_id_lock);

    id = getLsb32(usb_coord_controller_ids);

    if(id < 0)
    {
        free(entry);
        return 0;
    }

    mask = 1;
    mask <<= id;
    usb_coord_controller_ids &= ~mask;

    entry->controller_id = id+1;
    entry->status = ucc_id_assigned;
    entry->usb_major = usb_major;
    entry->usb_minor = usb_minor;
    entry->device_count = 0;

    dapi_map_insert(usb_coord_controller_map, unique_key, entry);

    dapi_lock_release(usb_controller_id_lock);

    return (id+1);
}

/*! New device connection handler
 *  \param response Response message buffer pointer
 *  \param resSize Response message buffer size
 *  \param msg Incoming message buffer
 *  \param msgSize Size of incoming message
 *  \param unique_key Unique key between the sender and receiver
 *
 *  \return busMsgBadFormat if the incoming message is corrupt
 */
busMsgResponse usb_process_device_connect(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    /* TODO: Add device identification and addressing  */
    device_connect_msg *dc_msg;
    const usb_coord_controller_entry *controller;

    if(!msg || (msgSize < sizeof(device_connect_msg)))
        return busMsgBadFormat;

    dc_msg = (device_connect_msg *)msg;

    /* Validate the controller ID  */
    if ( (controller = dapi_map_get(usb_coord_controller_map, unique_key) ) )
    {
        if(dc_msg->controller_id != controller->controller_id)
        {
            printf("[USB ]: Device connect - Controller ID mismatch detected!\n");
            return busMsgBadFormat;
        }

        if(dc_msg->flags & USB_NEW_CONNECT_ADDRESSED)
        {
            /* Spawn a task that can wait on I/O on its own */
            dapi_spawn_task(&usb_device_connect_task, dc_msg);
        }
        else
            return busMsgBadFormat;

        return busMsgAcknowledged;
    }
    else
    {
        printf("[USB ]: Unable to find corresponding controller for new device request!\n");
        return busMsgNoAck;
    }
}

/*! New port connection handler
 *  \param response Response message buffer pointer
 *  \param resSize Response message buffer size
 *  \param msg Incoming message buffer
 *  \param msgSize Size of incoming message
 *  \param unique_key Unique key between the sender and receiver
 *
 *  \return busMsgBadFormat if the incoming message is corrupt
 */
busMsgResponse usb_process_port_connect(void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const int64_t unique_key)
{
    uint64_t full_addr;
    usb_port_connect_msg *pc_msg;
    const usb_handle_info *hi;

    if(!msg || (msgSize < sizeof(usb_port_connect_msg)))
        return busMsgBadFormat;

    pc_msg = (usb_port_connect_msg *)msg;

    /* Get the full USB address of the device/interface making the request */
    full_addr = usb_get_addr_from_handle(pc_msg->parent_handle, &hi);

    if(!full_addr)
        return busMsgBadFormat;

    if(pc_msg->controller_id != (full_addr/USB_MAX_ADDRESS))
        return busMsgBadFormat;

    if(hi->type != usb_device_info_type)
        return busMsgBadFormat;

    if(!hi->device.hub_info)
        return busMsgBadFormat;

    dapi_spawn_task(&usb_port_connect_task, pc_msg);

    return busMsgAcknowledged;
}

