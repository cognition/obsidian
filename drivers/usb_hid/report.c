/* HID Report parsing logic
 *
 * Author: Joseph Kinzel */

#include <driverapi.h>
#include <dapi_container.h>
#include "hid_const.h"
#include "hid_struct.h"
#include "report.h"

/*! Destroys a global state stack
 *  \param global_stack Stack to destroy, may be null
 */
static void hid_clear_report_stack(hid_report_state *report_stack)
{
    hid_report_state *report_item = report_stack;

    while(report_item)
    {
            hid_report_state *prev = report_item->prev;

            free(report_item);

            report_item = prev;
    }
}


/*! Allocates and initializes a new report state, copying an existing state if provided
 *  \param prior Prior state to copy or NULL for the initial state
 *  \return A pointer to the new state
 */
static hid_report_state *hid_create_report_state(hid_report_state *prior)
{
    hid_report_state *state = (hid_report_state *)malloc( sizeof(hid_report_state) );
    if(prior)
    {
        memcpy(state, prior, sizeof(hid_report_state));
        state->prev = prior;
    }
    else
        memset(state, 0, sizeof(hid_report_state));

    return state;
}

typedef struct _id_queue_entry
{
    struct _id_queue_entry *prev;
    uint32_t id;
} id_queue_entry;

/*! Processes an input/output/feature field
 *  \param Chain of items describing attributes of the field
 *  \param report_id_map Map containing all known report ids
 *  \param report_id_field Flag indicating if there is a report id associated with reports from this device
 *  \return Size of the field or report in bytes
 */
static int hid_process_iof_chain(hid_iof_item *chain, void *report_id_map, const int report_id_field)
{
    int max_report_bytes;
    id_queue_entry *ids;

    ids = NULL;
    max_report_bytes = 0;

    if(!chain)
        return 0;

    /* Iterate through the subfields in the chain and combine them into id specific chains */
    for(hid_iof_item *current_item = chain; current_item; )
    {
        hid_report_package *package;
        hid_iof_item *prev = current_item->prev;

        package = (hid_report_package *)dapi_map_get(report_id_map, current_item->state.global.report_id);
        if(!package)
        {
            id_queue_entry *entry;

            package = (hid_report_package *)calloc(1, sizeof(hid_report_package) );
            dapi_map_insert(report_id_map, current_item->state.global.report_id, package);

            entry = (id_queue_entry *)calloc(1, sizeof(id_queue_entry));
            entry->id = current_item->state.global.report_id;
            entry->prev = ids;
            ids = entry;
        }

        current_item->next = package->components;
        current_item->prev = NULL;

        package->components = current_item;

        current_item = prev;
    }

    /* Iterate through the report ids and calculate the length of each report */
    for(id_queue_entry *current_report = ids; current_report; )
    {
        id_queue_entry *prior;
        hid_report_package *package;
        int report_bits, report_bytes;

        prior = current_report->prev;
        report_bits = 0;

        package = (hid_report_package *)dapi_map_get(report_id_map, current_report->id);
        if(!package)
            return -1;

        /* Iterate through each report's chain of fields and calculate the total number of bits that constitute the report */
        for(hid_iof_item *report_field = package->components; report_field; report_field = report_field->next)
        {
            report_bits += report_field->state.global.report_count*report_field->state.global.report_size;
        }

        /* Calculate the number of bytes needed to hold the report */
        package->report_bits = report_bits;
        report_bytes = report_bits/8;
        if(report_bits%8)
            report_bytes++;

        /* Add an extra byte if there's multiple reports and an included report id value field in all of them */
        report_bytes += report_id_field;

        package->report_bytes = report_bytes;
        if(report_bytes > max_report_bytes)
            max_report_bytes = report_bytes;

        current_report = prior;
    }

    return max_report_bytes;
}

/*! Groups all the report fields by type
 *  \param parser_state State of parser
 *  \param results Probably grouped fields
 *  \return driverOpSuccess if successful or driverOpBadState if the parser state is corrupted
 */
static driverOperationResult hid_group_reports(hid_report_parse_state *parser_state, hid_report_result *results)
{
    int maximum_report_bytes, report_field_size;

    results->input_reports = dapi_map_create();
    results->output_reports = dapi_map_create();
    results->feature_reports = dapi_map_create();

    report_field_size = ((parser_state->flags & HID_REPORT_ID_PRESENT) ? 1:0);

    /* Process all the item chains for the major overall report types of input, output and features */
    maximum_report_bytes = hid_process_iof_chain(parser_state->input_fields, results->input_reports, report_field_size);
    if(maximum_report_bytes < 0)
        return driverOpBadState;

    results->max_input_report_size = maximum_report_bytes;

    maximum_report_bytes = hid_process_iof_chain(parser_state->output_fields, results->output_reports, report_field_size);
    if(maximum_report_bytes < 0)
        return driverOpBadState;

    maximum_report_bytes = hid_process_iof_chain(parser_state->feature_fields, results->feature_reports, report_field_size);
    if(maximum_report_bytes < 0)
        return driverOpBadState;

    return driverOpSuccess;
}

typedef union
{
    uint8_t byte;
    uint16_t word;
    uint32_t dword;
} multi_ptr;

/*! Decodes a global type report item
 *  \param state_stack Return pointer to the current report state stack
 *  \param parse_state Current parser state
 *  \param tag Current item tag value
 *  \param data_value Current item data value
 *  \return driverOpSuccess if successful or driverOpBadState if tag was unrecognized
 */
static driverOperationResult hid_decode_global_item(hid_report_state **state_stack, hid_report_parse_state *parse_state, const uint8_t tag, const uint32_t data_value)
{
    hid_report_state *current_report_state;

    current_report_state = *state_stack;

    if(!current_report_state)
        return driverOpBadState;

    switch(tag)
    {
        case HID_GLOBAL_TAG_USAGE_PAGE:
            current_report_state->global.usage_page = data_value & 0xFFFFU;
            break;
        case HID_GLOBAL_TAG_LOGICAL_MIN:
            current_report_state->global.logical_minimum = data_value;
            break;
        case HID_GLOBAL_TAG_LOGICAL_MAX:
            current_report_state->global.logical_maximum = data_value;
            break;
        case HID_GLOBAL_TAG_PHYS_MIN:
            current_report_state->global.physical_minimum = data_value;
            break;
        case HID_GLOBAL_TAG_PHYS_MAX:
            current_report_state->global.physical_maximum = data_value;
            break;
        case HID_GLOBAL_TAG_UNIT_EX:
            current_report_state->global.unit_ex = data_value;
            break;
        case HID_GLOBAL_TAG_UNIT:
            current_report_state->global.unit = data_value;
            break;
        case HID_GLOBAL_TAG_REPORT_SIZE:
            current_report_state->global.report_size = data_value;
            break;
        case HID_GLOBAL_TAG_REPORT_ID:
            current_report_state->global.report_id = data_value;
            parse_state->flags |= HID_REPORT_ID_PRESENT;
            break;
        case HID_GLOBAL_TAG_REPORT_COUNT:
            current_report_state->global.report_count = data_value;
            break;
        case HID_GLOBAL_TAG_PUSH:
            *state_stack = hid_create_report_state(current_report_state);
            break;
        case HID_GLOBAL_TAG_POP:
            if(!current_report_state->prev)
                return driverOpBadState;
            *state_stack = current_report_state->prev;
            current_report_state->prev = NULL;
            free(current_report_state);
            break;
        default:
            printf("[HID ]: Unrecognized global tag type: %u\n");
            return driverOpBadState;
            break;
    }

    return driverOpSuccess;
}

/*! Decodes an attribute of the local tag subtype
 *
 *  \param report_state Current state of the report being parsed
 *  \param tag Value indicating the type of tag
 *  \param data_value Tag-specific data portion of the attribute
 *  \return driverOpSuccess if successful or driverOpBadState if an unrecognized tag value was encountered.
 */
static driverOperationResult hid_decode_local_item(hid_report_state *report_state, const uint8_t tag, const uint32_t data_value)
{
    switch(tag)
    {
        case HID_LOCAL_TAG_USAGE:
            report_state->local.usage = data_value;
            break;
        case HID_LOCAL_TAG_USAGE_MIN:
            report_state->local.usage_min = data_value;
            break;
        case HID_LOCAL_TAG_USAGE_MAX:
            report_state->local.usage_max = data_value;
            break;
        case HID_LOCAL_TAG_DESIG_INDEX:
            report_state->local.desig_index = data_value;
            break;
        case HID_LOCAL_TAG_DESIG_MIN:
            report_state->local.desig_min = data_value;
            break;
        case HID_LOCAL_TAG_DESIG_MAX:
            report_state->local.desig_max = data_value;
            break;
        case HID_LOCAL_TAG_STRING_INDEX:
            report_state->local.string_index = data_value;
            break;
        case HID_LOCAL_TAG_STRING_MIN:
            report_state->local.string_min = data_value;
            break;
        case HID_LOCAL_TAG_STRING_MAX:
            report_state->local.string_max = data_value;
            break;
        case HID_LOCAL_TAG_DELIMITER:
            report_state->local.delimiter = data_value;
            break;
        default:
            printf("[HID ]: Unrecognized local tag type: %u\n", tag);
            return driverOpBadState;
            break;
    }

    return driverOpSuccess;
}

/*! Decodes a main-subtype attribute
 *
 *  \param report_state Current state of the report being parsed
 *  \param tag Tag value indicating the specific type of attribute
 *  \param data_value Data portion of the attribute
 *  \param parse_state Parser state information
 *  \return driverOpSuccess if successful or driverOpBadState if an unrecognized tag was encountered.
 */
driverOperationResult hid_decode_main_item(hid_report_state *report_state, const uint8_t tag, const uint32_t data_value, hid_report_parse_state *parse_state)
{
    hid_iof_item *iof_item;

    switch(tag)
    {
        case HID_MAIN_INPUT:
            iof_item = (hid_iof_item *)malloc(sizeof(hid_iof_item));
            memcpy(&iof_item->state, report_state, sizeof(hid_report_state));

            iof_item->prev = parse_state->input_fields;
            iof_item->next = NULL;
            iof_item->flags = 0;

            parse_state->input_fields = iof_item;

            break;
        case HID_MAIN_OUTPUT:
            iof_item = (hid_iof_item *)malloc(sizeof(hid_iof_item));
            memcpy(&iof_item->state, report_state, sizeof(hid_report_state));

            iof_item->prev = parse_state->output_fields;
            iof_item->next = NULL;
            iof_item->flags = 0;

            parse_state->output_fields = iof_item;

            break;
        case HID_MAIN_FEATURE:
            iof_item = (hid_iof_item *)malloc(sizeof(hid_iof_item));
            memcpy(&iof_item->state, report_state, sizeof(hid_report_state));

            iof_item->prev = parse_state->feature_fields;
            iof_item->next = NULL;
            iof_item->flags = 0;

            parse_state->feature_fields = iof_item;

            break;
        /* TODO: Properly implement collections */
        case HID_MAIN_COLLECTION:
            break;
        case HID_MAIN_COLLECTION_END:
            break;
        default:
            printf("[HID ]: Failed to parse main item!  Unknown tag: %u\n", tag);
            return driverOpBadState;
            break;
    }

    memset(&report_state->local, 0, sizeof(hid_local_state));

    return driverOpSuccess;
}

/*! Parses the report descriptor of an HID device
 *  \param dc HID Interface context structure for the device providing the descriptor
 *  \param report Report descriptor data buffer
 *  \param report_length Size of the report in bytes
 *  \param result Return pointer for the resulting parsed report information
 *  \return driverOpSuccess if successful or driverOpBadState if the report could not be properly parsed
 */
int hid_parse_report_desc(hid_dev_context *dc, volatile uint8_t *report, const uint16_t report_length, hid_report_result *result)
{
    driverOperationResult status;
    hid_report_parse_state parse_state;
    hid_report_state *report_state;

    report_state = hid_create_report_state(NULL);
    memset(&parse_state, 0, sizeof(hid_report_parse_state));

    for(int report_index = 0, rem_size = report_length; report_index < report_length; )
    {
        if(report[report_index] == HID_LI_INFO_VAL)
        {
            /* NOTE: No long item tag values are actually defined in the HID spec as of 1.11 but might be at some point in the future.
               Vendor specific tag items are technically valid but also something this generic driver is ultimately ignorant of.
             */
            int li_length;
            volatile hid_long_item *li;

            li = (volatile hid_long_item *)&report[report_index];

            li_length = li->bDataSize + sizeof(hid_long_item);
            if(li->bLongItemTag < HID_LI_LONG_TAG_VENDOR_START)
            {
                printf("[HID ]: Error invalid long item tag type encountered! Value: %X\n", li->bLongItemTag);
                hid_clear_report_stack(report_state);
                return driverOpBadState;
            }

            report_index += li_length;
            rem_size -= li_length;
        }
        else
        {
            /* Short style tag, the only ones the specification currently defines */
            int si_length;
            uint8_t tag, data_size, type;
            uint32_t data_value;
            volatile multi_ptr *global_data_ptr;

            /* Extract the size and calculate the total length of the short item in order to advance the parser */
            global_data_ptr = (volatile multi_ptr *)(&report[report_index + 1]);
            data_size = HID_SI_GET_SIZE(report[report_index]);
            si_length = ((data_size == HID_SI_SIZE_4) ? (data_size+2):(data_size+1));

            if(rem_size < si_length)
            {
                printf("[HID ]: Short item length goes beyond maximum of report! Short item length: %u Remaining bytes: %u\n", si_length, rem_size);
                hid_clear_report_stack(report_state);
                return driverOpBadState;
            }

            /* Get the data portion of the item */
            switch(data_size)
            {
                case HID_SI_SIZE_0:
                    data_value = 0;
                    break;
                case HID_SI_SIZE_1:
                    data_value = global_data_ptr->byte;
                    break;
                case HID_SI_SIZE_2:
                    data_value = global_data_ptr->word;
                    break;
                case HID_SI_SIZE_4:
                    data_value = global_data_ptr->dword;
                    break;
                default:
                    return driverOpBadState;
                    break;
            }

            tag = HID_SI_GET_TAG(report[report_index]);
            type = HID_SI_GET_TYPE(report[report_index]);

            /* Decode the tag and data value based upon the tag sub-type */
            switch( type )
            {
                case HID_SI_TYPE_MAIN:
                    status = hid_decode_main_item(report_state, tag, data_value, &parse_state);
                    if(status != driverOpSuccess)
                    {
                        printf("[HID ]: Failed to parse report descriptor for device @ %X:%u - main item offset %u\n", dc->full_address, dc->interface_num, report_index);
                        hid_clear_report_stack(report_state);
                        return status;
                    }
                    break;
                case HID_SI_TYPE_GLOBAL:
                    status = hid_decode_global_item(&report_state, &parse_state, tag, data_value);
                    if(status != driverOpSuccess)
                    {
                        printf("[HID ]: Failed to parse report descriptor for device @ %X:%u - global item offset %u\n", dc->full_address, dc->interface_num, report_index);
                        hid_clear_report_stack(report_state);
                        return status;
                    }
                    break;
                case HID_SI_TYPE_LOCAL:
                    status = hid_decode_local_item(report_state, tag, data_value);
                    if(status != driverOpSuccess)
                    {
                        printf("[HID ]: Failed to parse report descriptor for device @ %X:%u - local item offset %u\n", dc->full_address, dc->interface_num, report_index);
                        hid_clear_report_stack(report_state);
                        return status;
                    }
                    break;
                case HID_SI_TYPE_RESERVED:
                    break;
            }

            report_index += si_length;
            rem_size -= si_length;
        }

    }

    /* Indicate whether or not a report ID will be present in the results */
    if(parse_state.flags & HID_REPORT_ID_PRESENT)
    {
        result->flags |= HID_REPORT_ID_PRESENT;
    }

    status = hid_group_reports(&parse_state, result);
    if(status != driverOpSuccess)
    {
        printf("[HID ]: Error grouping reports on device @ %X:%u Code: %u\n", dc->full_address, dc->interface_num, status);
        return status;
    }

    printf("[HID ]: Report parsing completed! Input report size: %u\n", result->max_input_report_size);
    hid_clear_report_stack(report_state);
    return driverOpSuccess;
}

