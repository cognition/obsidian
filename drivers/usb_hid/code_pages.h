/* Author: Joseph Kinzel */

#ifndef USB_HID_CODE_PAGE_H_
#define USB_HID_CODE_PAGE_H_

#include <compiler.h>

typedef struct COMPILER_PACKED
{
	uint16_t unicode, alt_unicode, event_code;

} code_page_entry;

/* Keyboard page (0x7 codes) */
#define HID_KB_RESERVED                    0x00
#define HID_KB_ERROR_ROLLOVER              0x01
#define HID_KB_POST_FAIL                   0x02
#define HID_KB_ERROR_UNDEF                 0x03

#define HID_KB_KEY_A                       0x04
#define HID_KB_KEY_B                       0x05
#define HID_KB_KEY_C                       0x06
#define HID_KB_KEY_D                       0x07
#define HID_KB_KEY_E                       0x08
#define HID_KB_KEY_F                       0x09
#define HID_KB_KEY_G                       0x0A
#define HID_KB_KEY_H                       0x0B
#define HID_KB_KEY_I                       0x0C
#define HID_KB_KEY_J                       0x0D
#define HID_KB_KEY_K                       0x0E
#define HID_KB_KEY_L                       0x0F
#define HID_KB_KEY_M                       0x10
#define HID_KB_KEY_N                       0x11
#define HID_KB_KEY_O                       0x12
#define HID_KB_KEY_P                       0x13
#define HID_KB_KEY_Q                       0x14
#define HID_KB_KEY_R                       0x15
#define HID_KB_KEY_S                       0x16
#define HID_KB_KEY_T                       0x17
#define HID_KB_KEY_U                       0x18
#define HID_KB_KEY_V                       0x19
#define HID_KB_KEY_W                       0x1A
#define HID_KB_KEY_X                       0x1B
#define HID_KB_KEY_Y                       0x1C
#define HID_KB_KEY_Z                       0x1D

#define HID_KB_KEY_1_EXCLAIM               0x1E
#define HID_KB_KEY_2_AT                    0x1F
#define HID_KB_KEY_3_HASH                  0x20
#define HID_KB_KEY_4_DOLLAR                0x21
#define HID_KB_KEY_5_PERCENT               0x22
#define HID_KB_KEY_6_CARAT                 0x23
#define HID_KB_KEY_7_AND                   0x24
#define HID_KB_KEY_8_ASTERISK              0x25
#define HID_KB_KEY_9_LEFT_PAREN            0x26
#define HID_KB_KEY_0_RIGHT_PAREN           0x27

#define HID_KB_KEY_RETURN                  0x28
#define HID_KB_KEY_ESCAPE                  0x29
#define HID_KB_KEY_DELETE                  0x2A
#define HID_KB_KEY_TAB                     0x2B
#define HID_KB_KEY_SPACEBAR                0x2C
#define HID_KB_KEY_DASH_UNDER              0x2D
#define HID_KB_KEY_EQUAL_PLUS              0x2E
#define HID_KB_KEY_LEFT_BRACKET_BRACE      0x2F
#define HID_KB_KEY_RIGHT_BRACKET_BRACE     0x30
#define HID_KB_KEY_SLASH_PIPE              0x31
#define HID_KB_KEY_TILDE                   0x32
#define HID_KB_KEY_SEMI_COLON              0x33
#define HID_KB_KEY_QUOTES                  0x34
#define HID_KB_KEY_GRAVE_TILDE             0x35
#define HID_KB_KEY_COMMA_LESS_THAN         0x36
#define HID_KB_KEY_PERIOD_GREATER_THAN     0x37
#define HID_KB_KEY_BACKSLASH_QUESTION      0x38
#define HID_KB_KEY_CAPSLOCK                0x39

#define HID_KB_KEY_F1                      0x3A
#define HID_KB_KEY_F2                      0x3B
#define HID_KB_KEY_F3                      0x3C
#define HID_KB_KEY_F4                      0x3D
#define HID_KB_KEY_F5                      0x3E
#define HID_KB_KEY_F6                      0x3F
#define HID_KB_KEY_F7                      0x40
#define HID_KB_KEY_F8                      0x41
#define HID_KB_KEY_F9                      0x42
#define HID_KB_KEY_F10                     0x43
#define HID_KB_KEY_F11                     0x44
#define HID_KB_KEY_F12                     0x45

#define HID_KB_KEY_PRINTSCREEN             0x46
#define HID_KB_KEY_SCROLL_LOCK             0x47
#define HID_KB_KEY_PAUSE                   0x48
#define HID_KB_KEY_INSERT                  0x49
#define HID_KB_KEY_HOME                    0x4A
#define HID_KB_KEY_PAGE_UP                 0x4B
#define HID_KB_KEY_DELETE_FORWARD          0x4C
#define HID_KB_KEY_END                     0x4D
#define HID_KB_KEY_PAGE_DOWN               0x4E

#define HID_KB_KEY_RIGHT_ARROW             0x4F
#define HID_KB_KEY_LEFT_ARROW              0x50
#define HID_KB_KEY_DOWN_ARROW              0x51
#define HID_KB_KEY_UP_ARROW                0x52

#define HID_KB_KEY_NUM_LOCK                0x53
#define HID_KB_KEY_KEYPAD_DIV              0x54
#define HID_KB_KEY_KEYPAD_MULT             0x55
#define HID_KB_KEY_KEYPAD_MINUS            0x56
#define HID_KB_KEY_KEYPAD_PLUS             0x57
#define HID_KB_KEY_KEYPAD_ENTER            0x58
#define HID_KB_KEY_KEYPAD_1_END            0x59
#define HID_KB_KEY_KEYPAD_2_DOWN_ARR       0x5A
#define HID_KB_KEY_KEYPAD_3_PAGE_DOWN      0x5B
#define HID_KB_KEY_KEYPAD_4_LEFT_ARR       0x5C
#define HID_KB_KEY_KEYPAD_5                0x5D
#define HID_KB_KEY_KEYPAD_6_RIGHT_ARR      0x5E
#define HID_KB_KEY_KEYPAD_7_HOME           0x5F
#define HID_KB_KEY_KEYPAD_8_UP_ARR         0x60
#define HID_KB_KEY_KEYPAD_9_PAGE_UP        0x61
#define HID_KB_KEY_KEYPAD_0_INSERT         0x62
#define HID_KB_KEY_KEYPAD_DOT_DEL          0x63
#define HID_KB_KEY_NON_US_BACKSLASH_PIPE   0x64
#define HID_KB_KEY_APP                     0x65
#define HID_KB_KEY_POWER                   0x66
#define HID_KB_KEY_KEYPAD_EQUAL            0x67

#define HID_KB_KEY_F13                     0x68
#define HID_KB_KEY_F14                     0x69
#define HID_KB_KEY_F15                     0x6A
#define HID_KB_KEY_F16                     0x6B
#define HID_KB_KEY_F17                     0x6C
#define HID_KB_KEY_F18                     0x6D
#define HID_KB_KEY_F19                     0x6E
#define HID_KB_KEY_F20                     0x6F
#define HID_KB_KEY_F21                     0x70
#define HID_KB_KEY_F22                     0x71
#define HID_KB_KEY_F23                     0x72
#define HID_KB_KEY_F24                     0x73

#define HID_KB_KEY_EXECUTE                 0x74
#define HID_KB_KEY_HELP                    0x75
#define HID_KB_KEY_MENU                    0x76
#define HID_KB_KEY_SELECT                  0x77
#define HID_KB_KEY_STOP                    0x78
#define HID_KB_KEY_AGAIN                   0x79
#define HID_KB_KEY_UNDO                    0x7A
#define HID_KB_KEY_CUT                     0x7B
#define HID_KB_KEY_COPY                    0x7C
#define HID_KB_KEY_PASTE                   0x7D
#define HID_KB_KEY_FIND                    0x7E
#define HID_KB_KEY_MUTE                    0x7F
#define HID_KB_KEY_VOLUME_UP               0x80
#define HID_KB_KEY_VOLUME_DOWN             0x81
#define HID_KB_KEY_LOCKING_CAPS            0x82
#define HID_KB_KEY_LOCKING_NUM             0x83
#define HID_KB_KEY_LOCKING_SCROLL          0x84

#define HID_KB_KEY_KEYPAD_COMMA            0x85
#define HID_KB_KEY_KEY_ES                  0x86
#define HID_KB_KEY_INT_1                   0x87
#define HID_KB_KEY_INT_2                   0x88
#define HID_KB_KEY_INT_3                   0x89
#define HID_KB_KEY_INT_4                   0x8A
#define HID_KB_KEY_INT_5                   0x8B
#define HID_KB_KEY_INT_6                   0x8C
#define HID_KB_KEY_INT_7                   0x8D
#define HID_KB_KEY_INT_8                   0x8E
#define HID_KB_KEY_INT_9                   0x8F

#define HID_KB_KEY_LANG_1                  0x90
#define HID_KB_KEY_LANG_2                  0x91
#define HID_KB_KEY_LANG_3                  0x92
#define HID_KB_KEY_LANG_4                  0x93
#define HID_KB_KEY_LANG_5                  0x94
#define HID_KB_KEY_LANG_6                  0x95
#define HID_KB_KEY_LANG_7                  0x96
#define HID_KB_KEY_LANG_8                  0x97
#define HID_KB_KEY_LANG_9                  0x98
#define HID_KB_KEY_ALT_ERASE               0x99
#define HID_KB_KEY_SYS_REQ                 0x9A
#define HID_KB_KEY_CANCEL                  0x9B
#define HID_KB_KEY_CLEAR                   0x9C
#define HID_KB_KEY_PRIOR                   0x9D
#define HID_KB_KEY_RETURN_ALT              0x9E
#define HID_KB_KEY_SEPERATOR               0x9F
#define HID_KB_KEY_OUT                     0xA0
#define HID_KB_KEY_OPER                    0xA1
#define HID_KB_KEY_CLEAR_AGAIN             0xA2
#define HID_KB_KEY_PROPS                   0xA3
#define HID_KB_KEY_EXSEL                   0xA4

#define HID_KB_KEY_LEFT_CONTROL            0xE0
#define HID_KB_KEY_LEFT_SHIFT              0xE1
#define HID_KB_KEY_LEFT_ALT                0xE2
#define HID_KB_KEY_LEFT_GUI                0xE3
#define HID_KB_KEY_RIGHT_CONTROL           0xE4
#define HID_KB_KEY_RIGHT_SHIFT             0xE5
#define HID_KB_KEY_RIGHT_ALT               0xE6
#define HID_KB_KEY_RIGHT_GUI               0xE7


uint16_t usb_hid_translate_kb_value(const uint8_t byte);

#endif
