/* HID Constant codes
 *
 * Contains values defined with the USB HID 1.11 specification
 *
 * Author: Joseph Kinzel */

#ifndef HID_CONST_H_
#define HID_CONST_H_

/* Main item types */
#define HID_MAIN_INPUT                   0x8U
#define HID_MAIN_OUTPUT                  0x9U
#define HID_MAIN_FEATURE                 0xBU
#define HID_MAIN_COLLECTION              0xAU
#define HID_MAIN_COLLECTION_END          0xCU

/* Collection types */
#define HID_COLLECTION_PHYSICAL          0x00U
#define HID_COLLECTION_APPLICATION       0x01U
#define HID_COLLECTION_LOGICAL           0x02U
#define HID_COLLECTION_REPORT            0x03U
#define HID_COLLECTION_NAMED_ARRAY       0x04U
#define HID_COLLECTION_USAGE_SWITCH      0x05U
#define HID_COLLECTION_USAGE_MODIFIER    0x06U

/* Input, Output, Feature field values */
#define HID_IOF_GET_CONSTANT(x)          ((x) & 0x0001U)
#define HID_IOF_GET_VARIABLE(x)          ((x) & 0x0002U)
#define HID_IOF_GET_RELATIVE(x)          ((x) & 0x0004U)
#define HID_IOF_GET_WRAP(x)              ((x) & 0x0008U)
#define HID_IOF_GET_NONLINEAR(x)         ((x) & 0x0010U)
#define HID_IOF_GET_PREFERRED(x)         ((x) & 0x0020U)
#define HID_IOF_GET_NULL(x)              ((x) & 0x0040U)
#define HID_IOF_GET_VOLATILE(x)          ((x) & 0x0080U)
#define HID_IOF_GET_BITFIELD(x)          ((x) & 0x0100U)

#define HID_IOF_FIELD_DC                 0x001U
#define HID_IOF_FIELD_AV                 0x002U
#define HID_IOF_FIELD_AR                 0x004U
#define HID_IOF_FIELD_NWW                0x008U
#define HID_IOF_FIELD_LNL                0x010U
#define HID_IOF_FIELD_PSNP               0x020U
#define HID_IOF_FIELD_NS                 0x040U
#define HID_IOF_FIELD_VNV                0x080U
#define HID_IOF_FIELD_BFBB               0x100U

/* Short item macros and values */
#define HID_SI_GET_SIZE(x)               ((x) & 0x03U)

#define HID_SI_SIZE_0                    0
#define HID_SI_SIZE_1                    1
#define HID_SI_SIZE_2                    2
#define HID_SI_SIZE_4                    3

#define HID_SI_GET_TYPE(x)               (((x) >> 2) & 0x03U)

#define HID_SI_TYPE_MAIN                 0
#define HID_SI_TYPE_GLOBAL               1
#define HID_SI_TYPE_LOCAL                2
#define HID_SI_TYPE_RESERVED             3

#define HID_SI_GET_TAG(x)                (((x) >> 4) & 0x0FU)

/* Long item macros and values */
#define HID_LI_INFO_VAL                  0xFEU
#define HID_LI_LONG_TAG_VENDOR_START     0xF0U

/* Global Items */
#define HID_GLOBAL_TAG_USAGE_PAGE        0x00U
#define HID_GLOBAL_TAG_LOGICAL_MIN       0x01U
#define HID_GLOBAL_TAG_LOGICAL_MAX       0x02U
#define HID_GLOBAL_TAG_PHYS_MIN          0x03U
#define HID_GLOBAL_TAG_PHYS_MAX          0x04U
#define HID_GLOBAL_TAG_UNIT_EX           0x05U
#define HID_GLOBAL_TAG_UNIT              0x06U
#define HID_GLOBAL_TAG_REPORT_SIZE       0x07U
#define HID_GLOBAL_TAG_REPORT_ID         0x08U
#define HID_GLOBAL_TAG_REPORT_COUNT      0x09U
#define HID_GLOBAL_TAG_PUSH              0x0AU
#define HID_GLOBAL_TAG_POP               0x0BU

/* Local Items */
#define HID_LOCAL_TAG_USAGE              0x00U
#define HID_LOCAL_TAG_USAGE_MIN          0x01U
#define HID_LOCAL_TAG_USAGE_MAX          0x02U
#define HID_LOCAL_TAG_DESIG_INDEX        0x03U
#define HID_LOCAL_TAG_DESIG_MIN          0x04U
#define HID_LOCAL_TAG_DESIG_MAX          0x05U
#define HID_LOCAL_TAG_STRING_INDEX       0x07U
#define HID_LOCAL_TAG_STRING_MIN         0x08U
#define HID_LOCAL_TAG_STRING_MAX         0x09U
#define HID_LOCAL_TAG_DELIMITER          0x0AU

#endif // HID_CONST_H_
