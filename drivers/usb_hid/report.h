/* Report descriptor parsing header
 *
 * Author: Joseph Kinzel */

#ifndef HID_REPORT_H_
#define HID_REPORT_H_

/* Indicates that input ports on the device's interrupt in endpoint will include an additional leading byte indicating the report ID */
#define HID_REPORT_ID_PRESENT           0x01U

int hid_parse_report_desc(hid_dev_context *dc, volatile uint8_t *report, const uint16_t report_length, hid_report_result *result);

#endif // HID_REPORT_H_
