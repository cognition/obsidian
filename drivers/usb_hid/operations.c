/* Operations where the driver talks to the USB coordinator driver
 *
 *
 * Author: Joseph Kinzel */

#include "operations.h"
#include <driverapi.h>
#include <usb.h>

/*! Performs a class and interface targeted control transaction
 *  \param dev_context HID Interface context
 *  \param sync Wait/Signal synchronization object
 *  \param request_dir Direction of the request (USB_REQ_TYPE_DEV_TO_HOST or USB_REQ_TYPE_HOST_TO_DEV)
 *  \param request_value Requested operation (bRequest)
 *  \param value Request associated value (wValue)
 *  \param buffer Buffer associated with the transaction
 *  \param buffer_length Length of the buffer or zero if there is no buffer (wLength)
 *  \return 1 if successful, 0 otherwise
 */
int hid_control_transaction(hid_dev_context *dev_context, sync_object *sync, const uint8_t request_dir, const uint8_t request_value,
                                    const uint16_t value, void *buffer, const uint16_t buffer_length) {

    usbDevTransaction dt;
    usbDevRunTransactionsRequest rt;

    int status;
    void *response;
    uint8_t direction;
    uint32_t res_size = 0;
    response = NULL;

    direction = ((request_dir & USB_REQ_TYPE_DEV_TO_HOST) ? USB_TRANSACTION_IN:USB_TRANSACTION_OUT);
    /* Build out the device transaction structure */
    dt.type = usbTransactionControl;
    dt.requestor = dev_context->bus_spec;
    dt.direction = direction;
    dt.sync = sync;
    dt.flags = 0;
    /* Default control pipe */
    dt.endpoint = 0;
    dt.control.bm_request_type = (USB_REQ_TYPE_DEV_TO_HOST & request_dir) | USB_REQ_TYPE_INTERFACE | USB_REQ_TYPE_CLASS;
    dt.control.b_request = request_value;
    dt.control.data_ptr = NULL;
    dt.control.w_index = dev_context->interface_num;
    dt.control.data_ptr = buffer;
    dt.control.w_length = buffer_length;
    dt.control.w_value = value;

    /* Initialize the run transaction message */
    rt.endpoint = 0;
    rt.direction = direction;
    rt.requestor = dev_context->bus_spec;

    /* Queue and run the transaction */
    status = busMsg(response, &res_size, BUS_ID_USB, dev_context->full_address, &dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, dev_context->hid_dev_handle);
    if(status != busMsgAcknowledged)
    {
        printf("[HID ]: Control transaction failed! Code: %u\n", status);
        return 0;
    }

    status = busMsg(response, &res_size, BUS_ID_USB, dev_context->full_address, &rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, dev_context->hid_dev_handle);
    if(status != busMsgAcknowledged)
    {
        printf("[HID ]: Control transaction run failed! Code: %u\n", status);
        return 0;
    }
    return 1;
}

/*! Sets the protocol of the HID interface
 *  \param value Protocol being requested
 *  \param sync Wait/Signal synchronization object that will be triggered with the request completes
 */
int hid_set_protocol(hid_dev_context *dev_context, uint16_t value, sync_object *sync)
{
    return hid_control_transaction(dev_context, sync, USB_REQ_TYPE_HOST_TO_DEV, HID_REQ_SET_PROTOCOL, value, NULL, 0);
}

/*! Sets the idle rate of the HID device
 *  \param dev_context HID Interface context instance
 *  \param duration Amount of time to idle the interrupt in pipe on unchanged requests, in 4ms units
 *  \param report_id ID value of the target report
 *  \param sync Signal/Wait style synchronization object that will trigger when the request completes
 */
int hid_set_idle(hid_dev_context *dev_context, const uint8_t duration, const uint8_t report_id, sync_object *sync)
{
    uint16_t value;

    value = duration;
    value <<= 8;
    value |= report_id;

    return hid_control_transaction(dev_context, sync, USB_REQ_TYPE_HOST_TO_DEV, HID_REQ_SET_IDLE, value, NULL, 0);
}

/*! Sets the values in a report
 *  \param dev_context HID INterface context instance
 *  \param report_type Type of the report to be set, usually an output report
 *  \param report_id ID value of the target report
 *  \param report_buffer Buffer containing the new values of the report
 *  \param report_length Length of the report buffer
 *  \param sync Signal/Wait style sychronization object that will trigger when the request completes
 */
int hid_set_report(hid_dev_context *dev_context, const uint8_t report_type, const uint8_t report_id, uint8_t *report_buffer, const uint16_t report_length, sync_object *sync)
{
    uint16_t w_value;

    w_value = report_type;
    w_value <<= 8;
    w_value |= report_id;

    return hid_control_transaction(dev_context, sync, USB_REQ_TYPE_HOST_TO_DEV, HID_REQ_SET_REPORT, w_value, report_buffer, report_length);
}

/*! Requests the parsing of an endpoint descriptor to a usbConfigureEndpointFields structure
 *  \param dev_context HID Interface context instance
 *  \param desc_buffer Input descriptor buffer, should be of the endpoint type
 *  \param desc_rem_bytes Maximum potential parse size of the descriptor buffer
 *  \param addr Full USB address of the parent device
 *  \return Number of bytes parsed if the descriptor was parsed successfully or zero if corruption was encountered in the descriptor buffer
 */
int hid_parse_endpoint(hid_dev_context *dev_context, const uint8_t *desc_buffer, const int desc_rem_bytes, const uint64_t addr)
{
    int status;
    usbConfigureEndpointFields ep_info;
    usb_decode_ep_request request;
    usb_decode_ep_response response;
    uint32_t response_length = sizeof(usb_decode_ep_response);

    /* Initialize the endpoint decode request and issue it to the bus coordinator driver */
    request.requestor = dev_context->bus_spec;
    request.ep_desc = desc_buffer;
    request.ep_fields = &ep_info;
    request.max_desc_bytes = desc_rem_bytes;

    status = busMsg(&response, &response_length, BUS_ID_USB, dev_context->full_address, &request, sizeof(usb_decode_ep_request), usbCoordDecodeEndpointDesc, dev_context->hid_dev_handle );
    if(status != busMsgAcknowledged)
    {
        printf("[HID ]: Coordinator rejected decode endpoint request! Status: %u\n", status);
        return 0;
    }

    /* HID devices should only have interrupt endpoints */
    if(ep_info.ep_type == USB_EP_TYPE_INTERRUPT)
    {
        /* Update the interface context instance with the endpoint value for the in or out endpoint */
        if(ep_info.direction == USB_EP_DIR_OUT)
        {
            if(!dev_context->int_out_ep)
                dev_context->int_out_ep = ep_info.ep_num;
            else
                return 0;
        }
        else
        {
            if(!dev_context->int_in_ep)
                dev_context->int_in_ep = ep_info.ep_num;
            else
                return 0;
        }
    }
    else
        return 0;

    return response.desc_bytes_traversed;
}

/*! HID Get report request
 *  \param dev_context HID Interface context instance
 *  \param report_type Type of the report to retrieve
 *  \param report_num ID of the report to retrieve
 *  \param report_length Length of the report and buffer to be returned
 */
uint8_t *hid_get_report(hid_dev_context *dev_context, uint8_t report_type, uint8_t report_num, uint8_t report_length)
{
    uint16_t report_value;
    uint8_t *report;
    sync_object *wait_obj;

    report_value = report_type;
    report_value <<= 8;
    report_value |= report_num;

    report = (uint8_t *)calloc(1, report_length);

    sync_create_signal_obj(&wait_obj, 0, 0);

    if(!hid_control_transaction(dev_context, wait_obj, USB_REQ_TYPE_DEV_TO_HOST, HID_REQ_GET_REPORT, report_value, report, report_length))
    {
        free(report);
        return NULL;
    }

    sync_object_wait(wait_obj, 500);
    sync_destroy_signal_obj(wait_obj);

    return report;
}
