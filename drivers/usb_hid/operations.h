#ifndef HID_OPERATIONS_H_
#define HID_OPERATIONS_H_

#include "hid_struct.h"
#include <sync.h>

int hid_control_transaction(hid_dev_context *dev_context, sync_object *sync, const uint8_t request_dir, const uint8_t request_value,
                                    const uint16_t value, void *buffer, const uint16_t buffer_length);

int hid_set_protocol(hid_dev_context *dev_context, uint16_t value, sync_object *sync);
int hid_set_idle(hid_dev_context *dev_context, const uint8_t duration, const uint8_t report_id, sync_object *sync);
int hid_parse_endpoint(hid_dev_context *dev_context, const uint8_t *desc_buffer, const int desc_rem_bytes, const uint64_t addr);
uint8_t *hid_get_report(hid_dev_context *dev_context, uint8_t report_type, uint8_t report_num, uint8_t report_length);
int hid_set_report(hid_dev_context *dev_context, const uint8_t report_type, const uint8_t report_id, uint8_t *report_buffer, const uint16_t report_length, sync_object *sync);

#endif // HID_OPERATIONS_H_
