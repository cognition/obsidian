/* USB HID Driver structures header
 *
 * Author: Joseph Kinzel */

#ifndef HID_STRUCT_H_
#define HID_STRUCT_H_

#include "dapi_types.h"
#include <usb.h>
#include "hid_const.h"

/* HID 1.1 Spec defined descriptor type values */
#define HID_DESC_TYPE_HID           0x21
#define HID_DESC_TYPE_REPORT        0x22
#define HID_DESC_TYPE_PHYSICAL      0x23

/*USB Interface Subclass field values */
#define HID_SUBCLASS_BOOT           0x01

/* USB Interface Protocol field values */
#define HID_PROTOCOL_BOOT_KB        0x01
#define HID_PROTOCOL_BOOT_MOUSE     0x02

#define HID_FLAG_BOOT_PROTO         0x01

/* Rough device types */
#define HID_DEV_TYPE_KB             0x0001U
#define HID_DEV_TYPE_MOUSE          0x0002U
#define HID_DEV_TYPE_GAMEPAD        0x0004U

/* Class specific request values */
#define HID_REQ_GET_REPORT          0x01U
#define HID_REQ_GET_IDLE            0x02U
#define HID_REQ_GET_PROTOCOL        0x03U

#define HID_REQ_SET_REPORT          0x09U
#define HID_REQ_SET_IDLE            0x0AU
#define HID_REQ_SET_PROTOCOL        0x0BU

/* Set protocol values */
#define HID_PROTO_BOOT              0
#define HID_PROTO_REPORT            1


/* Report type values */
#define HID_REPORT_TYPE_INPUT       1
#define HID_REPORT_TYPE_OUTPUT      2
#define HID_REPORT_TYPE_FEATURE     3

typedef struct COMPILER_PACKED
{
    uint8_t type;
    uint16_t w_length;
} hidOptionalDescriptorEntry;

/* USB HID 1.1 Specification defined descriptor, should be included under the interface descriptor */
typedef struct COMPILER_PACKED
{
    uint8_t b_length, b_desc_type;
    uint16_t bcd_hid;
    uint8_t country_code;
    uint8_t b_num_descs;
    /* Report descriptor */
    uint8_t b_class_desc_type;
    uint16_t w_desc_length;
    /* Optional descriptors */
    hidOptionalDescriptorEntry optional[];
} hidDescriptor;

typedef struct COMPILER_PACKED
{
    uint8_t info;
    uint8_t data[];
} hid_short_item;

typedef struct COMPILER_PACKED
{
    uint8_t info, bDataSize, bLongItemTag;
    uint8_t data[];
} hid_long_item;

typedef struct _hid_global_state
{
    struct _hid_global_state *prev;
    uint32_t usage_page;
    uint32_t logical_minimum, logical_maximum;
    uint32_t physical_minimum, physical_maximum;
    uint32_t report_count, report_size;
    uint8_t report_id;
    uint32_t unit, unit_ex;
} hid_global_state;

typedef struct _hid_local_state
{
    struct _hid_local_state *prev;
    uint32_t usage, desig_index, string_index;
    uint32_t usage_min, usage_max;
    uint32_t desig_min, desig_max;
    uint32_t string_min, string_max, delimiter;
} hid_local_state;

typedef struct _hid_report_state
{
    struct _hid_report_state *prev;
    hid_local_state local;
    hid_global_state global;
} hid_report_state;

typedef struct _hid_iof_item
{
    struct _hid_iof_item *prev, *next;
    uint32_t flags;
    hid_report_state state;
} hid_iof_item;

typedef struct _hid_report_package
{
    hid_iof_item *components;
    uint32_t report_bytes, report_bits, report_id;
} hid_report_package;

typedef struct
{
    hid_iof_item *input_fields, *output_fields, *feature_fields;
    uint32_t flags;
} hid_report_parse_state;

typedef struct
{
    void *input_reports, *output_reports, *feature_reports;
    int max_input_report_size;
    uint32_t flags;
} hid_report_result;

/* HID Device context structure, main structure of each interface */
typedef struct
{
    DEVICE_HANDLE hid_dev_handle;
    BUS_HANDLE bus_spec;
    sync_object *interrupt_sync;
    uint64_t full_address;
    uint32_t device_types;
    uint32_t flags;
    uint16_t int_in_status;
    uint8_t interface_num, alt_count, version;
    uint8_t int_in_ep, int_out_ep;
    struct _hid_cb_arg *cb;
    usbDevTransaction dt;
    usbDevRunTransactionsRequest rt;
    hid_report_result report_info;
} hid_dev_context;

/* Boot protocol mouse structure */
typedef struct COMPILER_PACKED
{
    uint8_t button_mask;
    int8_t x_disp, y_disp;
    uint8_t dev_data[];
} hid_boot_mouse_report;

/* HID report callback */
typedef struct _hid_cb_arg
{
    union
    {
        void *buffer_ptr;
        volatile uint8_t *buffer;
    };
    int buffer_size;
    hid_dev_context *device;
    dapi_channel_ref *publish_channel;
} hid_cb_arg;


#endif // HID_STRUCT_H_
