#include "code_pages.h"
#include <uinput/keyboard.h>

static const uint16_t hid_kb_to_uinput_map[256] = {

    0, 0, 0, 0,

    uinput_kb_a_pressed,             // 0x04
    uinput_kb_b_pressed,             // 0x05
    uinput_kb_c_pressed,             // 0x06
    uinput_kb_d_pressed,             // 0x07
    uinput_kb_e_pressed,             // 0x08
    uinput_kb_f_pressed,             // 0x09
    uinput_kb_g_pressed,             // 0x0A
    uinput_kb_h_pressed,             // 0x0B
    uinput_kb_i_pressed,             // 0x0C
    uinput_kb_j_pressed,             // 0x0D
    uinput_kb_k_pressed,             // 0x0E
    uinput_kb_l_pressed,             // 0x0F
    uinput_kb_m_pressed,             // 0x10
    uinput_kb_n_pressed,             // 0x11
    uinput_kb_o_pressed,             // 0x12
    uinput_kb_p_pressed,             // 0x13
    uinput_kb_q_pressed,             // 0x14
    uinput_kb_r_pressed,             // 0x15
    uinput_kb_s_pressed,             // 0x16
    uinput_kb_t_pressed,             // 0x17
    uinput_kb_u_pressed,             // 0x18
    uinput_kb_v_pressed,             // 0x19
    uinput_kb_w_pressed,             // 0x1A
    uinput_kb_x_pressed,             // 0x1B
    uinput_kb_y_pressed,             // 0x1C
    uinput_kb_z_pressed,             // 0x1D

    uinput_kb_one_pressed,           // 0x1E
    uinput_kb_two_pressed,           // 0x1F
    uinput_kb_three_pressed,         // 0x20
    uinput_kb_four_pressed,          // 0x21
    uinput_kb_five_pressed,          // 0x22
    uinput_kb_six_pressed,           // 0x23
    uinput_kb_seven_pressed,         // 0x24
    uinput_kb_eight_pressed,         // 0x25
    uinput_kb_nine_pressed,          // 0x26
    uinput_kb_zero_pressed,          // 0x27

    uinput_kb_return_pressed,             // 0x28
    uinput_kb_escape_pressed,             // 0x29
    uinput_kb_delete_pressed,             // 0x2A
    uinput_kb_tab_pressed,                // 0x2B
    uinput_kb_space_pressed,              // 0x2C

    uinput_kb_dash_pressed,               // 0x2D
    uinput_kb_plus_pressed,               // 0x2E
    uinput_kb_left_bracket_pressed,       // 0x2F
    uinput_kb_right_bracket_pressed,      // 0x30
    uinput_kb_slash_pressed,              // 0x31
    uinput_kb_tilde_pressed,              // 0x32
    uinput_kb_semicolon_pressed,          // 0x33
    uinput_kb_quote_pressed,              // 0x34
    uinput_kb_tilde_pressed,              // 0x35
    uinput_kb_comma_pressed,              // 0x36
    uinput_kb_period_pressed,             // 0x37
    uinput_kb_backslash_pressed,          // 0x38
    uinput_kb_caps_lock_pressed,          // 0x39

    uinput_kb_f1_pressed,                 // 0x3A
    uinput_kb_f2_pressed,                 // 0x3B
    uinput_kb_f3_pressed,                 // 0x3C
    uinput_kb_f4_pressed,                 // 0x3D
    uinput_kb_f5_pressed,                 // 0x3E
    uinput_kb_f6_pressed,                 // 0x3F
    uinput_kb_f7_pressed,                 // 0x40
    uinput_kb_f8_pressed,                 // 0x41
    uinput_kb_f9_pressed,                 // 0x42
    uinput_kb_f10_pressed,                // 0x43
    uinput_kb_f11_pressed,                // 0x44
    uinput_kb_f12_pressed,                // 0x45

    uinput_kb_print_screen_pressed,       // 0x46
    uinput_kb_scroll_lock_pressed,        // 0x47
    uinput_kb_pause_pressed,              // 0x48
    uinput_kb_insert_pressed,             // 0x49
    uinput_kb_home_pressed,               // 0x4A
    uinput_kb_page_up_pressed,            // 0x4B
    uinput_kb_delete_pressed,             // 0x4C
    uinput_kb_end_pressed,                // 0x4D
    uinput_kb_page_down_pressed,          // 0x4E

    uinput_kb_right_pressed,              // 0x4F
    uinput_kb_left_pressed,               // 0x50
    uinput_kb_down_pressed,               // 0x51
    uinput_kb_up_pressed,                 // 0x52

    uinput_kb_num_lock_pressed,           // 0x53
    uinput_kb_numpad_backslash_pressed,   // 0x54
    uinput_kb_numpad_asterisk_pressed,    // 0x55
    uinput_kb_numpad_dash_pressed,        // 0x56
    uinput_kb_numpad_plus_pressed,        // 0x57
    uinput_kb_numpad_enter_pressed,       // 0x58
    uinput_kb_numpad_one_pressed,         // 0x59
    uinput_kb_numpad_two_pressed,         // 0x5A
    uinput_kb_numpad_three_pressed,       // 0x5B
    uinput_kb_numpad_four_pressed,        // 0x5C
    uinput_kb_numpad_five_pressed,        // 0x5D
    uinput_kb_numpad_six_pressed,         // 0x5E
    uinput_kb_numpad_seven_pressed,       // 0x5F
    uinput_kb_numpad_eight_pressed,       // 0x60
    uinput_kb_numpad_nine_pressed,        // 0x61
    uinput_kb_numpad_zero_pressed,        // 0x62
    uinput_kb_numpad_period_pressed,      // 0x63

    0, /* Non US backslash/pipe */        // 0x64
    0, /* "App" key */                    // 0x65
    0, /* Power key */                    // 0x66
    0, /* Numpad equals */                // 0x67

    0, /* F13 */                          // 0x68
    0, /* F14 */                          // 0x69
    0, /* F15 */                          // 0x6A
    0, /* F16 */                          // 0x6B
    0, /* F17 */                          // 0x6C
    0, /* F18 */                          // 0x6D
    0, /* F19 */                          // 0x6E
    0, /* F20 */                          // 0x6F
    0, /* F21 */                          // 0x70
    0, /* F22 */                          // 0x71
    0, /* F23 */                          // 0x72
    0, /* F24 */                          // 0x73

    0, /* Execute key */                  // 0x74
    0, /* Help key */                     // 0x75
    uinput_kb_menu_pressed,               // 0x76
    0, /* Select key */                   // 0x77
    uinput_kb_stop_pressed,               // 0x78
    0, /* Again key */                    // 0x79
    0, /* Undo key */                     // 0x7A
    0, /* Cut key */                      // 0x7B
    0, /* Copy key */                     // 0x7C
    0, /* Paste key */                    // 0x7D
    0, /* Find key */                     // 0x7E
    uinput_kb_mute_pressed,               // 0x7F
    uinput_kb_volume_up_pressed,          // 0x80
    uinput_kb_volume_down_pressed,        // 0x81
    0, /* Locking caps */                 // 0x82
    0, /* Locking num */                  // 0x83
    0, /* Locking scroll */               // 0x84

    uinput_kb_comma_pressed,              // 0x85
    0, /* ES */                           // 0x86
    0, /* Int. 1 */                       // 0x87
    0, /* Int. 2 */                       // 0x88
    0, /* Int. 3 */                       // 0x89
    0, /* Int. 4 */                       // 0x8A
    0, /* Int. 5 */                       // 0x8B
    0, /* Int. 6 */                       // 0x8C
    0, /* Int. 7 */                       // 0x8D
    0, /* Int. 8 */                       // 0x8E
    0, /* Int. 9 */                       // 0x8F

    0, /* Lang. 1 */                      // 0x90
    0, /* Lang. 2 */                      // 0x91
    0, /* Lang. 3 */                      // 0x92
    0, /* Lang. 4 */                      // 0x93
    0, /* Lang. 5 */                      // 0x94
    0, /* Lang. 6 */                      // 0x95
    0, /* Lang. 7 */                      // 0x96
    0, /* Lang. 8 */                      // 0x97
    0, /* Lang. 9 */                      // 0x98

    0, /* Alternate erase */              // 0x99
    uinput_kb_print_screen_pressed,       // 0x9A
    0, /* Cancel */                       // 0x9B
    0, /* Clear */                        // 0x9C
    0, /* Prior */                        // 0x9D
    0, /* Return alt */                   // 0x9E
    0, /* Separator */                    // 0x9F
    0, /* Out */                          // 0xA0
    0, /* Operator */                     // 0xA1
    0, /* Clear/Again */                  // 0xA2
    0, /* Props */                        // 0xA3
    0, /* ExSel */                        // 0xA4


    0, /* Reserved */                     // 0xA5
    0, /* Reserved */                     // 0xA6
    0, /* Reserved */                     // 0xA7
    0, /* Reserved */                     // 0xA8
    0, /* Reserved */                     // 0xA9
    0, /* Reserved */                     // 0xAA
    0, /* Reserved */                     // 0xAB
    0, /* Reserved */                     // 0xAC
    0, /* Reserved */                     // 0xAD
    0, /* Reserved */                     // 0xAE
    0, /* Reserved */                     // 0xAF

    0, /* Keypad 00 */                    // 0xB0
    0, /* Keypad 000 */                   // 0xB1
    0, /* Keypad Thousands sep. */        // 0xB2
    0, /* Keypad decimal sep. */          // 0xB3
    0, /* Currency unit */                // 0xB4
    0, /* Currency sub-unit */            // 0xB5

    0, /* Keypad ( */                     // 0xB6
    0, /* Keypad ) */                     // 0xB7
    0, /* Keypad { */                     // 0xB8
    0, /* Keypad } */                     // 0xB9
    0, /* Keypad Tab */                   // 0xBA
    0, /* Keypad backspace */             // 0xBB
    0, /* Keypad A */                     // 0xBC
    0, /* Keypad B */                     // 0xBD
    0, /* Keypad C */                     // 0xBE
    0, /* Keypad D */                     // 0xBF
    0, /* Keypad E */                     // 0xC0
    0, /* Keypad F */                     // 0xC1
    0, /* Keypad XOR */                   // 0xC2
    0, /* Keypad ^ */                     // 0xC3
    0, /* Keypad % */                     // 0xC4
    0, /* Keypad < */                     // 0xC5
    0, /* Keypad > */                     // 0xC6
    0, /* Keypad & */                     // 0xC7
    0, /* Keypad && */                    // 0xC8
    0, /* Keypad | */                     // 0xC9
    0, /* Keypad || */                    // 0xCA
    0, /* Keypad : */                     // 0xCB
    0, /* Keypad # */                     // 0xCC
    0, /* Keypad space */                 // 0xCD
    0, /* Keypad @ */                     // 0xCE
    0, /* Keypad ! */                     // 0xCF
    0, /* Keypad Memory Store */          // 0xD0
    0, /* Keypad Memory Recall */         // 0xD1
    0, /* Keypad Memory Clear */          // 0xD2
    0, /* Keypad Memory Add */            // 0xD3
    0, /* Keypad Memory Subtract */       // 0xD4
    0, /* Keypad Memory Multiply */       // 0xD5
    0, /* Keypad Memory Divide */         // 0xD6
    0, /* Keypad pos/neg */               // 0xD7
    0, /* Keypad clear */                 // 0xD8
    0, /* Keypad clear entry */           // 0xD9
    0, /* Keypad binary */                // 0xDA
    0, /* Keypad octal */                 // 0xDB
    0, /* Keypad decimal */               // 0xDC
    0, /* Keypad hex */                   // 0xDD
    0, /* Reserved */                     // 0xDE
    0, /* Reserved */                     // 0xDF

    uinput_kb_left_ctrl_pressed,          // 0xE0
    uinput_kb_left_shift_pressed,         // 0xE1
    uinput_kb_left_alt_pressed,           // 0xE2
    uinput_kb_left_win_pressed,           // 0xE3
    uinput_kb_right_ctrl_pressed,         // 0xE4
    uinput_kb_right_shift_pressed,        // 0xE5
    uinput_kb_right_alt_pressed,          // 0xE6
    uinput_kb_right_win_pressed,          // 0xE7

    0, /* Reserved */                     // 0xE8
    0, /* Reserved */                     // 0xE9
    0, /* Reserved */                     // 0xEA
    0, /* Reserved */                     // 0xEB
    0, /* Reserved */                     // 0xEC
    0, /* Reserved */                     // 0xED
    0, /* Reserved */                     // 0xEE
    0, /* Reserved */                     // 0xEF

    0, /* Reserved */                     // 0xF0
    0, /* Reserved */                     // 0xF1
    0, /* Reserved */                     // 0xF2
    0, /* Reserved */                     // 0xF3
    0, /* Reserved */                     // 0xF4
    0, /* Reserved */                     // 0xF5
    0, /* Reserved */                     // 0xF6
    0, /* Reserved */                     // 0xF7
    0, /* Reserved */                     // 0xF8
    0, /* Reserved */                     // 0xF9
    0, /* Reserved */                     // 0xFA
    0, /* Reserved */                     // 0xFB
    0, /* Reserved */                     // 0xFC
    0, /* Reserved */                     // 0xFD
    0, /* Reserved */                     // 0xFE
    0  /* Reserved */                     // 0xFF
};

uint16_t usb_hid_translate_kb_value(const uint8_t byte)
{
    return hid_kb_to_uinput_map[byte];
}
