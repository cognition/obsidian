/* HID Driver initialization code
 *
 * Author: Joseph Kinzel */

#include <driverapi.h>
#include <usb.h>
#include "operations.h"
#include "hid_const.h"
#include "hid_struct.h"
#include "code_pages.h"
#include "report.h"

COMPILER_UNUSED static const int hid_exponents[16] = {0, 1, 2 ,3, 4, 5, 6, 7, -8, -7, -6, -5, -4, -2, -1};


/* Prototypes for module information table */
driverOperationResult hid_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet);
driverOperationResult hid_deinit(void *context);
driverOperationResult hid_enum(void *context, void *identifyFunc);

/* Module export table */
COMPILER_MODINFO_SECTION const modExportTable moduleInfoTable = {&hid_init, &hid_deinit, &hid_enum};

/*! Non-boot HID input report callback handler
 *
 * \param context Pointer to the CALLBACK context.
 * \param bytes_transferred Number of bytes transferred in by the interrupt input pipe for this transaction
 */
void hid_handle_report(void *context, const uint64_t bytes_transferred)
{
    hid_cb_arg *cb_info;
    hid_dev_context *dc;
    uint32_t res_size;

    cb_info = (hid_cb_arg *)context;
    dc = (hid_dev_context *)cb_info->device;

    res_size = 0;

    /* Insert a new transaction to replace the one we're servicing */
    if(busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, &dc->dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, dc->hid_dev_handle) != busMsgAcknowledged)
    {
        printf("[HID ]: Failed to queue interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
        return;
    }

    if(busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, &dc->rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, dc->hid_dev_handle) != busMsgAcknowledged)
    {
        printf("[HID ]: Failed to run interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
        return;
    }
}

/*! Boot mouse callback handler
 *  \param context Pointer to the CALLBACK context
 *  \param bytes_transferred Number of bytes that were successfully transferred
 */
void hid_handle_boot_mouse(void *context, const uint64_t bytes_transferred)
{
    hid_cb_arg *cb_info;
    hid_dev_context *dc;
    volatile hid_boot_mouse_report *boot_mouse_report;
    uint32_t res_size;

    cb_info = (hid_cb_arg *)context;
    dc = (hid_dev_context *)cb_info->device;
    boot_mouse_report = (volatile hid_boot_mouse_report *)cb_info->buffer;

    res_size = 0;

    printf("[HID ]: Boot mouse state - Buttons: %01X X Disp: %i Y Disp: %i\n", boot_mouse_report->button_mask, boot_mouse_report->x_disp, boot_mouse_report->y_disp);

    /* Replace the transaction we're currently servicing with a new one */
    if(busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, &dc->dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, dc->hid_dev_handle) != busMsgAcknowledged)
    {
        printf("[HID ]: Failed to queue interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
        return;
    }

    if(busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, &dc->rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, dc->hid_dev_handle) != busMsgAcknowledged)
    {
        printf("[HID ]: Failed to run interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
        return;
    }

}

/*! Boot keyboard callback handler
 *
 *  \param context Controller context callback pointer
 *  \param bytes_transferred Bytes transferred in the transaction
 */
void hid_handle_boot_keyboard(void *context, const uint64_t bytes_transferred)
{
    hid_cb_arg *cb_info;
    hid_dev_context *dc;
    uint32_t res_size;

    cb_info = (hid_cb_arg *)context;
    dc = (hid_dev_context *)cb_info->device;

    res_size = 0;

    if(cb_info->publish_channel)
    {
        for(int report_index = 2; report_index < 8; report_index++)
        {
            uint16_t msg;

            msg = usb_hid_translate_kb_value(cb_info->buffer[report_index]);
            if(msg)
                sync_channel_publish(cb_info->publish_channel, &msg, 2);
        }
    }

    /* Replace the transaction being serviced with a new one */
    if(busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, &dc->dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, dc->hid_dev_handle) != busMsgAcknowledged)
    {
        printf("[HID ]: Failed to queue interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
        return;
    }

    if(busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, &dc->rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, dc->hid_dev_handle) != busMsgAcknowledged)
    {
        printf("[HID ]: Failed to run interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
        return;
    }

}

busMsgResponse hid_init_transactions(hid_dev_context *dc)
{
    busMsgResponse status;
    usbDevTransaction *dt;
    usbDevRunTransactionsRequest *rt;
    hid_cb_arg *cb_info;
    uint32_t res_size;

    res_size = 0;
    cb_info = dc->cb;
    dt = &dc->dt;
    rt = &dc->rt;

    /* Initialize the input report transaction structure in the device context structure */
    dt->endpoint = dc->int_in_ep;
    dt->direction = USB_TRANSACTION_IN;
    dt->requestor = dc->bus_spec;
    /* Synchronization object must be of the callback type to actually handle the result of the transaction */
    dt->sync = dc->interrupt_sync;
    dt->type = usbTransactionInterrupt;
    dt->transfer.buffer = cb_info->buffer_ptr;
    dt->transfer.length = cb_info->buffer_size;
    dt->flags = 0;

    /* If multiple reports are present or the device is a boot mouse it's possible that short packets will occur, flag that in the transaction */
    if( dc->report_info.flags & HID_REPORT_ID_PRESENT )
        dt->flags = USB_TRANSACTION_SMALLER;

    if((dc->flags & HID_FLAG_BOOT_PROTO) && (dc->device_types & HID_DEV_TYPE_MOUSE) )
        dt->flags = USB_TRANSACTION_SMALLER;

    /* Initialize the run transaction structure */
    rt->endpoint = dc->int_in_ep;
    rt->direction = USB_TRANSACTION_IN;
    rt->requestor = dc->bus_spec;

    /* Queue and run the initial set of transactions */
    for(int queue_index = 0; queue_index < 8; queue_index++)
    {
        status = busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, dc->hid_dev_handle);
        if(status != busMsgAcknowledged)
        {
            printf("[HID ]: Failed to queue interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
            return status;
        }
    }

    status = busMsg(NULL, &res_size, BUS_ID_USB, dc->full_address, rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, dc->hid_dev_handle);
    if(status != busMsgAcknowledged)
    {
        printf("[HID ]: Failed to run interrupt transaction for device @ %X interface %u\n", dc->full_address, dc->interface_num);
        return status;
    }

    printf("[HID ]: Transaction initialization complete for device @ %X\n", dc->full_address);
    return busMsgAcknowledged;
}

/*! Parses the HID descriptor that should be part of the full configuration descriptor, see HID rev 1.2 spec for more details
 *
 * \param dev_context Device context structure of the device providing the descriptor
 * \param desc_buffer Pointer to the descriptor's data
 * \param desc_rem Remaining bytes in the descriptor
 * \param addr Full USB address of the device
 * \param wait_obj Pointer to a wait and signal style synchornization object
 * \return Number of bytes parsed/contained in the HID descriptor or zero if the parsing should fail for any reason.
 */
int hid_parse_hid_desc(hid_dev_context *dev_context, const uint8_t *desc_buffer, const int desc_rem, uint64_t addr, sync_object *wait_obj)
{
    hidDescriptor *desc_ptr;
    volatile uint8_t *report;
    COMPILER_UNUSED const uint8_t *report_ptr;
    int total_size;
    uint64_t sync_ret;
    uint32_t res_size;
    usbDevTransaction dt;
    usbDevRunTransactionsRequest rt;
    busMsgResponse bus_response;
    desc_ptr = (hidDescriptor *)desc_buffer;

    total_size = desc_ptr->b_length;
    report_ptr = &desc_buffer[total_size];

    if(!desc_ptr->b_num_descs)
        return 0;

    report = (volatile uint8_t *)calloc(1, desc_ptr->w_desc_length);

    sync_object_reset(wait_obj);

    /* Set up a device transaction to retrieve the report descriptor */
    dt.sync = wait_obj;
    dt.flags = 0;
    dt.direction = USB_TRANSACTION_IN;
    dt.endpoint = 0;
    dt.requestor = dev_context->bus_spec;
    dt.type = usbTransactionControl;
    dt.control.bm_request_type = USB_REQ_TYPE_DEV_TO_HOST | USB_REQ_TYPE_INTERFACE;
    dt.control.b_request = USB_CONTROL_REQ_GET_DESCRIPTOR;
    dt.control.w_length = desc_ptr->w_desc_length;
    dt.control.data_ptr = report;
    dt.control.w_index = dev_context->interface_num;
    dt.control.w_value = (HID_DESC_TYPE_REPORT << 8);

    rt.direction = USB_TRANSACTION_IN;
    rt.endpoint = 0;
    rt.requestor = dev_context->bus_spec;

    res_size = 0;

    /* Run the descriptor request transaction */
    bus_response = busMsg(NULL, &res_size, BUS_ID_USB, dev_context->full_address, &dt, sizeof(usbDevTransaction), usbCoordTransactionQueue, dev_context->hid_dev_handle);
    if(bus_response != busMsgAcknowledged)
    {
        printf("[HID ]: Unable to queue GET REPORT DESCRIPTOR transaction for device @ %X:%u Response: %u\n", dev_context->full_address, dev_context->interface_num, bus_response);
    }

    bus_response = busMsg(NULL, &res_size, BUS_ID_USB, dev_context->full_address, &rt, sizeof(usbDevRunTransactionsRequest), usbCoordTransactionRun, dev_context->hid_dev_handle);
    if(bus_response != busMsgAcknowledged)
    {
        printf("[HID ]: Unable to run GET REPORT transaction for device @ %X:%u Response: %u\n", dev_context->full_address, dev_context->interface_num, bus_response);
    }

    /* Wait for the report descriptor request to complete and validate the success of the request */
    sync_object_wait(wait_obj, 1000);

    sync_ret = sync_object_get_status(wait_obj);

    if(!sync_ret)
    {
        printf("[HID ]: Get report descriptor transaction for device @ %X with size %u failed!\n", addr, desc_ptr->w_desc_length);
        return 0;
    }

    /* Parse the report descriptor */
    if( hid_parse_report_desc(dev_context, report, desc_ptr->w_desc_length, &dev_context->report_info) != driverOpSuccess)
    {
        printf("[HID ]: Failed parse report descriptor for device @ %X:%u\n", addr, dev_context->interface_num);
        return 0;
    }

    free((void *)report);

    return total_size;
}

/*! Parses all the descriptors in the interface descriptor for the HID function
 *
 * \param dev_context HID function's device context instance pointer
 * \param desc_buffer Buffer of the interface descriptor
 * \param desc_rem Size of the interface descriptor and any descriptors under it
 * \param addr Full USB address of the parent device
 * \param wait_obj Wait and signal style synchronization object
 * \return driverOpSuccess if successful or driverOpBadState if the some form of corruption was encountered while parsing the descriptors
 */
driverOperationResult hid_parse_descriptors(hid_dev_context *dev_context, const uint8_t *desc_buffer, int desc_rem, const uint64_t addr, sync_object *wait_obj)
{
    int desc_size;
    int desc_length = 0;
    desc_size = desc_rem;

    /* Skip the first descriptor since it's the interface descriptor */
    for(int offset = desc_buffer[USB_DESC_FIELD_LENGTH_OFF]; offset < desc_size; offset += desc_length)
    {
        desc_length = desc_buffer[offset + USB_DESC_FIELD_LENGTH_OFF];
        /* Look for the HID descriptor and any endpoint descriptors */
        switch(desc_buffer[offset + USB_DESC_FIELD_TYPE_OFF])
        {
            case usbDescEndpoint:
                desc_length = hid_parse_endpoint(dev_context, &desc_buffer[offset], desc_rem, addr);
                break;
            case HID_DESC_TYPE_HID:
                desc_length = hid_parse_hid_desc(dev_context, &desc_buffer[offset], desc_rem, addr, wait_obj);
                if(!desc_length)
                {
                    printf("[HID ]: Failed to parse HID descriptor for interface %u of device @ %X\n", dev_context->interface_num, dev_context->full_address);
                    return driverOpBadState;
                }
                break;
            default:
                printf("[HID ]: Unrecognized descriptor type: %u address: %X offset: %u\n", desc_buffer[offset + USB_DESC_FIELD_TYPE_OFF], addr, offset);
                return driverOpBadState;
                break;
        }

        if(!desc_length)
            return driverOpBadState;

        desc_rem -= desc_length;
    }

    /* At a minimum there should be one interrupt in endpoint to receive reports from the device */
    if(dev_context->int_in_ep)
        return driverOpSuccess;
    else
        return driverOpBadState;
}

/*! Destroys a device context and any allocated substructures
 *  \param context Device context to destroy
 *
 *  \note Endpoints should be halted and any outstanding transactions canceled to avoid triggering synchronization objects contained with the structure.
 */
void hid_destroy_context(hid_dev_context *context)
{
    if(context->cb)
        free(context->cb);
    if(context->interrupt_sync)
        sync_destroy_signal_obj(context->interrupt_sync);

    free(context);
}

/* Driver instance initialization function */
driverOperationResult hid_init(DEVICE_HANDLE dev, BUS_HANDLE bus_spec, busAddressDesc *desc, void *input_context, void **contextRet)
{
	usb_device_entry_context *entry_context;
	usbInterfaceDescriptor *interface_desc;
	hid_dev_context *dev_context;
	sync_object *wait_obj;

    int status;
    hid_cb_arg *cb;

    /* Get the USB Coordinator driver provided information */
	entry_context = (usb_device_entry_context *)input_context;
	interface_desc = (usbInterfaceDescriptor *)&entry_context->desc_buffer[0];

	/* Initialize the device context structure */
	dev_context = (hid_dev_context *)calloc(1, sizeof(hid_dev_context));
	dev_context->hid_dev_handle = dev;
	dev_context->bus_spec = bus_spec;
    dev_context->interface_num = interface_desc->bInterfaceNumber;
    dev_context->full_address = desc->address;

    /* Attempt to ascertain what kind of HID device we're dealing with here */
	if(interface_desc->bInterfaceSubClass == HID_SUBCLASS_BOOT)
	{
	    printf("[HID ]: Device @ %X Interface: %u supports the boot protocol\n", dev_context->full_address, dev_context->interface_num);
		dev_context->flags |= HID_FLAG_BOOT_PROTO;
		switch(interface_desc->bInterfaceProtocol)
		{
			case HID_PROTOCOL_BOOT_KB:
			    printf("[HID ]: Found boot keyboard!\n");
				dev_context->device_types |= HID_DEV_TYPE_KB;
				break;
			case HID_PROTOCOL_BOOT_MOUSE:
				dev_context->device_types |= HID_DEV_TYPE_MOUSE;
				break;
			default:
				break;
		}
	}

    sync_create_signal_obj(&wait_obj, 0, 0);

    /* Fall back to the boot protocol for devices that support it for the time being */
    if(dev_context->flags & HID_FLAG_BOOT_PROTO)
    {

        if(!hid_set_protocol(dev_context, HID_PROTO_BOOT, wait_obj))
            printf("[HID ]: Unable to set boot protocol on device @ %X\n", desc->address);
        sync_object_wait(wait_obj, 500);

        sync_object_reset(wait_obj);
    }

    /* Parse the descriptors to find the appropriate endpoints and HID report size */
    status = hid_parse_descriptors(dev_context, entry_context->desc_buffer, entry_context->length, desc->address, wait_obj);
    if(status != driverOpSuccess)
    {
        printf("[HID ]: Failed to parse descriptors for HID device!\n");
        free(dev_context);
        sync_destroy_signal_obj(wait_obj);
        return driverOpBadState;
    }


    printf("[HID ]: HID Device @ %X iface: %u initialized. INT out ep: %u INT in ep: %u Flags: %X\n", desc->address, dev_context->interface_num,
            dev_context->int_out_ep, dev_context->int_in_ep, dev_context->flags);

    /* Initialize the call back context structure */
    cb = (hid_cb_arg *)calloc(1, sizeof(hid_cb_arg));
    cb->device = dev_context;

    dev_context->cb = cb;

    if(dev_context->flags & HID_FLAG_BOOT_PROTO)
    {
        if(dev_context->device_types & HID_DEV_TYPE_KB)
        {
            /* Clear the LEDS and attach the boot keyboard protocol callback handler */
            uint8_t no_leds = 0;

            hid_set_idle(dev_context, 25, 0, wait_obj);

            cb->publish_channel = sync_channel_find("\\uio\\input");
            if(!cb->publish_channel)
            {
                printf("[HID ]: HID Boot keyboard @ %X was unable to connect to the uio input channel!\n", dev_context->full_address);
            }

            sync_object_wait(wait_obj, 1000);

            sync_create_callback_obj(&dev_context->interrupt_sync, cb, &hid_handle_boot_keyboard);

            hid_set_report(dev_context, HID_REPORT_TYPE_OUTPUT, 0, &no_leds, 1, wait_obj);

            sync_object_wait(wait_obj, 1000);
        }
        else if(dev_context->device_types & HID_DEV_TYPE_MOUSE)
        {
            /* Attach the boot mouse protocol handler */
            sync_create_callback_obj(&dev_context->interrupt_sync, cb, &hid_handle_boot_mouse);
        }
        else
        {
            printf("[HID ]: Device @ %X Interface: %u supports boot protocol but is not a keyboard/mouse - Device types: %X\n", dev_context->full_address, dev_context->interface_num, dev_context->device_types);

        }
    }
    else
    {
        sync_create_callback_obj(&dev_context->interrupt_sync, cb, &hid_handle_report);

        printf("[HID ]: Device @ %X Interface: %u does not support the boot protocol!\n", dev_context->full_address, dev_context->interface_num);

    }

    /* Allocate a callback buffer of appropriate size */
    cb->buffer_size = dev_context->report_info.max_input_report_size;
    cb->buffer = (volatile uint8_t *)calloc(1, cb->buffer_size);

    /* Return the device context to the USB coordinator driver and kernel */
    *contextRet = dev_context;

    hid_init_transactions(dev_context);
    wait(750);

    sync_destroy_signal_obj(wait_obj);

    printf("[HID ]: Setup complete for device @ %X\n", dev_context->full_address);


	return driverOpSuccess;
}

/* USB HID Driver deinitialization function */
driverOperationResult hid_deinit(void *context)
{
    hid_destroy_context((hid_dev_context *)context);
	return driverOpSuccess;
}

/* USB HID Driver enumeration function */
driverOperationResult hid_enum(void *context, void *identifyFunc)
{
	return driverOpSuccess;
}




