#include <stdio.h>
#include <stdlib.h>

int main(char **argv, int argc)
{
	char buffer[256], *str;
	printf("Test app running!\n");
	printf("Enter a string: ");
	fflush(stdout);
	str = fgets(buffer, 255, stdin);
	printf("\nYou entered: %s\n", str);
	exit(0);
	return 0;
}
