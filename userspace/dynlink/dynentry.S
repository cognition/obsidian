.global dynLinkEntry, dynInitArrayExec, dynPltStub

.section .text

dynLinkEntry:
	/* On entry from the kernel:
	 * RBP = Dynamic stack
	 * RSP = User stack
   * RDI = argc
   * RSI = argv
	 */
	subq $32, %rbp
	movq %rdi, 24(%rbp)
	movq %rsi, 16(%rbp)
	movq %rsp, 8(%rbp)
	movq %rdx, (%rbp)
	movq %rbp, %rsp
	call main
  /* RAX = Entry address of the program */

	movq (%rbp), %rdx
	movq 8(%rbp), %rsp

  movq 16(%rbp), %rsi
	movq 24(%rbp), %rdi
	/* RDI = argc, RSI = argv */

  /* Jump into the program's entry */
	xorq %rbp, %rbp
	jmp *%rax

dynPltStub:
  /* Save the program or library state */
	xchgq (%rsp), %rdi
	xchgq 8(%rsp), %rsi
	sub $104, %rsp
	mov %rbp, 96(%rsp)
	mov %r15, 88(%rsp)
	mov %r14, 80(%rsp)
	mov %r13, 72(%rsp)
	mov %r12, 64(%rsp)
	mov %r11, 56(%rsp)
	mov %r10, 48(%rsp)
	mov %r9, 40(%rsp)
	mov %r8, 32(%rsp)
	mov %rdx, 24(%rsp)
	mov %rcx, 16(%rsp)
	mov %rbx, 8(%rsp)
	mov %rax, (%rsp)

	call pltLinkHandler

  /* Restore the program or library state */
	mov 96(%rsp), %rbp
	mov 88(%rsp), %r15
	mov 80(%rsp), %r14
	mov 72(%rsp), %r13
	mov 64(%rsp), %r12
	mov 56(%rsp), %r11
	mov 48(%rsp), %r10
	mov 40(%rsp), %r9
	mov 32(%rsp), %r8
	mov 24(%rsp), %rdx
	mov 16(%rsp), %rcx
	mov 8(%rsp), %rbx

	add $104, %rsp

	xchgq (%rsp), %rdi
	xchgq 8(%rsp), %rsi

	add $16, %rsp
	/* RAX = Resolved function address
	   RSP = Points to the return address of the caller
	*/
	jmp *%rax

dynInitArrayExec:
.execLoop:
	test %rsi, %rsi
	jz .end
	mov (%rdi), %rax
	push %rdi
	push %rsi
	push %rdx
	call *%rax
	pop %rdx
	pop %rsi
	pop %rdi
	add $8, %rdi
	sub $8, %rsi
	jmp .execLoop
.end:
	ret
