#ifndef COMPILER_H_
#define COMPILER_H_

#define NULL                          0

#define TRUE                          1
#define FALSE                         0


#ifdef __GNUC__

	#define COMPILER_PACKED             __attribute__((packed))
	#define COMPILER_ALIGN(x)           __attribute__((aligned(x)))

	#define COMPILER_GET_LSB(x)         __builtin_ffsl(x);

	typedef __UINT8_TYPE__    uint8_t;
	typedef __UINT16_TYPE__   uint16_t;
	typedef __UINT32_TYPE__   uint32_t;
	typedef __UINT64_TYPE__   uint64_t;

	typedef __INT8_TYPE__    int8_t;
	typedef __INT16_TYPE__   int16_t;
	typedef __INT32_TYPE__   int32_t;
	typedef __INT64_TYPE__   int64_t;

	typedef __SIZE_TYPE__    size_t;
	typedef __UINTPTR_TYPE__ uintptr_t;
#endif

#endif
