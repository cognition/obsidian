#ifndef DYNLINK_H_
#define DYNLINK_H_

int initResolveDynReloc(const int dIndex);
void bindTableInfo(const int slot, const int bindSymTab);
void dlPrint(const char *msg);
void dlPrintDec(uint64_t num);

#endif
