#include "compiler.h"
#include "string.h"
#include "dyncache.h"
#include "dynlink.h"

#define HASH_TAB_SIZE                   256

#define FNV_32_PRIME                    (0x01000193UL)

#define SYSCALL_DYNLINK_LOAD_LIB        0x8000002U



typedef struct COMPILER_PACKED
{
	const char * namePointer;
	uint32_t hashValue, tableIndex;
} hashTableEntry;

static hashTableEntry COMPILER_ALIGN(4096) libs[HASH_TAB_SIZE];

static uint32_t fnv_32a_str(const char *str)
{
	uint32_t hval = 0;
	unsigned char *s = (unsigned char *)str;	/* unsigned string */

    /*
     * FNV-1a hash each octet in the buffer
     */
	while (*s) {

	/* xor the bottom with the current octet */
		hval ^= (uint32_t)*s++;
		hval += (hval<<1) + (hval<<4) + (hval<<7) + (hval<<8) + (hval<<24);
	}

    /* return our new hash value */
    return hval;
}

/*! Attempts to load the requested library
 *! \param name Library being requested
 *! \return The index in the read only link table for the library or -1 if an error was encountered.
 */
static int loadLibrary(const char *name)
{
	int retCode;
	asm volatile (  "syscall\n": "=a" (retCode) : "D" (SYSCALL_DYNLINK_LOAD_LIB), "S" (name), "d" (strlen(name)): "r11", "rcx", "memory");
	return retCode;
}

/* Gets the userspace index of a requested library
 * \param name String identifying the library to find or load
 * \return Index of the loaded library or -1 if the library could not be loaded
 */
int getLibIndex(const char *name)
{
	int tabIndex;

	/* Hash the name string for a hash table look up */
	uint32_t hash = fnv_32a_str(name);

  /* Check if the library is already in the hash table */
	tabIndex = hash%HASH_TAB_SIZE;
	if(libs[tabIndex].hashValue == hash)
	{
		if(!strcmp(name, libs[tabIndex].namePointer))
			return libs[tabIndex].tableIndex;
	}
	else
	{
		/* If not than ask the kernel to load the library */
		int index = loadLibrary(name);
		if(index <= 0)
			return -1;

		/* Bind the corresponding user space dynamic table information structure
		   to this library */
		bindTableInfo(index, TRUE);

		libs[tabIndex].namePointer = name;
		libs[tabIndex].hashValue = hash;
		libs[tabIndex].tableIndex = index;

		/* Resolve the library's relocations */
		initResolveDynReloc(index);

		return index;
	}
	return -1;
}
