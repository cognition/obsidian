#include "string.h"

static inline int min(const int first, const int second)
{
  return (first > second ? second:first);
}

static inline int max(const int first, const int second)
{
  return (first < second ? second:first);
}


int strcmp(const char *first, const char *second)
{
  int firstLength, secondLength;
  firstLength = strlen(first);
  secondLength = strlen(second);

  for(int i = 0; i < max(firstLength, secondLength); i++)
  {
    if(first[i] < second[i])
      return -1;
    else if(first[i] > second[i])
      return 1;
  }

  return 0;
}

int strlen(const char *str)
{
  int i;
  for(i = 0; i < 255; i++)
    if(str[i] == 0)
      break;

  return i;
}
