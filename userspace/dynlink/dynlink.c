#include "compiler.h"
#include "string.h"
#include "elf_const.h"
#include "dyncache.h"
#include "dynlink.h"
#include "dynerror.h"

#define SYSCALL_EXIT                  0x0000001U
#define SYSCALL_DYNLINK_INIT_TABLE    0x8000001U
#define SYSCALL_DYNLINK_MSG_PRINT     0x8000003U

/* Dynamic table presence flags */
#define DYN_PLT_SIZE_PRESENT          0x000001U
#define DYN_GOT_PRESENT               0x000002U
#define DYN_HASH_PRESENT              0x000004U
#define DYN_STRTAB_PRESENT            0x000008U
#define DYN_SYMTAB_PRESENT            0x000010U
#define DYN_RELA_PRESENT              0x000020U
#define DYN_RELASZ_PRESENT            0x000040U
#define DYN_RELAENTSZ_PRESENT         0x000080U
#define DYN_STRSZ_PRESENT             0x000100U
#define DYN_SYMSZ_PRESENT             0x000200U
#define DYN_INIT_PRESENT              0x000400U
#define DYN_FINI_PRESENT              0x000800U
#define DYN_SYMBOLIC_PRESENT          0x001000U
#define DYN_REL_PRESENT               0x002000U
#define DYN_RELSZ_PRESENT             0x004000U
#define DYN_RELENTSZ_PRESENT          0x008000U
#define DYN_PLT_PRESENT               0x010000U
#define DYN_BIND_NOW                  0x020000U
#define DYN_INITARR_PRESENT           0x040000U
#define DYN_FINIARR_PRESENT           0x080000U
#define DYN_INITARRSZ_PRESENT         0x100000U
#define DYN_FINIARRSZ_PRESENT         0x200000U
#define DYN_IS_PIE                    0x400000U

/* Dynamic table component valid values */
#define DYN_VALID_PLT                 (DYN_PLT_SIZE_PRESENT | DYN_PLT_PRESENT | DYN_GOT_PRESENT)
#define DYN_VALID_STRTAB              (DYN_STRTAB_PRESENT | DYN_STRSZ_PRESENT)
#define DYN_VALID_SYMTAB              (DYN_SYMTAB_PRESENT | DYN_SYMSZ_PRESENT | DYN_VALID_STRTAB)
#define DYN_VALID_RELA                (DYN_RELA_PRESENT | DYN_RELASZ_PRESENT | DYN_RELAENTSZ_PRESENT | DYN_VALID_SYMTAB)
#define DYN_VALID_REL                 (DYN_REL_PRESENT | DYN_RELSZ_PRESENT | DYN_RELENTSZ_PRESENT | DYN_VALID_SYMTAB)
#define DYN_VALID_INITARR             (DYN_INITARR_PRESENT | DYN_INITARRSZ_PRESENT)
#define DYN_VALID_FINIARR             (DYN_FINIARR_PRESENT | DYN_FINIARRSZ_PRESENT)


#define DINFO_MAX_TABLE_COUNT         256
#define DINFO_MAP_COUNT               (DINFO_MAX_TABLE_COUNT/64)


typedef Elf64_Dyn dynEntryType;
typedef Elf64_Sym symEntryType;
typedef Elf64_Rela relaRelocType;

typedef struct
{
	dynEntryType *dynTable;
	size_t dynTableSize;
	uintptr_t baseAddress;
	union
	{
		uintptr_t entry;
		size_t symTabSize;
	} typeSpecific;
} kDynEntry;

/* Dynamic information table */
typedef struct COMPILER_PACKED
{
	dynEntryType *dynEntries;
	uintptr_t dynCount;

	uintptr_t pltBase, pltCount, pltSize;

	uintptr_t relaBase, relaSize, relaEntSize;

	uintptr_t strBase, strSize;
	uintptr_t symBase, symEntSize, symTotalSize;

	uintptr_t hashBase;
	uintptr_t gotBase;

	uintptr_t finiAddr, finiArr, finiArrSize;

	uint32_t flags, reqLibRemCount;
	uint64_t loadedReferenceMap[DINFO_MAP_COUNT];

} dynamicTableInfo;

typedef enum
{
	dlSuccess,
	dlInvalidDT,
	dlInvalidReloc,
	dlInvalidSymbol,
	dlLibNotFound,
	dlSymNotFound,
	dlLimit
} dlStatus;


/* Multisize relocation union */
typedef union
{
	uint64_t word64;
	int64_t sword64;
	uint32_t word32;
	int32_t sword32;
	uintptr_t wordclass;
	uint16_t word16;
	int16_t sword16;
	uint8_t word8;
	int8_t sword8;
} relocField;

/* Relocation logic flags */
#define RELOC_OP_FLAGS_LOCAL_ONLY               0x1U
#define RELOC_OP_FLAGS_RELA                     0x2U

/* Read only kernel dynamic info */
typedef struct __attribute__ ((packed ))
{
	/* An array of kernel dynamic information structures */
	kDynEntry *kDynTable;
	/* Count and capacities information about loaded libraries */
	uint32_t currentMaxEntry, softLibLimit, hardLibLimit, failCode;
} kDynInfo;

typedef void (*funcPtr)(void);


static kDynInfo aSpaceTable;
static __attribute__ ((aligned(4096))) dynamicTableInfo dTabInfo[256];

extern void dynPltStub(uintptr_t index, uintptr_t tableIndex);
extern void dynInitArrayExec(uintptr_t arrayBase, uintptr_t arrayLength, const uintptr_t baseAddress);

static int attemptExternalResolve(relaRelocType *entry, const uintptr_t imageBase, const symEntryType *target, dynamicTableInfo *dTable, uintptr_t *retPtr);

static void linkerExit(int code)
{
	asm volatile (  "syscall\n":: "D" (SYSCALL_EXIT), "S" (code): "r11", "rcx", "memory");
}

static void memcpy(void *dest, void *src, uintptr_t count)
{
	uintptr_t d_addr, s_addr, rem;
	uint64_t *s_qptr, *d_qptr;
	uint32_t d_pre, s_pre;
	uint8_t *s_ptr, *d_ptr;
	rem = count;
	d_addr = (uintptr_t)dest;
	s_addr = (uintptr_t)src;

	d_pre = (8 - (d_addr % 8)) % 8;
	s_pre = (8 - (s_addr % 8)) % 8;

	if(s_pre | d_pre)
	{
		if(s_pre <= count)
		{
			uint8_t *d_ptr, *s_ptr;
			d_ptr = (uint8_t *)d_addr;
			s_ptr = (uint8_t *)s_addr;
			for(uint32_t pre = 0; pre < s_pre; pre++)
			{
				*d_ptr = *s_ptr;
				d_ptr++;
				s_ptr++;
			}
			rem -= s_pre;
			s_addr += s_pre;
			d_addr += s_pre;
		}
	}

	s_qptr = (uint64_t *)s_addr;
	d_qptr = (uint64_t *)d_addr;

	while(rem >= 64)
	{
		d_qptr[0] = s_qptr[0];
		d_qptr[1] = s_qptr[1];
		d_qptr[2] = s_qptr[2];
		d_qptr[3] = s_qptr[3];
		d_qptr[4] = s_qptr[4];
		d_qptr[5] = s_qptr[5];
		d_qptr[6] = s_qptr[6];
		d_qptr[7] = s_qptr[7];

		s_qptr = &s_qptr[8];
		d_qptr = &d_qptr[8];

		rem -= 64;
	}

	s_ptr = (uint8_t *)s_qptr;
	d_ptr = (uint8_t *)d_qptr;
	for(uint32_t index = 0; index < rem; index++)
		d_ptr[index] = s_ptr[index];
}

void dlPrint(const char *msg)
{
	int msgLength = strlen(msg);
	asm volatile("syscall\n":: "D" (SYSCALL_DYNLINK_MSG_PRINT), "S" (msg), "d" (msgLength): "r11", "rcx");
}

/*! Decodes PLT entries for lazy linking
 *  \sa dynPltStub
 *  \param dTableEntry Entry of relocation's dynamic table
 *  \param relocIndex Index of the relocation
 *  \return Address of target function
 */
uintptr_t pltLinkHandler(uint32_t dTableEntry, uint64_t relocIndex)
{

	uintptr_t address;
	int dlResult;
	/* Get the table, entry, relocation and symbol pointers for the PLT entry */
	dynamicTableInfo *dTable = &dTabInfo[dTableEntry];
	const kDynEntry *entry = &aSpaceTable.kDynTable[dTableEntry];
	relaRelocType *reloc = (relaRelocType *)(dTable->pltBase + dTable->relaEntSize*relocIndex);
	symEntryType *sym = (symEntryType *)(dTable->symBase + dTable->symEntSize*( ELF64_R_SYM(reloc->info) ) );
	/* Make sure the provided entries are sane */
	if((dTableEntry >= DINFO_MAX_TABLE_COUNT) || !entry->baseAddress)
		linkerExit(DYNLINK_PLT_CORRUPT);

	/* Attempt to resolve the relocation */
	dlResult = attemptExternalResolve(reloc, entry->baseAddress, sym, dTable, &address);
	if(dlResult != dlSuccess)
		linkerExit(DYNLINK_UNRESOLVED);

	return address;
}

/*! Resolves a relocation entry
 *  \param reloc The relocation entry
 *  \param sym The symbol entry
 *  \param imageBase The base address of the in memory image refered to by the relocation
 *  \param sharedBase The base address of the in memory image which contains symbol used in resolution
 *  \param dTable The dynamic table
 *  \param opFlags Flags determining behavior of the operation
 *  \param retPtr Secondary target output pointer for the relocation value
 *  \return dlSuccess If the relocation was resolved successfully or dlInvalidReloc if the relocation is an invalid type for runtime resolution
 */
static int __attribute__ ((noinline)) resolveReloc(relaRelocType *reloc, const symEntryType *sym, const uintptr_t imageBase, const uintptr_t sharedBase,
	const dynamicTableInfo *dTable, const uint32_t opFlags, uintptr_t *retPtr)
{
	relocField *addend, *offsetPtr, relocValue;
	uint32_t relocationType = ELF64_R_TYPE(reloc->info);
	uintptr_t value;

	/* Get the target memory address for the relocation and addend portion of the relocation */
	offsetPtr = (relocField *)(reloc->offset + imageBase);
	addend = (relocField *)(&reloc->addend);

	/* Calculate the actual relocation values and update the in memory values */
	switch( (elfX86_64_Reloc)relocationType )
	{
		case R_X86_64_64:
			relocValue.word64 = sym->value + addend->word64 + sharedBase;
			offsetPtr->word64 = relocValue.word64;
			break;
		case R_X86_64_GLOB_DAT:
			offsetPtr->wordclass = sym->value + sharedBase;
			break;
		case R_X86_64_JUMP_SLOT:
			/* Might be able to resolve ahead of time */
			value = sharedBase;
			value += sym->value;
			offsetPtr->wordclass = value;
			/* Jump slots also require a return value */
			if(retPtr)
				*retPtr = value;
			break;
		case R_X86_64_RELATIVE:
			relocValue.wordclass = sharedBase + addend->wordclass;
			offsetPtr->wordclass = relocValue.wordclass;
			break;
		/* TODO: TLS specific relocation that need to be implemented */
		case R_X86_64_DTPMOD64:
			break;
		case R_X86_64_DTPOFF64:
			break;
		default:
			/* Shouldn't occur in executable */
			return dlInvalidReloc;
			break;
	}

	return dlSuccess;
}

/*! Attempts to resolve relocation against an external library
 * \param entry Relocation entry pointer
 * \param relocImageBase Base address of the relocation's memory image
 * \param target Potential target symbols
 * \param relocDTable Relocation entry's dynamic table
 * \param inquiryIndex Index of the library relocations are being attempted against
 * \param retPtr Return pointer for the relocation's value (ignored if NULL)
 * \return dlSucess if the relocation was resolved, dlInvalidReloc if the relocation is malformed or dlSymNotFound if the symbol was not present in the library
 */
static int crossResolve(relaRelocType *entry, const uintptr_t relocImageBase, const symEntryType *target,
	const dynamicTableInfo *relocDTable, const uint32_t inquiryIndex, uintptr_t *retPtr) {

	const char *targetString, *symString;
	const kDynEntry *inquiryDynEntry;
	uint32_t targetType;
	const dynamicTableInfo *inquiryTable;

	/* Get the information of library being used for symbol resolution */
	inquiryDynEntry = &aSpaceTable.kDynTable[inquiryIndex];
	inquiryTable = &dTabInfo[inquiryIndex];

	/* Make sure the string offset value is sane */
	if(target->symName >= relocDTable->strSize)
		return dlInvalidReloc;

	targetType = ELF64_SYM_TYPE(target->symInfo);
	targetString = (const char *)(relocDTable->strBase + target->symName);

  /* TODO: GNU Hash table based resolution at some point */
	/* Crawl the symbol table and try to match the target symbol */
	for(uintptr_t offset = 0; offset < inquiryTable->symTotalSize; offset += inquiryTable->symEntSize)
	{
		uint32_t inquiryType, inquiryBind;
		symEntryType *inquiry = (symEntryType *)(inquiryTable->symBase + offset);

		inquiryType = ELF64_SYM_TYPE(inquiry->symInfo);
		inquiryBind = ELF64_SYM_BIND(inquiry->symInfo);

		/* If the symbol is of the right type and is defined attempt to link against it */
		if(inquiryType == targetType && (inquiryBind == STB_GLOBAL || inquiryBind == STB_WEAK) )
		{
			const char *inquiryName;
			inquiryName = (const char *)(inquiryTable->strBase + inquiry->symName);

			if(strcmp(inquiryName, targetString) == 0)
				return resolveReloc(entry, inquiry, relocImageBase, inquiryDynEntry->baseAddress, relocDTable, RELOC_OP_FLAGS_RELA, retPtr);
		}
	}

	return dlSymNotFound;
}

/* Arch specific LSB supprt function */
static uint32_t getLsb64(const uint64_t value)
{
	uint64_t ret;
	asm ("bsfq %1, %0\n": "=r" (ret): "r" (value));
	return ret;
}

/*! Attempts to resolve a relocation entry by crawling the required library entries
 *  \param entry Relocation entry pointer
 *  \param imageBase Base address of memory image correpsonding to the relocation
 *  \param target Symbol to be resolved
 *  \param dTable Linker dynamic information table entry for the relocation's memory image
 *  \param retPtr Return pointer for the value resolved (ignored if NULL)
 *  \return dlSuccess on Successful resolution of the relocation, dlLibNotFound if a required library cannot be loaded,
 *		dlInvalidDT if the ELF dynamic table is corrupted, dlSymNotFound if the symbol cannot be found in any of the libaries,
 *		dlInvalidReloc if the relocation entry is corrupt.
 */
static int attemptExternalResolve(relaRelocType *entry, const uintptr_t imageBase, const symEntryType *target, dynamicTableInfo *dTable, uintptr_t *retPtr)
{
	const char *symName;
	uint32_t baseIndex = 0;
	/* Check all the loaded libraries we know are attached to this in memory image first and attempt to resolve against them */
	for(int mapIndex = 0; mapIndex < DINFO_MAP_COUNT; mapIndex++, baseIndex += 64)
	{
		/* Iterate through the bitmap dword by dword */
		uint64_t currentMap, visitedMask = 0;
		currentMap = dTable->loadedReferenceMap[mapIndex];
		/* Check each entry in the dword bitmap */
		while(currentMap ^ visitedMask)
		{
			uint32_t maskIndex;
			uint64_t entryMask = 1;
			maskIndex = getLsb64(currentMap ^ visitedMask);
			entryMask <<= maskIndex;
			visitedMask |= entryMask;
			maskIndex += baseIndex;

			/* Attempt to resolve using the indicated loaded library */
			if( crossResolve(entry, imageBase, target, dTable, maskIndex, retPtr) == dlSuccess)
				return dlSuccess;
		}
	}

	/* Attempt to load any unloaded libraries and resolve the relocation again them */
	if(dTable->reqLibRemCount)
	{
		dynEntryType *dEntry = dTable->dynEntries;
		/* Iterate through the DT_NEEDED entries to find the required libraries */
		for(int dtIndex = 0; dtIndex < dTable->dynCount; dtIndex++, dEntry++)
		{
			if(dEntry->d_tag == DT_NEEDED)
			{
				int libIndex, wordIndex;
				uintptr_t stringAddr;
				const char *strPtr;
				uint64_t mask = 1;

				/* Entry sanity check */
				if(dEntry->d_un.d_val >= dTable->strSize)
					return dlInvalidDT;

				/* Construct the string pointer for the library's name or path */
				stringAddr = dTable->strBase;
				stringAddr += dEntry->d_un.d_val;
				strPtr = (const char *)stringAddr;

				/* Retrieve the index of the library */
				libIndex = getLibIndex(strPtr);
				/* If a needed library can't be loaded return an appropriate error */
				if(libIndex < 0)
					return dlLibNotFound;
				/* Calculate a mask value and index to determine if we've already checked against this library */
				wordIndex = libIndex/64;
				mask <<= (libIndex%64);
				if(dTable->loadedReferenceMap[wordIndex] & mask)
					continue;
				else
				{
					/* If not then update the reference mask, require library count and attempt to resolve */
					dTable->loadedReferenceMap[wordIndex] |= mask;
					dTable->reqLibRemCount--;
					if( crossResolve(entry, imageBase, target, dTable, libIndex, retPtr) == dlSuccess)
						return dlSuccess;
					/* If all libraries have been loaded stop parsing the ELF Dynamic table for DT_NEEDED entries */
					if(!dTable->reqLibRemCount)
						break;
				}
			}
		}
	}

	/* Check for the case where there's an undefined weak symbol that can be zeroed out */
	symName = (const char *)(target->symName + dTable->strBase);

  if( ELF64_SYM_BIND(target->symInfo) == STB_WEAK)
	{
		uint32_t relocationType = ELF64_R_TYPE(entry->info);
		if(relocationType == R_X86_64_GLOB_DAT)
		{
			/* Undefined weak relocations are simply set to zero if they no defintion exists for them */
			uint64_t zero = 0;
			relocField *field = (relocField *)(entry->offset + imageBase);
			field->wordclass = zero;
			return dlSuccess;
		}
		else
		{
			dlPrint("Unknown weak undef relocation.");
			return dlSymNotFound;
		}
	}
	else
	{

		dlPrint("Unable to resolve symbol: ");
		dlPrint(symName);

		return dlSymNotFound;
	}

}

/*! Attempts to resolve all the relocations in a given memory image other than runtime jump slots
 *  \param imageBase Base address of the memory image
 *  \param tableBase Base address of the relocation table
 *  \param tableSize In memory size of the relocation table
 *  \param tableEntrySize Size of each relocation entry
 *  \param dTable Pointer to the dynamic linker's information table for the memory image
 *  \param opFlags Operations flag that control resolution behavior
 *  \return dlSuccess if successful, other values upon errors
 */
static int __attribute__ ((noinline)) resolveRelocTable(const uintptr_t imageBase, const uintptr_t tableBase,
	const uintptr_t tableSize, const uintptr_t tableEntrySize, dynamicTableInfo *dTable,
	const uint32_t opFlags) {

	/* Iterate through all the relocations in a given table and attempt to resolve them */
	for(uintptr_t tableOffset = 0; tableOffset < tableSize; tableOffset += tableEntrySize)
	{
		uint32_t symIndex;
		symEntryType *sym;
		relaRelocType *entry = (Elf64_Rela *)(tableOffset + tableBase);
		uint32_t relocationType = ELF64_R_TYPE(entry->info);
		symIndex = ELF64_R_SYM(entry->info);

		sym = (Elf64_Sym *)(dTable->symBase + dTable->symEntSize*symIndex);

		if(sym->symSectionIndex == SHN_UNDEF && sym->symName)
		{
			if(relocationType == R_X86_64_JUMP_SLOT)
			{
				/* Adjust each of the default entries to take into account the base address */
				relocField *offsetPtr = (relocField *)(entry->offset + imageBase);
				uintptr_t newValue = offsetPtr->wordclass;
				newValue += imageBase;
				offsetPtr->wordclass = newValue;
			}
			else if( attemptExternalResolve(entry, imageBase, sym, dTable, NULL) != dlSuccess)
			{
				/* Externally defined symbol, not local to this object */
				dlPrint("Error: Encountered invalid external relocation");
				dlPrint( (const char *)(dTable->strBase + sym->symName));
				return dlInvalidReloc;
			}
		}
		else
		{
			if( resolveReloc(entry, sym, imageBase, imageBase, dTable, opFlags, NULL ) != dlSuccess)
			{
				dlPrint("Error: Encountered invalid relocation");
				return dlInvalidReloc;
			}
		}
	}

	return dlSuccess;
}

void dlPrintDec(uint64_t num)
{
	unsigned char digits[21];
	int digitOffset = 20;
	digits[digitOffset] = 0;
	do
	{
		digits[--digitOffset] = (num%10 + 48);
		num /= 10;
	} while(num);
	dlPrint(&digits[digitOffset]);
}

/*!
	\brief Jump slot fixup, points jump slots to the correct address in relocatable objects


	\details Jump slots assume a base of zero for relocatable objects, however in reality they will always be reloaded to some other base address.
	As such, it is necessary to adjust the value of all jump slots by adding the proper base address so that the lazy linking mechanism will
	function properly.

	\param imageBase Base address of the object
	\param tableBase Base address of the PLT relocation table
	\param tableSize Total size of the table
	\param tableEntrySize Size of each entry in the table
	\param dTable Pointer to the dynamic information table for this object
*/
static int jumpRelocate(const uintptr_t imageBase, const uintptr_t tableBase,
	const uintptr_t tableSize, const uintptr_t tableEntrySize, const dynamicTableInfo *dTable,
	const uint32_t opFlags) {

		if(!imageBase)
		{
			/* If the loaded base address is 0 then there's no fixup that needs to be done */
			return dlSuccess;
		}

	/* Iterate through all entires in the jump table */
	for(uintptr_t tableOffset = 0; tableOffset < tableSize; tableOffset += tableEntrySize)
	{
		uint32_t symIndex, symBinding;
		Elf64_Sym *sym;
		Elf64_Rela *entry = (Elf64_Rela *)(tableOffset + tableBase);
		uint32_t relocationType = ELF64_R_TYPE(entry->info);
		symIndex = ELF64_R_SYM(entry->info);

		sym = (Elf64_Sym *)(dTable->symBase + dTable->symEntSize*symIndex);
		symBinding = ELF64_SYM_BIND(sym->symInfo);

		if(relocationType == R_X86_64_JUMP_SLOT)
		{
			uintptr_t value;
			relocField *offsetPtr = (relocField *)(entry->offset + imageBase);
			if(sym->symSectionIndex == SHN_UNDEF)
			{
				/* Adjust each of the default entries to take into account the base address */
				value = offsetPtr->wordclass;
			}
			else
			{
				/* Use the provided symbol value */
				value = sym->value;
			}
			value += imageBase;
			offsetPtr->wordclass = value;
		}
		else
			return dlInvalidReloc;
	}

	return dlSuccess;
}

/*! Performs the inital processing of an in memory image (library or executable)
 *  This includes the parsing of the ELF dynamic table, linker structure setup and any initial relocations which can or must be resolved.
 *
 * \param dIndex Index of the in memory image's structures
 * \return dlSuccess if successful, an error code indicating any issues encountred \sa dlStatus
 */
int __attribute__ ((noinline)) initResolveDynReloc(const int dIndex)
{
	const kDynEntry *kEntry = &aSpaceTable.kDynTable[dIndex];
	const dynEntryType *entry;
	uint64_t mask = 1;
	uintptr_t initAddr, initArr, initArrSize;
	dynamicTableInfo *dTable = &dTabInfo[dIndex];
	const dynEntryType *dEntries = dTable->dynEntries;
	const uintptr_t selfBase = kEntry->baseAddress;

	/* Initialize the reference map */
	dTable->loadedReferenceMap[0] = 0;
	dTable->loadedReferenceMap[1] = 0;
	dTable->loadedReferenceMap[2] = 0;
	dTable->loadedReferenceMap[3] = 0;

	dTable->reqLibRemCount = 0;
	entry = dEntries;

	/* Process the ELF dynamic table */
	for(int dynIndex = 0; dynIndex < dTable->dynCount; dynIndex++, entry++)
	{
		if(entry->d_tag <= DT_FINI_ARRAYSZ)
		{
			switch(entry->d_tag)
			{
				case DT_NEEDED:
					/* Required library, just keep track of it count wise */
					dTable->reqLibRemCount++;
					break;
				case DT_NULL:
					/* Null entry indicates the end of the array, set the exit condition */
					dynIndex = dTable->dynCount;
					break;
				case DT_PLTRELSZ:
					/* PLT Relocation size */
					dTable->pltSize = entry->d_un.d_val;
					dTable->flags |= DYN_PLT_SIZE_PRESENT;
					break;
				case DT_PLTGOT:
					/* PLT GOT Address */
					dTable->gotBase = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_GOT_PRESENT;
					break;
				case DT_HASH:
				  /* GNU Hash table extension */
					dTable->hashBase = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_HASH_PRESENT;
					break;
				case DT_STRTAB:
					/* String table offset */
					dTable->strBase = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_STRTAB_PRESENT;
					break;
				case DT_SYMTAB:
					/* Symbol table offset */
					dTable->symBase = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_SYMTAB_PRESENT;
					break;
				case DT_RELA:
					/* RELA type relocation table offset */
					dTable->relaBase = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_RELA_PRESENT;
					break;
				case DT_RELASZ:
					/* RELA table total size */
					dTable->relaSize = entry->d_un.d_val;
					dTable->flags |= DYN_RELASZ_PRESENT;
					break;
				case DT_RELAENT:
					/* RELA relocation entry size */
					dTable->relaEntSize = entry->d_un.d_val;
					dTable->flags |= DYN_RELAENTSZ_PRESENT;
					break;
				case DT_STRSZ:
					/* String table total size */
					dTable->strSize = entry->d_un.d_val;
					dTable->flags |= DYN_STRSZ_PRESENT;
					break;
				case DT_SYMENT:
					/* Symbol table entry size */
					dTable->symEntSize = entry->d_un.d_val;
					dTable->flags |= DYN_SYMSZ_PRESENT;
					break;
				case DT_INIT:
					/* Initialization function offset */
					initAddr = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_INIT_PRESENT;
					break;
				case DT_FINI:
					/* Finalizer function offset */
					dTable->finiAddr = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_FINI_PRESENT;
					break;
				case DT_SONAME:
					/* TODO: Probably necessary in the future for library symbolic links and versioning */
					break;
				case DT_RPATH:
					/* TODO: Library search path, will require another syscall to register w/ kernel */
					break;
				case DT_SYMBOLIC:
					dTable->flags |= DYN_SYMBOLIC_PRESENT;
					break;
				case DT_REL:
					/* REL Type relocation table offset, should not be present on x86_64 */
					dTable->flags |= DYN_REL_PRESENT;
					break;
				case DT_PLTREL:
					/* PLT relocation type, should always be RELA on x86_64 */
					if( entry->d_un.d_val != DT_RELA)
					{
						dlPrint("Error: All objects must use RELA style PLT relocations for this platform.\n");
						return dlInvalidDT;
					}
					break;
				case DT_DEBUG:
					/* Ignore as this is not a debugger */
					break;
				case DT_TEXTREL:
					/* Ignored per specification */
					break;
				case DT_JMPREL:
					/* PLT offset */
					dTable->pltBase = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_PLT_PRESENT;
					break;
				case DT_BIND_NOW:
					/* Ahead of time binding flag */
					dTable->flags |= DYN_BIND_NOW;
					break;
				case DT_INIT_ARRAY:
					/* Initializer array */
					initArr = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_INITARR_PRESENT;
					break;
				case DT_FINI_ARRAY:
					/* Finalizer array */
					dTable->finiArr = selfBase + entry->d_un.d_ptr;
					dTable->flags |= DYN_FINIARR_PRESENT;
					break;
				case DT_INIT_ARRAYSZ:
					/* Initializer array size */
					initArrSize = entry->d_un.d_val;
					dTable->flags |= DYN_INITARRSZ_PRESENT;
					break;
				case DT_FINI_ARRAYSZ:
					/* Finalizer array size */
					dTable->finiArrSize = entry->d_un.d_val;
					dTable->flags |= DYN_FINIARRSZ_PRESENT;
					break;
				default:
					break;
			}
		}
		else if(entry->d_tag == DT_FLAGS_1)
		{
			/* Check the extra flags for a position independent executable */
			if(entry->d_un.d_val & FLAGS_1_PIE)
				dTable->flags |= DYN_IS_PIE;
		}
	}

	/* Rel type entries not supported on the x86_64 platform */
	if(dTable->flags & DYN_REL_PRESENT)
	{
		dlPrint("Error - REL Type relocations should not be present on this platform");
		return dlInvalidDT;
	}

	/* RELA type entries are though, make sure they're valid if present */
	if(dTable->flags & DYN_RELA_PRESENT)
	{
		if((dTable->flags & DYN_VALID_RELA) != DYN_VALID_RELA)
			return dlInvalidDT;
		else
		{
			int tResult = resolveRelocTable(selfBase, dTable->relaBase, dTable->relaSize, dTable->relaEntSize, dTable, RELOC_OP_FLAGS_RELA);
			if(tResult != dlSuccess)
			{
				dlPrint("Failed to process RELA relocation table!");
				return tResult;
			}
		}
	}


	/* Deal with the PLT if present */
	if(dTable->flags & DYN_PLT_PRESENT)
	{
		/* Update the first two GOT entries so lazy PLT linking works */
		uintptr_t *GOT = (uintptr_t *)dTable->gotBase;
		uint32_t opFlags = (dTable->flags & DYN_BIND_NOW) ? 0:RELOC_OP_FLAGS_LOCAL_ONLY;

		int tResult;
		uintptr_t entrySize = 0;
		/* The PLT also requires an associated relocation type to be valid */
		if((dTable->flags & DYN_VALID_PLT) != DYN_VALID_PLT)
			return dlInvalidDT;
		else
		{
			entrySize = dTable->relaEntSize;
			opFlags |= RELOC_OP_FLAGS_RELA;
		}
		/* Relocate the jump table entries */
		tResult = jumpRelocate(selfBase, dTable->pltBase, dTable->pltSize, entrySize, dTable, RELOC_OP_FLAGS_RELA);
		if(tResult != dlSuccess)
			return tResult;


		/* ELF memory image specific data pointer */
		GOT[1] = dIndex;
		/* Dynamic linker lazy evaluation function for PLT entries */
		GOT[2] = (uintptr_t)&dynPltStub;
	}


	/* Validate initialization function, if present and call initializer */
	if(dTable->flags & DYN_INIT_PRESENT)
	{
		funcPtr init = (funcPtr)initAddr;
		(*init)();
	}

	/* Validate initializer array, if present */
	if(dTable->flags & DYN_INITARR_PRESENT)
	{
		if((dTable->flags & DYN_VALID_INITARR) != DYN_VALID_INITARR)
			return dlInvalidDT;
		else
			dynInitArrayExec(initArr, initArrSize, selfBase);
	}

	/* Validate finalizer array, if present */
	if(dTable->flags & DYN_FINIARR_PRESENT)
		if((dTable->flags & DYN_VALID_FINIARR) != DYN_VALID_FINIARR)
			return dlInvalidDT;


	return dlSuccess;
}


static int initLinkTable(const kDynInfo *table)
{
	int retCode;
	asm volatile ( "syscall\n": "=a" (retCode) : "D" (SYSCALL_DYNLINK_INIT_TABLE), "S" (table): "r11", "rcx", "memory");
	return retCode;
}

void bindTableInfo(const int slot, const int bindSymTab)
{
	const kDynEntry *dInfo = &aSpaceTable.kDynTable[slot];
	dynamicTableInfo *tabEntry = &dTabInfo[slot];
	tabEntry->dynEntries = dInfo->dynTable;
	tabEntry->dynCount = dInfo->dynTableSize/sizeof(dynEntryType);
	if(bindSymTab)
		tabEntry->symTotalSize = dInfo->typeSpecific.symTabSize;
}

uintptr_t main(int argc, char **argv)
{
	int result;
	const kDynEntry *progEntry;
	int progIndex = 0;
	/* Bind the linker's userspace limits table */
	result = initLinkTable(&aSpaceTable);
	if(result)
		linkerExit(DYNLINK_TABLE_FAIL);

	/* Update the memory image information table */
	bindTableInfo(progIndex, FALSE);
	progEntry = aSpaceTable.kDynTable;
	/* Perform initial resolution of the executable's memory image */
	result = initResolveDynReloc(progIndex);
	if(result != dlSuccess)
		linkerExit(DYNLINK_UNRESOLVED);

	/* Execute the program */
	return (progEntry->typeSpecific.entry + progEntry->baseAddress);
}
