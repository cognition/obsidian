#ifndef ELF_CONST_H_
#define ELF_CONST_H_

#define STB_LOCAL                    0
#define STB_GLOBAL                   1
#define STB_WEAK                     2

/* ELF64 Symbol table entry structure */
typedef struct COMPILER_PACKED
{
	uint32_t symName;
	uint8_t symInfo, symOther;
	uint16_t symSectionIndex;
	uint64_t value;
	uint64_t symSize;
} Elf64_Sym;

#define ELF64_R_SYM(x)                (x >> 32)
#define ELF64_R_TYPE(x)               (x & 0xFFFFFFFFUL)

#define ELF64_SYM_TYPE(x)             ((x & 0xFU) >> 4)
#define ELF64_SYM_BIND(x)             (x >> 4)

/* Symbol section indexes */
#define SHN_UNDEF                     0

#define FLAGS_1_PIE                   0x8000000

/* 'Rel' type relocation entry */
typedef struct COMPILER_PACKED
{
	uint64_t offset, info;
} Elf64_Rel;

/* 'RELA' type relocation entry */
typedef struct COMPILER_PACKED
{
	uint64_t offset, info, addend;
} Elf64_Rela;

/* ELF64 Relocations for the X86_64 ABI */
typedef enum
{
	R_X86_64_NONE = 0,
	R_X86_64_64,
	R_X86_64_PC32,
	R_X86_64_GOT32,
	R_X86_64_PLT32,
	R_X86_64_COPY,
	R_X86_64_GLOB_DAT,
	R_X86_64_JUMP_SLOT,
	R_X86_64_RELATIVE,
	R_X86_64_GOTPCREL,
	R_X86_64_32,
	R_X86_64_32S,
	R_X86_64_16,
	R_X86_64_PC16,
	R_X86_64_8,
	R_X86_64_PC8,
	R_X86_64_DTPMOD64,
	R_X86_64_DTPOFF64,
	R_X86_64_TPOFF64,
	R_X86_64_TLSGD,
	R_X86_64_TLSLD,
	R_X86_64_DTPOFF32,
	R_X86_64_GOTTPOFF,
	R_X86_64_TPOFF32,
	R_X86_64_PC64,
	R_X86_64_GOTOFF64,
	R_X86_64_GOTPC32,
	R_X86_64_GOT64,
	R_X86_64_GOTPCREL64,
	R_X86_64_GOTPC64,
	_relDep0,
	R_X86_64_PLTOFF64,
	R_X86_64_SIZE32,
	R_X86_64_SIZE64,
	R_X86_64_GOTPC32_TLSDESC,
	R_X86_64_TLSDESC_CALL,
	R_X86_64_TLSDESC,
	R_X86_64_IRELATIVE,
	R_X86_64_RELATIVE64,
	_relDep1,
	_relDep2,
	R_X86_64_GOTPCRELX,
	R_X86_64_REX_GOTPCRELX
} elfX86_64_Reloc;

/* ELF64 Dynamic table entry tags */
typedef enum
{
	DT_NULL = 0,
	DT_NEEDED,
	DT_PLTRELSZ,
	DT_PLTGOT,
	DT_HASH,
	DT_STRTAB,
	DT_SYMTAB,
	DT_RELA,
	DT_RELASZ,
	DT_RELAENT,
	DT_STRSZ,
	DT_SYMENT,
	DT_INIT,
	DT_FINI,
	DT_SONAME,
	DT_RPATH,
	DT_SYMBOLIC,
	DT_REL,
	DT_RELSZ,
	DT_RELENT,
	DT_PLTREL,
	DT_DEBUG,
	DT_TEXTREL,
	DT_JMPREL,
	DT_BIND_NOW,
	DT_INIT_ARRAY,
	DT_FINI_ARRAY,
	DT_INIT_ARRAYSZ,
	DT_FINI_ARRAYSZ,
	DT_FLAGS_1 = 0x6ffffffBU
} elfDtEntryType;


/* ELF64 Dynamic table entry structure */
typedef struct COMPILER_PACKED
{
	int64_t d_tag;
	union
	{
		uint64_t d_val;
		uint64_t d_ptr;
	} d_un;
} Elf64_Dyn;


#endif
