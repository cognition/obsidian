#ifndef DYNLINK_STRING_H_
#define DYNLINK_STRING_H_

int strcmp(const char *first, const char *second);
int strlen(const char *str);

#endif
