/* String fragment implementation file
 *
 * Used wrapping and referencing sub string objects
 *
 * Author: Joseph Kinzel */

#include <string_fragment.h>

/*! Offset constructor from a source string
 *
 *  \param src Source string containing the fragment
 *  \param start Begging of the fragment within the string
 */
stringFragment::stringFragment(const String &src, short start) : begin(start)
{
	dPtr = src.getSharedPtr();
	size = src.length() - begin;
	if(!dPtr.get())
    {
        kprintf("Error empty fragment copy! Purported source length: %u Caller: %p\n", src.length(), COMPILER_FUNC_CALLER);
    }

}

stringFragment::stringFragment(const String &src, short start, short span) : begin(start), size(span)
{
    dPtr = src.getSharedPtr();
}

/*! Offset constructor from another fragment
 *
 *  \param src Source fragment containing the new fragment
 *  \param start Beginning offset of the sub fragment
 */
stringFragment::stringFragment(const stringFragment &src, short start) : begin(start+src.begin)
{
	dPtr = src.dPtr;
	size = src.size - start;
	if(!dPtr.get())
    {
        kprintf("Error empty fragment offset copy! Source length: %u Caller: %p\n", src.length(), COMPILER_FUNC_CALLER);
    }

}

/*! Fully bounded constructor from another fragment
 *
 *  \param src Source fragment containing the new fragment
 *  \param start Offset into the source fragment of the new fragment
 *  \param fragSpan Total length of the new fragment
 */
stringFragment::stringFragment(const stringFragment &src, short start, short fragSpan) : begin(start + src.begin)
{
	if(fragSpan)
	{
		dPtr = src.dPtr;
		size = fragSpan;
	}
	else
    {
		dPtr = String::zeroLengthString;
		size = 0;
    }
	if(!dPtr.get())
    {
        kprintf("Error empty fragment partial copy! Source length: %u fragspan: %u Caller: %p\n", src.length(), fragSpan, COMPILER_FUNC_CALLER);
    }

}

//! Copy assignment
stringFragment &stringFragment::operator =(const stringFragment &other)
{
	dPtr = other.dPtr;
	if(!dPtr.get())
    {
        kprintf("Error empty fragment copy! Caller: %p\n", COMPILER_FUNC_CALLER);
    }

	size = other.size;
	begin = other.begin;
	return *this;
}

//! Move assignment
stringFragment &stringFragment::operator =(stringFragment &&other)
{
    dPtr = other.dPtr;

	if(!dPtr.get())
    {
        kprintf("Error empty fragment temp copy! Caller: %p\n", COMPILER_FUNC_CALLER);
    }

    size = other.size;
    begin = other.begin;
    return *this;
}

/*! Fragment comparison function
 *
 *  \param other Fragment to compare against
 *
 *  \return -1 if the fragments match completely or the first index where the fragments differ
 */
int stringFragment::compare(const stringFragment &other) const
{
    int minSize = (size > other.size) ? other.size:size;

    for(int i = 0; i < minSize; i++)
	{
		if( dPtr->at(i+begin) != other.dPtr->at(i+other.begin))
		{
			return i;
		}
	}
	if(size == other.size)
    {
        return -1;
    }
	else
    {
		return minSize;
    }
}

//! Fragment length accessor
short stringFragment::length() const
{
	return size;
}

//! Fragment character accessor
char stringFragment::operator[](const short index) const
{
	return dPtr->at(begin + index);
}

/*! Shifts the window of a string fragment forward
 *
 *  \param shift Amount to shift the fragment's window
 */
void stringFragment::shift(const short shift)
{
	begin += shift;
	size -= shift;
}

/*! Trims the length of a string fragment
 *
 *  \param amount Amount to reduce the fragment's length by
 */
void stringFragment::trim(const short amount)
{
    size -= amount;
}

//! Raw ascii array conversion accessor
const char *stringFragment::toBuffer(void) const
{
    return &(dPtr->getBuffer()[begin]);
}

/*! Token separated fragment extraction function
 *
 *  \param frag Resulting string fragment
 *  \param str Source string
 *  \param start Starting search index for tokens
 *  \param token Separator token value
 *
 *  \return Index of the terminating token or -1 if the termination was caused by reaching the end of the string
 */
int stringFragment::getNextFragment( stringFragment &frag, const String &str, const int start, const char token)
{
    int nextToken = str.findToken(token, start);
    if(nextToken >= start)
        frag = stringFragment(str, start, nextToken - start);
    else
        frag = stringFragment(str, start);
    /*
    if(nextToken >= start)
        frag.trim(str.length() - nextToken);
    */

    return nextToken;
}
