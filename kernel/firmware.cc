#include "firmware.h"
#include <io/io.h>

#define EBDA_ADDRESS            0x40E
#define ACPI_RSDP_BIOS_BASE     0xE0000
#define ACPI_RSDP_BIOS_LENGTH   0x20000

static const uint64_t acpiRsdpSignature = 0x2052545020445352UL;
static const uint16_t *ebdaAddress = reinterpret_cast <const uint16_t *> ( EBDA_ADDRESS );
static uintptr_t acpiRsdpAddress = 0;

struct COMPILER_PACKED acpiRsdpHeader
{
	uint64_t signature;
	uint8_t checksum, oemId[6], revision;
	uint32_t rsdtAddress, length;
	uint64_t xsdtAddress;
	uint8_t extendedChecksum, _reserved[33];
};

//Verifys a checksum value
bool Firmware::doChecksum( void *data, uint32_t size )
{
	uint8_t * __restrict__ buffer = reinterpret_cast <uint8_t *> ( data );
	uint8_t checkSum = 0;
	while( size )
	{
		checkSum += *buffer;
		buffer++;
		size--;
	}
	return ( !checkSum );
}

static bool searchRSDP( uintptr_t addr, uint32_t length, uintptr_t &rsdpAddress )
{
    using namespace Firmware;
	while( length )
	{
		acpiRsdpHeader *testPointer = reinterpret_cast <acpiRsdpHeader *> ( addr );
		if( testPointer->signature == acpiRsdpSignature )
			if( doChecksum( testPointer, testPointer->length ) )
			{
				rsdpAddress = addr;
				return true;
			}
		addr += 16;
		length -= 16;
	}
	return false;
}

uintptr_t Firmware::getAcpiRSDP(bootLoaderInfo *bli)
{
		if( !acpiRsdpAddress )
		{
            if(bli)
			{
                if(bli->acpiRSDP)
				{
                    acpiRsdpAddress = bli->acpiRSDP;
					kprintf("Found EFI ACPI RSDP Address @ %X\n", acpiRsdpAddress);
					return acpiRsdpAddress;
				}
			}
			{
			uintptr_t ebdaBase = ( *ebdaAddress )<<4;
			if( ebdaBase )
				if( searchRSDP( ebdaBase, 1024, acpiRsdpAddress ) )
                    return acpiRsdpAddress;
            if(!searchRSDP( ACPI_RSDP_BIOS_BASE, ACPI_RSDP_BIOS_LENGTH, acpiRsdpAddress ))
                return 0;
            }
		}
		return acpiRsdpAddress;
}
