#ifndef AVL_TREE_TEMPLATE_
#define AVL_TREE_TEMPLATE_

#include <compiler.h>
#include "allocator.cc"
#include "stack.cc"
#include "comparable.cc"
#include <algo/algo.h>

//! Basic object which contains most of the logic for tree operations for an AVL tree and contains node data
template <typename T, typename comp = defaultComparable<T> > class avlBaseNode
{
	public:
		avlBaseNode() : left( nullptr ), right( nullptr )
		{
            reset();
		}

		/*! Constructor for a new node
		 *  \param object Object to be held by the node
		 */
		avlBaseNode( T object ) : data( object ), left( nullptr ), right( nullptr )
		{
            reset();
		}

		virtual ~avlBaseNode() = default;

		void destroyNode()
		{
		    if(left)
                left->destroyNode();
            if(right)
                right->destroyNode();
            delete this;
		}

		void reset(void)
		{
		    height = 1;
		    leftHeight = 0;
		    rightHeight = 0;
		    balance = 0;
		}

		void setData( T &value )
		{
			data = value;
		}

        void copyData( T &dest) const
        {
            dest = data;
        }

		/*! Attempts to find an existing object node, if one does not exist it inserts
		 *  \param root Base node reference for the operation
		 *  \param obj Object to form a node around if no entry is found
		 *  \param result Resulting node reference field
		 *  \return Height of the current subtree
		 */
		int8_t findOrInsert( class avlBaseNode<T, comp> *&root, T &obj, avlBaseNode<T, comp> *&result, int &count )
		{
		    if( !root )
			{
				root = new avlBaseNode<T, comp>( obj );
				result = root;
				count++;
				return 1;
			}
			else if( comp().greater(data, obj) )
			{
				leftHeight = left->findOrInsert( left, obj, result, count );
				return checkLeftBalance( root );
			}
			else if( comp().less(data, obj) )
			{
				rightHeight = right->findOrInsert( right, obj, result, count );
				return checkRightBalance( root );
			}
			else
			{
				result = this;
				return height;
			}

		}

		/*! Inserts a node into a subtree
		 *  \param root Reference to the current node pointer
		 *  \param node Node to be inserted
		 *  \return height Height of the subtree after the node has been inserted
		 */
		int8_t insert( class avlBaseNode <T, comp> *&root, class avlBaseNode<T, comp> *node )
		{
			if( !root )
			{
				root = node;
				return 1;
			}
			else if( comp().greater(data, node->data) )
			{
				if(left)
                {
                    leftHeight = left->insert( left, node );
                    return checkLeftBalance( root );
                }
                else
                {
                    left = node;
                    leftHeight = 1;
                    balance = leftHeight - rightHeight;
                    height = max(leftHeight, rightHeight) + 1;
                    return height;
                }

			}
			else
			{
			    if(right)
                {
                    rightHeight = right->insert( right, node );
                    return checkRightBalance( root );
                }
                else
                {
                    right = node;
                    rightHeight = 1;
                    balance = leftHeight - rightHeight;
                    height = max(leftHeight, rightHeight) + 1;
                    return height;
                }

			}
		}

		/*! Retrieves a node
		 *  \param p Reference of the node being examined
		 *  \param search Node being searched for
		 *  \param result Reference to a data object that will hold the result value
		 *  \return Height of the subtree after the object has been retrieved or retrieval failed
		 */
		int8_t retrieve( class avlBaseNode<T, comp> *&p, const T &search, class avlBaseNode<T, comp> *&result )
		{
			if( !p )
			{
				result = nullptr;
				return 0;
			}
			else if( comp().greater(data, search) )
			{
				if(left)
                    leftHeight = left->retrieve( left, search, result );
                else
                    result = nullptr;
				return checkRightBalance( p );
			}
			else if( comp().less(data, search) )
			{
			    if(right)
                    rightHeight = right->retrieve( right, search, result );
                else
                    result = nullptr;
				return checkLeftBalance( p );
			}
			else
			{
				result = this;
				return remove( p );
			}
		}

		/*! Removes a node from the tree
		 *  \param root Reference value holding the current node's pointer
		 *  \return Interger value representing the final height of the subtree located at the node's removal point
		 */
		int8_t remove( class avlBaseNode<T, comp> *&root )
		{
			if( left )
			{
				leftHeight = left->getRightMost( left, root );
				root->left = left;
				root->right = right;
				return root->updateHeight();
			}
			else if( right )
			{
				rightHeight = right->getLeftMost( right, root );
				root->left = left;
				root->right = right;
				return root->updateHeight();
			}
			else
			{
				root = nullptr;
				return 0;
			}
		}

		/*! Retrieves the node furthest to the left of a given subtree
		 *  \param root Reference to the node pointer of the subtree that's being checked
		 *  \param replace Reference to the location which the right most node should be placed in
		 *  \return Final height of the subtree
		 */
		int8_t getLeftMost( avlBaseNode<T, comp> *&root, avlBaseNode<T, comp> *&replace )
		{
			if( left )
			{
				leftHeight = left->getLeftMost( left, replace );
				return checkRightBalance( root );
			}
			else
			{
				replace = this;
				return remove(root);
			}
		}

		/*! Retrieves the node furthest to the right of a given subtree
		 *  \param root Reference to the node pointer of the subtree that's being checked
		 *  \param replace Reference to the location which the right most node should be placed in
		 *  \return Final height of the subtree
		 */
		int8_t getRightMost( avlBaseNode<T, comp> *&root, avlBaseNode<T, comp> *&replace )
		{
			if( right )
			{
				rightHeight = right->getRightMost( right, replace );
				return checkLeftBalance( root );
			}
			else
			{
				replace = this;
				root = left;
				if( root )
					return root->height;
				else
					return 0;
			}
		}

        /*! Attempts to find a minimum value from the ones contained within the tree and an external value
         *   and returns that value, retrieving it from the tree if necessary
         *  \param root Reference to the node pointer of the subtree that's being checked
         *  \param testValue External value to compare in tree results against
         *  \param result Reference to object where the operation result will be returned
         *  \return Height of the current subtree
         */
        int8_t testAndRetrieveMinimum( avlBaseNode<T, comp> *&root, T &testValue, T &result, int &count)
        {
            if( left )
            {
                leftHeight = left->testAndRetrieveMinimum(left, testValue, result, count);
                return checkRightBalance( root );
            }
            else if( comp().greater(testValue, data) )
            {
                result = data;
                count--;
                return remove(root);
            }
            else
            {
                result = testValue;
                return height;
            }
        }

        const avlBaseNode<T, comp> *getLeft(void) const
		{
		    return left;
		}

		const avlBaseNode<T, comp> *getRight(void) const
		{
		    return right;
		}

		//! Data object contained in the node
		T data;
	private:

		/*! Checks the balance of the node after an operation as altered it's left subtree
		 *  \param root Reference to the node who's balance we're checking
		 *  \return Height of the subtree located at this node's position
		 */
		int8_t checkLeftBalance( class avlBaseNode<T, comp> *&root )
		{
			balance = leftHeight - rightHeight;
			if( balance == 2 )
			{
			    if( left->balance == -1 )
                        leftHeight = left->rotateLeft( left );
				return rotateRight( root );
			}
			if( rightHeight == height )
				height++;
			return height;
		}

		/*! Checks the balance of the node after an operation as altered it's right subtree
		 *  \param root Reference to the node who's balance we're checking
		 *  \return Height of the subtree located at this node's position
		 */
		int8_t checkRightBalance( class avlBaseNode<T, comp> *&root )
		{
			balance = leftHeight - rightHeight;
			if( balance == -2 )
			{
                if( right->balance == 1 )
                    rightHeight = right->rotateRight( right );
				return rotateLeft(root);
			}
			if( leftHeight == height )
				height++;
			return height;
		}

		/*! Preforms a right rotation operation on a subtree
		 *  \param self Node the rotation is being preformed on
		 *  \return New height of the subtree
		 */
		int8_t rotateRight( class avlBaseNode<T, comp> *&self )
		{
			self = left;
			left = self->right;
			self->right = this;

			self->rightHeight = updateLeftHeight();
			return self->updateHeightCache();
		}

		/*! Preforms a left rotation operation on a subtree
		 *  \param self Node the rotation is being preformed on
		 *  \return New height of the subtree
		 */
		int8_t rotateLeft( class avlBaseNode<T, comp> *&self )
		{
			self = right;

			right = self->left;
			self->left = this;

			self->leftHeight = updateRightHeight();
			return self->updateHeightCache();
		}


		int8_t updateLeftHeight(void)
		{
		    if(left)
                leftHeight = left->height;
            else
                leftHeight = 0;

            balance = leftHeight - rightHeight;
            height = max(leftHeight, rightHeight);
            return ++height;
		}

		int8_t updateRightHeight(void)
		{
		    if(right)
                rightHeight = right->height;
            else
                rightHeight = 0;

            balance = leftHeight - rightHeight;
            height = max(leftHeight, rightHeight);
            return ++height;
		}

		int8_t updateHeightCache(void)
		{
		    balance = leftHeight - rightHeight;
		    height = max(leftHeight, rightHeight);
		    return ++height;
		}

		/*! Recalculates the height of a node
		 *  \return Height of the node
		 */
		int8_t updateHeight()
		{
			height = 0;
			if( left )
			{
				leftHeight = left->height;
				if( leftHeight >= height )
					height = leftHeight;
			}
			else
				leftHeight = 0;
			if( right )
			{
				rightHeight = right->height;
				if( rightHeight >= height )
					height = rightHeight;
			}
			else
				rightHeight = 0;
			height++;
			balance = leftHeight - rightHeight;
			return height;
		}

	protected:
		//! Pointer to the left leaf
		avlBaseNode<T, comp> *left;
		//! Pointer to the right leaf
		avlBaseNode<T, comp> *right;
	private:
		//! Node balance value
		int8_t balance;
		//! Height of the node
		int8_t height;
		//! Height of left leaf
		int8_t leftHeight;
		//! Height of the right leaf
		int8_t rightHeight;
};

//! Basic avlNode class, for nodes which will not hold reference objects
template <typename T, typename comp = defaultComparable<T> > class avlNode : public avlBaseNode<T, comp>
{
	public:
		/*! Constructor for a new node
		 *  \param object Object to be held by the node
		 */
		avlNode( T object ) : avlBaseNode<T>( object )
		{
		}

		virtual ~avlNode() = default;

		/*! Finds a data object corresponding to a descriptor object
		 *  \param obj Object describing what the other object we're trying to find
		 *  \return Pointer to the data object if one was found, nullptr otherwise
		 */
		template <typename compType> const T *find( compType &obj ) const
		{
            comp comparator;
			if( comparator.greater(avlBaseNode<T, comp>::data, obj) )
			{
				if(avlBaseNode<T, comp>::left)
					return static_cast <avlNode <T, comp> *> ( avlBaseNode<T, comp>::left )->find( obj );
				else
					return nullptr;
			}
			else if( comparator.less(avlBaseNode<T, comp>::data, obj) )
			{
				if(avlBaseNode<T, comp>::right)
					return static_cast <avlNode <T, comp> *> ( avlBaseNode<T, comp>::right )->find( obj );
				else
					return nullptr;
			}
			else
				return &this->data;

		}

        template <typename compType> T *find( compType &obj )
		{
            comp comparator;
			if( comparator.greater(avlBaseNode<T, comp>::data, obj) )
			{
				if(avlBaseNode<T, comp>::left)
					return static_cast <avlNode <T, comp> *> ( avlBaseNode<T, comp>::left )->find( obj );
				else
					return nullptr;
			}
			else if( comparator.less(avlBaseNode<T, comp>::data, obj) )
			{
				if(avlBaseNode<T, comp>::right)
					return static_cast <avlNode <T, comp> *> ( avlBaseNode<T, comp>::right )->find( obj );
				else
					return nullptr;
			}
			else
				return &this->data;

		}


};


//! Object which basically holds the root node of an avlTree and simplifies syntax
template <typename T, typename comp, typename A> class avlBaseTree
{
	public:
		//! Constructor
		avlBaseTree() : root( nullptr ), count(0)
		{
		}

		virtual ~avlBaseTree()
		{
            if(root)
                root->destroyNode();
		}

		/*! Inserts an object into the tree
		 *  \param obj Object to be inserted
		 */
		T &insert( T obj )
		{
			avlBaseNode<T, comp> *node = new ( A::allocate( sizeof( avlBaseNode<T, comp> ) ) ) avlBaseNode<T, comp>( obj );
			insertNode(node);
			return node->data;
		}

        /*! Inserts an external node into the tree
         *  \param node Node to be inserted
         */
        void insertNode(avlBaseNode<T, comp> *node)
        {
            if(root)
                root->insert(root, node);
            else
                root = node;
            count++;
        }

        /*! Retrieves a node from the tree
         *  \param value Value to search for
         *  \return Pointer to the node if it exists, or nullptr if it does not
         */
        avlBaseNode<T, comp> *retrieveNode(T &value)
        {
            avlBaseNode<T, comp> *node;
            root->retrieve( root, value, node);
            return node;
        }

		T &findOrInsert( T &obj )
		{
			avlBaseNode<T, comp> *result;
			root->findOrInsert( root, obj, result, count );

			return result->data;
		}

        /*! Finds a minimum value in the tree, compares it to an external value and returns the lesser of the two
         *  \param testValue Value to test the tree's minimum value against
         *  \param result Reference value to resulting object
         */
        void testAndRetrieveMinimum(T &testValue, T &result)
        {
            if(root)
                root->testAndRetrieveMinimum(root, testValue, result, count);
            else
                result = testValue;
        }

		/*! Retrieves a node from the tree, copying the result into a result object
		 *  \param obj Reference to a comparisson object
		 *  \param result Reference to a result object
		 *  \return True if successful, false otherwise
		 */
		bool retrieve( T &obj, T &result )
		{
			avlBaseNode<T, comp> *node = nullptr;
			root->retrieve( root, obj, node );
			if( node )
			{
				node->copyData(result);
				node->~avlBaseNode<T, comp>();
				A::free( node );
				count--;
				return true;
			}
			else
				return false;
		}

		bool remove( T &obj)
		{
			avlBaseNode<T, comp> *node;
			root->retrieve(root, obj, node);
			if(node)
			{
				node->~avlBaseNode<T, comp>();
				A::free(node);
				count--;
				return true;
			}
			else
				return false;
		}

		int size(void) const
		{
		    return count;
		}

	    class avlIterator
	    {
            public:
                T operator *(void) const
                {
                    return current->data;
                }

                avlIterator &next(void)
                {
                    if(current)
                    {
                        const avlBaseNode<T, comp> *node = current->getLeft();
                        if(node)
                        {
                            do
                            {
                                const avlBaseNode<T, comp> *right = node->getRight();
                                if(right)
                                    traversal.push(node);
                                else
                                    current = node;
                                node = right;
                            } while(node);
                        }
                        else if(!traversal.isEmpty())
                        {
                            current = traversal.peek();
                            traversal.pop();
                        }
                        else
                            current = nullptr;
                    }
                    return *this;
                }

                avlIterator &operator++()
                {
                    return next();
                }

                avlIterator &operator++(int)
                {
                    return next();
                }

                bool isEnd(void) const
                {
                    return !current;
                }

                avlIterator(avlBaseNode<T, comp> *base)
                {
                    const avlBaseNode<T, comp> *node = base;
                    if(!node)
                        current = node;
                    else
                    {
                        while(node)
                        {
                            const avlBaseNode<T, comp> *right = node->getRight();
                            if(right)
                                traversal.push(node);
                            else
                                current = node;
                            node = right;
                        }
                    }
                }

            private:

                const avlBaseNode<T, comp> *current;
                Stack< const avlBaseNode<T, comp> * > traversal;
	    };


    public:
        class avlIterator front(void) const
        {
            return avlIterator(root);
        }

    protected:
		//! Root node pointer of the AVL tree
		avlBaseNode<T, comp> *root;
		int count;
    private:
		static A allocator;
};

//! Class for an AVL Tree which does not store reference variable types
template <typename T, typename comp = defaultComparable<T>, typename A = heapAllocator > class avlTree : public avlBaseTree<T, comp, A>
{
	public:
		//! Default constructor
		avlTree() : avlBaseTree<T, comp, A>()
		{
		}

		virtual ~avlTree() = default;

		/*! Finds a node in the tree and returns a pointer to it's data object
		 *  \param obj Reference to a comparison object
		 *  \return Returns a pointer to the data object if a result is found, nullptr otherwise
		 */
		template <typename objType> const T *find( objType &obj ) const
		{
			if(avlBaseTree<T, comp, A>::root)
				return static_cast < avlNode<T, comp> * > ( avlBaseTree<T, comp, A>::root )->find( obj );
			else
				return nullptr;
		}

		template <typename objType> T *find( objType &obj )
		{
			if(avlBaseTree<T, comp, A>::root)
				return static_cast < avlNode<T, comp> * > ( avlBaseTree<T, comp, A>::root )->find( obj );
			else
				return nullptr;
		}


		bool retrieveMinimum( T &dest )
		{
			avlBaseNode<T, comp> *minimum = nullptr;
			if( !avlBaseTree<T, comp, A>::root )
				return false;
            avlBaseTree<T, comp, A>::root->getRightMost( avlBaseTree<T, comp, A>::root, minimum );
			if( !minimum )
				return false;
			else
			{

                dest = minimum->data;
				minimum->~avlBaseNode<T, comp>();
				A::free( minimum );
				return true;
			}
		}

		bool retrieveMaximum( T &dest )
		{
			avlBaseNode<T, comp> *minimum = nullptr;
			if( !avlBaseTree<T, comp, A>::root )
				return false;
			avlBaseTree<T, comp, A>::root->getLeftMost( avlBaseTree<T, comp, A>::root, minimum );
			if( !minimum )
				return false;
			else
			{
				minimum->copyData( dest );
				minimum->~avlBaseNode<T, comp>();
				A::free( minimum );
				return true;
			}
		}
};

template <typename T, typename comp = defaultComparable<T> > class cachedAvlTree : public avlTree <T, comp, cacheAllocator< sizeof(avlBaseNode<T, comp>) > >
{
	public:
		cachedAvlTree() : avlTree<T, comp, cacheAllocator< sizeof(avlBaseNode<T, comp>)>  >() {}
};

#endif
