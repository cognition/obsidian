#ifndef UNIQUE_PTR_H_
#define UNIQUE_PTR_H_

template <typename T> class unique_ptr
{
    public:
        unique_ptr() : value(nullptr)
        {
        }

        unique_ptr(T *init) : value(init)
        {
        }

        unique_ptr(unique_ptr<T> &other) = delete;

        ~unique_ptr()
        {
            if constexpr (__is_array(T) )
                delete[] value;
            else
                delete value;
        }

        unique_ptr<T> &operator =(unique_ptr<T> &other)
        {
                return reset(other.release() );
        }

        unique_ptr<T> &operator =(const unique_ptr<T> &other) = delete;

        unique_ptr<T> &operator =(unique_ptr<T> &&other)
        {
            return reset( other.release() );
        }


		T &operator *() const
		{
			return *value;
		}

		T *operator ->()
		{
		    if(value)
                return value;
		    return nullptr;
		}

		T *operator ->() const
		{
		    if(value)
                return value;
		    return nullptr;
		}

		T *get(void) const
		{
			return value;
		}

		T *get(void)
		{
		    return value;
		}

        void reset(void)
        {
            if(value)
            {
                if constexpr(__is_array(T) )
                    delete[] value;
                else
                    delete value;
            }
        }

        T *release(void)
        {
            T *retValue = value;
            value = nullptr;
            return retValue;
        }

		unique_ptr<T> &reset(T *resetValue)
		{
            reset();
            value = resetValue;
            return *this;
		}

		T &operator[](uint64_t index)
		{
			return value[index];
		}

        T &operator[](uint64_t index) const
		{
			return value[index];
		}

    protected:

        T *value;
};


#endif // UNIQUE_PTR_H_
