#ifndef IS_SAME_TEMPLATE_H_
#define IS_SAME_TEMPLATE_H_

template <typename, typename > struct is_same
{
    constexpr is_same(void) {};

    constexpr operator bool () const
    {
        return false;
    }
};

template <typename T> struct is_same<T, T>
{
    constexpr is_same(void){};

    constexpr operator bool() const
    {
        return true;
    }
};

#endif // IS_SAME_TEMPALTE_H_
