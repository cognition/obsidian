#ifndef VARIANT_H_
#define VARIANT_H_

#include <compiler.h>
#include "is_same.cc"

template <typename... Types> class Variant
{
    /* Notes:
     *   - A model of parameter pack expansion and TCO underpins most of the utility functions in this class.
     *   - A typeIndex value of zero is reserved to represent and empty variant.
     *   - C++17 or higher is required for the 'if constexpr' expressions necessary for compile time TCO
     */

    //! Calculates the required maximum buffer size for the possible types the variant could contain
    template <typename T, typename... Args> static constexpr size_t union_size(void)
    {
        if constexpr(sizeof...(Args))
            return ((union_size<Args...>() > sizeof(T)) ? union_size<Args...>():sizeof(T));
        else
            return sizeof(T);
    }

    //! Buffer size
    static constexpr size_t ubufferSize = union_size<Types...>();

    static_assert(ubufferSize != 0);

    //! Gets the index of a given type for the variant
    template <typename T, typename header, typename... Args> static constexpr size_t get_index(const int index = 1)
    {
        if constexpr( is_same<T, header>() )
            return index;
        else if constexpr(sizeof...(Args))
            return get_index<T, Args...>(index + 1);
        else
            return index + 1;
    }

    //! Returns the maximum valid index value that could be encountered
    static constexpr size_t max_index(void)
    {
        return sizeof...(Types);
    }

    size_t typeIndex;
    uint8_t data[ubufferSize];

    //! Attempts to destroy the object of type T if the variant contains it
    template <typename T, typename... Args> bool destroyed(void)
    {
        if( typeIndex == get_index<T, Types...>() )
        {
            //Manual trigger the destructor of the matching type since the placement form of delete cannot be manually invoked
            static_assert(!__is_array(T), "Variant currently only support non-array types!");
            T *dataPtr = reinterpret_cast<T *>(data);
            dataPtr->T::~T();
            return true;
        }
        else
        {
            if constexpr (sizeof...(Args))
            {
                return destroyed<Args...>();
            }
            else
                return false;
        }

    }

    //! Attempts destroy the variant
    void destructExisting(void)
    {
        if(!destroyed<Types...>())
        {
            asm volatile ("cli\n"
                          "hlt\n":: "a" (0xBADFFFFU) );
        }
        typeIndex = 0;
    }

    /*! Tries to copy another variant containing an object of type T
     *  \param other Reference to the other variant
     */
    template <typename T, typename... Args> void tryCopy(const Variant<Types...> &other)
    {
        static_assert(get_index<T, Types...>() <= max_index(), "Type mismatch!");
        if(other.typeIndex == get_index<T, Types...>() )
        {
            typeIndex = other.typeIndex;
            const T *srcPtr = reinterpret_cast<const T *>(other.data);
            //Placement new with copy constructor since proper initialization isn't a given
            new (data) T(*srcPtr);
            return;
        }
        else
        {
            if constexpr(!sizeof...(Args))
            {
                typeIndex = 0;
                return;
            }
            else
                tryCopy<Args...>(other);
        }
    }

    /*! Copies the value of another variant
     *  \param other Other variant to copy from
     */
    void copy(const Variant<Types...> &other)
    {
        if(other.typeIndex)
            tryCopy<Types...>(other);
        else
            typeIndex = 0;
    }

    public:
        Variant() : typeIndex(0)
        {
        };

        Variant(Variant<Types...> &&other) = delete;

        Variant(const Variant<Types...> &other)
        {
            copy(other);
        }

        ~Variant()
        {
            if(typeIndex)
                destructExisting();
        }

        template <typename T> T *get_if(void)
        {
            static_assert(get_index<T, Types...>() <= max_index(), "Invalid request for variant type");

            if(typeIndex == get_index<T, Types...>() )
            {
                T* retPtr = reinterpret_cast <T *>(&data[0]);
                return retPtr;
            }
            else
                return nullptr;
        }

        template <typename T> void set(const T &value)
        {
            constexpr size_t index = get_index<T, Types...>();
            static_assert(index <= max_index(), "Invalid request for variant type");
            //Zero means uninitialized
            if(typeIndex)
                destructExisting();
            //Initialize data
            typeIndex = index;
            new (data) T(value);
        }

        template <typename T> void set(T &value)
        {
            constexpr size_t index = get_index<T, Types...>();
            static_assert(index <= max_index(), "Invalid request for variant type");
            //Zero means uninitialized
            if(typeIndex)
                destructExisting();
            //Initialize data
            typeIndex = index;
            new (data) T(value);
        }

        Variant<Types...> &operator =(const Variant<Types...> &other)
        {
            //Destroy existing object
            if(typeIndex)
                destructExisting();
            //Copy from the other object
            copy(other);
            return *this;
        }

        Variant<Types...> &operator =(Variant<Types...> &other)
        {
            //Destroy existing object
            if(typeIndex)
                destructExisting();
            //Copy from other object
            copy(other);
            return *this;
        }


        size_t getDataSize(void) const
        {
            return ubufferSize;
        }

        size_t getTypeIndex(void) const
        {
            return typeIndex;
        }
};

#endif // VARIANT_H_
