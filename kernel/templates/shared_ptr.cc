#ifndef SHARED_PTR_TEMPLATE_
#define SHARED_PTR_TEMPLATE_

#include <compiler.h>
#include <io/ioiface.h>

extern "C" void *bspStack, *bspStackEnd;
static const uintptr_t stackBase = reinterpret_cast <uintptr_t> (&bspStack);
static const uintptr_t stackEnd = reinterpret_cast <uintptr_t> (&bspStackEnd);

#define KERN_SPACE_START                0xFFFF800000000000UL


template <typename T> class shared_ptr
{
    private:
        void destroy(void) volatile
        {
            if(count)
            {
                uint64_t resCount = atomicDecRes(count);
                if(resCount == 0)
                {
                    delete value;
					delete count;
					count = nullptr;
                }
            }
        }

    public:
        shared_ptr() : value(nullptr), count(nullptr)
        {
        }

        explicit shared_ptr(T *init) : value(init), count(new uint64_t)
        {
            *count = 1;
        }

        shared_ptr(const shared_ptr<T> &other)
        {
            if(other.count)
                atomicInc(other.count);
            count = other.count;
            value = other.value;
        }

        shared_ptr(shared_ptr<T> &&other)
        {
            count = other.count;
            value = other.value;

            //checkValue();
            other.count = nullptr;
            other.value = nullptr;
        }

        ~shared_ptr()
        {
            destroy();
        }

        shared_ptr<T> &operator =(const shared_ptr<T> &other)
        {
            if(other.count)
                atomicIncRes(other.count);

            destroy();

            value = other.value;
            count = other.count;
            return *this;
        }

        shared_ptr<T> &operator =(shared_ptr<T> &&other)
        {
            if(other.value == value)
                return *this;

            destroy();

            value = other.value;
            count = other.count;

            other.value = nullptr;
            other.count = nullptr;

            return *this;
        }

		T &operator *() const
		{
			return *value;
		}

		T *operator ->()
		{
		    if(value)
            {
                if(count)
                    return value;
		    }
            kprintf("Error! shared_ptr invalid state! Value: %p Count: %p Addr: %p Caller: %p\n", value, count, this, COMPILER_FUNC_CALLER);
		    return value;
		}

		T *operator ->() const
		{
		    if(value)
            {
                if(count)
                    return value;
		    }
            kprintf("Error! shared_ptr invalid state! Value: %p Count: %p Addr: %p Caller: %p\n", value, count, this, COMPILER_FUNC_CALLER);
		    return value;
		}

		bool operator <(const shared_ptr<T> &other) const
		{
		    return (get() < other.get());
		}

		bool operator >(const shared_ptr<T> &other) const
		{
		    return (get() > other.get());
		}

        bool operator ==(const shared_ptr<T> &other) const
		{
		    return (value == other.value);
		}

		bool operator !=(shared_ptr<T> const &other) const
		{
		    return (value != other.value);
		}

		T *get(void) const
		{
			return value;
		}

		T *get(void)
		{
		    return value;
		}

        uint64_t getRefCount(void) const
        {
            if(count)
                return *count;
            else
                return 0;
        }

        void reset(void)
        {
            destroy();
            value = nullptr;
            count = nullptr;
        }

		shared_ptr<T> &reset(T *resetValue)
		{
            if(value == resetValue)
                return *this;

            destroy();

            count = new uint64_t;
            value = resetValue;

			*count = 1;
			return *this;
		}

		T &operator[](uint64_t index)
		{
			return value[index];
		}

        T &operator[](uint64_t index) const
		{
			return value[index];
		}

		void init(T *ptr)
		{
		    destroy();

		    count = new uint64_t;
		    *count = 1;

		    value = ptr;
		}

		uint64_t get_count(void) const
		{
		    if(!count)
                return 0;
            else
                return *count;
		}

    protected:
        shared_ptr(T *init, uint64_t *countPtr) : value(init), count(countPtr) {}

		template <typename derivedType, typename sourceType> friend shared_ptr<derivedType> static_pointer_cast( shared_ptr<sourceType> &source);

        T *value;
        volatile uint64_t *count;
};

/*! Downcasts a shared pointer object from one class to a base class while maintaining the reference counting state
 *  \param source Source shared pointer object
 *  \tparam derivedType Type of the shared pointer object derived
 *  \tparam sourceType Type
 *  \todo This should really use some compiler builtins to ensure derivedType can be downcast from sourceType
 */
template <typename derivedType, typename sourceType>  shared_ptr<derivedType> static_pointer_cast( shared_ptr<sourceType> &source)
{
    shared_ptr<derivedType> retPtr;
    if(source.count)
    {
        atomicIncRes(source.count);
        retPtr.count = source.count;
        retPtr.value = static_cast<derivedType *> (source.value);
    }
    return retPtr;
};




template <typename T> class static_shared_ptr : public shared_ptr<T>
{
    public:
        static_shared_ptr() = delete;

        static_shared_ptr(T *init) : shared_ptr<T>(init, &theCount), theCount(1)
        {
        }
    private:
        uint64_t theCount;
};

#endif

