/* Linked message passing events and listeners
 *
 * Author: Joseph Kinzel */


#ifndef NOTIFY_EVENT_H_
#define NOTIFY_EVENT_H_

#include <events.h>

template<typename msgType> class notifyEvent : public Event
{

    public:
        notifyEvent(msgType init) : eventMessage(new msgType), defaultMessage(init) {};

        void setStatus(msgType value)
        {
            *eventMessage = value;
            signalListeners();
        }

        void clear(void)
        {
            *eventMessage = defaultMessage;
        }

    protected:
        class notifyListener : public eventListener
        {
            public:
                notifyListener(Task *eventOwner, msgType init, const shared_ptr<msgType> &msg) : eventListener(eventOwner, EL_VALID), message(msg) {};
                virtual ~notifyListener() = default;

                virtual bool trigger(const uint32_t generation)
                {
                    if(matchGeneration(generation))
                        return validToObtained();
                    else
                        return false;
                }

                msgType getMessage(void) const
                {
                    return *message;
                }

                void clear(void)
                {
                    reset();
                }

            protected:
                shared_ptr<msgType> message;
        };


        shared_ptr<msgType> eventMessage;
        const msgType defaultMessage;

    public:
        shared_ptr<notifyListener> createListener(Task *eventOwner, msgType init)
        {
            return shared_ptr<notifyListener>(new notifyListener(eventOwner, init, eventMessage) );
        }

};



#endif // NOTIFY_EVENT_H_
