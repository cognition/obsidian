#ifndef VECTOR_TEMPLATE_
#define VECTOR_TEMPLATE_

#include <memory/ops.h>
#include "comparable.cc"

#define DEFAULT_INCREMENT_SIZE      8
#define DEFAULT_MAX_SIZE            8

//! Object providing array like operations for storing and retrieving data objects
template <typename dataType> class Vector
{
	public:
		//! Default constructor
		Vector() : size( 0 ), maxSize( DEFAULT_MAX_SIZE )
		{
			array = new dataType[maxSize];
		}

		//! Constructor with a provided size for pre-allocation
		Vector( const int initMaxSize ) : size( 0 ), maxSize( initMaxSize )
		{
			array = new dataType[initMaxSize];
		}

		Vector(const Vector<dataType> &other) : size(other.size), maxSize(other.maxSize)
		{
		    array = new dataType[maxSize];
		    for(int i = 0; i < size; i++)
                array[i] = other.array[i];
		}

		//! Destructor
		~Vector()
		{
		    delete[] array;
		}

		//! Destroys all objects within the array
		void destroyAll()
		{
			for( int i = 0; i < size; i++ )
				delete array[i];
		}

		/*! Inserts a data object into the array
		 *  \param t Object to be inserted
		 */
		void insert( const dataType &t )
		{
			if( size == maxSize )
			{
				maxSize += DEFAULT_INCREMENT_SIZE;
				dataType *nArray = new dataType[maxSize];
				for( int i = 0; i < size; i++ )
					nArray[i] = array[i];
				delete[] array;
				array = nArray;
			}
			array[size] = t;
			size++;
		}


		/*! Appends items from another vector
		 *  \param other Vector to append
		 */
		void insert( const Vector<dataType> &other)
		{
            int currentSize = size;
		    resize(size + other.size);
            //Copy the items over
            for(int i = 0; i < other.size; i++)
                array[currentSize + i] = other.array[i];

            //Update the size
            size += other.size;
		}

		/*! Inserts a elements from a raw array into the vector
		 *  \param buffer Raw buffer to copy elements from
		 *  \param bLength Number of items contained within the buffer
		 */
		void insert(const dataType *buffer, const int bLength)
		{
		    int currentSize = size;
		    resize(currentSize + bLength);
		    for(int i = 0; i < bLength; i++)
            {
                array[i+currentSize] = buffer[i];
            }
		}

		/*! Removes an object from the array at a given index
		 *  \param index Index of the object to be removed
		 *  \return True if the index was valid, false otherwise
		 */
		bool remove( int index )
		{
			if( index >= size )
				return false;
			size--;
			for( uint32_t i = index; i < size; i++ )
				array[i] = array[i+1];

			return true;
		}

		/*! Returns a reference to an object contained at a provided index value
		 *  \param index Index to retrieve the object from
		 *  \return Reference to the requested object, or an empty object
		 */
		dataType &operator[]( int index )
		{
			if( index < size )
				return array[index];
			else
            {
                kprintf("Out of range index %u vs current size %u max size %u Vector: %p Caller: %p\n", index, size, maxSize, this, COMPILER_FUNC_CALLER);
				return nullEntry;
            }
		}

		/*! Returns a mutable reference to the item at a specific index
		 *  \param index Item to reference
		 *  \return Reference to the item of the null entry if the bounds are invalid
		 */
		dataType &at(int index)
		{
		    if( index < size)
                return array[index];
            else
            {
                kprintf("Out of range index %u vs current size %u max size %u Vector: %p Caller: %p\n", index, size, maxSize, this, COMPILER_FUNC_CALLER);
                return nullEntry;
            }
		}

		/*! Returns an immutable reference to the item at a specific index
		 *  \param index Item to reference
		 *  \return Reference to the requested item or the null entry if the bounds are invalid
		 */
		const dataType &at(int index) const
		{
		    if( index < size)
                return array[index];
            else
            {
                kprintf("Out of range index %u vs current size %u max size %u Vector: %p Caller: %p\n", index, size, maxSize, this, COMPILER_FUNC_CALLER);
                return nullEntry;
            }
		}


		/*! Returns an immutable reference to the item at a specific index
		 *  \param index Item to reference
		 *  \return Reference to the requested item or the null entry if the bounds are invalid
		 */
		const dataType &operator[](int index) const
		{
		    if(index<size)
                return array[index];
            else
            {
                kprintf("Out of range index %u vs current size %u max size %u Vector: %p Caller: %p\n", index, size, maxSize, this, COMPILER_FUNC_CALLER);
                return nullEntry;
            }
		}

		//! Copy operator
		Vector<dataType> &operator =(const Vector<dataType> &other)
		{
		    if(other.array == array)
                return *this;

		    delete[] array;
		    array = new dataType[other.maxSize];
		    maxSize = other.maxSize;
		    size = other.size;
		    for(int i = 0; i < size; i++)
                array[i] = other.array[i];

            return *this;
		}

		/*! Returns the current size of the vector
		 *  \return Length of the vector
		 */
		int length() const
		{
			return size;
		}

		void clear()
		{
		    for(int i = 0; i < size; i++)
                array[i].~dataType();
            size = 0;
		}

		template <typename comp = defaultComparable<dataType> > void sort(void )
		{
		    comp comparator;
		    for(int i = 0; i < size; i++)
            {
                for(int j = i+1; j < size; j++)
                {
                    if(comparator.greater(array[i], array[j]))
                    {
                        dataType swap = array[j];
                        array[j] = array[i];
                        array[i] = swap;
                    }
                }
            }
		}

		template <typename comp = defaultComparable<dataType> > int find(const dataType &requested)
		{
		    comp comparator;
		    if(!size)
                return -1;

            int pivot = size/2;
            int index = pivot;
            while(pivot)
            {
                pivot /= 2;
                if(comparator.greater(requested, array[index]))
                    index += pivot;
                else if(comparator.lesser(requested, array[index]))
                    index -= pivot;
                else
                    return index;
            }

            return -1;
		}

		int capacity(void) const
		{
		    return maxSize;
		}

		const void *getDataPtr(void) const
		{
		    return reinterpret_cast< const void *> (array);
		}

		dataType *getBuffer(void)
		{
		    return array;
		}

		/*! Resizes the array
		 *  \param newSize Target size for the vector
		 */
		void resize(const int newSize)
		{
            if(newSize > maxSize)
            {
                dataType *oldBuffer = array;
                maxSize *= 2;
                array = new dataType[maxSize];
                for(int i = 0; i < size; i++)
                    array[i] = oldBuffer[i];

                delete[] oldBuffer;
            }

            size = newSize;

		}
    protected:
        //! Initializer for the StaticVector class
        Vector(dataType *src, int initLength) : size(initLength), maxSize(initLength), array(src){};

	private:
		//! Current size of the array
		int size;
		//! Maximum allocated size of the array
		int maxSize;
		//! Array containing the object data for the vector object
		dataType *array;
		//! A null or empty entry type, returned on invalid index references
		static dataType nullEntry;
};

template<typename dataType> dataType Vector<dataType>::nullEntry = dataType();

template<typename dataType> class StaticVector : public Vector<dataType>
{
    public:

    StaticVector(dataType *srcArr, const int length ) : Vector<dataType>(srcArr, length)
    {
    }
};

#endif
