#ifndef RADIX_TREE_TEMPLATE_
#define RADIX_TREE_TEMPLATE_

#include <string.h>
#include <string_fragment.h>
#include <cpu/bits.h>
#include <io/io.h>

template <typename T> class radixNode;

template <typename T> class radixNodeBank
{
	public:
		//! Capacity of each node bank
		static const uint8_t nodesPerBank = sizeof(uint16_t)*8;

		/*! Constructor
		 * \param index Index at which to insert the initial node
		 * \param value Initial node
		 */
		radixNodeBank(const uint8_t index, radixNode<T>  *value) : presentMask(1U<<index), count(1)
		{
			for(uint8_t i = 0; i < nodesPerBank; i++)
				nodes[i] = NULL;
			nodes[index] = value;
		}


		/*! Extracts a node at a given index from the bank
		 *  \param index Index to retrieve the node from
		 *  \return Pointer to the node
		 */
		radixNode<T> *extract(const uint8_t index)
		{
			radixNode<T> *ret = nodes[index];
			nodes[index] = NULL;
			presentMask &= ~(1<<index);
			count--;
			return ret;
		}

		/*! Retrieves the node at a given index
		 *
		 *  \param index Index of the node
		 *
		 *  \return Node at the requested index
		 */
		radixNode<T> *get(const uint8_t index)
		{
            radixNode<T> *ret = nodes[index];

            return ret;
		}

		/*! Retrieves a reference to a given node slot
		 *  \param index Index of node slot in the node bank
		 *  \return Reference to the node slot
		 */
		radixNode<T> *&getSlot(const uint8_t index)
		{
			return nodes[index];
		}

		/*! Inserts a node into the node bank at a given index
		 *  \param index Index to insert the node into
		 *  \param value Node to be inserted into the bank
		 */
		void insertNode(const uint8_t index, radixNode<T> *value)
		{
			const uint16_t mask = 1U<<index;
			nodes[index] = value;
			presentMask |= mask;
			count++;
		}

		/*! Retrieves the number of nodes current in the bank
		 *  \return Number of nodes currently in the bank
		 */
		inline uint8_t getNodeCount(void) const
		{
			return count;
		}

		/*! Extracts the last node in the bank
		 *  \return Pointer to the last node
		 */
		radixNode<T> *extractLast(void)
		{
			const uint8_t index = Bits::getLsb16(presentMask);
			return nodes[index];
		}

		/*! Attempts to remove a node or sub-node at a given index
		 *  \param index Index of node for removal operation
		 *  \param value Partial string value of current removal target
		 *  \return True if the target node was removed, false otherwise
		 */
		T *attemptRemove(const uint8_t index, const stringFragment &value )
		{
			radixNode<T> *&node = nodes[index];
			//Attempt the remove operation and return false if it fails
			T *ret = node->remove(node, value);
			if(ret && !node)
			{
				//If the node value is null then we need to lower the node count and clear the present bit in the bit mask
				const uint16_t mask = 1U<<index;
				presentMask &= !mask;
				count--;
			}
			return ret;
		}

	private:
		//! Bit mask indicating node presence
		uint16_t presentMask;
		//! Number of nodes currently housed in the bank
		uint8_t count;
		//! Array of node pointers stored in the bank
		radixNode<T> *nodes[radixNodeBank<T>::nodesPerBank];
};

template <typename T> class radixNode
{
	public:
		//! Number of potential sub-node banks in a given node
		static const uint8_t numNodeBanks = sizeof(uint8_t)*8;

		/*! Constructor
		 *  \param frag String fragment corresponding to this node
		 *  \param init Initial data value for this node
		 */
		radixNode( const stringFragment &frag, T *init = nullptr) : fragment( frag ), data( init ), bankPresenceMask(0), subCount(0)
		{
			//Initialize the node bank pointers as empty
			for( uint8_t i = 0; i < numNodeBanks; i++ )
			{
				banks[i] = NULL;
			}
		}

		radixNode( const radixNode<T> &other ) : fragment(other.frag), data(other.data), bankPresenceMask(other.bankPresenceMask), subCount(other.subCount)
		{
			for( uint8_t i = 0; i < numNodeBanks; i++ )
			{
				banks[i] = other.banks[i];
			}
		}

		radixNode<T> &operator =(const radixNode<T> &other)
		{
		    fragment = other.frag;
		    data = other.data;
		    bankPresenceMask = other.bankPresenceMask;
		    subCount = other.subCount;

			for( uint8_t i = 0; i < numNodeBanks; i++ )
			{
				banks[i] = other.banks[i];
			}
		    return *this;
		}

		/*! Inserts a node with a given element value into the sub-node banks
		 *  \param element Element value
		 *  \param node Sub node to insert
		 */
		void insertAt(const uint8_t element, radixNode<T> *node)
		{
			const uint8_t bankIndex = element/radixNodeBank<T>::nodesPerBank;
			const uint8_t bankOffset = element%radixNodeBank<T>::nodesPerBank;
			const uint8_t pMask = 1U<<bankIndex;

			radixNodeBank<T> *&bank = banks[bankIndex];

			subCount++;

			if(bankPresenceMask & pMask)
				bank->insertNode(bankOffset, node);
			else
			{
				bankPresenceMask |= pMask;
				bank = new radixNodeBank<T>(bankOffset, node);
			}
		}

		T *find(const stringFragment &frag )
		{
			int diff = frag.compare( fragment );
			if( diff == -1 )
				return data;
			else if( fragment.length() == diff )
			{
			    char value = frag[diff];
				const uint8_t bankIndex = value / radixNodeBank<T>::nodesPerBank;
				const uint8_t bitOffset = value % radixNodeBank<T>::nodesPerBank;

				radixNodeBank<T> *&nodeBank = banks[bankIndex];

				if( !nodeBank )
					return NULL;
				else
				{
					radixNode<T> *sub = nodeBank->get(bitOffset);
					if( !sub )
                    {
                        return NULL;
                    }
					else
						return sub->find( stringFragment( frag, diff ) );
				}
			}
			else
				return NULL;
		}


		/*! Finds an instance of a given value, or inserts one into a node
		 *  \param p Self reference pointer to the current node
		 *  \param frag String fragment associated with the data value
		 *  \param value Data value to be inserted if no existing value is found
		 *  \return Pointer to the data value instance associated with the string fragment
		 */
		T *findOrInsert(radixNode<T> *&p, stringFragment &&frag, T *value)
		{
			int diff = frag.compare( fragment );

			if( diff == -1 )
			{
				if(!data)
					data = value;
				return data;
			}
			else if( fragment[diff] == 0 )
			{

				uint8_t code = frag[diff];
				uint8_t bankNum = code / radixNodeBank<T>::nodesPerBank;
				uint8_t bankIndex = code % radixNodeBank<T>::nodesPerBank;

				radixNodeBank<T> *&nodeBank = banks[bankNum];

				if( !nodeBank )
				{
					subCount++;
					nodeBank = new radixNodeBank<T> (bankIndex, new radixNode<T> ( stringFragment(frag, diff), value));
					return value;
				}
				else
				{
					radixNode<T> *&subNode = nodeBank->getSlot(bankIndex);
                    if( !subNode )
					{

						nodeBank->insertNode(bankIndex, new radixNode<T>( stringFragment(frag, diff), value) );
						return value;
					}
					else
						return subNode->findOrInsert( subNode, stringFragment( frag, diff ), value );
				}
			}
			else if( frag[diff] == 0)
			{
			    p = new radixNode<T>( frag, value );
				p->insertAt(fragment[diff], this);
				fragment.shift(diff);
				return value;
			}
			else
			{
				uint8_t existing = fragment[diff];
				uint8_t inserted = frag[diff];
				stringFragment newFrag( frag, diff);
				radixNode<T> *newNode = new radixNode<T> (newFrag, value );
				p = new radixNode<T>( stringFragment(fragment, 0, diff) );
				fragment.shift(diff);
				p->insertAt(inserted, newNode);
				p->insertAt(existing, this);
				return value;
			}
		}

		/*! Removes a node or data value corresponding to a specific string fragment value
		 *  \param p Self pointer reference for the current node
		 *  \param frag String fragment value corresponding to the data that is to be removed
		 *  \return The value that was removed, or NULL if there was no node to remove
		 */
    T *remove(radixNode<T> *&p, const stringFragment &frag)
    {
        int diff = frag.compare( fragment );
        if( diff == -1 )
        {
            T *ret = data;
            data = NULL;
            if(subCount < 1)
            {
                p = NULL;
                delete this;
            }
            else if(subCount == 1)
            {
                const uint8_t bankNum = Bits::getLsb16(bankPresenceMask);
                p = banks[bankNum]->extractLast();
                delete banks[bankNum];
                delete this;
            }
            return ret;
        }
        else if( fragment[diff] == 0 )
        {
            uint8_t code = frag[diff];
            uint8_t bankNum = code / radixNodeBank<T>::nodesPerBank;
            uint8_t bankIndex = code % radixNodeBank<T>::nodesPerBank;

            radixNodeBank<T> *&nodeBank = banks[bankNum];

            if( !nodeBank )
                return NULL;
            else
            {
                T *ret = nodeBank->attemptRemove(bankIndex, stringFragment(frag, diff) );
                if(ret)
                {
                    subCount--;
                    if(!subCount)
                    {
                        bankPresenceMask &= ~(1<<bankNum);
                        delete nodeBank;
                        nodeBank = NULL;
                        if(!data)
                        {
                            p = NULL;
                            delete this;
                        }
                    }
                    else if( (subCount == 1) && !data )
                    {
                        bankNum = Bits::getLsb16(bankPresenceMask);
                        nodeBank = banks[bankNum];
                        p = nodeBank->extractLast();
                        p->fragment.shift( fragment.length() );


                        delete nodeBank;
                        delete this;
                    }
                    else if(nodeBank->getNodeCount() == 0)
                    {
                        delete nodeBank;
                        nodeBank = NULL;
                        bankPresenceMask &= ~(1<<bankNum);
                    }
                }
				return ret;
            }
        }
        else
            return NULL;
    }

		T *findBestMatch(const stringFragment &frag, const char seperator, int &matchIndex) const
		{
			int diff = fragment.compare(frag);
			if(diff == -1)
			{
				matchIndex += frag.length();
				return data;
			}
			else if( (fragment[diff] == 0) && (frag[diff] == seperator) )
			{
				uint8_t code = frag[diff];
				uint8_t bankNum = code / radixNodeBank<T>::nodesPerBank;
				uint8_t bankIndex = code % radixNodeBank<T>::nodesPerBank;
				matchIndex += diff;
				int match = matchIndex;
				radixNodeBank<T> * const &bank = banks[bankNum];
				if( bank )
				{
					const radixNode<T> *node = bank->getSlot(bankIndex);
					if(node)
					{
						T* value = node->findBestMatch( stringFragment(frag, diff), seperator, matchIndex);
						if(value)
							return value;
						else
							matchIndex = match;
					}
				}
				return data;
			}
			else
				return NULL;
		}

	private:
		//! Sub-node banks
		radixNodeBank<T> *banks[numNodeBanks];
		//! String fragment value associated with this node and its data
		stringFragment fragment;
		//! Data pointer for this node
		T *data;
		//! Mask indicating the validity of sub-node banks
		uint8_t bankPresenceMask;
		//! Number of direct sub-node children
		uint32_t subCount;
};

template <typename T> class radixTree
{
	public:
		radixTree() : root(nullptr)
		{
		}

		T *findOrInsert( T *obj)
		{
			if(!root)
			{
				root = new radixNode<T> ( stringFragment(obj->toString()), obj );
				return obj;
			}
			else
            {
				return root->findOrInsert( root, stringFragment(obj->toString()), obj);
            }
		}

		T *find( const T &obj ) const
		{
			if(!root)
				return NULL;

			stringFragment searchFrag( obj.toString() );
			return root->find( searchFrag );
		}

		T *find( String &name ) const
		{
			if(!root)
				return NULL;

			stringFragment searchFrag( name );
			return root->find( searchFrag );
		}

		T *find (const stringFragment &name)
		{
		    if(!root)
            {
                kprintf("Radix find failed - Tree empty.\n");
                return nullptr;
            }

            return root->find( name );
		}

		T *findBestMatch(const stringFragment &name, const char seperator, int &matchIndex) const
		{
			if(!root)
            {
				return NULL;
            }
			else
				return root->findBestMatch(name, seperator, matchIndex);
		}

		T *remove( T &obj )
		{
			if(!root)
				return NULL;

			stringFragment searchFrag( obj.toString() );
			return root->remove( root, searchFrag );
		}

		T *remove( const String &name )
		{
			if(!root)
				return NULL;
			else
			{
				return root->remove (root, stringFragment(name) );
			}
		}

	private:
		radixNode<T> *root;
};

#endif
