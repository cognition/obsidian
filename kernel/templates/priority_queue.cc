#ifndef PRIORITY_QUEUE_TEMPLATE_
#define PRIORITY_QUEUE_TEMPLATE_

#include <io/io.h>

/* Priority queue implementation built on an array based heap
 *
 * Author: Joseph Kinzel
 */

template <typename dataType, typename Comparator> class priorityQueue
{
    public:
        priorityQueue(const int initialCapacity = 16) : capacity(initialCapacity), heapSize(0)
        {
            array = new dataType[capacity];
        }


        virtual ~priorityQueue()
        {
            delete[] array;
        }

        int size(void) const
        {
            return heapSize;
        }


        bool empty(void) const
        {
            return !size();
        }

        dataType &peekTop(void) const
        {
            return array[0];
        }


        void conditionalRetrieveBest(dataType &dest, dataType &other)
        {
            if(!heapSize)
                dest = other;

            if( Comparator().compare(other, array[0]) )
            {

                dest = other;
            }
            else
            {
                retrieveBest(dest);
            }
        }

        bool retrieveBest(dataType &dest)
        {
            if(!heapSize)
                return false;

            retrieve(dest, 0);
            heapSize--;
            return true;
        }

        dataType &insert(dataType &d)
        {

            if(capacity == heapSize)
            {
                dataType *bigger = new dataType[capacity*2];

                for(int entry = 0; entry < capacity; entry++)
                    bigger[entry] = array[entry];

                capacity *= 2;
                delete[] array;
                array = bigger;
            }

            array[heapSize] = d;

            dataType &finalRef = percolate(heapSize);
            heapSize++;

            return finalRef;
        }


    private:
        void swap(const int first, const int second)
        {
            dataType saved = array[first];
            array[first] = array[second];
            array[second] = saved;
        }

        int parent(const int index) const
        {
            return ((index - 1)/2);
        }


        int leftChild(const int index) const
        {
            return (2*index + 1);
        }

        int rightChild(const int index) const
        {
            return(2*index + 2);
        }

        dataType &percolate(const int index)
        {
            if(index)
            {
                int pIndex = parent(index);
                if( Comparator().compare(array[index], array[pIndex]) )
                {
                    swap(pIndex, index);
                    return percolate(pIndex);
                }
            }

            return array[index];

        }

        void retrieve(dataType &dest, const int index)
        {
            int lChild, rChild, best;
            dest = array[index];

            lChild = leftChild(index);
            rChild = rightChild(index);

            if(lChild < heapSize)
            {
                if((rChild < heapSize) && Comparator().compare(array[rChild], array[lChild]))
                {
                    best = rChild;
                }
                else
                    best = lChild;

                retrieve(array[index], best);
            }
            else if(index < (heapSize-1))
            {
                array[index] = array[heapSize-1];
                percolate(index);

            }
        }


        int capacity, heapSize;
        dataType *array;
};


#endif // PRIORITY_QUEUE_TEMPLATE_
