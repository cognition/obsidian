#ifndef FIFO_QUEUE_TEMPLATE_
#define FIFO_QUEUE_TEMPLATE_

#include "shared_ptr.cc"
#include <compiler.h>
#include <cpu/lock.h>
#include <io/io.h>
#include <debug.h>


template <typename T> class queueObj
{
    public:
        queueObj(T &obj) : next(nullptr), data(obj)
        {
        }

        queueObj(const queueObj<T> &other) = delete;


        ~queueObj(){};
        queueObj<T> *next;
        T data;
};

template <typename T> class queueState
{
    public:
		queueState() : root(nullptr), tail(nullptr), count(0)
		{
		}

		queueState(queueState &other) = delete;
		queueState &operator =(const queueState &other) = delete;

		~queueState()
		{
		    queueObj<T> *ptr = root;
		    while(ptr)
            {
                queueObj<T> *next = ptr->next;
                delete ptr;
                ptr = next;
            }
            count = 0;
		}

            /*
		void add(T &obj)
		{
		    queueObj<T> *qObj = new queueObj<T>(obj);
		    spinLockInstance lock(&qLock);
            if(tail)
                tail->next = qObj;
            else
                root = qObj;
            tail = qObj;
            count++;
		}
*/
        void add(queueObj<T> *qObj)
		{
		    spinLockInstance lock(&qLock);
            if(tail)
                tail->next = qObj;
            else
                root = qObj;
            tail = qObj;
            atomicInc(&count);
		}



		COMPILER_NOINLINE T remove()
		{
            spinLockInstance lock(&qLock);

            if(!root || (reinterpret_cast<uintptr_t> (root) < KERNEL_VSPACE_START) )
            {
                kprintf("Error bad FIFOQ remove call!\n");
                asm volatile ("cli\n"
                              "hlt\n"::);
                return T();

            }
            queueObj<T> *q = root;
            T ret = q->data;
            root = q->next;
            if(!root)
                tail = nullptr;
            atomicDec(&count);
            delete q;

            return ret;
		}

		bool empty()
		{
            spinLockInstance lock(&qLock);

			return (!count);
		}

		T peek()
		{
            spinLockInstance lock(&qLock);
            return root->data;
		}

		size_t size()
		{
            spinLockInstance lock(&qLock);
            return count;
		}

    private:
        queueObj<T> *root, *tail;
        volatile size_t count;
        ticketLock qLock;
};


template <typename T> COMPILER_PACKED class fifoQueue
{

	public:
        fifoQueue() : data(new queueState<T>())
        {

        }

        fifoQueue(const fifoQueue &other) = delete;
        fifoQueue(fifoQueue &other) : data(other.data) {};

        fifoQueue &operator =(fifoQueue &other)
        {
            data = other.data;
            if( reinterpret_cast<uintptr_t> (data.get() ) < KERNEL_VSPACE_START  )
            {
                kprintf("FIFOQ - Corrupted copy operation! Got state value of %p from object %p\n", data.get(), &other);
                asm volatile ("cli\n"
                              "hlt\n"::);
            }
            return *this;
        }

        ~fifoQueue(){};

        /*
        void add(T &item)
        {
            data->add(item);
        }
        */
        void add(T &item)
        {
            queueObj<T> *qObj = new queueObj<T>(item);
            data->add(qObj);
        }

        T peek()
        {
            return data->peek();
        }

        COMPILER_NOINLINE T remove()
        {
            return data->remove();
        }

        COMPILER_NOINLINE bool empty(void) const
        {
            return data->empty();
        }

        size_t size(void) const
        {
            return data->size();
        }

        void discardAll(void)
        {
            queueState<T> *ns = new queueState<T> ();
            data.reset(ns);
        }

        queueState<T> *getStatePtr(void) const
        {
            return data.get();
        }
	private:
        shared_ptr<queueState<T>> data;
};

#endif
