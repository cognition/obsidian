#ifndef ALLOCATOR_TEMPLATE_H_
#define ALLOCATOR_TEMPLATE_H_

#include <compiler.h>
#include <cpu/bits.h>
#include <cpu/lock.h>
#include <memory/ops.h>
#include <memory/pnew.h>

#define CACHE_SET_BLOCK_SIZE            4096

class heapAllocator
{
	public:

		heapAllocator() {}
		static inline void *allocate( size_t size )
		{
			return reinterpret_cast <void *> ( new uint8_t[size] );
		}

		static inline void free( void *ptr )
		{
		    uint8_t *uptr = reinterpret_cast<uint8_t *> (ptr);
			delete[] uptr;
		}
};

template <const uint8_t objSize> class cacheObject
{
	public:
		cacheObject() {}

		void *getPtr()
		{
			return reinterpret_cast <void *> ( data );
		}

		uint8_t index;
		uint8_t data[objSize];
};

#define CACHE_ALLOCATOR_OBJ_COUNT       32

template <const uint8_t objSize> class cacheSet
{
	public:
		cacheSet() : next( nullptr ), prev( nullptr ), bitmap( UINT32_MAX )
		{
            for(int i = 0; i < 32; i++)
                objs[i].index = i;
		}

		void *allocate()
		{
			if( !bitmap )
				return nullptr;
			else
			{
				uint32_t mask = 1;
				uint8_t index = Bits::getLsb32( bitmap );
				mask <<= index;
				bitmap &= ~mask;
				return objs[index].getPtr();
			}
		}

		void free( cacheObject<objSize> *obj )
		{
			uint32_t mask = 1;
			mask <<= obj->index;
			bitmap |= mask;
		}

		bool inline hasFree()
		{
			return bitmap;
		}

		cacheSet<objSize> *next, *prev;
	private:
		uint32_t bitmap;
		cacheObject<objSize> objs[CACHE_ALLOCATOR_OBJ_COUNT];
};

template <const uint8_t T> class cacheAllocator
{
	public:
		cacheAllocator() {}

		static void *allocate( size_t COMPILER_UNUSED size )
		{
			void *ret;
			spinLockInstance cacheLock( &lock );
			//Check if we have any cache sets with free blocks, allocate one if we do not
			if( !active )
				active = new cacheSet<T>();
			//Allocate a block
			ret = active->allocate();
			//Check if the region we just allocated from is now full
			if( !active->hasFree() )
			{
				//Remove the block, and link the next value in
				cacheSet<T> *ptr = active;
				active = active->next;
				if( active )
					active->prev = nullptr;
				//Link our set into the full list
				ptr->next = full;
				if( full )
					full->prev = ptr;
				full = ptr;
			}
			return ret;
		}

		static void free( void *ptr )
		{
			cacheObject<T> *obj = reinterpret_cast <cacheObject<T> *> ( reinterpret_cast<uintptr_t> ( ptr ) - 1 );
			uintptr_t setAddress = reinterpret_cast <uintptr_t> ( obj );
            if( obj->index )
				setAddress -= obj->index*sizeof( cacheObject<T> ) + sizeof( uint32_t ) + sizeof( uintptr_t )*2;
			else
				setAddress -= sizeof( uint32_t ) + sizeof( uintptr_t )*2;

			cacheSet<T> *set = reinterpret_cast <cacheSet<T> *> ( setAddress );

			spinLockInstance cacheLock( &lock );
			if( !set->hasFree() )
			{
				if( set->next )
					set->next->prev = set->prev;
				if( set->prev )
					set->prev->next = set->next;
				else
					full = set->next;

				set->prev = nullptr;
				set->next = active;
				if( active )
					active->prev = set;
				active = set;
			}
			set->free( obj );
		}
	private:
		static cacheSet<T> *active, *full;
		static ticketLock lock;
};

template <const uint8_t T> cacheSet<T> *cacheAllocator<T>::active = nullptr;
template <const uint8_t T> cacheSet<T> *cacheAllocator<T>::full = nullptr;
template <const uint8_t T> ticketLock cacheAllocator<T>::lock;


#define CACHE_ALLOCATOR(x)          cacheAllocator< sizeof(x) >

#endif
