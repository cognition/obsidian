#ifndef STACK_H_
#define STACK_H_

template <typename T> class Stack
{
    public:
        Stack() : top(nullptr)
        {

        }

        ~Stack()
        {
            while( !isEmpty() )
            {
                pop();
            }

        }

        bool isEmpty(void) const
        {
            return !top;
        }

        T peek()
        {
            T ret = top->getData();
            return ret;
        }

        void pop()
        {
            singleLinkObj *node = top;
            if(node)
            {
                top = top->getNext();
                delete node;
            }
        }

        void push(const T &obj)
        {
            singleLinkObj *head = new singleLinkObj(obj);
            head->attachNext(top);
            top = head;
        }

    private:

    class singleLinkObj
    {
        public:
            singleLinkObj(T datum) : data(datum), next(nullptr)
            {
            }

            class singleLinkObj *getNext(void) const
            {
                return next;
            }

            void attachNext(singleLinkObj *attached)
            {
                next = attached;
            }

            T getData(void) const
            {
                return data;
            }
        private:
            T data;
            singleLinkObj *next;
    };

    singleLinkObj *top;
};


#endif // STACK_H_
