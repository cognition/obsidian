#ifndef RADIX_MAP_TEMPLATE_
#define RADIX_MAP_TEMPLATE_

#include <compiler.h>
#include <cpu/bits.h>
#include <memory/memory.h>

template <typename keyType, typename dataType, const int stepBits> class radixMapNode
{
    typedef radixMapNode<keyType, dataType, stepBits> nodeType;

    public:
        static constexpr int alphabetModulus = 1<<stepBits;
        static constexpr int keyBits = 8*sizeof(keyType);

        radixMapNode(const keyType initFrag, dataType initDatum, const int bitLength) : localFragment(initFrag), fragmentBits(bitLength), isLeaf(true), datum(initDatum) {};
        ~radixMapNode(){};


        //! Local fragment accessor
        keyType getKey(void) const
        {
            return localFragment;
        }

        //! Data item accessor
        const dataType *getDataPtr(void) const
        {
            return &datum;
        }


        /*! Locates a node
         *  \param key Key to search for
         *  \param prefix Prefix inherited from nodes above this
         *  \param remBits Bits in the key that have not yet been matched
         *  \return A pointer to the data item matching the key or null if no match could be found
         */
        const dataType *find(const keyType key, const keyType prefix = 0, const int remBits = keyBits) const
        {
            keyType currentFragment = localFragment | prefix;

            if(isLeaf)
            {
                //End of the road, it's either at this node or isn't present
                if(currentFragment != key)
                    return nullptr;
                else
                    return getDataPtr();
            }
            else
            {
                //Calculate an index and descend
                int nRemBits = remBits - fragmentBits;
                int nIndex = (key>>(nRemBits - stepBits))%alphabetModulus;
                if(nodeType *next = bank[nIndex])
                {
                    return next->find(key, currentFragment, nRemBits);
                }
                else
                    return nullptr;
            }
        }

        /*! Inserts a key into an existing map
         *  \param entry Reference to prior link field of the current node
         *  \param key Key value of the entry being inserted
         *  \param data Data value of the entry being inserted
         *  \param prefix Current combined prefix of any prior nodes traversed
         *  \param remBits Bits which have yet to be resolved in the key
         *  \return True if the key was successfully inserted, false otherwise
         */
        bool insert(nodeType *&entry, const keyType key, dataType data, const keyType prefix, const int remBits)
        {
            int msb;
            keyType diff = (prefix | localFragment) ^ key;

            if(!diff)
                return false;

            msb = Bits::getMsb64(diff);
            if(msb <= (remBits - fragmentBits))
            {
                if(!isLeaf)
                {
                    //We've encountered an existing non-data node we can either traverse or insert a new node into
                    uint64_t sKeyValue = key>>(remBits - (fragmentBits + stepBits) );
                    int bankIndex = sKeyValue%alphabetModulus;
                    if(!bank[bankIndex])
                    {
                        bank[bankIndex] = new nodeType(diff, data, remBits - fragmentBits);
                        return true;
                    }
                    else
                        return bank[bankIndex]->insert(bank[bankIndex], key, data, prefix | localFragment, remBits - fragmentBits);
                }
            }

            int pDiff = (msb - (msb%stepBits)) + stepBits;
            split(entry, key, data, prefix, remBits, pDiff);
            return true;
        }

    private:
        /*! Internal non-leaf node constructor
         *  \param initFrag Fragment value for the node
         *  \param bitLength Length of the fragment in bits
         */
        radixMapNode(const keyType initFrag, const int bitLength) : localFragment(initFrag), fragmentBits(bitLength),
            isLeaf(false) {
                Memory::set(bank, 0, sizeof(nodeType *)*alphabetModulus );
            };

        /*! Splits an existing node
         *  \param entry Entry reference pointer of an existing node
         *  \param key Key value of a node that's being inserted
         *  \param data Data value of the node that's being inserted
         *  \param prefix Existing prefix value
         *  \param remBits Bits that have yet to be resolved
         *  \param pDiff Portion of the key that includes the non-matching digit and all digits less significant than it
         */
        void split(nodeType *&entry, const keyType key, dataType data, const keyType prefix, const int remBits, const int pDiff )
        {
            keyType mask = 0;
            mask = ~mask;
            mask <<= pDiff;
            keyType splitFrag = localFragment & mask;

            nodeType *splitNode = new nodeType(splitFrag, remBits - pDiff);
            nodeType *nNode = new nodeType(key ^ (prefix | splitFrag), data, pDiff );


            uint64_t nShift = key>>(pDiff - stepBits);
            uint64_t lShift = localFragment>>(pDiff - stepBits);
            int nIndex = nShift%alphabetModulus;
            int splitIndex = lShift%alphabetModulus;

            splitNode->bindBankEntry(splitIndex, this);
            splitNode->bindBankEntry(nIndex, nNode );

            localFragment ^= splitFrag;
            fragmentBits = pDiff;

            entry = splitNode;
        }

        void bindBankEntry(const int index, nodeType *node)
        {
            bank[index] = node;
        }

        keyType localFragment;
        int fragmentBits;

        //Leaves don't have children and interior nodes don't have data, tagged union makes sense
        bool isLeaf;
        union
        {
            radixMapNode<keyType, dataType, stepBits> *bank[alphabetModulus];
            dataType datum;
        };
};

//! A radix map containing an integer keyType and data value
template <typename keyType, typename dataType, const int stepBits> class radixMap
{
    public:
        radixMap() : root(nullptr), entryCount(0) {};

        /*! Inserts a new value into the tree
         *  \param key Key of the value to be inserted
         *  \param data Data item to insert
         *  \return True if the insertion was successful and false if the item was already in the map
         */
        bool insert(keyType key, dataType data)
        {
            if(!root)
            {
                entryCount++;
                root = new radixMapNode<keyType, dataType, stepBits>(key, data, radixMapNode<keyType, dataType, stepBits>::keyBits);
                return true;
            }
            else if (root->insert(root, key, data, 0, 64))
            {
                entryCount++;
                return true;
            }
            else
                return false;
        }

        /*! Attempts to find the entry corresponding to a key
         *  \param key Key value
         *  \return A pointer to the data item matching the key or a nullptr if no matching item was found
         */
        const dataType *find(keyType key) const
        {
            if(!root)
                return nullptr;
            else
                return root->find(key);
        }

        int size(void) const
        {
            return entryCount;
        }
    private:
        radixMapNode<keyType, dataType, stepBits> *root;
        int entryCount;
};

#endif // RADIX_MAP_TEMPLATE_
