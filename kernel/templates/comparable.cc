#ifndef COMPARABLE_TEMPLATE_H_
#define COMPARABLE_TEMPLATE_H_

#include <templates/shared_ptr.cc>

/*! Basic comparable class
 * \tparam T Primary comparison type
 * \brief Overloads comparisons to the > and < operators
 */
template<typename T> class defaultComparable
{
    public:
        defaultComparable(void) = default;

        /*!  Greater than function
         * \param first First object
         * \param second Second object
         * \tparam U Secondary comparison object type
         */
        template <typename U = T> bool greater(const T &first, const U &second) const
        {
            return (first > second);
        }

        /*!  Less than function
         * \param first First object
         * \param second Second object
         * \tparam U Secondary comparison object type
         */
        template <typename U = T> bool less(const T &first, const U &second) const
        {
            return (first < second);
        }
};

/*! Pointer deferencing comparison class
 * \tparam T Primary pointer type
 * \brief Deferences primary object and secondary then uses the > and < operators
 */
template <typename T> class pointerContentsComparable
{
    public:
        pointerContentsComparable() = default;

        /*! Greater than function
         * \tparam U Secondary type
         * \param first First object of type T
         * \param second Other object of type U
         */
        template <typename U> bool greater(const T *first, const U second) const
        {
            return (*first > second);
        }

        template<> bool greater<T *>(const T *first, T *second) const
        {
            return (*first > *second);
        }

        /*! Less than function
         * \tparam U Secondary type
         * \param first First object of type T
         * \param second Other object of type U
         */
        template <typename U> bool less(const T *first, const U second) const
        {
            return (*first < second);
        }

        template<> bool less<T *>(const T *first, T *second) const
        {
            return (*first < *second);
        }
};


template <typename T> class sharedContentsComparable
{
    public:
        sharedContentsComparable() = default;
        bool greater(const shared_ptr<T> &first, const shared_ptr<T> &second)
        {
            return (*first > *second);
        }

        bool less(const shared_ptr<T> &first, const shared_ptr<T> &second)
        {
            return (*first < *second);
        }

        template <typename U> bool greater(const shared_ptr<T> &first, const U second)
        {
            return (*first > second);
        }

        template <typename U> bool less(const shared_ptr<T> &first, const U second)
        {
            return (*first < second);
        }
};


#endif // COMPARABLE_TEMPLATE_H_
