#ifndef MAP_TEMPLATE_H_
#define MAP_TEMPLATE_H_

#include "avltree.cc"

template <typename keyType, typename dataType> class taggedType
{
    public:
        taggedType(){};

        taggedType(keyType kv) : key(kv) {};

        taggedType(keyType kv, dataType dv) : data(dv), key(kv) {};

        taggedType(taggedType &other) : taggedType(other.key, other.data)
        {
        }

        taggedType(taggedType &&other) = delete;


        bool operator ==(const taggedType &other) const
        {
            return (other.key == key);
        }

        bool operator >(const taggedType &other) const
        {
            return (key > other.key);
        }

        bool operator <(const taggedType &other) const
        {
            return (key < other.key);
        }

        taggedType &operator =(const taggedType &other)
        {
            key = other.key;
            data = other.data;
            return *this;
        }

        taggedType &operator =(taggedType &other)
        {
            key = other.key;
            data = other.data;
            return *this;
        }

        const dataType &getData(void) const
        {
            //kprintf("Data location: %p\n", &data);
            return data;
        }

        dataType &getData(void)
        {
            //kprintf("Data location: %p\n", &data);
            return data;
        }

        keyType getKey(void) const
        {
            return key;
        }

    private:
        dataType data;
        keyType key;
    };


template <typename keyType, typename dataType, typename compareType = defaultComparable< taggedType<keyType, dataType> >, typename nodeAllocator = heapAllocator >
    class Map : protected avlTree< taggedType<keyType, dataType> , compareType, nodeAllocator> {
        private:
            typedef avlTree< taggedType<keyType, dataType>, compareType, nodeAllocator> baseType;


        public:
            Map() : avlTree<taggedType<keyType, dataType>, compareType, nodeAllocator>() {};

            dataType &insert(keyType key, dataType &data)
            {
                taggedType<keyType, dataType> taggedObj(key, data);
                return baseType::insert(taggedObj).getData();
            }

            bool findOrInsert(const keyType key, const dataType &data)
            {
                taggedType<keyType, dataType> taggedObj(key, data);
                taggedType<keyType, dataType> &res = baseType::findOrInsert(taggedObj);
                return (res.getData() == data);
            }

            dataType *get(keyType key)
            {
                taggedType<keyType, dataType> taggedObj(key);
                if( taggedType<keyType, dataType> *ptr =  baseType::find(taggedObj) )
                {
                    return &(ptr->getData());
                }
                else
                    return nullptr;
            }

            const dataType *get(keyType key) const
            {
                taggedType<keyType, dataType> taggedObj(key);
                if( const taggedType<keyType, dataType> *ptr =  baseType::find(taggedObj) )
                {
                    return &(ptr->getData());
                }
                else
                    return nullptr;
            }

            const dataType *match(keyType &query) const
            {
                taggedType<keyType, dataType> taggedObj(query);
                if(const taggedType<keyType, dataType> *ptr =  baseType::find(taggedObj))
                {
                    query = ptr->getKey();
                    return &(ptr->getData());
                }
                else
                    return nullptr;

            }

            bool remove(keyType key)
            {
                taggedType<keyType, dataType> taggedObj(key);
                return baseType::remove(taggedObj);
            }

            bool retrieve(keyType key, dataType &ret)
            {
                taggedType<keyType, dataType> taggedObj(key);
                taggedType<keyType, dataType> resultObj;
                bool result = baseType::retrieve(taggedObj, resultObj);
                if(result)
                    ret = resultObj.getData();
                return result;
            }

            typename baseType::avlIterator front()
            {
                return baseType::front();
            }

            int size()
            {
                return avlTree< taggedType<keyType, dataType> , compareType, nodeAllocator>::size();
            }
};

#endif // MAP_TEMPLATE_H_
