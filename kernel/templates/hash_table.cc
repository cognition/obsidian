#ifndef HASH_TABLE_TEMPLATE_
#define HASH_TABLE_TEMPLATE_

#include <templates/vector.cc>
#include <io/io.h>

template <typename objType, typename hashFunc> class chainedHashTable
{
    protected:
        class hashedObject
        {
            public:
                hashedObject() : hash(0) {};

                hashedObject(objType &obj, const uint64_t hashValue) : hash(hashValue), object(obj) {};
                hashedObject(const objType &obj, const uint64_t hashValue) : hash(hashValue), object(obj) {};

                hashedObject(const hashedObject &other) : hash(other.hash), object(other.object) {};

                hashedObject &operator =(const hashedObject &other)
                {
                    hash = other.hash;
                    object = other.object;
                    return *this;
                }

                hashedObject &operator =(hashedObject &other)
                {
                    hash = other.hash;
                    object = other.object;
                    return *this;
                }


                uint64_t getHash(void) const
                {
                    return hash;
                }


                bool operator ==(const class hashedObject &other) const
                {
                    if(hash == other.hash)
                        if(object == other.object)
                            return true;

                    return false;
                }

                bool match(const objType &otherObj, const uint64_t otherHash)
                {
                    if(hash == otherHash)
                        if(object == otherObj)
                            return true;
                    return false;
                }

                objType *getObj(void)
                {
                    return &object;
                }
            private:
                uint64_t hash;
                objType object;
        };

    private:
        typedef Vector< hashedObject > hashChain;

  public:
        chainedHashTable(const int initSize = 64) : table(new hashChain[initSize]), tSize(initSize), collisions(0), entryCount(0) {};

        ~chainedHashTable()
        {
            delete[] table;
        }

        void insert(objType object, uint64_t hash)
        {

            uint64_t mask = tSize - 1;

            uint64_t index = hash & mask;

            hashChain &chain = table[ index ];
            chain.insert(hashedObject(object, hash));
            entryCount++;
            if(chain.length() != 1)
            {
                collisions++;
                if(((collisions*100)/entryCount) > maxCollisions)
                    expandTable();
            }
        }

        void insert(objType object)
        {
            hashFunc hasher;
            uint64_t hash = hasher(object);
            insert(object, hash);
        }

        objType *findByHash(const uint64_t hashValue)
        {
            uint64_t mask = tSize - 1;
            uint64_t index = mask & hashValue;
            hashChain &chain = table[index];
            for(size_t chainEntry = 0; chainEntry < chain.length(); chainEntry++)
                if(chain[chainEntry].getHash() == hashValue)
                {
                    return chain[chainEntry].getObj();
                }

            return nullptr;
        }

        bool find(const objType &obj, objType *&results)
        {
            hashFunc hasher;
            uint64_t hash = hasher(obj);
            uint64_t mask = tSize - 1;
            uint64_t index = mask & hash;

            bool found = false;

            hashChain &chain = table[index];
            for(size_t chainEntry = 0; chainEntry < chain.length(); chainEntry++)
                if(chain[chainEntry].match(obj, hash))
                {
                    results = chain[chainEntry].getObj();
                    found = true;
                }

            return found;
        }

        objType *findOrInsert(objType &obj)
        {
            hashFunc hasher;
            uint64_t hash = hasher(obj);
            uint64_t mask = tSize - 1;
            uint64_t index = mask & hash;

            hashChain &chain = table[index];
            for(size_t link = 0; link < chain.length(); link++)
            {
                if(chain[link].match(obj, hash))
                    return chain[link].getObj();
            }

            size_t newIndex = chain.length();
            chain.insert( hashedObject(obj, hash) );


            entryCount++;
            return chain[newIndex].getObj();
        }

    protected:


      void expandTable(void)
      {
            hashChain *exTable = new hashChain [tSize*2];
            const uint64_t exMask = (tSize*2)-1;
            collisions = 0;
            for(size_t tableIndex = 0; tableIndex < tSize; tableIndex++)
            {
                hashChain &entry = table[tableIndex];
                for(size_t chainIndex = 0; chainIndex < entry.length(); chainIndex++ )
                {
                    hashedObject obj = entry.at(chainIndex);
                    uint64_t hash = obj.getHash();
                    uint64_t bin = hash & exMask;
                    hashChain &exChain = exTable[bin];
                    exChain.insert(obj);
                    if(exChain.length() != 1)
                        collisions++;
                }
            }
            delete[] table;
            table = exTable;
            tSize *= 2;
            if( (collisions*100)/entryCount > maxCollisions)
                expandTable();
      }

    static const int maxCollisions = 3;

    hashChain *table;
    size_t tSize, collisions, entryCount;
};

#endif // HASH_TABLE_TEMPLATE_
