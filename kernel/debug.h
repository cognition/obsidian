#ifndef DEBUG_H_
#define DEBUG_H_

#include <io/io.h>
#include <cpu/registers.h>

class serialLine : public textOutputDevice
{
	public:
		serialLine(uint16_t port);
		virtual ioDevResult writeBuffer( const void *buffer, uintptr_t length );
		virtual ioDevResult preformOp( streamOp o );
		virtual ioDevResult echoChar(const char value);
		virtual void getDims(int &rows, int &cols);
	private:
		void putChar(uint8_t character);
		uint16_t ioBase;
};

#define BP_TYPE_INS_EXEC                   0x00
#define BP_TYPE_DATA_WRITE                 0x01
#define BP_TYPE_RW                         0x03

#define BP_LENGTH_BYTE                     0x00
#define BP_LENGTH_WORD                     0x01
#define BP_LENGTH_QWORD                    0x02
#define BP_LENGTH_DWORD                    0x03

#define DEBUG_STATUS_BP0                   0x01U
#define DEBUG_STATUS_BP1                   0x02U
#define DEBUG_STATUS_BP2                   0x04U
#define DEBUG_STATUS_BP3                   0x08U

void setWatch(uintptr_t value, const uint32_t adjust);
void disableWatch(uintptr_t value, const uint32_t adjust);
void setBreakpoint(const uint8_t index, void *value, uint32_t type, uint32_t length);
void disableBreakpoint(const uint8_t index);
void examineBp(const uint8_t index, savedRegs *regs);
void printWatchSaves();
#endif

