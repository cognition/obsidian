#include <memory/ops.h>

extern "C"
{
    #include "dapi_include/driverapi.h"
    #include "dapi_include/dapi_container.h"
}

struct COMPILER_PACKED queue_entry
{
    queue_entry *next;
    uint8_t data_area[];

    void *get_data_ptr(void)
    {
        return &data_area[0];
    }
};

struct object_queue
{
    queue_entry *head, *tail;
    uint32_t datum_size;

    object_queue(const uint32_t object_size) : head(nullptr), tail(nullptr), datum_size(object_size) {};

    void *push(void)
    {
        queue_entry *nqe;

        nqe = (queue_entry *)calloc(sizeof(queue_entry) + datum_size, 1);

        if(tail)
            tail->next = nqe;
        else
            head = nqe;
        tail = nqe;

        return nqe->get_data_ptr();
    }

    int pop(void *out_buffer)
    {
        queue_entry *entry;

        if(!head)
            return 0;

        entry = head;
        head = head->next;
        if(!head)
            tail = nullptr;

        if(out_buffer)
            Memory::copy(out_buffer, entry->get_data_ptr(), datum_size);

        free(entry);


        return 1;
    }

    int empty(void) const
    {
        return (head ? 0:1);
    }

    void *peek(void)
    {
        return head->get_data_ptr();
    }

    void purge(void)
    {
        queue_entry *entry = head;
        while(entry)
        {
            queue_entry *next;
            next = entry->next;

            free(entry);

            entry = next;
        }

        head = nullptr;
        tail = nullptr;
    }
};

extern "C" void *dapi_queue_create(const uint32_t datum_size)
{
    object_queue *oq = new object_queue(datum_size);

    return oq;
}

extern "C" void *dapi_queue_push(void *queue)
{
    object_queue *oq = reinterpret_cast <object_queue *> (queue);
    return oq->push();
}

extern "C" int dapi_queue_empty(void *queue)
{
    object_queue *oq = reinterpret_cast <object_queue *> (queue);
    return oq->empty();
}

extern "C" void *dapi_queue_peek(void *queue)
{
    object_queue *oq = reinterpret_cast <object_queue *> (queue);
    return oq->peek();
}


extern "C" int dapi_queue_pop(void *queue, void *out_buffer)
{
    object_queue *oq = reinterpret_cast <object_queue *> (queue);
    return oq->pop(out_buffer);
}

extern "C" void dapi_queue_destroy(void *queue)
{
    object_queue *oq = reinterpret_cast <object_queue *> (queue);
    oq->purge();
    delete oq;
}
