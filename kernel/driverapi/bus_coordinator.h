/* Author: Joseph Kinzel */

#ifndef BUS_COORDINATOR_H_
#define BUS_COORDINATOR_H_

#include <cpu/lock.h>
#include "bus_provider.h"

/*! Bus Coordinator class
 *
 *  Bus coordinators are designed to have single unique instance for a given bus and control access the underlying bus providers(s) that provide access to it
 *  Bus coordinators are currently expected to utilize an external driver module which is lazily loaded when access to a given bus requested by a controller
 *  This class also provides bus controller drivers with a way to manage bus specific and unique handles for devices and controllers connected to the bus
 */
class busCoordinator
{
    public:
        enum class busStateType
        {
            busEmpty,       //Empty coordinator object
            busModule,      //Coordinator that with loadable module behind it
            busModFail,     //Coordinator that has failed to load its module
            busActive       //Coordinator that has successfully loaded, initialize and bound its module to the coordinator object
        };

        busCoordinator();
        busCoordinator(const String &busModuleFilename, const String &busDescription);

        int bindManagerFunctions(const busManagerDesc *desc);
        busMsgResponse processMessage(const uint64_t addr, void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType, DEVICE_HANDLE dev);
        int processNotify(busRegDescriptor *desc, DEVICE_HANDLE dev, BUS_HANDLE *rbHandle);

        void createBusHandle(DEVICE_HANDLE devHandle, BUS_HANDLE &bHandle);
        bool registerBusHandle(BUS_HANDLE bHandle, Device *dev);
        Device *decodeBusHandle(const BUS_HANDLE bHandle);
        bool validateCoordHandle(const DEVICE_HANDLE dh) const;

        busProvider *getProvider(const uint64_t address, const uint32_t operation, DEVICE_HANDLE dHandle);

        bool present(const uint64_t base, const uint64_t length) const;
        Device *&generateBusHandle(BUS_HANDLE &bHandle);
        BUS_HANDLE generateMultifunctionDevice(const char *name, const char *refSpaceId, const uint64_t address, BUS_HANDLE parent_handle, DEVICE_HANDLE coord_dev);
    private:
        busStateType loadBusModule(void);
        int64_t getSenderUniqueKey(DEVICE_HANDLE senderHandle);

        Map<BUS_HANDLE, Device *> bHandleMap;
        avlTree<busProvider> busRanges;
        prngInstance uidCreator;
        busMsgRecvFunc msgPtr;
        busRegisterNotifyFunc registerPtr;
        DriverDevice *busDev;
        const String moduleFilename;
        const String busName;
        busStateType busState;
        uint64_t busKey;
        ticketLock coordLock;
        uint32_t coordOnlyMask;
};


#endif // BUS_COORDINATOR_H_
