/* Simple driver database implementation
 *  - Allows for the loading of plain text driver databases in a per bus fashion
 *  - Allows for the querying of bus and file generated ID formats and subsequent loading of module files
 *
 * Author: Joseph Kinzel
 */


#include <templates/map.cc>
#include <cpu/localstate.h>
#include "dbase.h"
#include "module_loader.h"
#include <fs/vfs.h>

#define MAX_BUS_COUNT                32
#define DBASE_FILE_BUFFER_SIZE       512

typedef Map<uint64_t, driverDesc> busMapType;
static busMapType busMaps[MAX_BUS_COUNT];

driverDesc::driverDesc() : driverName(""), driverRef(""), driverPath("") {};

driverDesc::driverDesc(const String &name, const String &refId, const String &path) : driverName(name), driverRef(refId), driverPath(path) {};

//! Name accessor
const String &driverDesc::getDriverName(void) const
{
    return driverName;
}

//! Refspace ID accessor
const String &driverDesc::getDriverRefId(void) const
{
    return driverRef;
}

//! Path accessor
const String &driverDesc::getDriverPath(void) const
{
    return driverPath;
}


class bufferedFile
{
    public:
        bufferedFile(fileHandle *fh, const int bufferSize) : handle(fh), buffer(new char[bufferSize]),
            bufferRemaining(0), bufferCapacity(bufferSize), bufferOffset(0), fileOffset(0)
            {
                off_t currentPos;
                fh->seek(seekEnd, 0, &fileEnd);
                fh->seek(seekBegin, 0, &currentPos);
            };

        ~bufferedFile()
        {
            delete[] buffer;
        }

        void reset(void)
        {
            handle->seek(seekBegin, 0);
            bufferOffset = 0;
            bufferRemaining = 0;
        }

        /*! Indicates whether or not the stream is at the end of file
         *  \return True if the steam is current at the end of file, false if it is not
         */
        bool atEOF(void) const
        {
            return (fileOffset == fileEnd);
        }

        //! Indicates if the file absolute end of data in the file has been reached
        bool atEnd(void) const
        {
            return (atEOF() && !bufferRemaining);
        }

        String getLine()
        {
            String line;
            if(bufferRemaining)
            {
                line = parseLine();
                return line;
            }

            bufferOffset = 0;
            fsResult result = handle->read(buffer, bufferCapacity, bufferRemaining);
            //kprintf("Read %u bytes from file...\n", bufferRemaining);
            fileOffset += bufferRemaining;
            while((result == fsValid) && bufferRemaining)
            {
                bufferOffset = 0;
                line = parseLine();
                if( line.length() )
                    return line;
                if( atEOF() )
                    break;
                result = handle->read(buffer, bufferCapacity, bufferRemaining);
                fileOffset += bufferRemaining;
            }
            if(!bufferRemaining)
            {
                line = prefix;
                prefix = "";
            }
            return line;
        }

    private:
        String parseLine()
        {
            int lineEnd = String::strntok(&buffer[bufferOffset], bufferRemaining, '\n');

            if(lineEnd != -1)
            {
                String retString(&buffer[bufferOffset], lineEnd);
                bufferRemaining -= lineEnd+1;

                if(prefix.length() )
                {
                    retString = prefix + retString;
                    prefix = "";
                }

                bufferOffset += lineEnd+1;

                return retString;
            }
            else
            {
                prefix = prefix + String(&buffer[bufferOffset], bufferRemaining);
                bufferRemaining = 0;
                return String();
            }
        }

        fileHandle *handle;
        char *buffer;
        ssize_t bufferRemaining, bufferCapacity, bufferOffset;
        off_t fileOffset, fileEnd;
        String prefix;
};

//! Driver database file instance class
class driverDatabaseFile
{
    public:
        driverDatabaseFile(const String path) : fileName(path) {};

        /*! Parses the entire database file
         *
         *  \param map  In memory bus ID map to load the entries into
         */
        driverOperationResult parse(busMapType &map)
        {
            fileHandle *dbaseFile = Vfs::open(fileName, fileSession::fileRead);

            if(!dbaseFile)
                return driverOpNotFound;

            //First two string lay out field names and bit width
            int lineCount = 2;
            bufferedFile file(dbaseFile, DBASE_FILE_BUFFER_SIZE);
            String fieldsString = file.getLine();
            String sizesString = file.getLine();

            if( !fieldsString.length() || !sizesString.length() )
            {
                kprintf("[DDB ]: Failed to read database format strings - FL: %u WL: %u\n", fieldsString.length(), sizesString.length() );
                return driverOpBadState;
            }

            fieldNames = fieldsString.splitToken(',');
            //Must have at least one value, a device name and a module path
            if(fieldNames.length() < 3)
            {
                kprintf("[DDB ]: Failed to load driver database file: %s - Field name parse failed!\n", fileName.toBuffer() );
                return driverOpBadState;
            }

            Vector<String> widthStrings = sizesString.splitToken(',');
            //Device name and module path do not have a corresponding width parameter
            if( (fieldNames.length() - widthStrings.length()) != 3)
            {
                kprintf("[DDB ]: Failed to load driver database file: %s - Width-field mismatch! FL: %u WS: %u\n", fileName.toBuffer(), fieldNames.length(), widthStrings.length() );
                return driverOpBadState;
            }

            kprintf("[DDB ]: Field name count: %u Width count: %u\n", fieldNames.length(), widthStrings.length() );
            //Extract and validate the field width values
            int totalWidth = 0;
            int remWidth = 64;
            for(int widthIndex = 0; widthIndex < widthStrings.length(); widthIndex++)
            {
                uint64_t widthValue;
                if(!widthStrings[widthIndex].decToUnsigned(widthValue))
                {
                    kprintf("[DDB ]: Failed to load driver database file: %s - Invalid field width value entry #%u of %s\n", fileName.toBuffer(), widthIndex, widthStrings[widthIndex].toBuffer() );
                    return driverOpBadState;
                }
                totalWidth += widthValue;
                if(totalWidth > 64)
                {
                    kprintf("[DDB ]: Failed to load driver database file: %s - Exceeded total width value with entry #%u with value %u\n", fileName.toBuffer(), widthIndex, widthValue);
                    return driverOpBadState;
                }

                fieldWidths.insert(widthValue);
                remWidth -= widthValue;
                fieldShifts.insert(remWidth);
            }

            //Extract the actual entries...
            while( !file.atEnd() )
            {
                lineCount++;
                String line = file.getLine();
                if(line.length())
                {
                    if(line.at(0) != ';')
                    {
                        if(!parseEntry(map, line, lineCount))
                            return driverOpBadState;
                    }
                }
            }
            for(int i = 0; i < keyEntries.length(); i++)
            {
                const driverDesc *dd = map.get(keyEntries[i]);
                if(!dd)
                    kprintf("[DBSE]: Unable to validate entry for key %X\n", keyEntries[i]);
            }

            return driverOpSuccess;
        }

    private:
        /*! Parses a single driver entry line
         *  \param map Bus map to insert the resulting entry into
         *  \param entry String containing the entry
         *  \param lineNum Line number the entry occurs on
         *  \return true if parsing the entry succeeded, false otherwise
         */
        bool parseEntry(busMapType &map, const String &entry, const int lineNum)
        {
            uint64_t key = 0;
            Vector<String> fields = entry.splitToken(',');
            if(fields.length() != fieldNames.length())
            {
                kprintf("[DDB ]: Invalid field count in file %s:%u\n", fileName.toBuffer(), lineNum);
                return false;
            }

            //Build the complete mask field by field
            int descStart = fieldWidths.length();
            for(int fieldIndex = 0; fieldIndex < fieldWidths.length(); fieldIndex++)
            {
                uint64_t mask = 0;
                mask = ~mask;
                mask <<= fieldWidths[fieldIndex];
                mask = ~mask;

                uint64_t value;
                if(fields[fieldIndex].hexToUnsigned(value))
                {
                    if((value & mask) == value)
                    {
                        value &= mask;
                        value <<= fieldShifts[fieldIndex];

                        key |= value;
                    }
                }
                else
                {
                    kprintf("[DDB ]: Invalid field \'%s\' in file %s:%u\n", fieldNames[fieldIndex].toBuffer(), fileName.toBuffer(), lineNum);
                    return false;
                }

            }

            //Last two fields must be the driver name and path
            driverDesc desc(fields[descStart], fields[descStart+1], fields[descStart+2]);
            map.insert(key, desc);
            keyEntries.insert(key);
            return true;
        }

        String fileName;
        Vector<String> fieldNames;
        Vector<int> fieldWidths;
        Vector<int> fieldShifts;
        Vector<uint64_t> keyEntries;
};

static const driverDesc *findDriver(const int busId, const uint64_t key)
{
    if(busId >= MAX_BUS_COUNT)
        return nullptr;
    //kprintf("[DBSE]: Attempting to find driver for bus %u key: %X\n", busId, key);
    return busMaps[busId].get(key);
}

namespace driverDatabase
{

bool findModule(const driverDesc *&drv, const busAddressDesc *addr, const uint64_t key)
{
    drv = findDriver(addr->busId, key);

    return drv;
}

/*! Attempts to load and initialize a driver for a given bus-key combination
 *  \param addr Bus address of the device
 *  \param key Bus specific id key
 *  \param pDevice Parent device of the new device
 */
driverRequestStatus loadDbDriver(busCoordinator *manager, const busAddressDesc *addr, BUS_HANDLE bus_spec, const uint64_t key, Device *pDevice, void *context)
{
    //Locate the applicable bus map
    if(addr->busId >= MAX_BUS_COUNT)
        return driverRequestStatus::drModNotFound;
    if(!busMaps[addr->busId].size())
        return driverRequestStatus::drModNotFound;

    //Attempt to find a matching driver
    const driverDesc *drv = findDriver(addr->busId, key);
    if(!drv)
        return driverRequestStatus::drModNotFound;

    //Attempt to load the driver module
    DriverDevice *dev;
    notifyEvent<modStatus> notifyObj( modStatus::modNotFound );
    modStatus status = moduleLoader::queueModule( drv->getDriverPath(), pDevice, *addr, drv->getDriverName(), drv->getDriverRefId(), dev, &notifyObj);

    if(status == modStatus::modLoaded)
    {
        dev->setBusHandle(bus_spec);
        if( !manager->registerBusHandle(bus_spec, dev ) )
            return driverRequestStatus::drModRegisterFailed;
        //Attempt to initialize the module
        if(!dev->init(context))
            return driverRequestStatus::drModInitFailed;
        else
            return driverRequestStatus::drRunning;
    }
    else if(status == modStatus::modAllocated)
    {
        return driverRequestStatus::drQueued;
    }
    else
        return driverRequestStatus::drModLoadFailed;
}

/*! Loads driver file information from a database file
 *  \param busId Identifier of the bus map to load the driver information into
 *  \param path Path of the driver database file
 *  \return driverOpBadArg if the Bus ID is invalid or a database has already been loaded for that bus
 *  \return driverOpBadState if the Bus database file is corrupted or incorrectly formatted
 *  \return driverOpSuccess if the call succeeded
 */
driverOperationResult coordLoadDriverDatabase(const int busId, const String &path)
{
    driverDatabaseFile db(path);

    if(busId >= MAX_BUS_COUNT)
        return driverOpBadArg;
    if(busMaps[busId].size())
        return driverOpBadArg;

    driverOperationResult status = db.parse(busMaps[busId]);
    if(status != driverOpSuccess)
        return status;

    return driverOpSuccess;

}
}


