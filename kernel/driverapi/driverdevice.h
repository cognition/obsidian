/* Author: Joseph Kinzel */

#ifndef DRIVER_DEVICE_H_
#define DRIVER_DEVICE_H_

#include <device.h>
#include "driver.h"
#include <templates/map.cc>

extern "C"
{
    #include "dapi_include/driverapi.h"
}

//! A hardware device which also uses a loadable module as a driver
class DriverDevice : public Device
{
    public:
        static const constexpr uint32_t DRVDEV_HAS_MSI  = 0x00010000U;
        static const constexpr uint32_t DRVDEV_HAS_MSIX = 0x00020000U;

        DriverDevice( shared_ptr<Driver> drv, const String &fullDeviceName );
        DriverDevice(shared_ptr<Driver> drv, const String &fullDeviceName, const String &refSpaceId, Device *p, const busAddressDesc &devAddr);
        virtual bool init(void *context);

        //! Module interface related functions
        int allocatePhysRegion(uintptr_t &vBase, uintptr_t &physBase, size_t contiguousSize, uintptr_t align, int flags);
        int freePhysRegion(const uintptr_t vBase);

        int registerPhysRegion(uintptr_t &vBase, uintptr_t base, uintptr_t length);
        int deregisterPhysRegion(uintptr_t vBase);

        int registerInterruptHandler(interruptFunc func, void *context, int *interruptCount);
        int deregisterInterruptHandler();

        const busAddressDesc &getAddress(void) const;
        int getDeviceResources(resourceEntry **resources, int *resourceCount);

        DEVICE_HANDLE getHandle(void) const
        {
            return handle;
        }

        void setBusHandle(BUS_HANDLE bh);

        static DriverDevice *getDriverDevFromHandle(DEVICE_HANDLE dHandle);
    private:
        static DEVICE_HANDLE registerDevice(DriverDevice *dev);

        DEVICE_HANDLE handle;
        BUS_HANDLE busHandle;
        busAddressDesc addr;
        shared_ptr<Driver> devDriver;
        resourceEntry *driverRes;
        void *driverContext;
        uint32_t intFlags, devFlags;
        int resourceCount;
        Map<uintptr_t, regionObj64> physRegions;
};

#endif // DRIVER_DEVICE_H_
