#include <cpu/lock.h>

extern "C" {
#include "dapi_include/driverapi.h"
}

extern "C" void *dapi_lock_create()
{
    void *ret = reinterpret_cast<void *> (new spinLockObj());
    return ret;
}

extern "C" void dapi_lock_destroy(void *lock)
{
    spinLockObj *lockObj = reinterpret_cast <spinLockObj *>(lock);
    delete lockObj;
}

extern "C" void dapi_lock_acquire(void *lock)
{
    spinLockObj *lockObj = reinterpret_cast <spinLockObj *>(lock);
    lockObj->acquire();
}

extern "C" void dapi_lock_release(void *lock)
{
    spinLockObj *lockObj = reinterpret_cast <spinLockObj *>(lock);
    lockObj->release();
}
