/* Driver synchornization API
 *
 * Author: Joseph Kinzel
 */

#include <compiler.h>
#include <events.h>
#include <scheduler.h>
#include <cpu/localstate.h>
#include <templates/notify_event.cc>
#include <addrspace/kern_map.h>
#include <debug.h>

extern "C"
{
    #include "dapi_include/driverapi.h"
    #include "dapi_include/sync.h"
    #include "dapi_include/dapi_container.h"
}

#define SYNC_OBJ_TERMINATE               0x8000U

struct trackedLifetime
{
    union
    {
        void *creator;
        uintptr_t creator_addr;
    };

    union
    {
        void *destroyer;
        uintptr_t destroyer_addr;
    };

    bool operator ==(const trackedLifetime &other) const
    {
        return ((creator_addr == other.creator_addr) && (destroyer_addr == other.destroyer_addr));
    }
};

avlTree<sync_object *> activeSyncObjs;
Map<sync_object *, trackedLifetime> manifestedSyncObjs;

/*
static int syncBpValue = 0;

static int getSyncBp(void)
{
    int value = syncBpValue;
    syncBpValue++;
    syncBpValue &= 0x3U;

    return value;
}
*/

class driverSyncPackage : public notifyEvent<uint64_t>
{
    public:
        driverSyncPackage(const uint64_t initValue) : notifyEvent<uint64_t>(initValue),
            syncListener( createListener(localState::getSched()->getCurrentTask(), initValue) ){};

        void clear()
        {
            notifyEvent<uint64_t>::clear();
            syncListener->clear();
        }

        void wait(const uint32_t duration)
        {
            pushListener( static_pointer_cast<eventListener>( syncListener) );
            Scheduler::waitCurrent(this, duration);
        }

        void signal(const uint64_t value)
        {
            *eventMessage = value;
            signalListeners();
        }

        uint64_t getValue(void) const
        {
            return syncListener->getMessage();
        }


        bool hasObtained(void) const
        {
            return syncListener->hasObtained();
        }
    private:
        shared_ptr<notifyEvent<uint64_t>::notifyListener> syncListener;
};


static void sync_corrupted_msg(sync_object *sync, void *caller_ptr)
{
    uintptr_t rbase, roffset, caller_addr;
    caller_addr = reinterpret_cast <uintptr_t> (caller_ptr);
    String regionName = kernelSpace::decodeAddress(caller_addr, rbase, roffset);

    if( sync_object **ret = activeSyncObjs.find(sync) )
    {
        kprintf("Corrupted synch object %p Type: %X - Caller [%s]:%X Base: %p Processor: %u - System halted.\n", sync, sync->obj_type,
                regionName.toBuffer(), roffset, rbase, localState::getProcId() );
        printWatchSaves();
    }
    else if( const trackedLifetime *tl = manifestedSyncObjs.get(sync) )
    {
        uintptr_t creatorBase, creatorOffset, destroyerBase, destroyerOffset;
        String creatorString = kernelSpace::decodeAddress(tl->creator_addr, creatorBase, creatorOffset);
        String destroyerString = kernelSpace::decodeAddress(tl->destroyer_addr, destroyerBase, destroyerOffset);
        kprintf("Attempt to interact with previously released sync object %p - Caller [%s]:%X Base: %p Processor: %u\nCreator: [%s]+%X Destroyer: [%s]+%X\nCreator base: %X Destroyer base: %X System halted.\n", sync,
                regionName.toBuffer(), roffset, rbase, localState::getProcId(), creatorString.toBuffer(), creatorOffset, destroyerString.toBuffer(), destroyerOffset, creatorBase, destroyerBase );
    }
    else
    {
        kprintf("Attempt to interact with untracked sync object %p - Caller [%s]:%X Base: %p Processor: %u - System halted.\n", sync,
                regionName.toBuffer(), roffset, rbase, localState::getProcId() );

    }

    asm volatile ("cli\n"
                  "hlt\n"::);
}

/*! Creates an attachment to a synchronization object instance
 *
 *  \param sync Synchronization to create an attachment for
 *
 *  \return Zero if this the first attachment created or one otherwise
 */
extern "C" int sync_attach(sync_object *sync)
{
    if( !atomicInc(&sync->attachments) )
        return 0;
    else
        return 1;
}

static int sync_detach_internal(sync_object *sync, void *caller)
{
    if(!atomicDecRes(&sync->attachments))
    {
        if(sync->sync_flags & SYNC_OBJ_TERMINATE)
        {
            driverSyncPackage *dsp;

            switch(sync->obj_type)
            {
                case syncObjCallback:
                    break;
                case syncObjFuture:
                case syncObjWait:
                    dsp = reinterpret_cast<driverSyncPackage *>(sync->wait);
                    delete dsp;
                    break;
                default:
                    sync_corrupted_msg(sync, caller);
                    break;
            }

            if( !activeSyncObjs.remove(sync) )
            {
                kprintf("Error: Attempted to remove invalid sync_object: %p\n", sync);
                asm volatile ("cli\n"
                              "hlt\n"::);
            }
            else
            {
                if(trackedLifetime *entry = manifestedSyncObjs.get(sync))
                {
                    entry->destroyer = caller;
                }
                delete sync;
                return 1;
            }
        }
    }

    return 0;
}

extern "C" void sync_detach(sync_object *sync)
{
    sync_detach_internal(sync, COMPILER_FUNC_CALLER);
}

/*! Spawns a new task
 *  \param func Function entry point
 *  \param context Function entry argument
 *  \return Process id for the new task
 */
extern "C" pid_t dapi_spawn_task(taskFunc func, void *context)
{
    pid_t pid;
    Task::allocatePid(pid);
    Task *driverTask = new Task(func, context, 75, pid);
    //kprintf("[SYNC]: Spawned new task %p\n", driverTask);
    Scheduler::registerTask(driverTask);

    return pid;
}

/*! Creates a new future/promise style synchronization object
 *  \param object Return pointer for the new future object
 *  \param default_value Initialization value for the future that should be returned if the task has not completed by the specified maximum_wait time
 *  \param max_wait Maximum timeout when the promise's data is requested
 */
extern "C" int sync_create_future_obj(sync_object **object, const uint64_t default_value, const uint32_t max_wait)
{
    sync_object *so;
    driverSyncPackage *dsp = new driverSyncPackage(default_value);

    so = new sync_object;

    so->future = dsp;
    so->obj_type = syncObjFuture;
    so->max_wait = max_wait;

    //Always one default attachment, which is the basic lifetime attachment (create -> destroy)
    so->attachments = 1;
    so->sync_flags = 0;

    *object = so;

    activeSyncObjs.insert(so);
    manifestedSyncObjs.findOrInsert(so, trackedLifetime{ {COMPILER_FUNC_CALLER}, {nullptr}});
    return 1;
}

static void sync_init_signal_obj(sync_object *so, const uint64_t default_value)
{
    driverSyncPackage *dsp = new driverSyncPackage(default_value);

    so->wait = dsp;
    so->obj_type = syncObjWait;
    so->flags = 0;

    //Always one default attachment, which is the basic lifetime attachment (create -> destroy)
    so->attachments = 1;
    so->sync_flags = 0;

    activeSyncObjs.insert(so);
    manifestedSyncObjs.findOrInsert(so, trackedLifetime{ {COMPILER_FUNC_CALLER}, {nullptr}});
}

/*! Creates a new synchronization object
 *  \param object Return pointer to the created synchronization object
 */
extern "C" int sync_create_signal_obj(sync_object **object, const uint64_t default_value, const uint32_t flags)
{
    sync_object *so;
    so = new sync_object;

    sync_init_signal_obj(so, default_value);

    *object = so;

    return 1;
}

/*! Creates a new call based synchronization object
 *  \param object Return pointer for the newly created callback object
 *  \param context Context value to pass along to the callback when triggered
 *  \param cb Callback function to trigger when signaled
 */
extern "C" int sync_create_callback_obj(sync_object **object, void *context, callbackFunc cb)
{
    sync_object *cb_obj;

    cb_obj = new sync_object;

    cb_obj->obj_type = syncObjCallback;
    cb_obj->func = cb;
    cb_obj->context = context;

    //Always one default attachment, which is the basic lifetime attachment (create -> destroy)
    cb_obj->attachments = 1;
    cb_obj->sync_flags = 0;

    *object = cb_obj;
    //kprintf("[SYNC]: New callback object @ %p with context %p and target %p\n", cb_obj, context, cb);

    activeSyncObjs.insert(cb_obj);
    manifestedSyncObjs.findOrInsert(cb_obj, trackedLifetime{ {COMPILER_FUNC_CALLER}, {nullptr}});
    return 1;
}

/*! Causes the current task to wait on a synchronization object
 *  \param obj Synchronization object
 *  \param timeout Maximum wait time
 */
extern "C" int sync_object_wait(sync_object *obj, uint32_t timeout)
{
    driverSyncPackage *dsp;

    if(sync_attach(obj))
    {
        switch(obj->obj_type)
        {
            case syncObjFuture:
            case syncObjCallback:
                return 0;
                break;
            case syncObjWait:
                dsp = reinterpret_cast<driverSyncPackage *>(obj->wait);
                dsp->wait(timeout);
                return 1;
                break;
            default:
                sync_corrupted_msg(obj, COMPILER_FUNC_CALLER);
                return 0;
                break;
        }
    }
    else
    {
        sync_corrupted_msg(obj, COMPILER_FUNC_CALLER);
        return 0;
    }
}

/*! Signals a synchronization object
 *  \param obj Synchronization object to signal
 */
extern "C" int sync_object_signal(sync_object *obj, uint64_t status)
{
    driverSyncPackage *dsp;
    if( sync_attach(obj) )
    {
        switch(obj->obj_type)
        {
            case syncObjFuture:
                dsp = reinterpret_cast<driverSyncPackage *>(obj->wait);
                dsp->signal(status);
                return 1;
                break;
            case syncObjCallback:
                obj->func(obj->context, status);
                return 1;
                break;
            case syncObjWait:
                dsp = reinterpret_cast<driverSyncPackage *>(obj->wait);
                dsp->signal(status);
                return 1;
                break;
            default:
                sync_corrupted_msg(obj, COMPILER_FUNC_CALLER);
                break;
        }
    }
    else
        return 0;
}

/*! Gets the status associated with the sync object
 *  \param obj Synchronization object
 *  \return Status value of the object
 */
extern "C" uint64_t sync_object_get_status(sync_object *obj)
{
    driverSyncPackage *dsp;
    uint64_t ret = 0;

    if( sync_attach(obj) )
    {
        switch(obj->obj_type)
        {
            case syncObjFuture:
                dsp = reinterpret_cast<driverSyncPackage *>(obj->wait);
                if( !dsp->hasObtained() )
                    dsp->wait(obj->max_wait);
                ret = dsp->getValue();
                break;
            case syncObjCallback:
                ret = 0;
                break;
            case syncObjWait:
                dsp = reinterpret_cast<driverSyncPackage *>(obj->wait);
                ret = dsp->getValue();
                break;
            default:
                sync_corrupted_msg(obj, COMPILER_FUNC_CALLER);
                ret = 0;
                break;
        }

        sync_detach_internal(obj, COMPILER_FUNC_CALLER);
    }
    else
        sync_corrupted_msg(obj, COMPILER_FUNC_CALLER);

    return ret;
}

/*! Resets the status and value of synchronization object
 *  \param obj Synchronization object to be reset
 */
extern "C" void sync_object_reset(sync_object *obj)
{
    driverSyncPackage *dsp;
    //This shouldn't really be possible if a module is being responsible here unless there's some memory corruption problem
    if( sync_attach(obj) )
    {
        switch(obj->obj_type)
        {
            case syncObjCallback:
                break;
            case syncObjFuture:
            case syncObjWait:
                dsp = reinterpret_cast<driverSyncPackage *>(obj->wait);
                dsp->clear();
                break;
            default:
                sync_corrupted_msg(obj, COMPILER_FUNC_CALLER);
                break;
        }
        sync_detach_internal(obj, COMPILER_FUNC_CALLER);
    }
    else
        sync_corrupted_msg(obj, COMPILER_FUNC_CALLER);
}

/*! Destroys a synchronization object
 *  \param obj Object to destroy
 */
extern "C" int sync_destroy_signal_obj(sync_object *obj)
{
    atomicOr(&obj->sync_flags, SYNC_OBJ_TERMINATE);
    return sync_detach_internal(obj, COMPILER_FUNC_CALLER);
}


struct driver_monitor
{
    void *wait_queue;
    void *lock;
    uint32_t entry_size;

    driver_monitor() : wait_queue(dapi_queue_create(sizeof(sync_object *))), lock(dapi_lock_create())
    {
    }

    int acquire(const uint32_t timeout)
    {
        sync_object *sync, **qsync;
        dapi_lock_acquire(lock);
        int empty = dapi_queue_empty(wait_queue);


        sync_create_signal_obj(&sync, 0, 0);
        qsync = reinterpret_cast<sync_object **> ( dapi_queue_push(wait_queue) );
        *qsync = sync;

        dapi_lock_release(lock);

        if(empty)
            return empty;
        else
        {
            sync_object_wait(sync, timeout);
            return sync_object_get_status(sync);
        }
    }

    void release()
    {
        sync_object *sync, **next_sync;

        dapi_lock_acquire(lock);

        dapi_queue_pop(wait_queue, &sync);

        sync_destroy_signal_obj(sync);
        if(!dapi_queue_empty(wait_queue))
        {
            next_sync = reinterpret_cast <sync_object **> (dapi_queue_peek(wait_queue) );
            sync = *next_sync;
            sync_object_signal(sync, 1);
        }

        dapi_lock_release(lock);
    }

    void purge()
    {
        dapi_lock_acquire(lock);

        while(!dapi_queue_empty(wait_queue))
        {
            sync_object *sync;
            dapi_queue_pop(wait_queue, &sync);
            sync_object_signal(sync, 0);
            sync_destroy_signal_obj(sync);
        }


        dapi_queue_destroy(wait_queue);

        dapi_lock_release(lock);
    }
};

extern "C" void *sync_monitor_create()
{
    driver_monitor *dm = new driver_monitor();
    return dm;
}

extern "C" int sync_monitor_acquire(void *monitor, const uint32_t timeout )
{
    driver_monitor *dm = reinterpret_cast <driver_monitor *>(monitor);
    return dm->acquire(timeout);
}

extern "C" void sync_monitor_release(void *monitor)
{
    driver_monitor *dm = reinterpret_cast <driver_monitor *>(monitor);
    dm->release();
}

extern "C" void sync_monitor_destroy(void *monitor)
{
    driver_monitor *dm = reinterpret_cast <driver_monitor *>(monitor);
    dm->purge();
    delete dm;
}

