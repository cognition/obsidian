/* Coordinates access to a device bus.
 *
 * Author: Joseph Kinzel */

#include "bus_coordinator.h"
#include "module_loader.h"
#include "mfdevice.h"

//! Empty constructor
busCoordinator::busCoordinator() : msgPtr(nullptr), registerPtr(nullptr), busState(busStateType::busEmpty), busKey(0), coordOnlyMask(0) {};

/*! Default object constructor
 *  \param busModuleFilename File path of the driver module
 *  \param busDescription Descripton of the bus coordinator
 */
busCoordinator::busCoordinator(const String &busModuleFilename, const String &busDescription) : uidCreator(), msgPtr(nullptr),
    registerPtr(nullptr), moduleFilename(busModuleFilename), busName(busDescription), busState(busStateType::busModule), coordOnlyMask(0)
    {
        uidCreator.seed( random::get() );
        busKey = uidCreator.get();
    };

/*! Binds driver functions to the coordinator object
 *  \param desc Driver provided function descriptor
 *  \return driverOpSuccess if successful, driverOpBadState if the object is not waiting for a bind request
 */
int busCoordinator::bindManagerFunctions(const busManagerDesc *desc)
{
    if(busState == busStateType::busModule)
    {
        msgPtr = desc->recv;
        registerPtr = desc->notify;
        coordOnlyMask = desc->coord_only_mask;
        return driverOpSuccess;
    }
    else
        return driverOpBadState;
}

/*! Wrapper for processing a busMsg API request directed to this bus
 *  \param response Pointer to the response message buffer
 *  \param resSize Input and output pointer to the response message's maximum size and returned size
 *  \param msg Input message to the coordinator
 *  \param msgSize Size of the input message
 *  \param msgType Type of the message
 *  \param dev Device handle of the caller
 *
 *  \return busOpNotSupported if there is no driver attached to the coordinator for whatever reason or a value forwarded from the driver
 */
busMsgResponse busCoordinator::processMessage(const uint64_t addr, void *response, uint32_t *resSize, void *msg, const uint32_t msgSize, const uint32_t msgType, DEVICE_HANDLE dev)
{
    if(busState == busStateType::busModule)
        busState = loadBusModule();
    if(busState == busStateType::busActive)
        return msgPtr(addr, response, resSize, msg, msgSize, msgType, getSenderUniqueKey(dev) );
    else
        return busMsgNotSupported;
}

/*! Wrapper for processing the registration of a new bus range for this bus
 *  \param baseAddress Base address that's being registered
 *  \param spanLength Length of the address space being registered
 *  \param dev Device handle of the caller
 *  \return busOpNoSupported if there is no functional driver attached to the coordinator or a value forwarded from the driver if one is functional.
 */
int busCoordinator::processNotify(busRegDescriptor *desc, DEVICE_HANDLE dev, BUS_HANDLE *rbHandle)
{
    if(busState == busStateType::busModule)
        busState = loadBusModule();
    if(busState == busStateType::busActive)
    {
        void *retContext;
        BUS_HANDLE bHandle;
        createBusHandle(dev, bHandle);
        *rbHandle = bHandle;
        busRegisterResponse response = registerPtr(desc->baseAddress, desc->length, bHandle, getSenderUniqueKey(dev), &retContext);
        if(response == busRegisterApproved)
        {
            spinLockInstance lock(&coordLock);
            busRanges.insert( busProvider(desc, retContext) );
        }

        return response;
    }
    else
        return busOpNotSupported;
}

/*! Generates a multifunction device on bus coordinator's bus
 *  \param name String naming the device
 *  \param refSpaceId String Indicating the reference space ID for the device
 *  \param address Bus specific address of the device
 *  \param parent_handle Bus handle of the parent device
 *  \param coord_dev Coordinator device handle
 *  \return A bus handle for the newly created device or zero if the parent device could not be found
 */
BUS_HANDLE busCoordinator::generateMultifunctionDevice(const char *name, const char *refSpaceId, const uint64_t address, BUS_HANDLE parent_handle, DEVICE_HANDLE coord_dev)
{
    if(Device *pDev = decodeBusHandle(parent_handle))
    {
        BUS_HANDLE bHandle;
        multifunctionDevice *mfDev = new multifunctionDevice(name, refSpaceId, pDev);
        //Device *mfCast = mfDev;
        generateBusHandle(bHandle) = mfDev;
        return bHandle;
    }
    else
        return 0;
}

/*! Generates a new and unique bus handle
 *  \return A new and unique bus specific handle
 */
Device *&busCoordinator::generateBusHandle(BUS_HANDLE &bHandle)
{
    Device *nDev = nullptr;
    spinLockInstance lock(&coordLock);
    do
    {
        bHandle = uidCreator.get();
    }
    while( bHandleMap.get(bHandle) || !bHandle );
    return bHandleMap.insert(bHandle, nDev);
}

/*! Creates a bus specific handle for a device
 *  \param devHandle Global device handle of the device requesting a bus handle
 *  \param bHandle Return reference of the new bus handle
 */
void busCoordinator::createBusHandle(DEVICE_HANDLE devHandle, BUS_HANDLE &bHandle)
{
    Device *dev = DriverDevice::getDriverDevFromHandle(devHandle);
    generateBusHandle(bHandle) = dev;
}

bool busCoordinator::registerBusHandle(BUS_HANDLE bHandle, Device *dev)
{
    if(Device **entry = bHandleMap.get(bHandle))
    {
        *entry = dev;
        return true;
    }
    else
    {
        kprintf("[BUS ]: Failed to find entry for existing bus handle %X on bus \"%s\"\n", bHandle, busName.toBuffer() );
        return false;
    }
}

/*! Locates the device object referenced by a given bus handle
 *  \param bhandle Bus handle that references a device
 *  \return A pointer to the device object referenced by the handle, if one exists, or nullptr if the provided bus handle is invalid
 */
Device *busCoordinator::decodeBusHandle(const BUS_HANDLE bHandle)
{
    if(Device **result = bHandleMap.get(bHandle))
        return *result;
    else
        return nullptr;
}

/*! Validates a device handle as belonging to the bus coordinator represented by busCoordinator instance
 *  \param dh A device handle presumably referencing the coordinator
 *  \return True if the handle responds to this coordinator instance and false if it does not
 */
bool busCoordinator::validateCoordHandle(const DEVICE_HANDLE dh) const
{
    DEVICE_HANDLE expected = busDev->getHandle();
    if(dh != expected)
    {
        kprintf("Failed to validate coordinator handle expected: %X found: %X\n", expected, dh);
        return false;
    }
    else
        return true;
}

/*! Gets a bus provider object that's accessible by a specific device
 *  \param address Bus address that the provider enables access to
 *  \param operation Requested operation that may or may not be accessible only to the bus coordinator
 *  \param dHandle Device handle of the driver requesting the operation
 *  \return A pointer to the busProvider object that would potentially provide the requested operation, if accessible, or a nullptr if the request is invalid
 */
busProvider *busCoordinator::getProvider(const uint64_t address, const uint32_t operation, DEVICE_HANDLE dHandle)
{
    if(operation & coordOnlyMask)
    {
        if(dHandle != busDev->getHandle())
        {
            kprintf("[BUSC]: Non-coordinator device requested access to a coordinator operation on address: %X\n", address);
            return nullptr;
        }
    }

    return busRanges.find(address);
}

/*! Indicates if a given bus address range intersects with an existing controller
 *  \param base Base of the address range
 *  \param length Length of the address range
 */
bool busCoordinator::present(const uint64_t base, const uint64_t length) const
{
    regionObj64 obj(base, length);
    if(const busProvider *p = busRanges.find(obj) )
        return true;
    else
        return false;
}

/*! Attempts to load and initialize the module attached to the coordinator
 *  \return busModFail if initialization failed or bus active if the module was successfully initialized
 */
busCoordinator::busStateType busCoordinator::loadBusModule(void)
{
    if( moduleLoader::directLoadandInit(moduleFilename, busName, busDev ) )
        return busStateType::busActive;
    else
    {
        kprintf("Failed to load module %s for bus: %s\n", moduleFilename.toBuffer(), busName.toBuffer());
        return busStateType::busModFail;
    }
}

/*! Creates a unique key between the coordinator and another device
 *  \param senderHandle Device to create the unique key with
 *
 *  \note Primarily used to represent coordinator messages
 *  \return A unique key between a coordinator instance and device
 */
int64_t busCoordinator::getSenderUniqueKey(DEVICE_HANDLE senderHandle)
{
    return (busKey ^ senderHandle);
}

