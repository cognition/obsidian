#ifndef MODULE_ALLOCATOR_H_
#define MODULE_ALLOCATOR_H_

#include <compiler.h>

class tierModuleAllocator
{
    public:
        tierModuleAllocator();

        bool allocateRequiredSpace(void);
        void reset(void);

        uintptr_t getBaseAddress(void) const;
        uintptr_t reqPages(size_t pages);


    private:
        uintptr_t baseAddress;
        size_t pagesRequested;
};

#endif // MODULE_ALLOCATOR_H_
