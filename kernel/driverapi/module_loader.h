#ifndef MODULE_LOADER_H_
#define MODULE_LOADER_H_

extern "C" {
    #include "dapi_include/driverapi.h"
}
#include <string.h>
#include <compiler.h>
#include "driverdevice.h"
#include <templates/notify_event.cc>

namespace moduleLoader
{
    bool init(void);
    modStatus queueModule(const String modName, Device *parent, const busAddressDesc &addr, const String &devName, const String &refSpaceId, DriverDevice *&devDrv, notifyEvent<modStatus> *waitObj);
    bool directLoadandInit(const String &modName, const String &devName, DriverDevice *&dev);
    modStatus loadModules(void);
};



#endif // MODULE_LOADER_H_
