/* Driver API sync function API implementation
 *
 * Author: Joseph Kinzel */

#include "named_channel.h"
#include <refspace/refspace.h>

extern "C"
{
    #include "dapi_include/sync.h"
}

/*! Creates a new unlisted synchronization channel
 *
 *  \param buffer_size Size of the buffer, must be a power of two greater or equal to the architectural page size
 *  \param slot_count Number of pending message slots the channel has available to it
 *  \param chan_type Type of channel
 *
 *  \return A pointer to a DAPI channel reference object
 */
extern "C" dapi_channel_ref *sync_channel_create(const uint32_t buffer_size, const uint32_t slot_count, const dapi_channel_type chan_type)
{
    dapi_channel_ref *ret_ref;

    ret_ref = new dapi_channel_ref;
    ret_ref->current_slot = 0;
    ret_ref->flags = 0;
    ret_ref->type = chan_type;
    ret_ref->internal_obj = new driverComChannel(slot_count, buffer_size);

    return ret_ref;
}

extern "C" dapi_channel_ref *sync_channel_find(const char *name)
{
    namedChannel *chan;

    if(!refSpace::findNamedChannel(String(name), chan) )
        return NULL;
    else
    {
        dapi_channel_ref *ret_ref = new dapi_channel_ref;
        ret_ref->current_slot = 0;
        ret_ref->flags = 0;
        //TOOD: Fix me
        ret_ref->type = DAPI_CHANNEL_TYPE_MULTIP_MULTIR;
        ret_ref->internal_obj = static_cast <driverComChannel *> (chan);

        return ret_ref;
    }
}

/*! Publish a message to an active channel
 *
 *  \param channel Pointer to the channel reference object
 *  \param msg Pointer to the message buffer
 *  \param msg_size Size of the message
 *
 *  \return A non-zero value
 */
extern "C" int sync_channel_publish(dapi_channel_ref *channel, void *msg, const uint32_t msg_size)
{
    driverComChannel *channel_obj;

    channel_obj = reinterpret_cast<driverComChannel *> (channel->internal_obj);

    return channel_obj->publish( reinterpret_cast <uint8_t *> (msg), msg_size);
}

/*! Subscribes to a channel to automatically receive notification of new messages based on certain criteria
 *
 *  \param channel Pointer to the channel reference object
 *  \param cb_object Callback object that will be triggered if the notification criteria is met
 *  \param trigger_type Trigger behavior type that will determine notfication
 *
 *  \return A new channel reference with the specified trigger criteria
 */
extern "C" dapi_channel_ref *sync_channel_subscribe(dapi_channel_ref *channel, sync_object *cb_object, const uint32_t trigger_type)
{
    dapi_channel_ref *sub_ref;

    sub_ref = new dapi_channel_ref;

    Memory::copy(sub_ref, channel, sizeof(dapi_channel_ref));

    sub_ref->flags = trigger_type;

    return sub_ref;
}

/*! Attempts to read a single message worth of data from a channel
 *
 *  \param channel Pointer to the channel reference object
 *  \param target_buffer Buffer that will receive the message
 *  \param buffer_size Capacity of the receiving buffer
 *
 *  \return Number of bytes read in from the message, or zero if no new messages were available
 */
extern "C" uint32_t sync_channel_read(dapi_channel_ref *channel, void *target_buffer, const uint32_t buffer_size)
{
    driverComChannel *channel_obj;
    uint32_t bytesRead;

    channel_obj = reinterpret_cast<driverComChannel *> (channel->internal_obj);

    bytesRead = channel_obj->read(channel->current_slot, reinterpret_cast <uint8_t *> (target_buffer), buffer_size);
    if(bytesRead)
        channel->current_slot++;

    return bytesRead;
}
