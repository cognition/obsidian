/* Multifunction device implementation
 *
 * Simple device wrapper that allows for the establishment of a single device with no direct functionality of its own
 * but that maintains several child functions or interfaces that provide functionality or require drivers.
 *
 * Author: Joseph Kinzel */

#include "mfdevice.h"

multifunctionDevice::multifunctionDevice(const String &name, const String &refSpaceId, Device *parentDevice) : Device(name, refSpaceId, parentDevice) {}

bool multifunctionDevice::init(void *context)
{
    return true;
}
