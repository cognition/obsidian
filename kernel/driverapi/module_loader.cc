#include "module_loader.h"
#include "module_allocator.h"
#include "driverdevice.h"
#include <templates/hash_table.cc>
#include <exec/elf_module.h>
#include <cpu/localstate.h>
#include <templates/fifoq.cc>
#include <scheduler.h>

struct moduleLoadState
{
    Driver *mod;
    DriverDevice *dev;
    notifyEvent<modStatus> *waitObj;
};

//! Modules queued for loading in this pass
static fifoQueue<moduleLoadState> *passModules;
//! Allocator for the module pass
static tierModuleAllocator passAllocator;
//! Lock for the loader data structures
static ticketLock modAllocLock;
//! Hash table for loaded modules
static chainedHashTable<Driver, Driver::moduleHasher> *modTable;

namespace moduleLoader
{
    //! Global initializer, just allocates structures
    bool init(void)
    {
        passModules = new fifoQueue<moduleLoadState>();
        modTable = new chainedHashTable<Driver, Driver::moduleHasher>();

        return true;
    }


    bool directLoadandInit(const String &modName, const String &devName, DriverDevice *&dev)
    {
        tierModuleAllocator singleAlloc;
        Driver desired(modName);
        Driver *instance = modTable->findOrInsert(desired);
        modStatus status = instance->getStatus();
        switch(status)
        {
            case modStatus::modEntry:
                status = instance->attemptModAllocate(singleAlloc);
                if(status != modStatus::modAllocated)
                {
                    kprintf("Failed to allocate module %s Reason: %s\n", modName.toBuffer(), modStatusToString(status));
                    return false;
                }
                if(!singleAlloc.allocateRequiredSpace())
                {
                    kprintf("Module allocator failed to acquire space for module!\n");
                    return false;
                }
            case modStatus::modAllocated:
                status = instance->attemptLoadModule(singleAlloc);
                if(status != modStatus::modLoaded)
                {
                    kprintf("Failed to load allocated module %s Reason: %s\n", modName.toBuffer(), modStatusToString(status) );
                    return false;
                }
            case modStatus::modLoaded:
                dev = new DriverDevice(shared_ptr<Driver>(instance), devName.toBuffer() );
                if(!dev->init(nullptr))
                {
                    kprintf("Failed to initialize module %s\n", modName.toBuffer());
                    return false;
                }
                break;
            case modStatus::modCorrupted:
            case modStatus::modNoMemory:
            case modStatus::modNotFound:
            case modStatus::modTooBig:
                kprintf("Failed to load module %s Reason: %s\n", modName.toBuffer(), modStatusToString(status));
                return false;
                break;
        }

        return true;
    }

    /*! Attempts to queue a module for loading or returns a loaded module */
    modStatus queueModule(const String modName, Device *parent, const busAddressDesc &addr, const String &devName, const String &refSpaceId, DriverDevice *&devDrv, notifyEvent<modStatus> *waitObj)
    {
        Driver desired(modName);
        manualScopedLock lock(&modAllocLock);
        lock.acquire();


        Driver *instance = modTable->findOrInsert(desired);
        modStatus status = instance->getStatus();
        auto statusListener = waitObj->createListener( localState::getSched()->getCurrentTask(), modStatus::modEntry );

        switch(status)
        {
            case modStatus::modEntry:
                status = instance->attemptModAllocate(passAllocator);
                if(status != modStatus::modAllocated)
                {
                    lock.release();
                    kprintf("Unexpected result from mod entry! %u Corrupted: %u Not Found: %u\n", status, modStatus::modCorrupted, modStatus::modNotFound);
                    return status;
                }
            case modStatus::modAllocated:
                {
                    DriverDevice *drv = new DriverDevice(shared_ptr<Driver> (instance), devName, refSpaceId, parent, addr);
                    instance->bind(drv);
                    moduleLoadState state = {instance, drv, waitObj};
                    passModules->add(state);
                    waitObj->pushListener( static_pointer_cast<eventListener>(statusListener) );
                    lock.release();
                    do
                    {
                        kprintf("[MOD ]: Waiting for module load to occur - %u loads queued. Listener: %p\n", passModules->size(), statusListener.get()  );
                        Scheduler::waitCurrent(waitObj, 500);
                    } while( !statusListener->hasObtained() );
                    status = statusListener->getMessage();
                    if(status == modStatus::modLoaded)
                        devDrv = drv;
                    return status;
                    break;
                }
            case modStatus::modLoaded:
                    devDrv = new DriverDevice(shared_ptr<Driver> (instance), devName, refSpaceId, parent, addr);
                    instance->bind(devDrv);
                    lock.release();
                    return modStatus::modLoaded;
                    break;
            case modStatus::modTooBig:
            case modStatus::modNotFound:
            case modStatus::modCorrupted:
            case modStatus::modNoMemory:
                return status;
                break;
        }
    }

    /*! Attempts to load all queued modules
     *  \return modLoaded if successful or misc errors \sa elfModule::attemptLoadModule
     */
    modStatus loadModules(void)
    {
        modStatus retStatus = modStatus::modLoaded;
        spinLockInstance lock(&modAllocLock);
        //Don't both allocating space if we don't need any
        if(passModules->size())
        {
            if(!passAllocator.allocateRequiredSpace())
                return modStatus::modNoMemory;
            //Attempt to load each queued module
            do
            {
                modStatus status;
                moduleLoadState entry = passModules->remove();
                if(entry.mod->getStatus() == modStatus::modLoaded)
                {
                    entry.waitObj->setStatus(modStatus::modLoaded);
                    kprintf("[MOD ]: Module already loaded....\n");
                    continue;
                }
                else
                    status = entry.mod->attemptLoadModule(passAllocator);
                if(status != modStatus::modLoaded)
                {

                    kprintf("Failed to load module status: %s\n", modStatusToString(status));
                    retStatus = status;
                }
                else
                {
                    kprintf("[MOD ]: Setting wait return status...\n");

                    entry.waitObj->setStatus(modStatus::modLoaded);
                }
            } while( !passModules->empty() );

            if(retStatus != modStatus::modLoaded)
                return retStatus;

            passAllocator.reset();

        }
        else
        {
            //kprintf("No module load requests queued.\n");
            return modStatus::modAllocated;
        }

        /*
        //Initialize all driver dependent devices
        for(int modIndex = 0; modIndex < passModules->length(); modIndex++)
        {
            moduleLoadState &entry = passModules->at(modIndex);
            if(entry.mod->getStatus() == modStatus::modLoaded)
                if(!entry.dev->init(NULL))
                    kprintf("Failed to initialize DriverDevice!\n");
        }
        */

        return retStatus;
    }
};
