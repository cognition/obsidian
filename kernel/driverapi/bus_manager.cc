/* Bus management system
 * Abstracts the connection and addressing of various bus controllers (bus providers) and bus drivers (bus coordinators)
 * Also establishes DAPI hooks for bus access functions.
 *
 * Author: Joseph Kinzel */

#include "bus_manager.h"
#include "bus_coordinator.h"
#include <bus/pci.h>
#include <bus/io.h>
#include "module_loader.h"
#include "dbase.h"

static ticketLock coordsLock;
static Map<uint32_t, busCoordinator> busCoordinators;

static busProvider *locateProvider(const int busId, const uint64_t busAddress, const uint32_t operation, DEVICE_HANDLE dHandle)
{
    if(busCoordinator *manager = busCoordinators.get(busId))
        return manager->getProvider(busAddress, operation, dHandle);
    else
        return nullptr;
}

/*! Registers a database with a specific bus
 *  \param dbase_path Path of the database to load
 *  \param bus_id Bus database the file will be loaded into
 *  \param coord_dev Device handle of the bus coordinator
 *  \return driverOpBadArg is any of the arguments to the call are malformed, driverOpNotFound if the database is not found and driverOpSuccess if operation succeeded
 */
extern "C" int registerDriverDatabase(const char *dbase_path, const int bus_id, DEVICE_HANDLE coord_dev)
{
    busCoordinator *manager = busCoordinators.get(bus_id);
    if(!manager)
        return driverOpBadArg;

    if(!manager->validateCoordHandle(coord_dev))
        return driverOpBadArg;

    return driverDatabase::coordLoadDriverDatabase(bus_id, String(dbase_path) );
}

/*! Registers a device as a bus coordinator
 *  \param desc Descriptor of bus coordinator's operations
 *  \param coordinator Coordinator handle
 */
extern "C" int registerBusCoordinator(const busManagerDesc *desc, DEVICE_HANDLE coordinator)
{
    busCoordinator *manager = busCoordinators.get(desc->busId);
    if(!manager)
        return busOpResources;

    return manager->bindManagerFunctions(desc);
}

/*! Allocates a new bus handle for use by a bus coordinator driver
 *  \note Coordinator only call.
 *
 *  \param bus_id ID of the bus requesting the handle
 *  \param coord_dev Device handle of the bus coordinator
 *  \return A new and unique BUS_HANDLE value or zero if validation of the request failed
 */
extern "C" BUS_HANDLE coordAllocBusHandle(const int bus_id, DEVICE_HANDLE coord_dev)
{
    if( busCoordinator *manager = busCoordinators.get(bus_id) )
    {
        if( !manager->validateCoordHandle(coord_dev) )
            return 0;

        BUS_HANDLE bHandle;

        manager->generateBusHandle(bHandle);
        return bHandle;
    }
    else
        return 0;
}

/*! Registers a new multifunction device on a specified bus
 *  \note Coordinator only call.
 *
 *  \param name String describing the device
 *  \param ref_space_id Reference space id of the device
 *  \param address Bus specific address of the device
 *  \param parent_handle Handle of the parent device, if applicable
 *  \param coord_dev Device handle of the coordinator
 *  \param bus_id Bus ID value for the bus registering the device
 *  \return A valid bus handle of the device if registration succeeded or zero if it failed
 */
extern "C" BUS_HANDLE coordRegisterMultifunction(const char *name, const char *ref_space_id, const uint64_t address, BUS_HANDLE parent_handle, DEVICE_HANDLE coord_dev, const int bus_id)
{
    if( busCoordinator *manager = busCoordinators.get(bus_id) )
    {
        if( !manager->validateCoordHandle(coord_dev) )
            return 0;

        return manager->generateMultifunctionDevice(name, ref_space_id, address, parent_handle, coord_dev);
    }
    else
        return 0;
}

/*! Frees a bus handle for a given bus
 *  \note Coordinator exclusive call
 *
 *  \param bus_id Bus identification of the bus requesting the value
 *  \param handle Handle value to be freed
 *  \param coord_dev Coordinator device handle
 */
extern "C" void coordFreeBusHandle(const int bus_id, BUS_HANDLE handle, DEVICE_HANDLE coord_dev)
{
    if( busCoordinator *manager = busCoordinators.get(bus_id) )
    {
        if( !manager->validateCoordHandle(coord_dev) )
            return;

        //TODO: Finish me
    }
}

/*! Registers the driver for a bus coordinator
 *  \param busId ID of the bus to register a coordinator driver for
 *  \param fileName Filename of the driver
 *  \param busName Name of the bus the coordinator is serving
 *  \return True if the successful, false if a driver was already registered
 */
bool registerBusCoordinatorDriver(const uint32_t busId, const String &fileName, const String &busName)
{
    spinLockInstance lock(&coordsLock);
    //Only one manager allowed for a given bus
    const busCoordinator *manager = busCoordinators.get(busId);
    if(manager)
        return busOpResources;

    busCoordinator nManager(fileName, busName);
    busCoordinators.insert(busId, nManager);
    return true;
}

/*! Registers a bus controller with the bus coordinator and operation routing mechanims
 *  \param desc Bus controller descriptor
 *  \param regHandle Bus specific handle to identify the controller to the coordinator
 *  \param dHandle Device handle of the controller
 *  \return busOpInvalidArg arg if any of the arguments are invalid.
 *  \return busOpResources if a coordinator for the requested bus could not be found or another controller currently claims part of the range being provided.
 *  \return busOpNoAccess if the coordinator refuses the registration request.
 *  \return busOpSuccess If the request was successful.
 */
extern "C" int registerBusRange(busRegDescriptor *desc, BUS_HANDLE *regHandle, DEVICE_HANDLE dHandle)
{

    if(!desc || !regHandle)
        return busOpInvalidArg;

    //Validate the descriptor
    if(desc->capabilities & BUS_CAPS_TRANSACTIONS)
        if(!desc->queueTransaction || !desc->runTransactions)
            return busOpInvalidArg;
    if(desc->capabilities & BUS_CAPS_CONFIG_ADDR)
        if(!desc->updateConfig)
            return busOpInvalidArg;


    //Attempt to find the bus coordinator and notify it of the registration request
    if(busCoordinator *manager = busCoordinators.get(desc->busId))
    {
        //Check for an existing controller that overlaps the requested range
        if( manager->present(desc->baseAddress, desc->length) )
            return busOpResources;

        int status = manager->processNotify(desc, dHandle, regHandle );
        if(status != busRegisterApproved)
            return busOpNoAccess;
        else
            return busOpSuccess;
    }
    else
        return busOpResources;
}

/*! Registers a new child device at a specific bus address
 *  \param dev_addr Bus specific address of the child device
 *  \param coord_dev Device handle of the bus coordinator
 *  \param parent_handle Bus specific handle of the parent device
 *  \param id_key Identifier key of the device
 *  \param context Device initialization context information
 */
extern "C" int registerNewDevice(const busAddressDesc *dev_addr, DEVICE_HANDLE coord_dev, BUS_HANDLE parent_handle, const uint64_t id_key, BUS_HANDLE bus_spec, void *context )
{
    int busId = dev_addr->busId;
    //Attempt to find and validate the bus coordinator and parent device handles
    if( busCoordinator *manager = busCoordinators.get(busId) )
    {
        if( !manager->validateCoordHandle(coord_dev) )
            return driverOpBadArg;
        Device *pDevice = manager->decodeBusHandle(parent_handle);
        if( !pDevice )
        {
            kprintf("Unable to decode bus handle %X for bus %u\n", parent_handle, busId);
            return driverOpBadArg;
        }

        //Query the driver database for a driver that matches the new device.
        driverRequestStatus status = driverDatabase::loadDbDriver(manager, dev_addr, bus_spec, id_key, pDevice, context);
        switch(status)
        {
            case driverRequestStatus::drModNotFound:
                kprintf("[_BUS]: No matching module found for bus: %u key: %X\n", busId, id_key);
                return driverOpNotFound;
                break;
            case driverRequestStatus::drModRegisterFailed:
                return driverOpBadState;
                break;
            case driverRequestStatus::drModLoadFailed:
                //kprintf("[_BUS]: Failed to load module for bus: %u key: %X\n", busId, id_key);
                return driverOpBadState;
                break;
            case driverRequestStatus::drModInitFailed:
                //kprintf("[_BUS]: Failed to initialize module for address: %X bus: %u key: %X\n", dev_addr->address, busId, id_key);
                return driverOpBadState;
                break;
            case driverRequestStatus::drQueued:
                //kprintf("[_BUS]: Queued module for address: %X bus: %u key: %X\n", dev_addr->address, busId, id_key);
                return driverOpSuccess;
                break;
            case driverRequestStatus::drRunning:
                //kprintf("[_BUS]: Module loaded and running for address: %X bus: %u key: %u\n", dev_addr->address, busId, id_key);
                return driverOpSuccess;
                break;
        }
    }
    else
    {
        kprintf("[_BUS]: Unable to resolve address %X for bus %u\n", dev_addr->address, dev_addr->busId);
        return busOpAddressInvalid;
    }
}

/*! Sends a message to a bus coordinator
 *  \param response Pointer to the response buffer
 *  \param resSize of the response buffer during input, size of the provided response during output
 *  \param busId Bus ID of the target coordinator
 *  \param msg Message buffer
 *  \param msgSize Size of the message buffer
 *  \param msgType Type code of the message
 *  \param dev Device handle of the sender
 */
extern "C" busMsgResponse busMsg(void *response, uint32_t *resSize, const int busId, uint64_t busAddress, void *msg, const uint32_t msgSize, const uint32_t msgType, DEVICE_HANDLE dev)
{
    if( busCoordinator *manager = busCoordinators.get(busId) )
        return manager->processMessage(busAddress, response, resSize, msg, msgSize, msgType, dev);
    else
        return busMsgNotSupported;
}

extern "C" int busQueueTransaction(const int bus_id, const uint64_t address, void *t_info, const uint32_t t_info_length, sync_object *sync, DEVICE_HANDLE caller_handle)
{
    if( busProvider *provider = locateProvider(bus_id, address, BUS_CAPS_TRANSACTIONS, caller_handle) )
        return provider->queueTransaction(address, t_info, t_info_length, sync);
    else
    {
        kprintf("Unable to locate bus provider for bus %u address: %X\n", bus_id, address);
        return busOpAddressInvalid;
    }
}

extern "C" int busRunTransactions(const int bus_id, const uint64_t address, sync_object *sync, DEVICE_HANDLE caller_handle)
{
    if( busProvider *provider = locateProvider(bus_id, address, BUS_CAPS_TRANSACTIONS, caller_handle) )
        return provider->runTransactions(address, sync);
    else
        return busOpAddressInvalid;
}

extern "C" int busConfigAddress(const int bus_id, const uint64_t address, void *config_info, const uint32_t config_size, sync_object *sync, DEVICE_HANDLE caller_handle)
{
    if( busProvider *provider = locateProvider(bus_id, address, BUS_CAPS_CONFIG_ADDR, caller_handle) )
        return provider->configAddress(address, config_info, config_size, sync);
    else
        return busOpAddressInvalid;
}

extern "C" int pciReadByte(uint8_t *dest, const uint32_t busAddress, const uint16_t offset)
{
    if(pciBus::readByte(busAddress, offset, *dest))
       return busOpSuccess;
    else
        return busOpAddressInvalid;
}

extern "C" int pciReadWord(uint16_t *dest, const uint32_t busAddress, const uint16_t offset)
{
    if(pciBus::readWord(busAddress, offset, *dest))
       return busOpSuccess;
    else
        return busOpAddressInvalid;

}

extern "C" int pciReadDWord(uint32_t *dest, const uint32_t busAddress, const uint16_t offset)
{
    if(pciBus::readDWord(busAddress, offset, *dest))
       return busOpSuccess;
    else
        return busOpAddressInvalid;

}

extern "C" int pciReadQWord(uint64_t *dest, const uint32_t busAddress, const uint16_t offset)
{
    if(pciBus::readQWord(busAddress, offset, *dest))
       return busOpSuccess;
    else
        return busOpAddressInvalid;

}

extern "C" int pciWriteByte(const uint32_t busAddress, const uint16_t offset, const uint8_t value)
{
    if(pciBus::writeByte(busAddress, offset, value))
        return busOpSuccess;
    else
        return busOpAddressInvalid;

}

extern "C" int pciWriteWord(const uint32_t busAddress, const uint16_t offset, const uint16_t value)
{
    if(pciBus::writeWord(busAddress, offset, value))
        return busOpSuccess;
    else
        return busOpAddressInvalid;

}

extern "C" int pciWriteDWord(const uint32_t busAddress, const uint16_t offset, const uint32_t value)
{
    if(pciBus::writeDWord(busAddress, offset, value))
        return busOpSuccess;
    else
        return busOpAddressInvalid;

}

extern "C" int pciWriteQWord(const uint32_t busAddress, const uint16_t offset, const uint64_t value)
{
    if(pciBus::writeQWord(busAddress, offset, value))
        return busOpSuccess;
    else
        return busOpAddressInvalid;

}

int pciReadBar(uint64_t *base, uint64_t *length, int *type, const uint32_t address, const uint8_t barNum)
{
    uint32_t flags;
    if(barNum > 5)
        return busOpInvalidArg;
    if(pciBus::readBAR(*base, *length, flags, address, barNum))
    {
        if(flags & PCI_BAR_IO_SPACE)
            *type = BUS_ID_IO;
        else
            *type = BUS_ID_MEMORY;

        return busOpSuccess;
    }
    else
        return busOpAddressInvalid;
}

extern "C" int pciEnableBusMaster(const uint32_t address)
{
    uint16_t cmdValue;
    if(!pciBus::readWord(address, PCI_CONFIG_COMMAND, cmdValue))
        return busOpAddressInvalid;

    cmdValue |= PCI_COMMAND_BUS_MASTER;
    if(!pciBus::writeWord(address, PCI_CONFIG_COMMAND, cmdValue))
        return busOpAddressInvalid;

    return busOpSuccess;
}

/* IO Bus functions */
int ioBusReadByte(uint8_t *dest, const uint16_t port)
{
    *dest = ioBus::readByte(port);
    return busOpSuccess;
}

int ioBusReadWord(uint16_t *dest, const uint16_t port)
{
    *dest = ioBus::readWord(port);
    return busOpSuccess;
}

int ioBusReadDWord(uint32_t *dest, const uint16_t port)
{
    *dest = ioBus::readDWord(port);
    return busOpSuccess;
}

int ioBusWriteByte(const uint16_t port, const uint8_t value)
{
    ioBus::writeByte(port, value);
    return busOpSuccess;
}

int ioBusWriteWord(const uint16_t port, const uint16_t value)
{
    ioBus::writeWord(port, value);
    return busOpSuccess;
}

int ioBusWriteDWord(const uint16_t port, const uint32_t value)
{
    ioBus::writeDWord(port, value);
    return busOpSuccess;
}
