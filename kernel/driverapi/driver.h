/* Author: Joseph Kinzel */

#ifndef DRIVER_H_
#define DRIVER_H_

#include <exec/elf_module.h>
#include "module_allocator.h"

extern "C"
{
    #include "dapi_include/driverapi.h"
}

enum class driverRequestStatus
{
    drModNotFound,
    drModLoadFailed,
    drQueued,
    drModInitFailed,
    drModRegisterFailed,
    drRunning
};

struct moduleInfo
{
    initFunc init;
    deinitFunc deinit;
    enumerateFunc enumerate;
    uint32_t flags;
};

//! Represents a request to load a module
class Driver : protected elfModule
{
    public:
        class moduleHasher
        {
            public:
                uint64_t operator()(const Driver &mod)
                {
                    return mod.getHash();
                }
        };


        Driver();
        Driver(const String requestedDriver);

        modStatus attemptModAllocate(tierModuleAllocator &alloc);
        modStatus attemptLoadModule(tierModuleAllocator &alloc);
        void bind(class DriverDevice *dev);

        bool operator ==(const Driver &other);

        driverOperationResult init(DEVICE_HANDLE handle, BUS_HANDLE bus_spec, void **context, void *init_context, busAddressDesc *addr);

        modStatus getStatus(void) const
        {
           return modReqStatus;
        }

        shared_ptr<moduleInfo> getInfo(void)
        {
           return shared_ptr<moduleInfo> (info);
        }

    private:
        shared_ptr<moduleInfo> info;
        Vector<DriverDevice *> devices;
        ticketLock driverLock;
        int activeDeviceCount;
        volatile modStatus modReqStatus;
};

#endif // DRIVER_H_
