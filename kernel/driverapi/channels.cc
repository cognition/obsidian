/* Communication channels implementation
 *
 * Allows buffered and event driven message passing between components
 *
 * Author: Joseph Kinzel */

#include "channels.h"
#include <memory/virtual.h>

/*! Constructor
 *
 *  \param slotCount Number of slots, should be a power of two
 *  \param bufferSize Size of the buffer, should be a power of two >>> slot count
 */
driverComChannel::driverComChannel(const uint16_t slotCount, const uint16_t bufferSize) : channelSlots(slotCount), channelSize(bufferSize), listenerCount(0)
{
    uint32_t bufferPages;

    bufferPages = bufferSize/REGULAR_PAGE_SIZE;

    buffer = reinterpret_cast<uint8_t *> ( virtualMemoryManager::allocateRegion(bufferPages, PAGE_WRITE | PAGE_PRESENT) );
    slotInfo = new uint64_t[slotCount];
    Memory::set( const_cast <uint64_t *> (slotInfo), 0, slotCount*sizeof(uint64_t));

    currentState = packState(0, 0, 0, bufferSize);
}

//! Destructor
driverComChannel::~driverComChannel()
{
    virtualMemoryManager::unmapRegion( reinterpret_cast<uintptr_t> (buffer), channelSize, true, true );

    delete[] slotInfo;
}

/*! Attempts to publishes a message to the channel
 *
 *  \param srcBuffer Pointer to the source message buffer
 *  \param msgLength Message length
 *
 *  \return True if the publish operation succeeded, false if there was insufficient space or slots to publish the message
 */
bool driverComChannel::publish(uint8_t *srcBuffer, const uint16_t msgLength)
{
    uint16_t slot, offset;
    uint64_t initial, update, slotEntry;

    do
    {
        uint16_t spaceRem, oldSlot, targetSlot;

        // Extract all packed fields
        initial = currentState;

        spaceRem = extractBufferSpace(initial);
        oldSlot = extractOldSlot(initial);
        targetSlot = extractNextSlot(initial);
        offset = extractOffset(initial);

        // Check if there's sufficient buffer space for the message
        if(spaceRem >= msgLength)
        {
            // Check if there's a free slot for the message
            if((oldSlot == targetSlot) && slotInfo[oldSlot])
                doFullTrigger(oldSlot);
            else
            {
                spaceRem -= msgLength;
                slot = targetSlot++;
                update = packState(offset + msgLength, targetSlot % channelSlots, oldSlot, spaceRem);

                if(atomicBoolCAS(&currentState, initial, update))
                    break;
            }
        }
        else
            doFullTrigger(oldSlot);
    } while ( true );

    /* Copy the data out then update the slot entry to finalize the operation */

    copyBufferToRingBuffer(srcBuffer, offset, msgLength);

    slotEntry = createSlotEntry(offset, msgLength, listenerCount);
    atomicBoolCAS(&slotInfo[slot], 0, slotEntry);

    if( triggerEachPublish.length() )
        doPublishTrigger(slot);

    return true;
}

/*! Signal all the callbacks for a full channel
 *
 *  \param slot First full slot
 */
void driverComChannel::doFullTrigger(const uint16_t slot)
{
    for(int syncIndex = 0; syncIndex < triggerFull.length(); syncIndex++)
        sync_object_signal( triggerFull[syncIndex], slot);
}

/*! Signal all the callbacks for a newly published message
 *
 *  \param slot Slot that the message was published to
 */
void driverComChannel::doPublishTrigger(const uint16_t slot)
{
    for(int syncIndex = 0; syncIndex < triggerEachPublish.length(); syncIndex++)
        sync_object_signal( triggerEachPublish[syncIndex], slot);
}

/*! Reads a single message from the channel
 *
 *  \param lastSlot Last slot read by the requestor
 *  \param targetBuffer Target buffer to copy the message into
 *  \param bufferMax Maximum size of the target buffer
 *
 *  \return Number of bytes copied out into the buffer
 */
uint32_t driverComChannel::read(const uint16_t lastSlot, uint8_t *targetBuffer, const uint16_t bufferMax)
{
    uint64_t slot, state;
    uint32_t slotOffset;

    slot = lastSlot;
    slot %= channelSlots;

    state = currentState;

    uint64_t updatedEntry, releaseBytes, slotEntry;

    //Attempt to retrieve the slot entry and its info
    do
    {
        slotEntry = slotInfo[slot];

        slotOffset = getSlotOffset(slotEntry);
        releaseBytes = getSlotBytes(slotEntry);
    } while(!slotEntry);

    copyBufferFromRingBuffer(targetBuffer, slotOffset, bufferMax, releaseBytes);

    // Attempt to retrieve and update the slot state
    do
    {
        slotEntry = slotInfo[slot];
        updatedEntry = slotEntry;

        if(slotValid(slotEntry) && getSlotListeners(slotEntry) )
        {

            if( getSlotListeners(slotEntry) == 1)
                updatedEntry = 0;
            else
                --updatedEntry;

        }
        else
        {
            kprintf("Invalid slot entry value: %X for slot %u\n", slotEntry, slot);
            return 0;
        }
    } while ( !atomicBoolCAS(&slotInfo[slot], slotEntry, updatedEntry) );

    if(!updatedEntry)
    {
        //Update the overall state to indicate additional free buffer space
        uint64_t updatedState;
        do
        {
            state = currentState;
            updatedState = incStateBufferSize(state, releaseBytes);

        } while(!atomicBoolCAS(&currentState, state, updatedState));
    }

    return releaseBytes;
}

/*! Subscribes to a channel with a specified trigger behavior
 *
 *  \param cb_object Synchronization object that will be signaled when the triggered condition occurs
 *  \param triggerBehavior Determines triggering behavior, either on each publish or only when the channel is full
 */
void driverComChannel::subscribe(sync_object *cb_object, const uint32_t triggerBehavior)
{
    if(triggerBehavior == DAPI_CHANNEL_TRIGGER_PUBLISH)
        triggerEachPublish.insert(cb_object);
    else
        triggerFull.insert(cb_object);

    listenerCount++;
}

/*! Copies data to the channel's ring buffer
 *
 *  \param srcBuffer Source buffer containing the data
 *  \param offset Offset into the ring buffer where the copied data should begin
 *  \param bufferSize Size of the input buffer
 */
void driverComChannel::copyBufferToRingBuffer(uint8_t *srcBuffer, const uint32_t offset, const uint32_t bufferSize)
{
    uint32_t tail = channelSize - offset;

    if( tail >= bufferSize)
        Memory::copy(&buffer[offset], srcBuffer, bufferSize);
    else
    {
        Memory::copy(&buffer[offset], srcBuffer, tail);
        Memory::copy(buffer, &srcBuffer[tail], bufferSize - tail);
    }
}

/*! Copies data from the channel's ring buffer to an output buffer
 *
 *  \param targetBuffer Pointer to the output buffer
 *  \param slotOffset Beggining offset of the data within the ring buffer that contains the source data
 *  \param bufferMax Maximum size of the output buffer
 *  \param msgSize Size of the data in the ring buffer
 */
void driverComChannel::copyBufferFromRingBuffer(uint8_t *targetBuffer, const uint32_t slotOffset, const uint32_t bufferMax, const uint32_t msgSize)
{
    uint32_t outSize = max(msgSize, bufferMax);

    uint32_t tail = channelSize - slotOffset;
    if( tail >= outSize )
        Memory::copy(targetBuffer, &buffer[slotOffset], outSize);
    else
    {
        Memory::copy(targetBuffer, &buffer[slotOffset], tail);
        Memory::copy(&targetBuffer[tail], buffer, outSize - tail);
    }
}

