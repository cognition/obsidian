/* Author: Joseph Kinzel */

#include "driver.h"

Driver::Driver() : elfModule(), activeDeviceCount(0), modReqStatus(modStatus::modEntry) {};

/*! Constructor
 *  \param requestName Name of the module being requested
 */
Driver::Driver(const String requestName) : elfModule(requestName), activeDeviceCount(0), modReqStatus(modStatus::modEntry)
{
}

driverOperationResult Driver::init(DEVICE_HANDLE dev_handle, BUS_HANDLE bus_spec, void **context, void *init_context, busAddressDesc *addr)
{
	if(modReqStatus != modStatus::modLoaded)
	{
		kprintf("Error: Cannot initialize device due to status mismatch - Expected: %s Found: %s\n", modStatusToString(modStatus::modLoaded), modStatusToString(modReqStatus)) ;
		return driverOpResources;
	}

	return info->init(dev_handle, bus_spec, addr, init_context, context);
}

/*! Attempts allocate space based on the executable's header
*  \param alloc Pass allocator to use
*  \return modNotFound if the module cannot be found, modCorrupted if the headers are corrupted or modAllocated if allocation succeeded
*/
modStatus Driver::attemptModAllocate(tierModuleAllocator &alloc)
{
	if(!open())
		modReqStatus = modStatus::modNotFound;
	else if(!parse(elfFile::ET_DYN, elfFile::ELF_MODULE))
	{
		modReqStatus = modStatus::modCorrupted;
		kprintf("Module parse failed!\n");
	}
	else
		modReqStatus = allocateModule(alloc);
	return modReqStatus;
}

/*! Attempts to load a previously allocated module
*  \param alloc Allocator used for determining the modules in memory mapping
*  \return modCorrupted if the modules symbol table is corrupted or modLoaded if the module was loaded successfully
*/
modStatus Driver::attemptLoadModule(tierModuleAllocator &alloc)
{
	modReqStatus = loadModule(alloc);
	if(modReqStatus == modStatus::modLoaded)
	{
		const modExportTable *srcTable = reinterpret_cast <const modExportTable *> (tableAddress);
		info.reset(new moduleInfo);
		info->init = srcTable->init;
		info->deinit = srcTable->deinit;
		info->enumerate = srcTable->enumerate;
		info->flags = 0;
	}

	return modReqStatus;
}

/*! Binds a device to this driver instance
*  \param dev Drive based device to bind
*/
void Driver::bind(DriverDevice *dev)
{
	spinLockInstance lock(&driverLock);
	devices.insert(dev);
	activeDeviceCount++;
}

bool Driver::operator ==(const Driver &other)
{
	hashedString *base = this;
	return (*base == other);
}
