#ifndef DRIVER_DBASE_H_
#define DRIVER_DBASE_H_

#include <string.h>
#include "driver.h"
#include "bus_coordinator.h"

extern "C"
{
    #include "dapi_include/driverapi.h"
}

//Simple driver description entry, just the name of the driver and the path to the module
class driverDesc
{
    public:
        driverDesc();
        driverDesc(const String &name, const String &refId, const String &path);

        const String &getDriverName(void) const;
        const String &getDriverRefId(void) const;
        const String &getDriverPath(void) const;

private:
        const String driverName, driverRef, driverPath;
};

namespace driverDatabase
{

    bool findModule(const driverDesc *&drv, const busAddressDesc *addr, const uint64_t key);
    driverOperationResult coordLoadDriverDatabase(const int busId, const String &path);
    driverRequestStatus loadDbDriver(busCoordinator *manager, const busAddressDesc *addr, BUS_HANDLE bus_spec, const uint64_t key, Device *pDevice, void *context);
};
#endif // DRIVER_DBASE_H_
