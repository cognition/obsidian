#ifndef BUS_PROVIDER_H_
#define BUS_PROVIDER_H_

#include "driverdevice.h"
#include <templates/map.cc>
#include <prng.h>

extern "C"
{
    #include "dapi_include/driverapi.h"
}

/*! Bus Provider class
 *
 *  This is a general bus controller interface that relies on an external driver to provide access to the bus itself.
 *  Bus Provider objects are registered with and have their access controlled by a single Bus Coordinator object for a given bus.
 *
 */
class busProvider : public regionObj64
{
    public:
        busProvider();
        busProvider(const busRegDescriptor *desc, void *mContext);

        int readQWord(uint64_t *dest, const uint64_t address, const uint32_t offset) const;
        int readDWord(uint32_t *dest, const uint64_t address, const uint32_t offset) const;
        int readWord(uint16_t *dest, const uint64_t address, const uint32_t offset) const;
        int readByte(uint8_t *dest, const uint64_t address, const uint32_t offset) const;
        int readPacket(void *dest, const uint64_t address, const uint32_t offset, const uint32_t *bufferSize) const;

        int writeQWord(const uint64_t address, const uint32_t offset, const uint64_t value) const;
        int writeDword(const uint64_t address, const uint32_t offset, const uint32_t value) const;
        int writeWord(const uint64_t address, const uint32_t offset, const uint16_t value) const;
        int writeByte(const uint64_t address, const uint32_t offset, const uint8_t value) const;
        int writePacket(const uint64_t address, const uint32_t offset, void *packet, const uint32_t packLength) const;
        int queueTransaction(const uint64_t address, void *t_info, const uint32_t t_info_length, sync_object *sync);
        int runTransactions(const uint64_t address, sync_object *sync);
        int configAddress(const uint64_t address, void *config_info, const uint32_t config_size, sync_object *sync);
        void *getManagerContext(void);

    private:

        uint32_t capabilities;
        read64Func readQWordPtr;
        read32Func readDWordPtr;
        read16Func readWordPtr;
        read8Func readBytePtr;
        readPacketFunc readPacketPtr;

        write64Func writeQWordPtr;
        write32Func writeDWordPtr;
        write16Func writeWordPtr;
        write8Func writeBytePtr;
        writePacketFunc writePacketPtr;

        queueTransactionFunc queueTransactionPtr;
        runTransactionsFunc runTransactionsPtr;

        addressConfigFunc updateAddrPtr;

        void *contextPtr, *managerContext;

};

#endif // BUS_PROVIDER_H_
