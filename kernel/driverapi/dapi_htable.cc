#include <templates/hash_table.cc>

extern "C" {
#include "dapi_include/dapi_container.h"
}

class nullHash
{
    nullHash(){};

    uint64_t operator()(void *obj)
    {
        return 0;
    }
};

typedef chainedHashTable<void *, nullHash> genericHt;

extern "C" void *dapi_ht_create(const uint32_t init_size)
{
    genericHt *hashTable = new genericHt(init_size);
    return reinterpret_cast <void *>(hashTable);
}

extern "C" void dapi_ht_insert(void *ht, int64_t key, void *value)
{
    genericHt *hashTable = reinterpret_cast<genericHt *> (ht);
    hashTable->insert(value, key);
}

extern "C" void *dapi_ht_get_entry( void *ht, int64_t key)
{
    genericHt *hashTable = reinterpret_cast<genericHt *> (ht);
    void **result = hashTable->findByHash(key);
    if(!result)
        return nullptr;
    else
        return *result;
}

extern "C" void dapi_ht_destroy(void *ht)
{
    genericHt *hashTable = reinterpret_cast<genericHt *> (ht);
    delete hashTable;
}
