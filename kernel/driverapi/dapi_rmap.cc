#include <templates/radix_map.cc>

extern "C"
{
    #include "dapi_include/dapi_container.h"
}

typedef radixMap<uint64_t, void *, 4> rmapType;

/* Module radix map C API
 * All maps have a 64-bit unsigned key, radix of 4 and store pointers
 */

/*! Creates a radix map for a module to use
 *  \return A pointer to the newly created radix map
 */
extern "C" void *dapi_radix_map_create(void)
{
    rmapType *retValue = new rmapType();

    return retValue;
}

/*! Destroys an existing radix map
 *  \param rmap Map to destory
 */
extern "C" void dapi_radix_map_destroy(void *rmap)
{
    rmapType *rm = reinterpret_cast<rmapType *> (rmap);
    delete rm;
}

/*! Attempts to insert an object into the map
 *  \param rmap Radix map object pointer
 *  \param key Key of the value being inserted
 *  \param value Value to be inserted
 *  \return 0 if a conflicting object is already in the map, 1 if the insertion succeeded
 */
extern "C" int dapi_radix_map_insert(void *rmap, const uint64_t key, void *value)
{
    rmapType *rm = reinterpret_cast<rmapType *> (rmap);

    if(!rm->insert(key, value))
        return 0;
    else
        return 1;
}

/*! Attempts to locate an existing entry in the radix map
 *  \param rmap Radix map to search for entry
 *  \param key Key corresponding to the entry
 *  \return A null pointer if the value was not found or the value corresponding to the requested key
 */
extern "C" const void *dapi_radix_map_find(void *rmap, const uint64_t key)
{
    rmapType *rm = reinterpret_cast<rmapType *> (rmap);

    void *const *value = rm->find(key);
    if(!value)
        return nullptr;
    else
        return *value;
}
