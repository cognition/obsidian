/* Author: Joseph Kinzel */

#include "bus_provider.h"

//! Empty constructor
busProvider::busProvider() : capabilities(0), contextPtr(0) {};

/*! Default constructor
 *  \param desc Driver provided descriptor
 *  \param mContext Bus coordinator, controller specific context
 */
busProvider::busProvider(const busRegDescriptor *desc, void *mContext) :
        regionObj64(desc->baseAddress, desc->length),
        capabilities(desc->capabilities),
        //Read function pointers
        readQWordPtr(desc->readQWord),
        readDWordPtr(desc->readDWord),
        readWordPtr(desc->readWord),
        readBytePtr(desc->readByte),
        readPacketPtr(desc->readPacket),
        //Write function pointers
        writeQWordPtr(desc->writeQWord),
        writeDWordPtr(desc->writeDWord),
        writeWordPtr(desc->writeWord),
        writeBytePtr(desc->writeByte),
        writePacketPtr(desc->writePacket),
        //Transaction function pointers
        queueTransactionPtr(desc->queueTransaction),
        runTransactionsPtr(desc->runTransactions),

        updateAddrPtr(desc->updateConfig),

        //Context values for the device providing access to the bus range and the bus specific coordinator
        contextPtr(desc->contextPtr),
        managerContext(mContext) {};

/*! Read a quad word from the bus
 *  \param dest Destination quad word sized pointer
 *  \param address Bus specific address
 *  \param offset Offset into a device specific space
 *  \return busOpNotSupported if the controller lacks the request capability
 */
int busProvider::readQWord(uint64_t *dest, const uint64_t address, const uint32_t offset) const
{
    if(capabilities & BUS_CAPS_READ_QWORD)
        return readQWordPtr(dest, contextPtr, address, offset);
    else
        return busOpNotSupported;
}

/*! Read a double word from the bus
 *  \param dest Destination double word sized pointer
 *  \param address Bus specific address
 *  \param offset Offset into a device specific space
 *  \return busOpNotSupported if the controller lacks the request capability
 */
int busProvider::readDWord(uint32_t *dest, const uint64_t address, const uint32_t offset) const
{
    if(capabilities & BUS_CAPS_READ_DWORD)
        return readDWordPtr(dest, contextPtr, address, offset);
    else
        return busOpNotSupported;
}

/*! Read a word from the bus
 *  \param dest Destination word sized pointer
 *  \param address Bus specific address
 *  \param offset Offset into a device specific space
 *  \return busOpNotSupported if the controller lacks the request capability
 */
int busProvider::readWord(uint16_t *dest, const uint64_t address, const uint32_t offset) const
{
    if(capabilities & BUS_CAPS_READ_WORD)
        return readWordPtr(dest, contextPtr, address, offset);
    else
        return busOpNotSupported;
}

/*! Read a byte from the bus
 *  \param dest Destination byte sized pointer
 *  \param address Bus specific address
 *  \param offset Offset into a device specific space
 *  \return busOpNotSupported if the controller lacks the request capability
 */
int busProvider::readByte(uint8_t *dest, const uint64_t address, const uint32_t offset) const
{
    if(capabilities & BUS_CAPS_READ_BYTE)
        return readBytePtr(dest, contextPtr, address, offset);
    else
        return busOpNotSupported;
}

/*! Read a packet from the bus
 *  \param dest Destination packet buffer pointer
 *  \param address Bus specific address
 *  \param offset Offset into a device specific space
 *  \param bufferSize Pointer to the value that indicates the allocated size of a buffer on entry and the total size of the buffer upon return
 *  \return busOpNotSupported if the controller lacks the requested capability
 */
int busProvider::readPacket(void *dest, const uint64_t address, const uint32_t offset, const uint32_t *bufferSize) const
{
    if(capabilities & BUS_CAPS_READ_PACKET)
        return readPacketPtr(dest, contextPtr, address, offset, bufferSize);
    else
        return busOpNotSupported;
}

/*! Writes a quad word value to the bus
 *  \param address Bus address of the device targeted by the write
 *  \param offset Device specific offset to write the value to
 *  \param value Value to write
 *  \return busOpNotSupported if the controller lacks the requested capability
 */
int busProvider::busProvider::writeQWord(const uint64_t address, const uint32_t offset, const uint64_t value) const
{
    if(capabilities & BUS_CAPS_WRITE_QWORD)
        return writeQWordPtr(contextPtr, address, offset, value);
    else
        return busOpNotSupported;
}

/*! Writes a double word value to the bus
 *  \param address Bus address of the device targeted by the write
 *  \param offset Device specific offset to write the value to
 *  \param value Value to write
 *  \return busOpNotSupported if the controller lacks the requested capability
 */
int busProvider::writeDword(const uint64_t address, const uint32_t offset, const uint32_t value) const
{
    if(capabilities & BUS_CAPS_WRITE_DWORD)
        return writeDWordPtr(contextPtr, address, offset, value);
    else
        return busOpNotSupported;
}

/*! Writes a word value to the bus
 *  \param address Bus address of the device targeted by the write
 *  \param offset Device specific offset to write the value to
 *  \param value Value to write
 *  \return busOpNotSupported if the controller lacks the requested capability
 */
int busProvider::writeWord(const uint64_t address, const uint32_t offset, const uint16_t value) const
{
    if(capabilities & BUS_CAPS_WRITE_WORD)
        return writeWordPtr(contextPtr, address, offset, value);
    else
        return busOpNotSupported;
}

/*! Writes a byte value to the bus
 *  \param address Bus address of the device targeted by the write
 *  \param offset Device specific offset to write the value to
 *  \param value Value to write
 *  \return busOpNotSupported if the controller lacks the requested capability
 */
int busProvider::writeByte(const uint64_t address, const uint32_t offset, const uint8_t value) const
{
    if(capabilities & BUS_CAPS_WRITE_BYTE)
        return writeBytePtr(contextPtr, address, offset, value);
    else
        return busOpNotSupported;
}

/*! Writes a packet to the bus
 *  \param address Bus address of the device targeted by the write
 *  \param offset Device specific offset to write the value to
 *  \param packet Pointer to the packet buffer to output
 *  \param packLength Length of the packet buffer
 *  \return busOpNotSupported if the controller lacks the requested capability
 */
int busProvider::writePacket(const uint64_t address, const uint32_t offset, void *packet, const uint32_t packLength) const
{
    if(capabilities & BUS_CAPS_WRITE_PACKET)
        return writePacketPtr(contextPtr, address, offset, packet, packLength);
    else
        return busOpNotSupported;
}

/*! Queues a transaction on the controller
 *  \param address Bus address of the device
 *  \param t_info Bus specific transaction information
 *  \param t_info_length Transaction information buffer length
 *  \param sync Transaction synchronization object
 *  \return busOpNotSupport if the operation is not supported by the controller
 */
int busProvider::queueTransaction(const uint64_t address, void *t_info, const uint32_t t_info_length, sync_object *sync)
{
    if(capabilities & BUS_CAPS_TRANSACTIONS)
        return queueTransactionPtr(contextPtr, address, t_info, t_info_length, sync);
    else
        return busOpNotSupported;
}

/*! Runs queued transactions for a given device
 *  \param address Bus address of the target device
 *  \param sync Synchronization object to run the completion of the transactions
 *  \return busOpNotSupported if the operation is not supported by the controller
 */
int busProvider::runTransactions(const uint64_t address, sync_object *sync)
{
    if(capabilities & BUS_CAPS_TRANSACTIONS)
        return runTransactionsPtr(contextPtr, address, sync);
    else
        return busOpNotSupported;
}

/*! Sends a configuration command to the bus controller or device
 *  \param address Bus address of the device or one that corresponds to the controller
 *  \param config_info Configuration command information
 *  \param config_size Size of the configuration command buffer
 *  \param sync Synchronization object to bind to the completion of the command
 */
int busProvider::configAddress(const uint64_t address, void *config_info, const uint32_t config_size, sync_object *sync)
{
    if(capabilities & BUS_CAPS_CONFIG_ADDR)
        return updateAddrPtr(contextPtr, address, config_info, config_size, sync);
    else
        return busOpNotSupported;
}

void *busProvider::getManagerContext(void)
{
    return managerContext;
}

