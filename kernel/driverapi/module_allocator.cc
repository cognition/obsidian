#include "module_allocator.h"
#include <memory/virtual.h>

//tierModuleAllocator class
tierModuleAllocator::tierModuleAllocator() : baseAddress(0), pagesRequested(0)
{
}

bool tierModuleAllocator::allocateRequiredSpace(void)
{
    baseAddress = virtualMemoryManager::allocateRegion(pagesRequested, PAGE_PRESENT | PAGE_WRITE );

    return true;
}

uintptr_t tierModuleAllocator::getBaseAddress(void) const
{
    return baseAddress;
}

uintptr_t tierModuleAllocator::reqPages(size_t pages)
{
    uintptr_t offset = atomicAdd(&pagesRequested, pages);
    offset *= REGULAR_PAGE_SIZE;
    return offset;
}

void tierModuleAllocator::reset(void)
{
    baseAddress = 0;
    pagesRequested = 0;
}
