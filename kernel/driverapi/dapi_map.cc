#include <templates/map.cc>

extern "C" {
    #include "dapi_include/dapi_container.h"
}

typedef Map<uint64_t, void *> dapi_map_type;

extern "C" void *dapi_map_create()
{
    dapi_map_type *nmap = new dapi_map_type();

    return nmap;
}

extern "C" void dapi_map_destroy(void *map)
{
    dapi_map_type *realmap = reinterpret_cast <dapi_map_type *> (map);
    delete realmap;
}

extern "C" void dapi_map_insert(void *map, const uint64_t key, void *value)
{
    dapi_map_type *realmap = reinterpret_cast <dapi_map_type *> (map);
    realmap->insert(key, value);
}

extern "C" const void *dapi_map_get(void *map, const uint64_t key)
{
    dapi_map_type *realmap = reinterpret_cast <dapi_map_type *> (map);
    void ** entry = realmap->get(key);
    if(!entry)
        return nullptr;
    else
        return *entry;
}

extern "C" void *dapi_map_get_mut(void *map, const uint64_t key)
{
    dapi_map_type *realmap = reinterpret_cast <dapi_map_type *> (map);
    void ** entry = realmap->get(key);
    if(!entry)
        return nullptr;
    else
        return *entry;
}
