/* DriverDevice class
 *
 * Encapsulates the resource managing Device class while managing Driver class references
 * and the C based DAPI resources of a given driver instance.
 *
 * Author: Joseph Kinzel */

#include "driverdevice.h"
#include <bus/pci.h>
#include <memory/virtual.h>
#include <introute.h>

extern "C"
{
    #include "dapi_include/driverapi.h"
}

/* Constructor for bus management drivers
 * \param drv Driver for the bus manager
 * \param n Name of the bus manager
 */
DriverDevice::DriverDevice(shared_ptr<Driver> drv, const String &fullDeviceName) : Device(fullDeviceName), handle(registerDevice(this)), busHandle(0), devDriver(drv),
    driverRes(nullptr), driverContext(nullptr), intFlags(INT_FLAGS_DELIVERY_FIXED | INT_FLAGS_DELIVERY_PHYS), devFlags(0), resourceCount(0)
    {
        addr.busId = 0;
        addr.address = 0;
    }

/*! Constructor for hardware devices
 * \param drv Pointer to the driver associated with this device
 * \param n Device name
 * \param p Parent device pointer
 * \param devAddr Bus address descriptor for the device
 */
DriverDevice::DriverDevice(shared_ptr<Driver> drv, const String &fullDeviceName, const String &refSpaceId, Device *p, const busAddressDesc &devAddr) : Device(fullDeviceName, refSpaceId, p), handle(registerDevice(this)), busHandle(0), addr(devAddr), devDriver(drv),
    driverRes(nullptr), driverContext(nullptr), intFlags(INT_FLAGS_DELIVERY_FIXED | INT_FLAGS_DELIVERY_PHYS), devFlags(0), resourceCount(0)
    {
        switch(devAddr.busId)
        {
            case BUS_ID_PCI:
                intFlags =  INT_FLAGS_POLARITY_LOW | INT_FLAGS_TRIGGER_LEVEL;
                if(int msiOffset = pciBus::getCapabilityOffset(devAddr.address, PCI_CAP_MSI) )
                    devFlags |= DRVDEV_HAS_MSI;
                else if(int msixOffset = pciBus::getCapabilityOffset(devAddr.address, PCI_CAP_MSI_X) )
                    devFlags |= DRVDEV_HAS_MSIX;


                break;
        }
    }

void DriverDevice::setBusHandle(BUS_HANDLE bh)
{
    busHandle = bh;
}

/*! Wrapper for the C based DAPI initialization function
 *  \param context Initialization information provided by a parent device or bus coordinator
 *  \return True if driver initialization proceeded as expected, false otherwise
 */
bool DriverDevice::init(void *context)
{
    Vector<systemResource> devResources;

    switch(addr.busId)
    {
        case BUS_ID_PCI:
            pciBus::getDevResources(addr.address, devResources);
            break;
        default:
            break;
    }

    if(!acquireResources(devResources))
        return false;

    driverOperationResult res = devDriver->init(handle, busHandle, &driverContext, context, &addr);
    if(res != driverOpSuccess)
    {
        kprintf("Error driver device initialization failed - code: %u\n", res);
        return false;
    }
    else
        return true;
}

/*! Allocates a contiguous region of physical memory to the device
 *  \param vBase Return reference for the virtual base address of the region
 *  \param physBase Return reference for the physical base address of the region
 *  \param contiguousSize Size of the region
 *  \param align Alignment requirement of the region
 *  \param flags Control flags
 *  \return driverOpSuccess if the operation is successful driverOpNoMemory if memory was not available
 */
int DriverDevice::allocatePhysRegion(uintptr_t &vBase, uintptr_t &physBase, size_t contiguousSize, uintptr_t align, int flags)
{
    //TODO: Actually process control flags for 32-bit memory requirements

    physBase = physicalMemory::allocateRegion(contiguousSize, align);
    if(!physBase)
        return driverOpNoMemory;
    regionObj64 physRegion(physBase, contiguousSize);

    vBase = virtualMemoryManager::mapRegion(physBase, contiguousSize, PAGE_CACHE_DISABLE | PAGE_WRITE | PAGE_PRESENT);

    physRegions.insert(vBase, physRegion);
    return driverOpSuccess;
}

/*! Frees a physical memory region previously allocated by the device
 *  \param vBase Virtual base address of the region to be freed
 *  \return driverOpSuccess if the region was freed, driverOpBadArg if the region was not recognized
 */
int DriverDevice::freePhysRegion(const uintptr_t vBase)
{
    regionObj64 phys;
    if( physRegions.retrieve(vBase, phys) )
    {
        virtualMemoryManager::unmapPhysRegion(phys.getBase(), phys.getLength());
        physicalMemory::reclaimRegion(phys.getBase(), phys.getLength() );
        return driverOpSuccess;
    }
    else
        return driverOpBadArg;
}

/*! Maps a physical address region provided by the device
 *  \param vBase Virtual base address return reference
 *  \param base Physical base address
 *  \param length Length of the region in bytes
 *  \return driverOpResources if provided physical range is not provided by the device, driverOpSuccess if the function succeeded
 */
int DriverDevice::registerPhysRegion(uintptr_t &vBase, uintptr_t base, uintptr_t length)
{
    systemResource physRequest(systemResourceMemory, base, length);
    systemResource  *found = getMatchingResource(physRequest);

    if(!found)
        return driverOpResources;
    if( !found->encases(physRequest) )
        return driverOpResources;

    vBase = virtualMemoryManager::mapRegion(base, length, PAGE_CACHE_DISABLE | PAGE_WRITE | PAGE_PRESENT);
    return driverOpSuccess;
}

int DriverDevice::deregisterPhysRegion(uintptr_t vBase)
{
    //TODO: FINISH ME
    return driverOpSuccess;
}

/*! Registers the interrupt handler for this device
 *  \param func Interrupt function handler for the device
 *  \param context Device/module provided context to be passed to the interrupt handler
 *  \return driverOpSuccess if an interrupt of IRQ handler was installed, driverOpResources if the device has no interrupt or IRQ resources
 */
int DriverDevice::registerInterruptHandler(interruptFunc func, void *context, int *interruptCount)
{
    //Attempt to find an interrupt line resource
    systemResource lineRequest(systemResourceInterrupt, 0, 255);
    systemResource *intLine = getMatchingResource(lineRequest);

    uint32_t flags = addr.busId == BUS_ID_PCI ? INT_FLAGS_TRIGGER_LEVEL:INT_FLAGS_TRIGGER_EDGE;

    int lineValue = intLine ? intLine->getBase():-1;
    if(intLine || (devFlags & (DRVDEV_HAS_MSI | DRVDEV_HAS_MSIX)) )
    {
        if(int allocatedCount = interruptManager::allocateInterrupts(addr.busId, addr.address, func, *interruptCount, lineValue, context, flags))
        {
            *interruptCount = allocatedCount;
            return driverOpSuccess;
        }
        else
        {
            *interruptCount = 0;
            return driverOpResources;
        }
    }

    //Attempt to find an IRQ resource
    systemResource irqRequest(systemResourceIrq, 0, 16);
    systemResource *devIrq = getMatchingResource(irqRequest);
    if(devIrq)
    {
        if(interruptManager::installIrqHandler(devIrq->getBase(), func, context, intFlags))
            return driverOpSuccess;
        else
            return driverOpResources;
    }

    return driverOpResources;
}

int DriverDevice::deregisterInterruptHandler()
{
    //TODO: FINISH ME
    return driverOpSuccess;
}

//! Access for the device's bus address
const busAddressDesc &DriverDevice::getAddress(void) const
{
    return addr;
}

/*! Retrieves a module compatible resource array buffer for the device
 *  \param resourceRet Return pointer for the resource array
 *  \param resourceCountRet Return pointer for the number of resource entries in the array
 *  \return driverOpSuccess as parameter checking is done in the API stubs
 */
int DriverDevice::getDeviceResources(resourceEntry **resourceRet, int *resourceCountRet)
{
    if(!driverRes)
    {
        //Collect all relevant resource entries for the device
        Vector<resourceEntry> retEntries;

        for(auto iter = resources.front(); !iter.isEnd(); iter++ )
        {
            systemResource r = *iter;
            resourceEntry outEntry;
            outEntry.base = r.getBase();
            outEntry.length = r.getLength();
            outEntry.busSpecific = 0;
            switch( r.getType() )
            {
                case systemResourceIo:
                    outEntry.busId = BUS_ID_IO;
                    retEntries.insert(outEntry);
                    break;
                case systemResourceMemory:
                    outEntry.busId = BUS_ID_MEMORY;
                    retEntries.insert(outEntry);
                    break;
                case systemResourcePci:
                    outEntry.busId = BUS_ID_PCI;
                    retEntries.insert(outEntry);
                    break;
                default:
                    break;
            }
        }

        //Create the return buffer and copy everything over
        resourceEntry *devEntries = new resourceEntry[retEntries.length()];

        for(int entry = 0; entry < retEntries.length(); entry++)
            devEntries[entry] = retEntries[entry];


        driverRes = devEntries;
        resourceCount = retEntries.length();
    }

    //Update return information
    *resourceRet = driverRes;
    *resourceCountRet = resourceCount;

    return driverOpSuccess;
}
