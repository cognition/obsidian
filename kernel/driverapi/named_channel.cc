/* Named channels implementation
 *
 *  Simple channel that can be placed within the reference space
 *
 * Author: Joseph Kinzel */


#include "named_channel.h"

/*! Constructs a named channel
 *
 *  \param refId Reference space  name
 *  \param channelType Class of messages communicated by the channel
 *  \param initSlotCount Number of message slots in the channel object
 *  \param initBufferSize Size of the channel's buffer
 */
namedChannel::namedChannel(const String &refId, const refSpaceClass channelType, const uint16_t initSlotCount, const uint16_t initBufferSize) :
    refSpaceObj(refId, channelType, rsChannel), driverComChannel(initSlotCount, initBufferSize)
    {

    }
