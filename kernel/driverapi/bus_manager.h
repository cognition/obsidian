#ifndef DRIVER_BUS_MANAGER_H_
#define DRIVER_BUS_MANAGER_H_

#include <string.h>

bool registerBusCoordinatorDriver(const uint32_t busId, const String &fileName, const String &busName);

#endif // DRIVER_BUS_MANAGER_H_
