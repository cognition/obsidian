/* Multifunction device class
 *
 * Abstracts a single device that has no direct functionality of its own but is parent to multiple functions or interfaces under it.
 *
 * Author: Joseph Kinzel */

#ifndef MF_DEVICE_H_
#define MF_DEVICE_H_

#include <device.h>

class multifunctionDevice : public Device
{
    public:
        multifunctionDevice(const String &name, const String &refSpaceId, Device *parentDevice);
        virtual bool init(void *context);
};

#endif // MF_DEVICE_H_
