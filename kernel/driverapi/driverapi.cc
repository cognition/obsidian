/* Some of the Driver C - API implementations
 * Provides interfaces between the C style driver API and C++ kernel side functions
 * Also provides functionality to resolve symbol relocations in driver modules
 *
 * Author: Joseph Kinzel */

#include <cpu/localstate.h>
#include "driverdevice.h"

extern "C"
{
    #include "dapi_include/driverapi.h"
    #include "dapi_include/sync.h"
    #include "dapi_include/dapi_container.h"
    #include "dapi_include/refspace.h"
}

bool getApiSymbolAddress(const char *string, const uint32_t maxLength, uintptr_t &value);

#define MAKE_TABLE_ENT(x)             {(reinterpret_cast<uintptr_t> (&x)), #x, (sizeof(#x)-1) }
#define REDIRECT_TABLE_ENT(x, y)      {(reinterpret_cast<uintptr_t> (&x)), #y, (sizeof(#y)-1) }

static Map<DEVICE_HANDLE, DriverDevice *> handleMap;
static ticketLock handleMapLock;


DEVICE_HANDLE DriverDevice::registerDevice(DriverDevice *dev)
{
    DEVICE_HANDLE key;
    spinLockInstance lock(&handleMapLock);
    do
    {
        key = random::get();
    } while(handleMap.get(key));
    handleMap.insert(key, dev);
    return key;
}


struct COMPILER_PACKED apiTableEntry
{
    uintptr_t address;
    const char *name;
    uint32_t length;
};

static apiTableEntry apiTable[] =
{
    //Basic memory allocation functions
    MAKE_TABLE_ENT(malloc),
    MAKE_TABLE_ENT(free),
    MAKE_TABLE_ENT(allocatePhysRegion),
    MAKE_TABLE_ENT(freePhysRegion),
    //Bus coordinator and controller functions
    MAKE_TABLE_ENT(busMsg),
    MAKE_TABLE_ENT(registerBusCoordinator),
    MAKE_TABLE_ENT(registerBusRange),
    MAKE_TABLE_ENT(coordAllocBusHandle),
    MAKE_TABLE_ENT(coordFreeBusHandle),
    MAKE_TABLE_ENT(coordRegisterMultifunction),
    //PCI bus functions
    MAKE_TABLE_ENT(pciReadByte),
    MAKE_TABLE_ENT(pciReadWord),
    MAKE_TABLE_ENT(pciReadDWord),
    MAKE_TABLE_ENT(pciReadQWord),

    MAKE_TABLE_ENT(pciWriteByte),
    MAKE_TABLE_ENT(pciWriteWord),
    MAKE_TABLE_ENT(pciWriteDWord),
    MAKE_TABLE_ENT(pciWriteQWord),

    MAKE_TABLE_ENT(pciReadBar),
    MAKE_TABLE_ENT(pciEnableBusMaster),
    //IO bus functions
    MAKE_TABLE_ENT(ioBusReadByte),
    MAKE_TABLE_ENT(ioBusReadWord),
    MAKE_TABLE_ENT(ioBusReadDWord),

    MAKE_TABLE_ENT(ioBusWriteByte),
    MAKE_TABLE_ENT(ioBusWriteWord),
    MAKE_TABLE_ENT(ioBusWriteDWord),
    //MMIO region functions
    MAKE_TABLE_ENT(registerMMIORegion),
    MAKE_TABLE_ENT(deregisterMMIORegion),

    //Interrupt handler functions
    MAKE_TABLE_ENT(registerInterruptHandler),
    MAKE_TABLE_ENT(deregisterInterruptHandler),

    MAKE_TABLE_ENT(getDeviceResources),
    MAKE_TABLE_ENT(wait),
    REDIRECT_TABLE_ENT(kprintf, printf),
    REDIRECT_TABLE_ENT(kmemset, memset),
    REDIRECT_TABLE_ENT(kmemcpy, memcpy),
    MAKE_TABLE_ENT(calloc),
    MAKE_TABLE_ENT(getPhysBase),
    // Hash table functions
    MAKE_TABLE_ENT(dapi_ht_create),
    MAKE_TABLE_ENT(dapi_ht_destroy),
    MAKE_TABLE_ENT(dapi_ht_insert),
    MAKE_TABLE_ENT(dapi_ht_get_entry),
    // Radix map functions
    MAKE_TABLE_ENT(dapi_radix_map_create),
    MAKE_TABLE_ENT(dapi_radix_map_destroy),
    MAKE_TABLE_ENT(dapi_radix_map_insert),
    MAKE_TABLE_ENT(dapi_radix_map_find),
    // Tree based map functions
    MAKE_TABLE_ENT(dapi_map_create),
    MAKE_TABLE_ENT(dapi_map_destroy),
    MAKE_TABLE_ENT(dapi_map_insert),
    MAKE_TABLE_ENT(dapi_map_get),
    MAKE_TABLE_ENT(dapi_map_get_mut),
    // Queue management functions
    MAKE_TABLE_ENT(dapi_queue_create),
    MAKE_TABLE_ENT(dapi_queue_destroy),
    MAKE_TABLE_ENT(dapi_queue_push),
    MAKE_TABLE_ENT(dapi_queue_pop),
    MAKE_TABLE_ENT(dapi_queue_peek),
    MAKE_TABLE_ENT(dapi_queue_empty),
    // Lock functions
    MAKE_TABLE_ENT(dapi_lock_create),
    MAKE_TABLE_ENT(dapi_lock_destroy),
    MAKE_TABLE_ENT(dapi_lock_acquire),
    MAKE_TABLE_ENT(dapi_lock_release),
    //Task management and sync functions
    MAKE_TABLE_ENT(dapi_spawn_task),

    MAKE_TABLE_ENT(sync_attach),
    MAKE_TABLE_ENT(sync_detach),
    MAKE_TABLE_ENT(sync_create_signal_obj),
    MAKE_TABLE_ENT(sync_create_callback_obj),
    MAKE_TABLE_ENT(sync_destroy_signal_obj),
    MAKE_TABLE_ENT(sync_object_signal),
    MAKE_TABLE_ENT(sync_object_reset),
    MAKE_TABLE_ENT(sync_object_get_status),
    MAKE_TABLE_ENT(sync_object_wait),
    MAKE_TABLE_ENT(sync_monitor_create),
    MAKE_TABLE_ENT(sync_monitor_acquire),
    MAKE_TABLE_ENT(sync_monitor_release),
    MAKE_TABLE_ENT(sync_monitor_destroy),

    //Bus operations
    MAKE_TABLE_ENT(busQueueTransaction),
    MAKE_TABLE_ENT(busRunTransactions),
    MAKE_TABLE_ENT(busConfigAddress),

    //Device enumeration and driver operations
    MAKE_TABLE_ENT(registerNewDevice),
    MAKE_TABLE_ENT(registerDriverDatabase),

    //Reference space operations
    MAKE_TABLE_ENT(refspace_channel_find),

    //Channel operations
    MAKE_TABLE_ENT(sync_channel_create),
    MAKE_TABLE_ENT(sync_channel_find),
    MAKE_TABLE_ENT(sync_channel_publish),
    MAKE_TABLE_ENT(sync_channel_subscribe),
    MAKE_TABLE_ENT(sync_channel_read)
};

static constexpr const uint32_t apiTableEntryCount = sizeof(apiTable)/sizeof(apiTableEntry);

bool getApiSymbolAddress(const char *string, const uint32_t maxLength, uintptr_t &value)
{
    int length = String::strnlen(string, maxLength);
    for(uint32_t tableIndex = 0; tableIndex < apiTableEntryCount; tableIndex++)
    {
        apiTableEntry &entry = apiTable[tableIndex];
        if(length != entry.length)
            continue;
        if(String::strncmp(string, entry.name, length) == 0 )
        {
            //kprintf("Matched target symbol \'%s\' with API symbol \'%s\'\n", string, entry.name);
            value = apiTable[tableIndex].address;
            return true;
        }
    }

    kprintf("Failed to find symbol %s\n", string);
    return false;
}

DriverDevice *DriverDevice::DriverDevice::getDriverDevFromHandle(DEVICE_HANDLE dHandle)
{
    if(DriverDevice **devPtr = handleMap.get(dHandle) )
        return *devPtr;
    else
        return nullptr;
}

/*! Allocates a block of memory for a driver
 *  \param size Size of the memory region to allocate
 *  \return A pointer to the memory region
 */
extern "C" void *malloc(size_t size)
{
    void *ptr;
    heapProcessorCache *cache = localState::getProcessorCache();
    ptr = cache->allocate(size);
    return ptr;
}

/*! Allocates and zeros a block of memory for a driver
 *  \param num_items Number of items in the block
 *  \param item_size Size of each item in the block
 *  \return A pointer to the memory region
 */
extern "C" void *calloc(size_t num_items, size_t item_size)
{
    size_t totalSize = num_items*item_size;
    heapProcessorCache *cache = localState::getProcessorCache();
    void *ptr = cache->allocate(totalSize);
    Memory::set(ptr, 0, totalSize);
    return ptr;
}

/*! Frees a previously allocated block of memory
 *  \param ptr Block to free
 */
extern "C" int free(void *ptr)
{
    heapProcessorCache *cache = localState::getProcessorCache();
    return cache->free(ptr);
}

/*! Allocates and maps a physical region of memory
 *  \param vBase Return pointer for the virtual address of the region
 *  \param physBase Return pointer for the physical base address of the region
 *  \param contiguousSize Contiguous size of the physical region
 *  \param align Alignment of the region
 *  \param flags Flags describing the attributes of the region
 *  \param dHandle Device handle of driver device
 *  \return driverOpSuccess if the allocation succeeded, or a driverOperationResult value describing the reason for failure
 */
extern "C" int allocatePhysRegion(uintptr_t *vBase, uintptr_t *physBase, size_t contiguousSize, uintptr_t align, int flags, DEVICE_HANDLE dHandle)
{
    if(DriverDevice *dev = DriverDevice::getDriverDevFromHandle(dHandle))
        return dev->allocatePhysRegion(*vBase, *physBase, contiguousSize, align, flags);
    else
        return driverOpNoAccess;

}

/*! Frees a previously allocated region of physical memory
 *  \param vBase Base address that the region is mapped to
 *  \param dHandle Device handle that allocated the region
 */
extern "C" int freePhysRegion(uintptr_t vBase, DEVICE_HANDLE dHandle)
{
    if(DriverDevice *dev = DriverDevice::getDriverDevFromHandle(dHandle))
        return dev->freePhysRegion(vBase);
    else
        return driverOpNoAccess;
}

/*! Maps a region of memory specifically for MMIO operations
 *  \param mmioBase Return pointer for the virtual address of the region
 *  \param physBase Base address of the region to map
 *  \param physLength Length of the physical memory the MMIO region is comprised of
 *  \param dHandle Device handle of the driver making the request
 */
extern "C" int registerMMIORegion(uintptr_t *mmioBase, uintptr_t physBase, size_t physLength, DEVICE_HANDLE dHandle)
{
    if(DriverDevice *dev = DriverDevice::getDriverDevFromHandle(dHandle))
        return dev->registerPhysRegion(*mmioBase, physBase, physLength);
    else
        return driverOpNoAccess;
}

/*! Unmaps a previously mapped MMIO region
 *  \param mmioBase The virtual base address of the region
 *  \param dHandle Device handle of the region owner/mapper
 */
extern "C" int deregisterMMIORegion(uintptr_t mmioBase, DEVICE_HANDLE dHandle)
{
    if(DriverDevice *dev = DriverDevice::getDriverDevFromHandle(dHandle))
        return dev->deregisterPhysRegion(mmioBase);
    else
        return driverOpNoAccess;
}

/*! Allocates a single interrupt and maps it to a handler
 *  \param func Interrupt handling function pointer
 *  \param context Device and instance specific context
 *  \param dHandle Device handle of the device with an interrupt resource
 */
extern "C" int registerInterruptHandler(interruptFunc func, void *context, int *interruptCount, DEVICE_HANDLE dHandle)
{
    if(DriverDevice *dev = DriverDevice::getDriverDevFromHandle(dHandle))
        return dev->registerInterruptHandler(func, context, interruptCount);
    else
        return driverOpNoAccess;
}

/*! Deregisters the currently mapped interrupt handler(s) for the device
 *  \param dHandle Device handle of the device who's interrupt is being handled
 */
extern "C" int deregisterInterruptHandler(DEVICE_HANDLE dHandle)
{
    if(DriverDevice *dev = DriverDevice::getDriverDevFromHandle(dHandle))
        return dev->deregisterInterruptHandler();
    else
        return driverOpNoAccess;
}

/*! Retrieves the resources of a given device
 *  \param dHandle Device handle of the device for which the resources are being retrieved
 *  \param resources Return pointer of resource array
 *  \param resourceCount Return pointer for the number of resource entries that are returned
 */
extern "C" int getDeviceResources(DEVICE_HANDLE dHandle, resourceEntry **resources, int *resourceCount)
{
    if(!resources || !resourceCount)
        return driverOpBadArg;

    if(DriverDevice *dev = DriverDevice::getDriverDevFromHandle(dHandle))
        return dev->getDeviceResources(resources, resourceCount);
    else
        return driverOpNoAccess;
}

/*! Sleeps a task for a certain number of milliseconds
 *  \param ms Number of milliseconds to sleep for
 */
extern "C" void wait(int ms)
{
    processorScheduler *sched = localState::getSched();
    sched->sleepCurrent(ms);
}

extern "C" uint64_t getPhysBase(uintptr_t addr)
{
    uint64_t phys;
    if( !virtualMemoryManager::getPhysicalAddress(addr, phys) )
    {
        kprintf("[DAPI]: Failed to resolve virtual address %p\n", addr);
        return 0;
    }
    else
        return phys;
}

