#ifndef DRIVER_CHANNELS_H_
#define DRIVER_CHANNELS_H_

#include <compiler.h>
#include <templates/vector.cc>

extern "C"
{
#include "dapi_include/sync.h"
}

class driverComChannel
{
    public:
        driverComChannel();
        driverComChannel(const uint16_t slotCount, const uint16_t bufferSize);
        ~driverComChannel();

        bool publish(uint8_t *srcBuffer, const uint16_t msgLength);
        uint32_t read(const uint16_t lastSlot, uint8_t *targetBuffer, const uint16_t bufferMax);
        void subscribe(sync_object *cb_object, const uint32_t triggerBehavior);

        int getChannelSlots(void) const
        {
            return channelSlots;
        }

        int getChannelSize(void) const
        {
            return channelSize;
        }

    private:

        void doPublishTrigger(const uint16_t slot);
        void doFullTrigger(const uint16_t slot);
        void copyBufferToRingBuffer(uint8_t *srcBuffer, const uint32_t offset, const uint32_t bufferSize);
        void copyBufferFromRingBuffer(uint8_t *targetBuffer, const uint32_t slotOffset, const uint32_t bufferMax, const uint32_t msgSize);

        uint16_t extractOffset(const uint64_t state) const
        {
            return (state>>48);
        }


        uint16_t extractBufferSpace(const uint64_t state) const
        {
            return ((state>>32) & 0xFFFFU);
        }

        uint16_t extractOldSlot(const uint64_t state) const
        {
            return ((state >> 16) & 0xFFFFU);
        }

        uint16_t extractNextSlot(const uint64_t state) const
        {
            return (state & 0xFFFFU);
        }

        uint64_t packState(const uint16_t offset, const uint16_t nextSlot, const uint16_t oldestSlot, const uint16_t bufferSpace) const
        {
            uint64_t state;

            state = offset;
            state <<= 16;
            state |= bufferSpace;
            state <<= 16;
            state |= oldestSlot;
            state <<= 16;
            state |= nextSlot;

            return state;
        }

        uint64_t createSlotEntry(uint16_t offset, const uint16_t msgLength, const uint16_t listeners) const
        {
            uint64_t entry;

            entry = 1;
            entry <<= 16;
            entry |= offset;
            entry <<= 16;
            entry |= msgLength;
            entry <<= 16;
            entry |= listeners;

            return entry;
        }

        uint64_t incStateBufferSize(const uint64_t state, const uint16_t releaseBytes) const
        {
            uint64_t freeBytes;

            freeBytes = extractBufferSpace(state);
            freeBytes += releaseBytes;
            freeBytes <<= 32;

            freeBytes |= (state & 0xFFFF0000FFFFFFFFUL);

            return freeBytes;
        }

        bool slotValid(const uint64_t slotEntry) const
        {
            return (slotEntry>> 48);
        }

        uint16_t getSlotListeners(const uint64_t slotEntry) const
        {
            return (slotEntry & 0xFFFFU);
        }

        uint16_t getSlotBytes(const uint64_t slotEntry) const
        {
            return ((slotEntry >> 16) & 0xFFFFU);
        }

        uint16_t getSlotOffset(const uint64_t slotEntry) const
        {
            return ((slotEntry >> 32) & 0xFFFFU);
        }

        Vector<sync_object *> triggerEachPublish, triggerFull;

        uint8_t *buffer;

        /* | 63 - 48       | 47 - 32                | 31 - 16            | 15 - 0               |
           | Valid         | Buffer offset          | Message size       | Remaining listeners  |
        */
        volatile uint64_t *slotInfo;

        /* | 63 - 48       | 47 - 32                | 31 - 16            |  15 - 0             |
           | Buffer offset | Buffer space available | Oldest active slot | Next available slot |
         */
        volatile uint64_t currentState;
        const uint16_t channelSlots, channelSize;
        uint16_t listenerCount;

};

#endif // DRIVER_CHANNELS_H_
