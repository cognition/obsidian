/* Named channels header
 *
 *  Simple channel that can be placed within the reference space
 *
 * Author: Joseph Kinzel */

#ifndef NAMED_CHANNEL_H_
#define NAMED_CHANNEL_H_

#include "channels.h"
#include <refspace/refspaceobj.h>

class namedChannel : public refSpaceObj, public driverComChannel
{
    public:
        namedChannel(const String &refId, const refSpaceClass channelType, const uint16_t slotCount, const uint16_t bufferSize);

};

#endif // NAMED_CHANNEL_H_
