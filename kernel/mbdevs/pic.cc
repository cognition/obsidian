#include "pic.h"
#include <bus/io.h>

#define PIC_MASTER_CMD              0x20
#define PIC_MASTER_DATA             0x21

#define PIC_SLAVE_CMD               0xA0
#define PIC_SLAVE_DATA              0xA1

#define PIC_MASTER_IRQ_OFFSET       0x30
#define PIC_SLAVE_IRQ_OFFSET        0x38

legacyDualPIC::legacyDualPIC( Device *p ) : Device( "Dual 8295A Programmable interrupt controllers", "legacy_pic", p )
{
	primaryMaskSave = ioBus::readByte( PIC_MASTER_DATA );
	secondaryMaskSave = ioBus::readByte( PIC_SLAVE_DATA );
}


bool legacyDualPIC::init( void COMPILER_UNUSED *context )
{
	if( !acquireResource( systemResource( systemResourceIo, PIC_MASTER_CMD, 2 ) ) )
		return false;
	if( !acquireResource( systemResource( systemResourceIo, PIC_SLAVE_CMD, 2 ) ) )
		return false;

	//ICW1
	ioBus::writeByte( PIC_MASTER_CMD, ICW1_INIT | ICW1_ICW4 );
	ioBus::writeByte( PIC_SLAVE_CMD, ICW1_INIT | ICW1_ICW4 );

	//ICW2
	ioBus::writeByte( PIC_MASTER_DATA, PIC_MASTER_IRQ_OFFSET );
	ioBus::writeByte( PIC_SLAVE_DATA, PIC_SLAVE_IRQ_OFFSET );

	//ICW3
	ioBus::writeByte( PIC_MASTER_DATA, 4 );
	ioBus::writeByte( PIC_SLAVE_DATA, 2 );

	//ICW4
	ioBus::writeByte( PIC_MASTER_DATA, ICW4_8086 );
	ioBus::writeByte( PIC_SLAVE_DATA, ICW4_8086 );

	//Mask all interrupts
	ioBus::writeByte( PIC_MASTER_DATA, 0xFF );
	ioBus::writeByte( PIC_SLAVE_DATA, 0xFF );

	return true;
}

