#ifndef IOAPIC_H_
#define IOAPIC_H_

#include <device.h>
#include <introute.h>

//! Object representing an IOAPIC device
class ioApic :  public Device, public interruptRouter
{
	public:
		ioApic( Device *dev, uintptr_t physBase, uint32_t intBase );
		//! Destructor
		virtual ~ioApic();
		virtual bool init( void *context );
		virtual void routeInterrupt( uint32_t line, uint32_t destination, uint8_t vector, uint32_t flags ) const;
		virtual void maskInterrupt( uint32_t line );
		virtual void unmaskInterrupt( uint32_t line );
	private:
		//! Virtual mapping of the MMIO region
		uintptr_t virtualBase;
		//! Physical base address of the MMIO region
		uintptr_t mmioPhys;
		//! Internal register select mmio regist
		volatile uint32_t *selectReg;
		//! Window/data register
		volatile uint32_t *windowReg;
};

#endif
