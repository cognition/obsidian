#ifndef MBDEV_PIC_H_
#define MBDEV_PIC_H_

#include <device.h>

#define ICW1_INIT       0x10
#define ICW1_ICW4       0x01

#define ICW4_8086       0x01

class legacyDualPIC : public Device
{
	public:
		legacyDualPIC( Device *p );
		virtual bool init( void *context );
	private:
		uint8_t primaryMaskSave, secondaryMaskSave;
};

#endif

