#include "ioapic.h"
#include <memory/memory.h>
#include <io/io.h>

//MMIO Register offsets
#define IOAPIC_SELECT_REG_OFFSET        0x00
#define IOAPIC_WINDOW_REG_OFFSET        0x10

//Register window values
#define IOAPIC_REG_ID                   0x00
#define IOAPIC_REG_VERSION              0x01
#define IOAPIC_REG_ARBITRATION          0x02

#define IOAPIC_REG_REDIRECT_BASE        0x10

#define IOAPIC_FLAGS_MASK               0x10000

ioApic::ioApic( Device *p, uintptr_t physBase, uint32_t intBase ) : Device( "IOAPIC", "ioapic", p ),
	interruptRouter( intBase, 0 ), virtualBase( 0 ), mmioPhys( physBase )
{
}

ioApic::~ioApic()
{
	if( virtualBase )
		virtualMemoryManager::unmapPhysRegion( mmioPhys, REGULAR_PAGE_SIZE );
}

bool ioApic::init( void COMPILER_UNUSED *context )
{
	if( !acquireResource( systemResource( systemResourceMemory, mmioPhys, REGULAR_PAGE_SIZE ) ) )
		return false;
	else
	{
		virtualBase = virtualMemoryManager::mapRegion( mmioPhys, REGULAR_PAGE_SIZE, PAGE_PRESENT | PAGE_CACHE_DISABLE | PAGE_WRITE );
		selectReg = reinterpret_cast <volatile uint32_t *> ( virtualBase );
		windowReg = reinterpret_cast <volatile uint32_t *> ( virtualBase + IOAPIC_WINDOW_REG_OFFSET );

		*selectReg = IOAPIC_REG_VERSION;
		uint32_t value = *windowReg;
		regLength = value;
		regLength >>= 16;
		regLength &= 0xFF;
		regLength++;

		if( !acquireResource( systemResource( systemResourceInterruptRoute, regBase, regLength ) ) )
			return false;
	}
	kprintf( "INTR: Found IOAPIC @ %X Controlling lines: %u-%u\n", mmioPhys, regBase, ( regBase+regLength-1 ) );
	return true;
}


void ioApic::routeInterrupt( uint32_t line, uint32_t destination, uint8_t vector, uint32_t flags ) const
{
    uint32_t offset = line - regBase;
	offset <<= 1;
	offset += IOAPIC_REG_REDIRECT_BASE+1;
	*selectReg = offset;

	uint32_t hiValue = ( destination << 24 );
	uint32_t loValue = vector;
	loValue |= flags;

	//Write the upper value of the redirect register
	*windowReg = hiValue;
	offset--;
	//Write the lower value of the redirect register
	*selectReg = offset;
	*windowReg = loValue;
}

void ioApic::maskInterrupt( uint32_t line )
{
	//Calculate register select value
	uint32_t offset = line - regBase;
	*selectReg = IOAPIC_REG_REDIRECT_BASE + offset*2;

	//Set the mask flag
	*windowReg |= IOAPIC_FLAGS_MASK;
}

void ioApic::unmaskInterrupt( uint32_t line )
{
	//Calculate register select value
	uint32_t offset = line - regBase;
	*selectReg = IOAPIC_REG_REDIRECT_BASE + offset*2;

	//Set the mask flag
	*windowReg &= ~IOAPIC_FLAGS_MASK;
}
