#ifndef EVENTS_H_
#define EVENTS_H_

#include "task.h"

class timedEventInstance : public Event
{
    public:
        timedEventInstance() : value(0) {};

        timedEventInstance(uint64_t v);
        timedEventInstance(timedEventInstance &other);
        timedEventInstance &operator =(timedEventInstance &t);
        ~timedEventInstance() = default;

        bool operator ==(const timedEventInstance &t) const;
        bool operator >(const timedEventInstance &t) const;
        bool operator <(const timedEventInstance &t) const;
        uint64_t getValue() const;
        void setValue(uint64_t v);
        size_t getListenCount(void) const;
        void clearEvents(void);

        class mostImminentTEI
        {
            public:
                mostImminentTEI() = default;
                bool compare(const timedEventInstance &first, const timedEventInstance &second) const
                {
                    return (first < second);
                }
        };
    private:
        volatile uint64_t value;
};

class schedulerEvent : public eventListener
{
	public:
		schedulerEvent(Task *eventOwner);
		virtual ~schedulerEvent();
		virtual bool trigger(const uint32_t generation);
};


class preemptEvent : public eventListener
{
	public:
        preemptEvent(Task *eventOwner);
        virtual ~preemptEvent();
        virtual bool trigger(const uint32_t generation);
};

#endif
