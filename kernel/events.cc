#include "events.h"
#include <cpu/localstate.h>
#include <io/io.h>

//Event class
Event::Event() : listeners()
{
}

Event::Event(Event &other) : listeners(other.listeners)
{
};

Event::~Event()
{
}

void Event::pushListener( eqEntry &&entry )
{
    listeners.add(entry);
}

bool Event::hasEvents(void) const
{
    return !listeners.empty();
}

void Event::signalListeners(void)
{
    while(!listeners.empty() )
    {
        uint32_t targetGen;
        shared_ptr<eventListener> listen = popListener(targetGen);
        listen->trigger(targetGen);
    }
}

shared_ptr<eventListener> Event::popListener(uint32_t &targetGeneration)
{
    return listeners.remove().split(targetGeneration);
}

Event &Event::operator =(Event &other)
{
    listeners = other.listeners;
    return *this;
}

Event::eqEntry::eqEntry(shared_ptr<eventListener> &&e) : listener(e), targetGeneration(e->getGeneration() )
{
    //kprintf("[TASK]: EQ entry created with listener %p target generation: %u\n", listener.get(), targetGeneration);
}


Event::eqEntry::eqEntry() : targetGeneration(0){};

Event::eqEntry::eqEntry( eqEntry &other) : listener(other.listener), targetGeneration(other.targetGeneration){}

shared_ptr<eventListener> Event::eqEntry::split(uint32_t &generationRet)
{
    generationRet = targetGeneration;
    return listener;
}

//eventListener class
eventListener::eventListener(class Task *eventOwner, const uint32_t flagsInit) : owner(eventOwner), generation(0), flags(flagsInit)
{

}

eventListener::~eventListener()
{
}

void eventListener::bumpGeneration(void)
{
    generation++;
}

uint32_t eventListener::getGeneration(void) const
{
    return generation;
}

bool eventListener::hasObtained(void)
{
    return (flags & EL_OBTAINED);
}

bool eventListener::validToServicing(void)
{
    return atomicBoolCAS(&flags, EL_VALID, EL_VALID | EL_SERVICING);
}

bool eventListener::validToObtained(void)
{
    return atomicBoolCAS(&flags, EL_VALID, EL_VALID | EL_OBTAINED);
}

bool eventListener::servicingToObtained(void)
{
    return atomicBoolCAS(&flags, EL_VALID | EL_SERVICING, EL_VALID | EL_OBTAINED);
}

bool eventListener::servicingToValid(void)
{
    return atomicBoolCAS(&flags, EL_VALID | EL_SERVICING, EL_VALID);
}

bool eventListener::matchGeneration(const uint32_t targetGeneration)
{
    uint32_t replaceValue = targetGeneration+1;
    return atomicBoolCAS(&generation, targetGeneration, replaceValue);
}

bool eventListener::invalidate(void)
{
    return atomicBoolCAS(&flags, EL_VALID, 0);
}

bool eventListener::reset(void)
{
    for(uint32_t expected = flags; !expected || (expected & EL_OBTAINED); expected = flags)
    {
        if(atomicBoolCAS(&flags, expected, EL_VALID))
            return true;
    }
    return false;
}


//timedEventInstance class
timedEventInstance::timedEventInstance(uint64_t v) : Event(), value(v)
{
}

timedEventInstance::timedEventInstance(timedEventInstance &other) : Event(other), value(other.value) {};


timedEventInstance &timedEventInstance::operator =(timedEventInstance &t)
{
    listeners = t.listeners;
    value = t.value;
    return *this;
}

bool timedEventInstance::operator ==(const timedEventInstance &t) const
{
    return (value == t.value);
}

bool timedEventInstance::operator >(const timedEventInstance &t) const
{
    return (value > t.value);
}

bool timedEventInstance::operator <(const timedEventInstance &t) const
{
	return (value < t.value);
}

uint64_t timedEventInstance::getValue() const
{
    return value;
}

void timedEventInstance::setValue(uint64_t v)
{
    value = v;
}

size_t timedEventInstance::getListenCount(void) const
{
    return listeners.size();
}

void timedEventInstance::clearEvents(void)
{
    listeners.discardAll();
}

//schedulerEvent class

/*! Constructor for a scheduler event
 *  \param eventOwner Task which owns this event
 *  \param i Event to bind this event to
 */
schedulerEvent::schedulerEvent(Task *eventOwner) : eventListener(eventOwner, eventListener::EL_VALID)
{
}

schedulerEvent::~schedulerEvent()
{

}

//! Tells the processor scheduler to run it's current scheduler event
bool schedulerEvent::trigger(const uint32_t targetGeneration)
{
    processorScheduler *sched = localState::getSched();
    if(targetGeneration == generation)
    {
        bumpGeneration();
        if(sched->validateSchedEventCaller(owner))
            sched->invokeCurrentSchedulerEvent(COMPILER_FUNC_CALLER);
        return true;
    }
    else
    {
        //kprintf("Scheduler event generation mismatch found: %u expected: %u\n", targetGeneration, generation);
        return false;
    }
}

//preemptEvent class

/*! Constructor for a preemption Event
 *  \param eventOwner Task which owns this event
 *  \param i Event object to bind this event to
 */
preemptEvent::preemptEvent(Task *eventOwner) : eventListener(eventOwner, eventListener::EL_VALID)
{
}

preemptEvent::~preemptEvent()
{
}

//! Communicates with the processor scheduler and attempts to preempt the current task
bool preemptEvent::trigger(const uint32_t generation)
{
    processorScheduler *sched = localState::getSched();
    sched->attemptTaskPreempt(owner);
    return true;
}

//! waitListener class
waitListener::waitListener(Task *eventOwner) : eventListener(eventOwner, EL_VALID)
{
}

waitListener::~waitListener()
{
}

bool waitListener::trigger(const uint32_t expectedGeneration)
{
    return owner->triggerWaitListener(this, expectedGeneration);
}
