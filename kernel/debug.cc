#include "debug.h"
#include <memory/virtual.h>
#include <addrspace/kern_map.h>

#define SERIAL_DATA_OFF           0
#define SERIAL_INT_OFF            1
#define SERIAL_FIFO_OFF           2
#define SERIAL_LINE_CONTROL_OFF   3
#define SERIAL_LINE_STATUS_OFF    5

#define SERIAL_DLAB               0x80
#define SERIAL_LS_THRE            0x20

extern "C" void io_debug_print(const char *format, ...);

struct watchpointSave
{
    uint64_t ip;
    uint64_t value;
    uint64_t prior;
};

static watchpointSave watchPointSaves[4][8];
static int wpTriggerIndices[8];

/*
static void printStack( uintptr_t rsp, uint32_t count )
{
    uint64_t *stack = reinterpret_cast <uint64_t *> ( rsp );
    while( count )
    {
        kprintf( "Stack [%x] - %x\n", stack, *stack );
        stack++;
        count--;
    }
}
*/

/*
static void printRegs( const savedRegs *regs )
{
    kprintf( "RAX=%016X RBX=%016X RCX=%016X\n", regs->rax, regs->rbx, regs->rcx );
    kprintf( "RDX=%016X RDI=%016X RSI=%016X\n", regs->rdx, regs->rdi, regs->rsi );
    kprintf( "RBP=%016X  R8=%016X  R9=%016X\n", regs->rbp, regs->r8, regs->r9 );
    kprintf( "R10=%016X R11=%016X R12=%016X\n", regs->r10, regs->r11, regs->r12 );
    kprintf( "R13=%016X R14=%016X R15=%016X\n", regs->r13, regs->r14, regs->r15 );
}
*/

union watchPoint
{
    uint8_t watch8;
    uint16_t watch16;
    uint32_t watch32;
    uint64_t watch64;
};

uint8_t inline inpb(uint16_t port)
{
	uint8_t ret;
	asm volatile ("inb %%dx, %%al\n": "=a" (ret) : "d" (port) );
	return ret;
}

void inline outpb(uint16_t port, uint8_t value)
{
	asm volatile ("outb %%al, %%dx\n":: "a" (value), "d" (port) );
}

serialLine::serialLine(uint16_t port) : textOutputDevice(-1, 0), ioBase(port)
{
	outpb(port+SERIAL_INT_OFF, 0);
	outpb(port+SERIAL_LINE_CONTROL_OFF, SERIAL_DLAB);
	outpb(port+0, 2);
	outpb(port+1, 0);
	outpb(port+SERIAL_LINE_CONTROL_OFF, 0x3);
}

void serialLine::putChar(uint8_t character)
{
	while ((inpb(ioBase + SERIAL_LINE_STATUS_OFF) & SERIAL_LS_THRE) == 0)
	{
		asm volatile ("pause\n"::);
	}
	outpb(ioBase+SERIAL_DATA_OFF, character);
}

ioDevResult serialLine::writeBuffer( const void *buffer, uintptr_t length )
{
	const uint8_t *data = reinterpret_cast <const uint8_t *> (buffer);
	for(uintptr_t i = 0; i < length; i++)
		putChar(data[i]);
	return ioSuccess;
}

ioDevResult serialLine::echoChar(const char value)
{
    putChar(value);
    return ioSuccess;
}

ioDevResult serialLine::preformOp( streamOp o )
{
	switch(o)
	{
		case streamOpNewLine:
			putChar(0xA);
			break;
		case streamOpTab:
			putChar(0x9);
			break;
		default:
				return ioFeatureError;
			break;
	}
	return ioSuccess;
}

void serialLine::getDims(int &rows, int &cols)
{
    rows = 120;
    cols = 50;
}

void disableBreakpoint(const uint8_t index)
{
    uint64_t debugControl;
    uint32_t enableMask = 0x2U;
    asm volatile ("movq %%dr7, %0\n": "=r" (debugControl):);

    enableMask <<= index*2;
    debugControl &= ~enableMask;

    asm volatile ("movq %0, %%dr7\n":: "r" (debugControl):);
}

static uint8_t watchIndexRotor = 0;

void disableWatch(uintptr_t value, const uint32_t adjust)
{
    uint64_t bp0, bp1, bp2, bp3;
    uint64_t settingsReg, bpAddr;

    uint32_t enableMask = 0x2U;
    bpAddr = value - adjust;

    asm volatile ("movq %%dr7, %0\n": "=r" (settingsReg):);

    if(settingsReg & enableMask)
    {
        asm volatile ("mov %%dr0, %0\n": "=r" (bp0): );
        if(bp0 == bpAddr)
            settingsReg &= ~enableMask;
    }

    enableMask <<= 2;
    if(settingsReg & enableMask)
    {
        asm volatile ("mov %%dr1, %0\n": "=r" (bp1): );
        if(bp1 == bpAddr)
            settingsReg &= ~enableMask;

    }

    enableMask <<= 2;
    if(settingsReg & enableMask)
    {
        asm volatile ("mov %%dr2, %0\n": "=r" (bp2): );
        if(bp2 == bpAddr)
            settingsReg &= ~enableMask;
    }

    enableMask <<= 2;
    if(settingsReg & enableMask)
    {
        asm volatile ("mov %%dr3, %0\n": "=r" (bp3): );
        if(bp3 == bpAddr)
            settingsReg &= ~enableMask;
    }

        asm volatile ("movq %0, %%dr7\n":: "r" (settingsReg) );
}

void setWatch(uintptr_t value, const uint32_t adjust)
{
    const int wIndex = watchIndexRotor++;
    watchIndexRotor &= 0x03U;

    setBreakpoint(wIndex, reinterpret_cast <void *> (value - adjust), BP_TYPE_DATA_WRITE, BP_LENGTH_QWORD);
}

void setBreakpoint(const uint8_t index, void *value, uint32_t type, uint32_t length)
{
    uint32_t valueMask = 0x000F0000U;
    uint32_t enableMask = 0x2U;

    uint64_t settingsReg;
    asm volatile ("movq %%dr7, %0\n": "=r" (settingsReg):);

    //Memory::set(watchPointSaves[index], 0, sizeof(watchpointSave)*8);
    wpTriggerIndices[index] = 0;

    switch(index)
    {
    case 0:
        settingsReg &= ~valueMask;
        settingsReg |= enableMask | (type<<16) | (length<<18);
        asm volatile ("movq %0, %%dr0\n"
                      "movq %1, %%dr7\n":: "r" (value), "r" (settingsReg));

        break;
    case 1:
        valueMask <<= 4;
        enableMask <<= 2;
        settingsReg &= ~valueMask;
        settingsReg |= enableMask | (type<<20) | (length<<22);
        asm volatile ("movq %0, %%dr1\n"
                      "movq %1, %%dr7\n":: "r" (value), "r" (settingsReg));
        break;
    case 2:
        valueMask <<= 8;
        enableMask <<= 4;
        settingsReg &= ~valueMask;
        settingsReg |= enableMask | (type<<24) | (length<<26);
        asm volatile ("movq %0, %%dr2\n"
                      "movq %1, %%dr7\n":: "r" (value), "r" (settingsReg));
        break;
    case 3:
        valueMask <<= 12;
        enableMask <<= 6;
        settingsReg &= ~valueMask;
        settingsReg |= enableMask | (type<<28) | (length<<30);
        asm volatile ("movq %0, %%dr3\n"
                      "movq %1, %%dr7\n":: "r" (value), "r" (settingsReg));

        break;
    default:
        kprintf("Error invalid breakpoint enable call with index: %u!\n", index);
        break;
    }
}

void examineBp(const uint8_t index, savedRegs *regs)
{
    uint64_t regValue, mask, config;
    asm ("movq %%dr7, %0\n": "=r" (config):);

    switch(index)
    {
    case 0:
        mask = config >> 16;
        asm volatile ("movq %%dr0, %0\n": "=r" (regValue):);
        break;
    case 1:
        mask = config >> 20;
        asm volatile ("movq %%dr1, %0\n": "=r" (regValue):);

        break;
    case 2:
        asm volatile ("movq %%dr2, %0\n": "=r" (regValue):);
        mask = config >> 24;
        break;
    case 3:
        asm volatile ("movq %%dr3, %0\n": "=r" (regValue):);
        mask = config >> 28;
        break;
    default:
        return;
        break;
    }

    uint32_t breakType = mask & 0x3U;
    uint32_t length = (mask >> 2) & 0x3U;

    volatile watchPoint *watchPtr = reinterpret_cast <volatile watchPoint *> (regValue);

    watchpointSave *savePtr = &watchPointSaves[index][wpTriggerIndices[index]++];
    wpTriggerIndices[index] &= 0x7U;
    savePtr->ip = regs->rip;
    savePtr->prior = savePtr->value;

    uint64_t rbase, roffset;
    String regionName = kernelSpace::decodeAddress(regs->rip, rbase, roffset);

    switch(breakType)
    {
        case BP_TYPE_DATA_WRITE:
            io_debug_print("[DBG ]: Write watchpoint(%X) encountered @ Code [%s]+%X Base: %p \n", regValue, regionName.toBuffer(), roffset, rbase );
            switch(length)
            {
                case 0:
                    savePtr->value = watchPtr->watch8;
                    break;
                case 1:
                    savePtr->value = watchPtr->watch16;
                    break;
                case 2:
                    //io_debug_print("New value[64]: %X\n", watchPtr->watch64);
                    savePtr->value = watchPtr->watch64;
                    break;
                case 3:
                    //io_debug_print("New value[32]: %X\n", watchPtr->watch32);
                    savePtr->value = watchPtr->watch32;
                    break;
                default:
                    return;
                    break;
            }
            break;
        default:
            return;
            break;
    }
}

void printWatchSaves()
{
    uint64_t enableMask = 0x2;
    uint64_t debugControl, wp0, wp1, wp2, wp3;
    asm volatile ("mov %%dr7, %0\n": "=r" (debugControl):);
    asm volatile ("mov %%dr0, %0\n": "=r" (wp0): );
    asm volatile ("mov %%dr1, %0\n": "=r" (wp1): );
    asm volatile ("mov %%dr2, %0\n": "=r" (wp2): );
    asm volatile ("mov %%dr3, %0\n": "=r" (wp3): );

    kprintf("Watchpoints - [0]:%016X [1]:%016X [2]:%016X [3]:%016X\n", wp0, wp1, wp2, wp3);

    for(int i = 0; i < 4; i++, enableMask <<= 2)
    {
        if(debugControl & enableMask)
        {
            for(int j = 0; j < 8; j++)
            {
                uintptr_t rbase, roffset;

                int index = (j + wpTriggerIndices[i]) & 0x7F;

                String regionName = kernelSpace::decodeAddress(watchPointSaves[i][index].ip, rbase, roffset);

                kprintf("BP[%u][%u]: Code [%s]+%X Base: %p Prior value: %X Current value: %X\n", i, j, regionName.toBuffer(), roffset, rbase, watchPointSaves[i][index].prior, watchPointSaves[i][index].value);
            }
        }
    }
}
