#include "regionobj.h"

//regionObj64 class
regionObj64::regionObj64()
{
}

regionObj64::regionObj64( uint64_t base, uint64_t length ) : regBase( base ), regLength( length )
{
}

bool regionObj64::operator ==( const regionObj64 &region ) const
{
	const uint64_t selfEnd = regBase + regLength;
	const uint64_t regEnd = region.regBase + region.regLength;
	if( regBase >= region.regBase )
    {
		if(regEnd >= selfEnd)
            return true;
        else
            return (regBase < regEnd);
    }
	else
    {
        if(selfEnd > region.regBase)
            return true;
        else
            return (selfEnd >= regEnd);
    }
}

bool regionObj64::operator ==( const uint64_t value ) const
{
	const uint64_t selfEnd = regBase + regLength;
	if( value >= regBase )
		return ( value < selfEnd );
	else
		return false;
}

bool regionObj64::operator >( const regionObj64 &region ) const
{
	const uint64_t regEnd = region.regBase + region.regLength;
	return ( regBase >= regEnd );
}

bool regionObj64::operator >( const uint64_t value ) const
{
	return ( regBase > value );
}

bool regionObj64::operator <( const regionObj64 &region ) const
{
	return ( (regBase+regLength) <= region.regBase );
}

bool regionObj64::operator <( const uint64_t value ) const
{
	return ( (regBase+regLength) <= value );
}

bool regionObj64::encases(const regionObj64 &other) const
{
    return ((regBase <= other.regBase) && ((regBase+regLength) >= (other.regBase + other.regLength)));
}


regionObj64 &regionObj64::operator =(const regionObj64 &other)
{
    regBase = other.regBase;
    regLength = other.regLength;
    return * this;
}


uint64_t regionObj64::getBase() const
{
    return regBase;
}

uint64_t regionObj64::getLength() const
{
    return regLength;
}

uint64_t regionObj64::limit(void) const
{
    return (regBase + regLength);
}

void regionObj64::decrementLength(const uint64_t amount)
{
    regLength -= amount;
}

void regionObj64::augmentBase(const uint64_t amount)
{
    regBase += amount;
}

void regionObj64::setBase(const uint64_t newBase)
{
    regBase = newBase;
}

void regionObj64::setLength(const uint64_t newLength)
{
    regLength = newLength;
}

//regionObj32 class
regionObj32::regionObj32()
{
}

regionObj32::regionObj32( uint32_t base, uint32_t length ) : regBase( base ), regLength( length )
{
}

bool regionObj32::operator ==( const regionObj32 &region ) const
{
	uint32_t selfEnd = regBase + regLength;
	uint32_t regEnd = region.regBase + region.regLength;
	if( regBase >= region.regBase )
    {
		if(regEnd >= selfEnd)
            return true;
        else
            return (regBase < regEnd);
    }
	else
    {
        if(selfEnd > region.regBase)
            return true;
        else
            return (selfEnd >= regEnd);
    }
}

bool regionObj32::operator >( const regionObj32 &region ) const
{
	uint32_t regEnd = region.regBase + region.regLength;
	return ( regBase >= regEnd );
}

bool regionObj32::operator <( const regionObj32 &region ) const
{
	uint32_t regEnd = regBase + regLength;
	return ( region.regBase >= regEnd );
}

bool regionObj32::operator <(const uint32_t value ) const
{
    const uint32_t regEnd = regBase + regLength;
    return (value >= regEnd);
}


bool regionObj32::operator ==( const uint32_t value ) const
{
	uint32_t selfEnd = regBase + regLength;
	if( value >= regBase )
		return (value < selfEnd );
	else
		return false;
}

bool regionObj32::operator >( const uint32_t value ) const
{
	return ( regBase > value );
}


uint32_t regionObj32::getBase(void) const
{
    return regBase;
}

uint32_t regionObj32::getLength(void) const
{
    return regLength;
}

void regionObj32::setBase(const uint32_t base)
{
    regBase = base;
}

void regionObj32::setLength(const uint32_t length)
{
    regLength = length;
}
