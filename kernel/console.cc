#include "console.h"
#include <memory/memory.h>

#define CONSOLE_LINE_WIDTH_BYTES    160
#define CONSOLE_BUFFER_SIZE         ((CONSOLE_LINE_WIDTH_BYTES)*25)

#define TEXT_BUFFER_PHYS            0xB8000UL

#define VGA_CONSOLE_ROWS            25
#define VGA_CONSOLE_COLS            80

uint16_t bootConsole::memBuffer[CONSOLE_BUFFER_SIZE];


//! Constructor for the bootConsole class
bootConsole::bootConsole() : textOutputDevice( CONSOLE_BUFFER_SIZE, 0 ), hwBuffer( reinterpret_cast <uint16_t *> ( TEXT_BUFFER_PHYS ) ), colorMask( 0x0700 ), active(false)
{
}

void bootConsole::init()
{
    active = true;
	Memory::set( memBuffer, 0, CONSOLE_BUFFER_SIZE);
	Memory::set( hwBuffer, 0, CONSOLE_BUFFER_SIZE );
}

//! Remaps the text mode buffer to a new location in virtual space
void bootConsole::relocate()
{
	if(active)
        hwBuffer = reinterpret_cast <uint16_t *> ( virtualMemoryManager::mapRegion( TEXT_BUFFER_PHYS, 0x8000, PAGE_WRITE | PAGE_PRESENT | PAGE_CACHE_DISABLE ) );
}

/*! \brief Outputs characters to the console
 *  \param buffer Source buffer to output
 *  \param length Length of buffer to be output
 *  \return Returns a value of ioOutOfRage if the source buffer is too large, and ioSuccess otherwise.
 */
ioDevResult bootConsole::writeBuffer( const void *buffer, uintptr_t length )
{
	uintptr_t bufferBytes = length*2;
	uintptr_t potentialOffset = currentOffset + bufferBytes;

	//Check if the text will go off screen
	if( potentialOffset >= maxLength )
	{
		potentialOffset -= maxLength;
		uint8_t lineCount = potentialOffset/CONSOLE_LINE_WIDTH_BYTES;
		if( potentialOffset%CONSOLE_LINE_WIDTH_BYTES )
			lineCount++;
		shiftBuffer( lineCount );
	}

	//Output the text to the screen
	const uint8_t *readBuffer = reinterpret_cast <const uint8_t *> ( buffer );
	uint16_t *mBuffPtr = &memBuffer[currentOffset/2];
	uint16_t *bufferPtr = &hwBuffer[currentOffset/2];

	currentOffset += bufferBytes;
	while( length )
	{
		uint16_t value = colorMask | *readBuffer;
		*bufferPtr = value;
		*mBuffPtr = value;
		readBuffer++;
		bufferPtr++;
		mBuffPtr++;
		length--;
	}

	return ioSuccess;
}

/*! \brief Preforms a specified stream operation, if applicable
 *  \param o Operation to be preformed
 *  \return Returns a value of ioSuccess if the operation was applicable or a value of ioFeatureError otherwise
 */
ioDevResult bootConsole::preformOp( const streamOp o )
{
	switch( o )
	{
		case streamOpSetColor:
			break;
		case streamOpCarriageReturn:
			currentOffset -= currentOffset%CONSOLE_LINE_WIDTH_BYTES;
			break;
        case streamOpBackspace:
            if(currentOffset)
            {
                currentOffset -= 2;
                memBuffer[currentOffset/2] = colorMask;
                hwBuffer[currentOffset/2] = colorMask;
            }
            break;
		case streamOpTab:
			currentOffset += 8;
			currentOffset &= ~0x7;
			break;
		case streamOpNewLine:
			currentOffset += CONSOLE_LINE_WIDTH_BYTES;
			currentOffset -= currentOffset%CONSOLE_LINE_WIDTH_BYTES;
			if(currentOffset >= (CONSOLE_BUFFER_SIZE))
                shiftBuffer(1);
			break;
		default:
			return ioFeatureError;
			break;
	}
	return ioSuccess;
}


static uint8_t scrollVal = 20;

/*! \brief Shifts the current text buffer up a line
 *  \param lines Number of lines to shift upwards
 */
void bootConsole::shiftBuffer( uint32_t lines )
{
	scrollVal += lines;
	if( scrollVal >= 24 )
	{
	    //asm ("xchg %%bx, %%bx\n"::);
		scrollVal = lines;
	}

	uintptr_t soffset = lines*CONSOLE_LINE_WIDTH_BYTES;
	currentOffset -= soffset;
	Memory::copy( memBuffer, &memBuffer[soffset/2], currentOffset);
	Memory::set( &memBuffer[( currentOffset/2 )], 0, soffset );
	Memory::copy( hwBuffer, memBuffer, maxLength);

}

ioDevResult bootConsole::echoChar(const char value)
{
    return writeBuffer(&value, 1);
}

void bootConsole::getDims(int &rows, int &cols)
{
    rows = VGA_CONSOLE_ROWS;
    cols = VGA_CONSOLE_COLS;
}
