#ifndef INTERRUPT_ROUTE_H_
#define INTERRUPT_ROUTE_H_

#include <device.h>
#include "regionobj.h"
#include <cpu/softint.h>

//Interrupt control flags
#define INT_FLAGS_POLARITY_HIGH                 0
#define INT_FLAGS_POLARITY_LOW                  0x2000

#define INT_FLAGS_DELIVERY_PHYS                 0
#define INT_FLAGS_DELIVERY_LOGICAL              0x1000

#define INT_FLAGS_TRIGGER_EDGE                  0x0
#define INT_FLAGS_TRIGGER_LEVEL                 0x8000

//Interrupt delivery flags
#define INT_FLAGS_DELIVERY_FIXED                0
#define INT_FLAGS_DELIVERY_LOWEST_PRIO          0x100
#define INT_FLAGS_DELIVERY_SMI                  0x200
#define INT_FLAGS_DELIVERY_NMI                  0x400
#define INT_FLAGS_DELIVERY_INIT                 0x500
#define INT_FLAGS_DELIVERY_EXTINT               0x700

//! Base class for a device which routes interrupts
class interruptRouter : public regionObj32
{
	public:
		interruptRouter( uint32_t baseInt, uint32_t count );
		virtual ~interruptRouter() = default;
		virtual void routeInterrupt( uint32_t line, uint32_t destination, uint8_t vector, uint32_t flags ) const = 0;
		virtual void maskInterrupt( uint32_t line ) = 0;
		virtual void unmaskInterrupt( uint32_t line ) = 0;
};

//! Dummy class for interrupt routing
class emptyRouter : public interruptRouter
{
	public:
		emptyRouter();
		virtual ~emptyRouter() = default;
		virtual void routeInterrupt( uint32_t line, uint32_t destination, uint8_t vector, uint32_t flags ) const;
		virtual void maskInterrupt( uint32_t line );
		virtual void unmaskInterrupt( uint32_t line );
};

//! Object representing an IRQ to global interrupt line mapping
class irqRedirectionEntry
{
	public:
		irqRedirectionEntry( const uint8_t irqNum, const uint32_t intLine, const uint32_t intFlags );
		bool operator ==( const irqRedirectionEntry &e ) const;
		bool operator ==( const uint8_t val ) const;
		bool operator >( const irqRedirectionEntry &e ) const;
		bool operator >( const uint8_t val ) const;
		bool operator <( const irqRedirectionEntry &e ) const;
		bool operator <( const uint8_t val ) const;
		//! Global interrupt line number that the IRQ translates to
		const uint32_t line;
		//! MPS Flags
		const uint32_t flags;
		//! IRQ Number which will be redirected
		const uint8_t irq;
};

//! Global interrupt management namespace
namespace interruptManager
{
    bool installIoApic( Device *parent, uint32_t phys, uint32_t globalBase );
    bool installDualPic( Device *parent );
    bool installInterruptHandler( uint32_t line, uint8_t priority, softInterrupt::intHandler handler, void *context, uint32_t flags );
    int installInterruptHandler( uint32_t line, softInterrupt::intHandler handler, void *context, uint32_t flags );
    int installIrqHandler( const uint8_t irq, softInterrupt::intHandler handler, void *context, const uint32_t flags );
    void registerIrqRedirect( uint8_t irq, uint32_t redirLine, uint32_t flags );
    int installPciMsiHandler( const uint32_t address, const uint32_t msiOffset, const int interruptCount, softInterrupt::intHandler handler, void *context);
    int allocateInterrupts(const int busId, const uint64_t address, softInterrupt::intHandler handler, const uint32_t requested_count, const int physicalLine, void *context, const uint32_t flags);
}


#endif
