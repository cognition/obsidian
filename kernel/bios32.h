#ifndef BIOS32_H_
#define BIOS32_H_

#include <compiler.h>

union signature4Byte
{
    char bytes[4];
    uint32_t value;
};

struct COMPILER_PACKED regs32
{
    uint16_t ds, es;
    uint32_t eax, ebx, ecx, edx, edi, esi, eflags;
};

struct COMPILER_PACKED farCall32
{
    uint32_t eip;
    uint16_t cs, _reserved;
    regs32 regs;
};

extern "C" void callFar32(farCall32 *callInfo);

class bios32service
{
    public:
        bios32service(uint32_t baseAddress, uint32_t serviceLength, uint32_t serviceEntry);
        void execute(regs32 *callInfo);
    private:
        uint16_t cs, ds;
        uint32_t entry;
};

namespace bios32
{
    bool init(void);
    bios32service *requestService(uint32_t id);
}

#endif
