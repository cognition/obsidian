#ifndef TIMER_H_
#define TIMER_H_

class Timer
{
	public:
		Timer();
		//Reads the processor's timestamp counter
		static inline uint64_t readTSC()
		{
			uint64_t ret;
			asm volatile (   "mfence\n"
			        "xor %%rax, %%rax\n"
			        "rdtsc\n"
			        "shl $32, %%rdx\n"
			        "or %%rax, %%rdx\n": "=d" ( ret ) :: "eax" );
			return ret;
		}
};

#endif
