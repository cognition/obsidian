#ifndef REGION_OBJ_H_
#define REGION_OBJ_H_

#include <compiler.h>

class regionObj64
{
	public:
		regionObj64();
		regionObj64( uint64_t base, uint64_t length );
		bool operator ==( const regionObj64 &region ) const;
		bool operator ==( const uint64_t value ) const;
		bool operator >( const regionObj64 &region ) const;
		bool operator >( const uint64_t value ) const;
		bool operator <( const regionObj64 &region ) const;
		bool operator <( const uint64_t value ) const;
		bool encases(const regionObj64 &other) const;
		regionObj64 &operator =(const regionObj64 &other);

		void augmentBase(const uint64_t amount);
		void decrementLength(const uint64_t amount);

		uint64_t getBase() const;
        uint64_t getLength() const;
        uint64_t limit(void) const;

        void setBase(const uint64_t newBase);
        void setLength(const uint64_t newLength);
	protected:
		//! Base address of the region
		uint64_t regBase;
		//! Length of the region
		uint64_t regLength;
};

class regionObj32
{
	public:
		regionObj32();
		regionObj32( uint32_t base, uint32_t length );
		bool operator ==( const regionObj32 &region ) const;
		bool operator ==( const uint32_t value ) const;
		bool operator >( const regionObj32 &region ) const;
		bool operator >( const uint32_t value ) const;
        bool operator <( const regionObj32 &region ) const;
        bool operator <(const uint32_t value ) const;
		uint32_t getBase(void) const;
		uint32_t getLength(void) const;

		inline void augmentLength(const uint32_t delta)
		{
		    regBase +=  delta;
		}

		void setBase(const uint32_t base);
		void setLength(const uint32_t length);
	protected:
		//! Base address of the region
		uint32_t regBase;
		//! Length of the region
		uint32_t regLength;
};


#endif
