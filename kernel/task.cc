/* Task class implementation - Manages task creation, tear down and processor state preservation and restoration.
 *
 * Author: Joseph Kinzel */

#include <cpu/localstate.h>
#include <cpu/features.h>
#include <memory/memory.h>
#include <io/io.h>
#include "task.h"
#include <debug.h>

#define MAX_FDS             1024

#define DEFAULT_TASK_PRIORITY    50

static const uint32_t initMxcsr = 0xF80;
static ticketLock pidLock;
extern "C" void *bspStack, *bspStackEnd;

static void kernJobTask( void (*p)(void *), void *context )
{
	p( context );
	localState::getSched()->exitCurrentTask();
}

static Map<regionObj64, Task *> kStackMap;

//! Empty constructor
Task::Task() : kernelStack( NULL ), flags(0), regs( nullptr ), vectorState(NULL), pml4( 0 ), fdBank(0, 0), destroyed(false)
{
}

//! Running task constructor
Task::Task(const uint64_t kStackStart, const uint64_t kStackEnd, const uint32_t prio, const pid_t tPid) : next(nullptr), prev(nullptr), taskWaitListener(new waitListener(this)),
    kernelStackRange( kStackStart, kStackEnd - kStackStart ),
                     kernelStack(kStackStart), priority(prio), flags(0), regs(nullptr), vectorState(NULL), pid(tPid), fdBank(0,0), destroyed(false)
{
    Task *self = this;
    kStackMap.insert(kernelStackRange, self);
    asm ("mov %%cr3, %0\n": "=r" (pml4):);
    addrSpace.reset(new addressSpace(pml4));
}


//! Kernel mode task constructor
Task::Task( void *ip, const pid_t tPid ) :
    // Link and list pointers
    next(NULL), prev(NULL), taskWaitListener(new waitListener(this)),
    kernelStackRange( virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ) ),
    // Kernel mode stack
    kernelStack( kernelStackRange.limit() - 8),
    //Task priority
    priority(DEFAULT_TASK_PRIORITY),
    // Task information flags
    flags(0),
    // Saved register pointer
	regs( reinterpret_cast <savedRegs *> ( kernelStack - ( sizeof( savedRegs ) + 8 ) ) ),
	//Vector processing state
	vectorState(NULL),
	// PML4 pointer
	pml4( virtualMemoryManager::getKernelPrimaryVirtualStructure() ),
	// Scheduler
	sched(NULL),
	//PID
	pid(tPid),
	fdBank(0, 0),
    destroyed( false )
{
    Task *self = this;
    kStackMap.insert(kernelStackRange, self);
    Memory::set( const_cast <savedRegs *> (regs), 0, sizeof(savedRegs) );
	//Setup data segments
	regs->ds = KERNEL_DATA_SEG;
	regs->es = KERNEL_DATA_SEG;
	regs->fs = KERNEL_DATA_SEG;
	regs->gs = KERNEL_DATA_SEG;

	//Setup code segment and pointer
	regs->cs = KERNEL_CODE_64;
	regs->rip = reinterpret_cast <uintptr_t> ( ip );

	//Setup stack
	regs->ss = KERNEL_DATA_SEG;
	regs->rsp = kernelStack;

	//Setup EFLAGS
	regs->rflags = X86_FLAGS_BASE | X86_FLAGS_INTERRUPT_ENABLE;
}


/*! Kernel side constructor for a task that will execute a single function
 *  \param ip Pointer to the function which will be executed
 *  \param context Argument to pass to the function call
 *  \param prio Function priority
 */
Task::Task( void ( *ip )( void * ), void *context, uint32_t prio, const pid_t tPid, bool isUser ) :
    // Link and list pointers
    next(NULL), prev(NULL), taskWaitListener(new waitListener(this)),
	kernelStackRange(virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES )),
	// Kernel mode stack
	kernelStack( kernelStackRange.limit() - 8 ),
	// TLS
	userTLS(0),
	// Priority
	priority( prio ),
	// Flags
	flags( isUser ? TASK_FLAG_SAVE_USER:0),
	// Saved registers
	regs( reinterpret_cast <savedRegs *> ( kernelStack - (sizeof( savedRegs ) + 8) ) ),
	// Vector register state
	vectorState(NULL),
	// Virtual address table for the device
	pml4( isUser ? virtualMemoryManager::cloneKPVS():virtualMemoryManager::getKernelPrimaryVirtualStructure() ),
	// Address space
	addrSpace(new addressSpace(pml4) ),
	// Scheduler
	sched(NULL),
	// PID
	pid(tPid),
	// File descriptor bank
	fdBank(3, MAX_FDS - 3),
	// Destroyed flag, for cleanup
	destroyed( false )
{
    Task *self = this;
    kStackMap.insert(kernelStackRange, self);
    //Setup data segments
	regs->ds = KERNEL_DATA_SEG;
	regs->es = KERNEL_DATA_SEG;
	regs->fs = KERNEL_DATA_SEG;
	regs->gs = KERNEL_DATA_SEG;

	//Setup code segment
	regs->cs = KERNEL_CODE_64;
	//Entry point is the single function call task logic
	regs->rip = reinterpret_cast <uintptr_t> ( &kernJobTask );

    //First parameter is stored in RDI according to the x86-64 ABI
	regs->rdi = reinterpret_cast <uintptr_t> ( ip );
	//Second is stored in RSI
	regs->rsi = reinterpret_cast <uintptr_t> ( context );

	//Setup stack
	regs->ss = KERNEL_DATA_SEG;
	regs->rsp = kernelStack;

	//Setup EFLAGS
	regs->rflags = X86_FLAGS_BASE | X86_FLAGS_INTERRUPT_ENABLE;
}

//! Destructor
Task::~Task()
{
    //Delete the saved vector state if it exists
    if(vectorState)
        delete vectorState;

    /*
    kprintf("Deleting file handles...\n");
    // Delete all the open file handles if they exist


    auto iter = fdMap.front();
    while( !iter.isEnd() )
    {
        auto pair = *iter;
        fdHandleTypes fht = pair.getData();
        delete fh;
        iter++;
    }
    */
    freePid(pid);

    //Unmap the thread's kernel mode stack from the address space
    virtualMemoryManager::unmapRegion(kernelStack - DEFAULT_KSTACK_SIZE, DEFAULT_KSTACK_SIZE, true, true);
}

bool Task::kStackValidate(const void *base, const uint64_t size)
{
    regionObj64 query(reinterpret_cast<const uintptr_t> (base), size );
    bool result = kernelStackRange.encases(query);
    if(!result)
    {
        kprintf("[TASK]: CT: %p PID: %u Value: %p Stack @ %p-%p\n", this, getPid(), base, kernelStackRange.getBase(), kernelStackRange.limit() - 1);
        if( Task **stackOwner = kStackMap.get(query) )
        {
            Task *own = *stackOwner;
            kprintf("[TASK]: Register stack belongs to Task %p PID: %u not Task %p PID: %u\n",
                    own, own->getPid(), this, getPid() );
        }

    }
    return result;
}

//! Context switch into this task
void COMPILER_NORETURN Task::execute()
{
    //kprintf("[TASK]: Entering task %p @ %p\n", this, regs->rip);
    if(flags & TASK_FLAG_SAVE_USER)
	{
		localState::setUserStackPtr(userStackSave);
		localState::setKernStackPtr(kernelStack);
		loadUserTLS();
		sched->maskVectorCatch();
	}
    regionObj64 regsQuery(reinterpret_cast<uintptr_t> (regs), sizeof(savedRegs) );
	if(!kernelStackRange.encases(regsQuery))
    {
        kprintf("[TASK]: Error! Saved registers not on task's kernel stack! Task: %p Regs: %p\n\t Stack %X-%X\n",
                this, regs, kernelStackRange.getBase(), kernelStackRange.limit() - 1);
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    if(!kStackValidate( reinterpret_cast <void *>(regs->rsp), 8))
    {
        kprintf("[TASK]: Error RSP does not point to appropriate kernel stack on entry! RSP: %p KStack range: %p-%p\n",
                regs->rsp, kernelStackRange.getBase(), kernelStackRange.limit() - 1);
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    if(!kStackValidate( const_cast <const savedRegs *> (regs), sizeof(savedRegs)))
    {
        kprintf("[TASK]: Execute call with bad stack pointer! RPTR: %p KStack range: %p-%p PID: %u\n", regs, kernelStackRange.getBase(), kernelStackRange.limit() - 1, getPid());
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    //kprintf("[TASK]: Entering task[%u] @ %X:%X with stack @ %X Kstack range: %p-%p\n", getPid(), regs->cs, regs->rip, regs->rsp, kernelStackRange.getBase(), kernelStackRange.limit() - 1);
	asm volatile ( "mov %1, %%cr3\n"
	               "mov %0, %%rsp\n"
				   "jmp isrReturn\n":: "r" ( regs ), "r" ( pml4 ): "memory" );

    COMPILER_UNREACHABLE();
}

/*! Saves the state of a task
 *  \param regPtr Pointer to the register saves on the stack
 */
void Task::save( savedRegs *regPtr, const void *topCaller )
{
	uint64_t extFeatures = localState::getFeatures();
	if(extFeatures & TASK_FLAG_SAVE_USER)
		userStackSave = localState::getUserStackPtr();

	regionObj64 stackQuery(reinterpret_cast<uintptr_t> (regPtr), sizeof(savedRegs) );
	if(!kernelStackRange.encases(stackQuery))
    {
        kprintf("[TASK]: Error saved registers not on task's kernel stack! Task: %p Regs: %p\n\tKStack range: %p-%p Caller: %p Idle: %p Regs ptr: %p\n",
                this, regPtr, kernelStackRange.getBase(), kernelStackRange.getBase() + kernelStackRange.getLength(), topCaller, sched->getIdle(), &regs );

        if( Task **stackOwner = kStackMap.get(stackQuery) )
        {
            Task *own = *stackOwner;
            kprintf("[TASK]: Register stack belongs to Task %p PID: %u not Task %p PID: %u",
                    own, own->getPid(), this, getPid() );
        }

        printWatchSaves();

        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    regionObj64 rspQuery(regPtr->rsp, 8);
    if(!kernelStackRange.encases(rspQuery))
    {
        kprintf("[TASK]: Error RSP does not point to kernel stack on save! RSP: %p KStack range: %p-%p\n", regPtr->rsp, kernelStackRange.getBase() + kernelStackRange.limit() - 1);
        asm volatile ("cli\n"
                      "hlt\n"::);
    }
    regs = regPtr;
}

/*! Retrieves the tasks priority value
 *  \return Priority value of the task
 */
uint32_t Task::getPriority() const
{
	return priority;
}

void Task::waitForEvent(Event *e)
{
    spinLockInstance lock(&taskLock);
    e->pushListener( static_pointer_cast<eventListener, waitListener>(taskWaitListener) );
}

bool Task::triggerWaitListener(const waitListener *listener, const uint32_t generation)
{
    spinLockInstance lock(&taskLock);
    if( listener == taskWaitListener.get() )
    {
        if(taskWaitListener->matchGeneration(generation))
        {
            sched->unwaitTask( localState::getSched(), this);
            return true;
        }
        else
        {
            //kprintf("Task wait listener[%p] generation mismatch! Found: %u Expected: %u\n", listener, taskWaitListener->getGeneration(), generation );
        }
    }
    else
    {
        //kprintf("[TASK]: Wait lilstener mismatch provided: %p Expected: %p Task: %p\n", listener, taskWaitListener.get(), this);
    }
    return false;
}

/*! Binds the task to a processor scheduler
 *  \param s Scheduler to bind the task to
 */
void Task::bindScheduler(processorScheduler *s)
{
    this->sched = s;
}

bool Task::verifyUserStack(uintptr_t ptr, uintptr_t length)
{
	return addrSpace->validateSpan(ptr, length, true);
}

void Task::saveVectorState(void)
{
	const uint64_t features = localState::getFeatures();
	if(features & CPU_FEATURE_FXSAVE)
	{
		asm volatile ("fxsave (%0)\n":: "r" (vectorState) );
	}
	else
	{
		asm volatile ("fnsave (%0)\n":: "r" (&vectorState->nonFxData.fpuState[0]));
		asm volatile ("movaps %%xmm0, (%0)\n"
						"movaps %%xmm1, 16(%0)\n"
						"movaps %%xmm2, 32(%0)\n"
						"movaps %%xmm3, 48(%0)\n"
						"movaps %%xmm4, 64(%0)\n"
						"movaps %%xmm5, 80(%0)\n"
						"movaps %%xmm6, 96(%0)\n"
						"movaps %%xmm7, 112(%0)\n"
						"movaps %%xmm8, 128(%0)\n"
						"movaps %%xmm9, 144(%0)\n"
						"movaps %%xmm10, 160(%0)\n"
						"movaps %%xmm11, 176(%0)\n"
						"movaps %%xmm12, 192(%0)\n"
						"movaps %%xmm13, 208(%0)\n"
						"movaps %%xmm14, 224(%0)\n"
						"movaps %%xmm15, 240(%0)\n"
						"stmxcsr (%1)\n":: "r" (&vectorState->nonFxData.xmmBank), "r" (&vectorState->nonFxData.mxcsr));
	}
}

void Task::destoryAddressSpace(void)
{
    addrSpace.reset();
}

void Task::printInstructionPointer(void)
{
    if(regs)
        kprintf("[TASK]: Task: %p RIP: %p\n", this, regs->rip);
}

//! Loads the task's vector state
void Task::loadVectorState(void)
{
	const uint64_t features = localState::getFeatures();
	if(!vectorState)
	{
		vectorState = new vectorRegs;
		asm volatile ("fninit\n"
						"ldmxcsr %0\n":: "m" (initMxcsr) );
		return;
	}

	if(features & CPU_FEATURE_FXSAVE)
	{
			asm volatile ("frstor (%0)\n":: "r" (vectorState) );
	}
	else
	{
		asm volatile ("frstor (%0)\n"
						"movaps (%1), %%xmm0\n"
						"movaps 16(%1), %%xmm1\n"
						"movaps 32(%1), %%xmm2\n"
						"movaps 48(%1), %%xmm3\n"
						"movaps 64(%1), %%xmm4\n"
						"movaps 80(%1), %%xmm5\n"
						"movaps 96(%1), %%xmm6\n"
						"movaps 112(%1), %%xmm7\n"
						"movaps 128(%1), %%xmm8\n"
						"movaps 144(%1), %%xmm9\n"
						"movaps 160(%1), %%xmm10\n"
						"movaps 176(%1), %%xmm11\n"
						"movaps 192(%1), %%xmm12\n"
						"movaps 208(%1), %%xmm13\n"
						"movaps 224(%1), %%xmm14\n"
						"movaps 240(%1), %%xmm15\n"
						"ldmxcsr (%2)\n":: "r" (&vectorState->nonFxData.fpuState[0]), "r" (&vectorState->nonFxData.xmmBank), "r" (&vectorState->nonFxData.mxcsr) );
	}
}

void Task::setQuantum(const uint32_t qValue)
{
    quantum = qValue;
}

uint32_t Task::getQuantum(void) const
{
    return quantum;
}


bool Task::allocatePid(pid_t &ret)
{
    spinLockInstance pidBankLock(&pidLock);
    return pidBank.getSingle(ret);
}

void Task::freePid(pid_t p)
{
    spinLockInstance pidBankLock(&pidLock);
    pidBank.freeSingle(p);
}

