#ifndef PCI_BUS_H_
#define PCI_BUS_H_

#include <regionobj.h>
#include <device.h>
#include <acpi/acpica.h>
#include "pci_bridge.h"
#include <templates/vector.cc>

#define BIOS32_PCI_STRING           0x49435024U

//PCI Capability values
#define PCI_CAP_MSI                 0x05U
#define PCI_CAP_MSI_X               0x11U

//MSI functionality values
#define MSI_CAP_OFF_CONTROL         0x02U
#define MSI_CAP_OFF_ADDRESS         0x04U
#define MSI_CAP_OFF_MESSAGE32       0x08U
#define MSI_CAP_OFF_MESSAGE64       0x0CU
#define MSI_CAP_OFF_MASK            0x10U
#define MSI_CAP_OFF_PENDING         0x14U

#define MSI_CONTROL_ENABLE          0x0001U

#define MSI_CONTROL_GET_MM_CAP(x)   (1<<(x>>1))
#define MSI_CONTROL_SET_MM(x)       (x<<4)

#define MSI_CONTROL_64BIT           0x80U
#define MSI_CONTROL_VECTOR_MASK     0x100U



//! Initialization structure for a pciBusRange object
struct pciBusInit
{
	Device *parent;
	uintptr_t address;
};

//! Structure which describes a PCI MMIO Access region
struct COMPILER_PACKED pciMmioEntry
{
	//! Physical base address of the MMIO region
	uint64_t base;
	//! Segment group number
	uint16_t segment;
	//! Starting bus number
	uint8_t startBus;
	//! Ending bus number
	uint8_t endBus;
	//! Reserved space
	uint32_t _reserved;
};

//! Structure which describes the PCI MMIO ACPI table
struct COMPILER_PACKED pciMcfg
{
	uint32_t signature, length;
	uint8_t revision, checksum, oemId[6], oemTableId[8],oemRev[4];
	uint8_t creatorId[4], creatorRev[4], _reserved[8];
	pciMmioEntry entries[];
};

//! An object used to access a specific range of PCI Bus addresses
class pciBusRange : public pciBridge, public regionObj64, protected Device
{
	public:
		pciBusRange();
	    pciBusRange(uint8_t endBus);
		pciBusRange( uint16_t segment, uint8_t startBus, uint8_t endBus );

		virtual bool init( void *context );
		//! Configuration space access functions
		bool readByte( const uint32_t address, const uint32_t reg, uint8_t &data ) const;
		bool readWord( const uint32_t address, const uint32_t reg, uint16_t &data ) const;
		bool readDWord( const uint32_t address, const uint32_t reg, uint32_t &data ) const;
		bool readQWord( const uint32_t address, const uint32_t reg, uint64_t &data ) const;

		bool writeByte( const uint32_t address, const uint32_t reg, const uint8_t value ) const;
		bool writeWord( const uint32_t address, const uint32_t reg, const uint16_t value ) const;
		bool writeDWord( const uint32_t address, const uint32_t reg, const uint32_t value ) const;
		bool writeQWord( const uint32_t address, const uint32_t reg, const uint64_t value ) const;

		bool enumerate(const uint32_t rootBridgeAddress, ACPI_HANDLE rootHandle);

		bool bindDevice( Device *dev, uint32_t address );
	private:
        //! PCI Identification key functions

        bool isPci2PciBridge(const uint8_t classCode, const uint8_t subClass, const uint8_t progInterface, const uint8_t headerType);

        bool enumerateFunctions(Vector<pci2PciBridge *> &bridges, const uint32_t deviceAddress, Device *parent, const int primaryFunction = 0);
        bool enumerateDevices(Vector<pci2PciBridge *> &bridges, const uint8_t bus, const uint8_t maxBus, Device *parent, const int baseDevice = -1);
        bool enumerateBus(pciBridge *parentBridge, Device *parentDev, ACPI_HANDLE parentHandle, const uint8_t busNum, const uint8_t subBus);

		bool isMmio;
		uintptr_t virtBase;

};

namespace pciBus
{
    bool loadDriverDb(const String &path);

    //Registers a PCI access through PCI address space
    bool registerTraditional( Device *p);
    //Registers a PCI Configuration space MMIO access region
    bool registerMmioRange( Device *p, uint64_t base, uint16_t segment, uint8_t busStart, uint8_t busEnd );

    bool enumeratePciRootBus(uint32_t rootBridgeAddress, ACPI_HANDLE bridgeHandle);

    //PCI Configuration space read functions, should not be used by most drivers
    bool readByte( uint32_t address, uint32_t offset, uint8_t &data );
    bool readWord( uint32_t address, uint32_t offset, uint16_t &data );
    bool readDWord( uint32_t address, uint32_t offset, uint32_t &data );
    bool readQWord( uint32_t address, uint32_t offset, uint64_t &data );

    //PCI Configuration space write functions, should not be used by most drivers
    bool writeByte( uint32_t address, uint32_t offset, uint8_t value );
    bool writeWord( uint32_t address, uint32_t offset, uint16_t value );
    bool writeDWord( uint32_t address, uint32_t offset, uint32_t value );
    bool writeQWord( uint32_t address, uint32_t offset, uint64_t value );

    //Base address register range extraction
    bool readBAR(uint64_t &base, uint64_t &length, uint32_t &flags, const uint32_t address, const uint32_t barNum);

    uint8_t getCapabilityOffset(const uint32_t address, const uint8_t capabilityId);

    void getDevResources(const uint32_t addr, Vector<systemResource> &devRes);
}

#endif
