#include "pci_pin.h"

pciPinMap::pciPinMap(const uint8_t pinInit, const uint32_t addrInit, systemResource &intResource) : pinInt(intResource), pinAddr( ((addrInit & 0xFFFF0000U) >> 14) | pinInit) {}

pciPinMap::pciPinMap(const uint8_t pinInit, const uint32_t addrInit) : pinAddr( ((addrInit & 0xFFFF0000U) >> 14) | pinInit) {}

bool pciPinMap::operator ==(const pciPinMap &other) const
{
    return (pinAddr == other.pinAddr);
}


bool pciPinMap::operator <(const pciPinMap &other) const
{
    return (pinAddr < other.pinAddr);
}

bool pciPinMap::operator >(const pciPinMap &other) const
{
    return (pinAddr > other.pinAddr);
}
