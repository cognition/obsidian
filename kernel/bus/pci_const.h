#ifndef PCI_CONST_H_
#define PCI_CONST_H_

#define PCI_BAR_IO_SPACE                          0x1U
#define PCI_BAR_64BIT                             0x2U

#define PCI_COMMAND_DECODE_IO                     0x0001U
#define PCI_COMMAND_DECODE_MEM                    0x0002U
#define PCI_COMMAND_BUS_MASTER                    0x0004U
#define PCI_COMMAND_SC                            0x0008U
#define PCI_COMMAND_MEM_WRITE_INVD                0x0010U
#define PCI_COMMAND_VGA_SNOOP                     0x0020U
#define PCI_COMMAND_PARRITY_ERROR                 0x0040U
#define PCI_COMMAND_SERR_ENABLE                   0x0100U
#define PCI_COMMAND_FAST_BACK2BACK                0x0200U
#define PCI_COMMAND_INTERRUPT_DISABLE             0x0400U


#define PCI_ADDR_DEVICE_GRAN                      0x8U

#define PCI_ADDR_DEVICE_MASK                      0xF8U
#define PCI_ADDR_FUNCTION_MASK                    0x07U

//! Common configuration space field offsets
#define PCI_CONFIG_VENDOR_ID                      0x00U
#define PCI_CONFIG_DEVICE_ID                      0x02U

#define PCI_CONFIG_COMMAND                        0x04U
#define PCI_CONFIG_STATUS                         0x06U

#define PCI_CONFIG_REVISION                       0x08U
#define PCI_CONFIG_PROGIF                         0x09U
#define PCI_CONFIG_SUBCLASS                       0x0AU
#define PCI_CONFIG_CLASS_CODE                     0x0BU

#define PCI_CONFIG_CLSIZE                         0x0CU
#define PCI_CONFIG_LATENCY_TIMER                  0x0DU
#define PCI_CONFIG_HEADER_TYPE                    0x0EU
#define PCI_CONFIG_BIST                           0x0FU

#define PCI_CONFIG_CAP_LIST                       0x34U
#define PCI_CONFIG_INT_PIN                        0x3DU

//Header Type 1 fields

#define PCI_CONFIG_SUBBUS                         0x1AU
#define PCI_CONFIG_SECONDARY_BUS                  0x19U

//! Class codes
#define PCI_CC_BRIDGE                             0x06U

//! Bridge subclass code
#define PCI_BRIDGE_SUBCLASS_HOST                  0x00U
#define PCI_BRIDGE_SUBCLASS_PCI2PCI               0x04U

//! Header type values
#define PCI_HEADER_STANDARD                       0x00U
#define PCI_HEADER_BRIDGE                         0x01U
#define PCI_HEADER_MULTIFUNCTION                  0x80U

#define PCI_DEV_NOT_PRESENT                       0xFFFFU

#define PCI_DEVICE_LIMIT                          32
#define PCI_FUNCTION_LIMIT                        8

#define PCI_MASK_SEG(x)                           ((x) & 0xFFFF0000U)

#define PCI_EXTRACT_DEVICE(x)                     (((x) & PCI_ADDR_DEVICE_MASK) >> 3)
#define PCI_EXTRACT_FUNCTION(x)                   ((x) & PCI_ADDR_FUNCTION_MASK)
#define PCI_EXTRACT_BUS(x)                        (((x)>>8) & 0xFFU)
#define PCI_MASK_FUNCTION(x)                      PCI_EXTRACT_FUNCTION(x)

#define PCI_MASK_HEADER_TYPE(x)                   ((x) & 0x7FU)

#define PCI_BAR_BASE                               0x10U
#define PCI_BAR_COUNT                              6

//! Status masks
#define PCI_STATUS_INTERRUPT                       0x0008U
#define PCI_STATUS_CAPLIST                         0x0010U
#define PCI_STATUS_66MHZ                           0x0020U
#define PCI_STATUS_FAST_BACK2BACK                  0x0080U
#define PCI_STATUS_MDPE                            0x0100U
#define PCI_STATUS_SIGNALED_TARGET_ABORT           0x0800U
#define PCI_STATUS_RECV_TARGET_ABORT               0x1000U
#define PCI_STATUS_RECV_MASTER_ABORT               0x2000U
#define PCI_STATUS_SIGNALED_SYS_ERROR              0x4000U
#define PCI_STATUS_DETECTED_PARITY_ERROR           0x8000U

#endif // PCI_CONST_H_
