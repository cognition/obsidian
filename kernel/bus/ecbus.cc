#include "ecbus.h"
#include <cpu/localstate.h>
#include <io/io.h>
#include <acpi/acpisys.h>

extern "C"
{
    #include <acpi/local_include/acpi.h>
}

#define EC_MAX_GLOCK_WAIT                  100
#define EC_MAX_COMMAND_WAIT                100

static Vector<embeddedController *> controllers;
static uint32_t ecEventHandler(ACPI_HANDLE device, UINT32 number, void *ecPtr);
static String ecHid("PNP0C09");

//ecQueryHandler class
ecQueryHandler::ecQueryHandler(uint8_t val, ACPI_HANDLE obj, ACPI_OBJECT_TYPE objType) : handle(obj), handleType(objType), number(val)
{
}

bool ecQueryHandler::operator ==(const uint8_t value) const
{
	return (number == value);
}

bool ecQueryHandler::operator ==(const ecQueryHandler &obj) const
{
	return (number == obj.number);
}

bool ecQueryHandler::operator >(const ecQueryHandler &obj) const
{
	return (number > obj.number);
}

bool ecQueryHandler::operator >(const uint8_t value) const
{
	return (number > value);
}

bool ecQueryHandler::operator <(const ecQueryHandler &obj) const
{
	return (number < obj.number);
}

bool ecQueryHandler::operator <(const uint8_t value) const
{
	return (number < value);
}

void ecQueryHandler::execute() const
{
	if(handleType == ACPI_TYPE_METHOD)
	{
		ACPI_STATUS status = AcpiEvaluateObject(handle, NULL, NULL, NULL);
		if(status != AE_OK)
		{
			kprintf("Error calling EC Query event handler: %u\n", status);
			asm("int3\n"::);
		}
	}
	else
	{
		//! \todo SMBus-EC-Host controller logic goes here
	}
}

//ecCommand class
ecCommand::ecCommand(uint8_t addr, uint8_t cmd, uint8_t data, Task *requestTask) : Event(), commandByte(cmd), addressByte(addr), dataByte(data)
{
}


//embeddedController class
embeddedController::embeddedController(const char *fullPath) : acpiDevice("Embedded Controller", fullPath),
		globalLock(0), command(NULL), data(NULL), currentCommand(NULL),
		commandStatus(EC_CMD_STATUS_COMPLETE)
{
	controllers.insert( this );
}

embeddedController::embeddedController(ACPI_HANDLE devPtr, const char *fullPath) : acpiDevice("Embedded Controller", devPtr, fullPath),
		globalLock(0), command(NULL), data(NULL), currentCommand(NULL), commandStatus(EC_CMD_STATUS_COMPLETE)
{
	controllers.insert( this );
}


embeddedController::~embeddedController()
{
	delete command;
	delete data;
}

bool embeddedController::init(void *context)
{
	ecInitInfo *info = reinterpret_cast <ecInitInfo *> (context);
	if(info->parent)
		registerParent(info->parent);

	Vector<systemResource> &ecDevRes = *(info->ecResources);
    if(!acquireResources(ecDevRes))
        return false;

    command = systemRegister::createRegisterFromResource(ecDevRes[0]);
    data = systemRegister::createRegisterFromResource(ecDevRes[1]);
    if(!command || !data)
        return false;

    AcpiInstallGpeHandler(info->gpeBlock, info->gpeNumber, ACPI_GPE_EDGE_TRIGGERED, &ecEventHandler, this);


	return true;
}

embeddedController *embeddedController::installEc(ACPI_HANDLE ptr, Device *parentObj)
{
	ACPI_STATUS status;
	char fullName[80];
	ACPI_BUFFER nameBuffer = {80, fullName};

	status = AcpiGetName(ptr, ACPI_FULL_PATHNAME, &nameBuffer);
	if(status != AE_OK)
	{
		kprintf("  EC: Error retrieving full device pathname!\n");
		return NULL;
	}

	for(uint32_t i = 0; i < controllers.length(); i++)
	{
		embeddedController *ec = controllers[i];
		if(ec->getFullAcpiPath() == fullName)
		{
			ec->parentAttach(parentObj);
			AcpiAttachData(ptr, &AcpiGenericRemoveHandler, static_cast <Device *> (ec) );
		}
	}


	ACPI_OBJECT gpeObj;
	ecInitInfo initInfo;
	ACPI_BUFFER retBuffer = { sizeof(ACPI_OBJECT), &gpeObj };

	initInfo.parent = parentObj;
	status = AcpiEvaluateObject(ptr, String("_GPE").getBuffer(), NULL, &retBuffer);

	if(status != AE_OK)
		return NULL;

	if( gpeObj.Type == ACPI_TYPE_INTEGER )
	{
		initInfo.gpeBlock = NULL;
		initInfo.gpeNumber = gpeObj.Integer.Value;
	}
	else if(gpeObj.Type == ACPI_TYPE_PACKAGE)
	{
		if(gpeObj.Package.Count == 2)
		{
			ACPI_OBJECT *gpeNameObj = &gpeObj.Package.Elements[0];
			ACPI_OBJECT *gpeNumObj = &gpeObj.Package.Elements[1];


			if( (gpeNameObj->Type != ACPI_TYPE_STRING) || (gpeNumObj->Type != ACPI_TYPE_INTEGER))
			{
				kprintf("  EC: _GPE Object package contains invalid types!\n");
				return NULL;
			}

			String gpeName(gpeNameObj->String.Pointer, gpeNameObj->String.Length);
			status = AcpiGetHandle(ptr, gpeName.toBuffer(), &initInfo.gpeBlock);

			if(status != AE_OK)
			{
				kprintf("  EC: Unable to find GPE block required by the controller!\n");
				return NULL;
			}

			initInfo.gpeNumber = gpeNumObj->Integer.Value;
		}
		else
		{
			kprintf("  EC: Package returned by _GPE contains an invalid number of elements!\n");
			return NULL;
		}
	}
	else
	{
		kprintf("  EC: _GPE evaluated to an invalid object type.\n");
		return NULL;
	}

	Vector<systemResource> ecDevRes;
	if( !acpiServices::getDeviceResources(ptr, ecDevRes) )
		return NULL;

	if(ecDevRes.length() < 2)
	{
		kprintf(" EC: Invalid resource settings provided!\n");
		return NULL;
	}

	initInfo.ecResources = &ecDevRes;

	embeddedController *ecDev = new embeddedController(ptr, fullName);
	if(!ecDev->init(&initInfo))
	{
		delete ecDev;
		return NULL;
	}
	else
	{
		AcpiAttachData(ptr, &AcpiGenericRemoveHandler, ecDev);
		return ecDev;
	}
}

void embeddedController::doCommandProcess(void)
{
	spinLockInstance lock(&controllerLock);
	if(commandStatus & EC_CMD_STATUS_COMPLETE)
	{
		if(commandQueue.empty())
		{
			commandStatus |= EC_CMD_STATUS_READY;
			currentCommand = NULL;
			AcpiReleaseGlobalLock(globalLock);
		}
		else
		{
			currentCommand = commandQueue.remove();
			startCommand();
		}
	}
	else
	{
		if(currentCommand->commandByte == EC_CMD_WRITE)
		{
			if(commandStatus & EC_CMD_STATUS_WRITE_ADDR)
			{
				data->write(currentCommand->addressByte);
				commandStatus &= EC_CMD_STATUS_WRITE_ADDR;
			}
			else
			{
				data->write(currentCommand->dataByte);
				completeCurrent();
			}
		}
	}
}

void embeddedController::startCommand(void)
{
	command->write(currentCommand->commandByte);

	switch(currentCommand->commandByte)
	{
		case EC_CMD_READ:
			data->write(currentCommand->addressByte);
			break;
		case EC_CMD_WRITE:
			commandStatus |= EC_CMD_STATUS_WRITE_ADDR;
			break;
		case EC_CMD_QUERY:
			commandStatus |= EC_CMD_STATUS_QUERY;
			break;
	}
	commandStatus &= ~EC_CMD_STATUS_COMPLETE;
}

void embeddedController::completeCurrent(void)
{
	commandStatus |= EC_CMD_STATUS_COMPLETE;
	currentCommand = NULL;
}

void embeddedController::commandFinish(void)
{
	spinLockInstance lock(&controllerLock);
	if(commandStatus & EC_CMD_STATUS_QUERY)
	{
		handleQuery( data->read() );
		completeCurrent();
	}
	else
	{
		currentCommand->dataByte = data->read();
		completeCurrent();
	}
}

uint8_t embeddedController::issueRead(uint8_t address)
{
	ecCommand readCommand(address, EC_CMD_READ, 0, Scheduler::getCurrentTask() );
	dispatchCommand(&readCommand);
	Scheduler::waitCurrent(&readCommand, EC_MAX_COMMAND_WAIT);
	return readCommand.dataByte;
}

void embeddedController::issueWrite(uint8_t address, uint8_t value)
{
	ecCommand writeCommand(address, EC_CMD_WRITE, value, Scheduler::getCurrentTask() );
	dispatchCommand(&writeCommand);
	Scheduler::waitCurrent(&writeCommand, EC_MAX_COMMAND_WAIT);
}

void embeddedController::issueQuery(void)
{
	ecCommand queryCommand(0, EC_CMD_QUERY, 0, Scheduler::getCurrentTask() );
	dispatchCommand(&queryCommand);
}

void embeddedController::dispatchCommand(ecCommand *cmd)
{
	spinLockInstance lock(&controllerLock);
	if( !currentCommand && commandQueue.empty() )
	{
		if( getStatus() & EC_STATUS_IBF )
		{
			AcpiAcquireGlobalLock(EC_MAX_GLOCK_WAIT, &globalLock);
			currentCommand = cmd;
			startCommand();
			return;
		}
	}

	commandQueue.add(cmd);
}

uint8_t embeddedController::getStatus(void) const
{
	return command->read();
}

bool embeddedController::hasWork(void) const
{
	return ( currentCommand );
}

void embeddedController::handleQuery(uint8_t val)
{
	if(val)
	{
		const ecQueryHandler *handler = queryHandlers.find(val);
		if(!handler)
		{
			char bytes[5] = {'_', 'Q'};
			bytes[2] = ((val >> 4) & 0xFU) + '0';
			bytes[3] = (val & 0xFU) + '0';
			bytes[4] = 0;

			ACPI_HANDLE methodHandle;
			ACPI_STATUS status = AcpiGetHandle(devHandle, bytes, &methodHandle);
			if(status != AE_OK)
			{
				kprintf("Error finding query method handle: %u\n", status);
				asm ("int3\n"::);
			}

			ACPI_OBJECT_TYPE handleType;
			AcpiGetType(methodHandle, &handleType);
			if(handleType != ACPI_TYPE_METHOD)
			{
				kprintf("Error query response object is not a method!\n");
				asm ("int3\n"::);
			}

			queryHandlers.insert( ecQueryHandler(val, methodHandle, ACPI_TYPE_METHOD) );
			status = AcpiEvaluateObject(methodHandle, NULL, NULL, NULL);
			if(status != AE_OK)
			{
				kprintf("Error executing query method handler: %u\n", status);
				asm ("int3\n"::);
			}
		}
		else
			handler->execute();
	}
	commandStatus &= ~EC_CMD_STATUS_QUERY;
}

void embeddedController::parentAttach(Device *p)
{
	registerParent(p);
}

ACPI_STATUS ecAccessHandler(uint32_t Function, ACPI_PHYSICAL_ADDRESS Address, uint32_t BitWidth, uint64_t *Value, void *HandlerContext, void COMPILER_UNUSED *RegionContext)
{
	embeddedController *ecDev = reinterpret_cast <embeddedController *> (HandlerContext);

	if(!Value)
		return AE_BAD_PARAMETER;
	if(!ecDev)
		return AE_NOT_EXIST;
	if(BitWidth != 8)
		return AE_BAD_PARAMETER;

	switch(Function)
	{
		case ACPI_READ:
			*Value = ecDev->issueRead(Address);
			return AE_OK;
			break;
		case ACPI_WRITE:
			ecDev->issueWrite(Address, *Value);
			return AE_OK;
			break;
		default:
			return AE_BAD_PARAMETER;
			break;
	}
	return AE_OK;
}

ACPI_STATUS ecAddressSpaceHandler(ACPI_HANDLE COMPILER_UNUSED Region, uint32_t COMPILER_UNUSED Function, void COMPILER_UNUSED *HandlerContext, void COMPILER_UNUSED **ReturnContext)
{
	return AE_OK;
}

static uint32_t ecEventHandler(ACPI_HANDLE COMPILER_UNUSED device, UINT32 COMPILER_UNUSED number, void *ecPtr)
{
	embeddedController *ec = reinterpret_cast <embeddedController *> (ecPtr);
	uint8_t status = ec->getStatus();

	if(status & EC_STATUS_OBF)
		ec->commandFinish();
	if(status & EC_STATUS_SCI_EVENT)
		ec->issueQuery();
	else if(status & EC_STATUS_IBF)
		ec->doCommandProcess();

	return ACPI_REENABLE_GPE;
}

/*! ACPICA walk function handler, locates and activates all EC controllers
 *  \param dev ACPICA Handle for the device
 *  \param Level Level of depth into the namespace that the device is located at
 *  \param context Walk specific context value
 *  \param returnValue Walk specific return value pointer
 *  \return AE_OK If the device is initialized successfully, AE_ERROR otherwise
 */
ACPI_STATUS ecDeviceFound(ACPI_HANDLE dev, UINT32 COMPILER_UNUSED Level, void COMPILER_UNUSED *context, void COMPILER_UNUSED **returnValue)
{
	char fullName[80];
	ACPI_BUFFER nameBuffer = { 80, fullName };
	Device *parent = NULL;
	ACPI_HANDLE pHandle = dev;
	ACPI_STATUS pStatus;
	embeddedController *ec = NULL;

	do
    {


        pStatus = AcpiGetParent(pHandle, &pHandle);
        if( pStatus != AE_OK )
            return pStatus;


		void *data = NULL;
		pStatus = AcpiGetData(pHandle, &AcpiGenericRemoveHandler, &data);
		if(ACPI_SUCCESS(pStatus))
			parent = reinterpret_cast <Device *> (data);
        else
            return pStatus;
    } while(!parent);


	AcpiGetName(dev, ACPI_FULL_PATHNAME, &nameBuffer);
	String nameString(fullName);

	for(uint32_t i = 0; i < controllers.length(); i++)
	{
		if(nameString.findSubstring(controllers[i]->getFullAcpiPath().toBuffer() ) != -1)
		{
			ec = controllers[i];
			goto installAddressSpaceHandler;
		}
	}

	ec = embeddedController::installEc(dev, parent);
	if(!ec)
		return AE_ERROR;

installAddressSpaceHandler:
	return AcpiInstallAddressSpaceHandler(dev, ACPI_ADR_SPACE_EC, &ecAccessHandler, &ecAddressSpaceHandler, ec);
}
