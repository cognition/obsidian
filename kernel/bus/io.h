#ifndef BUS_IO_H_
#define BUS_IO_H_

namespace ioBus
{
//Read functions
inline uint8_t readByte( const uint16_t port )
{
	uint8_t ret;
	asm volatile ( "inb %%dx, %%al\n": "=a" ( ret ): "d" ( port ) );
	return ret;
}

inline uint16_t readWord( const uint16_t port )
{
	uint16_t ret;
	asm volatile ( "inw %%dx, %%ax\n": "=a" ( ret ): "d" ( port ) );
	return ret;
}

inline uint32_t readDWord( const uint16_t port )
{
	uint32_t ret;
	asm volatile ( "inl %%dx, %%eax\n": "=a" ( ret ): "d" ( port ) );
	return ret;
}

//Write functions
inline void writeByte( const uint16_t port, const uint8_t byte )
{
	asm volatile ( "outb %%al, %%dx\n":: "a" ( byte ), "d" ( port ) );
}

inline void writeWord( const uint16_t port, const uint16_t word )
{
	asm volatile ( "outw %%ax, %%dx\n":: "a" ( word ), "d" ( port ) );
}

inline void writeDWord( const uint16_t port, const uint32_t dword )
{
	asm volatile ( "outl %%eax, %%dx\n":: "a" ( dword ), "d" ( port ) );
}
}

#endif
