#ifndef SYSTEM_REGISTER_H_
#define SYSTEM_REGISTER_H_

#include <compiler.h>
#include <resources.h>

enum dwordBusSize : uint8_t
{
	dBusSizeByte, dBusSizeWord, dBusSizeDword
};

enum qwordBusSize : uint8_t
{
	qBusSizeByte, qBusSizeWord, qBusSizeDword, qBusSizeQword
};

//!Register base class
class systemRegister
{
	public:
		systemRegister(void);
		virtual ~systemRegister();
		virtual uint64_t read() const = 0;
		virtual void write(uint64_t value) const = 0;
		static systemRegister *createRegisterFromResource(systemResource &res);
	protected:
};

class mmioRegister : public systemRegister
{
	public:
		mmioRegister(uintptr_t addr, const qwordBusSize busAccessSize, const uint8_t regLength);
		virtual ~mmioRegister();
		virtual uint64_t read() const;
		virtual void write(uint64_t value) const;
	private:
		const uintptr_t address;
		const uint8_t physLength;
		const qwordBusSize accessSize;
};

class ioRegister : public systemRegister
{
	public:
		ioRegister(uint16_t addr, const dwordBusSize busAccessSize);
		virtual ~ioRegister();
		virtual uint64_t read() const;
		virtual void write(uint64_t value) const;
	private:
		const uint16_t address;
		const dwordBusSize accessSize;
};

#endif
