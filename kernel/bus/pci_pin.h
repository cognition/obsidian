#ifndef PCI_PIN_H_
#define PCI_PIN_H_

#include <resources.h>

class pciPinMap
{
    public:
        pciPinMap(const uint8_t pinInit, const uint32_t addrInit);
        pciPinMap(const uint8_t pinInit, const uint32_t addrInit, systemResource &intResource);
        bool operator ==(const pciPinMap &other) const;
        bool operator <(const pciPinMap &other) const;
        bool operator >(const pciPinMap &other) const;
        systemResource getResource(void) const
        {
            return pinInt;
        }
    private:
        systemResource pinInt;
        uint32_t pinAddr;

};


#endif // PCI_PIN_H_
