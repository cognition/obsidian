#include "register.h"
#include "io.h"
#include <memory/virtual.h>

static bool getDwordBusRegSize(const uint8_t size, dwordBusSize &bSize)
{
	switch(size)
	{
		case 1:
			bSize = dBusSizeByte;
			return true;
			break;
		case 2:
			bSize = dBusSizeWord;
			return true;
			break;
		case 4:
			bSize = dBusSizeDword;
			return true;
			break;
		default:
			return false;
			break;
	}
}

static bool getQwordBusRegSize(const uint8_t size, qwordBusSize &bSize)
{
	switch(size)
	{
		case 1:
			bSize = qBusSizeByte;
			return true;
			break;
		case 2:
			bSize = qBusSizeWord;
			return true;
			break;
		case 4:
			bSize = qBusSizeDword;
			return true;
			break;
		case 8:
			bSize = qBusSizeQword;
			return true;
			break;
		default:
			return false;
			break;
	}
}

//systemRegister class
systemRegister::systemRegister(void)
{
}

systemRegister::~systemRegister()
{
}

systemRegister *systemRegister::createRegisterFromResource(systemResource &res)
{
	uint8_t regSize = res.getLength();

	//Create the actual register object based on type
	switch( res.getType() )
	{
		case systemResourceIo:
		{
			dwordBusSize dBusSize;
			if(!getDwordBusRegSize(regSize, dBusSize))
				return NULL;
			else
				return new ioRegister(res.getBase(), dBusSize);
			break;
		}
		case systemResourceMemory:
		{
			qwordBusSize qBusSize;
			if(!getQwordBusRegSize(regSize, qBusSize))
				return NULL;
			else
				return new mmioRegister(res.getBase(), qBusSize, regSize);
			break;
		}
		default:
			return NULL;
			break;
	}
}

//ioRegister class
ioRegister::ioRegister(uint16_t addr, const dwordBusSize busAccessSize) : systemRegister(), address(addr), accessSize(busAccessSize)
{
}

ioRegister::~ioRegister()
{
}

uint64_t ioRegister::read() const
{
	switch(accessSize)
	{
		case dBusSizeByte:
			return ioBus::readByte(address);
			break;
		case dBusSizeWord:
			return ioBus::readWord(address);
			break;
		case dBusSizeDword:
			return ioBus::readDWord(address);
			break;
	}
}

void ioRegister::write(uint64_t value) const
{
	switch(accessSize)
	{
		case dBusSizeByte:
			return ioBus::writeByte(address, value);
			break;
		case dBusSizeWord:
			return ioBus::writeWord(address, value);
			break;
		case dBusSizeDword:
			return ioBus::writeDWord(address, value);
			break;
	}
}


//mmioRegister class
mmioRegister::mmioRegister(uintptr_t addr, const qwordBusSize busAccessSize, const uint8_t regLength) : systemRegister(), address( virtualMemoryManager::registerMmioMapping(addr, regLength) ), physLength(regLength), accessSize(busAccessSize)
{
}

mmioRegister::~mmioRegister()
{
	virtualMemoryManager::unmapPhysRegion(address, physLength);
}

uint64_t mmioRegister::read() const
{
	volatile uint8_t *reg8 = reinterpret_cast <volatile uint8_t *> (address);
	volatile uint16_t *reg16 = reinterpret_cast <volatile uint16_t *> (address);
	volatile uint32_t *reg32 = reinterpret_cast <volatile uint32_t *> (address);
	volatile uint64_t *reg64 = reinterpret_cast <volatile uint64_t *> (address);

	switch(accessSize)
	{
		case qBusSizeByte:
			return *reg8;
			break;
		case qBusSizeWord:
			return *reg16;
			break;
		case qBusSizeDword:
			return *reg32;
			break;
		case qBusSizeQword:
			return *reg64;
			break;
	}
}

void mmioRegister::write(uint64_t value) const
{
	volatile uint8_t *reg8 = reinterpret_cast <volatile uint8_t *> (address);
	volatile uint16_t *reg16 = reinterpret_cast <volatile uint16_t *> (address);
	volatile uint32_t *reg32 = reinterpret_cast <volatile uint32_t *> (address);
	volatile uint64_t *reg64 = reinterpret_cast <volatile uint64_t *> (address);

	switch(accessSize)
	{
		case qBusSizeByte:
			*reg8 = value & 0xFFU;
			break;
		case qBusSizeWord:
			*reg16 = value & 0xFFFFU;
			break;
		case qBusSizeDword:
			*reg32 = value & 0xFFFFFFFFU;
			break;
		case qBusSizeQword:
			*reg64 = value;
			break;
	}
}
