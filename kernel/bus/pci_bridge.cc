#include "pci.h"
#include <driverapi/dbase.h>
#include <driverapi/module_loader.h>
#include <cpu/localstate.h>

extern "C"
{
    #include <acpi/local_include/acpi.h>
}

#define PCI_BRIDGE_IO_BASE                         0x1C
#define PCI_BRIDGE_IO_LIMIT                        0x1D

#define PCI_BRIDGE_IO_BASE_HIGH                    0x30
#define PCI_BRIDGE_IO_LIMIT_HIGH                   0x32

#define PCI_BRIDGE_MEMORY_BASE                     0x20
#define PCI_BRIDGE_MEMORY_LIMIT                    0x22

#define PCI_BRIDGE_PREFETCH_MEMORY_BASE            0x24
#define PCI_BRIDGE_PREFETCH_MEMORY_LIMIT           0x26

#define PCI_BRIDGE_PREFETCH_MEMORY_BASE_HIGH       0x28
#define PCI_BRIDGE_PREFETCH_MEMORY_LIMIT_HIGH      0x2C

#define ACPI_ADDR_EXTRACT_DEV(x)                   ((x)>>16)
#define ACPI_ADDR_EXTRACT_FUNC(x)                  ((x) & 0xFFFFU)

/*! Finds a matching device driver for a PCI device
 *  \param parent Parent device object
 *  \param baseAddress Full PCI address of the device
 *  \return True if a driver was found and queued (or existed already), false if no driver could be found
 */
bool pciBridge::findDriver(Device *parent, const uint32_t baseAddress)
{
    uint64_t ccId, vendorId;

    const driverDesc *drvInfo;
    busAddressDesc addr = {baseAddress, BUS_ID_PCI};


    makeVendorIdKey(vendorId, baseAddress);


    if(!driverDatabase::findModule(drvInfo, &addr, vendorId) )
    {
        getClassCode(ccId, baseAddress);
        if(!driverDatabase::findModule(drvInfo, &addr, ccId) )
            return false;
    }

    DriverDevice *devDrv;
    kprintf("Found driver \"%s\" for PCI Device @ %u:%u:%u\n",
            drvInfo->getDriverPath().toBuffer(), PCI_EXTRACT_BUS(baseAddress), PCI_EXTRACT_DEVICE(baseAddress), PCI_EXTRACT_FUNCTION(baseAddress) );
    //FIXME: Actually implement device description strings
    notifyEvent<modStatus> waitObj(modStatus::modNotFound);
    modStatus qResult = moduleLoader::queueModule( drvInfo->getDriverPath(), parent, addr, drvInfo->getDriverName(), drvInfo->getDriverRefId(), devDrv, &waitObj);
    if(qResult == modStatus::modLoaded)
    {
        //Attempt to directly initialize the device if the driver is already loaded
        if(!devDrv->init(NULL))
            return false;
    }
    else
    {
        kprintf("Error initializing driver %s for PCI Device @ %u:%u:%u - %s\n", drvInfo->getDriverPath().toBuffer(),
                PCI_EXTRACT_BUS(baseAddress), PCI_EXTRACT_DEVICE(baseAddress), PCI_EXTRACT_FUNCTION(baseAddress), modStatusToString(qResult) );
    }


    return true;
}

pci2PciBridge *pciBridge::makePciBridge(Vector<pci2PciBridge *> &bridges, uint32_t baseAddress, Device *parent, const uint8_t secondaryBus, const uint8_t subBus)
{
    pci2PciBridge *p2p = new pci2PciBridge(baseAddress, parent, secondaryBus, subBus);
    bridges.insert(p2p);
    bridgeMap.insert(regionObj32(secondaryBus, 1), p2p );
    return p2p;
}

/* Attempts to retrieve the interrupt line a device is connected to
 * \param targetBus PCI Bus the device is connected to
 * \param addr ACPI style device and function address value
 * \param pin Current pin value of the device
 * \param res Return reference for the interrupt resource
 * \return True if the device has an associated interrupt resource, false otherwise
 */
bool pciBridge::getPinRoute(const uint8_t targetBus, const uint32_t addr, const uint8_t pin, systemResource &res) const
{
    if(targetBus == busProvided)
    {
        if(!pinMappings.size())
        {
            //Default to the specification defined fallback PCI-to-PCI bridge routing semantics
            int pinIndex = ACPI_ADDR_EXTRACT_DEV(addr) + pin;
            pinIndex %= 4;
            if(pinIndex < bridgePins.length())
            {
                res = bridgePins[pinIndex];
                return true;
            }
            else
            {
                kprintf("[PCI ]: Invalid pin mapping %u requested for device @ B: %u D: %u F: %u\n", pinIndex, targetBus, ACPI_ADDR_EXTRACT_DEV(addr), ACPI_ADDR_EXTRACT_FUNC(addr));
                return false;
            }
        }
        else
        {
            pciPinMap pinRequest(pin, addr);
            const pciPinMap *mapping = pinMappings.find(pinRequest);
            if(!mapping)
            {

                kprintf("[PCI ]: Could not locate entry for pin %u requested for device @ B: %u D: %u F: %u\n", pin, targetBus, ACPI_ADDR_EXTRACT_DEV(addr), ACPI_ADDR_EXTRACT_FUNC(addr));

                return false;
            }
            else
            {
                res = mapping->getResource();
                return true;
            }
        }
    }
    else
    {
        // Find a PCI-to-PCI bridge behind this bridge device if the bus number does not match
        regionObj32 tBusObj(targetBus, 1);
        if(pci2PciBridge * const *bridgePtr = bridgeMap.get(tBusObj))
        {
            const pci2PciBridge *bridge = *bridgePtr;

            /*
            const uint64_t otherBridgeAddress = bridge->getBridgeAddress();

            kprintf("Found bridge providing bus %u @ %u:%u:%u\n", targetBus, PCI_EXTRACT_BUS(otherBridgeAddress),
                    PCI_EXTRACT_DEVICE(otherBridgeAddress), PCI_EXTRACT_FUNCTION(otherBridgeAddress));
            */

            return bridge->getPinRoute(targetBus, addr, pin, res);
        }
        else
        {

            kprintf("[PCI ]: Could not locate bridge providing bus %u requested for device @ B: %u D: %u F: %u\n", targetBus, targetBus, ACPI_ADDR_EXTRACT_DEV(addr), ACPI_ADDR_EXTRACT_FUNC((addr)));
            return false;
        }
    }
}

/*! Attempts to retrieve the PCI interrupt pin routing table from the ACPI namespace
 *  \param bridgeHandle Handle the bridge's ACPI namespace device
 *  \return True if the PCI routing table was obtained and processed, false otherwise
 */
bool pciBridge::processRoutingTable(ACPI_HANDLE &bridgeHandle)
{
    // Have ACPICA automatically do the data allocations
    ACPI_BUFFER routingTableBuffer = {ACPI_ALLOCATE_BUFFER, NULL};

    // Get the routing table
    ACPI_STATUS status = AcpiGetIrqRoutingTable(bridgeHandle, &routingTableBuffer);
    if(status == AE_NOT_FOUND)
        return false;
    else if(ACPI_FAILURE(status))
    {
        kprintf("[PCI ] - Failed to obtained root bridge/complex routing table error: %s\n", AcpiFormatException(status));
        return false;
    }

    //Parse the routing table
    uint8_t *prtByteBuffer = reinterpret_cast <uint8_t *> (routingTableBuffer.Pointer);
    for(int prtOffset = 0; prtOffset < routingTableBuffer.Length; )
    {
        ACPI_PCI_ROUTING_TABLE *entry = reinterpret_cast <ACPI_PCI_ROUTING_TABLE *> (&prtByteBuffer[prtOffset]);
        // Exit the loop if there's a zero length/terminal entry encountered
        if(!entry->Length)
            break;
        // Make sure the pin value is valid
        if(entry->Pin >= 4)
            break;
        if(!entry->Source[0])
        {
            // Fixed global interrupt line
            systemResource intRes(systemResourceInterrupt, entry->SourceIndex, 1);
            pciPinMap pin(entry->Pin, entry->Address, intRes);
            pinMappings.insert(pin);
        }
        else
        {
            //PCI Interrupt link device pathname, potentially configurable
            ACPI_HANDLE linkHandle;
            //Get the device handle for the provided path name
            status = AcpiGetHandle(bridgeHandle, entry->Source, &linkHandle);
            if(ACPI_FAILURE(status))
            {
                kprintf("PCI: Unable to located interrupt source handle!\n");
                return false;
            }
            //Get the interrupt link device resources
            Vector<systemResource> linkResources;
            acpiServices::getDeviceResources(linkHandle, linkResources);
            if(entry->SourceIndex >= linkResources.length() )
            {
                kprintf("PCI: Invalid resource index for interrupt link object!\n");
                return false;
            }

            systemResourceType resType = linkResources[entry->SourceIndex].getType();
            if( (resType != systemResourceInterrupt) && (resType != systemResourceIrq))
            {
                kprintf("Invalid resource type found on interrupt link: %s\n", linkResources[entry->SourceIndex].toString().toBuffer() );
                return false;
            }

            pinMappings.insert( pciPinMap(entry->Pin, entry->Address, linkResources[entry->SourceIndex]) );
        }
        prtOffset += entry->Length;
    }

    AcpiOsFree(routingTableBuffer.Pointer);

    return true;
}


pci2PciBridge::pci2PciBridge(const uint32_t address, Device *p, const uint8_t secondaryInit, const uint8_t subInit) :
    Device("PCI to PCI Bridge", "pci2pci_bridge", p), pciBridge(secondaryInit), handle(ACPI_ROOT_OBJECT), secondaryBus(secondaryInit), subordinateBus(subInit)
    {
        bridgeAddress = address;
    };


/*! Initialized the bridge device
 *  \param context Pointer to the PCI Bus address of the device
 *  \return true if the initialization was successful, false otherwise
 */
bool pci2PciBridge::init(void *context)
{
    bridgeInitInfo *initInfo = reinterpret_cast <bridgeInitInfo *>(context);
    pciBusRange *busRange = reinterpret_cast <pciBusRange *> (initInfo->busRange);

    uint64_t base, length;
    uint32_t flags;

    //Check BARs
    pciBus::readBAR(base, length, flags, bridgeAddress, 0);

    if(base || length)
    {
        systemResource res( (flags & PCI_BAR_IO_SPACE) ? systemResourceIo:systemResourceMemory, base, length);
        if(!acquireResource(res))
            return false;
    }
    if( !(flags & PCI_BAR_64BIT))
    {
        //Only check the second BAR if we don't have a 64-bit memory register
        pciBus::readBAR(base, length, flags, bridgeAddress, 1);

        if(base || length)
        {
            systemResource r2( (flags & PCI_BAR_IO_SPACE) ? systemResourceIo:systemResourceMemory, base, length);
            if(!acquireResource(r2))
                return false;
        }
    }

    uint8_t ioBase8, ioLimit8;
    uint16_t ioBase, ioLimit;
    uint16_t memoryBase16, memoryLimit16, pMemoryBase16, pMemoryLimit16;
    uint32_t memoryBase32, memoryLimit32, pMemoryBase32, pMemoryLimit32;
    uint64_t pMemoryBase, pMemoryLimit;

    //Attempt to retrieve forwarded I/O range
    busRange->readByte(bridgeAddress, PCI_BRIDGE_IO_BASE, ioBase8);
    busRange->readByte(bridgeAddress, PCI_BRIDGE_IO_LIMIT, ioLimit8);

    ioBase = ioBase8 & 0xF0;
    ioLimit = ioLimit8 & 0xF0;

    ioBase <<= 8;
    ioLimit <<= 8;

    //There's no I/O address forwarded if both limit and base are zero, or the limit is below the base (nonsensical)
    if((ioBase | ioLimit) && (ioLimit >= ioBase))
    {
        ioLimit |= 0xFFFU;
        systemResource ioRes(systemResourceIo, ioBase, (ioLimit - ioBase) + 1);
        if(!acquireResource(ioRes))
            return false;
    }

    //Attempt to retrieve forwarded memory region
    busRange->readWord(bridgeAddress, PCI_BRIDGE_MEMORY_BASE, memoryBase16);
    busRange->readWord(bridgeAddress, PCI_BRIDGE_MEMORY_LIMIT, memoryLimit16);

    memoryBase32 = memoryBase16 & 0xFFF0;
    memoryLimit32 = memoryLimit16 & 0xFFF0;

    memoryBase32 <<= 16;
    memoryLimit32 <<= 16;

    //There's no standard memory address forwarded if both limit and base are zero, or the limit is below the base (nonsensical)
    if( (memoryBase32 | memoryLimit32) && (memoryLimit32 >= memoryBase32)  )
    {
        memoryLimit32 |= 0xFFFFFU;
        systemResource memRes(systemResourceMemory, memoryBase32, (memoryLimit32 - memoryBase32) + 1);
        if(!acquireResource(memRes))
            return false;
    }

    //Attempt to retrieve forwarded prefetch range
    busRange->readDWord(bridgeAddress, PCI_BRIDGE_PREFETCH_MEMORY_BASE_HIGH, pMemoryBase32);
    busRange->readDWord(bridgeAddress, PCI_BRIDGE_PREFETCH_MEMORY_LIMIT_HIGH, pMemoryLimit32);

    pMemoryBase = pMemoryBase32;
    pMemoryLimit = pMemoryLimit32;

    pMemoryBase <<= 32;
    pMemoryLimit <<= 32;

    busRange->readWord(bridgeAddress, PCI_BRIDGE_PREFETCH_MEMORY_BASE, pMemoryBase16);
    busRange->readWord(bridgeAddress, PCI_BRIDGE_PREFETCH_MEMORY_LIMIT, pMemoryLimit16);

    pMemoryBase32 = pMemoryBase16 & 0xFFF0U;
    pMemoryLimit32 = pMemoryLimit16 & 0xFFF0U;

    pMemoryBase32 <<= 16;
    pMemoryLimit32 <<= 16;

    pMemoryBase |= (uint64_t)pMemoryBase32;
    pMemoryLimit |= (uint64_t)pMemoryLimit32;

    //There's no prefetchable memory address forwarded if both limit and base are zero, or the limit is below the base (nonsensical)
    if((pMemoryBase | pMemoryLimit) && (pMemoryLimit >= pMemoryBase))
    {
        pMemoryLimit |= 0xFFFFFUL;
        systemResource memRes(systemResourceMemory, pMemoryBase, (pMemoryLimit - pMemoryBase) + 1);
        if(!acquireResource(memRes))
            return false;
    }

    if(handle != ACPI_ROOT_OBJECT)
    {
        if(processRoutingTable(handle))
            return true;
    }

    if(!initInfo->parentBridge)
    {
        kprintf("[PCI ]: Fatal error: No interrupt routing information available on root bridge!\n");
        return false;
    }
    // Record the bridge's interrupt pins so they can be forwarded if necessary
    for(int pin = 0; pin < 4; pin++)
    {
        systemResource pinRes;
        if(!(initInfo->parentBridge->getPinRoute(PCI_EXTRACT_BUS(bridgeAddress), getAcpiAddr(), pin, pinRes)))
        {
            kprintf("[PCI ]: Unable to get pin info on bus %X for device %X and pin %u\n", PCI_EXTRACT_BUS(bridgeAddress), PCI_EXTRACT_DEVICE(bridgeAddress), pin);
            return false;
        }
        else
            bridgePins.insert(pinRes);
    }
    return true;
}
