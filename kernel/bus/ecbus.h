#ifndef EC_BUS_H_
#define EC_BUS_H_

#include <task.h>
#include <device.h>
#include <bus/register.h>
#include <templates/fifoq.cc>
#include <acpi/acpidev.h>
#include <templates/vector.cc>

#define EC_STATUS_SMI_EVENT             0x40
#define EC_STATUS_SCI_EVENT             0x20
#define EC_STATUS_BURST                 0x10
#define EC_STATUS_CMD                   0x08
#define EC_STATUS_IBF                   0x02
#define EC_STATUS_OBF                   0x01

#define EC_CMD_READ                     0x80
#define EC_CMD_WRITE                    0x81
#define EC_CMD_BURST_ENABLE             0x82
#define EC_CMD_BURST_DISABLE            0x83
#define EC_CMD_QUERY                    0x84

//Command status flags
#define EC_CMD_STATUS_QUERY             0x80
#define EC_CMD_STATUS_COMPLETE          0x40

#define EC_CMD_STATUS_WRITE_ADDR        0x02
#define EC_CMD_STATUS_READY             0x01

struct ecInitInfo
{
	//! ACPICA device handle to a GPE block, or null if the gpe number in one of the default GPE blocks
	ACPI_HANDLE gpeBlock;
	//! GPE Index
	uint32_t gpeNumber;
	//! Parent device
	Device *parent;
	//! Hardware resources for the EC
	Vector<systemResource> *ecResources;
};

//! Class representing a handler for an EC presented query value
class ecQueryHandler
{
	public:
		ecQueryHandler(uint8_t val, ACPI_HANDLE obj, ACPI_OBJECT_TYPE objType);
		bool operator ==(const ecQueryHandler &obj) const;
		bool operator ==(const uint8_t value) const;
		bool operator >(const ecQueryHandler &obj) const;
		bool operator >(const uint8_t value) const;
		bool operator <(const ecQueryHandler &obj) const;
		bool operator <(const uint8_t value) const;

		void execute() const;
	private:
		const ACPI_HANDLE handle;
		const ACPI_OBJECT_TYPE handleType;
		const uint8_t number;
};

//! Class representing a queued embedded controller command
class ecCommand : public Event
{
	public:
	    //ecCommand() = default;
		ecCommand(uint8_t addr, uint8_t cmd, uint8_t data, Task *requestTask);
		bool trigger(const uint32_t expectedGeneration);
		uint8_t commandByte, addressByte, dataByte;
};

//! Class representing an ACPI Compatible embedded controller device
class embeddedController : public acpiDevice
{
	public:
		embeddedController(const char *fullPath);
		embeddedController(ACPI_HANDLE devPtr, const char *fullPath);
		virtual ~embeddedController();
		virtual bool init(void *context);

		static embeddedController *installEc(ACPI_HANDLE ptr, Device *parentObj);
		//Asynchronous command processing
		void doCommandProcess(void);
		void commandFinish(void);
		bool hasWork(void) const;

		//Bus read and write commands
		uint8_t issueRead(uint8_t address);
		void issueWrite(uint8_t address, uint8_t value);
		void issueQuery(void);
		uint8_t getStatus(void) const;

		//ECDT related functions
		void parentAttach(Device *p);
	private:
		void startCommand(void);
		void completeCurrent(void);
		void dispatchCommand(ecCommand *cmd);
		void handleQuery(uint8_t val);

		ticketLock controllerLock;
		uint32_t globalLock;
		systemRegister *command, *data;
		avlTree<const ecQueryHandler> queryHandlers;
		fifoQueue<ecCommand *> commandQueue;
		ecCommand *currentCommand;
		uint8_t commandStatus;
};

ACPI_STATUS ecDeviceFound(ACPI_HANDLE dev, UINT32 Level, void *context, void **returnValue);
ACPI_STATUS ecAccessHandler(uint32_t Function, ACPI_PHYSICAL_ADDRESS Address, uint32_t BitWidth, uint64_t *Value, void *HandlerContext, void *RegionContext);
ACPI_STATUS ecAddressSpaceHandler(ACPI_HANDLE Region, uint32_t Function, void *HandlerContext, void **ReturnContext);

#endif
