#ifndef PCI_BRIDGE_H_
#define PCI_BRIDGE_H_

#include "pci_const.h"
#include "pci_pin.h"
#include <device.h>
#include <acpi/acpisys.h>
#include <templates/vector.cc>
#include <templates/avltree.cc>

class pciBridge
{
    public:
        struct bridgeLinkInfo
        {
            Vector<class pci2PciBridge *> *bridges;
            int currentIndex;
        };

        pciBridge(const uint8_t baseBus) : bridgeAddress(0), busProvided(baseBus) {};
        static ACPI_STATUS pciMatchToBridge(ACPI_HANDLE Dev, uint32_t level, void *context, void **returnValue);


        pci2PciBridge *makePciBridge(Vector<class pci2PciBridge *> &bridges, uint32_t baseAddress, Device *parent, const uint8_t secondaryBus, const uint8_t subBus);

        uint32_t getBridgeAddress(void) const
        {
            return bridgeAddress;
        }

        uint8_t getBus(void) const
        {
            return PCI_EXTRACT_BUS(bridgeAddress);
        }

        uint8_t getDevice(void) const
        {
            return PCI_EXTRACT_DEVICE(bridgeAddress);
        }

        uint8_t getFunction(void) const
        {
            return PCI_EXTRACT_FUNCTION(bridgeAddress);
        }

        bool getPinRoute(const uint8_t targetBus, const uint32_t addr, const uint8_t pin, systemResource &res) const;
    protected:
        struct bridgeInitInfo
        {
            pciBridge *parentBridge;
            void *busRange;
        };

        bool findDriver(Device *parent, const uint32_t address);
        bool makeVendorIdKey(uint64_t &key, const uint32_t address);
        bool getClassCode(uint64_t &code, const uint32_t address);
        bool processRoutingTable(ACPI_HANDLE &bridgeHandle);

        Vector<systemResource> bridgePins;
        Map<regionObj32, class pci2PciBridge *> bridgeMap;
        avlTree<pciPinMap> pinMappings;
        uint32_t bridgeAddress;
        uint8_t busProvided;
};

class pci2PciBridge : public Device, public pciBridge
{
    public:
        struct pciBridgeInit
        {
            uint32_t address;
            ACPI_HANDLE bridgeHandle;
        };

        pci2PciBridge(const uint32_t address, Device *p, const uint8_t secondaryBusInit, const uint8_t subBusInit);
        virtual bool init(void *context);

        uint32_t getAcpiAddr(void) const
        {
            uint32_t acpiAddr = PCI_EXTRACT_DEVICE(bridgeAddress);
            acpiAddr <<= 16;
            acpiAddr |= PCI_EXTRACT_FUNCTION(bridgeAddress);
            return acpiAddr;
        }

        uint8_t getSecondaryBus(void) const
        {
            return secondaryBus;
        }

        uint8_t getSubBus(void) const
        {
            return subordinateBus;
        }

        void setAcpiHandle(ACPI_HANDLE newHandle)
        {
            handle = newHandle;
        }

        ACPI_HANDLE getAcpiHandle(void)
        {
            return handle;
        }


    protected:
        Vector<pci2PciBridge *> bridges;
        ACPI_HANDLE handle;
        uint8_t secondaryBus, subordinateBus;
};

#endif // PCI_BRIDGE_H_
