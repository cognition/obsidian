#include "pci.h"
#include "io.h"
#include "pci_bridge.h"

#include <driverapi/dbase.h>
#include <acpi/acpisys.h>
#include <templates/avltree.cc>
#include <templates/map.cc>
#include <memory/memory.h>
#include <io/io.h>
#include <bios32.h>

extern "C"
{
    #include <driverapi/dapi_include/dapi_types.h>
    #include "acpi/local_include/acpi.h"
}

#define PCI_BIOS_PRESENT        0xB101U

#define PCI_IO_BASE             0xCF8U
#define PCI_IO_LENGTH           0x8U

#define PCI_IO_ADDRESS          0xCF8U
#define PCI_IO_DATA             0xCFCU

#define PCI_ENABLE              0x80000000U

#define PCI_PRESENT_SIGNATURE   0x20494350U

#define EFLAGS_CARRY            0x1U

/* Driver ID key method values */
#define PCI_DRIVER_KEY_TYPE_CLASS             0x0001
#define PCI_DRIVER_KEY_TYPE_VENDORDEV         0x0002

static avlTree<pciBusRange *, pointerContentsComparable<pciBusRange> > busRanges;

static uint32_t rootBusCount = 0;

namespace pciBus {

/** \brief Registers a more traditional I/O based PCI interface
 *
 * \param p Device* Parent device, usually the motherboard
 * \return bool True if a PCI interface was found and enabled, false otherwise
 *
 */
bool registerTraditional( Device *p)
{
    bios32service *pciService = bios32::requestService(BIOS32_PCI_STRING);
    if(pciService)
    {
        regs32 pciInstallCheck;
        pciInstallCheck.eax = PCI_BIOS_PRESENT;
        pciService->execute(&pciInstallCheck);

        if( (pciInstallCheck.eflags & EFLAGS_CARRY) || (pciInstallCheck.eax & 0xFF00) || (pciInstallCheck.edx != PCI_PRESENT_SIGNATURE) )
            return false;
        else
        {
            uint8_t maxBus = pciInstallCheck.ecx & 0xFF;
            if(!maxBus)
                maxBus = 0xFFU;
            pciBusRange *range = new pciBusRange( maxBus );
            pciBusInit rangeInit = {p, 0};
            if( !range->init(&rangeInit) )
                delete range;
            else
            {
                busRanges.insert( range );
                kprintf(" PCI: Found PCI BIOS with for Segment 0 Buses: 0-%u\n", maxBus);
                return true;
            }
        }
    }
    return false;
}

/** \brief Registers a PCI Configuration space MMIO access region
 *
 * \param p Device* Parent device, usually a motherboard
 * \param base uint64_t Base physical memory address provided by MCFG table
 * \param segment uint16_t PCI Segment group
 * \param busStart uint8_t PCI Bus base number covered by this MMIO region
 * \param busEnd uint8_t PCI Bus end number covered by this MMIO region
 * \return bool True if region was successfully initialized, false if it failed
 */
bool registerMmioRange( Device *p, uint64_t base, uint16_t segment, uint8_t busStart, uint8_t busEnd )
{
	pciBusInit mmioInit = {p, base};
	pciBusRange *range = new pciBusRange( segment, busStart, busEnd );

	if( !range->init( &mmioInit ) )
	{
		delete range;
		return false;
	}
	else
	{
		busRanges.insert( range );
		uintptr_t length = ( busEnd - busStart ) + 1;
		length <<= 20;
		kprintf( " PCI: Found PCI Bus MMIO Configuration space @ %X-%X\n", base, ( base+length-1 ) );
		kprintf( "\tSegment: %u Buses: %u-%u\n", segment, busStart, busEnd );
		return true;
	}
}

//! \note PCI Configuration space read functions, should not be used by most drivers

/*! Reads a byte from the PCI Configuration Space of a device
 *  \param address Native address tag of the device
 *  \param offset Register offset of the field to be read
 *  \param data Return reference for the byte that was read
 *  \return True if the address and offset was valid, false otherwise
 */
bool readByte( uint32_t address, uint32_t offset, uint8_t &data )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->readByte( address, offset, data );
	}
	else
		return false;
}

/*! Reads a word from the PCI Configuration Space of a device
 *  \param address Native address tag of the device
 *  \param offset Register offset of the field to be read
 *  \param data Return reference for the word that was read
 *  \return True if the address and offset was valid, false otherwise
 */
bool readWord( uint32_t address, uint32_t offset, uint16_t &data )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->readWord(address, offset, data );
	}
	else
		return false;
}

/*! Reads a doubleword from the PCI Configuration Space of a device
 *  \param address Native address tag of the device
 *  \param offset Register offset of the field to be read
 *  \param data Return reference for the doubleword that was read
 *  \return True if the address and offset was valid, false otherwise
 */
bool readDWord( uint32_t address, uint32_t offset, uint32_t &data )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->readDWord(address, offset, data );
	}
	else
		return false;
}

/*! Reads a quadword from the PCI Configuration Space of a device
 *  \param address Native address tag of the device
 *  \param offset Register offset of the field to be read
 *  \param data Return reference for the quadword that was read
 *  \return True if the address and offset was valid, false otherwise
 */
bool readQWord( uint32_t address, uint32_t offset, uint64_t &data )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->readQWord(address, offset, data );
	}
	else
		return false;
}

//PCI Configuration space write functions, should not be used by most drivers

/*! Writes a byte to the PCI Configuration Space of a specific device
 *  \param address Native address tag of the device
 *  \param offset Offset of the register to be written
 *  \param value Byte value to be write to the target register
 *  \return True if the addess and offset values were valid, false otherwise
 */
bool writeByte( uint32_t address, uint32_t offset, uint8_t value )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->writeByte(address, offset, value );
	}
	else
		return false;
}

/*! Writes a word to the PCI Configuration Space of a specific device
 *  \param address Native address tag of the device
 *  \param offset Offset of the register to be written
 *  \param value Word value to be write to the target register
 *  \return True if the addess and offset values were valid, false otherwise
 */
bool writeWord( uint32_t address, uint32_t offset, uint16_t value )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->writeWord(address, offset, value );
	}
	else
		return false;
}

/*! Writes a doubleword to the PCI Configuration Space of a specific device
 *  \param address Native address tag of the device
 *  \param offset Offset of the register to be written
 *  \param value Doubleword value to be write to the target register
 *  \return True if the addess and offset values were valid, false otherwise
 */
bool writeDWord( uint32_t address, uint32_t offset, uint32_t value )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->writeDWord(address, offset, value );
	}
	else
		return false;
}

/*! Writes a quadword to the PCI Configuration Space of a specific device
 *  \param address Native address tag of the device
 *  \param offset Offset of the register to be written
 *  \param value Quadword value to be write to the target register
 *  \return True if the addess and offset values were valid, false otherwise
 */
bool writeQWord( uint32_t address, uint32_t offset, uint64_t value )
{
	if(pciBusRange **rangePtr = busRanges.find(address))
	{
        const pciBusRange *range = *rangePtr;
		return range->writeQWord(address, offset, value );
	}
	else
		return false;
}

/**! \brief Enumerates a PCI Root Bridge
 *
 * \param rootBridgeAddress const uint32_t Condensed PCI address of the host bridge
 * \param bridgeHandle ACPI_HANDLE Handle to the ACPI
 * \return bool True if the bus was enumerated successfully, false if an error was encountered
 *
 */
bool enumeratePciRootBus(const uint32_t rootBridgeAddress, ACPI_HANDLE bridgeHandle)
{
    if( pciBusRange **rangePtr = busRanges.find(rootBridgeAddress) )
    {
        pciBusRange *range = *rangePtr;
        return range->enumerate(rootBridgeAddress, bridgeHandle);
    }
    else
    {
        //TODO: Attempt to initialize from the bridge handle object
        kprintf("Non-initialized bus range found!\n");
        return false;
    }
}

/*! Reads the value of device's base address register
 *  \param base Return reference containing the base address
 *  \param length Return reference containing the length of the region
 *  \param flags Return reference containing the flags of the BAR
 *  \param address Device address
 *  \param barNum Base address register being requested
 */
bool readBAR(uint64_t &base, uint64_t &length, uint32_t &flags, const uint32_t address, const uint32_t barNum)
{
    uint32_t barBase;
    flags = 0;
    if(barNum > 5)
        return false;

    uint16_t control;
    if(!readWord(address, PCI_CONFIG_COMMAND, control))
        return false;
    //Disable I/O and memory decodings since we'll be writing to the BARs
    if(!writeWord(address, PCI_CONFIG_COMMAND, control & ~(PCI_COMMAND_DECODE_IO | PCI_COMMAND_DECODE_MEM)))
        return false;

    if( !readDWord(address, PCI_BAR_BASE + 4*barNum, barBase) )
        return false;

    if(barBase & PCI_BAR_IO_SPACE)
    {
        flags |= PCI_BAR_IO_SPACE;
        uint32_t barLength;
        base = barBase & ~0x3U;
        // Retrieve length by writing all ones to the BAR and reading back the value
        if(!writeDWord(address, PCI_BAR_BASE + 4*barNum, 0xFFFFFFFFU))
            return false;
        if( !readDWord(address, PCI_BAR_BASE + 4*barNum, barLength) )
            return false;
        //Restore the BAR value
        if (! writeDWord(address, PCI_BAR_BASE + 4*barNum, barBase) )
            return false;
        /* PCI Specification says to read back the register after the all ones write, any zeros returned will be "don't care" values
           negating the register should specify the length of the resource region, unless it is zero in which case the register is unused
        */
        if(barLength)
            length = ~barLength + 1;
        else
            length = 0;

        writeWord(address, PCI_CONFIG_COMMAND, control);

        return true;
    }
    else if(((barBase >> 1) &0x3U) == 0x2)
    {
        //64-bit memory region
        flags |= PCI_BAR_64BIT;
        uint32_t upperAddr, upperLength, lowerLength;
        if(!readDWord(address, PCI_BAR_BASE + 4*barNum+4, upperAddr))
        {
            return false;
        }
        base = upperAddr & ~0xFU;
        base <<= 32;
        base |= barBase & ~0xFU;

        writeDWord(address, PCI_BAR_BASE + 4*barNum+4, 0xFFFFFFFFU);
        /* Really just here so static analysis tools don't complain
         * TODO: Probably worth having an address specific config space object just to avoid the constant lookups
         */
        if( !readDWord(address, PCI_BAR_BASE + 4*barNum+4, upperLength) )
            return false;
        writeDWord(address, PCI_BAR_BASE + 4*barNum+4, upperAddr);

        writeDWord(address, PCI_BAR_BASE + 4*barNum, 0xFFFFFFFFU);
        // Same as the last one
        if( !readDWord(address, PCI_BAR_BASE + 4*barNum, lowerLength) )
            return false;
        writeDWord(address, PCI_BAR_BASE + 4*barNum, barBase);

        length = (!upperLength) ? 0:~upperLength;
        if(length)
            length++;
        length <<= 32;
        length |= !lowerLength ? 0:(~lowerLength + 1);

        writeWord(address, PCI_CONFIG_COMMAND, control);

        return true;
    }
    else if(((barBase >> 1) & 0x3U) == 0x00)
    {
        //32-bit memory region
        uint32_t barLength;
        base = barBase & ~0xFU;
        // Retrieve length by writing all ones to the BAR and reading back the value
        if(!writeDWord(address, PCI_BAR_BASE + 4*barNum, 0xFFFFFFFFU))
            return false;
        if( !readDWord(address, PCI_BAR_BASE + 4*barNum, barLength) )
            return false;
        //Restore the BAR value
        if (! writeDWord(address, PCI_BAR_BASE + 4*barNum, barBase) )
            return false;
        /* PCI Specification says to read back the register after the all ones write, any zeros returned will be "don't care" values
           negating the register should specify the length of the resource region, unless it is zero in which case the register is unused
        */
        if(barLength)
            length = ~barLength + 1;
        else
            length = 0;

        writeWord(address, PCI_CONFIG_COMMAND, control);

        return true;
    }
    else
        return false;
}

void getDevResources(const uint32_t addr, Vector<systemResource> &devRes)
{
    uint64_t base, length;
    uint32_t flags;
    for(int bar = 0; bar < PCI_BAR_COUNT; bar++)
    {
        flags = 0;
        if(!readBAR(base, length, flags, addr, bar))
            continue;
        if(!(base | length) )
            continue;

        //kprintf("BAR[%u] - Base: %X Length: %X Flags: %X\n", bar, base, length, flags);

        if(flags & PCI_BAR_IO_SPACE)
        {
            systemResource io(systemResourceIo, base, length);
            devRes.insert(io);
        }
        else
        {
            systemResource mem(systemResourceMemory, base, length);
            devRes.insert(mem);
            if(flags & PCI_BAR_64BIT)
                bar++;
        }
    }

    //Get the interrupt pin
    if(pciBusRange * const *rangePtr = busRanges.find( addr ) )
    {
        const pciBusRange *range = *rangePtr;

        uint8_t pinIndex;
        range->readByte( addr, PCI_CONFIG_INT_PIN, pinIndex );
        systemResource pinResource;

        //PCI records the pin value as A=1, B=2,etc... while ACPI records it as A=0, B=2, etc..
        pinIndex--;
        uint32_t acpiAddr = PCI_EXTRACT_DEVICE(addr);
        acpiAddr <<= 16;
        acpiAddr |= PCI_EXTRACT_FUNCTION(addr);

        if(range->getPinRoute(PCI_EXTRACT_BUS(addr), acpiAddr, pinIndex, pinResource))
            devRes.insert(pinResource);
    }
}

uint8_t getCapabilityOffset(const uint32_t address, const uint8_t capabilityId)
{
    uint16_t capHeader, statusValue;
    uint8_t currentCapId, capOffset, nextCap;

    if(!readWord(address, PCI_CONFIG_STATUS, statusValue ))
        return 0;
    if(!(statusValue & PCI_STATUS_CAPLIST))
        return 0;

    if(!readByte(address, PCI_CONFIG_CAP_LIST, capOffset))
        return 0;

    do
    {
        capOffset &= 0xFC;
        if(!readWord(address, capOffset, capHeader))
        {
            kprintf("PCI Read word failed!\n");

            return 0;
        }

        currentCapId = capHeader & 0xFFU;
        nextCap = capHeader>>8;
        kprintf("[PCI]: Found capability %u at offset %X Next @ %X\n", currentCapId, capOffset, nextCap);
        if(currentCapId == capabilityId)
            return capOffset;
        else
            capOffset = nextCap;

    } while(capOffset);

    return capOffset;
}

}

//pciBusRange class

/** \brief Junk initializer
 */
pciBusRange::pciBusRange() : pciBridge(0), Device( String("PCI Root Bus(Junk)")), isMmio(false)
{
    regBase = 0;
    regLength = 0;
}

/*! Constructor for a PCI Bus with a MMIO based access mechanism
 *  \param segment Segment of this configuration region
 *  \param startBus Lowest bus addressable through this configuration mechanism
 *  \param endBus Highest bus addressable through this configuration mechanism
 */
pciBusRange::pciBusRange( uint16_t segment, uint8_t startBus, uint8_t endBus ) : pciBridge(startBus), Device( "PCI Root Bus" ), isMmio( true )
{
	regBase = startBus;
	regBase <<= 8;
	regLength = endBus;
	regLength++;
	regLength <<=8;
	regLength -= regBase;
	regBase |= segment<<16;
	rootBusCount++;
}


/*! Constructor for a PCI with a legacy I/O based access mechanism
 *  \param endBus Highest bus addressable through this mechanism
 */
pciBusRange::pciBusRange(uint8_t endBus) : pciBridge(0), regionObj64(0, (endBus+1)<<8 ), Device("PCI Root Bus"), isMmio(false), virtBase(0)
{
    regBase = 0;
    regLength = (endBus+1)<<8;
    rootBusCount++;
}

/*! Tests if a given device is a PCI to PCI bridge or not
 *  \param classCode PCI Class code of the device
 *  \param subClass PCI Subclass code
 *  \param progInterface PCI Programming interface
 *  \param headerType PCI Header Type field
 *  \return True if the device conforms to the PCI to PCI Bridge architecture specification, false otherwise
 */
bool pciBusRange::isPci2PciBridge(const uint8_t classCode, const uint8_t subClass, const uint8_t progInterface, const uint8_t headerType)
{
    if(classCode != PCI_CC_BRIDGE)
        return false;
    if(subClass != PCI_BRIDGE_SUBCLASS_PCI2PCI)
        return false;
    if(progInterface > 1)
        return false;
    if( (headerType & 0x7F) != PCI_HEADER_BRIDGE)
        return false;

    return true;
}

/*! Enumerates all the functions of a specific device
 *  \param deviceAddress Address of the device with multiple functions
 *  \param primaryFunction Function that has already been enumerated
 *  \return True if the functions were enumerated successfully, false otherwise
 */
bool pciBusRange::enumerateFunctions(Vector<pci2PciBridge *> &bridges, const uint32_t deviceAddress, Device *busParent, const int primaryFunction)
{
    uint32_t baseAddress = deviceAddress & ~PCI_ADDR_FUNCTION_MASK;
    const uint8_t currentBus = PCI_EXTRACT_BUS(baseAddress);

    for(uint32_t i = 0; i < 8; i++, baseAddress++)
    {
        //Step through all unenumerated sub functions
        if(i == primaryFunction)
            continue;


        uint16_t vendorId;
        if(!readWord(baseAddress, PCI_CONFIG_VENDOR_ID, vendorId))
            return false;
        //Check that the subfuction is present
        if(vendorId != PCI_DEV_NOT_PRESENT)
        {
            //Extract identifying information
            uint8_t classCode, subClass, progInterface, headerType;

            if(!readByte(baseAddress, PCI_CONFIG_CLASS_CODE, classCode))
                return false;
            if(!readByte(baseAddress, PCI_CONFIG_SUBCLASS, subClass))
                return false;
            if(!readByte(baseAddress, PCI_CONFIG_PROGIF, progInterface))
                return false;
            if(!readByte(baseAddress, PCI_CONFIG_HEADER_TYPE, headerType))
                return false;

            //Test if the device is a bridge to another PCI Bus
            if(isPci2PciBridge(classCode, subClass, progInterface, headerType))
            {
                uint8_t subBus, secondaryBus;

                //kprintf("Function @ %u:%u:%u is a PCI to PCI Bridge device.\n", currentBus, PCI_EXTRACT_DEVICE(baseAddress), i);
                if(!readByte(baseAddress, PCI_CONFIG_SUBBUS, subBus))
                    return false;
                if(!readByte(baseAddress, PCI_CONFIG_SECONDARY_BUS, secondaryBus))
                    return false;
                if(secondaryBus != currentBus)
                {
                    makePciBridge(bridges, baseAddress, busParent, secondaryBus, subBus);
                }
            }
            else
            {
                /*
                kprintf("Found PCI Device @ Bus: %u Device: %u Function: %u CC: %X SC: %X PI: %X\n", PCI_EXTRACT_BUS(baseAddress),
                    PCI_EXTRACT_DEVICE(baseAddress), PCI_EXTRACT_FUNCTION(baseAddress), classCode, subClass, progInterface);
                */
                findDriver(busParent, baseAddress);

            }

        }
    }
    return true;
}

/** \brief Attempts to enumerate all devices on a provided bus and any devices bridges on this bus
 *
 * \param bus const uint8_t Target bus
 * \param maxBus const uint8_t Highest potential bus behind this one
 * \param baseDevice const int Base device on this bus, only applicable is a known host bus exists
 * \param busParent Parent device of the bus
 * \return True if devices were enumerated, false if an error was encountered
 */
bool pciBusRange::enumerateDevices(Vector<pci2PciBridge *> &bridges, const uint8_t bus, const uint8_t maxBus, Device *busParent, const int baseDevice)
{
    uint32_t baseAddress = bus;
    baseAddress <<= 8;
    baseAddress |= PCI_MASK_SEG(  getBase() );
    for(uint8_t device = 0; device < PCI_DEVICE_LIMIT; device++, baseAddress += PCI_ADDR_DEVICE_GRAN)
    {
        if(device == baseDevice)
            continue;

        uint16_t vendorId;
        if(!readWord(baseAddress, PCI_CONFIG_VENDOR_ID, vendorId))
            return false;
        if(vendorId != PCI_DEV_NOT_PRESENT)
        {
            uint8_t classCode, subClass, progInterface, headerType;

            if(!readByte(baseAddress, PCI_CONFIG_CLASS_CODE, classCode))
                return false;
            if(!readByte(baseAddress, PCI_CONFIG_SUBCLASS, subClass))
                return false;
            if(!readByte(baseAddress, PCI_CONFIG_PROGIF, progInterface))
                return false;
            if(!readByte(baseAddress, PCI_CONFIG_HEADER_TYPE, headerType))
                return false;

            /*
            kprintf("Found PCI Device @ Bus: %u Device: %u CC: %X SC: %X PI: %X %s\n", PCI_EXTRACT_BUS(baseAddress), PCI_EXTRACT_DEVICE(baseAddress),
                    classCode, subClass, progInterface, headerType & PCI_HEADER_MULTIFUNCTION ? "with multiple functions":"");
            */

            if(headerType & PCI_HEADER_MULTIFUNCTION)
            {
                if(!enumerateFunctions(bridges, baseAddress, busParent))
                    return false;
            }

            if(isPci2PciBridge(classCode, subClass, progInterface, headerType))
            {

                //kprintf("Device @ BUS: %u Device: %u is a PCI to PCI Bridge device.\n", PCI_EXTRACT_BUS(baseAddress), PCI_EXTRACT_DEVICE(baseAddress));
                uint8_t subBus, secondaryBus;

                if(!readByte(baseAddress, PCI_CONFIG_SUBBUS, subBus))
                    return false;
                if(!readByte(baseAddress, PCI_CONFIG_SECONDARY_BUS, secondaryBus))
                    return false;
                if(bus != secondaryBus)
                {
                    makePciBridge(bridges, baseAddress, busParent, secondaryBus, subBus);
                }
            }
            else
                findDriver(busParent, baseAddress);
        }
    }
    return true;
}

static ACPI_STATUS doNothing(ACPI_HANDLE Dev, uint32_t level, void *context, void **returnValue)
{
    return AE_OK;
}

bool pciBusRange::enumerateBus(pciBridge *parentBridge, Device *parentDev, ACPI_HANDLE parentHandle, const uint8_t busNum, const uint8_t subBus)
{
    Vector<pci2PciBridge *> bridges;

    if(!enumerateDevices(bridges, busNum, subBus, parentDev ))
        return false;
    if(bridges.length() != 0)
    {
        ACPI_STATUS status;
        pciBridge::bridgeLinkInfo bridgeLinkContext = {&bridges, 0};
        void *retVal;

        status = AcpiWalkNamespace(ACPI_TYPE_DEVICE, parentHandle, 1, &pciBridge::pciMatchToBridge, &doNothing, &bridgeLinkContext, &retVal);
        if(ACPI_FAILURE(status))
        {
            kprintf("[ACPI]: Namespace walk of root bridge failed! Error: %s\n", AcpiFormatException(status));
            return false;
        }

        bridgeInitInfo bInit = {parentBridge, this};
        for(int bridgeIndex = 0; bridgeIndex < bridges.length(); bridgeIndex++ )
        {
            pci2PciBridge *currentBridge = bridges[bridgeIndex];
            if(!currentBridge->init(&bInit) )
            {
                kprintf("[PCI ]: Error initializing PCI-to-PCI Bridge @ %X:%X:%X\n", currentBridge->getBus(), currentBridge->getDevice(), currentBridge->getFunction() );
                return false;
            }
            if(!enumerateBus(currentBridge, currentBridge, currentBridge->getAcpiHandle(), currentBridge->getSecondaryBus(), currentBridge->getSubBus()))
            {
                kprintf("[PCI ]: Error failed to enumerate PCI-to-PCI bridge @ %X:%X:%X\n", currentBridge->getBus(), currentBridge->getDevice(), currentBridge->getFunction());
                return false;
            }
        }

    }

    return true;
}

static void pciRangeAcpiNsDelete(ACPI_HANDLE handle, void *data)
{

}

/** \brief Enumerates the bus behind this pciBusRange object
 *
 * \param rootBridgeAddress const uint32_t Reported address of the host bridge on this device
 * \param bridgeHandle ACPI_HANDLE Handle to bridge device in the ACPI namespace
 * \return bool True if the host bridge was found and the bus was enumerated, false otherwise
 *
 * \note Some host bridges might not be visible through the PCI configuration space,
 *   in that case enumeration is attempted from the base bus.
 */
bool pciBusRange::enumerate(const uint32_t rootBridgeAddress, ACPI_HANDLE bridgeHandle)
{
    uint8_t classCode, subClass, progInterface, headerType, subBus, secondaryBus;
    uint16_t vendorId;

    bridgeAddress = rootBridgeAddress;
    if(!readWord(rootBridgeAddress, PCI_CONFIG_VENDOR_ID, vendorId))
        return false;

    kprintf("Attempting to enumerate PCI bus rooted @ %X\n", rootBridgeAddress);

    if(vendorId != 0xFFFF)
    {
        // Read configuration information
        if(!readByte(rootBridgeAddress, PCI_CONFIG_CLASS_CODE, classCode))
            return false;
        if(!readByte(rootBridgeAddress, PCI_CONFIG_SUBCLASS, subClass))
            return false;
        if(!readByte(rootBridgeAddress, PCI_CONFIG_PROGIF, progInterface))
            return false;
        if(!readByte(rootBridgeAddress, PCI_CONFIG_HEADER_TYPE, headerType))
            return false;

        // Check that the root bridge conforms to a host bridge device
        if(classCode != PCI_CC_BRIDGE)
        {
            kprintf("Class code mismatch.  Found %X Expected: %X\n", classCode, PCI_CC_BRIDGE);
            return false;
        }
        if(subClass != PCI_BRIDGE_SUBCLASS_HOST)
        {
            kprintf("Subclass mismatch.  Found %X Expected: %X\n", subClass, PCI_BRIDGE_SUBCLASS_HOST);

            return false;
        }
        if(progInterface > 1)
        {
            kprintf("Program interface invalid!\n");
            return false;
        }
        // Get the bridge header topology fields
        if(!readByte(rootBridgeAddress, PCI_CONFIG_SUBBUS, subBus))
            return false;
        if(!readByte(rootBridgeAddress, PCI_CONFIG_SECONDARY_BUS, secondaryBus))
            return false;

    }
    else
    {
        // A host bridge device was not present at the given address, enumerate the entirety of this bus range instead
        secondaryBus = PCI_EXTRACT_BUS(regBase);
        subBus = PCI_EXTRACT_BUS(regBase + regLength);
    }

    if(!processRoutingTable(bridgeHandle))
    {

        kprintf("Unable to process routing table!\n");
        return false;
    }
    else
    {
        kprintf("[PCI ]: Root bridge has %u interrupt pin mappings\n", pinMappings.size() );
    }

    // Get the resources for the root/host bridge from the ACPI namespace
    Vector<systemResource> rootResources;
    if(!acpiServices::getDeviceResources(bridgeHandle, rootResources))
        return false;

    if(!acquireResources(rootResources))
    {
        kprintf("[PCI ]: Failed to acquire root bridge resources.\n");
        return false;
    }



    ACPI_BUFFER rootBridgeName = {ACPI_ALLOCATE_BUFFER, NULL};

    AcpiGetName(bridgeHandle, ACPI_FULL_PATHNAME, &rootBridgeName);
    kprintf("ACPI: Root bridge path - %s\n", rootBridgeName.Pointer);
    AcpiOsFree(rootBridgeName.Pointer);

    AcpiAttachData(bridgeHandle, &pciRangeAcpiNsDelete, static_cast <Device *> (this));

    return enumerateBus(this, this, bridgeHandle, regBase, regBase + regLength - 1);
}

bool pciBusRange::init( void *context )
{
	const pciBusInit *rangeInit = reinterpret_cast <const pciBusInit *> (context);
	registerParent( rangeInit->parent );

	if( isMmio )
	{
		uintptr_t memBase = rangeInit->address;
		uintptr_t memLength = regLength*4096;
		systemResource mem( systemResourceMemory, memBase, memLength );

		//Acquire memory and PCI Address range resources
		if( !acquireResource( mem ) )
			return false;
		//Map MMIO Configuration space into virtual memory
		virtBase = virtualMemoryManager::mapRegion( memBase, memLength, PAGE_PRESENT | PAGE_WRITE | PAGE_CACHE_DISABLE);
		return true;
	}
	else
	{
	    // Reserve the legacy I/O ports and PCI Buses
	    systemResource io( systemResourceIo, PCI_IO_BASE, PCI_IO_LENGTH);
	    if( !acquireResource(io) )
            return false;
        else if( !acquireResource( systemResource(systemResourcePci, regBase, regLength) ) )
            return false;
		return true;
	}
}

bool pciBusRange::readByte( const uint32_t address, const uint32_t reg, uint8_t &data ) const
{
	uint32_t dword;
	uint8_t shiftVal = reg & 0x3;
	shiftVal *= 8;
	if( !readDWord( address, reg & ~0x3, dword ) )
		return false;
	else
	{
	    dword >>= shiftVal;
		data = dword & 0xFFU;
		return true;
	}
}

bool pciBusRange::readWord( const uint32_t address, const uint32_t reg, uint16_t &data ) const
{
	if( reg & 0x1 )
    	{

        	kprintf("[PCI ]: Invalid offset for read word[%X]\n", reg);
		return false;
    	}
	else
	{
		uint32_t dword;
		uint8_t shiftVal = reg & 0x2;
		shiftVal *= 8;
		if( !readDWord( address, reg & ~0x3, dword ) )
			return false;
		else
		{
			data = ( dword >> shiftVal ) & 0xFFFF;
			return true;
		}
	}
}

bool pciBusRange::readDWord( const uint32_t address, const uint32_t reg, uint32_t &data ) const
{
	if( reg & 0x3 )
    	{

        	kprintf("[PCI ]: Invalid offset for read dword[%X]\n", reg);
		return false;
    	}
	else if( isMmio )
	{
	    // Calculate offset into the MMIO region
		uintptr_t offset = address - regBase;
		offset <<= 12;
		offset += reg;
		// Read the value from memory
		volatile uint32_t *ptr = reinterpret_cast <volatile uint32_t *> ( virtBase + offset );

        data = *ptr;
        return true;
	}
	else if( reg > 252 )
		return false;
	else
	{
		uint32_t addressDWord = address;
		addressDWord <<= 8;
	    addressDWord |= reg | PCI_ENABLE;
	    ioBus::writeDWord(PCI_IO_ADDRESS, addressDWord );
		data = ioBus::readDWord(PCI_IO_DATA);
		return true;
	}
}

bool pciBusRange::readQWord( const uint32_t address, const uint32_t reg, uint64_t &data )const
{
	uint32_t lowD, highD;
	if( reg & 0x3 )
    	{
        kprintf("[PCI ]: Invalid offset for read qword[%X]\n", reg);
		return false;
    	}
    	else if( isMmio )
	{
	    	// Calculate offset into the MMIO region
		uintptr_t offset = address - regBase;
		offset <<= 12;
		offset += reg;
		// Read the value from memory
		volatile uint64_t *ptr = reinterpret_cast <volatile uint64_t *> ( virtBase + offset );

        	data = *ptr;
        	return true;
	}
	else if( !readDWord( address, reg, lowD ) || !readDWord( address, reg+4, highD ) )
		return false;
	else
	{
		data = highD;
		data <<= 32;
		data |= lowD;
		return true;
	}
}

bool pciBusRange::writeByte( uint32_t address, const uint32_t reg, uint8_t value ) const
{
	uint32_t dword, regAlign;
	regAlign = reg & ~0x3;
	if( !readDWord( address, regAlign, dword ) )
		return false;
	else
	{
	    	uint32_t vOut = value;
		uint8_t shiftVal = reg & 0x3;
		shiftVal *= 8;
		uint32_t mask = 0xFFU;
		mask <<= shiftVal;
		mask = ~mask;
		dword &= mask;
		vOut <<= shiftVal;
		dword |= vOut;
		return writeDWord( address, regAlign, dword );
	}
}

bool pciBusRange::writeWord( uint32_t address, const uint32_t reg, uint16_t value ) const
{
	uint32_t dword, regAlign;
	regAlign = reg & ~0x3;
	if( reg & 0x1 )
		return false;
	else if( !readDWord( address, regAlign, dword ) )
		return false;
	else
	{
	    	uint32_t vOut = value;
		uint8_t shiftVal = reg & 0x2;
		shiftVal *= 8;
		uint32_t mask = 0xFFFFU;
		mask <<= shiftVal;
		mask = ~mask;
		dword &= mask;
		vOut <<= shiftVal;
		dword |= vOut;
		return writeDWord( address, regAlign, dword );
	}
}

bool pciBusRange::writeDWord( uint32_t address, uint32_t reg, uint32_t value ) const
{
	if( reg & 0x3 )
		return false;
	else if( isMmio )
	{
		uint64_t offset = address - regBase;
		offset <<= 12;
		offset += reg;

		volatile uint32_t *ptr = reinterpret_cast <volatile uint32_t *> ( virtBase + offset );
		*ptr = value;

		return true;
	}
	else if( reg > 252 )
		return false;
	else
	{
	    uint32_t addressDWord = address << 8;
	    addressDWord |= reg | PCI_ENABLE;
		ioBus::writeDWord(PCI_IO_ADDRESS, addressDWord );
		ioBus::writeDWord(PCI_IO_DATA, value);
		return true;
	}
}

bool pciBusRange::writeQWord( uint32_t address, uint32_t reg, uint64_t value ) const
{
	uint32_t lowDWord, highDWord;

	lowDWord = value;
	highDWord = value>>32;

	if( reg & 0x3 )
		return false;
	else if( !writeDWord( address, reg, lowDWord ) || !writeDWord( address, reg+4, highDWord ) )
		return false;
	else
		return true;
}

bool pciBus::loadDriverDb(const String &path)
{
    driverOperationResult op_res;

    op_res = driverDatabase::coordLoadDriverDatabase(BUS_ID_PCI, path);
    if(op_res != driverOpSuccess)
    {
        kprintf("Unable to load PCI driver database! Code: %u\n", op_res);
        return false;
    }
    else
        return true;
}

bool pciBridge::makeVendorIdKey(uint64_t &key, const uint32_t address)
{
    uint8_t byteValue;
    uint16_t wordValue;

    if(!pciBus::readWord(address, PCI_CONFIG_VENDOR_ID, wordValue))
        return false;

    key = PCI_DRIVER_KEY_TYPE_VENDORDEV;
    key <<= 16;
    key |= wordValue;
    key <<= 16;
    if(!pciBus::readWord(address, PCI_CONFIG_DEVICE_ID, wordValue))
        return false;
    key |= wordValue;
    if(!pciBus::readByte(address, PCI_CONFIG_PROGIF, byteValue))
        return false;
    key <<= 16;
    key |= byteValue;
    return true;
}

bool pciBridge::getClassCode(uint64_t &code, const uint32_t address)
{
    uint8_t readValue;

    if(!pciBus::readByte(address, PCI_CONFIG_CLASS_CODE, readValue))
        return false;

    code = PCI_DRIVER_KEY_TYPE_CLASS;
    code <<= 16;
    code |= readValue;

    if(!pciBus::readByte(address, PCI_CONFIG_SUBCLASS, readValue))
        return false;
    code <<= 16;
    code |= readValue;

    if(!pciBus::readByte(address, PCI_CONFIG_PROGIF, readValue))
        return false;
    code <<= 16;
    code |= readValue;

    return true;
}

ACPI_STATUS pciBridge::pciMatchToBridge(ACPI_HANDLE Dev, uint32_t level, void *context, void **ReturnValue)
{
    //TODO: Should change link info to contain a list of entries
    bridgeLinkInfo *linkInfo = reinterpret_cast <bridgeLinkInfo *> (context);

    ACPI_DEVICE_INFO *devInfo;

    ACPI_STATUS status = AcpiGetObjectInfo(Dev, &devInfo);
    if(ACPI_FAILURE(status) )
    {
        ACPI_BUFFER nameBuffer = {ACPI_ALLOCATE_BUFFER, NULL};
        AcpiGetName(Dev, ACPI_FULL_PATHNAME, &nameBuffer);

        kprintf("Error retrieving device info for device: %s Error: %s\n", nameBuffer.Pointer, AcpiFormatException(status));
        ACPI_FREE(nameBuffer.Pointer);
        return status;
    }

    if(devInfo->Valid & ACPI_VALID_ADR)
    {
        for(int bIndex = 0; bIndex < linkInfo->bridges->length(); bIndex++ )
        {
            pci2PciBridge *currentBridge = linkInfo->bridges->at(bIndex);
            if(currentBridge->getAcpiAddr() == devInfo->Address)
            {
                currentBridge->setAcpiHandle(Dev);
                linkInfo->currentIndex++;
                if(linkInfo->currentIndex == linkInfo->bridges->length())
                {
                    ACPI_FREE(devInfo);
                    return AE_CTRL_TERMINATE;
                }
                break;
            }
        }
    }

    ACPI_FREE(devInfo);

    return AE_OK;
}
