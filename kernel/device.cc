#include "device.h"
#include <io/io.h>
#include <cpu/lock.h>

//resourceLease class

/*! Constructs a new resourceLease object
 *  \param receiver Device which recieves the resource in the lease contract
 *  \param holder Device which loans the resource in the lease contract
 *  \param r The resource being leased
 */
resourceLease::resourceLease( class Device &receiver, class Device &holder, systemResource &r ) : leasee( receiver ), lessor( holder ), res( r )
{
//	kprintf("Created new resource lease: %p\n", this);
//	asm ("xchg %%bx, %%bx\n"::);
}

//! Destructor, cleans up the lease and returns resources to the original holder
resourceLease::~resourceLease()
{
	Device *rec = &leasee;
	Device *hold = &lessor;
	if(!rec)
	{
		kprintf("Reciever is null! Caller: %p\n", __builtin_return_address(0));
		asm ("xchg %%bx, %%bx\n"::);
	}
	else if(!hold)
	{
		kprintf("Holder is null! Caller: %p\n", __builtin_return_address(0));
		asm ("xchg %%bx, %%bx\n"::);
	}
	lessor.recieveResource( res );
}

//Device class

//! Constructor for root node device


Device::Device( const String &s ) : refSpaceObj(s, rscHardwareDev, rsBusAddress, nullptr), name( s ), resources(), parent( NULL )
{
}

Device::Device( const String &fullDeviceName, const String &refSpaceId) : refSpaceObj( refSpaceId, rscHardwareDev, rsBusAddress), name(fullDeviceName), resources(), parent(nullptr)
{

}

Device::Device( const String &fullDeviceName, const String &refSpaceId, class refSpaceObj *parentRef) : refSpaceObj( refSpaceId, rscHardwareDev, rsBusAddress, parentRef),
    name(fullDeviceName), resources(), parent(nullptr)
{
}


/*! Constructor for subordinate devices, with a name initializer
 *  \param n String describing the name of the device
 *  \param p Parent device of the device we're constructing
 */
Device::Device( const String &fullDeviceName, const String &refSpaceId, class Device *p ) : refSpaceObj( refSpaceId, rscHardwareDev, rsBusAddress, p), name( fullDeviceName ), resources(), parent( p )
{
	parent->addChild( this );
}

//! Destructor
Device::~Device()
{
	kprintf("Destroying device: %s\n", name.toBuffer() );
	leases.destroyAll();
	children.destroyAll();
}

/*! Call which adds a resource to a device
 *  \param res Resource being given to the device
 *  \note This implementation is really incomplete at the moment
 *  \todo Actually implement some real logic here
 */
void Device::recieveResource( systemResource &res )
{
	spinLockInstance lock( &devLock );
	systemResource *resPtr = nullptr;

	if(res.getBase())
    {
        //Attempt to find adjacent region before the resource range being added
        systemResource pre(res.getType(), res.getBase() - 1UL, 1);
        resPtr = resources.find(pre);
        //If found merge the regions
        if(resPtr)
            resPtr->extend( res.getLength() );
    }

    if(!resPtr)
        resPtr = &resources.insert(res);

    if(res.getBase() + res.getLength() )
    {
        //Attempt to find adjacent region trailing the resource range being added
        systemResource tail;
        systemResource post(res.getType(), res.getBase() + res.getLength(), 1);
        //If found merge regions
        if(resources.retrieve(post, tail))
            resPtr->extend( tail.getLength() );
    }
}

/*! Attempts to lease a resource from a device
 *  \param dev Device which is requesting the resource
 *  \param res Resource being requested
 *  \return True if the lease request was successful, false otherwise
 */
bool Device::leaseResource( Device *dev, systemResource &res )
{
	systemResource handle;
	spinLockInstance lock( &devLock );
	//Attempt to retrieve the resource from the resource tree of the device, abort if unsuccessful
	if( !resources.retrieve( res, handle ) )
    {
        kprintf("[DEV ]: Parent \"%s\"could not find resource %s\n", name.toBuffer(), res.toString().toBuffer());
        for(auto iter = resources.front(); !iter.isEnd(); iter++)
        {
            systemResource pRes = *iter;
            kprintf("[DEV ]: Parent has resource %s\n", pRes.toString().toBuffer());
        }
        asm volatile ("cli\n"
                      "hlt\n"::);
		return false;
    }
	//Make sure the device is claiming just the resource it requested
	handle.partition( resources, res );
	//Create and insert the lease in both devices
	resourceLease *lease = new resourceLease( *dev, *this, res );
	//leases.insert( lease );
	dev->leases.insert( lease );
	dev->recieveResource(res);
	return true;
}

/*! Attempts to acquire a resource
 *  \param res Resource to acquire
 *  \return True if successful, false otherwise
 */
bool Device::acquireResource( systemResource res )
{
	if(!parent)
	{
	    kprintf("Failed to acquire resource %s for device %s because it lacked a parent device!\n", res.toString().toBuffer(), name.toBuffer() );
		return false;
	}
	else if((res.getType() == systemResourceInterrupt) || (res.getType() == systemResourceIrq))
	{
	    resources.insert(res);
	    return true;
	}
    else
    {
        systemResource *entry = resources.find(res);
        //Check if there's an existing collision
	    if(entry)
        {
            //Check if the resource is already owned
            if( !entry->encases(res) )
            {
                //Check if the desired resource encompasses the existing one instead
                if(res.encases(*entry))
                {
                    //Remove the existing resource and attempt to acquire the newly requested range instead
                    systemResource removed;
                    resources.retrieve(*entry, removed);

                    for(int i = 0; i < leases.length(); i++)
                    {
                        resourceLease *contract = leases[i];
                        if(contract->involves(removed) )
                        {
                            leases.remove(i);
                            delete contract;
                            break;
                        }
                    }
                }
                else
                {
                    //Resource ranges an incompatible and we have a conflict
                    return false;
                }
            }
            else
            {
                //Existing resource already encompasses the requested one
                return true;
            }
        }
		return parent->leaseResource( this, res );
    }
}

/*! Attempts to acquire a collection of resources
 * \param resources Vector of target resources
 * \return True if successful, false otherwise
 */
bool Device::acquireResources( Vector<systemResource> &requestedResources)
{
   for(int resIndex = 0; resIndex < requestedResources.length(); resIndex++)
   {
        systemResource &res = requestedResources[resIndex];
        if(!acquireResource(res))
        {
            kprintf("Failed to acquire resource %s from parent device!\n", res.toString().toBuffer() );
            return false;
        }
   }

   return true;
}

/*! Adds a child device to a Device object
 *  \param d Pointer to the child device
 */
void Device::addChild( Device *d )
{
	spinLockInstance lock( &devLock );
	children.insert( d );
}

/*! Registers a parent device
 *  \param d Pointer to the parent
 */
void Device::registerParent( Device *p )
{
	spinLockInstance lock( &devLock );
	parent = p;
	p->addChild( this );
}

/*! Finds a resource matching a requested one, if the device owns it
 * \param target Requested resource range
 * \return A pointer to the first matching resource located or null if no matching resource was found
 */
systemResource* Device::getMatchingResource(const systemResource& target)
{
    return resources.find(target);
}
