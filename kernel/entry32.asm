[BITS 32]
[SECTION .text]

EXTERN _start, _roStart, _dataStart, _bssStart, _end
EXTERN _start_ctors, _end_ctors, _start_dtors, _end_dtors
EXTERN loadIDT, kernelMain, apKernMain, schedWaitCurrent, kprintf
EXTERN __cxa_finalize, mainIDTR, ApL4
GLOBAL entry32, kernApSetup64, entry64, apInitInfo, apActiveFlag, apEarlyFlag, masterLDT, bspGDT, bspStack, bspStackEnd, bspTss, callFar32
GLOBAL bootFb, bootGlyphs, taskWaitCurrent, apTempGdtr

%ifdef IDT_TEST
EXTERN loadTestIDT
%endif

%include "asmdefs.inc"
%include "cpu_state.inc"

%define BSP_STACK_SIZE       16384
%define BSP_IST_SIZE         16384

%define B32_STACK_SIZE       4096
%define PHYS_VIRT_DELTA      (0xFFFF800000000000 - 0x200000)

call32_compat_entry:

    mov eax, [esp+12]
    mov ebx, [esp+16]
    mov ecx, [esp+20]
    mov edx, [esp+24]
    mov edi, [esp+28]
    mov esi, [esp+32]

    mov ds, word [esp+8]
    mov es, word [esp+10]

    call far dword [esp]

    mov [esp+12], eax
    mov [esp+16], ebx
    mov [esp+20], ecx
    mov [esp+24], edx
    mov [esp+28], edi
    mov [esp+32], esi

    lahf
    movzx eax, ah
    mov [esp+36], eax

    mov ax, KERNEL_DATA_SEG
    mov ds, ax
    mov es, ax

    jmp far dword [esp+40]

[BITS 64]
[DEFAULT REL]

kernApSetup64:
    mov rax, kernApBridge64
    jmp rax


kernApBridge64:

    lgdt [rel apInitInfo.gdtr]                  ; Load the GDTR with this BSP's GDT

    mov ax, KERNEL_DATA_SEG                     ; Reload data segments
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax



    mov rsp, [rel apInitInfo.stack]             ; Setup the AP's stack


    mov rdx, [rel apInitInfo.kernGs]            ; Setup the GS register base
    mov eax, edx
    shr rdx, 32
    mov ecx, MSR_KERN_GS_BASE
    wrmsr
    mov ecx, MSR_GS_BASE
    wrmsr



    mov ax, KERNEL_TSS_SEL                      ; Load the TSS
    ltr ax
    mov ax, KERNEL_LDT_SEL                      ; Load the LDT
    lldt ax

    lidt [rel mainIDTR]                         ; Load the IDT

    mov rdi, [rel apInitInfo.apicBase]          ; Pass the processor's physical APIC address as a parameter

    mov byte al, 1
    lock xchg byte [rel apActiveFlag], al         ; Signal to the IPI processor that the processor is active

    call apKernMain

    cli
    hlt



enterLongMode:
    mov rax, entry64

    push KERNEL_CODE_64                 ; Push a value for our CS selector onto the stack
    push rax                            ; Push our higher half address onto the stack
    o64 retf                            ; Far return

entry64:
    mov cr3, rbx
    mov rsp, bspStack+BSP_STACK_SIZE    ; Setup the stack

    mov rbx, bspGdtr
    lgdt [rbx]                          ; Reload the GDT

    mov ax, KERNEL_DATA_SEG             ; Update the data segment pointers
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    mov rbx, cr4
    or rbx, 0x200
    mov cr4, rbx

%ifdef IDT_TEST
    call loadTestIDT
%endif

    sub rsp, 8
    mov [rsp+4], dword KERNEL_CODE_SEG
    mov eax, PADJUST(.csUpdate)
    mov [rsp], eax
    o32 retf

.csUpdate:
    mov rax, .csUpdate2
    jmp rax
.csUpdate2:
    mov rax, cr3
    mov [rel ApL4], rax					; Save the kernel's virtual address pointer for AP startup

    push rdi                            ; Save the address of the bootInfo structure
    push rsi                            ; Save the highest physical address used in for virtual memory mapping

    add rdi, 42                         ; Initialize the bootup frame buffer and glyph data structure
    mov [rel bootFb], rdi
    add rdi, 52
    mov [rel bootGlyphs], rdi

.loadTSS:
    lea rax, [rel bspTss]
    lea rdi, [rel bspGDT.bspTSS_Selector]
    mov [rdi+2], ax                     ; Point the TSS descriptor to the TSS
    shr rax, 16
    mov [rdi+4], al
    mov [rdi+7], ah
    shr rax, 16
    mov [rdi+8], eax


    mov ax, KERNEL_TSS_SEL
    ltr ax                              ; Load the TSS selector

.loadLDT:
    lea rax, [rel masterLDT]
    lea rdi, [rel bspGDT.masterLDT]
    mov [rdi+2], ax
    shr rax, 16
    mov [rdi+4], al
    mov [rdi+7], ah
    shr rax, 16
    mov [rdi+8], eax
    mov ax, KERNEL_LDT_SEL

    lldt ax

    lea rdi, [rel masterLDT]
    mov ecx, 128
    mov eax, 1
.createFreeList:
    stosq
    inc eax
    dec ecx
    jnz .createFreeList
    xor rax, rax
    mov [rdi-8], rax

.loadIDT:

    call loadIDT                        ; Load the IDT

    lea rdi, [rel _start_ctors]
    lea rsi, [rel _end_ctors]

    finit

.executeCtors:                          ; Call any global constructors
    cmp rdi, rsi
    je .doMain

    push rdi
    push rsi

    mov rax, [rdi]
    call rax

    pop rsi
    pop rdi
    add rdi, 8
    jmp .executeCtors

.doMain:

    pop rsi
    pop rdi


    call kernelMain                     ; Enter our high level kernel code

    call __cxa_finalize

    lea rsi, [rel _start_dtors]
    lea rdi, [rel _end_dtors]

.executeDtors:                          ; Call any global destructors
    sub rdi, 8
    cmp rdi, rsi
    jl .systemHalt
    push rdi
    push rsi

    mov rax, [rdi]
    call rax

    pop rsi
    pop rdi
    jmp .executeDtors


.systemHalt:
    cli
    hlt

; This is pretty much just a wrapper that saves the state information and preps the stack for a later IRET via Task::execute and isrReturn
taskWaitCurrent:

; Adjust the stack down so there's space for the SS and RSP values for the later IRET
sub rsp, 8
; Save the flags and CS for the IRET
pushf
cli
push qword KERNEL_CODE_SEG
; Save all our GP registers
sub rsp, CS_SAVE
mov [rsp+RDX_SAVE], rdx
mov [rsp+RCX_SAVE], rcx
mov [rsp+RBX_SAVE], rbx
mov [rsp+RAX_SAVE], rax
mov [rsp+RSI_SAVE], rsi
mov [rsp+RDI_SAVE], rdi
mov [rsp+RBP_SAVE], rbp

mov [rsp+R15_SAVE], r15
mov [rsp+R14_SAVE], r14
mov [rsp+R13_SAVE], r13
mov [rsp+R12_SAVE], r12
mov [rsp+R11_SAVE], r11
mov [rsp+R10_SAVE], r10
mov [rsp+R9_SAVE], r9
mov [rsp+R8_SAVE], r8

mov rdx, KERNEL_DATA_SEG
xchg [rsp+SS_AUTOSAVE], rdx
mov [rsp+RIP_SAVE], rdx
mov rdx, rsp
add rdx, (SS_AUTOSAVE+8)
mov [rsp+RSP_AUTOSAVE], rdx

; Save the segment registers
mov dx, gs
mov [rsp+GS_SAVE], dx
mov dx, fs
mov [rsp+FS_SAVE], dx
mov dx, es
mov [rsp+ES_SAVE], dx
mov dx, ds
mov [rsp+DS_SAVE], dx

; Set the third argument to the schedWaitCurrent function to the saved registers on the stack
mov rdx, rsp

jmp schedWaitCurrent


callFar32:
    swapgs
    push rbp
    push rax
    push rbx
    push rcx
    push rdx
    push rsi
    push rdi
    mov [rel lmStackSave], rsp
    mov rbx, PHYS_VIRT_DELTA

    lea rsp, [rel bios32Stack]
    lea rax, [rel .returnTo64]
    sub rax, rbx
    add rsp, B32_STACK_SIZE
    sub rsp, rbx

    push rax
    mov eax, KERNEL_CODE_64
    mov [rsp+4], eax
    sub rsp, 40
    mov rsi, rdi
    mov rdi, rsp
    mov rcx, 5
    rep movsq

	push qword 0x18
    lea rax, [rel call32_compat_entry]
    sub rax, rbx
    push rax
    o64 retf

.returnTo64:
    mov rax, .returnToHH
    jmp rax
.returnToHH:
    mov rsp, [rel lmStackSave]

    mov rdi, [rsp]
    lea rsi, [rel bios32Stack]
    add rsi, (B32_STACK_SIZE - 48)
    mov rcx, 5
    rep movsq

    pop rdi
    pop rsi
    pop rdx
    pop rcx
    pop rbx
    pop rax
    pop rbp
    swapgs
    ret

[section .data]

bspTss:
    dd  0
    dq (bspRing0 + BSP_STACK_SIZE - 8), (bspRing0 + BSP_STACK_SIZE - 8), (bspRing0 + BSP_STACK_SIZE - 8)
    dq 0, (bspIst1 + BSP_IST_SIZE - 8), (bspIst2 + BSP_IST_SIZE - 8), (bspIst3 + BSP_IST_SIZE - 8), (bspIst4 + BSP_IST_SIZE - 8)
    dq (bspIst5 + BSP_IST_SIZE - 8), (bspIst6 + BSP_IST_SIZE - 8), (bspIst7 + BSP_IST_SIZE - 8), 0
    dw 0, 0
bspGDT:
    dq 0
.kernelCode64:
    dw 0xFFFF, 0
    db 0, 0x9A, 0xAF, 0
.kernelData:
    dw 0xFFFF, 0
    db 0, 0x92, 0xCF, 0
.kernelCode32:
    dw 0xFFFF, 0
    db 0, 0x9A, 0xCF, 0
.userCode32:
    dw 0xFFFF, 0
    db 0, 0xFA, 0xCF, 0
.userData:
    dw 0xFFFF, 0
    db 0, 0xF2, 0xCF, 0
.userCode64:
    dw 0xFFFF, 0
    db 0, 0xFA, 0xAF, 0
.userData64:
    dw 0xFFFF, 0
    db 0, 0xF2, 0xCF, 0
.masterLDT:
    dw 0x1000, 0
    db 0, 0x82, 0, 0
    dq 0
.bspTSS_Selector:
    dd bspGDT-bspTss
    db 0, 0x89, 0, 0
    dq 0
endBSPGDT:

bspGdtr:
    dw  (endBSPGDT-bspGDT)
    dq  bspGDT

apTempGdtr:
    dw (endBSPGDT-bspGDT)
    dq PADJUST(bspGDT)

updateJump:
    dw KERNEL_CODE_64
    dq PADJUST(entry64.csUpdate)


[section .bss]

bootFb          resq     1
bootGlyphs      resq     1
bspRing0 resq   (BSP_STACK_SIZE/8)

nxMask  resq    1

apEarlyFlag resb 1

alignb  4096

bspStack resq   (BSP_STACK_SIZE/8)
bspStackEnd:

masterLDT       resq    256
bspIst1         resq    (BSP_IST_SIZE/8)
bspIst2         resq    (BSP_IST_SIZE/8)
bspIst3         resq    (BSP_IST_SIZE/8)
bspIst4         resq    (BSP_IST_SIZE/8)
bspIst5         resq    (BSP_IST_SIZE/8)
bspIst6         resq    (BSP_IST_SIZE/8)
bspIst7         resq    (BSP_IST_SIZE/8)

bios32Stack     resd    1024

lmStackSave     resq    1

apActiveFlag    resb   1

alignb 8
apInitInfo:
    .gdtr       resb    10
    .kernGs     resq    1
    .apicBase   resq    1
    .stack      resq    1
