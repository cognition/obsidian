#ifndef BOOT_LOADER_H_
#define BOOT_LOADER_H_

#include "compiler.h"

//Memory map entry types
#define MMAP_RANGE_RAM              0x1
#define MMAP_RANGE_RESERVED         0x2
#define MMAP_RANGE_ACPI_RECLAIM     0x3
#define MMAP_RANGE_ACPI_NVS         0x4
#define MMAP_RANGE_UNUSABLE         0x5
#define MAMP_RANGE_DISABLED         0x6

//Potential flags for represented by the memory map's extended attribute field
#define MMAP_EXT_ATTR_VALID         0x1
#define MMAP_EXT_ATTR_NV            0x2
#define MMAP_EXT_ATTR_SLOW          0x4
#define MMAP_EXT_ATTR_ERROR_LOG     0x8

//! Structure describing a Int 15/AX = E820h memory map entry
struct COMPILER_PACKED memoryMap
{
	//! Size of this memory map entry
	uint32_t size;
	//! Base address of the region
	uint64_t base;
	//! Length of the region
	uint64_t length;
	//! Entry type
	uint32_t type;
	//! Extra attributes describing region
	uint32_t extendedAttributes;

};

// EDD Interface path structs
//! Structure describing the interface path field for a legacy I/O controller
struct COMPILER_PACKED eddLegacyInterfacePath
{
	//! I/O Port address of the controller
	uint16_t baseAddress;
	//! Space reserved by specification
	uint16_t _reserved[3];
};

//! Structure describing the interface path field for a PCI controller
struct COMPILER_PACKED eddPciInterfacePath
{
	//! Bus portion of the PCI address
	uint8_t bus;
	//! Device portion of the PCI address
	uint8_t slot;
	//! Function portion of the PCI address
	uint8_t function;
	//! Space reserved by specification
	uint8_t _reserved[5];
};

//! Union describing the interface path field
union eddInterfacePath
{
	//! Legacy I/O Interface information
	eddLegacyInterfacePath legacy;
	//! PCI Bus device information
	eddPciInterfacePath pci;
};


// EDD Device path values
//! Structure which describes device path information for ATA drives
struct COMPILER_PACKED eddAtaDevicePath
{
	//! Master or slave field, 0 = master, 1 = slave
	uint8_t masterSlave;
	//! Space reserved by specification
	uint8_t _reserved[7];
};

//! Structure which describes device path information for ATAPI drives
struct COMPILER_PACKED eddAtapiDevicePath
{
	//! Master or slave field, 0 = master, 1 = slave
	uint8_t masterSlave;
	//! Logical unit number of the device
	uint8_t lun;
	//! Space reserved by specification
	uint8_t _reserved[6];
};

//! Structure which describes device path information for SCSI drives
struct COMPILER_PACKED eddScsiDevicePath
{
	//! Logical unit number of this device on the controller
	uint8_t lun;
	//! Space reserved by specification
	uint8_t _reserved[7];
};

//! Structure which describes device path information for USB drives
struct COMPILER_PACKED eddUsbDevicePath
{
	//! Number of the device on this controller
	uint8_t deviceNumber;
	//! Space reserved by specification
	uint8_t _reserved[7];
};

//! Structure which describes device path information for FireWire drives
struct COMPILER_PACKED eddFireWireDevicePath
{
	//! GUID
	uint64_t guid;
};

//! Structure which describes the device path information for a fibre channel drive
struct COMPILER_PACKED eddFibreDevicePath
{
	//! World wide number, or something to that effect
	uint64_t wwn;
};

//! Union describing information potentially stored in the devicepath field
union eddDevicePath
{
	//! ATA Device path information
	eddAtaDevicePath ata;
	//! ATAPI Device path information
	eddAtapiDevicePath atapi;
	//! SCSI Device path information
	eddScsiDevicePath scsi;
	//! USB Device path information
	eddUsbDevicePath usb;
	//! Firewire Device path information
	eddFireWireDevicePath firewire;
	//! FIBRE Channel device path information
	eddFibreDevicePath fibre;
};

//! EDD Drive information structure
struct COMPILER_PACKED eddDriveInfo
{
	//! Size of the buffer containing this structure
	uint16_t bufferLength;
	//! Information flags describing the drive's features
	uint16_t infoFlags;
	//! Cylinder count, if applicable
	uint32_t cylinders;
	//! Head count, if applicable
	uint16_t heads;
	//! Sectors per track
	uint16_t sectorsPerTrack;
	//! Number of sectors
	uint64_t numSectors;
	//! Sector size
	uint16_t bytesPerSector;
	//! \note bootloader does NOT currently save this information
	uint32_t deviceParameterExtensionPtr;
	//! If a value of 0BEEDh is present then the device patch information following is valid
	uint16_t devicePathKey;
	//! Length of the device path
	uint8_t devicePathLength;
	//! Reserved
	uint8_t _reserved[3];
	//! Host bus identifier
	uint32_t hostBus;
	//! Interface type, e.g ATA, ATAPI, etc.
	uint64_t interfaceType;
	//! Describes the host bus specific location of the drive controller
	uint64_t interfacePath;
	//! Describes the interface specific location of the drive itself
	uint64_t devicePath;
	//! Another reserved value...
	uint8_t _reserved2;
	//! Checksum value
	uint8_t checksum;
};

typedef struct COMPILER_PACKED
{
    //! Frame buffer virtual address, 0
	uint64_t fbVirtAddr;
    //! Back buffer virtual address, 8
	uint64_t bbVirtAddr;
	//! FB Dimensions, 16
	uint32_t width, height, bytesPerPixel, pixPerScanLine;
	//! FB Total memory size, 32
	uint32_t totalSize;
	//! Color masks, 36
	uint32_t redMask, greenMask, blueMask, rsvdMask;
	//! 52 bytes in total length
} frameBufferInfo;

typedef struct COMPILER_PACKED
{
	union
	{
		uint8_t *pixelBuffer;
		uint32_t *dwordBuffer;
	} pixels;
	uint32_t charWidth, charHeight;
	uint8_t bytesPerPixel;
} glyphBuffer;


//! Information structure passed to the kernel by the bootloader
struct COMPILER_PACKED bootLoaderInfo
{
	//! Physical address of the initial ramdisk, 0
	uint32_t initrdAddress;
	//! Size of the initial ramdisk, 4
	uint32_t initrdLength;
	//! Pointer to the start of the memory map, 8
	memoryMap *memoryMapPtr;
	//! Length of the memory map in bytes, 16
	uint16_t memoryMapLength;
	//! Offset to the PNP Bios information structure, 18
	uint16_t pnpOffset;
	//! Segment of the PNP Bios information structure, 20
	uint16_t pnpSeg;
	//! End of physical memory used by kernel, ramdisk and initial paging structures, 22
	uint32_t physEnd;
	//! End of virtual memory required by the kernel and EFI runtime servicesm, 26
 	uint64_t virtEnd;
	//! Physical address of the ACPI RSDP, 34
	uint64_t acpiRSDP;
	//! Frame buffer information, 42
	frameBufferInfo fbInfo;
	//! Font glyphs
	glyphBuffer glyphs;
	//! Bios boot drive number
	uint8_t bootDriveNumber;
};

#endif
