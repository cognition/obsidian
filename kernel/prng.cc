#include <prng.h>
#include <cpu/lock.h>
#include <io/io.h>

/* Xorishiro256++ implementation
 *
 * Code slightly modified from posted version @ http://prng.di.unimi.it/xoshiro256plusplus.c
 */

static uint64_t splitMix64(uint64_t &x)
{
    x += 0x9e3779b97f4a7c15;
    uint64_t z = x;
	z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
	z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
	return z ^ (z >> 31);
}



static ticketLock randLock;
static prngInstance primaryPrng;

prngInstance::prngInstance()
{
    for(int i = 0; i < sizeof(state)/sizeof(uint64_t); i++)
        state[i] = 0;
}

void prngInstance::seed(const uint64_t value)
{
        uint64_t last = value;

        for(int i = 0; i < 4; i++)
        {
            last = state[i] = splitMix64(last);
            //kprintf("SEED[%u] = %X\n", i, state[i]);
        }
}

uint64_t prngInstance::get(void)
{
    const uint64_t result = leftRotate64(state[0] + state[3], 23) + state[0];

    const uint64_t t = state[1] << 17;

    state[2] ^= state[0];
    state[3] ^= state[1];
    state[1] ^= state[2];
    state[0] ^= state[3];

    state[2] ^= t;

    state[3] = leftRotate64(state[3], 45);

    return result;
}

namespace random
{
    void seed(uint64_t value)
    {
        spinLockInstance lock(&randLock);
        primaryPrng.seed(value);
    }


    uint64_t get(void)
    {
       	spinLockInstance lock(&randLock);
        return primaryPrng.get();
    }
};
