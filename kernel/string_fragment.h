/* String fragment header file
 *
 * Author: Joseph Kinzel */

#ifndef STRING_FRAGMENT_H_
#define STRING_FRAGMENT_H_

#include <compiler.h>
#include <string.h>

class stringFragment
{
	public:
		explicit stringFragment(const String &src, short start = 0);
        explicit stringFragment(const String &src, short start, short span);
		explicit stringFragment(const stringFragment &src, short start = 0);
		explicit stringFragment(const stringFragment &src, short start, short fragSpan);

		stringFragment &operator =(const stringFragment &other);
		stringFragment &operator =(stringFragment &&other);
		int compare(const stringFragment &other) const;

		short length() const;
		char operator[](const short index) const;
		void shift(const short start);
        void trim(const short amount);

		const char *toBuffer(void) const;
		const void *getBufferPtr(void) const
		{
		    return dPtr.get();
		}

		static int getNextFragment(stringFragment &frag, const String &str, const int start, const char token);
	private:
		short begin, size;
		shared_ptr < Vector<char> > dPtr;
};

#endif

