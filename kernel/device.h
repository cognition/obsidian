#ifndef DEVICE_H_
#define DEVICE_H_

#include <resources.h>
#include <string.h>
#include <templates/vector.cc>
#include <templates/avltree.cc>
#include <refspace/refspace.h>

//! Object representing the exchange of resources from a device that is able to provide resources to one that needs them
class resourceLease
{
	public:
		resourceLease( class Device &receiver, class Device &holder, systemResource &r );

		bool involves(const systemResource &r) const
		{
		    return res.isExactly(r);
		}

		~resourceLease();

	private:
		//! Device receiving the resource
		class Device &leasee;
		//! Device leasing the resource
		class Device &lessor;
		//! Copy of the resource being leased
		systemResource res;
};

/*! Describes a basic hardware device that uses raw system resources and provides functions that include: \n
 *  \n
 *  Management of resources amongst devices. \n
 *  Device hierarchy and resource inheritance. \n
 */
class Device : public refSpaceObj
{
	public:
		Device( const String &nameString );
		Device( const String &fullDeviceName, const String &refSpaceId);
		Device( const String &fullDeviceName, const String &refSpaceId, class refSpaceObj *parentRef);
		Device( const String &fullDeviceName, const String &refSpaceId, class Device *p );
		virtual ~Device();
		//! Pure virtual function, used to call device specific initialization routines.
		virtual bool init( void *context) = 0;
		bool leaseResource( class Device *dev, systemResource &res );
		void recieveResource( systemResource &res );
	protected:
		bool acquireResource( systemResource res );
        bool acquireResources( Vector<systemResource> &resources);

		void addChild( Device *d );
		void registerParent( Device *p );
        systemResource *getMatchingResource(const systemResource &target);

		//! \b String object which denotes the device's name/identification
		String name;
		//! Vector containing resources owned by the device
		avlTree <systemResource> resources;

	private:
		Device();
		//! Vector which contains pointers to any leases this device actively posses
		Vector <resourceLease *> leases;
		//! Vector which contains the any devices directly subordinate to it
		Vector <Device *> children;
		//! Pointer to the parent device
		class Device *parent;
		ticketLock devLock;
		friend class baseBoard;
		friend class resourceLease;
};

#endif

