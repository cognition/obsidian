#ifndef IOSTREAM_H_
#define IOSTREAM_H_

#include <string.h>
#include "textoutdev.h"


//! Object wrapper that specifies a stream operation
class streamOperationObj
{
	public:
		streamOperationObj( const streamOp op, const uint32_t v );

		//! Operation to preform on an \b ioStream object
		const streamOp operation;
		//! Value to pass along with operation command
		const uint32_t value;
};

enum numberFormat
{
    numberFormatUnsignedDecimal,
    numberFormatSignedDecimal,
    numberFormatUnsignedHex,
    numberFormatSignHex,
    numberFormatUnsignedOctal,
    numberFormatSignedOctal,
    numberFormatFloat
};

#define STREAM_UPDATE_NUMBER_FORMAT     0x1
#define STREAM_UPDATE_NUMBER_WIDTH      0x2
#define STREAM_UPDATE_NUMBER_PRECISION  0x4
#define STREAM_UPDATE_COLOR             0x8

#define NUMBER_ATTR_ZERO_FILL           0x1
#define NUMBER_ATTR_LEFT_ALIGN          0x2

//! Object wrapper used to describe a formatting update to the stream
class streamFormattingObj
{
	public:
		streamFormattingObj( uint32_t uMask, numberFormat n, uint16_t nwidth, uint16_t fPrecision, uint32_t colorChoice );

		//! Mask that determines which fields will be updated on an ioStream object with the values contained in this object
		uint32_t updateMask;
		//! Number format to be used
		numberFormat nf;
		//! Minimum width in digits of numbers
		uint16_t numberWidth;
		//! Precision of floating point numbers
		uint16_t precision;
		//! Color to use for text
		uint32_t color;
};

//! Object that abstracts an ioDevice object into an ioStream
class ioStream
{
	public:
		ioStream( textOutputDevice &dev, uint32_t colorChoice, uint16_t numWidth, uint16_t fPrecision, numberFormat numFormat );
		class ioStream &operator <<( const char *string );
		class ioStream &operator <<( String &string );
		class ioStream &operator <<( size_t num );
		class ioStream &operator <<( void *address );
		class ioStream &operator <<( const streamOperationObj &o );
		class ioStream &operator <<( const streamFormattingObj &o );
		void vprintf( const char *format, va_list args );
		void putChar( const char c );

		void printUnsignedDecimal( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr );
		void printSignedDecimal32( uint32_t num, uint8_t width, uint8_t precision, const uint32_t attr );
		void printSignedDecimal64( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr );

		void printHexLower( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr );
		void printHexUpper( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr );
		void printOctal( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr );
		void printString( const char *string, uint8_t width, uint8_t precision, const uint32_t attr );

		textOutputDevice *getDevice(void);
	private:
		void printHex( uint64_t num, uint8_t width, uint8_t preicision, const uint32_t attr, const char *lut );
		void printNumber( size_t num );
		void outputNumString( const char *string, uint8_t width, uint8_t precision, uint8_t stringLength, const uint32_t attr );
		void attemptFillChar(char fillChar, uint8_t width, uint8_t stringLength);
		//! Device the ioStream is connected to
		textOutputDevice &device;
		//! Color text will be output in, if applicable
		uint32_t color;
		//! Minimum width of integers output by this stream in digits
		uint16_t numberWidth;
		//! Precision in digits of floating point numbers that will be output by this stream
		uint16_t numberPrecision;
		//! Current format of numbers output by this stream
		numberFormat nf;

};

#endif

