#ifndef IO_H_
#define IO_H_

#include "iostream.h"
#include "ioiface.h"

//Primary output stream
extern ioStream out;

//New line operation object
extern streamOperationObj endl;
extern streamOperationObj streamTab;
extern streamOperationObj streamFlush;

//Number formatting objects
extern const streamFormattingObj hexNumber;
extern const streamFormattingObj decimalNumber;

#endif
