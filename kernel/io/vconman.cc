#include "virt_console.h"
#include "vconman.h"
#include <io/ps2_kb.h>
#include <exec/exec_manager.h>
#include <scheduler.h>

#define VCONS_DEFAULT_COUNT   4

static unique_ptr<String> defaultSessionExec;

struct virtConsolePack
{
    virtConsolePack(textOutputDevice *target, const int tCols, const int tRows) : vcon(new virtConsole(*target, tCols, tRows)),
        stdOutPipe(new fileSession(vcon.get(), fileSession::sessionPipe | fileSession::sessionTerm ) ), stdOut(new fileHandle(stdOutPipe))
    {
        emInstanceStatus status = executableManager::getExecInstance(defaultSessionExec->toBuffer(), exec, sessionTask);
        if(status != emSuccess)
        {
            kprintf("Error loading file instance: %u\n", status);
        }
        else
            kprintf("File instance loaded successfully!\n");

        sessionTask->mapFileHandle(1, stdOut);
        sessionTask->mapFileHandle(2, stdOut);
        sessionTask->mapFileHandle(0, getStdIn() );

        if( Scheduler::registerTask(sessionTask) != schedSuccess)
            kprintf("Unable to schedule task!\n");

    }

    ~virtConsolePack()
    {
    }

    shared_ptr <fileHandle> &getStdIn(void)
    {
        return vcon->getStdIn();
    }

    unique_ptr<virtConsole> vcon;
    shared_ptr<fileSession> stdOutPipe;
    shared_ptr<fileHandle> stdOut;
    Task *sessionTask;
    shared_ptr<elfLoadedInstance> exec;
};

//! A collection of virtual consoles connected to specific instance of a textOutputDevice
struct outputPack
{
    /*! Constructs an output device pack on a given hardware device
     * \param tInit Hardware output device, or a reasonable proxy for one
     * \param tCols Columns displayed by the device
     * \param tRows Rows displayed by the device
     * \param vcount Number of virtual consoles in the pack and proxying the device
     */
    outputPack(textOutputDevice *tInit, const int tCols, const int tRows, const int vcount) :  tOut(tInit), vcons(vcount), rows(tRows), cols(tCols), activeCon(nullptr), activeIndex(0)
    {
        vcons.resize(vcount);
    }

    ~outputPack() = default;

    /*! Gets the virtual console at a given index
     *  \param index Index of the console to retrieve
     *  \return A pointer to the console or a nullptr if no console was found
     */
    virtConsole *getVirtConsole(const unsigned int index)
    {
        if(virtConsolePack *pack = getVirtPack(index))
            return pack->vcon.get();
        else
            return nullptr;
    }

    /*! Gets the virtual console pack for a given index
     * \param index Index of the pack being requested
     * \return A pointer to the virtualConsolePack object for a given object, or nullptr if the index is invalid
     */
    virtConsolePack *getVirtPack(const unsigned int index)
    {
        if(index < vcons.length())
        {
            if(virtConsolePack *pack = vcons[index].get())
                return pack;
            else
            {
                //Lazily allocate new packs for valid indices
                pack = new virtConsolePack(tOut, rows, cols);
                vcons[index] = unique_ptr<virtConsolePack> (pack);
                return pack;
            }
        }
        else
            return nullptr;
    }

    /*! Sets the active console in a pack
     *  \param index Index of the console to set as active
     *  \return True if the requested console could be activated, false if the index value is invalid
     */
    bool setActiveConsole(const unsigned int index)
    {
        if( virtConsole *con = getVirtConsole(index) )
        {
            if(activeCon)
                activeCon->setInactive();
            con->setActive();
            activeIndex = index;
            activeCon = con;
            return true;
        }
        else
        {
            return false;
        }
    }

    textOutputDevice *tOut;
    Vector< unique_ptr<virtConsolePack> > vcons;
    int rows, cols;
    virtConsole *activeCon;
    int activeIndex;
};

static outputPack *primaryOutput = nullptr;

//! Global interface connected to by input and output devices
namespace virtConsoleManager
{
    /*! Registers a hardware output device
     *  \param device Hardware device pointer
     *  \param tCols Number of columns the device is capable of displaying
     *  \param tRows Number of rows the device is capable of displaying
     */
    void addOutputEndpoint(textOutputDevice *device, const int tCols, const int tRows)
    {
        primaryOutput = new outputPack(device, tCols, tRows, VCONS_DEFAULT_COUNT);
    }

    /*! Processes a keypress
     *  \param keyValue Value of the key that was pressed
     *  \param stateMask State mask of modifier keys (CTRL, ALT, SHIFT, etc.)
     */
    void processKey(const uint8_t keyValue, const uint32_t stateMask)
    {
        if(primaryOutput)
        {
            //Console count sanity check
            static_assert((VCONS_DEFAULT_COUNT <= 9) && (VCONS_DEFAULT_COUNT > 0), "Error: VCONS_DEFAULT_COUNT must have a non-zero value less than or equal to 9");

            //Virtual consoles can be switched with CTRL + ALT + number keys
            if(PS2_KB_MOD_ALT(stateMask) && PS2_KB_MOD_CTRL(stateMask))
            {
                if((keyValue >= '0') && (keyValue <= ('0' + VCONS_DEFAULT_COUNT)) )
                {
                    primaryOutput->setActiveConsole(keyValue - '0');
                    return;
                }
            }
            //Forward the character to the actual console
            if(virtConsole *vcon = primaryOutput->activeCon)
                vcon->echoChar(keyValue);
        }
    }

    /*! Accessor for the standard out file handle of the primary text output device's virtual consoles
     *  \param index Index of the virtual console to retrieve the standard out handle for
     *  \param outHandle Return reference to the fileHandle object
     *  \return True if the call succeeded or false if the provided console index was invalid
     */
    bool getVirtConStdOut(const int index, shared_ptr <fileHandle> &outHandle)
    {
        if(primaryOutput)
        {
            if( virtConsolePack *vpack = primaryOutput->getVirtPack(index) )
            {
                outHandle = vpack->stdOut;
                return true;
            }
        }

        return false;
    }

    /*! Accessor for the standard in file handle of the primary text output device's virtual consoles
     *  \param index Index of the virtual console to retrieve the standard in handle for
     *  \param outHandle Return reference to the fileHandle object
     *  \return True if the call succeeded or false if the provided console index was invalid
     */
    bool getVirtConStdIn(const int index, shared_ptr <fileHandle> &outHandle)
    {
        if(primaryOutput)
        {
            if( virtConsolePack *vpack = primaryOutput->getVirtPack(index) )
            {
                outHandle = vpack->getStdIn();
                return true;
            }
        }
        return false;
    }

    /*! Gets the virtual console count for the primary output device
     *  \return Number of virtual consoles associated with the primary output device, or zero if the virtual console system has not yet been initialized
     */
    int getVirtConsoleCount(void)
    {
        if(!primaryOutput)
            return 0;
        else
            return primaryOutput->vcons.length();
    }

    /*! Sets the default executable path for new virtual console instances and initializes the first console
     *  \return True if the virtual console system was activated, false if it has not been initalized
     */
    bool setDefaultSession(const String &exec)
    {
        defaultSessionExec.reset(new String(exec));
        if(primaryOutput)
        {
            primaryOutput->setActiveConsole(0);
            return true;
        }
        else
            return false;
    }
}
