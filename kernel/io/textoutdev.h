#ifndef TEXT_OUT_DEVICE_H_
#define TEXT_OUT_DEVICE_H_

#include <compiler.h>

/*! \enum ioDevResult
 *  \brief Indicates the result of an ioDevice operation
 */
enum ioDevResult
{
    ioApiError,
    ioFeatureError,
    ioHwError,
    ioOutOfRange,
    ioSuccess = 1
};

//! Describes a single stream operation to preform
enum streamOp
{
    streamOpClear,              /*! Clear buffer */
    streamOpNewLine,            /*! New line */
    streamOpCarriageReturn,     /*! Carriage return */
    streamOpTab,                /*! Tab */
    streamOpBackspace,          /*! Deletes a character or spacing */
    streamOpShowCursor,         /*! Show cursor */
    streamOpHideCursor,         /*! Hide cursor */
    streamOpFlush,              /*! Flush the stream */
    streamOpSetColor           /*! Sets the color value */
};

//! Base class which describes a simple I/O device
class textOutputDevice
{
	public:
	    textOutputDevice() = delete;
		virtual ~textOutputDevice();
		ioDevResult seek( uintptr_t offset );

		//! Pure virtual function, should never be called
		virtual ioDevResult writeBuffer( const void *buffer, uintptr_t length ) = 0;

		//! Pure virtual function, should never be called
		virtual ioDevResult preformOp( const streamOp o ) = 0;

		//! Pure virtual function, should never be called
		virtual ioDevResult echoChar(const char value) = 0;

		virtual void getDims(int &rows, int &cols) = 0;

		bool isActive(void) const
		{
            return active;
		}

	protected:
		textOutputDevice( size_t length, size_t offset );
		textOutputDevice(textOutputDevice &other);
		//! Maximum length of the device in bytes
		size_t maxLength;
		//! Current offset of the device in bytes
		size_t currentOffset;
		//! Size of a tab in spaces;
		int tabSize;
        //! Indicates if the device is ready to receive data
        bool active;
};

#endif
