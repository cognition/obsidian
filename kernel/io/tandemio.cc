#include "tandemio.h"

tandemIo::tandemIo(textOutputDevice &first, textOutputDevice &second) : textOutputDevice(0, 0), firstDev(first), secondDev(second)
{
}

tandemIo::~tandemIo()
{
}

ioDevResult tandemIo::writeBuffer( const void *buffer, uintptr_t length )
{
    firstDev.writeBuffer(buffer, length);
    secondDev.writeBuffer(buffer, length);

    return ioSuccess;
}

ioDevResult tandemIo::preformOp( const streamOp o )
{
    firstDev.preformOp(o);
    secondDev.preformOp(o);
    return ioSuccess;
}

ioDevResult tandemIo::echoChar(const char value)
{
    firstDev.echoChar(value);
    secondDev.echoChar(value);

    return ioSuccess;
}

void tandemIo::getDims(int &rows, int &cols)
{
    secondDev.getDims(rows, cols);
}
