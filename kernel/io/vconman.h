
#ifndef VIRT_CONSOLE_MANAGER_H_
#define VIRT_CONSOLE_MANAGER_H_

#include "textoutdev.h"
#include <templates/shared_ptr.cc>
#include <fs/filehandle.h>


namespace virtConsoleManager
{
    void addOutputEndpoint(textOutputDevice *device, const int tCols, const int tRows);
    void processKey(const uint8_t keyValue, const uint32_t stateMask);
    bool getVirtConStdOut(const int index, shared_ptr <fileHandle> &outHandle);
    bool getVirtConStdIn(const int index, shared_ptr <fileHandle> &outHandle);
    int getVirtConsoleCount(void);
    bool setDefaultSession(const String &exec);
}

#endif // VIRT_CONSOLE_MANAGER_H_
