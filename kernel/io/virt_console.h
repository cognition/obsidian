#ifndef VIRT_CONSOLE_H_
#define VIRT_CONSOLE_H_

#include "textoutdev.h"
#include <templates/unique_ptr.cc>
#include <templates/shared_ptr.cc>
#include <fs/filehandle.h>

/*! Virtual console class.
 *  Scope: Both the direct functions of managing virtual console functions as well as some more general line discipline functions.
 *  Interactions:
 *  - Outputs directly to a (presumably hardware) text output device.
 *  - Receives input from a (presumably hardware) text input device.
 *  - Facilitates kernel output in early boot and possibly afterwards
 *  - Provides interface for userspace I/O pipes
 *  - Centralized, but external, focus manager that provides mux/demux between hardware devices and virtual consoles
 */
class virtConsole : public textOutputDevice
{
    public:
        virtConsole(textOutputDevice &outputDev, const int initCharWidth, const int initCharHeight);
        virtual ~virtConsole();

        void setActive(void);
        void setInactive(void);

        virtual ioDevResult writeBuffer( const void *buffer, uintptr_t length );
		virtual ioDevResult preformOp( const streamOp o );
		virtual ioDevResult echoChar(const char value);
        virtual void getDims(int &rows, int &cols);

		shared_ptr<fileHandle> &getStdIn(void);
    private:
        void scrollLines(const int lineCount);
        void initStdIn();
        shared_ptr<fileHandle> &getStdInProducer(void);

        ticketLock vconLock;
        textOutputDevice &outDev;
        unique_ptr<char> textBuffer;
        const int textCols, textRows, maxInputSize;
        int currentRow, currentCol, inputStartOffset;
        shared_ptr<fileSession> stdInConsumer, stdInProducer;
        shared_ptr<fileHandle> inConsumer, inProducer;
};

#endif // VIRT_CONSOLE_H_
