#ifndef IO_IOIFACE_H_
#define IO_IOIFACE_H_

#include <compiler.h>

extern "C"
{
	void kprintf( const char *format, ... );
	void kvprintf( const char *format, va_list args );
}
#endif
