
#ifndef PS2_DRIVER
#define PS2_DRIVER

#include <device.h>
#include <events.h>
#include <templates/vector.cc>


class ps2Controller : public Device
{
    public:
        ps2Controller(Device *parent);
        virtual bool init(void *context);

    private:
        enum ps2TargetPort
        {
            PORT1,
            PORT2
        };

        typedef bool (*ps2Handler)(Vector<uint8_t> &, const ps2TargetPort);


        class ps2WaitEvent : public Event
        {
            public:
                ps2WaitEvent(Task *task, ps2TargetPort port);

                void getDataBytes(void);
                const Vector<uint8_t> &getMessage(void) const;
                void clearMessage(void);
            private:
                Vector<uint8_t> message;
                const COMPILER_UNUSED ps2TargetPort waitPort;
        };

        bool selfTestValidate(Vector<uint8_t> &bytes);

        bool initFirstPortDevice(void);
        bool initSecondPortDevice(void);

        bool processIdentifyResponse(ps2Handler &portHandler, const Vector<uint8_t> &response, const ps2TargetPort port);
        bool waitDevACK(const ps2TargetPort port, Vector<uint8_t> &response);
        void readDataBytes(Vector<uint8_t> &bytes, const uint8_t clearFlags = 0) volatile;
        void waitInputBuffer();
        void flushOutputBuffer();
        uint8_t waitDataByte(void);
        void waitAndWriteData(const uint8_t value);
        void issueCommand(const uint8_t value);
        void writePort2(const uint8_t value);
        void waitResponseBytes(const ps2TargetPort port, Vector<uint8_t> &response);

        void enableFlags(const uint8_t value);
        static int port1Handler(void *ctx, const int index);
        static int port2Handler(void *ctx, const int index);

        //! PS/2 Device handlers
        static bool handleSimpleMouse(Vector<uint8_t> &bytes, const ps2TargetPort port);
        static bool handleScrollMouse(Vector<uint8_t> &bytes, const ps2TargetPort port);
        static bool handle5ButtonMouse(Vector<uint8_t> &bytes, const ps2TargetPort port);

        static bool handleKeyboard(Vector<uint8_t> &bytes, const ps2TargetPort port);

        //! Function pointers
        static bool (*handleP1Device)(Vector<uint8_t> &bytes, const ps2TargetPort port);
        static bool (*handleP2Device)(Vector<uint8_t> &bytes, const ps2TargetPort port);

        ps2WaitEvent *port1Wait, *port2Wait;
        Event port1Event, port2Event;
        volatile uint32_t flags;

};

#endif
