#include "ps2.h"
#include "ps2_kb.h"
#include <bus/io.h>
#include <io/vconman.h>
#include <scheduler.h>
#include <introute.h>
#include <io/io.h>
#include <templates/unique_ptr.cc>


//! Standard register I/O ports
constexpr const uint16_t PS2_PORT_COMMAND           = 0x64U;
constexpr const uint16_t PS2_PORT_STATUS            = 0x64U;
constexpr const uint16_t PS2_PORT_DATA              = 0x60U;

//! Status flags
constexpr const uint8_t PS2_STATUS_OUTPUT_BUFF      = 0x01U;
constexpr const uint8_t PS2_STATUS_INPUT_BUFF       = 0x02U;
//constexpr const uint8_t PS2_STATUS_SYS_FLAG         = 0x4;
/*
constexpr const uint8_t PS2_STATUS_COMMAND          = 0x8;

constexpr const uint8_t PS2_STATUS_TIME_OUT_ERR     = 0x40;
constexpr const uint8_t PS2_STATUS_PARITY_ERR       = 0x80;
*/

//! Controller command bytes
constexpr const uint8_t PS2_COMMAND_READ_CONFIG     = 0x20U;
constexpr const uint8_t PS2_COMMAND_WRITE_CONFIG    = 0x60U;

constexpr const uint8_t PS2_COMMAND_DISABLE_PORT2   = 0xA7U;
constexpr const uint8_t PS2_COMMAND_ENABLE_PORT2    = 0xA8U;
constexpr const uint8_t PS2_COMMAND_TEST_PORT2      = 0xA9U;

constexpr const uint8_t PS2_COMMAND_TEST_CONTROLLER = 0xAAU;

constexpr const uint8_t PS2_COMMAND_TEST_PORT1      = 0xABU;
constexpr const uint8_t PS2_COMMAND_DISABLE_PORT1   = 0xADU;
constexpr const uint8_t PS2_COMMAND_ENABLE_PORT1    = 0xAEU;

/*
constexpr const uint8_t PS2_COMMAND_READ_CONT_OUT   = 0xD0;
constexpr const uint8_t PS2_WRITE_CONT_OUT          = 0xD1;
constexpr const uint8_t PS2_WRITE_PORT1_OUT         = 0xD2;
constexpr const uint8_t PS2_WRITE_PORT2_OUT         = 0xD3;
*/
constexpr const uint8_t PS2_COMMAND_WRITE_PORT2_IN          = 0xD4U;

//! Standard PS/2 Device commands
constexpr const uint8_t PS2_DEV_RESET               = 0xFFU;
constexpr const uint8_t PS2_DEV_SCAN_ENABLE         = 0xF4U;
constexpr const uint8_t PS2_DEV_SCAN_DISABLE        = 0xF5U;
constexpr const uint8_t PS2_DEV_IDENTIFY            = 0xF2U;

//! PS/2 Keyboard specific commands
constexpr const uint8_t PS2_KB_CODESET_ACCESS       = 0xF0U;

COMPILER_UNUSED constexpr const uint8_t PS2_KB_CODESET_SUB_GET      = 0x00U;
constexpr const uint8_t PS2_KB_CODESET_SUB_SET1     = 0x01U;
COMPILER_UNUSED constexpr const uint8_t PS2_KB_CODESET_SUB_SET2     = 0x02U;
COMPILER_UNUSED constexpr const uint8_t PS2_KB_CODESET_SUB_SET3     = 0x03U;

//! Standard PS/2 Configuration value masks
constexpr const uint8_t PS2_CONFIG_PORT1_INT        = 0x1U;
constexpr const uint8_t PS2_CONFIG_PORT2_INT        = 0x2U;
constexpr const uint8_t PS2_CONFIG_PORT1_CLK        = 0x10;
constexpr const uint8_t PS2_CONFIG_PORT2_CLK        = 0x20U;
constexpr const uint8_t PS2_CONFIG_PORT1_TRANS      = 0x40U;

//! Controller Response bytes
constexpr const uint8_t PS2_TEST_PASSED             = 0x55U;
constexpr const uint8_t PS2_PORT_TEST_SUCCESS       = 0x00U;

//! Device response bytes
constexpr const uint8_t PS2_DEV_RESPONSE_ACK        = 0xFAU;
constexpr const uint8_t PS2_DEV_RESPONSE_TEST_PASS  = 0xAAU;
constexpr const uint8_t PS2_DEV_RESPONSE_TEST_FAIL1 = 0xFCU;
constexpr const uint8_t PS2_DEV_RESPONSE_TEST_FAIL2 = 0xFDU;

//! Device ID bytes
constexpr const uint8_t PS2_ID_MOUSE                = 0x00U;
constexpr const uint8_t PS2_ID_MOUSE_SCROLL         = 0x03U;
constexpr const uint8_t PS2_ID_MOUSE_FIVE           = 0x04U;
constexpr const uint8_t PS2_ID_MF_KB                = 0xABU;


//! Internal flags
constexpr const uint8_t PORT1_PRESENT               = 0x01U;
constexpr const uint8_t PORT2_PRESENT               = 0x02U;
constexpr const uint8_t PORT1_RESPONSE_WAIT         = 0x04U;
constexpr const uint8_t PORT2_RESPONSE_WAIT         = 0x08U;
constexpr const uint8_t PORT1_HANDLER_ACTIVE        = 0x10U;
constexpr const uint8_t PORT2_HANDLER_ACTIVE        = 0x20U;

//! Enumeration configuration values
constexpr const int TIMEOUT_LIMIT                   = 8;

static uint16_t kbMask = 0;

COMPILER_UNUSED static uint64_t port1DevBits = 0;
COMPILER_UNUSED static uint64_t port2DevBits = 0;

static ps2Controller *controllerInstance = nullptr;
static shared_ptr<fileHandle> inputWritePipe;

bool (*ps2Controller::handleP1Device)(Vector<uint8_t> &bytes, const ps2TargetPort port) = nullptr;
bool (*ps2Controller::handleP2Device)(Vector<uint8_t> &bytes, const ps2TargetPort port) = nullptr;

//! Static functions

bool ps2Controller::handleSimpleMouse(Vector<uint8_t> &bytes, const ps2TargetPort port)
{
    return true;
}

bool ps2Controller::handleScrollMouse(Vector<uint8_t> &bytes, const ps2TargetPort port)
{
    return true;
}

bool ps2Controller::handle5ButtonMouse(Vector<uint8_t> &bytes, const ps2TargetPort port)
{
    return true;
}

bool ps2Controller::handleKeyboard(Vector<uint8_t> &bytes, const ps2TargetPort port)
{
    if(!bytes.length())
        return true;

    uint16_t value = 0;

    if(bytes[0] == 0xE0U)
    {
        //TODO: Extended code handling
        kprintf("Extended scan code: ");
        for(int i = 0; i < bytes.length(); i++)
        {
            kprintf("%X ", bytes[i]);
        }

        kprintf("\n");
    }
    else
    {
        kbScanEntry &entry = ps2Keys[ bytes[0] ];
        if(bytes[0] <= KB_SCAN_RSHIFT_DOWN)
        {

            if(entry.controlMask)
                kbMask |= entry.controlMask;
            else
            {
                if( ps2ShiftActive(kbMask) )
                    value = entry.sValue;
                else
                    value = entry.cValue;

                if( ps2CapslockToggle(kbMask) )
                {
                    if(value >= u'A' && value <= u'Z')
                        value += 0x20U;
                    else if(value >= u'a' && value <= u'z')
                        value -= 0x20U;
                }
            }
        }
        else if(bytes[0] < 0x80U)
        {
            if(entry.controlMask)
                kbMask ^= entry.controlMask;
            else if( kbMask & PS2_KB_MASK_NUM_LOCK )
                value = entry.sValue;
            else
                value = entry.cValue;
        }
        else
        {
            if(entry.controlMask)
                kbMask &= ~entry.controlMask;
        }
    }

    if(value)
        virtConsoleManager::processKey(value, kbMask);

    return true;
}

int ps2Controller::port1Handler(COMPILER_UNUSED void *ctx, COMPILER_UNUSED const int index)
{
    if(controllerInstance->flags & PORT1_RESPONSE_WAIT)
    {
        //kprintf("[PS/2]: Getting port 1 bytes...\n");
        controllerInstance->port1Wait->getDataBytes();
        controllerInstance->port1Wait->signalListeners();

    }
    else if(controllerInstance->flags & PORT1_HANDLER_ACTIVE)
    {
        Vector<uint8_t> devResponse;
        controllerInstance->readDataBytes(devResponse);
        return (*ps2Controller::handleP1Device)(devResponse, PORT1);
    }
    return true;
}

int ps2Controller::port2Handler(COMPILER_UNUSED void *ctx, COMPILER_UNUSED const int index)
{
    if(controllerInstance->flags & PORT2_HANDLER_ACTIVE)
    {
        Vector<uint8_t> devResponse;
        controllerInstance->readDataBytes(devResponse);
        return (*ps2Controller::handleP2Device)(devResponse, PORT2);
    }
    else
    {
        controllerInstance->port2Wait->getDataBytes();
        controllerInstance->port2Wait->signalListeners();
    }

    return true;
}

//! Constructor
ps2Controller::ps2Controller(Device *parent) : Device("PS/2 Controller", "ps2_controller", parent), flags(PORT1_PRESENT)
{
    Task *current = Scheduler::getCurrentTask();
    controllerInstance = this;

    port1Wait = new ps2WaitEvent(current, PORT1);
    port2Wait = new ps2WaitEvent(current, PORT2);
}

/*! Reads bytes from the controller data buffer
 * @param bytes Buffer where the bytes will be placed
 * @param clearFlags Flags to clear after reading bytes
 */
void ps2Controller::readDataBytes(Vector<uint8_t> &bytes, const uint8_t clearFlags) volatile
{
    //Read bytes from the data port until the buffer is clear on the status port
    while(ioBus::readByte(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFF)
    {
        uint8_t byte = ioBus::readByte(PS2_PORT_DATA);
        bytes.insert(byte);
    }
    flags &= ~clearFlags;
}

//! Waits for the controller to process the current input
void ps2Controller::waitInputBuffer()
{
    while( ioBus::readByte(PS2_PORT_STATUS) & PS2_STATUS_INPUT_BUFF)
    {
    }
}

//! Discards all bytes in the PS/2 Controller's output buffer
void ps2Controller::flushOutputBuffer()
{
    while( ioBus::readByte(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFF )
    {
        ioBus::readByte(PS2_PORT_DATA);
    }
}

uint8_t ps2Controller::waitDataByte(void)
{
    while( !(ioBus::readByte(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFF) )
    {
    }

    return ioBus::readByte(PS2_PORT_DATA);
}

/*! Writes a data byte to the data port output buffer once it's clear
 * \param value Byte to write
 */
void ps2Controller::waitAndWriteData(const uint8_t value)
{
    waitInputBuffer();
    ioBus::writeByte(PS2_PORT_DATA, value);
    //waitInputBuffer();
}

/*! Writes a data byte to the second port of the PS/2 controller
 * \param value Value to write to Port2
 */
void ps2Controller::writePort2(const uint8_t value)
{
    issueCommand(PS2_COMMAND_WRITE_PORT2_IN);
    waitAndWriteData(value);
}

/*! Writes a command to the command port
 * \param value Command byte
 */
void ps2Controller::issueCommand(const uint8_t value)
{
    waitInputBuffer();
    ioBus::writeByte(PS2_PORT_COMMAND, value);
}

/*! Waits for a device on a given port to response
 * \pre Interrupts for the ports must be enabled and isrs installed
 * \param port Port to wait for data on
 * \param response Response return buffer
 */
void ps2Controller::waitResponseBytes(const ps2TargetPort port, Vector<uint8_t> &response)
{
    switch(port)
    {
        case PORT1:
            Scheduler::waitCurrent(port1Wait, 35);
            response = port1Wait->getMessage();
            port1Wait->clearMessage();
            atomicAnd(&this->flags, ~PORT1_RESPONSE_WAIT);
            break;
        case PORT2:
            Scheduler::waitCurrent(port2Wait, 35);
            response = port2Wait->getMessage();
            port2Wait->clearMessage();
            atomicAnd(&this->flags, ~PORT2_RESPONSE_WAIT);
            break;
    }

}

/*! Validates the response of a port device to a reset & self test request
 * \param bytes Device response bytes to analyze
 * \return True if the device passed the self test, false if any failure was encountered.
 */
bool ps2Controller::selfTestValidate( Vector<uint8_t> &bytes)
{
    if( bytes.length() >= 2)
    {
        if(bytes[0] == PS2_DEV_RESPONSE_ACK)
        {
            switch(bytes[1])
            {
                case PS2_DEV_RESPONSE_TEST_PASS:
                    return true;
                    break;
                case PS2_DEV_RESPONSE_TEST_FAIL2:
                case PS2_DEV_RESPONSE_TEST_FAIL1:
                default:
                    break;
            }
        }
    }
    else
    {
        kprintf("[PS/2]: Self test response too short! Expected at least 2 bytes found %u instead!\n", bytes.length() );
    }

    return false;
}

/*! Waits for the device on a port to issue an acknowledge response
 * \param port Port to wait on
 * \param response Complete device response
 * \return True if the response started with an acknowledge byte, false otherwise
 */
bool ps2Controller::waitDevACK(const ps2TargetPort port, Vector<uint8_t> &response)
{
    flags |= PORT1_RESPONSE_WAIT;
    waitResponseBytes(port, response);
    if( response.length() >= 1)
    {
        if(response[0] == PS2_DEV_RESPONSE_ACK)
            return true;
    }

    return false;
}

/*! Analyzes the response to an identification request and installs the proper handler
 * \param portHandler Reference to the target port handler function pointer
 * \param response Response bytes from the identify command
 * \return True if the device was successfully identified and a the proper handler installed for it
 */
bool ps2Controller::processIdentifyResponse(ps2Handler &portHandler, const Vector<uint8_t> &response, const ps2TargetPort port)
{
    Vector<uint8_t> responseBytes;
    int timeOutCount = 0;

    if(response.length() < 2)
        return false;

    //kprintf("[PS/2]: Processing identification for port %u\n", port);
    switch(response[1])
    {
        case PS2_ID_MOUSE:
            kprintf("Found PS/2 Mouse!\n");
            portHandler = &handleSimpleMouse;
            break;
        case PS2_ID_MOUSE_SCROLL:
            portHandler = &handleScrollMouse;
            break;
        case PS2_ID_MOUSE_FIVE:
            portHandler = &handle5ButtonMouse;
            break;
        case PS2_ID_MF_KB:
            if(response.length() < 3)
                return false;
            switch(response[2])
            {
                default:
                    portHandler = &handleKeyboard;
                    switch(port)
                    {
                        case PORT1:
                            // Set the keyboard to use scancode set 1
                            do
                            {
                                flags |= PORT1_RESPONSE_WAIT;
                                timeOutCount++;
                                if(timeOutCount == TIMEOUT_LIMIT)
                                    return false;
                                responseBytes.clear();
                                waitAndWriteData(PS2_KB_CODESET_ACCESS);
                                waitAndWriteData(PS2_KB_CODESET_SUB_SET1);
                            } while (!waitDevACK(PORT1, responseBytes));

                            timeOutCount = 0;
                            // Enable scanning
                            do
                            {
                                flags |= PORT1_RESPONSE_WAIT;
                                if(timeOutCount == TIMEOUT_LIMIT)
                                    return false;

                                responseBytes.clear();
                                waitAndWriteData(PS2_DEV_SCAN_ENABLE);
                            } while(!waitDevACK(PORT1, responseBytes));

                            flags |= PORT1_HANDLER_ACTIVE;
                            break;
                        case PORT2:
                            do
                            {
                                flags |= PORT2_RESPONSE_WAIT;
                                if(timeOutCount == TIMEOUT_LIMIT)
                                    return false;

                                responseBytes.clear();
                                writePort2(PS2_KB_CODESET_ACCESS);
                                writePort2(PS2_KB_CODESET_SUB_SET1);
                            } while(!waitDevACK(PORT2, responseBytes));
                            do
                            {
                                flags |= PORT2_RESPONSE_WAIT;
                                if(timeOutCount == TIMEOUT_LIMIT)
                                    return false;
                                responseBytes.clear();
                                writePort2(PS2_DEV_SCAN_ENABLE);
                            } while(!waitDevACK(PORT2, responseBytes));

                            flags |= PORT2_HANDLER_ACTIVE;
                            break;
                    }

                    kprintf("Found PS/2 Keyboard.\n");
                    break;
            }


            break;

        default:
            return false;
            break;
    }



    return true;
}

/*! Resets, identifies and installs the handler for the device attached to the first port
 * \return True if the device was identified and initialized successfully, false otherwise
 */
bool ps2Controller::initFirstPortDevice(void)
{
    Vector<uint8_t> responseBytes;

    // kprintf("Resetting...\n");
    //Reset the device and check the reported self test
    flags |= PORT1_RESPONSE_WAIT;
    waitAndWriteData(PS2_DEV_RESET);
    waitResponseBytes(PORT1, responseBytes);

    // kprintf("Validating reset response...\n");
    if( !selfTestValidate(responseBytes) )
    {
        kprintf("[PS/2]: Port 1 failed self test!\n");
        return false;
    }

    responseBytes.clear();
    //kprintf("Scan disable...\n");
    //Disable scanning
    flags |= PORT1_RESPONSE_WAIT;
    waitAndWriteData(PS2_DEV_SCAN_DISABLE);

    if( !waitDevACK(PORT1, responseBytes) )
        return false;
    responseBytes.clear();
    // kprintf("Identifying...\n");
    //Identify the type of device connected
    flags |= PORT1_RESPONSE_WAIT;
    waitAndWriteData(PS2_DEV_IDENTIFY);
    if( waitDevACK(PORT1, responseBytes))
        return processIdentifyResponse(handleP1Device, responseBytes, PORT1);
    else
        return false;
}

bool ps2Controller::initSecondPortDevice(void)
{
    Vector<uint8_t> responseBytes;

    //Reset and check the reported self test
    flags |= PORT2_RESPONSE_WAIT;
    writePort2(PS2_DEV_RESET);
    waitResponseBytes(PORT2, responseBytes);

    if( !selfTestValidate(responseBytes) )
    {

        kprintf("[PS/2]: Port 2 failed self test!\n");
        return false;
    }

    //Disable scanning
    flags |= PORT2_RESPONSE_WAIT;
    writePort2(PS2_DEV_SCAN_DISABLE);
    responseBytes.clear();

    if( !waitDevACK(PORT2, responseBytes) )
        return false;
    responseBytes.clear();

    //Identify the type of device connected
    flags |= PORT2_RESPONSE_WAIT;
    writePort2(PS2_DEV_IDENTIFY);
    if( !waitDevACK(PORT2, responseBytes) )
        return false;

    return processIdentifyResponse(handleP2Device, responseBytes, PORT2);
}

bool ps2Controller::init(void *context)
{
    uint8_t configByte;

    //Disable ports
    issueCommand(PS2_COMMAND_DISABLE_PORT1);
    issueCommand(PS2_COMMAND_DISABLE_PORT2);

    flushOutputBuffer();

    //Update configuration byte to disable translation and interrupts
    issueCommand(PS2_COMMAND_READ_CONFIG);
    configByte = waitDataByte();
    configByte &= ~(PS2_CONFIG_PORT1_INT | PS2_CONFIG_PORT2_INT | PS2_CONFIG_PORT1_TRANS);
    issueCommand(PS2_COMMAND_WRITE_CONFIG);
    waitAndWriteData(configByte);

    //Test the controller
    issueCommand(PS2_COMMAND_TEST_CONTROLLER);
    uint8_t response = waitDataByte();

    if(response != PS2_TEST_PASSED)
        return false;

    //Make sure it's a two port controller (almost certainly is)
    issueCommand(PS2_COMMAND_ENABLE_PORT2);
    issueCommand(PS2_COMMAND_READ_CONFIG);
    configByte = waitDataByte();
    if( !(configByte & PS2_CONFIG_PORT2_CLK) )
        flags |= PORT2_PRESENT;
    issueCommand(PS2_COMMAND_DISABLE_PORT2);

    if(flags & PORT1_PRESENT)
    {
        kprintf("[PS/2]: Port 1 is present\n");
        interruptManager::installIrqHandler(1, &port1Handler, nullptr, 0);
    }

    if(flags & PORT2_PRESENT)
    {
        kprintf("[PS/2]: Port 2 is present\n");
        interruptManager::installIrqHandler(12, &port2Handler, nullptr, 0);
    }

    uint8_t configNoInts = configByte;

    //Test the ports
    if(flags & PORT1_PRESENT)
    {
        //Test the status of the port itself
        issueCommand(PS2_COMMAND_TEST_PORT1);
        response = waitDataByte();
        if(response != PS2_PORT_TEST_SUCCESS)
        {
            kprintf("[PS/2]: Port 1 failed test!\n");
            flags &= ~PORT1_PRESENT;
        }
        else
        {
            //If the port is present enable it and enable interrupts for it
            issueCommand(PS2_COMMAND_ENABLE_PORT1);

            issueCommand(PS2_COMMAND_WRITE_CONFIG);
            waitAndWriteData(configNoInts | PS2_CONFIG_PORT1_INT);

            //Do the port and device specific initialization
            if(initFirstPortDevice())
            {
                kprintf("First PS/2 Port device initialized!\n");
                configByte |= PS2_CONFIG_PORT1_INT;
                configByte &= ~PS2_CONFIG_PORT1_CLK;
            }
            else
            {

                kprintf("[PS/2]: Disabled Port 1 Device\n");
                issueCommand(PS2_COMMAND_DISABLE_PORT1);
            }

            issueCommand(PS2_COMMAND_WRITE_CONFIG);
            waitAndWriteData(configNoInts);

        }

        flushOutputBuffer();
    }

    if(flags & PORT2_PRESENT)
    {
        issueCommand(PS2_COMMAND_TEST_PORT2);
        response = waitDataByte();
        if(response != PS2_PORT_TEST_SUCCESS)
        {
            kprintf("[PS/2]: Port 2 failed test!\n");
            flags &= ~PORT2_PRESENT;
        }
        else
        {
            issueCommand(PS2_COMMAND_ENABLE_PORT2);

            issueCommand(PS2_COMMAND_WRITE_CONFIG);
            waitAndWriteData(configNoInts | PS2_CONFIG_PORT2_INT);

            if(initSecondPortDevice())
            {
                kprintf("Second PS/2 Port device initialized!\n");
                configByte |= PS2_CONFIG_PORT2_INT;
                configByte &= ~PS2_CONFIG_PORT2_CLK;
            }
            else
            {
                kprintf("[PS/2]: Disabled Port 2 Device\n");
                issueCommand(PS2_COMMAND_DISABLE_PORT2);
            }

            issueCommand(PS2_COMMAND_WRITE_CONFIG);
            waitAndWriteData(configNoInts);
        }
        flushOutputBuffer();
    }

    issueCommand(PS2_COMMAND_WRITE_CONFIG);
    waitAndWriteData(configByte);
    flushOutputBuffer();

    return true;
}


ps2Controller::ps2WaitEvent::ps2WaitEvent(Task *task, ps2TargetPort port) : Event(), waitPort(port)
{
}

void ps2Controller::ps2WaitEvent::getDataBytes(void)
{
    controllerInstance->readDataBytes(message);
}

const Vector<uint8_t> &ps2Controller::ps2WaitEvent::getMessage(void) const
{
    return message;
}


void ps2Controller::ps2WaitEvent::clearMessage(void)
{
    message.clear();
}
