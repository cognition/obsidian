#ifndef TERM_IO_H_
#define TERM_IO_H_

#include <fs/filesystem.h>
#include <templates/shared_ptr.cc>

namespace termIo
{
    bool init(void);

    shared_ptr<fileHandle> getPrimaryOutput(void);
    shared_ptr<fileHandle> getPrimaryInput(void);

    void registerOutputDevice(textOutputDevice *dev);
    shared_ptr<fileHandle> getPrimaryInputProducerHandle(void);
};


#endif // TERM_IO_H_
