#include "termio.h"
#include <fs/filesession.h>
#include <fs/filehandle.h>

static textOutputDevice *outputDev = nullptr;
static shared_ptr<fileSession> primaryOutput;
static shared_ptr<fileSession> primaryInputConsumer;
static shared_ptr<fileSession> primaryInputProducer;

namespace termIo
{
    bool init(void)
    {
        primaryOutput.reset( new fileSession(outputDev, fileSession::sessionWriteOnly) );
        return fileSession::createPipe(primaryInputConsumer, fileSession::sessionTerm, primaryInputProducer, 0, 256);
    }

    shared_ptr<fileHandle> getPrimaryOutput(void)
    {
        return shared_ptr<fileHandle>( new fileHandle( primaryOutput ));
    }

    shared_ptr<fileHandle> getPrimaryInput(void)
    {
        return shared_ptr<fileHandle> ( new fileHandle( primaryInputConsumer ));
    }

    void registerOutputDevice(textOutputDevice *dev)
    {
        outputDev = dev;
    }

    shared_ptr<fileHandle> getPrimaryInputProducerHandle(void)
    {
        return shared_ptr<fileHandle>( new fileHandle( primaryInputProducer ));
    }
};
