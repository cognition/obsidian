#ifndef PS2_KB_H_
#define PS2_KB_H_

#include <compiler.h>

#define PS2_KB_MASK_LCTRL             0x0001U
#define PS2_KB_MASK_LOS               0x0002U
#define PS2_KB_MASK_LALT              0x0004U
#define PS2_KB_MASK_RALT              0x0008U
#define PS2_KB_MASK_ROS               0x0010U
#define PS2_KB_MASK_MENU              0x0020U
#define PS2_KB_MASK_RCTRL             0x0040U
#define PS2_KB_MASK_LSHIFT            0x0080U
#define PS2_KB_MASK_RSHIFT            0x0100U

#define PS2_KB_MASK_CAPS_LOCK         0x0200U
#define PS2_KB_MASK_SCROLL_LOCK       0x0400U
#define PS2_KB_MASK_NUM_LOCK          0x0800U

#define PS2_KB_MOD_CTRL(x)            (x & (PS2_KB_MASK_LCTRL | PS2_KB_MASK_RCTRL))
#define PS2_KB_MOD_ALT(x)             (x & (PS2_KB_MASK_LALT | PS2_KB_MASK_RALT))
#define PS2_KB_MASK_OS(x)             (x & (PS2_KB_MASK_LOS | PS2_KB_MASK_ROS))


//! Scan code set 1 values
#define KB_SCAN_ESCAPE_DOWN                0x01U
#define KB_SCAN_ONE_DOWN                   0x02U
#define KB_SCAN_TWO_DOWN                   0x03U
#define KB_SCAN_THREE_DOWN                 0x04U
#define KB_SCAN_FOUR_DOWN                  0x05U
#define KB_SCAN_FIVE_DOWN                  0x06U
#define KB_SCAN_SIX_DOWN                   0x07U
#define KB_SCAN_SEVEN_DOWN                 0x08U
#define KB_SCAN_EIGHT_DOWN                 0x09U
#define KB_SCAN_NINE_DOWN                  0x0AU
#define KB_SCAN_ZERO_DOWN                  0x0BU

#define KB_SCAN_MINUS_DOWN                 0x0CU
#define KB_SCAN_PLUS_DOWN                  0x0DU

#define KB_SCAN_BACKSPACE_DOWN             0x0EU
#define KB_SCAN_TAB_DOWN                   0x0FU

#define KB_SCAN_Q_DOWN                     0x10U
#define KB_SCAN_W_DOWN                     0x11U
#define KB_SCAN_E_DOWN                     0x12U
#define KB_SCAN_R_DOWN                     0x13U
#define KB_SCAN_T_DOWN                     0x14U
#define KB_SCAN_Y_DOWN                     0x15U
#define KB_SCAN_U_DOWN                     0x16U
#define KB_SCAN_I_DOWN                     0x17U
#define KB_SCAN_O_DOWN                     0x18U
#define KB_SCAN_P_DOWN                     0x19U

#define KB_SCAN_LBRACKET_DOWN              0x1AU
#define KB_SCAN_RBRACKET_DOWN              0x1BU

#define KB_SCAN_ENTER_DOWN                 0x1CU
#define KB_SCAN_LEFT_CTRL_DOWN             0x1DU

#define KB_SCAN_A_DOWN                     0x1EU
#define KB_SCAN_S_DOWN                     0x1FU
#define KB_SCAN_D_DOWN                     0x20U
#define KB_SCAN_F_DOWN                     0x21U
#define KB_SCAN_G_DOWN                     0x22U
#define KB_SCAN_H_DOWN                     0x23U
#define KB_SCAN_J_DOWN                     0x24U
#define KB_SCAN_K_DOWN                     0x25U
#define KB_SCAN_L_DOWN                     0x26U

#define KB_SCAN_SEMICOLON_DOWN             0x27U
#define KB_SCAN_QUOTE_DOWN                 0x28U
#define KB_SCAN_BACKTICK_DOWN              0x29U

#define KB_SCAN_LSHIFT_DOWN                0x2AU
#define KB_SCAN_BACKSLASH_DOWN             0x2BU

#define KB_SCAN_Z_DOWN                     0x2CU
#define KB_SCAN_X_DOWN                     0x2DU
#define KB_SCAN_C_DOWN                     0x2EU
#define KB_SCAN_V_DOWN                     0x2FU
#define KB_SCAN_B_DOWN                     0x30U
#define KB_SCAN_N_DOWN                     0x31U
#define KB_SCAN_M_DOWN                     0x32U
#define KB_SCAN_COMMA_DOWN                 0x33U
#define KB_SCAN_PERIOD_DOWN                0x34U
#define KB_SCAN_SLASH_DOWN                 0x35U

#define KB_SCAN_RSHIFT_DOWN                0x36U

#define KB_SCAN_KEYPAD_STAR                0x37U
#define KB_SCAN_LALT_DOWN                  0x38U
#define KB_SCAN_SPACE_DOWN                 0x39U
#define KB_SCAN_CAPSLOCK_DOWN              0x3AU

#define KB_SCAN_F1_DOWN                    0x3BU
#define KB_SCAN_F2_DOWN                    0x3CU
#define KB_SCAN_F3_DOWN                    0x3DU
#define KB_SCAN_F4_DOWN                    0x3EU
#define KB_SCAN_F5_DOWN                    0x3FU
#define KB_SCAN_F6_DOWN                    0x40U
#define KB_SCAN_F7_DOWN                    0x41U
#define KB_SCAN_F8_DOWN                    0x42U
#define KB_SCAN_F9_DOWN                    0x43U
#define KB_SCAN_F10_DOWN                   0x44U

#define KB_SCAN_NUMLOCK_DOWN               0x45U
#define KB_SCAN_SCROLL_LOCK_DOWN           0x46U

#define KB_SCAN_KEYPAD_7_DOWN              0x47U
#define KB_SCAN_KEYPAD_8_DOWN              0x48U
#define KB_SCAN_KEYPAD_9_DOWN              0x49U
#define KB_SCAN_KEYPAD_MINUS_DOWN          0x4AU
#define KB_SCAN_KEYPAD_4_DOWN              0x4BU
#define KB_SCAN_KEYPAD_5_DOWN              0x4CU
#define KB_SCAN_KEYPAD_6_DOWN              0x4DU
#define KB_SCAN_KEYPAD_PLUS_DOWN           0x4EU
#define KB_SCAN_KEYPAD_1_DOWN              0x4FU
#define KB_SCAN_KEYPAD_2_DOWN              0x50U
#define KB_SCAN_KEYPAD_3_DOWN              0x51U
#define KB_SCAN_KEYPAD_0_DOWN              0x52U
#define KB_SCAN_KEYPAD_PERIOD_DOWN         0x53U

#define KB_SCAN_F11_DOWN                   0x57U
#define KB_SCAN_F12_DOWN                   0x58U

//! Extended codes
#define KB_SCAN_EXT_MM_PREV                0x10U
#define KB_SCAN_EXT_MM_NEXT                0x19U

#define KB_SCAN_EXT_KEYPAD_ENTER           0x1CU
#define KB_SCAN_EXT_RIGHT_CONTROL          0x1DU

#define KB_SCAN_EXT_MM_MUTE                0x20U
#define KB_SCAN_EXT_APP_CALC               0x21U
#define KB_SCAN_EXT_MM_PLAY                0x22U
#define KB_SCAN_EXT_MM_STOP                0x24U

#define KB_SCAN_EXT_MM_VOL_DOWN            0x2EU
#define KB_SCAN_EXT_MM_VOL_UP              0x30U

#define KB_SCAN_EXT_WEB_HOME               0x32U

#define KB_SCAN_EXT_KEYPAD_SLASH           0x35U
#define KB_SCAN_EXT_RIGHT_ALT              0x38U

#define KB_SCAN_EXT_HOME                   0x47U
#define KB_SCAN_EXT_CURSOR_UP              0x48U
#define KB_SCAN_EXT_PAGE_UP                0x49U

#define KB_SCAN_EXT_CURSOR_LEFT            0x4BU
#define KB_SCAN_EXT_CURSOR_RIGHT           0x4DU

#define KB_SCAN_EXT_END                    0x4FU
#define KB_SCAN_EXT_CURSOR_DOWN            0x50U
#define KB_SCAN_EXT_PAGE_DOWN              0x51U
#define KB_SCAN_EXT_INSERT                 0x52U
#define KB_SCAN_EXT_DELETE                 0x53U

#define KB_SCAN_EXT_LEFT_GUI               0x5BU
#define KB_SCAN_EXT_RIGHT_GUI              0x5CU
#define KB_SCAN_EXT_APPS                   0x5DU
#define KB_SCAN_EXT_POWER                  0x5EU
#define KB_SCAN_EXT_SLEEP                  0x5FU

#define KB_SCAN_EXT_WAKE                   0x63U

#define KB_SCAN_EXT_WEB_SEARCH             0x65U
#define KB_SCAN_EXT_WEB_FAVORITES          0x66U
#define KB_SCAN_EXT_WEB_REFRESH            0x67U
#define KB_SCAN_EXT_WEB_STOP               0x68U
#define KB_SCAN_EXT_WEB_FORWARD            0x69U
#define KB_SCAN_EXT_WEB_BACK               0x6AU
#define KB_SCAN_EXT_APP_COMPUTER           0x6BU
#define KB_SCAN_EXT_APP_EMAIL              0x6CU
#define KB_SCAN_EXT_MM_SELECT              0x6DU

constexpr const bool ps2ShiftActive(const uint16_t value)
{
    return (value & (PS2_KB_MASK_LSHIFT | PS2_KB_MASK_RSHIFT) );
}

constexpr const bool ps2CapslockToggle(const uint16_t value)
{
    return (value & PS2_KB_MASK_CAPS_LOCK);
}


struct kbScanEntry
{
    uint16_t controlMask;
    uint16_t cValue, sValue;
};

extern "C" kbScanEntry ps2Keys[256];


#endif // PS2_KB_H_
