#include "io.h"

#include <cpu/lock.h>
#include <memory/virtual.h>

extern "C" void io_debug_print(const char *format, ...);

static void kvprintf_internal(const char *format, unsigned int length, va_list args);
ticketLock kprintLock;

/*!  Standards compliant printf implementation for in kernel use
 *   \param format Formatting string
 */
extern "C" void kprintf( const char *format, ... )
{
	va_list args;
	va_start( args, format );
	kvprintf( format, args );
	va_end( args );
}


/*!  Converts an unsigned integer in string form to an unsigned integer
 *   \param s String to convert
 *   \return Integer value contained within the string
 */
static uint32_t atoi( const char *&s, unsigned int & index )
{
	uint32_t value = 0;
	while( ( '0' <= *s ) && ( *s <= '9' ) )
	{
		value *= 10;
		value += *s - '0';
		s++;
		index++;
	}
	return value;
}

/*! Standards compliant vprintf implementation
 *  \param format Formatting string
 *  \param args Variable arguments list
 */
extern "C" void kvprintf( const char *format, va_list args )
{
    unsigned int length = String::strnlen(format, 255);

	spinLockInstance outLock( &kprintLock, __builtin_return_address(0) );
    kvprintf_internal(format, length, args);
}

extern "C" void io_debug_print(const char *format, ...)
{
    unsigned int length = String::strnlen(format, 255);
	va_list args;
	va_start( args, format );
	kvprintf_internal( format, length, args );
	va_end( args );
}

static void kvprintf_internal(const char *format, unsigned int length, va_list args)
{
	uint8_t width = 0;
	uint8_t precision = 0;
	uint32_t attr = 0;
	const char *string_arg;

	// Lock up
	for(unsigned int index = 0; index < length; index++, format++)
	{
		switch( *format )
		{
			case '%':
				//Formatting operation
				format++;
				index++;
				if(index == length)
                    continue;
				check_flags:
				switch( *format )
				{
				    //Zero fill specified
				    case '0':
                        attr |= NUMBER_ATTR_ZERO_FILL;
                        format++;
                        index++;
                        if(index >= length)
                            continue;
                        goto check_flags;
                    //Left align
                    case '-':
                        attr |= NUMBER_ATTR_LEFT_ALIGN;
                        format++;
                        index++;
                        if(index >= length)
                            continue;
                        goto check_flags;
                    default:
                        break;
				}

                if(*format == '*')
                {
                    width = va_arg(args, uint32_t);
                    format++;
                    index++;
                    if(index >= length)
                        continue;
                }
				else
                {
                    //Attempt to read in a width value
                    width = atoi( format, index );
                    if(index >= length)
                            continue;
                }
				//Check if we have a precision argument as well
				if( *format == '.' )
				{
					format++;
					index++;

                    if(*format == '*')
                    {
                        precision = va_arg(args, uint32_t);
                        format++;
                        index++;
                        if(index >= length)
                            continue;
                    }
                    else
                    {
                        precision = atoi( format, index );
                        if(index >= length)
                            continue;
                    }
				}
				//Length fields
				parse_length:
				switch(*format)
				{
				    case 'l':
                        format++;
                        index++;
                        if(index >= length)
                            continue;
                        goto parse_length;
				    default:
                        break;
				}
				//Number type specifiers
				switch( *format )
				{
					case 'i':
					case 'd':
						//Signed integer
						out.printSignedDecimal32( va_arg( args, uint32_t ), width, precision, attr );
						break;
					case 'u':
						//Unsigned integer
						out.printUnsignedDecimal( va_arg( args, uint64_t ), width, precision, attr );
						break;
					case 'x':
						//Lower case hexadecimal
						out.printHexLower( va_arg( args, uint64_t ), width, precision, attr );
						break;
					case 'X':
						//Upper case hexdecimal
						out.printHexUpper( va_arg( args, uint64_t ), width, precision, attr );
						break;
					case 'o':
						//Octal values
						out.printOctal( va_arg( args, uint64_t ), width, precision, attr );
						break;
					case 's':
						//String
						string_arg = va_arg( args, const char * );
						out.printString( string_arg, width, precision, attr );
						/*
						if(virtualMemoryManager::verifyAccess(reinterpret_cast <uintptr_t> (string_arg), 1, PAGE_PRESENT ))
                            out.printString( string_arg, width, precision, attr );
                        else
                        {
                            out.printString("[_OUT]: Error invalid string passed! ", 0, 0, 0);
                            out.printHexUpper( reinterpret_cast <uintptr_t> (string_arg), 16, 0, 0);
                            asm volatile ("cli\n"
                                          "hlt\n"::);
                        }
                        */
						break;
					case 'c':
						//A signal character
						out.putChar( va_arg( args, unsigned int ) );
						break;
					case '%':
						//The percentage sign character
						out.putChar( '%' );
						break;
					case 'p':
						//Implementation specific pointer value, just upper case hex for the kernel
						out.printHexUpper ( va_arg( args, uint64_t ), width, precision, attr );
						break;
					default:
						out.putChar( *format );
						break;
				}
				width = 0;
				precision = 0;
				attr = 0;
				break;
			case '\v':
			case '\n':
				//New line
				out << endl;
				break;
				//Carriage return
			case '\r':
				break;
				//Tab
			case '\t':
				out << streamTab;
				break;
				//Any other character
			default:
				out.putChar( *format );
				break;
		}
	}

	out << streamFlush;
}

