#include "virt_console.h"
#include <cpu/lock.h>
#include <memory/ops.h>

static const constexpr int VIRTCON_HARDWARE_INPUT_BUFFER_MAX = 256;

/*! Constructor
 *  \param outputDev Hardware device
 *  \param initCharWidth Width of the console (and output device) in columns
 *  \param initCharHeight Height of the console (and output device) in rows
 */
virtConsole::virtConsole(textOutputDevice &outputDev, const int initCharWidth, const int initCharHeight) :
    textOutputDevice(initCharWidth*initCharHeight, 0), outDev(outputDev),
    textBuffer(new char[initCharWidth*initCharHeight]),
    textCols(initCharWidth), textRows(initCharHeight), maxInputSize(255),
    currentRow(0), currentCol(0), inputStartOffset(0) {

    Memory::set(textBuffer.get(), ' ', initCharWidth*initCharHeight);
}

virtConsole::~virtConsole()
{
}

//! Marks the console as active/having focus
void virtConsole::setActive(void)
{

    outDev.preformOp(streamOpClear);
    outDev.writeBuffer(textBuffer.get(), currentOffset);
    outDev.preformOp(streamOpFlush);

    spinLockInstance lock(&vconLock);

    active = true;
}

//! Marks the console as inactive/lacking focus
void virtConsole::setInactive(void)
{
    spinLockInstance lock(&vconLock);
    active = false;
}

/*! Writes text to the console and forwards it to the output device
 *  \param buffer Buffer containing the text
 *  \param length Size of the buffer
 *  \sa textOutputDevice::writeBuffer
 *  \return An ioDevResult value indicating the success or failure of the write request
 */
ioDevResult virtConsole::writeBuffer( const void *buffer, uintptr_t length )
{
    spinLockInstance lock(&vconLock);

    int newOffset = currentOffset + length;
    //Scroll the console up if necessary
    if(newOffset >= maxLength)
    {
        int rowShift = newOffset - maxLength;
        rowShift += (textCols - 1);
        rowShift /= textCols;
        scrollLines(rowShift);
    }

    const uint8_t *textInBuffer = reinterpret_cast <const uint8_t *> (buffer);

    //Copy the characters over
    for(int dataIndex = 0; dataIndex < length; dataIndex++)
        textBuffer[currentOffset++] = textInBuffer[dataIndex];

    //Update the location variables
    inputStartOffset = currentOffset;

    currentCol = currentOffset%textCols;
    currentRow = currentOffset/textCols;

    //Forward to the output device if it's active
    if(active)
       outDev.writeBuffer(buffer, length);

    return ioSuccess;
}

/*! Performs a special operation on the virtual console and forwards it to the associated hardware/proxy device if active
 *  \param o Operation to perform
 *  \return ioDevResult value indicating the success or failure of the operation
 */
ioDevResult virtConsole::preformOp( const streamOp o )
{
    spinLockInstance lock(&vconLock);

    switch(o)
    {
        case streamOpClear:
            //Clear the buffer, reset positions
            currentCol = 0;
            currentRow = 0;
            currentOffset = 0;
            inputStartOffset = 0;
            Memory::set(textBuffer.get(), ' ', maxLength);
            break;

        case streamOpTab:
            if( (textCols - currentCol) > tabSize)
            {
                currentCol += (tabSize - (currentCol%tabSize));
                break;
            }
            //Continue on to the new line operation if it's the last tab
        case streamOpNewLine:
            currentRow++;
            if(currentRow == textRows)
                scrollLines(1);
            currentCol = 0;
            currentOffset = currentRow*textCols;
            break;

        case streamOpCarriageReturn:
            currentCol = 0;
            currentOffset = textCols*currentRow;
            break;

        case streamOpBackspace:
            //Process a backspace only up to the last writeBuffer's terminus
            if(currentOffset > inputStartOffset)
            {
                currentOffset--;
                textBuffer[currentOffset] = ' ';
                currentCol = currentOffset%textCols;
                currentRow = currentOffset/textCols;
            }
            else
                return ioOutOfRange;
            break;
        default:
            break;
    }

    if( active )
        return outDev.preformOp(o);
    else
        return ioSuccess;
}

/*! Echo a character from the input stream to the virtual console
 *  \param value Character value to echo
 *  \return ioSuccess if successful or an ioDevResult error if some error is encountered
 */
ioDevResult virtConsole::echoChar(const char value)
{
    const int currentInputSize = currentOffset - inputStartOffset;
    char echoChar = 0;

    //Parse ASCII characters into potential operations
    switch(value)
    {
        case 0xA:
            preformOp(streamOpNewLine);
            break;
        case 0x8:
            if( preformOp(streamOpBackspace) == ioOutOfRange)
                return ioSuccess;
            break;
        case 0x9:
            preformOp(streamOpTab);
            break;
        default:
            echoChar = value;
            if(currentInputSize < maxInputSize)
            {

                if(currentOffset == maxLength)
                    scrollLines(1);

                textBuffer[currentOffset++] = value;

            }
            else
                return ioSuccess;
            break;
    }

    //Inject values into the default standard in stream
    spinLock lock(&vconLock);
    lock.acquire();
    shared_ptr<fileHandle> producer = getStdInProducer();
    ssize_t written = 0;

    producer->write(&value, 1, written);
    lock.release();


    if( active && echoChar)
        outDev.echoChar(echoChar);

    return ioSuccess;
}

void virtConsole::getDims(int &rows, int &cols)
{
    outDev.getDims(rows, cols);
}

//! Initializes the standard in pipe interface
void virtConsole::initStdIn()
{
    spinLockInstance lock(&vconLock);

    //Consumer must have sessionTerm flag so isatty() works correctly
    fileSession::createPipe(stdInConsumer, fileSession::sessionTerm, stdInProducer, 0, VIRTCON_HARDWARE_INPUT_BUFFER_MAX);
    inConsumer.reset( new fileHandle(stdInConsumer) );
    inProducer.reset( new fileHandle(stdInProducer) );

}

shared_ptr<fileHandle> &virtConsole::getStdInProducer(void)
{
    if( !inProducer.get_count() )
        initStdIn();
    return inProducer;
}


shared_ptr<fileHandle> &virtConsole::getStdIn(void)
{
    if( !inConsumer.get_count() )
        initStdIn();
    return inConsumer;
}

/*! Scrolls the buffer up a given number of lines
 *  \param lineCount Number of lines to scroll up
 */
void virtConsole::scrollLines(const int lineCount)
{
    int rowBytes = textRows - lineCount;
    int scrollBytes = lineCount * textCols;
    rowBytes *= textCols;

    Memory::copy(textBuffer.get(), &textBuffer[scrollBytes], rowBytes);
    Memory::set( &textBuffer[maxLength - scrollBytes], ' ', scrollBytes);

    currentRow -= lineCount;
    currentOffset = currentRow * textCols + currentCol;
}

