#include "textoutdev.h"

/*! Base constructor for the class
 *  \param length Length of the ioDevice
 *  \param offset Initial offset
 */
textOutputDevice::textOutputDevice( size_t length, size_t offset ) : maxLength( length ), currentOffset( offset ), tabSize(4)
{
}

//! Destructor
textOutputDevice::~textOutputDevice()
{
}

/*! Seeks to a current offset in the stream
 *  \param offset Offset to try to seek to
 *  \return ioSuccess if the operation completed successfully, ioOutOfRange otherwise
 */
ioDevResult textOutputDevice::seek( uintptr_t offset )
{
	if( offset < maxLength )
		currentOffset = offset;
	else
		return ioOutOfRange;
	return ioSuccess;
}
