#include "ps2_kb.h"

#define SCAN_ENTRY(mask, unicode, shift)   {mask, unicode, shift}

constexpr const kbScanEntry emptyEntry = {0U, 0U, 0U};

kbScanEntry ps2Keys[256] = {
    emptyEntry,                                // 0x00
    emptyEntry,                                // 0x01, Escape pressed
    SCAN_ENTRY(0, u'1', u'!'),                 // 0x02, 1 pressed
    SCAN_ENTRY(0, u'2', u'@'),                 // 0x03, 2 pressed
    SCAN_ENTRY(0, u'3', u'#'),                 // 0x04, 3 pressed
    SCAN_ENTRY(0, u'4', u'$'),                 // 0x05, 4 pressed
    SCAN_ENTRY(0, u'5', u'%'),                 // 0x06, 5 pressed
    SCAN_ENTRY(0, u'6', u'^'),                 // 0x07, 6 pressed
    SCAN_ENTRY(0, u'7', u'&'),                 // 0x08, 7 pressed
    SCAN_ENTRY(0, u'8', u'*'),                 // 0x09, 8 pressed
    SCAN_ENTRY(0, u'9', u'('),                 // 0x0A, 9 pressed
    SCAN_ENTRY(0, u'0', u')'),                 // 0x0B, 0 pressed
    SCAN_ENTRY(0, u'-', u'_'),                 // 0x0C, - pressed
    SCAN_ENTRY(0, u'+', u'='),                 // 0x0D, + pressed

    SCAN_ENTRY(0, 0x0008, 0x0008),                  // 0x0E, Backspace pressed
    SCAN_ENTRY(0, 0x0009, 0x0009),                  // 0x0F, Tab pressed

    SCAN_ENTRY(0, u'q', u'Q'),                 // 0x10, Q pressed
    SCAN_ENTRY(0, u'w', u'W'),                 // 0x11, W pressed
    SCAN_ENTRY(0, u'e', u'E'),                 // 0x12, E pressed
    SCAN_ENTRY(0, u'r', u'R'),                 // 0x13, R pressed
    SCAN_ENTRY(0, u't', u'T'),                 // 0x14, T pressed
    SCAN_ENTRY(0, u'y', u'Y'),                 // 0x15, Y pressed
    SCAN_ENTRY(0, u'u', u'U'),                 // 0x16, U pressed
    SCAN_ENTRY(0, u'i', u'I'),                 // 0x17, I pressed
    SCAN_ENTRY(0, u'o', u'O'),                 // 0x18, O pressed
    SCAN_ENTRY(0, u'p', u'P'),                 // 0x19, P pressed

    SCAN_ENTRY(0, u'[', u'{'),                 // 0x1A, [ pressed
    SCAN_ENTRY(0, u']', u'}'),                 // 0x1B, ] pressed

    SCAN_ENTRY(0, u'\n', u'\n'),               // 0x1C, Enter key pressed
    SCAN_ENTRY(PS2_KB_MASK_LCTRL, 0, 0),       // 0x1D, Left control pressed
    SCAN_ENTRY(0, u'a', u'A'),                 // 0x1E, A pressed
    SCAN_ENTRY(0, u's', u'S'),                 // 0x1F, S pressed
    SCAN_ENTRY(0, u'd', u'D'),                 // 0x20, D pressed
    SCAN_ENTRY(0, u'f', u'F'),                 // 0x21, F pressed
    SCAN_ENTRY(0, u'g', u'G'),                 // 0x22, G pressed
    SCAN_ENTRY(0, u'h', u'H'),                 // 0x23, H pressed
    SCAN_ENTRY(0, u'j', u'J'),                 // 0x24, J pressed
    SCAN_ENTRY(0, u'k', u'K'),                 // 0x25, K pressed
    SCAN_ENTRY(0, u'l', u'L'),                 // 0x26, L pressed
    SCAN_ENTRY(0, u';', u':'),                 // 0x27, ; pressed
    SCAN_ENTRY(0, u'\'', u'\"'),               // 0x28, ' pressed
    SCAN_ENTRY(0, u'`', u'~'),                 // 0x29, backtick/tilde pressed

    SCAN_ENTRY(PS2_KB_MASK_LSHIFT, 0, 0),      // 0x2A, Left shift down

    SCAN_ENTRY(0, u'\\', u'|'),                // 0x2B, \ pressed

    SCAN_ENTRY(0, u'z', u'Z'),                 // 0x2C, Z pressed
    SCAN_ENTRY(0, u'x', u'X'),                 // 0x2D, X pressed
    SCAN_ENTRY(0, u'c', u'C'),                 // 0x2E, C pressed
    SCAN_ENTRY(0, u'v', u'V'),                 // 0x2F, V pressed
    SCAN_ENTRY(0, u'b', u'B'),                 // 0x30, B pressed
    SCAN_ENTRY(0, u'n', u'N'),                 // 0x31, N pressed
    SCAN_ENTRY(0, u'm', u'M'),                 // 0x32, M pressed
    SCAN_ENTRY(0, u',', u'<'),                 // 0x33, , pressed
    SCAN_ENTRY(0, u'.', u'>'),                 // 0x34, . pressed
    SCAN_ENTRY(0, u'/', u'?'),                 // 0x35, / pressed

    SCAN_ENTRY(PS2_KB_MASK_RSHIFT, 0, 0),      // 0x36, Right shift pressed
    SCAN_ENTRY(0, u'*', u'*'),                 // 0x37, Keypad star pressed
    SCAN_ENTRY(PS2_KB_MASK_LALT, 0, 0),        // 0x38, Left alt pressed
    SCAN_ENTRY(0, u' ', u' '),                 // 0x39, Space bar pressed
    SCAN_ENTRY(PS2_KB_MASK_CAPS_LOCK, 0, 0),   // 0x3A, Capslock pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x3B, F1 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x3C, F2 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x3D, F3 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x3E, F4 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x3F, F5 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x40, F6 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x41, F7 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x42, F8 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x43, F9 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x44, F10 Pressed

    SCAN_ENTRY(PS2_KB_MASK_NUM_LOCK, 0, 0),    // 0x45, Numlock Pressed
    SCAN_ENTRY(PS2_KB_MASK_SCROLL_LOCK, 0, 0), // 0x46, Scroll lock pressed

    SCAN_ENTRY(0, 0, u'7'),                    // 0x47, Keypad 7 pressed
    SCAN_ENTRY(0, 0, u'8'),                    // 0x48, Keypad 8 pressed
    SCAN_ENTRY(0, 0, u'9'),                    // 0x49, Keypad 9 pressed
    SCAN_ENTRY(0, u'-', u'-'),                 // 0x4A, Keypad minus pressed
    SCAN_ENTRY(0, 0, u'4'),                    // 0x4B, Keypad 4 pressed
    SCAN_ENTRY(0, 0, u'5'),                    // 0x4C, Keypad 5 pressed
    SCAN_ENTRY(0, 0, u'6'),                    // 0x4D, Keypad 6 pressed
    SCAN_ENTRY(0, u'+', u'+'),                 // 0x4E, Keypad plus pressed
    SCAN_ENTRY(0, 0, u'1'),                    // 0x4F, Keypad 1 pressed
    SCAN_ENTRY(0, 0, u'2'),                    // 0x50, Keypad 2 pressed
    SCAN_ENTRY(0, 0, u'3'),                    // 0x51, Keypad 3 pressed
    SCAN_ENTRY(0, 0, u'0'),                    // 0x52, Keypad 0 pressed
    SCAN_ENTRY(0, 0, u'.'),                    // 0x53, Keypad period pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x54, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x55, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x56, Unknown

    SCAN_ENTRY(0, 0, 0),                       // 0x57, F11 Pressed
    SCAN_ENTRY(0, 0, 0),                       // 0x58, F12 Pressed

    SCAN_ENTRY(0, 0, 0),                       // 0x59, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x5A, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x5B, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x5C, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x5D, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x5E, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x5F, Unknown

    SCAN_ENTRY(0, 0, 0),                       // 0x60, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x61, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x62, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x63, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x64, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x65, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x66, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x67, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x68, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x69, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x6A, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x6B, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x6C, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x6D, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x6E, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x6F, Unknown

    SCAN_ENTRY(0, 0, 0),                       // 0x70, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x71, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x72, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x73, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x74, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x75, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x76, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x77, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x78, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x79, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x7A, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x7B, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x7C, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x7D, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x7E, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0x7F, Unknown

    SCAN_ENTRY(0, 0, 0),                       // 0x80, Unknown
    emptyEntry,                                // 0x81, Escape released
    SCAN_ENTRY(0, u'1', u'!'),                 // 0x82, 1 released
    SCAN_ENTRY(0, u'2', u'@'),                 // 0x83, 2 released
    SCAN_ENTRY(0, u'3', u'#'),                 // 0x84, 3 released
    SCAN_ENTRY(0, u'4', u'$'),                 // 0x85, 4 released
    SCAN_ENTRY(0, u'5', u'%'),                 // 0x86, 5 released
    SCAN_ENTRY(0, u'6', u'^'),                 // 0x87, 6 released
    SCAN_ENTRY(0, u'7', u'&'),                 // 0x88, 7 released
    SCAN_ENTRY(0, u'8', u'*'),                 // 0x89, 8 released
    SCAN_ENTRY(0, u'9', u'('),                 // 0x8A, 9 released
    SCAN_ENTRY(0, u'0', u')'),                 // 0x8B, 0 released
    SCAN_ENTRY(0, u'-', u'_'),                 // 0x8C, - released
    SCAN_ENTRY(0, u'+', u'='),                 // 0x8D, + released

    SCAN_ENTRY(0, 0, 0x0008),                  // 0x8E, Backspace released
    SCAN_ENTRY(0, 0, 0x0009),                  // 0x8F, Tab released

    SCAN_ENTRY(0, u'q', u'Q'),                 // 0x90, Q released
    SCAN_ENTRY(0, u'w', u'W'),                 // 0x91, W released
    SCAN_ENTRY(0, u'e', u'E'),                 // 0x92, E released
    SCAN_ENTRY(0, u'r', u'R'),                 // 0x93, R released
    SCAN_ENTRY(0, u't', u'T'),                 // 0x94, T released
    SCAN_ENTRY(0, u'y', u'Y'),                 // 0x95, Y released
    SCAN_ENTRY(0, u'u', u'U'),                 // 0x96, U released
    SCAN_ENTRY(0, u'i', u'I'),                 // 0x97, I released
    SCAN_ENTRY(0, u'o', u'O'),                 // 0x98, O released
    SCAN_ENTRY(0, u'p', u'P'),                 // 0x99, P released

    SCAN_ENTRY(0, u'[', u'{'),                 // 0x9A, [ released
    SCAN_ENTRY(0, u']', u'}'),                 // 0x9B, ] released

    SCAN_ENTRY(0, u'\n', u'\n'),               // 0x9C, Enter key released
    SCAN_ENTRY(PS2_KB_MASK_LCTRL, 0, 0),       // 0x9D, Left control released
    SCAN_ENTRY(0, u'a', u'A'),                 // 0x9E, A released
    SCAN_ENTRY(0, u's', u'S'),                 // 0x9F, S released
    SCAN_ENTRY(0, u'd', u'D'),                 // 0xA0, D released
    SCAN_ENTRY(0, u'f', u'F'),                 // 0xA1, F released
    SCAN_ENTRY(0, u'g', u'G'),                 // 0xA2, G released
    SCAN_ENTRY(0, u'h', u'H'),                 // 0xA3, H released
    SCAN_ENTRY(0, u'j', u'J'),                 // 0xA4, J released
    SCAN_ENTRY(0, u'k', u'K'),                 // 0xA5, K released
    SCAN_ENTRY(0, u'l', u'L'),                 // 0xA6, L released
    SCAN_ENTRY(0, u';', u':'),                 // 0xA7, ; released
    SCAN_ENTRY(0, u'\'', u'\"'),               // 0xA8, ' released
    SCAN_ENTRY(0, u'`', u'~'),                 // 0xA9, backtick/tilde released

    SCAN_ENTRY(PS2_KB_MASK_LSHIFT, 0, 0),      // 0xAA, Left shift released

    SCAN_ENTRY(0, u'\\', u'|'),                // 0xAB, \ released

    SCAN_ENTRY(0, u'z', u'Z'),                 // 0xAC, Z released
    SCAN_ENTRY(0, u'x', u'X'),                 // 0xAD, X released
    SCAN_ENTRY(0, u'c', u'C'),                 // 0xAE, C released
    SCAN_ENTRY(0, u'v', u'V'),                 // 0xAF, V released
    SCAN_ENTRY(0, u'b', u'B'),                 // 0xB0, B released
    SCAN_ENTRY(0, u'n', u'N'),                 // 0xB1, N released
    SCAN_ENTRY(0, u'm', u'M'),                 // 0xB2, M released
    SCAN_ENTRY(0, u',', u'<'),                 // 0xB3, , released
    SCAN_ENTRY(0, u'.', u'>'),                 // 0xB4, . released
    SCAN_ENTRY(0, u'/', u'?'),                 // 0xB5, / released

    SCAN_ENTRY(PS2_KB_MASK_RSHIFT, 0, 0),      // 0xB6, Right shift released
    SCAN_ENTRY(0, u'*', u'*'),                 // 0xB7, Keypad star released
    SCAN_ENTRY(PS2_KB_MASK_LALT, 0, 0),        // 0xB8, Left alt released
    SCAN_ENTRY(0, u' ', u' '),                 // 0xB9, Space bar released
    SCAN_ENTRY(0, 0, 0),                       // 0xBA, Capslock released
    SCAN_ENTRY(0, 0, 0),                       // 0xBB, F1 released
    SCAN_ENTRY(0, 0, 0),                       // 0xBC, F2 released
    SCAN_ENTRY(0, 0, 0),                       // 0xBD, F3 released
    SCAN_ENTRY(0, 0, 0),                       // 0xBE, F4 released
    SCAN_ENTRY(0, 0, 0),                       // 0xBF, F5 released
    SCAN_ENTRY(0, 0, 0),                       // 0xC0, F6 released
    SCAN_ENTRY(0, 0, 0),                       // 0xC1, F7 released
    SCAN_ENTRY(0, 0, 0),                       // 0xC2, F8 released
    SCAN_ENTRY(0, 0, 0),                       // 0xC3, F9 released
    SCAN_ENTRY(0, 0, 0),                       // 0xC4, F10 released

    SCAN_ENTRY(0, 0, 0),                       // 0xC5, Numlock released
    SCAN_ENTRY(0, 0, 0),                       // 0xC6, Scroll lock released

    SCAN_ENTRY(0, 0, u'7'),                    // 0xC7, Keypad 7 released
    SCAN_ENTRY(0, 0, u'8'),                    // 0xC8, Keypad 8 released
    SCAN_ENTRY(0, 0, u'9'),                    // 0xC9, Keypad 9 released
    SCAN_ENTRY(0, u'-', u'-'),                 // 0xCA, Keypad minus released
    SCAN_ENTRY(0, 0, u'4'),                    // 0xCB, Keypad 4 released
    SCAN_ENTRY(0, 0, u'5'),                    // 0xCC, Keypad 5 released
    SCAN_ENTRY(0, 0, u'6'),                    // 0xCD, Keypad 6 released
    SCAN_ENTRY(0, u'+', u'+'),                 // 0xCE, Keypad plus released
    SCAN_ENTRY(0, 0, u'1'),                    // 0xCF, Keypad 1 released
    SCAN_ENTRY(0, 0, u'2'),                    // 0xD0, Keypad 2 released
    SCAN_ENTRY(0, 0, u'3'),                    // 0xD1, Keypad 3 released
    SCAN_ENTRY(0, 0, u'0'),                    // 0xD2, Keypad 0 released
    SCAN_ENTRY(0, 0, u'.'),                    // 0xD3, Keypad period released
    SCAN_ENTRY(0, 0, 0),                       // 0xD4, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xD5, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xD6, Unknown

    SCAN_ENTRY(0, 0, 0),                       // 0xD7, F11 released
    SCAN_ENTRY(0, 0, 0),                       // 0xD8, F12 released

    SCAN_ENTRY(0, 0, 0),                       // 0xD9, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xDA, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xDB, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xDC, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xDD, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xDE, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xDF, Unknown

    SCAN_ENTRY(0, 0, 0),                       // 0xE0, Extended codes

    SCAN_ENTRY(0, 0, 0),                       // 0xE1, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE2, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE3, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE4, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE5, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE6, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE7, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE8, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xE9, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xEA, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xEB, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xEC, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xED, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xEE, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xEF, Unknown

    SCAN_ENTRY(0, 0, 0),                       // 0xF0, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF1, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF2, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF3, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF4, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF5, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF6, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF7, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF8, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xF9, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xFA, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xFB, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xFC, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xFD, Unknown
    SCAN_ENTRY(0, 0, 0),                       // 0xFE, Unknown
    SCAN_ENTRY(0, 0, 0)                        // 0xFF, Unknown
};

