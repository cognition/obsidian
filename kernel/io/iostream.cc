#include "iostream.h"

static const char digitLut[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
static const char digitLowerLut[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

//! New line operation object
streamOperationObj endl( streamOpNewLine, 0 );
streamOperationObj streamTab( streamOpTab, 0);
streamOperationObj streamFlush( streamOpFlush, 0);

//! Unsigned hexadecimal number formatting object
const streamFormattingObj hexNumber( STREAM_UPDATE_NUMBER_FORMAT, numberFormatUnsignedHex, 0, 0, 0 );
//! Unsigned decimal number formatting object
const streamFormattingObj decimalNumber( STREAM_UPDATE_NUMBER_FORMAT, numberFormatUnsignedDecimal, 0, 0, 0 );

/*! \brief Wrapper object for \b streamOp operation types
 *  \param op \b streamOp specifying the operation this object will preform on an ioStream object
 *  \param v Value to pass along with corresponding operation command
 *  \sa streamOp, ioStream::operator <<(streamOperationObj &o)
 */
streamOperationObj::streamOperationObj( const streamOp op, const uint32_t v ) : operation( op ), value( v )
{
}

/*! \brief Constructor for a streamFormattingObj object
 *  \param uMask Update mask, specifies fields this object will update if passed to an \b ioStream object
 *  \param n Number format to use
 *  \param nwidth Minimum width of numbers being output, in digits
 *  \param fPrecision Precision of floating point numbers in digits
 *  \param colorChoice Color to be used
 *  \sa numberFormat
 */

streamFormattingObj::streamFormattingObj( uint32_t uMask, numberFormat n, uint16_t nwidth, uint16_t fPrecision, uint32_t colorChoice ) :
	updateMask( uMask ), nf( n ), numberWidth( nwidth ), precision( fPrecision ), color( colorChoice )
{
}

/*! \brief Constructor for an ioStream object
 *  \param dev \b ioDevice object to connect this string to
 *  \param colorChoice Initial color to use for fomatting this stream's output, if applicable
 *  \param numWidth Minimum width, in digits of numbers outputted by this stream
 *  \param fPrecision Precision in digits of floating point numbers output by this stream
 *  \param numFormat Default formatting of numbers that this stream will output
 */

ioStream::ioStream( textOutputDevice &dev, uint32_t colorChoice, uint16_t numWidth, uint16_t fPrecision, numberFormat numFormat )
	: device( dev ), color( colorChoice ), numberWidth( numWidth ), numberPrecision( fPrecision ), nf( numFormat )
{
}

static uint8_t strlen( const char *string )
{
	uint8_t num = 0;

	do
	{
		if( *string == 0 )
			break;
		string++;
		num++;
	} while( num < 255 );

	return num;
}

/*! \brief Outputs a string of text
 *  \param string ASCII formatted text string to output.
 *  \return Reference to self.
 */
ioStream &ioStream::operator <<( const char *string )
{
	uint8_t length = strlen( string );
	device.writeBuffer( string, length );
	return *this;
}

/*! \brief Outputs the contents of a \a String object
 *  \param s String to be output
 *  \return Reference to self
 */
ioStream &ioStream::operator <<( String &s )
{
	device.writeBuffer( s.getBuffer(), s.length() );
	return *this;
}

/*! \brief Outputs an integer
 *  \param num Integer to output
 *  \sa ioStream::printNumber(size_t num)
 *  \return Reference to self
 */
ioStream &ioStream::operator <<( size_t num )
{
	printNumber( num );
	return *this;
}

/*! \brief Outputs the address of a given pointer
 *  \param address Address to print out
 *  \sa ioStream::printNumber(size_t num)
 *  \return Reference to self
 */
ioStream &ioStream::operator <<( void *address )
{
	printNumber( reinterpret_cast <size_t> ( address ) );
	return *this;
}

/*! \brief Preforms a stream operation
 *  \param o \b streamOperationObj object specifiying the operation to preform
 *  \return Reference to self
 */
ioStream &ioStream::operator <<( const streamOperationObj &o )
{
	device.preformOp( o.operation );
	return *this;
}

/*! \brief Specificies the format of the given stream going forward
 *  \param o \b streamFormattingObj specifiying the formatting operations to preform
 *  \return Reference to self
 */
ioStream &ioStream::operator <<( const streamFormattingObj &o )
{
	if( o.updateMask & STREAM_UPDATE_NUMBER_FORMAT )
		nf = o.nf;
	if( o.updateMask & STREAM_UPDATE_NUMBER_WIDTH )
		numberWidth = o.numberWidth;
	if( o.updateMask & STREAM_UPDATE_NUMBER_PRECISION )
		numberPrecision = o.precision;
	if( o.updateMask & STREAM_UPDATE_COLOR )
		color = o.color;
	return *this;

}

void ioStream::printUnsignedDecimal( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr )
{
	char numberString[24];
	char *numberPtr = &numberString[23];
	*numberPtr = 0;
	uint8_t widthCounter = 0;

	do
	{
		numberPtr--;
		*numberPtr = digitLut[num%10];
		num /= 10;
		widthCounter++;
	} while( num );
	outputNumString( numberPtr, width, precision, widthCounter, attr );
}

void ioStream::printSignedDecimal32( uint32_t num, uint8_t width, uint8_t precision, const uint32_t attr )
{
    uint32_t tmp;
	char numberString[24];
	char *numberPtr = &numberString[23];
	*numberPtr = 0;
	uint8_t widthCounter = 0;

	int sign = num>>31;
	tmp = num;
	if(sign)
    {
        --tmp;
        tmp = ~tmp;
    }

    do
    {
        numberPtr--;
        *numberPtr = digitLut[tmp%10];
        tmp /= 10;
        widthCounter++;
    } while( tmp );

	if( sign )
	{
		numberPtr--;
		*numberPtr = '-';
		widthCounter++;
	}

	outputNumString( numberPtr, width, precision, widthCounter, attr );

}

void ioStream::printSignedDecimal64(uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr)
{
    uint64_t tmp;
	char numberString[24];
	char *numberPtr = &numberString[23];
	*numberPtr = 0;
	uint8_t widthCounter = 0;

	int sign = num>>63;
	tmp = num;
	if(sign)
    {
        --tmp;
        tmp = ~tmp;
    }

    do
    {
        numberPtr--;
        *numberPtr = digitLut[tmp%10];
        tmp /= 10;
        widthCounter++;
    } while( tmp );

	if( sign )
	{
		numberPtr--;
		*numberPtr = '-';
		widthCounter++;
	}

	outputNumString( numberPtr, width, precision, widthCounter, attr );

}

void ioStream::printHexLower( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr )
{
	printHex( num, width, precision, attr, digitLowerLut );
}

void ioStream::printHexUpper( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr )
{
	printHex( num, width, precision, attr, digitLut );
}


void ioStream::printHex( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr, const char *lut )
{
	char numberString[24];
	char *numberPtr = &numberString[23];
	*numberPtr = 0;
	uint8_t widthCounter = 0;

    int maxWidth = 16;

	do
	{
		numberPtr--;
		*numberPtr = lut[num & 0xF];
		num >>= 4;
		widthCounter++;
		maxWidth--;
	} while( num && maxWidth );

	outputNumString( numberPtr, width, precision, widthCounter, attr );
}

void ioStream::printOctal( uint64_t num, uint8_t width, uint8_t precision, const uint32_t attr )
{
	char numberString[24];
	char *numberPtr = &numberString[23];
	*numberPtr = 0;
	uint8_t widthCounter = 0;

	do
	{
		numberPtr--;
		*numberPtr = digitLut[num & 0x7];
		num >>= 3;
		widthCounter++;
	} while( num );

	outputNumString( numberPtr, width, precision, widthCounter, attr );
}

void ioStream::printString( const char *string, uint8_t width, uint8_t precision, const uint32_t attr )
{
	uint8_t stringLength = String::strnlen( string, 255 );
	char fillChar;

	if( attr & NUMBER_ATTR_ZERO_FILL )
		fillChar = '0';
	else
		fillChar = ' ';

	if( precision )
		if( precision < stringLength )
			stringLength = precision;
	if( attr & NUMBER_ATTR_LEFT_ALIGN )
	{
        device.writeBuffer( string, stringLength );
        attemptFillChar(fillChar, width, stringLength);
	}
	else
	{
        attemptFillChar(fillChar, width, stringLength);
        device.writeBuffer( string, stringLength );
	}
}

void ioStream::outputNumString( const char *string, uint8_t width, uint8_t precision, uint8_t stringLength, const uint32_t attr )
{
	char fillChar;

	if( attr & NUMBER_ATTR_ZERO_FILL )
		fillChar = '0';
	else
		fillChar = ' ';

	if( precision )
    {
        if(precision < stringLength)
            stringLength = precision;
    }

	if( attr & NUMBER_ATTR_LEFT_ALIGN )
	{
        device.writeBuffer( string, stringLength );
        attemptFillChar(fillChar, width, stringLength);
	}
	else
	{
        attemptFillChar(fillChar, width, stringLength);
        device.writeBuffer( string, stringLength );
	}
}

void ioStream::attemptFillChar(char fillChar, uint8_t width, uint8_t stringLength)
{
    if(stringLength < width)
    {
		uint8_t fillWidth = width - stringLength;
		while( fillWidth )
		{
			putChar( fillChar );
			fillWidth--;
		}
	}
}

/*! \brief Prints out a number, determined by the number format, precision and width of the stream
 *  \param num Number to output
 */
void ioStream::printNumber( size_t num )
{
	switch( nf )
	{
		case numberFormatSignedDecimal:
			printSignedDecimal64( num, numberWidth, numberPrecision, false );
			break;
		case numberFormatUnsignedDecimal:
			printUnsignedDecimal( num, numberWidth, numberPrecision, false );
			break;
		case numberFormatUnsignedHex:
			printHexUpper( num, numberWidth, numberPrecision, false );
			break;
		case numberFormatUnsignedOctal:
			printOctal( num, numberWidth, numberPrecision, false );
			break;
		default:
			break;
	}
}

void ioStream::putChar( const char c )
{
	device.writeBuffer( &c, 1 );
}

textOutputDevice *ioStream::getDevice(void)
{
    return &device;
}
