#ifndef TANDEM_IO_H_
#define TANDEM_IO_H_

#include "textoutdev.h"

class tandemIo : public textOutputDevice
{
    public:
        tandemIo(textOutputDevice &first, textOutputDevice &second);
        virtual ~tandemIo();

		virtual ioDevResult writeBuffer( const void *buffer, uintptr_t length );
		virtual ioDevResult preformOp( const streamOp o );
		virtual ioDevResult echoChar(const char value);
		virtual void getDims(int &rows, int &cols);

    private:
        textOutputDevice &firstDev;
        textOutputDevice &secondDev;
};


#endif // TANDEM_IO_H_
