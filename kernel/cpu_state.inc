
%define KERNEL_DATA_SEG         0x10
%define KERNEL_CODE_SEG         0x8
%define USER_CODE_SEG_32        0x20

; Stack saved register offsets

%define DS_SAVE                 0
%define ES_SAVE                 2
%define FS_SAVE                 4
%define GS_SAVE                 6

%define R8_SAVE                 8
%define R9_SAVE                 16
%define R10_SAVE                24
%define R11_SAVE                32
%define R12_SAVE                40
%define R13_SAVE                48
%define R14_SAVE                56
%define R15_SAVE                64
%define RBP_SAVE                72
%define RDI_SAVE                80
%define RSI_SAVE                88
%define RAX_SAVE                96
%define RBX_SAVE                104
%define RCX_SAVE                112
%define RDX_SAVE                120

%define ISR_VECTOR              128
%define ISR_ERROR               136

%define RIP_SAVE                144
%define CS_SAVE                 152
%define RFLAGS_SAVE             160
%define RSP_AUTOSAVE            168
%define SS_AUTOSAVE             176
