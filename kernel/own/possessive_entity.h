#ifndef POSSESSIVE_ENTITY_H_
#define POSSESSIVE_ENTITY_H_

/* Defines an entity capable of possessing and ownable object
 *
 * Author: Joseph Kinzel */

#include <templates/avltree.cc>

class ownershipRelation
{
    public:
        ownershipRelation(class possessiveEntity *initEntity, class ownableObject *initObj, const uint32_t relationFlags);

        ownableObject *getObjectRef(void) const;

        void setInactive(void);
        uint32_t getFlags(void) const;
        void doNotify(bool acquired);


        class orObjectPtrComparable
        {
            public:
                bool greater(ownershipRelation *first, ownershipRelation *second)
                {
                    return (first->obj > second->obj);
                }

                bool less(ownershipRelation *first, ownershipRelation *second)
                {
                    return (first->obj < second->obj);
                }

                bool greater(ownershipRelation *first, ownableObject *second)
                {
                    return(first->obj > second);
                }

                bool less(ownershipRelation *first, ownableObject *second)
                {
                    return (first->obj < second);
                }
        };

        class orEntityPtrComparable
        {
            public:
                bool greater(ownershipRelation *first, ownershipRelation *second)
                {
                    return (first->entity > second->entity);
                }

                bool less(ownershipRelation *first, ownershipRelation *second)
                {
                    return (first->entity < second->entity);
                }
        };

    private:
        uint32_t flags;
        class possessiveEntity *entity;
        class ownableObject *obj;
};

class ownableObject
{
    public:
        ownableObject(const uint32_t objOwnType);
        ownershipRelation *addOwner(class possessiveEntity *owner, const uint32_t ownFlags);
        void switchActive(class possessiveEntity *entity);

        static const constexpr uint32_t OBJ_OWN_EXCLUSIVE        = 0x0001U;
        static const constexpr uint32_t OBJ_OWN_SHARED           = 0x0002U;
        static const constexpr uint32_t OBJ_OWN_CHANGE_NOTIFY    = 0x0004U;

    private:
        avlTree<class ownershipRelation *, class ownershipRelation::orEntityPtrComparable> owners;
        ownershipRelation *activeOwner;
        uint32_t ownType;
};


class possessiveEntity
{
    public:
        possessiveEntity(const uint64_t ownerId);

        uint64_t getId(void) const;

        bool acquire(ownableObject *obj, const uint32_t requestFlags);
        bool cede(ownableObject *obj);
        bool share(ownableObject *obj, possessiveEntity *other);
    private:
        const uint64_t id;
        avlTree<class ownershipRelation *, class ownershipRelation::orObjectPtrComparable> ownedObjects;
};


#endif // POSSESSIVE_ENTITY_H_
