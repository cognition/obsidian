/* Possessive entity implementation
 *
 * General framework for any entity that can maintain exclusive control over object(s)
 *
 * Author: Joseph Kinzel */

#include "possessive_entity.h"

// ownableObject class

ownableObject::ownableObject(const uint32_t objOwnType) : activeOwner(nullptr), ownType(objOwnType)
{
}

ownershipRelation *ownableObject::addOwner(possessiveEntity *owner, const uint32_t ownFlags)
{
    if((ownType ^ ownFlags) & (OBJ_OWN_SHARED | OBJ_OWN_EXCLUSIVE))
       return nullptr;
    else
    {
        ownershipRelation *obj_or = new ownershipRelation(owner, this, ownFlags);
        owners.insert(obj_or);

        if(activeOwner)
            activeOwner->setInactive();

        activeOwner = obj_or;
        return obj_or;
    }
}

void ownableObject::switchActive(possessiveEntity *entity)
{

}



// possessiveEntity class

possessiveEntity::possessiveEntity(const uint64_t ownerId) : id(ownerId)
{
}

bool possessiveEntity::acquire(ownableObject *obj, const uint32_t requestFlags)
{
    if(ownershipRelation *relation = obj->addOwner(this, requestFlags))
    {
        ownedObjects.insert(relation);
        return true;
    }
    else
        return false;
}

uint64_t possessiveEntity::getId(void) const
{
    return id;
}

bool possessiveEntity::cede(ownableObject *obj)
{
    if(ownershipRelation **relation = ownedObjects.find(obj) )
    {
        ownedObjects.remove(*relation);
        return true;
    }

    return false;
}

bool possessiveEntity::share(ownableObject *obj, possessiveEntity *other)
{
    if(ownershipRelation **relation = ownedObjects.find(obj) )
    {
        (*relation)->setInactive();
        return true;
    }
    else
        return false;
}

//ownershipRelation class

ownershipRelation::ownershipRelation(possessiveEntity *initEntity, ownableObject *initObj, const uint32_t relationFlags) : flags(relationFlags), entity(initEntity), obj(initObj) {};

void ownershipRelation::setInactive(void)
{
    if(flags & ownableObject::OBJ_OWN_CHANGE_NOTIFY)
        doNotify(false);
}

uint32_t ownershipRelation::getFlags(void) const
{
    return flags;
}

void ownershipRelation::doNotify(bool acquired)
{
    //TODO: Finish me!
}


