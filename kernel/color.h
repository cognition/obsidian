#ifndef COLOR_H_
#define COLOR_H_

//Text mode colors
#define TEXT16FG_BLACK                  0
#define TEXT16FG_BLUE                   0x1
#define TEXT16FG_GREEN                  0x2
#define TEXT16FG_CYAN                   0x3
#define TEXT16FG_RED                    0x4
#define TEXT16FG_MAGENTA                0x5
#define TEXT16FG_BROWN                  0x6
#define TEXT16FG_LIGHT_GRAY             0x7
#define TEXT16FG_DARK_GRAY              0x8
#define TEXT16FG_LIGHT_BLUE             0x9
#define TEXT16FG_LIGHT_GREEN            0xA
#define TEXT16FG_LIGHT_CYAN             0xB
#define TEXT16FG_LIGHT_RED              0xC
#define TEXT16FG_LIGHT_MAGENTA          0xD
#define TEXT16FG_YELLOW                 0xE
#define TEXT16FG_WHITE                  0xF

#define TEXT16BG_BLACK                  0
#define TEXT16BG_BLUE                   0x10
#define TEXT16BG_GREEN                  0x20
#define TEXT16BG_CYAN                   0x30
#define TEXT16BG_RED                    0x40
#define TEXT16BG_MAGENTA                0x50
#define TEXT16BG_BOWN                   0x60
#define TEXT16BG_LIGHT_GRAY             0x70

#define TEXT16_BLINK                    0x80

#endif
