#include <gfx/fbconsole.h>
#include <memory/ops.h>
#include <io/io.h>

//! Testing definition
#ifdef FBCON_VALIDATE

//Fix this, printf isn't reliable here
#define assert(x)           if( !(x)) { \
                            asm volatile ("cli\n" \
                                          "hlt\n":: "a" (0xFBDEAD), "b" (__LINE__)); \
                             }

#else

#define assert(x)

#endif


extern frameBufferInfo *bootFb;
extern glyphBuffer *bootGlyphs;

fbConsole::fbConsole() : textOutputDevice(bootFb->bytesPerPixel * bootFb->pixPerScanLine * bootFb->height, 0), pixBuffer( reinterpret_cast<uint8_t * volatile> (bootFb->fbVirtAddr)),
 backBuffer( reinterpret_cast<uint8_t *> (bootFb->bbVirtAddr) ), glyphSrcBuffer(bootGlyphs->pixels.pixelBuffer), currentRow(0), currentCol(0), fbDevManager(nullptr)
{
}

fbConsole::~fbConsole()
{
}

void fbConsole::attachDev(ownableObject *obj)
{
    if(!fbDevManager)
        fbDevManager = new possessiveEntity(0);

    fbDevManager->acquire(obj, ownableObject::OBJ_OWN_SHARED);
}

//! Initializes the frame buffer console
void fbConsole::init(void)
{

	textRows = bootFb->height/bootGlyphs->charHeight;
	topMargin = (bootFb->height%bootGlyphs->charHeight)/2;
	textCols = bootFb->width/bootGlyphs->charWidth;
	leftMargin = (bootFb->width%bootGlyphs->charWidth)/2;
	topMargin *= bootFb->bytesPerPixel*bootFb->pixPerScanLine;
	leftMargin *= bootFb->bytesPerPixel;

	lineBytes = bootFb->bytesPerPixel * bootFb->pixPerScanLine;
	bufferSize = lineBytes * bootFb->height;
	scrollRemoveSize = lineBytes*bootGlyphs->charHeight;
	//scrollRemoveSize = textCols*bootGlyphs->charHeight*bootFb->bytesPerPixel*bootGlyphs->charWidth;
	bufferLineOffset = bootGlyphs->charHeight*bootFb->pixPerScanLine*bootFb->bytesPerPixel;

	charPixWidth = bootGlyphs->charWidth*bootFb->bytesPerPixel;
	glyphLineOffset = bootGlyphs->charWidth*bootFb->bytesPerPixel*0x80U;


 	clear();
}

/*! Validates the boot structure fields to see if a framebuffer is present
 *  \return True if a framebuffer and backbuffer are present, false otherwise
 */
bool fbConsole::isValid(void) const
{
	return ( (bootFb->fbVirtAddr != 0) && (bootFb->bbVirtAddr != 0));
}

/*! Copies a row, or part of one from the backbuffer to the pixel buffer
 *  \param row Row to copy
 *  \param col Number of columns to copy
 */
void fbConsole::copyTextRow(const uint32_t row, const uint32_t col)
{
    uint32_t targetOffset = bufferLineOffset*row;
    const uint32_t lineWidth = charPixWidth*col;

    assert((targetOffset + lineWidth) <= bufferSize);

    for(int i = 0; i < bootGlyphs->charHeight; i++, targetOffset += lineBytes)
        Memory::copy(&pixBuffer[targetOffset], &backBuffer[targetOffset], lineWidth);
}

/*! Prints a single glyph at a given offset in the buffer
 *  \param glpyh ASCII code for glyph
 *  \param targetOffset Offset into the framebuffer in bytes
 */
void fbConsole::printGlyph(const uint8_t glyph, uint32_t targetOffset)
{
	uint32_t sourceOffset = glyph;
	sourceOffset *= charPixWidth;

	assert(targetOffset < bufferSize);
	assert((targetOffset + lineBytes*(bootGlyphs->charHeight - 1) + charPixWidth) < bufferSize);

	for(int row = 0; row < bootGlyphs->charHeight; row++)
	{
		Memory::copy(&backBuffer[targetOffset], &glyphSrcBuffer[sourceOffset], charPixWidth );
		targetOffset += bootFb->pixPerScanLine*bootFb->bytesPerPixel;
		sourceOffset += glyphLineOffset;
	}
}

void fbConsole::outputCharacter(const uint8_t value, uint32_t &targetOffset)
{
    printGlyph(value, targetOffset);
    //If the last character on a line we'll need to adjust
    if((textCols - currentCol) == 1)
    {
        if((textRows - currentRow) > 2)
        {
            //Not on the last line, no need to scroll yet
            copyTextRow(currentRow, currentCol);
            currentRow++;
        }
        else
        {
            //Last line scroll one line
            scrollDown();
        }

        currentCol = 0;
        targetOffset = bufferLineOffset*currentRow + topMargin + leftMargin;
    }
    else
    {
        currentCol++;
        targetOffset += charPixWidth;
    }
}

/*! Outputs text to the screen
 * \param buffer Ascii formatted characters
 * \param length Number of characters in the buffer
 * \return ioSuccess in all cases
 */
ioDevResult fbConsole::writeBuffer( const void *buffer, uintptr_t length )
{
	uint32_t targetOffset =  leftMargin + topMargin + bufferLineOffset * currentRow + charPixWidth*currentCol;
	const uint8_t *values = reinterpret_cast<const uint8_t *>(buffer);

	for(uintptr_t index = 0; index < length; index++)
	    outputCharacter(values[index], targetOffset);

	return ioSuccess;
}

/*! Processes special stream operations
 *  \param o Steam operation to process
 *  \return ioSuccess if the operation is supported and succeeds, ioApiError if the operation is not supported
 */
ioDevResult fbConsole::preformOp( const streamOp o )
{
	switch(o)
	{
		case streamOpClear:
			clear();
			break;
		case streamOpNewLine:
			if(textRows - currentRow == 1)
				scrollDown();
			else
            {
                copyTextRow(currentRow, currentCol);
				currentRow++;
            }
		case streamOpCarriageReturn:
			currentCol = 0;
			break;
		case streamOpTab:
			currentCol += 8;
			currentCol /= 8;
			currentCol *= 8;
			if(currentCol >= textCols)
			{
				currentCol = currentCol >= textCols ? 8:currentCol;
			}
			break;
        case streamOpBackspace:
            //There's three scenarios, either we're just backspacing on the line, we're moving up a line or there's nothing to clear because we're at the start of the screen
            if(currentCol)
                currentCol--;
            else if(currentRow)
            {
                currentRow--;
                currentCol = textCols-1;
            }
            else
                break;
            //Update the screen
            printGlyph(' ', leftMargin + topMargin + bufferLineOffset * currentRow + charPixWidth*currentCol);
            copyTextRow(currentRow, currentCol+1);
            break;
		case streamOpShowCursor:
			break;
		case streamOpHideCursor:
			break;
		case streamOpSetColor:
			break;
        case streamOpFlush:
            if(currentCol)
                copyTextRow(currentRow, currentCol);
            break;
		default:
		    return ioApiError;
			break;
	}
	return ioSuccess;
}

/*! Echo's an input character to the screen
 *  \param value Value to output
 *  \return Always ioSuccess
 */
ioDevResult fbConsole::echoChar(const char value)
{
    uint32_t targetOffset = leftMargin + topMargin + bufferLineOffset * currentRow + charPixWidth*currentCol;

    outputCharacter( (uint8_t)value, targetOffset);
    copyTextRow(currentRow, currentCol);
    return ioSuccess;
}

void fbConsole::getDims(int &rows, int &cols)
{
    rows = textRows;
    cols = textCols;
}

//! Scrolls down a line
void fbConsole::scrollDown(void)
{
    assert(bufferSize == (bootFb->bytesPerPixel*bootFb->pixPerScanLine*bootFb->height));
    assert(scrollRemoveSize == textCols*bootGlyphs->charHeight*bootFb->bytesPerPixel*bootGlyphs->charWidth);
    assert( reinterpret_cast <uintptr_t> (backBuffer) == bootFb->bbVirtAddr );
    assert( bufferSize > scrollRemoveSize);
	Memory::copy(backBuffer, &backBuffer[scrollRemoveSize], bufferSize - scrollRemoveSize);
	Memory::copy( const_cast <uint8_t *>(pixBuffer), backBuffer, bufferSize - scrollRemoveSize);
	Memory::set( const_cast <uint8_t *>(&pixBuffer[bufferSize - scrollRemoveSize]), 0, scrollRemoveSize);
	Memory::set(&backBuffer[bufferSize - scrollRemoveSize], 0, scrollRemoveSize);
}

//! Clears the screen and back buffer
void fbConsole::clear(void)
{
	currentRow = 0;
	currentCol = 0;

	Memory::set( const_cast <uint8_t *>(pixBuffer), 0, bufferSize);
	Memory::set(backBuffer, 0, bufferSize);
}

/*! Sets the color of the text
 *  \param mask Color mask to use
 *  \todo Implement me!
 */
void fbConsole::setColor(uint32_t mask)
{
}
