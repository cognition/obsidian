/* Simple frame buffer device implementation
 *
 * Author: Joseph Kinzel */

#include <bootloader.h>
#include "frame_buffer.h"
#include <refspace/attribute.h>
#include <refspace/resource.h>
#include <memory/virtual.h>

extern frameBufferInfo *bootFb;

frameBufferDev::frameBufferDev(fbConsole *con, Task *initTask, refSpaceObj *linkParent) : Device("Frame buffer device", "fb"), ownableObject(ownableObject::OBJ_OWN_SHARED),
    fbWidth(bootFb->width), fbHeight(bootFb->height),
    fbBytesPerPixel(bootFb->bytesPerPixel), redMask(bootFb->redMask), greenMask(bootFb->greenMask), blueMask(bootFb->blueMask),
    resMask(bootFb->rsvdMask), fbSize(bootFb->totalSize), lineStrideBytes(bootFb->pixPerScanLine*fbBytesPerPixel), byteBuffer( reinterpret_cast <volatile uint8_t *> (bootFb->fbVirtAddr))
    {
        con->attachDev(this);
        linkParent->addChild(this);

        refSpaceAttribute::createU32Attr(bootFb->height, "height", this);
        refSpaceAttribute::createU32Attr(bootFb->width, "width", this);
        refSpaceAttribute::createU32Attr(bootFb->bytesPerPixel, "bytes_per_pixel", this);
        refSpaceAttribute::createU32Attr(bootFb->redMask, "red_mask", this);
        refSpaceAttribute::createU32Attr(bootFb->greenMask, "green_mask", this);
        refSpaceAttribute::createU32Attr(bootFb->blueMask, "blue_mask", this);
        refSpaceAttribute::createU32Attr(bootFb->rsvdMask, "rsvd_mask", this);

        uintptr_t fbPhys;
        virtualMemoryManager::getPhysicalAddress(bootFb->fbVirtAddr, fbPhys);

        new refSpaceResource(String("mem"), this, fbPhys, fbSize, PAGE_WRITE | PAGE_PRESENT | PAGE_CACHE_WC );
    }

bool frameBufferDev::init( void *context)
{
    return true;
}

bool frameBufferDev::isValid(void) const
{
    return ( (bootFb->fbVirtAddr != 0) && (bootFb->bbVirtAddr != 0));
}

blitResult frameBufferDev::shiftUp(const uint32_t pixels, uint8_t *backBuffer)
{
    uint32_t shiftBytes = pixels*lineStrideBytes;
    Memory::copy( const_cast <uint8_t *> (byteBuffer), &backBuffer[shiftBytes], fbSize - shiftBytes);

    return blitResult::blitSuccessful;
}

blitResult frameBufferDev::fillRegion(const uint8_t value, const uint32_t x, const uint32_t y, const uint32_t regionWidth, const uint32_t regionHeight)
{
    uint32_t bufferOffset = x*fbBytesPerPixel + y*lineStrideBytes;

    /*
    if( (x + regionWidth) >= fbWidth )
    {
        if(x >= fbWidth)
            return blitResult::blitFailOffset;
        else
            return blitResult::blitFailSize;
    }

    if( (y + regionHeight) >= fbHeight )
    {
        if(y >= fbHeight)
            return blitResult::blitFailOffset;
        else
            return blitResult::blitFailSize;
    }
    */
    for(int lineCount = 0; lineCount < regionHeight; lineCount++, bufferOffset += lineStrideBytes)
        Memory::set( const_cast <uint8_t *> (&byteBuffer[bufferOffset]), value, regionWidth * fbBytesPerPixel );

    return blitResult::blitSuccessful;
}


blitResult frameBufferDev::doBlit(uint8_t *srcBuffer, const uint32_t x, const uint32_t y, const uint32_t blitWidth, const uint32_t blitHeight, uint32_t blitLineStride)
{
    if( (x + blitWidth) >= fbWidth )
    {
        if(x >= fbWidth)
            return blitResult::blitFailOffset;
        else
            return blitResult::blitFailSize;
    }

    if( (y + blitHeight) >= fbHeight )
    {
        if(y >= fbHeight)
            return blitResult::blitFailOffset;
        else
            return blitResult::blitFailSize;
    }

    uint32_t bufferOffset = 0;
    uint32_t lineOffset = y*lineStrideBytes + x*fbBytesPerPixel;

    for(int lineIndex = 0; lineIndex < blitHeight; lineIndex++, lineOffset += lineStrideBytes, bufferOffset += blitLineStride)
        Memory::copy(const_cast <uint8_t *> (&byteBuffer[lineOffset]), &srcBuffer[bufferOffset], blitLineStride);

    return blitResult::blitSuccessful;
}

uint32_t frameBufferDev::getWidth(void) const
{
    return fbWidth;
}

uint32_t frameBufferDev::getHeight(void) const
{
    return fbHeight;
}

uint32_t frameBufferDev::getBpp(void) const
{
    return fbBytesPerPixel;
}

uint32_t frameBufferDev::getRedMask(void) const
{
    return redMask;
}

uint32_t frameBufferDev::getGreenMask(void) const
{
    return greenMask;
}

uint32_t frameBufferDev::getBlueMask(void) const
{
    return blueMask;
}

uint32_t frameBufferDev::getResMask(void) const
{
    return resMask;
}

uint32_t frameBufferDev::getLineStride(void) const
{
    return lineStrideBytes;
}

uint32_t frameBufferDev::getVirtBase(void) const
{
    return virtBase;
}

