#ifndef FRAMEBUFFER_CONSOLE_H_
#define FRAMEBUFFER_CONSOLE_H_

#include <compiler.h>
#include <bootloader.h>
#include <io/textoutdev.h>
#include <own/possessive_entity.h>

class fbConsole : public textOutputDevice
{
	public:
		fbConsole();
		virtual ~fbConsole();
		virtual ioDevResult writeBuffer( const void *buffer, uintptr_t length );
		virtual ioDevResult preformOp( const streamOp o );
		virtual ioDevResult echoChar(const char value);
		virtual void getDims(int &rows, int &cols);
		void init(void);
		bool isValid(void) const;
		void printFieldOffsets(void) const;

        void attachDev(ownableObject *obj);
    private:
		void printGlyph(const uint8_t glyph, uint32_t targetOffset);
		void outputCharacter(const uint8_t value, uint32_t &targetOffset);
		void scrollDown(void);
		void clear(void);
		void setColor(const uint32_t mask);
        void copyTextRow(const uint32_t row, const uint32_t col);

		uint8_t *pixBuffer;
		uint8_t *backBuffer, *glyphSrcBuffer;
		uint32_t currentRow, currentCol, textRows, textCols, topMargin, leftMargin;
		uint32_t lineBytes, scrollRemoveSize, bufferLineOffset, charPixWidth, glyphLineOffset;
		uint32_t bufferSize;

		possessiveEntity *fbDevManager;
};

#endif
