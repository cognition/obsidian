/* Basic software driven frame buffer class header file
 *
 * Author: Joseph Kinzel */

#ifndef GFX_FRAMEBUFFER_H_
#define GFX_FRAMEBUFFER_H_

#include <device.h>
#include <task.h>
#include <own/possessive_entity.h>
#include "fbconsole.h"

enum class blitResult
{
    blitSuccessful = 0x0001U,
    blitFailSize = 0x0002U,
    blitFailOffset = 0x0003,
    blitFailNotActive = 0x0004
};

class frameBufferDev : public Device, public ownableObject
{
    public:
        frameBufferDev(fbConsole *console, Task *initTask, refSpaceObj *linkParent);
        virtual bool init( void *context);

        bool isValid(void) const;

        blitResult doBlit(uint8_t *srcBuffer, const uint32_t x, const uint32_t y, const uint32_t blitWidth, const uint32_t blitHeight, const uint32_t blitLineStride);
        blitResult shiftUp(const uint32_t pixels, uint8_t *backBuffer);
        blitResult fillRegion(const uint8_t value, const uint32_t x, const uint32_t y, const uint32_t regionWidth, const uint32_t regionHeight);

        uint32_t getWidth(void) const;
        uint32_t getHeight(void) const;
        uint32_t getBpp(void) const;
        uint32_t getRedMask(void) const;
        uint32_t getGreenMask(void) const;
        uint32_t getBlueMask(void) const;
        uint32_t getResMask(void) const;
        uint32_t getLineStride(void) const;
        uint32_t getVirtBase(void) const;
    private:
        uint32_t fbWidth, fbHeight, fbBytesPerPixel, redMask, greenMask, blueMask, resMask;
        uint32_t fbSize, lineStrideBytes;

        union
        {
            uintptr_t virtBase;
            volatile uint8_t *byteBuffer;
            volatile uint32_t *dwordBuffer;
        };
};


#endif // GFX_FRAMEBUFFER_H_
