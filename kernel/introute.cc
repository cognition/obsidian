/* Interrupt routing logic.  Abstracts the various interrupt routing methods (Dual PIC, IOAPIC, Message Signaled Interrupts)
 *
 * Author: Joseph Kinzel */

#include "introute.h"
#include <mbdevs/ioapic.h>
#include <mbdevs/pic.h>
#include <cpu/processor.h>
#include <cpu/localstate.h>
#include <bus/pci.h>


extern "C"
{
    #include <driverapi/dapi_include/driverapi.h>
}


/*! Base constructor
 *  \param baseInt Global interrupt base address for the routing device
 *  \param length count Number of interrupt lines provided by the device
 */
interruptRouter::interruptRouter( uint32_t baseInt, uint32_t count ) : regionObj32( baseInt, count )
{
}

//emptyRouter class
emptyRouter::emptyRouter() : interruptRouter(0, 0)
{
}

void emptyRouter::routeInterrupt( uint32_t COMPILER_UNUSED line, uint32_t COMPILER_UNUSED destination,
	uint8_t COMPILER_UNUSED vector, uint32_t COMPILER_UNUSED flags ) const
{
}

void emptyRouter::maskInterrupt( uint32_t COMPILER_UNUSED line )
{
}

void emptyRouter::unmaskInterrupt( uint32_t COMPILER_UNUSED line )
{
}


//irqRedirectionEntry class
irqRedirectionEntry::irqRedirectionEntry( const uint8_t irqNum, const uint32_t intLine, const uint32_t intFlags ) : line( intLine ), flags( intFlags ), irq( irqNum )
{
}

bool irqRedirectionEntry::operator ==( const irqRedirectionEntry &e ) const
{
	return ( irq == e.irq );
}

bool irqRedirectionEntry::operator ==( const uint8_t val ) const
{
	return ( irq == val );
}

bool irqRedirectionEntry::operator >( const irqRedirectionEntry &e ) const
{
	return ( irq > e.irq );
}

bool irqRedirectionEntry::operator >( const uint8_t val ) const
{
	return ( irq > val );
}

bool irqRedirectionEntry::operator <( const irqRedirectionEntry &e ) const
{
	return (irq < e.irq );
}

bool irqRedirectionEntry::operator <( const uint8_t val ) const
{
	return (irq < val );
}


//! IRQ redirection tree
static avlTree<const irqRedirectionEntry> irqRedirects;
//! Global interrupt routing devices tree
static avlTree<const interruptRouter *, pointerContentsComparable<const interruptRouter> > routers;

/*! Translates an IRQ entry to a global interrupt line
 *  \param irq IRQ entry to translate
 *  \param flags Flags contained in the entry
 *  \return Global interrupt line that maps to requested IRQ line
 */
static uint32_t irqToIntLine( uint8_t irq, uint32_t &flags )
{
	if( const irqRedirectionEntry *entry = irqRedirects.find( irq ))
	{
		flags = entry->flags;
		return entry->line;
	}
	else
        return irq;
}

//! Interrupt management namespace
namespace interruptManager
{

/*! Installs an IOAPIC device
 *  \param parent Parent device of the IOAPIC
 *  \param phys Physical memory base address of the IOAPIC
 *  \param globalBase Global interrupt base address of the IOAPIC
 *  \return True if the device was initialize successfully, false otherwise
 */
bool installIoApic( Device *parent, uint32_t phys, uint32_t globalBase )
{
	ioApic *apic = new ioApic( parent, phys, globalBase );
	if( !apic->init( NULL ) )
	{
		delete apic;
		return false;
	}
	else
	{
		routers.insert( apic );
		return true;
	}
}

/*! Installs a legacy dual 8259A PIC device
 *  \param parent Parent device of the dual PICs
 *  \return True if the device was initialize successfully, false otherwise
 */
bool installDualPic( Device *parent )
{
	//Create the PIC device
	legacyDualPIC *pic = new legacyDualPIC( parent );
	//Attempt to allocate resources and initialize hardware
	if( !pic->init( NULL ) )
	{
		//If that fails destroy the object and return and indicate failure
		delete pic;
		return false;
	}
	else
		return true;
}

/*! Attempts to allocate a single or multiple interrupts for a given device address
 *  \param busId Bus type identifier
 *  \param address Bus address of the device
 *  \param handler Interrupt handler routine pointer
 *  \param requested_count Number of interrupts being requested
 *  \param physicalLine The physical global interrupt line possessed by the device
 *  \param context A context value to be passed to the interrupt handler routine when an interrupt is triggered
 *  \param flags Flags describing the trigger mode and delivery type of the interrupt
 *  \return The number of interrupts allocated to the device, or zero if no interrupts could be allocated to it
 */
int allocateInterrupts(const int busId, const uint64_t address, softInterrupt::intHandler handler, const uint32_t requested_count, const int physicalLine, void *context, const uint32_t flags)
{
    if(busId == BUS_ID_PCI)
    {
        const uint8_t msiOffset = pciBus::getCapabilityOffset(address, PCI_CAP_MSI);
        if(msiOffset)
            return installPciMsiHandler( address, msiOffset, requested_count, handler, context);
    }

    if(physicalLine < 0)
        return 0;

    return installInterruptHandler(physicalLine, handler, context, flags);
}

/*! Installs a handler for a given interrupt line
 *  \param line Global interrupt line to install the handler on
 *  \param handler Interrupt handling routine to be called when interrupt is triggered
 *  \param context Context argument to be passed to the handler
 *  \param flags Flags representing the trigger state and delivery method of the interrupt
 *  \return One if the line was mapped successfully, zero if it could not be allocated
 */
int installInterruptHandler( uint32_t line, softInterrupt::intHandler handler, void *context, uint32_t flags )
{
	uint8_t vector;

	if( const interruptRouter **routingDevicePtr = routers.find( line ) )
    {
        uint32_t requestedCount = 1;
        const interruptRouter *routingDevice = *routingDevicePtr;

        //Get a processor vector for interrupt delivery
        vector = Processor::getBestAvailableVector(flags & INT_FLAGS_TRIGGER_LEVEL, requestedCount );
        if(!vector)
        {
            kprintf("[INTR]: Unable to find valid vector to interrupt line %u\n", line);

            return 0;
        }

        Processor::installSoftIntHandlers( vector, handler, context, 1);
        routingDevice->routeInterrupt( line, localState::getProcId() , vector, flags );
        return 1;
    }
    else
        return 0;
}

/*! Install an IRQ handler for a given IRQ number
 *  \param irq IRQ which will trigger the interrupt
 *  \param handler IRQ Handling routine
 *  \param flags Flags representing the trigger state and delivery method
 *  \return Zero if the IRQ could not be mapped, One if it was mapped successfully
 */
int installIrqHandler( const uint8_t irq, softInterrupt::intHandler handler, void *context, const uint32_t flags )
{
    uint32_t irqFlags = flags;
	uint32_t line = irqToIntLine( irq, irqFlags );
	return installInterruptHandler( line, handler, context, irqFlags );
}

/*! Register an IRQ to Global interrupt line redirect
 *  \param irq IRQ line to be represented by the redirection entry
 *  \param redirLine Global interrupt line the IRQ maps to
 *  \param flags MPS Flags representing the delivery
 */
void registerIrqRedirect( uint8_t irq, uint32_t redirLine, uint32_t flags )
{
	irqRedirectionEntry entry( irq, redirLine, flags );
	irqRedirects.insert( entry );
}

/*! Install PCI MSI handler
 *  \param address PCI Address of the device
 *  \param msiOffset Offset of the MSI entry in
 *  \param handler Interrupt handling routine
 *  \param context Context value to be passed to the interrupt handler
 *  \return True if the handler was installed successfully, false otherwise
 */
int installPciMsiHandler( const uint32_t address, const uint32_t msiOffset, const int interruptCount, softInterrupt::intHandler handler, void *context)
{
    uint32_t msiAddress;
    uint16_t msiMessage;
    uint16_t msiControl;
    uint32_t vector, requestedIntCount;

    pciBus::readWord(address, msiOffset+MSI_CAP_OFF_CONTROL, msiControl);
    pciBus::readDWord(address, msiOffset + MSI_CAP_OFF_ADDRESS, msiAddress);
    if(msiControl & (MSI_CONTROL_64BIT | MSI_CONTROL_VECTOR_MASK) )
        pciBus::readWord(address, msiOffset + MSI_CAP_OFF_MESSAGE64, msiMessage);
    else
        pciBus::readWord(address, msiOffset + MSI_CAP_OFF_MESSAGE32, msiMessage);

    msiControl &= ~MSI_CONTROL_ENABLE;
    pciBus::writeWord(address, msiOffset + MSI_CAP_OFF_CONTROL, msiControl);


    msiControl |= MSI_CONTROL_ENABLE;

    msiAddress &= 0x0000FF3U;
    msiAddress |= ARCH_MSI_ADDR_BASE | ARCH_MSI_ADDR_DEST(localState::getProcId()) | ARCH_MSI_ADDR_DEST_PHYS;

    requestedIntCount = interruptCount;
    vector = Processor::getBestAvailableVector( true, requestedIntCount );
    if(!vector)
        return 0;
    Processor::installSoftIntHandlers( vector, handler, context, requestedIntCount);

    msiMessage &= 0x3800U;
    msiMessage |= ARCH_MSI_DELIVER_FIXED | ARCH_MSI_TRIGGER_LEVEL | ARCH_MSI_LEVEL_ASSERT | vector;

    uint16_t pciControl;

    pciBus::readWord(address, PCI_CONFIG_COMMAND, pciControl);
    pciControl |= PCI_COMMAND_INTERRUPT_DISABLE | PCI_COMMAND_DECODE_MEM | PCI_COMMAND_BUS_MASTER;
    pciBus::writeWord(address, PCI_CONFIG_COMMAND, pciControl);


    if(msiControl & MSI_CONTROL_64BIT)
    {
        pciBus::writeWord(address, msiOffset + MSI_CAP_OFF_MESSAGE64, msiMessage);
        pciBus::writeQWord(address, msiOffset + MSI_CAP_OFF_ADDRESS, msiAddress);
    }
    else
    {
        pciBus::writeWord(address, msiOffset + MSI_CAP_OFF_MESSAGE32, msiMessage);
        pciBus::writeDWord(address, msiOffset + MSI_CAP_OFF_ADDRESS, msiAddress);
    }

    if(msiControl & MSI_CONTROL_VECTOR_MASK)
    {
        uint32_t masks;
        masks = 0;
        pciBus::writeDWord(address, msiOffset + MSI_CAP_OFF_MASK, masks);
    }

    pciBus::writeWord(address, msiOffset + MSI_CAP_OFF_CONTROL, msiControl);

    return requestedIntCount;
}

}
