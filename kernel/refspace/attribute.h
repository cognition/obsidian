#ifndef REFSPACE_ATTRIBUTE_H_
#define REFSPACE_ATTRIBUTE_H_

/* Reference space attributes header
 *
 * Attributes provide traversable descriptions of their parent objects
 *
 * Author: Joseph Kinzel */

#include "refspaceobj.h"

class refSpaceAttribute : public refSpaceObj
{
    public:
        enum rsAttributeType
        {
            RS_ATTR_TYPE_UNINIT = 0,
            RS_ATTR_TYPE_STRING = 0x0001,
            RS_ATTR_TYPE_UINT32 = 0x0002,
            RS_ATTR_TYPE_INT32 = 0x0003,
            RS_ATTR_TYPE_UINT64 = 0x0004,
            RS_ATTR_TYPE_INT64 = 0x0005,
            RS_ATTR_TYPE_BINARY = 0x0006
        };

        union rsAttributeValue
        {
            Vector<uint8_t> *binary;
            uint32_t u32;
            uint64_t u64;
            int32_t i32;
            int64_t i64;
            String *str;
        };


        refSpaceAttribute(const String &refSpaceId, refSpaceObj *parent);
        void setValue(rsAttributeValue val, rsAttributeType valType);

        rsAttributeType getType(void) const;
        rsAttributeValue getValue(void) const;

        static refSpaceAttribute *createU32Attr(const uint32_t value, const String &id, refSpaceObj *parent);
        static refSpaceAttribute *createU64Attr(const uint64_t value, const String &id, refSpaceObj *parent);
        static refSpaceAttribute *createStringAttr(const String &value, const String &id, refSpaceObj *parent);


    private:
        rsAttributeType type;
        rsAttributeValue value;
};

#endif // REFSPACE_ATTRIBUTE_H_
