/* Container space, a reference space that simply contains other reference space objects
 *
 * Author: Joseph Kinzel
 */

#ifndef CONTAINER_SPACE_H_
#define CONTAINER_SPACE_H_

#include "refspaceobj.h"

class containerSpaceObj : public refSpaceObj
{
    public:
        containerSpaceObj();
        containerSpaceObj(const String &initName, refSpaceObj *parent);
};


#endif
