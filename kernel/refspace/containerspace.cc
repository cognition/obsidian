/* Container Space implementation
 *
 * Generic, potentially named, reference space object that can only hold other reference space objects
 *
 * Author: Joseph Kinzel
 */

#include "containerspace.h"

containerSpaceObj::containerSpaceObj(const String &initName, refSpaceObj *parent) : refSpaceObj(initName, rscContainer, rsOrganizational, parent)
{
}
