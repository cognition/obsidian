/* Reference space namespace functions
 *
 * Author: Joseph Kinzel */

#include "refspace.h"
#include <io/io.h>

extern "C"
{
    #include <driverapi/dapi_include/refspace.h>
}

static rootSpaceObj *rootNode = nullptr;

/* rootSpaceObj class */

//! Empty constructor for the root of the reference space
rootSpaceObj::rootSpaceObj() : refSpaceObj(String("\\"), rscContainer, rsOrganizational, nullptr)
{
    rootNode = this;
}

static refSpaceObj *findNamedObject(const String &path)
{
    stringFragment currentFrag(path, 0);
    int nextFragmentStart = 0;
    refSpaceObj *currentPathObj = rootNode;

    if(!rootNode)
    {
        kprintf("Unable to located root node object!\n");
        return nullptr;
    }

    do
    {
        nextFragmentStart = stringFragment::getNextFragment(currentFrag, path, nextFragmentStart, '\\');

        if(!currentFrag.length() )
        {
            ++nextFragmentStart;
            continue;
        }

        currentPathObj = currentPathObj->findChildByName(currentFrag);
        if(!currentPathObj)
        {
            kprintf("Unable to find fragment \'%.*s\'\n", currentFrag.length(), currentFrag.toBuffer() );
            return nullptr;
        }
        else if(currentPathObj->getSpaceType() == rsTerminal)
            return nullptr;




        ++nextFragmentStart;
    } while(nextFragmentStart);

    return currentPathObj;
}

refSpaceResource *refSpace::findResource(const String &path)
{
    if(path.findTokenRev('.') != -1)
        return nullptr;

    int attributeStart = path.findTokenRev('>');

    if(attributeStart == -1)
    {
        kprintf("Unable to find attribute separator in provided query string \'%s\'\n", path.toBuffer());
        return nullptr;
    }
    else if(path.findTokenRev('>', attributeStart-1) != -1)
        return nullptr;
    else if(path.findTokenRev('\\') > attributeStart)
        return nullptr;


    stringFragment attrName(path, attributeStart+1);


    String prefixPath = path.substr(0, attributeStart);

    if(refSpaceObj *parent = findNamedObject(prefixPath) )
    {
        if(refSpaceObj *attrObj = parent->findChildByName(attrName) )
        {
            if(attrObj->getClass() == rscResource)
                return static_cast <refSpaceResource *> (attrObj);
            else
                kprintf("Resource type mismatch!\n");
        }
        else
            kprintf("Unable to find attribute \'%s\' under parent object!\n", attrName.toBuffer());
    }
    else
    {
        kprintf("Unable to find parent object \'%s\'\n", prefixPath.toBuffer() );
    }

    return nullptr;

}

refSpaceAttribute *refSpace::findAttribute(const String &path)
{
    if(path.findTokenRev('>') != -1)
        return nullptr;

    int attributeStart = path.findTokenRev('.');

    if(attributeStart == -1)
    {
        kprintf("Unable to find attribute separator in provided query string \'%s\'\n", path.toBuffer());
        return nullptr;
    }
    else if(path.findTokenRev('.', attributeStart-1) != -1)
        return nullptr;
    else if(path.findTokenRev('\\') > attributeStart)
        return nullptr;


    stringFragment attrName(path, attributeStart+1);


    String prefixPath = path.substr(0, attributeStart);

    if(refSpaceObj *parent = findNamedObject(prefixPath) )
    {
        if(refSpaceObj *attrObj = parent->findChildByName(attrName) )
        {
            if(attrObj->getClass() == rscAttribute)
                return static_cast <refSpaceAttribute *> (attrObj);
            else
                kprintf("Attribute type mismatch!\n");
        }
        else
            kprintf("Unable to find attribute \'%s\' under parent object!\n", attrName.toBuffer());
    }
    else
    {
        kprintf("Unable to find parent object \'%s\'\n", prefixPath.toBuffer() );
    }

    return nullptr;
}

/* Global refspace namespace */
bool refSpace::findNamedChannel(const String &path, namedChannel *&chan )
{
    refSpaceObj *baseObj;

    baseObj = findNamedObject(path);

    if(baseObj)
    {
        if(baseObj->getClass() == rscMsgChannel)
        {
            chan = static_cast <namedChannel *> (baseObj);
            kprintf("Found channel matching %s with %u slots and a size of %u\n", path.toBuffer(), chan->getChannelSlots(), chan->getChannelSize());
            return true;
        }
    }


    return false;
}

extern "C" driverOperationResult refspace_channel_find(const char *channel_name, void **channel)
{
    namedChannel *chan;

    if( refSpace::findNamedChannel(String(channel_name), chan) )
    {
        *channel = static_cast <driverComChannel *> (chan);
        return driverOpSuccess;
    }
    else
        return driverOpNotFound;
}
