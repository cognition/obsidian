/* Refspace base object class implementation
 *
 * Author: Joseph Kinzel */

#include "refspaceobj.h"

/*! Parentless reference space object constructor
 *
 *  \param objName Reference space ID of the object
 *  \param initObjClass Reference space class of the object
 *  \param initObjType Reference space type of the object
 */
refSpaceObj::refSpaceObj(const String &objName, const refSpaceClass initObjClass, const refSpaceType initObjType) :
    children(new radixTree<refSpaceObj> () ),
    refObjName(objName),
    objClass(initObjClass),
    spaceType(initObjType),
    parent(nullptr),

    readPermissions(nullptr), writePermissions(nullptr), enumPermissions(nullptr),

    refBaseNameCounter(0) {};

/*! Constructor for a reference space object with an immediately known parent
 *
 *  \param objName Reference space name of the object
 *  \param initObjClass Reference space class of the object
 *  \param initObjType Reference space object type
 *  \param parentObj Parent object pointer
 */
refSpaceObj::refSpaceObj(const String &objName, const refSpaceClass initObjClass, const refSpaceType initObjType, refSpaceObj *parentObj) :
    children(new radixTree<refSpaceObj> () ),
    refObjName(objName),
    objClass(initObjClass),
    spaceType(initObjType),
    parent(parentObj),

    readPermissions(nullptr), writePermissions(nullptr), enumPermissions(nullptr),

    refBaseNameCounter(0) {

    if(parent)
        parent->registerRefSpaceChild(this);
}

/*! Copy constructor
 *
 *  \param other Source reference space object to copy
 */
refSpaceObj::refSpaceObj(const refSpaceObj &other) :
    children(other.children),
    refObjName(other.refObjName),
    objClass(other.objClass),
    spaceType(other.spaceType),
    parent(other.parent),

    readPermissions(other.readPermissions), writePermissions(other.writePermissions), enumPermissions(other.enumPermissions),

    refBaseNameCounter(other.refBaseNameCounter) {

}

//! Copy assignment
refSpaceObj &refSpaceObj::operator =(refSpaceObj &other)
{
    children = other.children;
    refObjName = other.refObjName;
    objClass = other.objClass;
    spaceType = other.spaceType;
    parent = other.parent;
    readPermissions = other.readPermissions;
    writePermissions = other.writePermissions;
    enumPermissions = other.enumPermissions;

    return *this;
}

/*! Stringifaction function, simply returns the reference object's name
 *
 *  \return A string containing the name of the reference object
 */
const String &refSpaceObj::toString(void) const
{
    return refObjName;
}

void refSpaceObj::registerRefSpaceChild(refSpaceObj *child)
{
    refSpaceObj *entry = children->findOrInsert(child);
    if(entry->refBaseNameCounter)
    {
        child->refBaseNameCounter = ++entry->refBaseNameCounter;
        child->refObjName = entry->refObjName + " #" + String::stringFromDec(child->refBaseNameCounter);
        children->findOrInsert(child);
    }
}

bool refSpaceObj::addChild(refSpaceObj *obj)
{
    registerRefSpaceChild(obj);
    return true;
}

/*! Attempts to find a child object by its name
 *
 *  \param childName fragment representing the child's name
 *
 *  \return Pointer to the child object if found or a null pointer if it could not be found
 */
refSpaceObj *refSpaceObj::findChildByName(const stringFragment &childName)
{
    return children->find( childName );
}

/*! Increments the base name counter value
 *
 *  \return The incremented counter value
 */
uint32_t refSpaceObj::incrementBaseNameCounter(void)
{
    return ++refBaseNameCounter;
}

//! Reference space object class type accessor
refSpaceClass refSpaceObj::getClass(void) const
{
    return objClass;
}

//! Reference space object space type accessor
refSpaceType refSpaceObj::getSpaceType(void) const
{
    return spaceType;
}

