/* Reference space header
 *
 * Author: Joseph Kinzel
 */

#ifndef REFSPACE_H_
#define REFSPACE_H_

#include "attribute.h"
#include "resource.h"
#include "refspaceobj.h"
#include "containerspace.h"
#include <driverapi/named_channel.h>

namespace refSpace
{
    refSpaceAttribute *findAttribute(const String &path);
    refSpaceResource *findResource(const String &path);
    bool findNamedChannel(const String &path, namedChannel *&chan );

};

#endif
