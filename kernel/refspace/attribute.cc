/* Reference space attributes implementation
 *
 * Author: Joseph Kinzel */

#include "attribute.h"

refSpaceAttribute::refSpaceAttribute(const String &refSpaceId, refSpaceObj *parent) : refSpaceObj(refSpaceId, rscAttribute, rsTerminal, parent), type(RS_ATTR_TYPE_UNINIT)
{
    value.u64 = 0;
}

void refSpaceAttribute::setValue(rsAttributeValue val, rsAttributeType valType)
{
    if(type == RS_ATTR_TYPE_BINARY)
        delete value.binary;
    else if(type == RS_ATTR_TYPE_STRING)
        delete value.str;

    value = val;
    type = valType;
}

refSpaceAttribute::rsAttributeType refSpaceAttribute::getType(void) const
{
    return type;
}

refSpaceAttribute::rsAttributeValue refSpaceAttribute::getValue(void) const
{
    return value;
}

refSpaceAttribute *refSpaceAttribute::createU32Attr(const uint32_t value, const String &id, refSpaceObj *parent)
{
    refSpaceAttribute *attr = new refSpaceAttribute(id, parent);

    rsAttributeValue uValue;
    uValue.i64 = 0;
    uValue.u32 = value;

    attr->setValue(uValue, RS_ATTR_TYPE_UINT32);

    return attr;
}

refSpaceAttribute *refSpaceAttribute::createU64Attr(const uint64_t value, const String &id, refSpaceObj *parent)
{
    refSpaceAttribute *attr = new refSpaceAttribute(id, parent);

    rsAttributeValue uValue;
    uValue.u64 = value;

    attr->setValue(uValue, RS_ATTR_TYPE_UINT64);

    return attr;
}

refSpaceAttribute *refSpaceAttribute::createStringAttr(const String &value, const String &id, refSpaceObj *parent)
{
    refSpaceAttribute *attr = new refSpaceAttribute(id, parent);

    rsAttributeValue uValue;
    uValue.str = new String(value);

    attr->setValue(uValue, RS_ATTR_TYPE_STRING);

    return attr;
}

