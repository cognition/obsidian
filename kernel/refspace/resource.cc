/* Reference space resource object implementation
 *
 * Used to contain userspace mappable hardware device resources
 *
 * Author: Joseph Kinzel */

#include "resource.h"

refSpaceResource::refSpaceResource(const String &id, refSpaceObj *parent, const physaddr_t base, const size_t resSize, physaddr_t permissions) :
    refSpaceObj(id, rscResource, rsTerminal, parent), baseAddr(base), resLength(resSize), regPerms(permissions) {


}

hardwareRegion *refSpaceResource::generateMappingRegion()
{
    hardwareRegion *hwRegion = new hardwareRegion(baseAddr, resLength, regPerms);
    return hwRegion;
}
