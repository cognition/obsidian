/* Reference space base object class header
 *
 * Author: Joseph Kinzel */

#ifndef REFSPACE_OBJ_H_
#define REFSPACE_OBJ_H_

#include <compiler.h>
#include <string.h>
#include <driverapi/channels.h>
#include <templates/radixtree.cc>

enum refSpaceClass
{
    rscContainer,
    rscHardwareDev,
    rscHardwareBusController,
    rscMsgChannel,
    rscUserInput,
    rscUserOutput,
    rscFileSystem,
    rscResource,
    rscAttribute,
    rscBeacon,
    rscNetworkConnection
};

enum refSpaceType
{
    rsOrganizational,
    rsBusAddress,
    rsTerminal,
    rsChannel,
    rsPath,
    rsVirtLink,
    rsIpV4,
    rsUrl,
    rsSubspace
};

struct refSpaceBusAddr
{
    uint64_t address;
    uint8_t busId;
};

struct refSpaceChannelInfo
{
    driverComChannel *channel;
    uint32_t read_request_classes;
    uint32_t write_request_classes;
};


class refSpaceObj
{
    public:
        virtual ~refSpaceObj() = default;
        const String &toString(void) const;
        refSpaceObj *findChildByName(const stringFragment &childName);
        bool addChild(refSpaceObj *obj);

        refSpaceObj(const refSpaceObj &other);
        refSpaceObj &operator =(refSpaceObj &other);

        refSpaceClass getClass(void) const;
        refSpaceType getSpaceType(void) const;
    protected:
        refSpaceObj(const String &objName, const refSpaceClass objClass, const refSpaceType objType);
        refSpaceObj(const String &objName, const refSpaceClass objClass, const refSpaceType objType, class refSpaceObj *parentObj);

    private:
        void registerRefSpaceChild(refSpaceObj *child);
        uint32_t incrementBaseNameCounter(void);

        shared_ptr< radixTree<refSpaceObj> > children;

        String refObjName;

        COMPILER_UNUSED refSpaceClass objClass;
        COMPILER_UNUSED refSpaceType spaceType;

        class refSpaceObj *parent;
        COMPILER_UNUSED void *readPermissions, *writePermissions, *enumPermissions;

        int refBaseNameCounter;
};

class rootSpaceObj : public refSpaceObj
{
    public:
        rootSpaceObj();
};

#endif // REFSPACE_OBJ_H_
