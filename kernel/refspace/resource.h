#ifndef REFSPACE_RESOURCE_H_
#define REFSPACE_RESOURCE_H_

#include "refspaceobj.h"
#include <addrspace/hwregion.h>

class refSpaceResource : public refSpaceObj
{
    public:
        refSpaceResource(const String &id, refSpaceObj *parent, const physaddr_t base, const size_t resSize, physaddr_t permissions);
        hardwareRegion *generateMappingRegion();
    private:
        physaddr_t baseAddr;
        size_t resLength;
        physaddr_t regPerms;
};

#endif // REFSPACE_RESOURCE_H_
