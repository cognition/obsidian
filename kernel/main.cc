#include <console.h>
#include <gfx/frame_buffer.h>
#include <gfx/fbconsole.h>
#include <io/io.h>
#include <io/vconman.h>
#include <io/termio.h>
#include <io/tandemio.h>

#include <refspace/containerspace.h>

#include <addrspace/kern_map.h>

#include <color.h>
#include <baseboard.h>
#include <cpu/localstate.h>
#include <memory/memory.h>
#include <templates/vector.cc>
#include <firmware.h>
#include <acpi/acpisys.h>
#include <debug.h>
#include <bus/pci.h>
#include <fs/initrd.h>
#include <fs/vfs.h>
#include <prng.h>
#include <exec/libcache.h>
#include <driverapi/module_loader.h>
#include <heap/operators.h>
#include <templates/variant.cc>
#include <timer.h>
#include <scheduler.h>
#include <gfx/frame_buffer.h>

#include <driverapi/bus_manager.h>

#define ALLOCATION_COUNT        50000

extern ticketLock kprintLock;

extern "C" void *_start, *_roStart, *_dataStart, *_end;

extern "C" void *bspStack, *bspTss, *bspStackEnd;
extern "C" int kernelMain( bootLoaderInfo *bootInfo );
extern "C" int apKernMain( uintptr_t apicBase );
extern "C" uint8_t apActiveFlag;


static serialLine debugConsole(0x3F8);
static bootConsole initConsole;
static fbConsole fbCon;

//! Selects the primary console device based on the availability of frame buffer information
COMPILER_UNUSED static textOutputDevice &determineConsole(void)
{
	if(fbCon.isValid())
	{
		fbCon.init();
		termIo::registerOutputDevice(&fbCon);
		return fbCon;
	}
    else
    {
        initConsole.init();
        termIo::registerOutputDevice(&initConsole);
        return initConsole;
    }
}

COMPILER_UNUSED static textOutputDevice &initDebug(void)
{
    termIo::registerOutputDevice(&debugConsole);
    return debugConsole;
}

COMPILER_UNUSED static textOutputDevice &determineBaseConsole(void)
{
    if(fbCon.isValid())
    {
        fbCon.init();
        return fbCon;
    }
    else
    {
        initConsole.init();
        return initConsole;
    }
}


static tandemIo tandemCon( debugConsole, determineBaseConsole());

COMPILER_UNUSED static textOutputDevice &useTandem(void)
{
    termIo::registerOutputDevice(&tandemCon);
    return tandemCon;
}


static textOutputDevice &bootOutDev = determineBaseConsole();

//ioStream out( initDebug(), TEXT16FG_LIGHT_GRAY | TEXT16BG_BLACK, 0, 0, numberFormatUnsignedHex );
//ioStream out( bootOutDev, TEXT16FG_LIGHT_GRAY | TEXT16BG_BLACK, 0, 0, numberFormatUnsignedHex );
ioStream out( useTandem(), TEXT16FG_LIGHT_GRAY | TEXT16BG_BLACK, 0, 0, numberFormatUnsignedHex);

//Kernel slab initialization
//static __attribute__ ( ( aligned( LARGE_PAGE_SIZE ) ) ) uint8_t initSlabArea[HEAP_SLAB_SIZE];

extern "C" uintptr_t initSlabArea;
static heapProcessorCache bspCache( &initSlabArea );
static localState bspState( &bspCache );

//Static zero length string buffer and pointers, these NEED to be initialized before any zero length string or fragment is potentially used
static char zeroStr[1] = {0};
static StaticVector<char> zeroVect(zeroStr, 1);
static_shared_ptr<Vector<char>> String::zeroLengthString(&zeroVect);


//Reference space initialization
static rootSpaceObj rootSpace;
static containerSpaceObj devSpace( String("device"), &rootSpace);
static containerSpaceObj userIoSpace( String("uio"), &rootSpace);
static containerSpaceObj graphicsSpace( String("gfx"), &rootSpace);
//Basic hardware device initialization
static baseBoard motherBoard(&devSpace);
static acpiServices acpiSubsystem( motherBoard );
static Processor bsp( &motherBoard );
static physicalMemory mainMemory( &motherBoard );

/* NOTE: ANY CLASS STATIC OBJECTS THAT DEPEND ON THE HEAP MUST BE DECLARED IN THIS FILE */
idPool Task::pidBank(1, MAX_PID_VALUE);
static Task initTask(reinterpret_cast <uint64_t> (&bspStack), reinterpret_cast<uint64_t> (&bspStackEnd), 25, 0 );

extern void *bootGlyphs;

static const uintptr_t textAddr = reinterpret_cast <uintptr_t> (&_start);
static const uintptr_t roAddr = reinterpret_cast <uintptr_t> (&_roStart);
static const uintptr_t dataAddr = reinterpret_cast <uintptr_t> (&_dataStart);
static const uintptr_t endAddr = reinterpret_cast <uintptr_t> (&_end);

static void enforcePermissions(void)
{

    const uintptr_t textLength = roAddr - textAddr;
    const uintptr_t roLength = dataAddr - roAddr;

    const uintptr_t execPages = textLength/ARCH_PAGE_SIZE + textLength%ARCH_PAGE_SIZE ? 1:0;
    const uintptr_t roPages = roLength/ARCH_PAGE_SIZE + roLength%ARCH_PAGE_SIZE ? 1:0;

    virtualMemoryManager::changePermissions(textAddr, execPages, PAGE_PRESENT);
    virtualMemoryManager::changePermissions(roAddr, roPages, PAGE_PRESENT | PAGE_NX);
}


COMPILER_UNUSED static inline uintptr_t getTSC()
{
	uintptr_t ret;
	asm volatile (   "mfence\n"
	        "rdtsc\n"
	        "shl $32, %%rdx\n"
	        "or %%rax, %%rdx\n": "=d" ( ret ) :: "rax" );
	return ret;
}

COMPILER_UNUSED static void heapTest(void)
{
    asm volatile ("cli\n"::);
    uint32_t *sizes = new uint32_t[ALLOCATION_COUNT];
    uint8_t **blocks = new uint8_t *[ALLOCATION_COUNT];

    for( uint32_t i = 0; i < ALLOCATION_COUNT; i++ )
	{
	    sizes[i] = random::get()%2048 + 16;
	    //sizes[i] = 64;
	}

	//Heap test
    kprintf( "Beginning allocation test...\n" );
	uint64_t start = getTSC();
    uint8_t *value;

	for( uint32_t i = 0; i < ALLOCATION_COUNT; i++ )
	{
	    uint8_t *buffer = new uint8_t[sizes[i]];
	    //uint8_t *buffer = new uint8_t[32];

	    value = buffer;
	    *buffer = i & 0xFF;
        blocks[i] = buffer;
	}

	uint64_t allocFinish = getTSC();

	for( uint32_t i = 0; i < ALLOCATION_COUNT; i++)
    {
        delete[] blocks[i];
    }

	uint64_t stop = getTSC();

	kprintf( "Allocation test took %u cycles.\n", ( stop - start ) );
	kprintf("Allocation + Free took approximately %u cycles per pair\n", (stop - start)/ALLOCATION_COUNT);
	kprintf("Allocations took approximately %u cycles each.\nFrees took approximately %u cycles each.\n", (allocFinish - start)/ALLOCATION_COUNT, (stop - allocFinish)/ALLOCATION_COUNT);
    kprintf("Value: %p\n", value);
    kprintf("Slab count: %u\n", localState::getProcessorCache()->getSlabCount() );
	delete[] sizes;
	delete[] blocks;

}

static uint16_t last_input_slot = 0;

static void uinput_reporter(void *context, const uint64_t slot)
{
    uint16_t uinput_value, bytes_read;
    driverComChannel *uinput_channel = reinterpret_cast <driverComChannel *> (context);
    if(slot < last_input_slot)
    {
        for(; last_input_slot < uinput_channel->getChannelSlots(); last_input_slot++)
        {
            bytes_read = uinput_channel->read(last_input_slot, reinterpret_cast <uint8_t *> (&uinput_value), 2);
            if(bytes_read)
            {
                kprintf("Got UINPUT value %u\n", uinput_value);
            }
        }

        last_input_slot = 0;
    }

    for(; last_input_slot <= slot; last_input_slot++)
    {
        bytes_read = uinput_channel->read(last_input_slot, reinterpret_cast <uint8_t *> (&uinput_value), 2);
        if(bytes_read)
        {
            kprintf("Got UINPUT value %u\n", uinput_value);
        }
    }
    if(last_input_slot == uinput_channel->getChannelSlots())
        last_input_slot = 0;
}


static void initStandardChannels(void)
{
    sync_object *uinput_cb;
    namedChannel *inputChannel = new namedChannel("input", rscMsgChannel, 128, 8192);
    userIoSpace.addChild(inputChannel);


    sync_create_callback_obj(&uinput_cb, static_cast <driverComChannel *> (inputChannel) , &uinput_reporter);
    inputChannel->subscribe(uinput_cb, DAPI_CHANNEL_TRIGGER_PUBLISH);
}

extern "C" int kernelMain( bootLoaderInfo *bootInfo )
{
    uintptr_t phys;
    enforcePermissions();

    kprintf( "Obsidian kernel booting...\n" );
    kernelSpace::registerRegion(regionObj64(KERNEL_VSPACE_START, roAddr - KERNEL_VSPACE_START), "Kernel code" );
    kernelSpace::registerRegion(regionObj64(roAddr, endAddr - roAddr), "Kernel data");
    kernelSpace::registerRegion(regionObj64(endAddr, bootInfo->virtEnd - endAddr), "Boot loaded data");

    heapProcessorCache::setRedZoneEnd(bootInfo->virtEnd);
	kprintf( "Boot info location: %X Physical memory mapping end: %X\n", bootInfo, bootInfo->physEnd );
	kprintf( "Initial ramdisk location: %p Length=%X\n", bootInfo->initrdAddress, bootInfo->initrdLength);

	//Print out basic boot processor information
	bsp.printInfo();
	Processor::initVectorAlloc();

	random::seed(Timer::readTSC());
	//Initialize physical and virtual memory management
	if(!mainMemory.init( bootInfo ))
    {
        kprintf("PMEM: Initialization failed!\n");
        return 0;
    }

    if(!moduleLoader::init())
        kprintf("Failed to initialize module loader...\n");

    //Relocate the boot console to high memory
    initConsole.relocate();
	//Mount the initial ramdisk image
	if(!initRd::initialize(bootInfo->initrdAddress, bootInfo->initrdLength))
	{
		kprintf("INIT: Error initializing ramdisk!\n");
		return 0;
	}

	//Load PCI driver database
	if( !pciBus::loadDriverDb("/initrd/drivers/bus_pci.dbase"))
        return 0;

    if( !registerBusCoordinatorDriver(BUS_ID_USB, String("/initrd/drivers/modules/usb_coord.so"), String("Universal Serial Bus")) )
    {
        kprintf("Unable to register USB bus coordinator!\n");
        return 0;
    }

	kprintf("KERN: ACPI RSDP Should be at %X Bootinfo structure: %X\n", bootInfo->acpiRSDP, bootInfo);
	//Locate the ACPI Root pointer before ACPI Initialization
	uintptr_t rsdpAddr = Firmware::getAcpiRSDP(bootInfo);
	kprintf("KERN: Located RSDP @ %X\n", rsdpAddr);

	termIo::init();

	if(!virtualMemoryManager::getPhysicalAddress(0, phys))
        virtualMemoryManager::mapPages(0, 0, LARGE_PAGE_SIZE/REGULAR_PAGE_SIZE, PAGE_PRESENT | PAGE_WRITE, false);

    initStandardChannels();

    //Initialize ACPI
	if( !acpiSubsystem.initialize(&initTask) )
	{
		kprintf( "ERROR: Failed to initialize ACPI Services!" );
		asm volatile ("cli\n"
                        "hlt\n"::);
		return 0;
	}
	else
        kprintf("[ACPI]: Initialization complete!\n");


	//TODO: Still needs to be fixed/tweaked
	//virtualMemoryManager::unmapIdentity();

	//File I/O Test

    /*
	fileHandle *sample = Vfs::open("/initrd/sample.txt", 0);
	if(!sample)
		kprintf("Unable to open sample file on initrd.\n");
	fileHandle *driverFile = Vfs::open("/initrd/drivers/drivers.dbase", 0);
	if(!driverFile)
		kprintf("Unable to open driver data base file on initrd.\n");
    */
    /*
   heapTest();

   asm volatile ("cli\n"
                 "hlt\n"::);
    */


    /*

    //Initialize the library cache
    libCache::init();
    libCache::addEntry("libc.so", "/initrd/lib/libc.so");
    libCache::addEntry("ld.so", "/initrd/lib/ld.so");

    int outputRows, outputCols;
    bootOutDev.getDims(outputRows, outputCols);
    kprintf("Attempting to initialize virtual console system.  Text output has %u rows and %u columns\n", outputRows, outputCols);
    virtConsoleManager::addOutputEndpoint(&bootOutDev, outputRows, outputCols);
    virtConsoleManager::setDefaultSession( String("/initrd/testapp") );
    */

    if(fbCon.isValid())
    {
        new frameBufferDev(&fbCon, localState::getSched()->getCurrentTask(), &graphicsSpace );
        kprintf("Framebuffer device initialized!\n");
        if(refSpaceAttribute *attr = refSpace::findAttribute(String("\\gfx\\fb.width") ))
        {
            refSpaceAttribute::rsAttributeValue value = attr->getValue();

            kprintf("Frame buffer has width %u\n", value.u32 );
        }
        else
            kprintf("Unable to find width attribute of frame buffer!\n");

        if(refSpaceAttribute *attr = refSpace::findAttribute(String("\\gfx\\fb.height") ))
        {
            refSpaceAttribute::rsAttributeValue value = attr->getValue();

            kprintf("Frame buffer has height %u\n", value.u32 );
        }
        else
            kprintf("Unable to find height attribute of frame buffer!\n");


    }

    do
    {
        Scheduler::sleep(5000);
        kprintf("Still awake..\n");
        heapTest();
    }
    while(1);

    kprintf("Kern done.\n");
	return 0;
}

extern "C" int apKernMain( uintptr_t apicBase )
{
	Processor *proc = new Processor( &motherBoard );

	proc->setId( localState::getProcId() );

	if( !proc->init( &apicBase ) )
	{
        kprintf("Initialization for processor %u failed!\n", proc->getId());
        asm volatile ("cli\n"
                      "hlt\n"::);
	}
	else
        kprintf("Initialized AP with ID: %u Base: %X\n", proc->getId(), apicBase);


    pid_t idle, cleanup;
    if(Task::allocatePid(idle) && Task::allocatePid(cleanup))
    {
        COMPILER_UNUSED processorScheduler *sched = new processorScheduler( proc, NULL, idle, cleanup );
        sched->executeCurrent();
    }
	kprintf("Processor %u has reached an invalid state!\n", proc->getId() );
	asm volatile ("cli\n"
               "hlt\n"::);
	return 0;
}
