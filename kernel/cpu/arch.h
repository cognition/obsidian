#ifndef SYSTEM_ARCH_H_
#define SYSTEM_ARCH_H_

//Architecture page sizes
#define ARCH_PAGE_SIZE              0x1000

#define ARCH_RESERVED_MEMORY_SIZE   0x2000

//Architectural definition
#define ARCH_VIRTUAL_NON_FIXED_BITS 48

//MSI Address format values and masks
#define ARCH_MSI_ADDR_BASE          0xFEE00000U

#define ARCH_MSI_ADDR_DEST(x)       ((x)<<12)

#define ARCH_MSI_ADDR_REDIR         0x8U
#define ARCH_MSI_ADDR_DEST_LOG      0x4U
#define ARCH_MSI_ADDR_DEST_PHYS     0


//MSI Message format masks
#define ARCH_MSI_TRIGGER_LEVEL      0x8000U
#define ARCH_MSI_TRIGGER_EDGE       0
#define ARCH_MSI_LEVEL_DEASSERT     0
#define ARCH_MSI_LEVEL_ASSERT       0x4000U

#define ARCH_MSI_DELIVER_FIXED      0
#define ARCH_MSI_DELIVER_LPRIO      0x10U
#define ARCH_MSI_DELIVER_NMI        0x40U
#define ARCH_MSI_DELIVER_EXTINT     0x70U

#endif // SYSTEM_ARCH_H_
