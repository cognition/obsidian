#include "localstate.h"

//! BSP Constructor
localState::localState( heapProcessorCache *hpc ) : heapCache( hpc ), proc( NULL ), procId(0)
{
	//! Point the GS BASE and KERNEL GS BASE msr registers to this structure
	uintptr_t selfAddress = reinterpret_cast <uintptr_t> ( this );
	asm ( "wrmsr\n":: "c" ( MSR_KERN_GS_BASE ), "a" ( selfAddress ), "d" ( selfAddress>>32 ) );
	asm ( "wrmsr\n":: "c" ( MSR_GS_BASE ), "a" ( this ), "d" ( selfAddress>>32 ) );
}

//! Ap Constructor
localState::localState( uint32_t apicId, uint32_t acpiIdValue ) : heapCache(new heapProcessorCache), procId(apicId), acpiId(acpiIdValue)
{
	uSecApicTimerConversion = getApicScaleUsec();
	hundreadNanoTscConversion = getTscScaleHundredNano();
	virtualMemoryManager::allocateQuickMapEntries(quickMapAddr, quickMapEntry, quickMapLargeAddr, quickMapLargeEntry);
}
