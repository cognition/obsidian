#ifndef LOCAL_STATE_H_
#define LOCAL_STATE_H_

#include <compiler.h>
#include <heap/cache.h>
#include <cpu/processor.h>
#include <scheduler.h>

//! Object which describes the local kernel state cache for a logical processor object
class COMPILER_PACKED localState
{
	public:
		localState( heapProcessorCache *hpc );
		localState( uint32_t apicId, uint32_t apciIdValue );

		/*! Gets the local cache used by the heap for this processor
		 *  \return heapLocalCache object pointer to the local heap cache of this processor
		 */
		static inline heapProcessorCache *getProcessorCache(void)
		{
			heapProcessorCache *ret;
			asm ( "mov %%gs:16, %0\n": "=r" ( ret ) );
			return ret;
		}

		/*! Sets the feature flags field for the current processor
		 *  \param featureMask Bits describing the feature set supported by this processor
		 */
		static inline void setFeatures( uint64_t featureMask )
		{
			asm ( "mov %0, %%gs:0\n":: "r" ( featureMask ) );
		}

		static inline uint64_t getFeatures(void)
		{
			uint64_t ret;
			asm ( "mov %%gs:0, %0\n": "=r" ( ret ): );
			return ret;
		}
		/*! Sets the extended feature flags field for the processor executing this code
		 *  \param extFeatureMask Bits describing the extended feature set supported by this processor
		 */
		static inline void setExtFeatures( uint64_t extFeatureMask )
		{
			asm ( "mov %0, %%gs:8\n":: "r" ( extFeatureMask ) );
		}

		static inline uint64_t getExtFeatures(void)
		{
			uint64_t ret;
			asm ( "mov %%gs:8, %0\n": "=r" ( ret ) : );
			return ret;
		}

		/*! Returns a pointer to the \a Processor object for the current processor
		 *  \return Pointer to the \a Processor object for this processor
		 */
		static inline Processor *getProc(void)
		{
			Processor *ret;
			asm ( "mov %%gs:24, %0\n": "=r" ( ret ): );
			return ret;
		}

		/*! Sets the processor object for the current processor
		 *  \param value Pointer to the processor object for this processor
		 */
		static inline void setProc( Processor *value )
		{
			asm ( "mov %0, %%gs:24\n":: "r" ( value ) );
		}

		/*! Retrieves the current processor's APIC ID
		 *  \return APIC ID
		 */
		static inline uint32_t getProcId(void)
		{
			uint32_t ret;
			asm ( "mov %%gs:32, %0\n": "=r" ( ret ): );
			return ret;
		}

		/*! Retrieves the processor's scheduler
		 *  \return Pointer to the processor's scheduler
		 */
		static inline processorScheduler *getSched(void)
		{
			processorScheduler *ret;
			asm ( "movq %%gs:40, %0\n": "=r" ( ret ): );
			return ret;
		}

		/*! Stores a pointer to the task processor's scheduler
		 *  \param sched Pointer to the processor's scheduler
		 */
		static inline void setSched( processorScheduler *sched )
		{
			asm ( "movq %0, %%gs:40\n":: "r" ( sched ) );
		}

		/*! Sets the scale value that converts APIC timer ticks into microseconds
		 *  \param scale Divisor used to convert the APIC timer to microseconds
		 */
		static inline void setApicScaleUsec( uint64_t scale )
		{
			asm ( "movq %0, %%gs:48\n":: "r" ( scale ): );
		}

		/*! Retrieves the divisor value to convert APIC timer ticks into microseconds
		 *  \return Divisor used to convert APIC timer ticks into microseconds
		 */
		static inline uint64_t getApicScaleUsec(void)
		{
			uint64_t ret;
			asm ( "movq %%gs:48, %0\n": "=r" ( ret ): );
			return ret;
		}

		static inline void setTscScaleHundredNano( uint64_t scale )
		{
			asm( "movq %0, %%gs:56\n":: "r" ( scale ) );
		}

		static inline uint64_t getTscScaleHundredNano(void)
		{
			uint64_t ret;
			asm volatile ( "movq %%gs:56, %0\n": "=r" ( ret ): );
			return ret;
		}

		static inline void setKernStackPtr(uintptr_t address)
		{
			asm volatile ( "movq %0, %%gs:64\n":: "r" (address));
		}

		static inline void setUserStackPtr(uintptr_t address)
		{
			asm volatile ( "movq %0, %%gs:72\n":: "r" (address));
		}

		static uint64_t getUserStackPtr(void)
		{
			uint64_t ret;
			asm volatile ("movq %%gs:72, %0\n": "=r" (ret) : );
			return ret;
		}

		static inline physaddr_t *getQuickMapEntry(void)
		{
		    physaddr_t *ret;
		    asm volatile ("movq %%gs:80, %0\n": "=r" (ret): );
		    return ret;
		}

		static inline uintptr_t getQuickMapAddr(void)
		{
		    uintptr_t ret;
		    asm volatile ("movq %%gs:88, %0\n": "=r" (ret):);
		    return ret;
		}

        static inline physaddr_t *getQuickMapLargeEntry(void)
		{
		    physaddr_t *ret;
		    asm volatile ("movq %%gs:96, %0\n": "=r" (ret): );
		    return ret;
		}

		static inline uintptr_t getQuickMapLargeAddr(void)
		{
		    uintptr_t ret;
		    asm volatile ("movq %%gs:104, %0\n": "=r" (ret):);
		    return ret;
		}

        static inline void setQuickMapEntry(physaddr_t *entry)
		{
		    asm volatile ("movq %0, %%gs:80\n":: "r" (entry));
		}

		static inline void setQuickMapAddr(const uintptr_t addr)
		{
		    asm volatile ("movq %0, %%gs:88\n":: "r" (addr));
		}

        static inline void setQuickMapLargeEntry(physaddr_t *entry)
		{
		    asm volatile ("movq %0, %%gs:96\n":: "r" (entry) );
		}

		static inline void setQuickMapLargeAddr(const uintptr_t addr)
		{
		    asm volatile ("movq %0, %%gs:104\n":: "r" (addr));
		}


	private:
		//! Bitmask describing the feature set of a processor
		COMPILER_UNUSED uint64_t features;
		//! Bitmask describing the extended feature set of a processor
		COMPILER_UNUSED uint64_t extFeatures;
		//! Pointer to the local heap cache for the processor
		COMPILER_UNUSED heapProcessorCache *heapCache;
		//! Pointer to the \a Processor object for the processor
		COMPILER_UNUSED Processor *proc;
		//! APIC ID for the processor
		COMPILER_UNUSED uint32_t procId;
		//! ACPI Processor Id
		COMPILER_UNUSED uint32_t acpiId;
		//! Pointer to processor's scheduler
		COMPILER_UNUSED processorScheduler *scheduler;
		//! LAPIC conversion factor from ticks to microseconds
		COMPILER_UNUSED uint64_t uSecApicTimerConversion;
		//! TSC conversion factor from cycles to 100 nanoseconds
		COMPILER_UNUSED uint64_t hundreadNanoTscConversion;
		//! Current task's kernel stack start
		COMPILER_UNUSED uintptr_t currentTaskKernStackStart;
		//! Current task's user stack save value
		COMPILER_UNUSED uintptr_t currentTaskUserSave;

		//! Quick page map/unmap entry
		COMPILER_UNUSED physaddr_t *quickMapEntry;
		//! Quick page map/unmap virtual address
		COMPILER_UNUSED uintptr_t quickMapAddr;

		//! Quck page map/unmap large entry
		COMPILER_UNUSED physaddr_t *quickMapLargeEntry;
		//! Quick large page map/unmap virtual address
		COMPILER_UNUSED uintptr_t quickMapLargeAddr;
};

#endif
