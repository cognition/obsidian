#include "bits.h"

namespace Bits
{

    template<> uint16_t getMsb<uint16_t>(const uint16_t value)
    {
        return getMsb16(value);
    }

    template<> uint32_t getMsb<uint32_t>(const uint32_t value)
    {
        return getMsb32(value);
    }

    template<> uint64_t getMsb<uint64_t>(const uint64_t value)
    {
        return getMsb64(value);
    }
}
