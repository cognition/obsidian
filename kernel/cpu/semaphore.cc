#include "semaphore.h"
#include "localstate.h"
#include <io/io.h>

semaphoreRequest::semaphoreRequest(const uint32_t requested) : requestedUnits(requested), flags(SR_FLAGS_VALID) {};

bool semaphoreRequest::hasObtained(void) const
{
    return (flags & SR_FLAGS_OBTAINED);
}

bool semaphoreRequest::invalidate(void)
{
    uint32_t expected, resulting;
    expected = SR_FLAGS_VALID;
    resulting = 0;
    return __sync_bool_compare_and_swap(&flags, expected, resulting);
}

bool semaphoreRequest::service(void)
{
    uint32_t expected, resulting;
    expected = SR_FLAGS_VALID;
    resulting = SR_FLAGS_VALID | SR_FLAGS_SERVICING;
    return __sync_bool_compare_and_swap(&flags, expected, resulting);
}

bool semaphoreRequest::inService(void) const
{
    return (flags & SR_FLAGS_SERVICING);
}

bool semaphoreRequest::clearService(void)
{
    uint32_t expected, resulting;
    expected = SR_FLAGS_VALID | SR_FLAGS_SERVICING;
    resulting = SR_FLAGS_VALID;
    return __sync_bool_compare_and_swap(&flags, expected, resulting);
}

bool semaphoreRequest::setObtained(void)
{
    uint32_t expected, resulting;
    expected = SR_FLAGS_VALID | SR_FLAGS_SERVICING;
    resulting = SR_FLAGS_VALID | SR_FLAGS_OBTAINED;
    return __sync_bool_compare_and_swap(&flags, expected, resulting);
}

uint32_t semaphoreRequest::getRequested(void) const
{
    return requestedUnits;
}


//semaphoreObj class
semaphoreObj::semaphoreObj( uint32_t maxUnitsValue, uint32_t initUnits ) : maxUnits( maxUnitsValue ), units( initUnits )
{
}

/*! Attempt to obtain units from the semaphore in a given time frame
 *  \param requestedUnits Number of units being requested from the semaphore
 *  \param timeoutLimit Amount of time to wait for the units to be obtained, a value of 0xFFFF representing an infinite wait
 *  \return True if the units were acquired from the semaphore successfully, false if they were not
 */
bool semaphoreObj::wait( uint32_t requestedUnits, uint16_t timeoutLimit )
{
    spinLock lock(&sLock);
    lock.acquire();
	while( queue.isEmpty() )
	{
		if( obtain( requestedUnits ) )
        {
            lock.release();
            return true;
        }
	}

    shared_ptr<semaphoreRequest> request( new semaphoreRequest(requestedUnits) );
	//Enqueue in wait queue
    queue.enter(request);
    lock.release();
    taskWaitCurrent( this, timeoutLimit);
    if( request->hasObtained() )
        return true;
    while(!request->invalidate() )
    {
        if(request->hasObtained() )
            return true;
    }

    return false;
}

/*! Attempts to obtain units from a semaphore
 *  \param unitsRequested Number of units being requested from the semaphore
 *  \return True if the units were obtained from the semaphore, false otherwise
 */
bool semaphoreObj::obtain( uint32_t unitsRequested )
{
		bool result;
		asm volatile ("mov (%%rsi), %%eax\n"
					  "1:\n"
					  "mov %%eax, %%edx\n"
					  "sub %%edi, %%edx\n"
					  "jl 2f\n"
					  "lock cmpxchgl %%edx, (%%rsi)\n"
					  "jz 2f\n"
					  "pause\n"
					  "jmp 1b\n"
					  "2:\n"
					  "setz %%al\n": "=a" (result): "S" (&units), "D" (unitsRequested) : "cc", "edx");
		return result;

}

/*! Signal units to a semaphore
 *  \param unitsReturned Number of units to return to the semaphore
 *  \return True if the number of units returned did not exceeds the semaphore's capacity, false if returning the units would exceed the semaphore's capacity
 */
bool semaphoreObj::signal( uint32_t unitsReturned )
{
	uint32_t value, newValue;
    do
	{
		value = units;
		newValue = value + unitsReturned;
		if(newValue > maxUnits)
			return false;
	} while( !__sync_bool_compare_and_swap(&units, value, newValue) );

	while( !queue.isEmpty() )
	{
		shared_ptr<semaphoreRequest> &req = queue.frontRef();
		if( req->service() )
        {
            if(obtain(req->getRequested()))
            {
                queue.pop();
                req->setObtained();
            }
            else
                req->clearService();
            break;
        }
        else
            queue.pop();
	}
    return true;
}
