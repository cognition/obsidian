#ifndef PROCESSOR_H_
#define PROCESSOR_H_

#include "arch.h"

//Segment selector values
#define KERNEL_CODE_64              0x8
#define KERNEL_DATA_SEG             0x10
#define KERNEL_TSS_SELECTOR         0x50

#define USER_CODE_32                0x20
#define USER_DATA_32                0x28
#define USER_CODE_64                0x30
#define USER_DATA_64                0x38

//Gdt entry type masks
#define GDT_ENTRY_TSS               0x8900

//Descriptor table entry types
#define DT_TYPE_CODE                0x1800
#define DT_TYPE_DATA                0x1000
#define DT_TYPE_PRESENT             0x8000

#define DT_TYPE_32BIT               0x400000

//MSR Values
#define MSR_APIC_BASE               0x1B
#define MSR_X2APIC_OFFSET           0x800
#define MSR_X2APIC_ICR              0x830

#define MSR_KERN_GS_BASE            0xC0000102U
#define MSR_GS_BASE                 0xC0000101U
#define MSR_FS_BASE                 0xC0000100U

//LAPIC Base MSR Values

#define LAPIC_ENABLE_X2APIC         0x400
#define LAPIC_ENABLE_GLOBAL         0x800
#define LAPIC_IS_BSP                0x100

//Processor LAPIC Reserved Vectors
#define VECTOR_LAPIC_SPURIOUS       0x20
#define VECTOR_LAPIC_TM             0x21
#define VECTOR_LAPIC_TIMER          0x22
#define VECTOR_LAPIC_ERROR          0x23
#define VECTOR_LAPIC_PMC            0x24

//LAPIC Register offsets
#define LAPIC_ID_REG                0x020U

#define LAPIC_EOI_REG               0x0B0U

#define LAPIC_SPURIOUS_VECT         0x0F0U

#define LAPIC_ISR_BASE              0x100U
#define LAPIC_TMR_BASE              0x180U
#define LAPIC_IRR_BASE              0x200U

#define LAPIC_TIMER_VECT            0x320U
#define LAPIC_TM_VECT               0x330U
#define LAPIC_PMC_VECT              0x340U

#define LAPIC_LINT0_VECT            0x350U
#define LAPIC_LINT1_VECT            0x360U
#define LAPIC_ERROR_VECT            0x370U

//LAPIC Timer registers
#define LAPIC_TIMER_INIT_REG        0x380U
#define LAPIC_TIMER_CURRENT_REG     0x390U
#define LAPIC_TIMER_DIV_REG         0x3E0U


//LAPIC Setting bitmasks
#define LAPIC_SPURIOUS_SOFT_ENABLE  0x100U

#define LAPIC_DELIVERY_FIXED        0

#define LAPIC_TIMER_ONESHOT         0

//LAPIC Timer divide values
#define LAPIC_TIMER_DIV_1           0xB
#define LAPIC_TIMER_DIV_2           0
#define LAPIC_TIMER_DIV_4           0x1
#define LAPIC_TIMER_DIV_8           0x2
#define LAPIC_TIMER_DIV_16          0x3
#define LAPIC_TIMER_DIV_32          0x8
#define LAPIC_TIMER_DIV_64          0x9
#define LAPIC_TIMER_DIV_128         0xA


//LAPIC ICR Values

#define LAPIC_DELIVERY_FIXED        0
#define LAPIC_DELIVERY_LOWEST_PRIO  0x100
#define LAPIC_DELIVERY_SMI          0x200
#define LAPIC_DELIVERY_NMI          0x400
#define LAPIC_DELIVERY_INIT         0x500
#define LAPIC_DELIVERY_STARTUP      0x600

#define LAPIC_DELIVERY_ASSERT       0x4000

#define LAPIC_DESTINATION_PHYSICAL  0x0
#define LAPIC_DESTINATION_LOGICAL   0x800
#define LAPIC_SEND_PENDING          0x1000

#define LAPIC_TRIGGER_LEVEL         0x8000

#define LAPIC_VECT_MASK             0x10000

//LAPIC register reserved bitmasks
#define LAPIC_SPURIOUS_KEEP         0xFFFFEC00U

//LAPIC LVT masks

#define LAPIC_LVT_TIMER_KEEP        0xFFF8EF00U
#define LAPIC_LVT_CMCI_KEEP         0xFFFEE800U
#define LAPIC_LVT_LINT_KEEP         0xFFFE0800U
#define LAPIC_LVT_ERROR_KEEP        0xFFFEE800U
#define LAPIC_LVT_PREF_KEEP         0xFFFEE800U
#define LAPIC_LVT_THERMAL_KEEP      0xFFFEE800U

//Rounding policy values
#define CPU_ROUND_EVEN                  0
#define CPU_ROUND_DOWN                  1
#define CPU_ROUND_UP                    2
#define CPU_ROUND_ZERO                  3

#define MASK_ALL_INTS()                 asm volatile ("cli\n"::);
#define UNMASK_ALL_INTS()               asm volatile ("sti\n"::);

#define X86_FLAGS_BASE                  0x000002U
#define X86_FLAGS_INTERRUPT_ENABLE      0x000200U
#define X86_FLAGS_NESTED_TASK           0x004000U
#define X86_FLAGS_RESUME                0x010000U
#define X86_FLAGS_ALIGN_CHECK           0x040000U
#define X86_FLAGS_IDENTIFY              0x200000U


#include <device.h>
#include <templates/vector.cc>
#include "softint.h"

//! Long mode TSS
struct COMPILER_PACKED tss64
{
	uint32_t _reserved;
	//! Default kernel mode stack pointer
	uint64_t rsp0;
    uint64_t rsp1;
    uint64_t rsp2;
    uint64_t _reserved2;
	//! First IST stack
	uint64_t ist1;
	//! Second IST stack
	uint64_t ist2;
	//! Third IST stack
	uint64_t ist3;
	//! Fourth IST stack
	uint64_t ist4;
	//! Fifth IST stack
	uint64_t ist5;
	//! Sixth IST stack
	uint64_t ist6;
	//! Seventh IST stack
	uint64_t ist7;
	uint64_t _reserved3;
	uint16_t _reserved4, ioMapOffset;
};

//! Enumeration describing the type of cache
enum cacheType
{
    cacheNull = 0,
    cacheTrace,         // 1
    cachePrefetch,      // 2
    cacheInsTLB,        // 3
    cacheDataTLB,       // 4
    cacheInstruction,   // 5
    cacheData,          // 6
    cacheL2,            // 7
    cacheL3             // 8
};

//! Object describing an individual cache on a processor
struct cacheDescriptor
{
	//! Describes the type of cache this descriptor represents
	cacheType type;
	//! Describes the size of the cache in bytes
	uint32_t size;
	//! Ways of associativity
	uint16_t associativity;
	//! Entries or line size of the cache
	uint16_t entries;
};

//! Structure describing a descriptor table register
struct COMPILER_PACKED dtReg
{
	//! Limit of the descriptor table
	uint16_t limit;
	//! Base address of the descriptor table
	uintptr_t base;
};

//! Processor and Local APIC device
class Processor : public Device
{
	public:
	    typedef uint64_t intstate_t;

		Processor( Device *p );
		uint8_t getColorBits();
		void printInfo();
		bool initLocalApic( uintptr_t phys );
		virtual bool init( void *context);
		uint32_t getId(void) const;
		void setId(uint32_t id);

        //MSR functions
		static uintptr_t readMSR( uint32_t address );
		static void writeMSR( uint32_t address, uint64_t value );

		void sendIPI( uint32_t destination, uint8_t vector, uint32_t flags );

        //LAPIC access/timer functions
		uint32_t getApicTimerVal(void);
		uint32_t getApicInitValue(void);
		void apicMaskTimer(void);
		void apicUnmaskTimer(void);
		void apicStartTimer( uint32_t initCount);
		void apicSetDiv(uint8_t div);
		uint32_t apicSwapTimer( uint32_t val );
		void apicStopTimer(void);
		void apicStall( uint32_t usec );
		void apicSendEOI(void);

		bool apicCheckRequested(const uint8_t index) const;
		bool apicCheckService(const uint8_t index) const;

		uint32_t readAPICID(void) const;

		static uintptr_t createTss();
		static void createGdt( dtReg &gdtr, uintptr_t tss );

		static void initVectorAlloc(void);
		static int getBestAvailableVector( bool isLevel, uint32_t &requestedCount );
		static void installSoftIntHandlers( uint8_t vector, softInterrupt::intHandler handler, void *context, const int handler_count );
		static bool serviceSoftIntVector(uint8_t vector);
		static uint16_t createLdtEntry(uint32_t base, uintptr_t limit, uint32_t flags);

		static inline intstate_t interruptDisable(void)
		{
            intstate_t prior;
            asm volatile ("pushf\n"
                          "cli\n"
                          "popq %0\n": "=r" (prior) :);
            return prior;
		}

		static inline void interruptRestore(intstate_t priorState)
		{
            intstate_t ifValue = priorState & X86_FLAGS_INTERRUPT_ENABLE;
            asm volatile ("pushf\n"
                          "orq %0, (%%rsp)\n"
                          "popf\n":: "r" (ifValue));
		}

	private:
		//! Retrieve CPUID information from the processor
		void getCpuidInfo();
		//Intel specific CPUID information functions
		bool parseIntelCacheDword( uint32_t value );
		void intelParseLeaf4();
		//AMD specific CPUID information functions
		void amdParseL1();
		void amdParseL2andL3();
		//LAPIC register interface functions
		uint32_t readLApicRegister( uint32_t reg ) const;
		void writeLApicRegister( uint32_t reg, uint32_t value );

		//! Processor's unique id field
		uint32_t procId;
		//!Processor's brand ID string
		String brandIdString;
		//! Processor extended ID field
		uint32_t extId;
		//! Processor ID field
		uint32_t versionId;
		//! Page coloring color count
		uint32_t colorCount;
		//! MMIO Interface for the APIC
		volatile uint32_t *apicMmIo;
		//! Array which holds the CPU's cache information
		Vector <cacheDescriptor> caches;

	public:
		//Control register masks
		static const uintptr_t x86_cr0_NM = 0x8U;
};

#endif

