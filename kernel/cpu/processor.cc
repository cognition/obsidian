/* Author: Joseph Kinzel */

#include <io/io.h>
#include <memory/memory.h>

#include "bits.h"
#include "features.h"
#include "processor.h"
#include "localstate.h"
#include "softint.h"

extern "C" void *bspGDT;
extern "C" uint32_t masterLDT[256];

extern "C" void *syscall64_entry, *syscall32_entry, *sysenter64_entry, *sysenter32_entry;


#define GDT_MAX_SIZE            0x60
#define STATIC_GDT_LENGTH       0x50

#define TSS_SIZE                104

#define SOFT_VECTOR_OFFSET      0x30

#define MXCSR_ROUNDING          0x6000U

static softInterrupt softVectors[256-SOFT_VECTOR_OFFSET];
static int vectorLoad[256];
static uint64_t vectorUseMap[256/64];
static uint64_t vectorLevelMap[256/64];
static uint32_t *freeLDTEntry = reinterpret_cast <uint32_t *> (&masterLDT[2]);
static uint32_t freeLdtIndex = 1;
static ticketLock vectorLock;

#define PAT_STRONG_UC           0x00U
#define PAT_WRITE_COMBINE       0x01UL
#define PAT_WRITE_THROUGH       0x04U
#define PAT_WRITE_PROTECTED     0x05U
#define PAT_WRITE_BACK          0x06U

#define MSR_PAT_REG             0x277U

#define MSR_EFER                0xC0000080U
#define MSR_STAR                0xC0000081U
#define MSR_LSTAR               0xC0000082U
#define MSR_CSTAR               0xC0000083U
#define MSR_FMASK               0xC0000084U

#define SYSCALL_ENABLE          0x1



//Processor class

/*! \brief Default constructor for the BSP
 *  \param p Parent device pointer
 */
Processor::Processor( Device *p ) : Device( "CPU", "cpu", p )
{
	localState::setProc( this );
	getCpuidInfo();
	writeMSR(MSR_PAT_REG, PAT_WRITE_BACK | (PAT_WRITE_THROUGH<<8) | (PAT_STRONG_UC<<16) | (PAT_WRITE_COMBINE<<32) );
	writeMSR(MSR_EFER, readMSR(MSR_EFER) | SYSCALL_ENABLE );

	uint64_t starValue = (USER_CODE_32 << 16) | KERNEL_CODE_64;
	starValue <<= 32;
	writeMSR(MSR_STAR, starValue );
	writeMSR(MSR_LSTAR, reinterpret_cast <uint64_t> (&syscall64_entry));
	writeMSR(MSR_FMASK, ~(X86_FLAGS_BASE | X86_FLAGS_INTERRUPT_ENABLE) );
}

/*! Gets the number of bits applicable for cache coloring
 *  \return Number of bits applicable for cache coloring
 */
uint8_t Processor::getColorBits()
{
	uint8_t bits = Bits::getMsb32( colorCount );
	uint32_t mask = 1<<bits;
	mask--;
	if( colorCount & mask )
		bits <<= 1;
	return bits;
}

/*! Returns the processor's unique id value
 *  \return The processor's unique id value
 */
uint32_t Processor::getId(void) const
{
	return procId;
}

/*! Sets the processor's unique id field
 *  \param id New value for the processor's unique id field
 */
void Processor::setId(uint32_t id)
{
	procId = id;
}


/*! Reads a value from a MSR register
 *  \param address Address to write to
 *  \return Value currently stored in the register
 */
uintptr_t Processor::readMSR( uint32_t address )
{
	uint64_t value;
	asm ( "xor %%eax, %%eax\n"
	      "rdmsr\n"
	      "shl $32, %%rdx\n"
	      "or %%rdx, %%rax\n": "=a" ( value ): "c" ( address ): "rdx" );
	return value;
}

/*! Writes into a specific MSR register
 *  \param address MSR Register address
 *  \param value Value to be written into the MSR
 */
void Processor::writeMSR( uint32_t address, uint64_t value )
{
	asm ( "wrmsr\n":: "a" ( value ), "d" ( value>>32 ), "c" ( address ) );
}

bool Processor::init( void *context )
{
	uint64_t features = localState::getFeatures();
	uintptr_t phys = *reinterpret_cast <uintptr_t *> ( context );
	// Make sure the processor actually has an LAPIC
	if( !( features & CPU_FEATURE_APIC ) )
		return false;

	uintptr_t apicBase = phys | LAPIC_ENABLE_GLOBAL;

    if( features & CPU_FEATURE_X2APIC )
		apicBase |= LAPIC_ENABLE_X2APIC;

	//Update the APIC Base MSR
	writeMSR( MSR_APIC_BASE, apicBase | (readMSR(MSR_APIC_BASE) & (LAPIC_ENABLE_X2APIC | LAPIC_IS_BSP)) );

    // Enable the X2APIC feature set if it's available

    uintptr_t realBase = readMSR(MSR_APIC_BASE);
    // Ideally we would be able to actually remap the Local APIC in all cases like the hardware is supposed to allow, but some emulators don't support it
	if((realBase ^ apicBase) & ~(LAPIC_ENABLE_GLOBAL | LAPIC_ENABLE_X2APIC | LAPIC_IS_BSP) )
            phys = realBase & ~(LAPIC_ENABLE_GLOBAL | LAPIC_ENABLE_X2APIC | LAPIC_IS_BSP);
    else
    {
        // Reserve the MMIO space used by the apic
        if( !acquireResource( systemResource( systemResourceMemory, phys, REGULAR_PAGE_SIZE ) ) )
            return false;
    }

	// Setup the MMIO registers
	apicMmIo = reinterpret_cast <uint32_t * volatile> ( virtualMemoryManager::mapRegion( phys, REGULAR_PAGE_SIZE, PAGE_PRESENT | PAGE_WRITE | PAGE_CACHE_DISABLE ) );

	//Program the LAPIC spurious register
	uint32_t spuriousVal = readLApicRegister(LAPIC_SPURIOUS_VECT) & LAPIC_SPURIOUS_KEEP;
	writeLApicRegister( LAPIC_SPURIOUS_VECT, spuriousVal | LAPIC_SPURIOUS_SOFT_ENABLE | VECTOR_LAPIC_SPURIOUS );

	//Program the appropriate LVT registers
	uint32_t timerLvt = readLApicRegister(LAPIC_TIMER_VECT) & LAPIC_LVT_TIMER_KEEP;
	writeLApicRegister( LAPIC_TIMER_VECT, timerLvt | VECTOR_LAPIC_TIMER | LAPIC_TIMER_ONESHOT);
	uint32_t errorLvt = readLApicRegister(LAPIC_ERROR_VECT) & LAPIC_LVT_ERROR_KEEP;
	writeLApicRegister( LAPIC_ERROR_VECT, errorLvt | VECTOR_LAPIC_ERROR );

	apicSetDiv( LAPIC_TIMER_DIV_1);

    apicStopTimer();
	return true;
}

/*! Writes a value out to an APIC register
 *  \param reg Apic register to write to
 *  \param value Value to write to the APIC register
 */
void Processor::writeLApicRegister( uint32_t reg, uint32_t value )
{
	uint64_t features = localState::getFeatures();

	if( features & CPU_FEATURE_X2APIC )
	{
		//Calculate the MSR offset
		uint32_t index = reg>>4;
		index += MSR_X2APIC_OFFSET;
		//Write to the register
		writeMSR( index, value );
	}
	else
	{
		//Calculate the MMIO Index and use it's interface
		uint32_t offset = reg>>2;
		apicMmIo[offset] = value;
	}
}

uint32_t Processor::readLApicRegister( uint32_t reg ) const
{
	uint64_t features = localState::getFeatures();

	if( features & CPU_FEATURE_X2APIC )
	{
		//Calculate the MSR offset
		uint32_t index = reg>>4;
		index += MSR_X2APIC_OFFSET;
		//Write to the register
		return readMSR( index );
	}
	else
	{
		//Calculate the MMIO Index and use it's interface
		uint32_t offset = reg>>2;
		return apicMmIo[offset];
	}
}

//! Reads and returns the current processor's APIC ID from its LAPIC
uint32_t Processor::readAPICID(void) const
{
    uint64_t features = localState::getFeatures();

    uint32_t regValue = readLApicRegister(LAPIC_ID_REG);
    if( (features & CPU_FEATURE_X2APIC) )
        regValue >>= 24;

    return regValue;
}


/*! Sends an Interprocessor Interrupt
 *  \param destination Destination idea to send a message to
 *  \param vector Vector to dispatch to the processor
 *  \param flags Flags indicating the delivery method of the interrupt
 */
void Processor::sendIPI( const uint32_t destination, const uint8_t vector, uint32_t flags )
{
	uint64_t features = localState::getFeatures();

	if( features & CPU_FEATURE_X2APIC )
	{
	    uint64_t priorValue = readMSR(MSR_X2APIC_ICR);
        uint32_t lowDWord;
	    lowDWord = priorValue & 0xFFF320000U;
	    lowDWord |= vector;
	    lowDWord |= flags;

		asm volatile (  "wrmsr\n":: "c" ( MSR_X2APIC_ICR ), "a" ( lowDWord ), "d" ( destination ) );
	}
	else
	{
		uint32_t lowDWord, highDWord;
        highDWord = apicMmIo[0x310>>2] & 0xFFFFFFU;
        lowDWord = apicMmIo[0x300>>2];
        lowDWord &= 0xFFF320000U;

		highDWord |= destination<<24;
		lowDWord |= vector | flags;

		apicMmIo[0x310>>2] = highDWord;
		apicMmIo[0x300>>2] = lowDWord;

		while( apicMmIo[0x300>>2] & LAPIC_SEND_PENDING )
		{
			asm volatile ( "pause\n" );
		}
	}
}

//! Dispatches an EOI to the processor's Local APIC
void Processor::apicSendEOI(void)
{
	writeLApicRegister( LAPIC_EOI_REG, 0 );
}

uint32_t Processor::getApicTimerVal(void)
{
	return readLApicRegister( LAPIC_TIMER_CURRENT_REG );
}

uint32_t Processor::getApicInitValue(void)
{
    return readLApicRegister( LAPIC_TIMER_INIT_REG );
}

void Processor::apicMaskTimer(void)
{
	uint32_t value = readLApicRegister( LAPIC_TIMER_VECT );
	value |= LAPIC_VECT_MASK;
	writeLApicRegister( LAPIC_TIMER_VECT, value );
}

void Processor::apicUnmaskTimer(void)
{
	uint32_t value = readLApicRegister( LAPIC_TIMER_VECT );
	value &= ~LAPIC_VECT_MASK;
	writeLApicRegister( LAPIC_TIMER_VECT, value );
}

void Processor::apicStartTimer( uint32_t initCount)
{
    writeLApicRegister( LAPIC_TIMER_INIT_REG, initCount );
}

void Processor::apicSetDiv(uint8_t div)
{
    uint32_t regValue = readLApicRegister(LAPIC_TIMER_DIV_REG);
	regValue &= ~0xFU;
	regValue |= div;
	writeLApicRegister( LAPIC_TIMER_DIV_REG, regValue );
}

void Processor::apicStopTimer(void)
{
	writeLApicRegister( LAPIC_TIMER_INIT_REG, 0 );
}

/*! Stalls the processor using CPU's APIC Timer
 *  \param usec Stall time in microseconds
 */
void Processor::apicStall( uint32_t usec )
{
	apicSetDiv( LAPIC_TIMER_DIV_1 );
	uint64_t value = usec;
	value *= localState::getApicScaleUsec();
	writeLApicRegister( LAPIC_TIMER_INIT_REG, value );
	asm ( "pushf\n"
          "sti\n"
	      "hlt\n"
	      "popf\n":: );
}

bool Processor::apicCheckRequested(const uint8_t index) const
{
    uint32_t dwordIndex, mask, value;

    mask = 1;
    dwordIndex = index/32;
    mask <<= index%32;

    value = readLApicRegister( LAPIC_IRR_BASE + dwordIndex*0x10 );

    return (value & mask);
}

bool Processor::apicCheckService(const uint8_t index) const
{
    uint32_t dwordIndex, mask, value;

    mask = 1;
    dwordIndex = index/32;
    mask <<= index%32;

    value = readLApicRegister( LAPIC_ISR_BASE + dwordIndex*0x10 );

    return (value & mask);
}

/*! Creates a TSS for an application processor
 *  \return Virtual address of the new TSS
 */
uintptr_t Processor::createTss()
{
	tss64 *tss = new tss64;
	Memory::set( tss, 0, sizeof( tss64 ) );
	//Create the stacks for the kernel, pretty much all that's in the TSS for long mode
	tss->rsp0 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	tss->ist1 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	tss->ist2 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	tss->ist3 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	tss->ist4 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	tss->ist5 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	tss->ist6 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	tss->ist7 = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	return reinterpret_cast <uintptr_t> ( tss );
}

void Processor::createGdt( dtReg &gdtr, uintptr_t tss )
{
	uint32_t *gdt = new uint32_t[GDT_MAX_SIZE/sizeof(uint32_t)];
	uint32_t *tssEntry = &gdt[KERNEL_TSS_SELECTOR/sizeof(uint32_t)];

	gdtr.limit = GDT_MAX_SIZE;
	gdtr.base = reinterpret_cast <uintptr_t> ( gdt );

	Memory::copy( gdt, &bspGDT, STATIC_GDT_LENGTH );

	*tssEntry = ( ( tss & 0xFFFFU )<<16 ) | TSS_SIZE;
	tssEntry[1] = ( ( tss>>16 ) & 0xFFU ) | ( tss & 0xFF000000U ) | GDT_ENTRY_TSS;
	tssEntry[2] = tss>>32;
	tssEntry[3] = 0;
}

uint16_t Processor::createLdtEntry(uint32_t base, uintptr_t limit, uint32_t flags)
{
    if(!freeLDTEntry)
        return 0;
    else
    {
        uint32_t ldword, hdword;
        uint32_t *entry = freeLDTEntry;
        uint16_t ret = freeLdtIndex;
        ret <<= 3;
        ret |= 0x4;

        freeLdtIndex = *entry;
        if(freeLdtIndex)
            freeLDTEntry = &masterLDT[freeLdtIndex*2];
        else
            freeLDTEntry = NULL;

        ldword = base & 0xFFFF;
        ldword <<= 16;
        ldword |= limit & 0xFFFF;

        hdword = base & 0xFF000000;
        hdword |= limit & 0xF0000;
        hdword |= (base >> 16) & 0xFF;
        hdword |= flags;

        *entry = ldword;
        entry[1] = hdword;

        return ret;
    }
}

/*! Attempts to allocate a number of requested global processor vectors
 *  \param isLevel Indicates if the requested vectors are for a level based trigger that might be shared with other vectors
 *  \param requested Number of vectors being requested
 *  \return Number of vectors that were ultimately allocated or zero if none could be allocated
 */
int Processor::getBestAvailableVector(bool isLevel, uint32_t &requested )
{
    uint64_t currentWordMask, nextWordMask, currentWord, nextWord;
    int arrayOffset = 0;
    uint64_t reqMask = 0;
    static const constexpr int mapMax = (256/64)-1;


    //Initialize all masks and data words
    reqMask = ~reqMask;
    reqMask <<= requested;
    reqMask = ~reqMask;

    currentWordMask = reqMask;
    nextWordMask = 0;

    asm volatile ("shldq %1, %0\n": "+r" (nextWordMask), "+r" (currentWordMask) : "c" (SOFT_VECTOR_OFFSET) );
    currentWordMask <<= SOFT_VECTOR_OFFSET;

    spinLockInstance lock(&vectorLock);
    currentWord = vectorUseMap[0];
    nextWord = vectorUseMap[1];

    for(int vectorStart = SOFT_VECTOR_OFFSET; vectorStart < (256 - requested); vectorStart++)
    {
        uint64_t currentDiff = (currentWord ^ currentWordMask) & currentWordMask;
        uint64_t nextDiff = (nextWord ^ nextWordMask) & nextWordMask;

        if( (currentDiff == currentWordMask) && (nextDiff == nextWordMask) )
        {
            vectorUseMap[arrayOffset] |= currentWordMask;

            if(isLevel)
                vectorLevelMap[arrayOffset] |= currentWordMask;
            if(arrayOffset < mapMax)
            {
                vectorUseMap[arrayOffset+1] |= nextWordMask;
                if(isLevel)
                    vectorLevelMap[arrayOffset] |= nextWordMask;
            }

            for(int markIndex = vectorStart; markIndex < (vectorStart + requested); markIndex++)
                vectorLoad[markIndex]++;

            return vectorStart;
        }

        asm volatile ("shldq %1, %0\n": "+r" (nextWordMask), "+r" (currentWordMask) : "c" (1) );
        currentWordMask <<= 1;
        if(!currentWordMask)
        {
            currentWordMask = nextWordMask;
            nextWordMask = 0;
            arrayOffset++;
            if(arrayOffset == mapMax)
                break;
            currentWord = vectorUseMap[arrayOffset];
            nextWord = vectorUseMap[arrayOffset+1];
        }
    }

    //Try again while halving the number of requested vectors
    requested >>= 1;

    if(requested)
        return getBestAvailableVector(isLevel, requested);
    else
    {
        kprintf("[PROC]: Unable to allocate vectors for %u interrupts\n", requested);
        return 0;
    }
}

//! Initializes the CPU vector allocation arrays
void Processor::initVectorAlloc(void)
{
    Memory::set( vectorLoad, 0, sizeof(vectorLoad));
    Memory::set( vectorUseMap, 0, sizeof(vectorUseMap));
    Memory::set( vectorLevelMap, 0, sizeof(vectorUseMap));
}

void Processor::installSoftIntHandlers( uint8_t vector, softInterrupt::intHandler handler, void *context, const int handlerCount )
{
    for(int handlerIndex = 0; handlerIndex < handlerCount; handlerIndex++ )
        softVectors[vector-SOFT_VECTOR_OFFSET + handlerIndex].insert( handler, context, handlerIndex );
}

bool Processor::serviceSoftIntVector(uint8_t vector)
{
    //kprintf("[CPU ]: Servicing software guided interrupt vector %X\n", vector);
    return softVectors[vector-SOFT_VECTOR_OFFSET].call();
}

//! Processor print information function
void Processor::printInfo()
{
	kprintf( " CPU: Found a %s processor - %s\n", brandIdString.toBuffer() , name.toBuffer() );

	for( int i = 0; i < caches.length(); i++ )
	{
		cacheDescriptor d =  caches[i];
		kprintf( " CPU: Cache type: %u Size: %u Associativity: %u\n", d.type, d.size, d.associativity );
	}

	kprintf( " CPU: Color count: %u\n", colorCount );
}
