%include "asmdefs.inc"

[BITS 16]
[ORG 0x1000]
_entry:
    cli
    jmp 0x0000:start

start:
    mov eax, cr0					    ; Enter protected mode
    or eax, 1

    mov cr0, eax
    lgdt [gdtr]					        ; Load the gdt

    jmp dword 0x0010:setup32

[BITS 32]

setup32:
    mov ax, 0x8					        ; Initialize the data segments
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    mov eax, [dataStart+8]
    mov byte [eax], 1

.prepareLongMode:


    mov eax, cr4
    or eax, 0x6B0                           ; Enable PAE, PSE, SSE and Global pages
    mov cr4, eax

    mov edi, dword [dataStart+24]			    ; Load the PML4 Address into EDX
    mov cr3, edi

    mov ecx, MSR_EFER                       ; Set the LME and NXE bits
    rdmsr
    or eax, 0x900
    wrmsr

    mov ebx, cr0
    and ebx, 0x10
    or ebx, 0x80000001
    mov cr0, ebx                            ; Enable paging

.waitLongMode:
    ; Spin until we can confirm that long mode is active
    rdmsr
    pause
    test eax, 0x400
    jz .waitLongMode

    mov eax, [dataStart]
    lgdt [eax]

    jmp far dword [dataStart + 16]



gdtr:
    dw ( gdt.gdtEnd - gdt - 1)
    dd gdt

gdt:
    dq	0
    .kernelData32:
        dw 0xFFFF, 0
        db 0, 0x92, 0xCF, 0
    .kernelCode32:
        dw 0xFFFF, 0
        db 0, 0x9A, 0xCF, 0
.gdtEnd:

dataStart:

