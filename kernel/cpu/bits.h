#ifndef BITS_H_
#define BITS_H_

#include <compiler.h>

namespace Bits
{
    template<typename width_type> width_type getMsb(const width_type value);

	inline uint16_t getMsb16( const uint16_t val )
	{
		uint16_t ret;
		asm ( "bsrw %1, %0\n": "=r" ( ret ): "r" ( val ) : "cc" );
		return ret;
	}

	inline uint32_t getMsb32( const uint32_t val )
	{
		uint32_t ret;
		asm ( "bsrl %1, %0\n": "=r" ( ret ): "r" ( val ) : "cc" );
		return ret;
	}

	inline uint64_t getMsb64( const uint64_t val )
	{
		uint64_t ret;
		asm ( "bsrq %1, %0\n": "=r" ( ret ): "r" ( val ) : "cc" );
		return ret;
	}

	inline uint16_t getLsb16( const uint16_t val )
	{
		uint16_t ret;
		asm ( "bsfw %1, %0\n": "=r" ( ret ): "r" ( val ) : "cc" );
		return ret;
	}

	inline uint32_t getLsb32( const uint32_t val )
	{
		uint32_t ret;
		asm ( "bsfl %1, %0\n": "=r" ( ret ): "r" ( val ) : "cc" );
		return ret;
	}

	inline uint64_t getLsb64( const uint64_t val )
	{
		uint64_t ret;
		asm ( "bsfq %1, %0\n": "=r" ( ret ): "r" ( val ) : "cc" );
		return ret;
	}

	inline uint64_t popCount64( const uint64_t val)
	{
	    uint64_t ret;
	    asm ( "popcntq %1, %0\n": "=r" (ret): "r" (val) : "cc" );
	    return ret;
	}
}

#endif
