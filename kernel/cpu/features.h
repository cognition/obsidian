#ifndef FEATURES_H_
#define FEATURES_H_

//EDX Features
#define CPU_FEATURE_APIC                0x20
#define CPU_FEATURE_FXSAVE              (1UL<<24)

//ECX Features
#define CPU_FEATURE_X2APIC              (1UL<<53)
#define CPU_FEATURE_XSAVE               (1UL<<58)
#define CPU_FEATURE_POPCNT              (1UL<<55)


//Extended features
#define CPU_EXT_FEATURE_SYSCALL         0x8000

#endif
