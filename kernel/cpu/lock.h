#ifndef CPU_LOCK_H_
#define CPU_LOCK_H_

#include <compiler.h>

class COMPILER_PACKED ticketLock
{
	public:
		ticketLock();
		~ticketLock() = default;
		void reset(void);
		volatile uint64_t nextTicket;
		volatile uint64_t currentTicket;
};


//! A basic spin lock class, designed for connecting to an external lock
class spinLock
{
	public:
		spinLock( ticketLock *lock );
		~spinLock() = default;
		void acquire(void *addr = 0);
		void release();
	private:
		ticketLock *lockPtr;
		uint64_t procFlags;
    protected:
		bool holding;
};

class manualScopedLock : public spinLock
{
    public:
        manualScopedLock( ticketLock *lock);
        ~manualScopedLock();
};

//! A instanced spin lock object that connects to an external lock
class spinLockInstance : protected spinLock
{
	public:
		spinLockInstance( ticketLock *lock, void *caller = 0 );
		~spinLockInstance();
};

//! A self contained spin lock object
class spinLockObj : public spinLock
{
	public:
		spinLockObj();
		~spinLockObj() = default;
	private:
		ticketLock lock;
};

#endif
