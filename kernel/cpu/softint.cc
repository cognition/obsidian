#include "softint.h"

softInterrupt::softInterrupt()
{
}

void softInterrupt::insert( intHandler h, void *context, const int handlerIndex )
{
    softIntEntry e(h, context, handlerIndex);
    handlers.insert( e );
}

bool softInterrupt::call()
{
	for( int counter = 0; counter < handlers.length(); counter++ )
	{
	    softIntEntry &entry = handlers[counter];
		if( entry.handler(entry.context, entry.index) )
            return true;
	}
	return false;
}
