#include "processor.h"
#include <memory/memory.h>
#include "localstate.h"

/*! \note AMD CPUID algorithms are based on the information contained in AMD CPUID Specification rev 2.34, Publication Number 25481
 */

//CPUID functions
#define CPUID_BASIC_INFO                0
#define CPUID_BASIC_FEATURES            0x1
#define CPUID_INTEL_CACHE_TLB           0x2
#define CPUID_INTEL_DCACHE              0x4

//Intel specific functions
#define CPUID_EXT_INFO                  0x80000000U
#define CPUID_EXT_FEATURES              0x80000001U

//AMD Specific functions
#define CPUID_AMD_L1_CACHE			    0x80000005U
#define CPUID_AMD_L2_AND_L3_CACHE	    0x80000006U

//! Brand string functions
#define CPUID_BRAND_STRING_START        0x80000002U
#define CPUID_BRAND_STRING_MID          0x80000003U
#define CPUID_BRAND_STRING_MAX          0x80000004U

//! Long mode addressing information
#define CPUID_EXT_LM_ADDRESSING         0x80000008U

//! Maximum number of page colors to use
#define MAX_PAGE_COLOR_COUNT            256

//! Cache descriptor creation macro
#define INTEL_CACHE_ENTRY(a,b,c,d)      {a, b, c, d}

#define INTEL_CACHE_FULLY_ASSOCIATIVE   0x200

#define AMD_CACHE_FULLY_ASSOCIATIVE     0xFF

//Intel Cache description tables

#define IC_CALL_LEAF_4                  0xFF

#define IC_FIRST_SET_START              0x00
#define IC_FIRST_SET_END                0x0E

const static cacheDescriptor firstSet[] =
{
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 00h
	INTEL_CACHE_ENTRY( cacheInsTLB, 4096, 4, 32 ),                 // 01h
	INTEL_CACHE_ENTRY( cacheInsTLB, LARGE_PAGE_SIZE*2, 2, 2 ),     // 02h
	INTEL_CACHE_ENTRY( cacheDataTLB, LARGE_PAGE_SIZE*2, 32, 64 ),  // 03h
	INTEL_CACHE_ENTRY( cacheDataTLB, LARGE_PAGE_SIZE*2, 4, 8 ),    // 04h
	INTEL_CACHE_ENTRY( cacheDataTLB, LARGE_PAGE_SIZE*2, 32, 32 ),  // 05h
	INTEL_CACHE_ENTRY( cacheInstruction, 8192, 4, 32 ),            // 06h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 07h
	INTEL_CACHE_ENTRY( cacheInstruction, 16384, 4, 32 ),           // 08h
	INTEL_CACHE_ENTRY( cacheInstruction, 32768, 4, 64 ),           // 09h
	INTEL_CACHE_ENTRY( cacheData, 8192, 2, 32 ),                   // 0Ah
	INTEL_CACHE_ENTRY( cacheInsTLB, LARGE_PAGE_SIZE*2, 4, 4 ),     // 0Bh
	INTEL_CACHE_ENTRY( cacheData, 16384, 4, 32 ),                  // 0Ch
	INTEL_CACHE_ENTRY( cacheData, 16384, 4, 64 ),                  // 0Dh
	INTEL_CACHE_ENTRY( cacheData, 24*1024, 6, 64 ),                // 0Eh
};

#define IC_SECOND_SET_START           0x21
#define IC_SECOND_SET_END             0x30

const static cacheDescriptor secondSet[] =
{
	INTEL_CACHE_ENTRY( cacheL2, 256*1024, 8, 64 ),                 // 21h
	INTEL_CACHE_ENTRY( cacheL3, 0x80000, 4, 64 ),                  // 22h
	INTEL_CACHE_ENTRY( cacheL3, 0x100000, 8, 64 ),                 // 23h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 24h
	INTEL_CACHE_ENTRY( cacheL3, LARGE_PAGE_SIZE, 8, 64 ),          // 25h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 26h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 27h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 28h
	INTEL_CACHE_ENTRY( cacheL3, LARGE_PAGE_SIZE*2, 8, 64 ),        // 29h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 2Ah
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 2Bh
	INTEL_CACHE_ENTRY( cacheData, 32768, 8, 64 ),                  // 2Ch
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 2Dh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 2Eh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 2Fh
	INTEL_CACHE_ENTRY( cacheInstruction, 32768, 8, 64 ),           // 30h
};

#define IC_THIRD_SET_START              0x40
#define IC_THIRD_SET_END                0x87

const static cacheDescriptor thirdSet[] =
{
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 40h
	INTEL_CACHE_ENTRY( cacheL2, 128*1024, 4, 32 ),                 // 41h
	INTEL_CACHE_ENTRY( cacheL2, 256*1024, 4, 32 ),                 // 42h
	INTEL_CACHE_ENTRY( cacheL2, 512*1024, 4, 32 ),                 // 43h
	INTEL_CACHE_ENTRY( cacheL2, 0x100000, 4, 32 ),                 // 44h
	INTEL_CACHE_ENTRY( cacheL2, LARGE_PAGE_SIZE, 4, 32 ),          // 45h
	INTEL_CACHE_ENTRY( cacheL3, 0x400000, 4, 64 ),                 // 46h
	INTEL_CACHE_ENTRY( cacheL3, 0x800000, 8, 64 ),                 // 47h
	INTEL_CACHE_ENTRY( cacheL2, 0x300000, 12, 64 ),                // 48h
	INTEL_CACHE_ENTRY( cacheL2, 0x400000, 16, 64 ),                // 49h
	INTEL_CACHE_ENTRY( cacheL3, 0x600000, 12, 64 ),                // 4Ah
	INTEL_CACHE_ENTRY( cacheL3, 0x800000, 16, 64 ),                // 4Bh
	INTEL_CACHE_ENTRY( cacheL3, 0xC00000, 12, 64 ),                // 4Ch
	INTEL_CACHE_ENTRY( cacheL3, 0x1000000, 16, 64 ),               // 4Dh
	INTEL_CACHE_ENTRY( cacheL2, 0x600000, 24, 64 ),                // 4Eh
	INTEL_CACHE_ENTRY( cacheInsTLB, 4096, 0, 32 ),                 // 4Fh
	INTEL_CACHE_ENTRY( cacheInsTLB, UINT32_MAX, 0, 64 ),           // 50h
	INTEL_CACHE_ENTRY( cacheInsTLB, UINT32_MAX, 0, 128 ),          // 51h
	INTEL_CACHE_ENTRY( cacheInsTLB, UINT32_MAX, 0, 256 ),          // 52h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 53h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 54h
	INTEL_CACHE_ENTRY( cacheInsTLB, LARGE_PAGE_SIZE, 7, 7 ),       // 55h

	INTEL_CACHE_ENTRY( cacheDataTLB, 0x400000, 4, 16 ),            // 56h
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 4, 16 ),                // 57h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 58h
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 16, 16 ),               // 59h
	INTEL_CACHE_ENTRY( cacheDataTLB, LARGE_PAGE_SIZE, 4, 32 ),     // 5Ah
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 0, 64 ),                // 5Bh
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 0, 128 ),               // 5Ch
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 0, 256 ),               // 5Dh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 5Eh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 5Fh
	INTEL_CACHE_ENTRY( cacheData, 16384, 8, 64 ),                  // 60h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 61h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 62h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 63h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 64h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 65h
	INTEL_CACHE_ENTRY( cacheData, 8192, 4, 64 ),                   // 66h
	INTEL_CACHE_ENTRY( cacheData, 16384, 4, 64 ),                  // 67h
	INTEL_CACHE_ENTRY( cacheData, 32768, 4, 64 ),                  // 68h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 69h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 6Ah
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 6Bh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 6Ch
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 6Dh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 6Eh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 6Fh
	INTEL_CACHE_ENTRY( cacheTrace, 12*1024, 8, 0 ),                // 70h
	INTEL_CACHE_ENTRY( cacheTrace, 16*1024, 8, 0 ),                // 71h
	INTEL_CACHE_ENTRY( cacheTrace, 32*1024, 8, 0 ),                // 72h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 73h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 74h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 75h
	INTEL_CACHE_ENTRY( cacheInsTLB, LARGE_PAGE_SIZE, 8, 8 ),       // 76h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 77h
	INTEL_CACHE_ENTRY( cacheL2, 0x100000, 4, 64 ),                 // 78h
	INTEL_CACHE_ENTRY( cacheL2, 128*1024, 8, 64 ),                 // 79h
	INTEL_CACHE_ENTRY( cacheL2, 256*1024, 8, 64 ),                 // 7Ah
	INTEL_CACHE_ENTRY( cacheL2, 512*1024, 8, 64 ),                 // 7Bh
	INTEL_CACHE_ENTRY( cacheL2, 0x100000, 8, 64 ),                 // 7Ch
	INTEL_CACHE_ENTRY( cacheL2, LARGE_PAGE_SIZE, 8, 64 ),          // 7Dh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 7Eh
	INTEL_CACHE_ENTRY( cacheL2, 512*1024, 2, 64 ),                 // 7Fh
	INTEL_CACHE_ENTRY( cacheL2, 512*1024, 8, 64 ),                 // 80h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // 81h
	INTEL_CACHE_ENTRY( cacheL2, 256*1024, 8, 32 ),                 // 82h
	INTEL_CACHE_ENTRY( cacheL2, 512*1024, 8, 32 ),                 // 83h
	INTEL_CACHE_ENTRY( cacheL2, 0x100000, 8, 32 ),                 // 84h
	INTEL_CACHE_ENTRY( cacheL2, 0x200000, 8, 32 ),                 // 85h
	INTEL_CACHE_ENTRY( cacheL2, 512*1024, 4, 64 ),                 // 86h
	INTEL_CACHE_ENTRY( cacheL2, 0x100000, 8, 64 )                  // 87h
};

#define IC_FOURTH_SET_START         0xB0
#define IC_FOURTH_SET_END           0xF1

const static cacheDescriptor fourthSet[] =
{
	INTEL_CACHE_ENTRY( cacheInsTLB, 4096, 4, 128 ),                // B0h
	INTEL_CACHE_ENTRY( cacheInsTLB, LARGE_PAGE_SIZE, 4, 8 ),       // B1h
	INTEL_CACHE_ENTRY( cacheInsTLB, 4096, 4, 64 ),                 // B2h
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 4, 128 ),               // B3h
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 4, 256 ),               // B4h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // B5h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // B6h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // B7h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // B8h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // B9h
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 4, 64 ),                // BAh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // BBh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // BCh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // BDh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // BEh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // BFh
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 4, 8 ),                 // C0h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C1h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C2h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C3h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C4h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C5h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C6h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C7h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C8h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // C9h
	INTEL_CACHE_ENTRY( cacheDataTLB, 4096, 4, 512 ),               // CAh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // CBh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // CCh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // CDh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // CEh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // CFh
	INTEL_CACHE_ENTRY( cacheL3, 0x80000, 4, 64 ),                  // D0h
	INTEL_CACHE_ENTRY( cacheL3, 0x100000, 4, 64 ),                 // D1h
	INTEL_CACHE_ENTRY( cacheL3, 0x200000, 4, 64 ),                 // D2h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // D3h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // D4h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // D5h
	INTEL_CACHE_ENTRY( cacheL3, 0x100000, 8, 64 ),                 // D6h
	INTEL_CACHE_ENTRY( cacheL3, 0x200000, 8, 64 ),                 // D7h
	INTEL_CACHE_ENTRY( cacheL3, 0x400000, 8, 64 ),                 // D8h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // D9h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // DAh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // DBh
	INTEL_CACHE_ENTRY( cacheL3, 0x180000, 12, 64 ),                // DCh
	INTEL_CACHE_ENTRY( cacheL3, 0x300000, 12, 64 ),                // DDh
	INTEL_CACHE_ENTRY( cacheL3, 0x600000, 12, 64 ),                // DEh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // DFh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // E0h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // E1h
	INTEL_CACHE_ENTRY( cacheL3, 0x200000, 16, 64 ),                // E2h
	INTEL_CACHE_ENTRY( cacheL3, 0x400000, 16, 64 ),                // E3h
	INTEL_CACHE_ENTRY( cacheL3, 0x800000, 16, 64 ),                // E4h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // E5h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // E6h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // E7h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // E8h
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // E9h
	INTEL_CACHE_ENTRY( cacheL3, 0xC00000, 24, 64 ),                // EAh
	INTEL_CACHE_ENTRY( cacheL3, 0x1200000, 24, 64 ),               // EBh
	INTEL_CACHE_ENTRY( cacheL3, 0x1800000, 24, 64 ),               // ECh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // EDh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // EEh
	INTEL_CACHE_ENTRY( cacheNull, 0, 0, 0 ),                       // EFh
	INTEL_CACHE_ENTRY( cachePrefetch, 64, 0, 0 ),                  // F0h
	INTEL_CACHE_ENTRY( cachePrefetch, 128, 0, 0 )                  // F1h
};

const static uint8_t amdL2L3Associativity[16] = {
	0,              // 00h  (Cache disabled)
	1,              // 01h  (Direct mapped)
	2,              // 02h  (2 way)
	0,              // 03h  (reserved)
	4,              // 04h  (4 way)
	0,              // 05h  (reserved)
	8,              // 06h  (8 way)
	0,              // 07h  (reserved)
	16,             // 08h  (16 way)
	0,              // 09h  (reserved)
	32,             // 0Ah  (32 way)
	48,             // 0Bh  (48 way)
	64,             // 0Ch  (64 way)
	96,             // 0Dh  (96 way)
	128,            // 0Eh  (128 way)
	0               // 0Fh  Fully associative
};


//! Retrieves CPUID information from the current processor
void Processor::getCpuidInfo()
{
	uint32_t baseMax, extendedMax;
	uint32_t eax, ebx, ecx, edx;
	char brandId[13], brandString[49];

	brandId[12] = 0;
	brandString[48] = 0;
	colorCount = 4;

	//Get info for basic CPUID functions
	asm ( "cpuid\n"
	      "mov %%ebx, (%1)\n"
	      "mov %%edx, 4(%1)\n"
	      "mov %%ecx, 8(%1)\n": "=a" ( baseMax ):
	      "r" ( brandId ), "a" ( CPUID_BASIC_INFO ) : "ebx", "ecx", "edx", "memory" );

	//Get the standard feature flags
	if( baseMax >= CPUID_BASIC_FEATURES )
	{
		asm ( "cpuid\n": "=a" ( versionId ), "=b" ( ebx ), "=c" ( ecx ), "=d" ( edx ) : "a" ( CPUID_BASIC_FEATURES ) );
		localState::setFeatures( ( ( uint64_t )ecx<<32 ) | edx );
	}

	//Initialize our brand id string
	brandIdString = brandId;

	//Get info for extended CPUID functions
	asm ( "cpuid\n": "=a" ( extendedMax ): "a" ( CPUID_EXT_INFO ): "ebx", "ecx", "edx" );
	if( extendedMax >= CPUID_EXT_FEATURES )
	{
		asm ( "cpuid\n": "=b" ( extId ),  "=c" ( ecx ), "=d" ( edx ): "a" ( CPUID_EXT_FEATURES ) );
		localState::setExtFeatures( ( ( uint64_t )ecx << 32 ) | edx );

		if( extendedMax >= CPUID_BRAND_STRING_MAX )
		{
			//Retrieve the processor's brand string
			asm ( "cpuid\n"
			      "mov %%eax, (%1)\n"
			      "mov %%ebx, 4(%1)\n"
			      "mov %%ecx, 8(%1)\n"
			      "mov %%edx, 12(%1)\n"
			      "mov $0x80000003, %%eax\n"
			      "cpuid\n"
			      "mov %%eax, 16(%1)\n"
			      "mov %%ebx, 20(%1)\n"
			      "mov %%ecx, 24(%1)\n"
			      "mov %%edx, 28(%1)\n"
			      "mov $0x80000004, %%eax\n"
			      "mov %%eax, 32(%1)\n"
			      "mov %%ebx, 36(%1)\n"
			      "mov %%ecx, 40(%1)\n"
			      "mov %%edx, 44(%1)\n":: "a" ( CPUID_BRAND_STRING_START ), "r" ( brandString ) : "ebx", "ecx", "edx" );
			name = brandString;
			if( extendedMax >= CPUID_EXT_LM_ADDRESSING )
			{

			}
		}
	}

	//Intel specific functions
	if( brandIdString == "GenuineIntel" )
	{
		if( baseMax >= CPUID_INTEL_DCACHE )
			intelParseLeaf4();
		else if( baseMax >= CPUID_INTEL_CACHE_TLB )
		{
			asm ( "cpuid\n": "=a" ( eax ), "=b" ( ebx ), "=c" ( ecx ), "=d" ( edx ) : "a" ( CPUID_INTEL_CACHE_TLB ) );
			parseIntelCacheDword( eax );
			parseIntelCacheDword( ebx );
			parseIntelCacheDword( ecx );
			parseIntelCacheDword( edx );
		}
	}
	else if( brandIdString == "AuthenticAMD" )     // AMD specific functions
	{
		if( extendedMax >= CPUID_AMD_L1_CACHE )
		{
			amdParseL1();
			if( extendedMax >= CPUID_AMD_L2_AND_L3_CACHE )
				amdParseL2andL3();
		}
	}
	// Cap the page color count
	if( colorCount > MAX_PAGE_COLOR_COUNT )
		colorCount = MAX_PAGE_COLOR_COUNT;
}

/*! Parses a DWORD containing up to 4 cache/TLB identifiers for an Intel processor
 *  \param value Double word value to be parsed
 *  \return True if the DWORD accurately described the caches on the processor, false if Leaf 4 needs to be queried
 */
bool Processor::parseIntelCacheDword( uint32_t value )
{
	uint8_t index;
	uint32_t color;
	bool status = true;
	while( value )
	{
		index = value & 0xFF;
		value >>= 8;
		const cacheDescriptor *entry;
		if( index >= IC_THIRD_SET_START )
		{
			if( index <= IC_THIRD_SET_END )
			{
				//Value is within the third set
				entry = &thirdSet[index - IC_THIRD_SET_START];
			}
			else if( ( index >= IC_FOURTH_SET_START ) && ( index <= IC_FOURTH_SET_END ) )
			{
				//Value is within the fourth set
				entry = &fourthSet[index - IC_FOURTH_SET_START];
			}
			else if( index == IC_CALL_LEAF_4 )
			{
				status = false;
				continue;
			}
			else
				continue;
		}
		else if( index <= IC_FIRST_SET_END )
		{
			//Value is within the first set
			entry = &firstSet[index];
		}
		else if( ( index >= IC_SECOND_SET_START ) && ( index <= IC_SECOND_SET_END ) )
		{
			//Value is within the second set
			entry = &secondSet[index - IC_SECOND_SET_START];
		}
		else
			continue;

		//Add cache entry to our cache array if appropriate
		if( entry->type != cacheNull )
			caches.insert( *entry );

		//Calculate page colors if appropriate
		switch( entry->type )
		{
			case cacheL2:
			case cacheL3:
				color = entry->size/( REGULAR_PAGE_SIZE*entry->associativity );
				if( color >= colorCount )
					colorCount = color;
				break;
			default:
				break;
		}
	}
	return status;
}

//! Parses CPUID Leaf 4, Deterministic cache parameters for information about the caches on the system
void Processor::intelParseLeaf4()
{
	uint32_t eax, ebx, ecx, edx;
	int counter = 0;
	uint8_t type = 0;

	do
	{
		asm ( "cpuid\n": "=a" ( eax ), "=b" ( ebx ), "=c" ( ecx ), "=d" ( edx )
		      : "a" ( CPUID_INTEL_DCACHE ), "c" ( counter ) );

		ecx++;
		type = eax & 0x1F;
		uint8_t level = ( eax>>5 ) & 0x7;
		uint16_t associativity = ( ebx>>22 ) + 1;
		uint16_t lineSize = ( ebx & 0xFFF ) + 1;
		uint32_t linePart = ( ( ebx >> 12 ) & 0x3FF ) + 1;

		uint32_t size = lineSize*linePart*ecx*associativity;

		if( !type )
			break;
		cacheType ctype = cacheNull;

		if( eax & INTEL_CACHE_FULLY_ASSOCIATIVE )
			associativity = size/REGULAR_PAGE_SIZE;

		if( level > 2 )
			ctype = cacheL2;
		else if( level < 2 )
		{
			if( type == 1 )
				ctype = cacheData;
			else
				ctype = cacheInstruction;
		}
		else
		{
			ctype = cacheL3;
		}

		cacheDescriptor desc = {ctype, size, associativity, lineSize};
		caches.insert( desc );

		uint32_t color = size/( REGULAR_PAGE_SIZE*associativity );
		if( color >= colorCount )
			colorCount = color;

		counter++;
	} while( type );
}

static void amdParseLowCache( Vector <cacheDescriptor> &caches, uint32_t val, cacheType type )
{
	uint32_t size;
	uint8_t associativity;
	uint8_t lineSize;

	//Parse cache information
	lineSize = val & 0xFF;
	associativity = ( val>>16 ) & 0xFF;
	size = ( val>>24 )*1024;
	//Check if we have the fully associative code
	if( associativity == AMD_CACHE_FULLY_ASSOCIATIVE )
		associativity = size/REGULAR_PAGE_SIZE;

	cacheDescriptor cacheObj;
	cacheObj.type = type;
	cacheObj.size = size;
	cacheObj.associativity = associativity;
	cacheObj.entries = lineSize;
	//Define the descriptor and add it to the cache vector
	caches.insert( cacheObj );
}

//! Parses the L1 Data and Instruction cache information for AMD processors
void Processor::amdParseL1()
{
	uint32_t dataCacheInfo, instructionCacheInfo;

	//Query the CPUID function for L1 cache data
	asm ( "cpuid\n": "=c" ( dataCacheInfo ), "=d" ( instructionCacheInfo ): "a" ( CPUID_AMD_L1_CACHE ) : "ebx" );

	amdParseLowCache( caches, dataCacheInfo, cacheData );
	amdParseLowCache( caches, instructionCacheInfo, cacheInstruction );
}

static void amdParseHighCache( Vector <cacheDescriptor> &caches, uint32_t val, uint32_t size, cacheType type )
{
	uint8_t associativity, lineSize;

	lineSize = val & 0xFF;
	associativity = ( val>>12 ) & 0xF;

	if( associativity )
	{
		if( associativity == 0xF )
			associativity = size/REGULAR_PAGE_SIZE;
		else
			associativity = amdL2L3Associativity[associativity];

		cacheDescriptor cacheObj;
		cacheObj.type = type;
		cacheObj.size = size;
		cacheObj.associativity = associativity;
		cacheObj.entries =  lineSize;
		caches.insert( cacheObj );
	}
}

//! Retrieves info about the L2 and L3 caches
void Processor::amdParseL2andL3()
{
	uint32_t l2Info, l3Info, size;
	//Query the CPUID function for L2 cache data
	asm ( "cpuid\n": "=c" ( l2Info ), "=d" ( l3Info ): "a" ( CPUID_AMD_L2_AND_L3_CACHE ) : "ebx" );

	size = ( l2Info>>16 )*1024;

	amdParseHighCache( caches, l2Info, size, cacheL2 );

	size = ( l3Info>>18 )*512*1024;
	amdParseHighCache( caches, l3Info, size, cacheL3 );
}

