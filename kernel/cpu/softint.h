#ifndef CPU_SOFT_INT_H_
#define CPU_SOFT_INT_H_

#include <templates/vector.cc>


class softInterrupt
{
	public:
	    typedef int (*intHandler)(void *, const int);

		softInterrupt();
		void insert( intHandler h, void *context, const int handler_index );
		bool call();
	private:
        class softIntEntry
        {
            public:
                softIntEntry(){};
                softIntEntry(intHandler h, void *c, const int handlerIndex) : handler(h), context(c), index(handlerIndex){};

                intHandler handler;
                void *context;
                int index;
        };


		Vector < softIntEntry > handlers;
};

#endif

