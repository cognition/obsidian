#ifndef CPU_REGISTERS_H_
#define CPU_REGISTERS_H_

#include <compiler.h>

//! Saved registers structure
struct COMPILER_PACKED savedRegs
{
	//! DS Segment register, offset 0
	uint16_t ds;
	//! ES Segment register, offset 2
	uint16_t es;
	//! FS Segment register, offset 4
	uint16_t fs;
	//! GS Segment register, offset 6
	uint16_t gs;
	//! R8 General purpose register, offset 8
	uint64_t r8;
	//! R9 General purpose register, offset 16
	uint64_t r9;
	//! R10 General purpose register, offset 24
	uint64_t r10;
	//! R11 General purpose register, offset 32
	uint64_t r11;
	//! R12 General purpose register, offset 40
	uint64_t r12;
	//! R13 General purpose register, offset 48
	uint64_t r13;
	//! R14 General purpose register, offset 56
	uint64_t r14;
	//! R15 General purpose register, offset 64
	uint64_t r15;
	//! RBP General purpose register, offset 72
	uint64_t rbp;
	//! RDI General purpose register, offset 80
	uint64_t rdi;
	//! RSI General purpose register, offset 88
	uint64_t rsi;
	//! RAX General purpose register, offset 96
	uint64_t rax;
	//! RBX General purpose register, offset 104
	uint64_t rbx;
	//! RCX General purpose register, offset 112
	uint64_t rcx;
	//! RDX General purpose register, offset 120
	uint64_t rdx;
	//! Interrupt or exception vector, offset 128
	uint64_t vector;
	//! Error value, offset 136
	uint64_t error;
	//! Instruction pointer, offset 144
	uint64_t rip;
	//! Code segment, offset 152
	uint64_t cs;
	//! FLAGS register value, offset 160
	uint64_t rflags;
	//! Stack pointer, privilege change, offset 168
	uint64_t rsp;
	//! Stack segment, privilege change, offset 176
	uint64_t ss;
};

union xmmReg
{
	uint64_t qwords[2];
	uint32_t dwords[4];
	uint16_t words[8];
	uint8_t bytes[16];
};

struct COMPILER_PACKED sseRegs
{
	xmmReg xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7, xmm8, xmm9, xmm10, xmm11, xmm12, xmm13, xmm14, xmm15;
};

struct COMPILER_PACKED fxState
{
	uint16_t fcw, fsw, ftw;
	uint8_t _reserved0;
	uint16_t fOp;
	uint32_t fpuIp;
	uint16_t fpuCs;
	uint32_t fpuDp;
	uint16_t fpuDs, _reserved1;
	uint32_t mxcsr, mxcsrMask;
	uint8_t mm0[16], mm1[16], mm2[16], mm3[16], mm4[16], mm5[16], mm6[16], mm7[16];
	sseRegs xmmBank;
	uint64_t _reserved2[6];
	uint64_t available[6];
};

struct COMPILER_PACKED noFxState
{
	uint8_t fpuState[104];
	uint32_t mxcsr;
	sseRegs xmmBank;
};

struct COMPILER_PACKED ymmReg
{
	xmmReg xmm[2];
};

union vectorRegs
{
	fxState fxData;
	noFxState nonFxData;
};

#endif
