#include "lock.h"
#include "localstate.h"

#include <io/io.h>
#include <addrspace/kern_map.h>

extern "C" void lock_error(const uintptr_t lockAddr, const uintptr_t callerAddr, const uint64_t ticket);

//ticketLock class
ticketLock::ticketLock() : nextTicket(0), currentTicket(0)
{
}

void ticketLock::reset(void)
{
    nextTicket = 0;
    currentTicket = 0;
}

//spinLock class
spinLock::spinLock( ticketLock *lock ) : lockPtr( lock ), holding(false)
{
}

extern "C" void lock_error(const uintptr_t lockAddr, const uintptr_t callerAddr, const uint64_t ticket)
{
    const uint64_t *lockPtr = reinterpret_cast <const uint64_t *> (lockAddr);
    uintptr_t rbase, roffset;
    String regionName = kernelSpace::decodeAddress(callerAddr, rbase, roffset);

    kprintf("[ CPU]: Acquisition stalled acquiring lock %p from caller %s+%X ticket: %u\nLOCK[0]: %X\nLOCK[1]: %X\n",
            lockAddr, regionName.toBuffer(), roffset, ticket, lockPtr[0], lockPtr[1]);
    asm volatile ("cli\n"
                  "hlt\n"::);
}

void COMPILER_INLINE spinLock::acquire(void *callerAddr)
{
    uint64_t num = 1;
    if(!callerAddr)
        callerAddr = __builtin_return_address(0);
    asm volatile (
		"pushf\n"
		"cli\n"
		"pop %0\n"
	    "lock xadd %1, (%2)\n"
	    "jmp 2f\n"
	    "1:\n"
	    "inc %%ecx\n"
	    "jno 3f\n"
	    "call lock_error\n"
	    "3:\n"
	    "pause\n"
	    "2:"
	    "cmp %1, 8(%2)\n"
	    "jne 1b\n"
	    : "=&r" (procFlags), "+d" (num): "D" ( lockPtr ), "c" (0),  "S" (callerAddr): "memory");
    holding = true;
}

void COMPILER_INLINE spinLock::release()
{
	asm volatile ("lock incq 8(%1)\n"
         "push %0\n"
         "popf\n" :: "r" (procFlags), "r" (lockPtr): "memory");
    holding = false;
}

//manualScopeLock class
manualScopedLock::manualScopedLock( ticketLock *lock) : spinLock(lock)
{
}

manualScopedLock::~manualScopedLock()
{
    if(holding)
        release();
}

//spinLockInstance class
//! Constructs and acquires a spinlock instance
COMPILER_INLINE spinLockInstance::spinLockInstance( ticketLock *lock, void *addr ) : spinLock( lock )
{
    acquire(addr);
}


//! Destructs and frees a spinlock instance
COMPILER_INLINE spinLockInstance::~spinLockInstance()
{
	release();
}

//spinLockObj class
spinLockObj::spinLockObj() : spinLock( &lock ), lock()
{
}
