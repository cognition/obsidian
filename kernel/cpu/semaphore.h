#ifndef CPU_SEMAPHORE_H_
#define CPU_SEMAPHORE_H_

#include <compiler.h>
#include "lock.h"
#include <task.h>
#include <events.h>

template <typename T> class fifoQueueMember
{
    public:
        fifoQueueMember(T init, fifoQueueMember<T> *p) : data(init), next(NULL), prev(p)
        {
        }

        ~fifoQueueMember() = default;

        T &getData()
        {
            return data;
        }

        void bindNext(fifoQueueMember<T> *n)
        {
            next = n;
        }
    private:
        T data;
    public:
        fifoQueueMember<T> *next, *prev;
};

template <typename T> class atomicFifoQueue
{
    public:
        atomicFifoQueue() : root(NULL), tail(NULL)
        {
        }

        ~atomicFifoQueue() = default;

        void enter(T obj)
        {
            spinLockInstance lockInstance(&queueLock);
            fifoQueueMember<T> *member = new fifoQueueMember<T>(obj, tail);

            if(!root)
            {
                root = member;
                tail = member;
            }
            else
            {
                tail->bindNext(member);
                tail = member;
            }
        }

        void pop(void)
        {
            spinLockInstance lockInstance(&queueLock);

            fifoQueueMember<T> *member = root;
            if(!member)
                return;
            else if(!member->next)
            {
                root = NULL;
                tail = NULL;
            }
            else
            {
                root = member->next;
                root->prev = NULL;
            }
            delete member;
        }

        T &frontRef() const
        {
            return root->getData();
        }

        bool isEmpty() const
        {
            return !root;
        }
    private:
        fifoQueueMember<T> *root, *tail;
        ticketLock queueLock;
};

class semaphoreRequest
{
    public:
        semaphoreRequest(const uint32_t requested);
        bool hasObtained(void) const;
        bool inService(void) const;
        uint32_t getRequested(void) const;

        bool invalidate(void);
        bool service(void);
        bool clearService(void);
        bool setObtained(void);

        static const constexpr uint32_t SR_FLAGS_VALID      = 0x0001U;
        static const constexpr uint32_t SR_FLAGS_SERVICING  = 0x0002U;
        static const constexpr uint32_t SR_FLAGS_OBTAINED   = 0x0004U;
    private:
        const uint32_t requestedUnits;
        volatile uint32_t flags;
};

//! A semaphore object, used for mutual exclusion
class semaphoreObj : public Event
{
	public:
		semaphoreObj( uint32_t maxUnitsValue, uint32_t initUnits );
		~semaphoreObj() = default;
		bool wait( uint32_t unitsRequest, uint16_t timeoutLimit );
		bool signal( uint32_t unitsReturned );
		bool obtain( uint32_t unitsRequested );
	private:
		atomicFifoQueue< shared_ptr<semaphoreRequest> > queue;
		ticketLock sLock;
		uint32_t maxUnits;
		volatile uint32_t units;
};

#endif
