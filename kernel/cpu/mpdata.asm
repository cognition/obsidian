%include "asmdefs.inc"


extern kernApSetup64, apTempGdtr, _end, apEarlyFlag
global ApL4:data, mpTrampolineLinkageStart, mpTrampolineLinkageEnd, mpTrampInfoStart, mpTrampInfoEnd

[BITS 64]
[default rel]
[SECTION .rodata]
mpTrampolineLinkageStart:
incbin "cpu/mptramp.bin"
mpTrampolineLinkageEnd:

[SECTION .data]
mpTrampInfoStart:
apGdtr              dq      PADJUST( apTempGdtr )
apEarlyFlagPtr      dq      PADJUST(apEarlyFlag)
apFarJumpOff        dq      PADJUST( kernApSetup64 ) + (KERNEL_CODE_64<<32)
ApL4                dq      0
mpTrampInfoEnd:

