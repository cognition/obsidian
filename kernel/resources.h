#ifndef RESOURCES_H_
#define RESOURCES_H_

#include <compiler.h>
#include <string.h>
#include <templates/avltree.cc>

//! Enumeration describing the type of a resource object
enum systemResourceType
{
    systemResourceNone = 0,
    systemResourceMemory,
    systemResourceInterrupt,
    systemResourceInterruptRoute,
    systemResourceIo,
    systemResourceIrq,
    systemResourceDma,
    systemResourcePci
};

//! Describes a raw system resource, e.g RAM, PCI addresses, etc.
class systemResource
{
	public:
		//Constructors
		systemResource();
		systemResource(const systemResource &other);
		systemResource( systemResourceType resType, uint64_t start, uint64_t size );

		systemResource &operator =(const systemResource &other);
		//Functions for sorting and comparisons
		bool operator ==( const systemResource &res ) const;
		bool operator >( const systemResource &res ) const;
		bool operator <( const systemResource &res ) const;
        bool encases(const systemResource &other) const;
        bool isExactly(const systemResource &other) const;

		//Accessors
		uint64_t getBase(void) const;
		uint64_t getLength(void) const;
		systemResourceType getType(void) const;

		String toString(void) const;

		void partition( avlTree<class systemResource> &resources, class systemResource &res );
		void extend(uint64_t additionalLength);
	private:
		//! Type of resource described by the object
		systemResourceType type;
		//! Base address of the resource's range
		uint64_t base;
		//! Length of the resource range
		uint64_t length;
};

#endif
