#ifndef CPP_RUNTIME_H_
#define CPP_RUNTIME_H_

#include <compiler.h>

typedef void ( *funcPtr )( void * );

//! Records information needed for the runtime to properly call a desctructor function at exit time
struct COMPILER_PACKED destructorCall
{
	//! Pointer to the destructor fuction
	funcPtr destructorFunc;
	//! Pointer to the object to be destroyed
	void *objPtr;
	//! Required for dynamic linking, pretty much not applicable in a kernel
	void *dsoHandle;
};

extern "C"
{
	int __cxa_atexit( funcPtr f, void *obj, void *dso );
	void __cxa_finalize( funcPtr f );
	void __cxa_pure_virtual();
}

#endif
