/* Main processor scheduler code and global scheduler namespace
 * Responsible for schedule, distributing, running, waiting and destroying tasks
 * Some additional assembly functions/hooks in stubs.asm
 *
 * Author: Joseph Kinzel */

#include "scheduler.h"
#include "timer.h"
#include <cpu/localstate.h>
#include <cpu/lock.h>
#include <io/io.h>
#include <driverapi/module_loader.h>

#include <debug.h>

#define SCHEDULER_CYCLE_LENGTH              16666
#define SCHEDULER_MIN_TASK_QUANTUM          20

#define SCHEDULER_DEEP_VALIDATE             1

#ifdef SCHEDULER_DEEP_VALIDATE
#define assert(x, ...)                      if(!(x)) { \
                                                kprintf("Assert \"%s\"failed @ %s:%u", #x, __FILE__, __LINE__);\
                                                kprintf(__VA_ARGS__);\
                                                asm volatile ("cli\n"\
                                                              "hlt\n"::);\
                                            }
#else
#define assert(x, ...)
#endif

#define SCHED_LOG_ERRORS                     0x0001U
#define SCHED_LOG_INFO                       0x0002U
#define SCHED_LOG_WAIT                       0x0004U
#define SCHED_LOG_CYCLE                      0x0008U
#define SCHED_LOG_WORK                       0x0010U
#define SCHED_LOG_TASK                       0x0020U

#define SCHED_LOG_TIMER                      0x0100U
#define SCHED_LOG_TIMER_DEEP                 0x0200U
#define SCHED_LOG_EVENTS                     0x0400U
#define SCHED_LOG_EVENTS_DEEP                0x0800U
#define SCHED_LOG_TICKS                      0x1000U

#define SCHED_CONDITIONS                     (SCHED_LOG_ERRORS | SCHED_LOG_INFO)

#define sched_log(condition, ...)            if constexpr(condition & SCHED_CONDITIONS) \
                                                kprintf("[SCHD]: " __VA_ARGS__);


static const uintptr_t initControlCache = 0x8000000B;

extern "C" void idleLoop(void *), *cleanupLoop;

extern "C" void schedWaitCurrent(Event *waitEvent, uint32_t mSec, savedRegs *regs);

/*! Constructor for a processor-specific scheduler
 *  \param schedProc Handle to the processor object which this scheduler services
 *  \param execTask Pointer to a currently executing task.  If it is null then the scheduler will execute its idle task instead
 *  \param idlePid PID of the idle task for this scheduler
 *  \param cleanupPid PID of the clean up task for this scheduler
 */
processorScheduler::processorScheduler( Processor *schedProc, Task *execTask, const pid_t idlePid, const pid_t cleanupPid ) : proc( schedProc ),
	schedSignalListener(new schedulerEvent(nullptr)),
	//Initialize all of the task pointers
	currentTask( execTask ), waitList( NULL ), retiredList( NULL ), destroyedList( NULL ),
	//Initialize the idle and cleanup tasks for this processor
	idleTask( new Task( &idleLoop, nullptr, 0, idlePid ) ), cleanupTask( new Task(&cleanupLoop, cleanupPid) ), remotePreempt( NULL ),
	//Initialize last vector task pointer and control regs cache
	lastVector( NULL ), controlCache( initControlCache ),
	//Call in the body to queueSchedTimer will initialize this
	currentSchedEvent(nullptr), currentCycleEvent(nullptr),
	//Initialize the current timers object
	schedTimer( 0 ), currentTimer( 0 ),
	//Initialize timer values
	localTicks( 0 ), currentTimerRemaining( 0 ), cycleEnd( calculateUsecDelay(SCHEDULER_CYCLE_LENGTH) ),
	//Initialize load values
	hardLoad( 0.0 ), softLoad( 0.0 ),
	//Initialize tick values
	idleTicks( 0 ), loadTicks( 0 ), initTimerTicks( 0 ), destroyedCount(0),
	//Initialize task counters
	retiredTaskCount( 0 ), taskCount( 0 ), waitTaskCount(0), schedState(0), schedSaveState(schedDiscard),
	//Initialize the task quantum values
	taskQuantum( currentTimerRemaining ), taskMinimum( SCHEDULER_MIN_TASK_QUANTUM*localState::getApicScaleUsec() ),
	//Initialize execution state
	execState( PROCESSOR_EXEC_STATE_RUNNING )
	{

	localState::setSched( this );
	idleTask->bindScheduler(this);
	cleanupTask->bindScheduler(this);

	if( execTask )
	{
	    execTask->next = nullptr;
	    execTask->prev = nullptr;
        execTask->bindScheduler(this);
        taskCount++;
    }
    else
        runIdle();

    cycleTimerInit();

    schedTimer = cycleTimer;
    currentSchedEvent = currentCycleEvent;

    runNextTimer();
    runCurrentTimer();

	Scheduler::registerProcessorScheduler( this );
}

/*! Adds a task to the scheduler's queue for the next cycle
 *  \param task Task to queue
 */
void processorScheduler::scheduleTasklet( Task *task )
{
	spinLockInstance lock( &procSchedLock );
	taskCount++;
	task->bindScheduler(this);

    pushRetiredTask( task );
	sched_log(SCHED_LOG_WORK, "CPU#%u added new task run: %u retired: %u Total: %u\n", proc->getId(), runnable.size(), retiredTaskCount, taskCount);
}

void processorScheduler::invalidateSchedTimer(void)
{
    if(schedTimer.getValue() < cycleEnd)
        currentSchedEvent->bumpGeneration();
}

//! Exits the current task and queues it for deallocation
void COMPILER_NORETURN processorScheduler::exitCurrentTask(void)
{
	MASK_ALL_INTS();

    spinLock schedLock(&procSchedLock);

    schedLock.acquire();
    sched_log( SCHED_LOG_TASK, "Exiting task %X with PID %u\n", currentTask, currentTask->getPid());

    //Move the task being terminated to the destroyed list for later cleanup
    currentTask->next = destroyedList;
    destroyedList = currentTask;
    destroyedCount++;
    taskCount--;

    currentTask->destoryAddressSpace();

    //Task will be preempted since it's stopping execution but similarly does not need its state saved
    schedState |= SCHED_PREEMPT_FLAG | SCHED_SAVED_FLAG;
    schedSaveState = schedDiscard;

    //Update the default time slice for the remainder of the scheduler cycle
    updateLocalTicks();
    uint64_t cycleRem = cycleEnd - localTicks;

    if(runnable.size())
        cycleRem /= runnable.size();


    runIdle();
    //Invalidate the current scheduler event
    invalidateSchedTimer();

    taskQuantum = cycleRem;

    schedLock.release();


    if(!currentTimerRemaining)
        triggerCurrentTimer();


    processPreempt(currentTask, nullptr);
    COMPILER_UNREACHABLE();
}

/*! Stalls execution of the current task, and does \a not preform a context switch for a certain number of microseconds
 *  \param usec Number of microseconds to wait
 */
void processorScheduler::stall( uint32_t usec )
{
	processorExecutionState current = execState;
	execState = PROCESSOR_EXEC_STATE_STALL;
	MASK_ALL_INTS();

	uint64_t init = Timer::readTSC();
	uint64_t delta = usec;
	delta *= localState::getTscScaleHundredNano();
	delta *= 10UL;
	uint64_t target = init + delta;

	uint64_t value;
	do
    {
        value = Timer::readTSC();
    } while(value < target);

    execState = current;

	UNMASK_ALL_INTS();
}

/*! Sleeps the current task for a specific number of milliseconds
 *  \param mSec Number of milliseconds to sleep the task for
 */
void processorScheduler::sleepCurrent( uint32_t mSec )
{
    taskWaitCurrent(nullptr, mSec);
}

/*! Sends the current task into a wait state based on a certain object
 *  \param waitObj Object which the task will wait for
 *  \param mSec Number of milliseconds to wait for the object
 *  \param regs Stack saved registers for the currently executing task
 */
void processorScheduler::waitCurrent(Event *additionalEvent, uint32_t mSec, savedRegs *regs)
{
    if(currentTask == idleTask)
    {
        sched_log(SCHED_LOG_ERRORS, "Attempt made to wait idle task!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    sched_log(SCHED_LOG_WAIT, "Waiting Task: %u - Enter counts - Run: %u Retired: %u Wait: %u Total: %u LT: %u\n", currentTask->getPid(), runnable.size(), retiredTaskCount, waitTaskCount, taskCount, localTicks);


    MASK_ALL_INTS();

    spinLock lock(&procSchedLock);
    lock.acquire();

    //Setup the actual wait condition
    Task *waitTask = currentTask;

    //Setup the timer related parameters and event
    updateLocalTicks();
    timedEventInstance waitInstance(calculateMsecDelay(mSec) + localTicks);
    spinLock tmpTimer(&procTimerLock);
    tmpTimer.acquire();
    timedEventInstance &tInstance = timers.insert( waitInstance );
    waitTask->waitForEvent(&tInstance);

    if(additionalEvent)
        waitTask->waitForEvent(additionalEvent);

    assert(waitList != currentTask, " circular linkage in waitList @ %p Value: %X\n", &waitList, waitList);

    //Wait queue management
    pushWaitTask(waitTask);
    waitTask->save(regs, 0);
    schedState |= SCHED_PREEMPT_FLAG | SCHED_SAVED_FLAG;
    schedSaveState = schedDiscard;
    taskQuantum = cycleEnd - localTicks;
    if(runnable.size())
        taskQuantum /= runnable.size();
    runIdle();

    sched_log(SCHED_LOG_WAIT, "Wait - Pre lock release!\n");

    lock.release();

    invalidateSchedTimer();

    if(!currentTimerRemaining)
        triggerCurrentTimer();


    if(!waitTask->kStackValidate(regs, sizeof(savedRegs)))
    {
        sched_log(SCHED_LOG_WAIT, "waitCurrent invalid state!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }
    tmpTimer.release();





    assert(taskCount == (waitTaskCount + runnable.size() + retiredTaskCount + ((currentTask != idleTask) ? 1:0)), " task count sanity check failed - T: %u W: %u Re: %u Ru: %u CT: %u\n", taskCount, waitTaskCount, retiredTaskCount, runnable.size(), ((currentTask != idleTask) ? 1:0) );
    assert(currentTask != waitTask, "Task is both waiting and running!\n");


    processPreempt(waitTask, nullptr);
}

/*! Hook from assembler code, which exists to push the task context onto the stack and create the 'regs' parameter
 *
 *  \param waitObj Object representing the condition the task will wait on
 *  \param mSec How long the task will wait for the condition to trigger before resuming execution
 *  \param regs Saved register context pointer
 */
extern "C" void COMPILER_NORETURN schedWaitCurrent(Event *otherEvent, uint32_t mSec, savedRegs *regs)
{
    MASK_ALL_INTS();
    processorScheduler *sched = localState::getSched();
    Task *et = sched->getCurrentTask();

    if(!et->kStackValidate(regs, sizeof(savedRegs)))
    {
        sched_log(SCHED_LOG_ERRORS, "SchedWaitCurrent entered with invalid state!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }
    sched->waitCurrent(otherEvent, mSec, regs);

    COMPILER_UNREACHABLE();
}

/*! Updates the internal state of the processor scheduler and signals the current timer
 *  \param regs Pointer to the general purpose register state for the current task
 */
bool COMPILER_NOINLINE processorScheduler::signalCurrentTimerEvent( savedRegs *regs )
{
    updateLocalTicks();
    sched_log(SCHED_LOG_TIMER_DEEP, "LAPIC Trigger! CTR: %u\n", currentTimerRemaining);
	//Check if our timer has actually triggered it's event
	if( !currentTimerRemaining )
	{
		if( execState != PROCESSOR_EXEC_STATE_STALL )
        {
            triggerCurrentTimer();
            //runNextTimer();
        }
		//Attempt to run any additional valid timers
        return true;
	}
	else
    {
        spinLockInstance timerLock(&procTimerLock);
        uint64_t initTimerRemaining = currentTimerRemaining;
        runCurrentTimer();
        sched_log(SCHED_LOG_TIMER_DEEP, "Timer roll over LT: %u InitCTR: %u ExitCTR: %u ITT: %u schedState: %X\n", localTicks, initTimerRemaining, currentTimerRemaining, initTimerTicks, schedState);
        return false;
    }
}

void COMPILER_NOINLINE processorScheduler::storeTimer(timedEventInstance &timer)
{
    sched_log(SCHED_LOG_TIMER_DEEP, "Storing timer value %u Caller: %p\n", timer.getValue(), COMPILER_FUNC_CALLER);
    timers.insert(timer);
}

//! Updates the value of the current local ticks
void processorScheduler::updateLocalTicks(void)
{
	uint32_t current = proc->getApicTimerVal();
	uint32_t offset = initTimerTicks - current;
    uint32_t eitt = initTimerTicks;

    initTimerTicks = current;
	currentTimerRemaining -= min(offset, currentTimerRemaining);
	localTicks += offset;
	sched_log(SCHED_LOG_TICKS, "ULT - Offset: %u Current: %u CTR: %u LT: %u ITT: %u EITT: %u Caller: %p\n", offset, current, currentTimerRemaining,
           localTicks, initTimerTicks, eitt, COMPILER_FUNC_CALLER);

}

void processorScheduler::processSchedTimerImminent(void)
{
    if(schedTimer < currentTimer)
    {
        timers.insert(currentTimer);
        timers.retrieveBest(currentTimer);
    }
}

//! Runs the next timer, if one is present
void COMPILER_NOINLINE processorScheduler::runNextTimer(void)
{
    sched_log(SCHED_LOG_TIMER_DEEP, "RNT Enter! Caller: %p Initial timer: %u\n", COMPILER_FUNC_CALLER, currentTimer.getValue() );

    do
    {
        if(currentTimerRemaining)
            storeTimer(currentTimer);

        if(!timers.retrieveBest(currentTimer))
        {
            sched_log(SCHED_LOG_ERRORS, "No additional timers available!\n");
        }
        else
            sched_log(SCHED_LOG_TIMER_DEEP, "Got timer with value: %u LT: %u\n", currentTimer.getValue(), localTicks);

        currentTimerRemaining = currentTimer.getValue() - localTicks;
        //If the timer's quantum has already expired due to a stall event, make sure to signal and remove it
        if( currentTimer.getValue() <= localTicks )
        {
            triggerCurrentTimer();
        }
        else
        {
            //Initialize the time remaining variable
            currentTimerRemaining = currentTimer.getValue() - localTicks;

            sched_log(SCHED_LOG_TIMER_DEEP, "Running timer CT: %u CTC: %u ST: %u STC: %u LT: %u Caller: %p\n", currentTimer.getValue(), currentTimer.getListenCount(),
                    schedTimer.getValue(), schedTimer.getListenCount(), localTicks, COMPILER_FUNC_CALLER);
            return;
        }
    } while(!currentTimerRemaining);
}

//! Sets up the processor's LAPIC Timer to run a timer
void processorScheduler::runCurrentTimer(void)
{
    //Initialize the time remaining variable
    currentTimerRemaining = currentTimer.getValue() - localTicks;

    if( currentTimerRemaining < UINT32_MAX )
        initTimerTicks = currentTimerRemaining;
    else
        initTimerTicks = UINT32_MAX;

    sched_log(SCHED_LOG_TIMER_DEEP, "RCT - LT: %u CTR: %u TV: %u ITT: %u Caller: %p\n", localTicks, currentTimerRemaining, currentTimer.getValue(), initTimerTicks, COMPILER_FUNC_CALLER);

    //Start the timer
    proc->apicStartTimer( initTimerTicks );
}

//! Triggers the event sequence tied to current timer
void processorScheduler::triggerCurrentTimer(void)
{
    if(currentTimer.getValue() > localTicks)
    {
        sched_log(SCHED_LOG_ERRORS, "Error invalid trigger current timer call! Caller: %p\n", COMPILER_FUNC_CALLER);
    }


    sched_log(SCHED_LOG_TIMER_DEEP, "Current trigger! ST: %u STC: %u CT: %u CTC: %u Caller: %p\n", schedTimer.getValue(), schedTimer.getListenCount(),
        currentTimer.getValue(), currentTimer.getListenCount(), COMPILER_FUNC_CALLER );

    if( !currentTimer.hasEvents() )
    {
        sched_log(SCHED_LOG_ERRORS, "Current timer has no events!\n");
        asm volatile("cli\n"
                     "hlt\n"::);
    }
    currentTimer.signalListeners();
}

/*! Returns a pointer to the current task
 *  \return Pointer to the current task
 */
Task *processorScheduler::getCurrentTask(void)
{
	return const_cast <Task *> ( currentTask );
}

/*! Returns the last task to use the processor's vector extensions
 * \return Pointer to the last task that used vector extensions or NULL if no task currently on this processor has used vector extensions
 */
Task *processorScheduler::getLastActiveVectorTask(void)
{
	return lastVector;
}

/*! Sets the last task to use the processor's vector extensions
 * \param t Task that last used the vector extensions of this processor
 */
void processorScheduler::setLastActiveVectorTask(Task *t)
{
	lastVector = t;
}


void processorScheduler::unmaskVectorCatch(void)
{
	controlCache &= ~Processor::x86_cr0_NM;
	asm volatile ("clts\n"::);
}

void processorScheduler::maskVectorCatch(void)
{
	if(controlCache & Processor::x86_cr0_NM)
		return;
	else
	{
		controlCache |= Processor::x86_cr0_NM;
		asm volatile ("mov %0, %%cr0\n":: "r" (controlCache) );
	}
}

/*! Attempts to unwait a currently waiting task
 *
 *  \param caller Scheduler invoking the unwait request
 *  \param t Task to be unwaited
 */
void processorScheduler::unwaitTask(processorScheduler *caller, Task *t)
{
    assert(t, "Attempted to unwait a null task");
    assert(taskCount == (waitTaskCount + runnable.size() + retiredTaskCount + ((currentTask != idleTask) ? 1:0)), " task count sanity check failed - T: %u W: %u Re: %u Ru: %u CT: %u\n", taskCount, waitTaskCount, retiredTaskCount, runnable.size(), ((currentTask != idleTask) ? 1:0) );
    assert(waitTaskCount > 0, " No tasks waiting...\n");

    sched_log(SCHED_LOG_WAIT, "CPU #%u is attempting to unwait task %u\n", caller->getProcId(), t->getPid() );

    if(caller == this)
    {
        spinLockInstance schedLock(&procSchedLock);

        removeWaitTask(t);

        attemptTaskPreempt(t);

    }
    else
        attemptRemotePreempt(t);
    sched_log(SCHED_LOG_WAIT, "Unwait successful!\n");
    assert(taskCount == (waitTaskCount + runnable.size() + retiredTaskCount + ((currentTask != idleTask) ? 1:0)), " task count sanity check failed - T: %u W: %u Re: %u Ru: %u CT: %u\n", taskCount, waitTaskCount, retiredTaskCount, runnable.size(), ((currentTask != idleTask) ? 1:0) );

}

/*! Attempts to preempt the currently executing task with another
 *  \param task Task attempting to preempt the currently executing task
 */
void processorScheduler::attemptTaskPreempt( Task *task )
{
    assert(currentTask != retiredList, " attempted to preempt an already retired task.\nCT: %p Idle: %p\n", currentTask, idleTask);

    updateLocalTicks();
    uint64_t cycleRem = cycleEnd - localTicks;

    sched_log(SCHED_LOG_EVENTS_DEEP, "ATP - Task: %p Current task: %p Idle: %p Current Time remaining: %u\n", task, currentTask, idleTask, currentTimerRemaining);

    //If the processor isn't doing anything then run the requested task immediately
    if( currentTask == idleTask )
    {
        if(cycleRem >= SCHEDULER_MIN_TASK_QUANTUM)
        {
            pushRunnableTask(task);
            schedState |= SCHED_PREEMPT_FLAG;
            schedSaveState = schedDiscard;

            invalidateSchedTimer();

            taskQuantum = cycleRem;


            return;
        }
    }
	else if( task->getPriority() > currentTask->getPriority() )
	{
	    cycleRem = cycleEnd - localTicks;
        cycleRem /= (runnable.size()+1);
        if(cycleRem >= SCHEDULER_MIN_TASK_QUANTUM)
        {
            //Similar save and preempt the currently running task if it is of lower priority than the task attempting to run
            pushRunnableTask(task);
            schedState |= SCHED_PREEMPT_FLAG;
            schedSaveState = schedSaveRunnable;
            taskQuantum = cycleRem;
            invalidateSchedTimer();

            return;
        }
	}
	else
	{
	    if(currentTask == task)
        {
            sched_log(SCHED_LOG_ERRORS, "Error! Task attempted to preempt itself!\n");
            asm volatile ("cli\n"
                          "hlt\n"::);
        }
	    //Otherwise push it to the appropriate position into the run list if there's enough time to do so
	    cycleRem = cycleEnd - schedTimer.getValue();
	    cycleRem /= (runnable.size()+1);
		if(cycleRem >= SCHEDULER_MIN_TASK_QUANTUM)
		{
			taskQuantum = cycleRem;

            pushRunnableTask(task);
            return;
		}
	}

    pushRetiredTask(task);


    assert(taskCount == (waitTaskCount + runnable.size() + retiredTaskCount + ((currentTask != idleTask) ? 1:0)), " task count sanity check failed - T: %u W: %u Re: %u Ru: %u CT: %u\n", taskCount, waitTaskCount, retiredTaskCount,
           runnable.size(), ((currentTask != idleTask) ? 1:0) );
}

/*! Removes a currently waiting task from the task wait list
 *
 *  \param t Task to remove from the wait list
 */
void processorScheduler::removeWaitTask(Task *t)
{
        if(t->prev)
            t->prev->next = t->next;
        else
        {
            assert(waitList == t, "attempted to unwait floating task: %p\n", t);
            waitList = t->next;
        }

        if(t->next)
            t->next->prev = t->prev;

        t->next = nullptr;
        t->prev = nullptr;

        waitTaskCount--;
}

/*! Pops a retired task from the retired list
 *
 *  \return Task from the front of the retired list
 */
Task *processorScheduler::popRetiredTask(void)
{
    Task *t = retiredList;

    retiredList = t->next;
    if(retiredList)
        retiredList->prev = nullptr;


    t->prev = nullptr;
    t->next = nullptr;

    retiredTaskCount--;

    return t;
}

/*! Pops a runnable task from the runnable heap
 *
 *  \return The task at the top of the runnable heap
 */
Task *processorScheduler::popRunnable(void)
{
    Task::taskPrioEntry tpe;
    runnable.retrieveBest(tpe);

    Task *t = tpe.getTask();

    return t;
}

/*! Pushes a runnable task onto the runnable heap
 *
 *  \param t Task to put on the runnable heap
 */
void processorScheduler::pushRunnableTask(Task *t)
{
    Task::taskPrioEntry tpe = t->toTPE();
    runnable.insert( tpe );
}

/*! Pushes a task to the waiting list
 *
 *  \param t Task to add to the wait list
 */
void processorScheduler::pushWaitTask(Task *t)
{
    t->prev = nullptr;

    t->next = waitList;
    if(waitList)
        waitList->prev = t;
    waitList = t;
    waitTaskCount++;
}

void processorScheduler::pushRetiredTask(Task *task)
{
    task->next = retiredList;
    if(retiredList)
        retiredList->prev = task;

    retiredList = task;
    retiredTaskCount++;

}

/*! Attempts to preempt the currently running task
 *
 * \param enterTask Active task at entry
 * \param enterRegs Register structure of the entry task
 * \param alwaysRunNextTimer Flag indicating if the function should always attempt to run the next timer, regardless of preemption status
 *
 */
void processorScheduler::processPotentialPreempt(Task *enterTask, savedRegs *enterRegs, const bool alwaysRunNextTimer)
{
    sched_log(SCHED_LOG_TICKS, "PPP! Caller: %p\n", COMPILER_FUNC_CALLER);

    if(schedState & SCHED_PREEMPT_FLAG)
    {
        processPreempt(enterTask, enterRegs);
    }
    else if(alwaysRunNextTimer)
    {
        spinLock timerLock(&procTimerLock);
        timerLock.acquire();
        sched_log(SCHED_LOG_TIMER, "Non-preempted timer next timer run!\n");
        runNextTimer();
        timerLock.release();
        if(schedState & SCHED_PREEMPT_FLAG)
            processPreempt(enterTask, enterRegs);
        else
            runCurrentTimer();

    }
}

/*! Preforms the actual task preemption operation
 *
 *  \param enterTask Active task prior to a preemption operation
 *  \param enterRegs Saved register context of the active task
 *
 */
void COMPILER_NORETURN processorScheduler::processPreempt(Task *enterTask, savedRegs *enterRegs)
{
    MASK_ALL_INTS();
    spinLock timerLock(&procTimerLock);
    spinLock schedDataLock(&procSchedLock);

    timerLock.acquire();

    do
    {
        schedState &= ~SCHED_PREEMPT_FLAG;

        /* Attempt to save the state of entering task if it has not been saved yet */
        if( !(schedState & SCHED_SAVED_FLAG) )
        {
            sched_log(SCHED_LOG_TASK, "Saving task %p PID: %u\n", enterTask, enterTask->getPid() );
            schedState |= SCHED_SAVED_FLAG;

            if(enterRegs)
                enterTask->save(enterRegs, COMPILER_FUNC_CALLER);

            //Only save the task to a list if it isn't the idle or
            if(currentTask && (currentTask != idleTask) && (currentTask != cleanupTask))
            {
                schedDataLock.acquire();

                switch(schedSaveState)
                {
                    case schedSaveRetired:
                        pushRetiredTask(currentTask);
                        currentTask->setQuantum(0);
                        break;
                    case schedSaveRunnable:
                        pushRunnableTask(currentTask);
                        break;
                    case schedDiscard:
                        currentTask->setQuantum(0);
                        break;
                    /*
                    case schedSaveWait:
                        pushWaitTask(currentTask);
                        break;
                    */
                    default:
                        sched_log(SCHED_LOG_ERRORS, "processPreempt called with invalid save value of %u\n", schedSaveState);
                        break;
                }

                schedSaveState = schedDiscard;
                runIdle();
                schedDataLock.release();
            }
        }

        runNextTimer();
    } while(schedState & SCHED_PREEMPT_FLAG);

    schedDataLock.acquire();

    if( runnable.empty() )
    {
        idleTask->setQuantum(0);
        taskQuantum = cycleEnd - localTicks;
        runIdle();
    }
    else
        setCurrentTask( popRunnable() );

    /* Clear the scheduler state */
    schedState = 0;
    schedSaveState = schedDiscard;

    schedDataLock.release();


    if(!currentTask->getQuantum())
        currentTask->setQuantum(taskQuantum);

    /* Queue up the schedule timer and load the current timer into the hardware  */
    queueSchedTimer( currentTask->getQuantum() );
    processSchedTimerImminent();
    runCurrentTimer();
    timerLock.release();

    if((currentTask != idleTask) && !currentTask->assertInterruptsEnabled() )
        sched_log(SCHED_LOG_ERRORS, "Interrupts not enabled for task %p!\n", currentTask);

    executeCurrent();

    COMPILER_UNREACHABLE();
}

/*! Attempts to preempt the scheduler from another logical processor
 *  \param task Task attempting to preempt
 */
void processorScheduler::attemptRemotePreempt( Task *task )
{
	uint32_t id = proc->getId();
	//Spin until the remote task preemption field the scheduler is open, then set it to the task
	while ( !__sync_bool_compare_and_swap(&remotePreempt, NULL, task) )
	{
		asm volatile ("pause\n"::);
	}

	//Send an IPI to the other scheduler to allow it to try and preempt it's currently running task
	Processor *localProc = localState::getProc();
	localProc->sendIPI(id, VECTOR_SCHED_REMOTE_PREEMPT, LAPIC_DELIVERY_FIXED | LAPIC_DESTINATION_PHYSICAL );
}

//! Executes the necessary steps to perform a remotely call task preemption
void processorScheduler::executeRemotePreempt(void)
{
    while(remotePreempt)
    {
        spinLockInstance lock(&procSchedLock);
        Task *t = const_cast <Task *> (remotePreempt);
        remotePreempt = nullptr;
        if(!t)
        {
            sched_log(SCHED_LOG_ERRORS, "Remote preempt with null task!\n");
            return;
        }

        removeWaitTask(t);
        attemptTaskPreempt(t);
    }
}

//! Signals that a scheduling event has occurred to the processor
void processorScheduler::invokeCurrentSchedulerEvent(void *caller)
{
    //Lock up
    spinLockInstance lock(&procSchedLock);
    sched_log(SCHED_LOG_EVENTS_DEEP, "invokeCSE triggered! LT: %u CE: %u TQ: %u Caller: %p\n", localTicks, cycleEnd, taskQuantum, caller);
    //schedState |= SCHED_CSE_FLAG;
    //Run the next task cycle if the time is appropriate

    if( localTicks >= cycleEnd )
    {
        schedNextCycle();
    }
    else
    {
        schedState |= SCHED_PREEMPT_FLAG;
        schedSaveState = schedSaveRetired;
    }
}

//! Schedules the next cycle of tasks
void processorScheduler::schedNextCycle(void)
{
    sched_log(SCHED_LOG_CYCLE, "Laying out next cycle... on CPU#%u %u running tasks, %u retired tasks, %u waiting tasks and %u total tasks.\n", proc->getId(),
            runnable.size(), retiredTaskCount, waitTaskCount, taskCount);


    assert( validateTaskCounts(), "Task count mismatch before schedule next cycle TC: %u RuC: %u ReC: %u WC: %u AC: %u\n", taskCount, runnable.size(), retiredTaskCount, waitTaskCount, calcActiveCount() );
    //Make sure we save the current task, if it's not simply idle time
    schedState |= SCHED_PREEMPT_FLAG;
	if((currentTask != idleTask) && (currentTask != cleanupTask) && currentTask)
    {
        pushRunnableTask(currentTask);
        currentTask->setQuantum(0);
    }

    //If there's eligible tasks move them back into the run queue
    while(retiredTaskCount)
    {
        Task *nextTask = popRetiredTask();
        nextTask->setQuantum(0);
        pushRunnableTask(nextTask);
    }

    assert(!retiredList && !retiredTaskCount, "Retired list pointer and task count mismatch! PTR: %p Count: %u\n", retiredList, retiredTaskCount);

    cycleTimerInit();
 	currentTask = idleTask;

    schedSaveState = schedDiscard;
    sched_log(SCHED_LOG_CYCLE, "Next cycle calculated - CE: %u LT: %u TQ: %u\n", cycleEnd, localTicks, taskQuantum);

}

//! Initializes the scheduler cycle timer parameters
void processorScheduler::cycleTimerInit(void)
{
    //Reset the cycle endpoint
    cycleEnd = calculateUsecDelay(SCHEDULER_CYCLE_LENGTH);

    taskQuantum = cycleEnd;

	//Calculate the new task quantum
	if( runnable.size() )
    {
        taskQuantum /= runnable.size();
    }

    //Schedule the first timer
    cycleEnd += localTicks;

    cycleTimer.clearEvents();
    cycleTimer.setValue(cycleEnd);
    currentCycleEvent = new schedulerEvent(currentTask);
    cycleTimer.pushListener( shared_ptr<eventListener> (currentCycleEvent) );
    storeTimer(cycleTimer);
}

/*! Calculates the number of ticks to wait from a length given in microseconds
 *  \param usec Number of microseconds
 *  \return Number of ticks to wait
 */
uint64_t processorScheduler::calculateUsecDelay( uint64_t usec )
{
	uint64_t delay = usec;
	delay *= localState::getApicScaleUsec();
	return delay;
}

/*! Calculates the number of ticks to wait from a length given in milliseconds
 *  \param usec Number of milliseconds
 *  \return Number of ticks to wait
 */
uint64_t processorScheduler::calculateMsecDelay(uint64_t msec )
{
    uint64_t delay = msec*1000;
    delay *= localState::getApicScaleUsec();
    return delay;
}

//! Executes the current task
void COMPILER_NORETURN processorScheduler::executeCurrent(void)
{
    sched_log(SCHED_LOG_TASK, "Running Task: %p\n", currentTask);
    currentTask->execute();
    COMPILER_UNREACHABLE();
}

/*! Set the current task the scheduler is executing
 *
 *  \param t Task to set to the currently running task
 */
void processorScheduler::setCurrentTask(Task *t)
{
    //kprintf("[SCHD]: Updating current task from %p to %p Idle: %p\n", currentTask, t, idleTask);
    //kprintf("[SCHD]: Updating CT from PID: %u to PID: %u Caller: %p\n", currentTask->getPid(), t->getPid(), COMPILER_FUNC_CALLER );
    currentTask = t;
}

//! Runs the idle thread
void COMPILER_NOINLINE processorScheduler::runIdle(void)
{
    sched_log(SCHED_LOG_TASK, "Running idle task... Caller: %p\n", COMPILER_FUNC_CALLER);
	setCurrentTask(idleTask);
}

//! Releases the resources of any destroyed tasks
void processorScheduler::cleanDestroyed(void)
{
    Task *destroyed = destroyedList;
    while(destroyed)
    {
        Task *next = destroyed->next;
        delete destroyed;
        destroyedCount--;
        destroyed = next;
    }
}

/*! Updates the scheduler timer to the next relevant trigger event, which is either the provided delay value or the end of the scheduler cycle
 *  \param delayTicks Number of processor timer ticks to attempt to delay until
 */
void COMPILER_NOINLINE processorScheduler::queueSchedTimer(uint64_t delayTicks)
{
    uint64_t targetTicks = delayTicks + localTicks;


    if(targetTicks < cycleEnd )
    {
        sched_log(SCHED_LOG_TIMER_DEEP, "QSTQ - TT - Delay: %u Target: %u LT: %u CE: %u Task: %p Caller: %p\n",
                  delayTicks, targetTicks, localTicks, cycleEnd, currentTask, COMPILER_FUNC_CALLER);

        schedTimer.setValue(targetTicks);
        //Make sure to reattach our singular scheduler event
        schedTimer.clearEvents();

        currentSchedEvent = new schedulerEvent(currentTask);

        schedTimer.pushListener( shared_ptr<eventListener> (currentSchedEvent) );
        storeTimer(schedTimer);
    }
    else
    {

        sched_log(SCHED_LOG_TIMER_DEEP, "QSTQ - CE - Delay: %u Target: %u LT: %u CE: %u Task: %p Caller: %p\n",
                  delayTicks, targetTicks, localTicks, cycleEnd, currentTask, COMPILER_FUNC_CALLER);

        //Cycle timer is always valid, just update the owner and have the current schedule timer and events point to it
        schedTimer = cycleTimer;
        currentCycleEvent->updateOwner(currentTask);
        currentSchedEvent = currentCycleEvent;
    }

}

bool processorScheduler::operator ==(const processorScheduler &sched ) const
{
	if( retiredTaskCount == sched.retiredTaskCount )
		if( hardLoad == sched.hardLoad )
			if( softLoad == sched.softLoad )
				return true;
	return false;
}

bool processorScheduler::operator >(const processorScheduler &sched ) const
{
    uint64_t loadQ, otherQ;

    loadQ = hardLoad;
    otherQ = sched.hardLoad;

    loadQ <<= 32;
    otherQ <<= 32;
    loadQ |= softLoad;
    otherQ |= sched.softLoad;

    if(loadQ > otherQ)
        return true;
	else if( (loadQ == otherQ) && (retiredTaskCount > sched.retiredTaskCount) )
		return true;
    else
        return false;
}

bool processorScheduler::operator <(const processorScheduler &sched ) const
{
    uint64_t loadQ, otherQ;

    loadQ = hardLoad;
    otherQ = sched.hardLoad;

    loadQ <<= 32;
    otherQ <<= 32;
    loadQ |= softLoad;
    otherQ |= sched.softLoad;


	if(loadQ < otherQ)
        return true;
	else if( (loadQ == otherQ) && (retiredTaskCount < sched.retiredTaskCount) )
		return true;
    else
        return false;
}


extern "C"
{
	void procAvailable(savedRegs *regs);
	void procSchedCleanup(void);


	//! Used to execute kernel specific maintenance when the processor is free
	void procAvailable(savedRegs *regs)
	{
	    //MASK_ALL_INTS();
	    processorScheduler *sched = localState::getSched();
	    Task *ct = sched->getIdle();

	    if(!ct->kStackValidate(regs, sizeof(savedRegs)))
        {
            sched_log(SCHED_LOG_ERRORS, "Idle entered with invalid state!\n");
            asm volatile ("cli\n"
                          "hlt\n"::);
        }

        moduleLoader::loadModules();
        //kprintf("Idle PPT\n");
        sched->processPotentialPreempt(ct, regs);
        if(ct != sched->getCurrentTask())
        {
            sched_log(SCHED_LOG_ERRORS, "Failed to preempt appropriately! CT: %p Idle: %p\n", sched->getCurrentTask(), ct);
            asm volatile ("cli\n"
                          "hlt\n"::);
        }
        UNMASK_ALL_INTS();

	}

	//! Task clean up logic
	void procSchedCleanup(void)
	{
	    processorScheduler *sched = localState::getSched();
		sched->cleanDestroyed();
	}
}
//Scheduler namespace

//! Tree holding the available processor schedulers
static cachedAvlTree<processorScheduler *, pointerContentsComparable<processorScheduler> > procSchedulerTree;
//! Global scheduler spinlock
static ticketLock globalSchedLock;


namespace Scheduler
{
    /*! Registers a processor specific scheduler with the global scheduler
     *  \param sched Processor scheduler to register
     */
    void registerProcessorScheduler( processorScheduler *sched )
    {
        spinLockInstance lock( &globalSchedLock );
        procSchedulerTree.insert( sched );
    }

    /*! Schedules a task for execution
     *  \param task Task to schedule
     *  \return schedSuccess if there were sufficient resources to schedule the task, schedNoneAvailable otherwise
     */
    schedResult registerTask( Task *task )
    {
        spinLockInstance lock( &globalSchedLock );
        processorScheduler *bestAvailable;
        if( !procSchedulerTree.retrieveMinimum( bestAvailable ) )
        {
            sched_log(SCHED_LOG_ERRORS, "No processors available! Tree: %p\n", &procSchedulerTree);
            return schedNoneAvailable;
        }
        else
        {
            bestAvailable->scheduleTasklet( task );
            procSchedulerTree.insert( bestAvailable );
            return schedSuccess;
        }
    }

	/*! Returns the task actively running on a logical processor
	 *  \return Task that is currently running on the processor
	 */
	Task *getCurrentTask(void)
	{
    	processorScheduler *sched = localState::getSched();
		return sched->getCurrentTask();
	}

	/*! Sets the currently executing task into a wait state
	 *  \param otherEvent Alternate event that can lift the wait condition besides the timer
	 *  \param mSec Amount of time to wait in milliseconds
	 */
    void waitCurrent( Event *otherEvent, uint32_t mSec)
    {
        //kprintf("[SCHD]: WC - WO: %p Caller: %p\n", waitObj, COMPILER_FUNC_CALLER);
    	taskWaitCurrent(otherEvent, mSec);
    }

    void sleep(const uint32_t mSec)
    {
        processorScheduler *sched = localState::getSched();
		sched->sleepCurrent(mSec);
    }
}

