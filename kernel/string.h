/* String object header file
 *
 * Author: Joseph Kinzel */
#ifndef STRING_H_
#define STRING_H_

#include <templates/vector.cc>
#include <templates/shared_ptr.cc>

//! Object which stores and preforms basic string operations
class String
{
	public:
		String();
		String( const char *src );
		String( const char *src, int length );
		String(const String &other);
		String &operator =( const String &src );
		String &operator =( const char *src );
		bool operator ==( const char *t ) const;
		bool operator ==(const String &other) const;
		bool operator <(const String &other) const;
		bool operator >(const String &other) const;
		String operator +(const String &other) const;
		String operator +(const char *other) const;

		bool copyToRaw(char *rawDest, const size_t maxSize);

		uint64_t hash(void) const;
		int length() const;
		//operator const char *() const;
		char *getBuffer(void);
		const char *toBuffer() const;
		const char at(const int index) const;
		int findSubstring(const char *s) const;
		int findToken(const char t, const int start = 0) const;
		int findTokenRev(const char t, const int start = -1) const;
		String substr(int start, int size) const;
		Vector<String> splitToken(const char token) const;
		shared_ptr< Vector<char> > const &getSharedPtr(void) const;

		bool hexToUnsigned(uint64_t &num) const;
		bool decToUnsigned(uint64_t &num) const;

		static int strlen( const char *s );
        static int strnlen( const char *s, const int maxLength);
        static int strncmp(const char *first, const char *second, const int totalLength);
        static int strntok(const char *str, const int maxLength, const char token);
		static String stringFromHex(const uint64_t value, const int digits = 0);
		static String stringFromDec(const uint64_t value, const int digits = 0);

        static static_shared_ptr<Vector<char> > zeroLengthString;

	private:
	    String(Vector<char> *init) : stringLength(init->length() - 1), buffer(init) {};
		//! Length of the string
		short stringLength;
		//! Buffer representing the string's data
		shared_ptr < Vector<char> > buffer;
};

class hashedString
{
    public:
        hashedString(const String &init);
        hashedString(const hashedString &other);
        bool operator ==(const hashedString &other) const;
        bool operator <(const hashedString &other) const;
        bool operator >(const hashedString &other) const;
        hashedString &operator =(const hashedString &other);

        uint64_t getHash(void) const
        {
            return hash;
        }

        class hasher
        {
        public:
            uint64_t operator()(const hashedString &hs)
            {
                return hs.hash;
            }
        };
    protected:
        uint64_t hash;
        String str;
};

#endif
