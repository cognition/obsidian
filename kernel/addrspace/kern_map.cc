/* Kernel address space map implementation
 *
 * Registers named regions of mapped memory in kernel space and decodes address into name:base:offset values.
 *
 * Author: Joseph Kinzel */

 #include "kern_map.h"
 #include <templates/map.cc>

 static Map<regionObj64, String> kernelAddressMap;

 namespace kernelSpace
{
    String decodeAddress(const uintptr_t addr, uintptr_t &base, uintptr_t &offset)
    {
        regionObj64 addrQuery(addr, 1);
        if( const String *name = kernelAddressMap.match(addrQuery) )
        {
            base = addrQuery.getBase();
            offset = addr - addrQuery.getBase();
            return *name;
        }
        else
        {
            offset = addr;
            return String("Unknown");
        }
    }

    bool registerRegion(const regionObj64 &region, const String &tag)
    {
        return kernelAddressMap.findOrInsert(region, tag);
    }

    bool deregisterRegion(const regionObj64 &region)
    {
        return kernelAddressMap.remove(region);
    }
};

