#ifndef ASREGION_H_
#define ASREGION_H_

#include <compiler.h>
#include <regionobj.h>
#include <templates/vector.cc>
#include <templates/shared_ptr.cc>
#include "page_bundle.h"

class asRegion : public regionObj64
{
    public:
        static const uint64_t regionDemandMap = 0x1;
        static const uint64_t regionExpandDown = 0x2;
        static const uint64_t regionMultipleBundleFlags = 0x4;
        static const uint64_t regionHasBundle = 0x8;
        static const uint64_t regionGroupMapped = 0x10;
        static const uint64_t regionCOW = 0x20;
        static const uint64_t regionContinuousPhys = 0x40;

        enum asRegionValues
        {
            execRegion = 0x0,
            heapRegion = 0x1,
            stackSystem = 0x3
        };

        void setBase(const uintptr_t base);
        uint64_t getFlags(void) const;
        uint64_t getMapFlags(void) const;

        virtual ~asRegion() = 0;

        bool mapAddressSpace(class addressSpace *target, const uint8_t extraAlign = 0);
        bool processCOW(const uintptr_t addr);

        asRegionValues getRegionType(void) const
        {
            return rt;
        }
        //Used to define relative region sizes
        static const uint8_t regionTypeBits = 2;

        uint64_t getPermissions(void) const
        {
            return permissions;
        }

    protected:

        explicit asRegion(uintptr_t base, uintptr_t length, const asRegionValues type, const uint64_t regPermissions);

        Vector< shared_ptr<class pageBundle> > bundles;
        Vector<uint64_t> bundleFlags;
        class addressSpace *as;
        uint64_t permissions;
        uintptr_t phys_base;
        asRegionValues rt;
        uint32_t flags;
};

class stackRegion : public asRegion
{
    public:
        stackRegion(const uintptr_t size, const uint64_t ownerPid);
};


#endif // ASREGION_H_
