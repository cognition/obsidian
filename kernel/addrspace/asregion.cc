#include "asregion.h"
#include "addrspace.h"

//asRegion class

/*! Base constructor
 *
 * \param base Initial base address of the region
 * \param length Region's total length
 * \param type Region type
 * \param ownerPid Address space pid
 */
asRegion::asRegion(uintptr_t base, uintptr_t length, const asRegionValues type, const uint64_t initPerms) : regionObj64(base, length), permissions(initPerms), rt(type), flags(0)
{
}

//! Destructor
asRegion::~asRegion()
{

}

/*! Processes a Copy-On-Write request for a given address
 *  \param addr Address to perform a COW operation on
 *  \return true if success, false if a failure occurs
 */
bool asRegion::processCOW(const uintptr_t addr)
{
    if(bundles.length() > 1)
    {
        kprintf("COW for multi-bundle regions NYI.\n");
        return false;
    }
    else
    {
        virtualMemoryManager::copySwap(addr, PAGE_PRESENT | PAGE_USER | PAGE_WRITE, false);
        return true;
    }
}

/*! Maps a region into a specific address space and the currently active space
 * \param target Address space to map the region into
 * \param extraAlign Extra alignment bits for region alignment
 * \return True if the region was mapped into the target address space successfully, false otherwise
 */
bool asRegion::mapAddressSpace(addressSpace *target, const uint8_t extraAlign)
{
    as = target;

    if(flags & regionGroupMapped)
    {
        //Deal with contiguous grouped regions
        if(!as->insertGroupRegion(this))
            return false;
    }
    else
    {
        // Attempt to get an available base address for the region in the appropriate portion of the address space
        if(!as->mapRegion(this, rt, extraAlign))
        return false;
    }
    //Map bundles if present
    if(flags & regionHasBundle)
    {
        for(int bundleIndex = 0; bundleIndex < bundles.length(); bundleIndex++)
        {
            physaddr_t regionFlags = (flags & regionMultipleBundleFlags) ? bundleFlags[bundleIndex]:bundleFlags[0];
            bundles[bundleIndex]->mapCurrent(regBase, regionFlags, this);
        }
    }
    else if(flags & regionDemandMap)
    {
        // Demand map pages, or the first page at least
        uintptr_t base;
        physaddr_t regionFlags = bundleFlags[0];
        if(flags & regionExpandDown)
            base = regBase + regLength - REGULAR_PAGE_SIZE;
        else
            base = regBase;
        virtualMemoryManager::mapPages(0, base, 1, regionFlags, true);
        Memory::set( reinterpret_cast<void *> (base), 0, REGULAR_PAGE_SIZE);
    }
    else
    {
        kprintf("ASREGION: Shouldn't be here....\n");
        return false;
    }

    return true;
}

/*! Set the base address of the region
 * \param base New base address
 */
void asRegion::setBase(const uintptr_t base)
{
    regBase = base;
}

uint64_t asRegion::getFlags(void) const
{
    return flags;
}

uint64_t asRegion::getMapFlags(void) const
{
    return bundleFlags[0];
}


//stackRegion class
stackRegion::stackRegion(const uintptr_t size, const uint64_t ownerPid) : asRegion(0, size, asRegion::stackSystem, PAGE_USER | PAGE_WRITE | PAGE_PRESENT | PAGE_NX)
{
    flags |= regionDemandMap | regionExpandDown;
    bundleFlags.insert(permissions);
}

