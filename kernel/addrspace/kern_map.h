/* Kernel side address space map header
 *
 * Author: Joseph Kinzel */

#ifndef KERN_MAP_H_
#define KERN_MAP_H_

#include <regionobj.h>
#include <string.h>

namespace kernelSpace
{
    String decodeAddress(const uintptr_t addr, uintptr_t &base, uintptr_t &offset);
    bool registerRegion(const regionObj64 &region, const String &tag);
    bool deregisterRegion(const regionObj64 &region);
};


#endif // KERN_MAP_H_
