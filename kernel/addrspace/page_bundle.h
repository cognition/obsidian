#ifndef PAGE_BUNDLE_H_
#define PAGE_BUNDLE_H_

#include <compiler.h>
#include <fs/filehandle.h>
#include <templates/shared_ptr.cc>
#include <templates/avltree.cc>
#include <templates/vector.cc>
#include <status.h>
#include "asregion.h"

class pageBundle : public regionObj64
{
    public:
        enum bundleEntryType
        {
            bundleRegularPages,
            bundlePageDirectories,
            bundleLargePages
        };

        ~pageBundle();
        size_t getPageCount(void);
        uintptr_t exchangeEntry(const uintptr_t index, const uintptr_t newEntry );
        static void createZeroBundle( shared_ptr<pageBundle> &bundle, const size_t pageCount, uintptr_t *kernPerserve );
        static bufferedFileStatus createFileBundle(Vector< shared_ptr<pageBundle> > &bundles, const size_t pageCount, shared_ptr<fileHandle> file, off_t fileOffset, uintptr_t memOffset, size_t memSize, size_t fileSize);
        bool mapCurrent(const uintptr_t vBase, uintptr_t flags, class asRegion *region);

        shared_ptr<pageBundle> clone(void);

        bool hasTemplate(void) const;
        void print(void) const;
    private:
        pageBundle(size_t pageCount, bundleEntryType t);
        pageBundle(pageBundle *src);
        void addDependent(class asRegion *as);

        static bufferedFileStatus singleFileBundle(shared_ptr<pageBundle> &bundleOut, const size_t pageCount, shared_ptr<fileHandle> file, off_t fileOffset, uintptr_t memOffset, size_t memSize, size_t fileSize);
        static bufferedFileStatus largeFileBundle(shared_ptr<pageBundle> &bundleOut, const size_t pageCount, shared_ptr<fileHandle> file, off_t fileOffset, uintptr_t memOffset, size_t memSize, size_t fileSize);


        ticketLock bundleLock;
        Vector<uintptr_t> pages;
        avlTree<class asRegion *> dependents;
        shared_ptr<pageBundle> bundleTemplate;
        bundleEntryType entryType;
};


#endif // PAGE_BUNDLE_H_
