/* Hardware memory region implementation
 *
 * User space mapping of a hardware device memory region
 *
 * Author: Joseph Kinzel */

#include "hwregion.h"

hardwareRegion::hardwareRegion(const uintptr_t physBase, const size_t regionLength, const physaddr_t hwPerms) : asRegion(0, regionLength, asRegion::stackSystem, hwPerms)
{
    phys_base = physBase;
    flags = regionContinuousPhys;
}
