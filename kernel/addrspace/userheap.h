#ifndef USER_HEAP_REGION_H_
#define USER_HEAP_REGION_H_

#include "asregion.h"

class userHeapRegion : public asRegion
{
    public:
        userHeapRegion(uintptr_t heapBase, size_t heapLength);
        virtual ~userHeapRegion();
        bool extend(uintptr_t exSize);


};


#endif // USER_HEAP_REGION_H_
