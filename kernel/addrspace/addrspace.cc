#include "addrspace.h"
#include "linkertab.h"
#include <scheduler.h>
#include <memory/phys.h>

#define DEFAULT_STACK_SIZE           40960
#define ARCH_STACK_WORD_SIZE         8

//addressSpace class

/*! Constructor
 *! \param ptBase Physical base address of the primary virtual structure
 */
addressSpace::addressSpace(const uintptr_t ptBase) : lt(nullptr), pageTableBase(ptBase)
{
};

//! Destructor
addressSpace::~addressSpace()
{
    for(auto iter = virtRegions.front(); !iter.isEnd(); iter++)
    {
        shared_ptr<asRegion> region = *iter;
        if(region->getFlags() & asRegion::regionDemandMap)
        {
            virtualMemoryManager::unmapRegion(region->getBase(), region->getLength(), true, false);
        }
    }
    virtualMemoryManager::unmapUserStructures();
}

/*! Accessor for the physical address of the primary virtual structure
 * \return Physical address of the primary virtual structure
 */
physaddr_t addressSpace::getTableBase(void) const
{
    return pageTableBase;
}

/*! Finds an available span of memory of the required length and alignment using ASLR
 *  \param length Length of the memory span
 *  \param regType Type of region
 *  \param extraAlign Number of extra bits required for alignment
 *  \return Zero if the operation failed or the address of the span if it succeeded
 */
uintptr_t addressSpace::findAvailableSpan(const uintptr_t length, const asRegion::asRegionValues regType, const uint8_t extraAlign)
{
    const uint32_t baseShift = PAGE_OFFSET_BITS + extraAlign;
    const uint32_t bits = virtualMemoryManager::maxAddressBits() - (1 + asRegion::regionTypeBits + baseShift);
    uint64_t mask = 1;

    //Form the mask for the ASLR eligible bits
    mask <<= bits;
    mask--;
    mask <<= baseShift;

    //Form the region bits value
    uintptr_t rtBits = regType;
    rtBits <<= baseShift + bits;


    shared_ptr<asRegion> *resulting = nullptr;

    regionObj64 searchRegion(0, length);

    uint32_t attemptCounter = 0;
    do
    {
        //Perform ASLR until we get a region that fits in memory
        uintptr_t base = random::get();
        base &= mask;
        base |= rtBits;
        if(!base)
            continue;

        searchRegion.setBase(base);

        resulting = virtRegions.find(searchRegion);
        attemptCounter++;
        // Fail if the attempts are exhausted
        if(attemptCounter >= 128)
            return 0;
    } while(resulting);

    return searchRegion.getBase();
}

bool addressSpace::insertGroupRegion(asRegion *reg)
{
    virtRegions.insert( shared_ptr<asRegion> (reg) );
    return true;
}


/*! Maps a region of memory into the current address space
 * \param reg Address space region to add to the address space
 * \param regType Type of region being mapped, \sa asRegion::asRegionValues
 * \param extraAlign Extra alignment bits for the region
 * \return True if the region was able to be mapped, false if an available region couldn't be found.
 */
bool addressSpace::mapRegion(class asRegion *reg, const asRegion::asRegionValues regType, const uint8_t extraAlign)
{
    uint32_t baseShift = PAGE_OFFSET_BITS + extraAlign;
    uint32_t bits = virtualMemoryManager::maxAddressBits() - (1 + asRegion::regionTypeBits + baseShift) + (regType == asRegion::heapRegion ? 1:0);
    uint64_t mask = 1;

    //Form the mask for the ASLR eligible bits
    mask <<= bits;
    mask--;
    mask <<= baseShift;

    //Form the region bits value
    uintptr_t rtBits = regType;
    rtBits <<= baseShift + bits;


    shared_ptr<asRegion> resulting;
    shared_ptr<asRegion> regPtr(reg);

    uint32_t attemptCounter = 0;
    do
    {
        //Perform ASLR until we get a region that fits in memory
        uintptr_t base = random::get();
        base &= mask;
        base |= rtBits;

        reg->setBase(base);

        resulting = virtRegions.findOrInsert(regPtr);
        attemptCounter++;
        // Fail if the attempts are exhausted
        if(attemptCounter >= 128)
            return false;
    } while(resulting != regPtr);

    return true;
}

/*! Maps a user stack into the address space
 * \return Zero if the function fails or the virtual address of the top of the new stack
 */
uintptr_t addressSpace::mapUserStack(void)
{
    // Create a new region object for the stack
    stackRegion *user = new stackRegion(DEFAULT_STACK_SIZE, Scheduler::getCurrentTask()->getPid());

    bool result = user->mapAddressSpace(this);

    if(!result)
        return 0;
    else
        return (user->getBase() + user->getLength() - ARCH_STACK_WORD_SIZE*2);
}

void addressSpace::extendUserStack(const uintptr_t stackPtr)
{
    //TODO: Implement me!
}

/*! Tests if a valid mapping exists in the address space for a provided range
 *  \param base Range base virtual address
 *  \param length Length of the range
 *  \param validateType True if the region type validation should take place
 *  \param rt Region type for type validation
 *  \return True if the span is fully contained within a valid mapped region, false otherwise
 */
bool addressSpace::validateSpan(const uintptr_t base, const uintptr_t length, const bool writeable) const
{
    regionObj64 target(base, length);
    const shared_ptr<asRegion> *exists = virtRegions.find(target);

    if(exists)
    {
        asRegion * const possible = exists->get();
        bool perms = writeable ? (possible->getPermissions() & (PAGE_WRITE | PAGE_COW)):true;
        return (possible->encases(target) && perms);
    }
    else
        return false;
}

/*! Attempts to demand map a user space page table entry into a region marked for demand mapping
 *  \param addr Virtual address of the fault/mapping request
 *  \param errorFlags Error flags associated with the page fault
 *  \return True if there was a valid mapping request into a demand mapped region, false if the operation was invalid (instruction fetch, invalid region, etc.)
 */
bool addressSpace::requestDemandMap(const uintptr_t addr, const uint64_t errorFlags)
{
    uintptr_t base = addr;
    base &= ~PAGE_OFFSET_MASK;

    regionObj64 range(base, REGULAR_PAGE_SIZE);

    //Find the corresponding region in the address space
    shared_ptr<asRegion> *exists = virtRegions.find(range);
    if(exists)
    {
        //If found, verify the request against the properties of the region
        asRegion * const potential = exists->get();
        const uint64_t regionFlags = potential->getFlags();
        if(regionFlags & asRegion::regionDemandMap)
        {
            uintptr_t regionEnd = potential->getLength() + potential->getBase();
            //The region must support demand mapping
            const uint64_t mapFlags = potential->getMapFlags();
            //Verify the region is writable if a write caused the fault
            if(( errorFlags & PAGE_WRITE ) <= (mapFlags & PAGE_WRITE))
            {
                uintptr_t pageCount = min(2, (regionEnd - addr + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE);
                virtualMemoryManager::mapPages(0, base, pageCount, mapFlags, true);
                Memory::set(reinterpret_cast<void *> (base), 0, pageCount*REGULAR_PAGE_SIZE);
                return true;
            }
            else
            {
                kprintf("AS - Demand map - Permission mismatch - Requested: %X Available: %X\n", errorFlags, mapFlags);
            }
        }
        else if(  (regionFlags & asRegion::regionCOW) && (errorFlags & PAGE_WRITE) )
        {
		//Copy on write
		return potential->processCOW(addr);
        }
        else
        {
            kprintf("AS - Demand map - Region was not marked for demand mapping!\n");
        }
    }
    else
        kprintf("AS - Demand map - Region not found! Address: %X\n", addr);

    return false;
}
