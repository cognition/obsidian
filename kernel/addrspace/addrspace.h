#ifndef ADDRSPACE_H_

#define ADDRSPACE_H_

#include <compiler.h>
#include <task.h>
#include <memory/phys.h>
#include <status.h>
#include <prng.h>
#include "asregion.h"

/*! Class representing userspace address space */
class addressSpace
{
    public:
        addressSpace(const uintptr_t ptBase);
        ~addressSpace();
        physaddr_t getTableBase(void) const;
        bool mapRegion(class asRegion *reg, const asRegion::asRegionValues regType, const uint8_t extraAlign = 0);
        uintptr_t findAvailableSpan(const uintptr_t length, const asRegion::asRegionValues regType, const uint8_t extraAlign);
        bool insertGroupRegion(asRegion *reg);

        uintptr_t mapUserStack(void);
        void extendUserStack(const uintptr_t stackPtr);
        void bindLinkTable(class linkerTableRegion *reg)
        {
            lt = reg;
        }

        linkerTableRegion *getLinkerTable(void)
        {
            return lt;
        }

        bool validateSpan(const uintptr_t base, const uintptr_t length, const bool writeable) const;
        bool requestDemandMap(const uintptr_t addr, const uint64_t errorFlags);
    private:

        avlTree < shared_ptr<asRegion>, sharedContentsComparable<asRegion> > virtRegions;
        linkerTableRegion *lt;
        Vector<class Task *> denizens;
        ticketLock spaceLock;
        physaddr_t pageTableBase;
};

#include "linkertab.h"

#endif // ADDRSPACE_H_
