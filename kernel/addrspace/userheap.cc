#include "userheap.h"
#include <memory/virtual.h>

userHeapRegion::userHeapRegion(uintptr_t heapBase, size_t heapLength) : asRegion(heapBase, heapLength, asRegion::heapRegion, PAGE_PRESENT | PAGE_WRITE | PAGE_NX | PAGE_USER)
{
    flags |= regionDemandMap;
    bundleFlags.insert(permissions);
}

userHeapRegion::~userHeapRegion()
{
}

bool userHeapRegion::extend(uintptr_t exSize)
{
    return false;
}

