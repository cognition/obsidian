#include "linkertab.h"
#include <memory/virtual.h>
#include <string.h>
#include <exec/elf_instance.h>
#include <exec/exec_manager.h>

typedef COMPILER_PACKED struct _kDynEntry
{
    //Pointer to the dynamic table
	void *dynTable;
	//Size of the dynamic table
	size_t dynTableSize;
	//Base address of the executable the table applies to
	uintptr_t baseAddress;


    //Size and location of the TLS initialization section for this object, not present if size = 0
    uintptr_t tlsSize, tlsPtr;
    //Offset of the TLS information
	int32_t tlsOffset;
	//Flags indicating TLS information
	uint32_t tlsFlags;

	//Pointer to the next TLS information entry
	struct _kDynEntry *tlsNext;

	//Either an entry address if it's an executable or the size of the symbol table if it's a library
	union
	{
		uintptr_t entry;
		size_t symTabSize;
	} typeSpecific;
} kDynEntry;

static const constexpr uint32_t linkerTabPages = 6;
static const constexpr uint32_t linkerTabMaxSize = REGULAR_PAGE_SIZE*linkerTabPages;

//Constructor
linkerTableRegion::linkerTableRegion(void) : asRegion(0, linkerTabMaxSize, asRegion::stackSystem, PAGE_PRESENT | PAGE_USER | PAGE_NX), userInfoPtr(0), availableSlotCount(254)
{
    flags |= regionHasBundle;
    bundleFlags.insert(permissions);

    shared_ptr<pageBundle> ltBundle;
    //Create a bundle of zeroed pages that will be double mapped to both user and kernel space
    pageBundle::createZeroBundle(ltBundle, linkerTabPages, &kernMapping);
    bundles.insert(ltBundle);

    //Start at 2 since entry zero is reserved for the executable itself and 1 for the dynamic linker which must be present
    regionObj32 freeslots(2, 254);
    available.insert(freeslots);
}

linkerTableRegion::~linkerTableRegion()
{
    //Unmap the kernel side table
    virtualMemoryManager::unmapRegion(kernMapping, linkerTabMaxSize, false, true);
}

/*! Allocates a free dynamic table entry slot
 * \returns A positive slot index if one was available or -1 if no slots were available
 */
int linkerTableRegion::getAvailableSlot(void)
{
    regionObj32 bestRange;
    if(!availableSlotCount)
        return -1;
    if(!available.retrieveMinimum(bestRange))
        return -1;
    else
    {
        int slot = bestRange.getBase();
        uint32_t length = bestRange.getLength();
        if(length > 1)
        {
            bestRange.setBase(slot+1);
            bestRange.setLength(length - 1);
            available.insert(bestRange);
        }
        availableSlotCount--;
        return slot;
    }
}

void linkerTableRegion::freeSlot(const int slot)
{
    /* Probably something that needs to be migrated into an actual interval tree structure */
    regionObj32 pre(slot -1, 1);
    regionObj32 post(slot +1, 1);
    regionObj32 *preResult, *postResult;

    preResult = available.find(pre);
    postResult = available.find(post);
    if(preResult && postResult)
    {
        uint32_t extend = postResult->getLength() + 1;
        available.remove(*postResult);
        preResult->setLength(extend + preResult->getLength());
    }
    else if(preResult)
        preResult->setLength(preResult->getLength() + 1);
    else if(postResult)
    {
        postResult->setBase(slot);
        postResult->setLength( postResult->getLength() + 1);
    }
    else
    {
        regionObj32 single(slot, 1);
        available.insert(single);
    }

    availableSlotCount++;

    uintptr_t entryAddress = kernMapping + sizeof(kDynEntry)*slot;
    kDynEntry *entry = reinterpret_cast<kDynEntry *>(entryAddress);
    entry->dynTable = 0;
    entry->dynTableSize = 0;
    entry->baseAddress = 0;
    entry->typeSpecific.entry = 0;
}

/*! Updates the slot value in a linker table region
 * \param slot Slot entry to update
 * \param instance Executable instance to bind into the slot
 */
void linkerTableRegion::writeSlotEntry(const int slot, const elfLoadedInstance *instance, const bool hasEntry)
{
    uintptr_t entryAddress = kernMapping + sizeof(kDynEntry)*slot;
    kDynEntry *entry = reinterpret_cast<kDynEntry *>(entryAddress);
    shared_ptr<elfSegment> dynSeg = instance->getDyn();

    //Setup the user space table entries
    entry->dynTable = reinterpret_cast<void *> (dynSeg->getBase() + instance->getBase());
    entry->dynTableSize = dynSeg->getLength();
    entry->baseAddress = instance->getBase();

    //Update the type specific entry
    if(hasEntry)
        entry->typeSpecific.entry = instance->getEntryAddr();
    else
        entry->typeSpecific.symTabSize = instance->getSymTabSize();

    if(userInfoPtr)
    {
        kDynInfo *info = reinterpret_cast<kDynInfo *>(userInfoPtr);
        asm volatile("1:\n"
                     "movl (%0), %%eax\n"
                     "cmp %%eax, %1\n"
                     "jg 2f\n"
                     "lock cmpxchgl %1, (%0)\n"
                     "jnz 1b\n"
                     "2:\n" :: "r" (&info->currentMaxEntry), "r" (slot): "rax", "memory");
    }
}

/*! Obtain a slot index for a desired library
 *  If the library is already loaded that handle will be returned.
 *  If the library is not yet loaded an attempt will be made to load it.
 *
 * \param libName Name of the library to be loaded
 * \param rpaths Additional paths provided by the caller
 * \return A positive index of the loaded library if it was located or loaded or a negative error code if it could not be loaded

 */
int linkerTableRegion::getLibrary(const String &libName, const Vector<String> &rpaths)
{
    hashedString target(libName);

    //Attempt to find if library is already instantiated in this address space
    taggedHashString *tagged = loaded.find(target);
    //If it is simply return the index
    if(tagged)
        return tagged->getTag();
    else
    {
        //Make sure there's actually entries available for the new library mapping
        if(!availableSlotCount)
            return -1;

        emInstanceStatus status;
        elfLoadedInstance *iPtr;
        //Check the general library paths first
        status = executableManager::getLibInstance(libPaths, libName.toBuffer(), iPtr);
        if(status != emSuccess)
        {
            //Check the provided additional paths and hope they serve some purpose
            if( rpaths.length() )
            {
                status = executableManager::getLibInstance(rpaths, libName.toBuffer(), iPtr);
                if(status != emSuccess)
                    return -status;
            }
            else
                return -status;

        }
        else
        {
            if(!iPtr->mapSegs(as))
            {
                kprintf("Error: Unable to map library into address space!\n");
                return -emNoMemory;
            }
            String libString(libName);
        }

        int slot = getAvailableSlot();
        if(!slot)
            return -1;

        writeSlotEntry(slot, iPtr, false);

        return slot;
    }

}

/*! Binds the information structure loaded in userspace to the linker table
 *  \param userInfoAddress Address of the user information structure
 */
void linkerTableRegion::bindUserInfo(uintptr_t userInfoAddress)
{
    userInfoPtr = userInfoAddress;
    kDynInfo *info = reinterpret_cast<kDynInfo *>(userInfoPtr);
    info->currentMaxEntry = 1;
    info->failCode = 0;
    info->kDynTable = reinterpret_cast <void *> (regBase);
    info->softLibLimit = 256;
}
