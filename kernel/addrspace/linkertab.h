#ifndef ADDR_LINKER_TABLE_H_
#define ADDR_LINKER_TABLE_H_

#include "asregion.h"
#include "exec/elf.h"

class linkerTableRegion : public asRegion
{
    public:
        linkerTableRegion();
        virtual ~linkerTableRegion();
        int getLibrary(const String &libName, const Vector<String> &rpaths);
        void writeSlotEntry(const int slot, const class elfLoadedInstance *instance, const bool hasEntry = false);
        void bindUserInfo(uintptr_t userInfoAddress);
        void freeSlot(const int slot);
        /* Read only kernel dynamic info */
        struct COMPILER_PACKED kDynInfo
        {
            /* An array of kernel dynamic information structures */
            void *kDynTable;
            /* Count and capacities information about loaded libraries */
            uint32_t currentMaxEntry, softLibLimit, hardLibLimit, failCode;
        };

    private:

        int getAvailableSlot(void);

        class taggedHashString : public hashedString
        {
            public:
                taggedHashString(const String &s, const uintptr_t initTag) : hashedString(s), tag(initTag) {}

                uintptr_t getTag(void) const
                {
                    return tag;
                }

                void setTag(const uintptr_t value)
                {
                    tag = value;
                }
            private:
                uintptr_t tag;
        };

        uintptr_t userInfoPtr;
        uintptr_t kernMapping;
        avlTree<regionObj32> available;
        avlTree<taggedHashString> loaded;
        Vector<String> libPaths;
        uint32_t availableSlotCount;
};

#endif // ADDR_LINKER_TABLE_H_
