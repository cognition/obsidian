#ifndef AS_HWREGION_H_
#define AS_HWREGION_H_

/* Hardware memory region header
 *
 * Author: Joseph Kinzel */

#include "asregion.h"

class hardwareRegion : public asRegion
{
    public:
        hardwareRegion(const uintptr_t physBase, const size_t regionLength, const physaddr_t hwPerms);
    private:

};

#endif // AS_HWREGION_H_
