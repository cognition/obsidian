#include "page_bundle.h"
#include <memory/phys.h>
#include <memory/virtual.h>

//pageBundle class

/*! Constructor
 * \param pageCount Number of pages the bundle will contain
 */
pageBundle::pageBundle(size_t pageCount, bundleEntryType t) : pages(pageCount), entryType(t)
{
}

/*! Copy constructor
 * \param other Pointer to another pageBundle structure which is to be copied.
 */
pageBundle::pageBundle(pageBundle *other) : entryType(other->entryType)
{
    spinLockInstance lock(&other->bundleLock);
    pages = other->pages;
    bundleTemplate = shared_ptr<pageBundle> (other);
}

//! Destructor
pageBundle::~pageBundle()
{
    if(!bundleTemplate.get())
    {
        //Reclaim all the pages if the bundle isn't based on a template
        for(uintptr_t pageIndex = 0; pageIndex < pages.length(); pageIndex++)
        {
            physicalMemory::reclaimPage(pages[pageIndex]);
        }
    }
    else
    {
        //If there is a template only reclaim the pages that don't match the template
        Vector<uintptr_t> &tPages = bundleTemplate->pages;
        for(uintptr_t pageIndex = 0; pageIndex < pages.length(); pageIndex++)
        {
            if(tPages[pageIndex] != pages[pageIndex])
                physicalMemory::reclaimPage(pages[pageIndex]);
        }
    }

};

/* Initializes a page bundle with zero'd memory pages
 * \param bundle Shared pointer bundle return object
 * \param pageCount Number of pages to map and initialize
 * \param kernPreserve Null if the bundle won't be double mapped, valid return pointer if it is
 */
void pageBundle::createZeroBundle( shared_ptr<pageBundle> &bundle, const size_t pageCount, uintptr_t *kernPreserve )
{
    pageBundle *obj = new pageBundle(pageCount, bundleRegularPages);

    uintptr_t vaddr = virtualMemoryManager::createMapping(obj->pages, pageCount);
    Memory::set( reinterpret_cast <void *> (vaddr), 0, pageCount*REGULAR_PAGE_SIZE);

    bundle.reset(obj);
    if(kernPreserve)
        *kernPreserve = vaddr;
    else
        virtualMemoryManager::unmapRegion(vaddr, REGULAR_PAGE_SIZE*pageCount, false, true);
}

/*! Page count accessor
 * \return Number of pages in the bundle
 */
size_t pageBundle::getPageCount(void)
{
    spinLockInstance lock(&bundleLock);
    return pages.length();
}

uintptr_t pageBundle::exchangeEntry(const uintptr_t index, const uintptr_t newEntry )
{
    spinLockInstance lock(&bundleLock);
    uintptr_t &entry = pages[index];

    uintptr_t oldEntry = entry;
    entry = newEntry;
    return oldEntry;
}

void pageBundle::addDependent(asRegion *as)
{
    dependents.insert(as);
}

/*! Allocates pages and loads segment data from an open file
 *  \param[out] bundleOut Shared pointer to the resulting page bundle object
 *  \param[in] pageCount Number of pages required for the bundle
 *  \param[in] file Shared pointer to the open file
 *  \param[in] fileOffset Offset of the segment's data in the file
 *  \param[in] memOffset Offset into memory of the loaded file data
 *  \param[in] memSize Size of the segment when loaded into memory
 *  \param[in] fileSize Size of the segment's data in the file
 *  \return bfIoError if a file error was encountered, bfSuccess if the function succeeded
 */
bufferedFileStatus pageBundle::singleFileBundle(shared_ptr<pageBundle> &bundleOut, const size_t pageCount, shared_ptr<fileHandle> file, const off_t fileOffset, const uintptr_t memOffset, const size_t memSize, const size_t fileSize)
{
    shared_ptr<pageBundle> bundle(new pageBundle(pageCount, pageBundle::bundleRegularPages));
    // Allocate the physical pages and temporarily map them into the virtual memory
    uintptr_t vinstance = virtualMemoryManager::createMapping(bundle->pages, pageCount);
    uint8_t *vptr = reinterpret_cast<uint8_t *> (vinstance);
    uintptr_t pOffset = memOffset & PAGE_OFFSET_MASK;
    if(pOffset)
        Memory::set(vptr, 0, pOffset);
    vptr = &vptr[pOffset];
    uintptr_t pNeeded = (pOffset + memSize + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE;
    if(pNeeded != pageCount)
    {
        kprintf("PB: Page allocation mismatch!\n");
        asm volatile ("xchg %%bx, %%bx\n"::);
    }

    if(fileSize)
    {
        // Read the bytes in from the file and copy them into the pages
        if( file->seekAndReadAll(seekBegin, fileOffset, vptr, fileSize) != fsValid)
        {
            virtualMemoryManager::unmapRegion(vinstance, pageCount*REGULAR_PAGE_SIZE, true, true);
            return bfIoError;
        }
    }
    size_t remSize = memSize - fileSize;

    // If there's excess space zero initialize it
    if(remSize)
        Memory::set(&vptr[fileSize], 0, remSize);

    // Unmap the page from memory, but don't free the memory behind it
    virtualMemoryManager::unmapRegion(vinstance, pageCount*REGULAR_PAGE_SIZE, false, true);
    bundle->regBase = memOffset;
    bundle->regLength = memSize;
    bundleOut = bundle;
    return bfSuccess;

}

/*! Allocates large pages and loads segment data from an open file
 *  \param[out] bundleOut Shared pointer to the resulting page bundle object
 *  \param[in] pageCount Number of pages required for the bundle
 *  \param[in] file Shared pointer to the open file
 *  \param[in] fileOffset Offset of the segment's data in the file
 *  \param[in] memOffset Offset into memory of the loaded file data
 *  \param[in] memSize Size of the segment when loaded into memory
 *  \param[in] fileSize Size of the segment's data in the file
 *  \return bfIoError if a file error was encountered, bfSuccess if the function succeeded
 */
bufferedFileStatus pageBundle::largeFileBundle(shared_ptr<pageBundle> &bundleOut, const size_t pageCount, shared_ptr<fileHandle> file, off_t fileOffset, uintptr_t memOffset, size_t memSize, size_t fileSize)
{
    shared_ptr<pageBundle> bundle(new pageBundle(pageCount, pageBundle::bundleLargePages));
    // Allocate the physical pages and temporarily map them into the virtual memory
    uintptr_t vinstance = virtualMemoryManager::createLargeMapping(bundle->pages, pageCount);
    uint8_t *vptr = reinterpret_cast<uint8_t *> (vinstance);

    if(fileSize)
    {
        if( file->seekAndReadAll(seekBegin, fileOffset, vptr, fileSize) != fsValid )
        {
            virtualMemoryManager::unmapRegion(vinstance, pageCount*LARGE_PAGE_SIZE, true, true);
            return bfIoError;
        }
    }

    size_t remSize = memSize - fileSize;

    // If there's excess space zero initialize it

    if(remSize)
        Memory::set(&vptr[fileSize], 0, remSize);
    // Unmap the page from memory, but don't free the memory behind it
    virtualMemoryManager::unmapRegion(vinstance, pageCount*LARGE_PAGE_SIZE, false, true);

    bundle->regBase = memOffset;
    bundle->regLength = memSize;
    bundleOut = bundle;
    return bfSuccess;

}

/*! Creates a group of page bundles and loads/initializes the region with data from an executable file
 *  \param[out] bundles Vector containing the bundles
 *  \param[in]  pageCount Number of regular sized pages involved
 *  \param[in]  file shared pointer to executable file
 *  \param[in]  fileOffset Offset into the file where data for the bundles begins
 *  \param[in]  memOffset Relative offset into memory where the segment/bundles will be loaded
 *  \param[in]  memSize Size of the segment in memory
 *  \param[in]  fileSize Size of the in file data for this segment
 *  \return bfSuccess if successful,bfIoError if a file related I/O error is encountered
 */
bufferedFileStatus pageBundle::createFileBundle(Vector< shared_ptr<pageBundle> > &bundles, const size_t pageCount, shared_ptr<fileHandle> file, off_t fileOffset,
                                                uintptr_t memOffset, size_t memSize, size_t fileSize)
{
    uintptr_t pre = (memOffset & LARGE_PAGE_MASK) ? LARGE_PAGE_SIZE - (memOffset & LARGE_PAGE_MASK):0;
    uintptr_t sectionOff = 0;
    size_t rfs = fileSize;
    bufferedFileStatus status;

    // Verify that a large page alignment is even possible
    if(pre < memSize)
    {
        uintptr_t mid = memSize - pre;
        sectionOff += pre;
        // Check that there's enough pages left after aligning to use a large page
        if(mid >= LARGE_PAGE_SIZE)
        {
            shared_ptr<pageBundle> bundle;

            //Allocate and load the precursor region if necessary
            if(pre)
            {
                rfs -= min(pre, fileSize);
                size_t pages = (pre + (memOffset & PAGE_OFFSET_MASK) + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE;
                status = singleFileBundle(bundle, pages, file, fileOffset, memOffset, pre, min(fileSize, pre));

                if(status != bfSuccess)
                    return status;
                bundles.insert(bundle);
            }
            //Allocate and load the large pages
            mid -= mid & LARGE_PAGE_MASK;
            uintptr_t post = memSize - (pre + mid);
            size_t lpages = mid/LARGE_PAGE_SIZE;
            status = largeFileBundle(bundle, lpages, file, fileOffset + sectionOff, 0, mid, rfs );
            if(status == bfSuccess)
                bundles.insert(bundle);
            else
            {
                bundles.clear();
                return status;
            }
            //Update the remaining file sourced bytes count
            rfs = rfs > mid ? (rfs - mid):0;

            //Allocate pages for and load/initialize any remaining regular page bytes
            if(post)
            {
                sectionOff += mid;
                size_t postPages = (post + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE;
                status = singleFileBundle(bundle, postPages, file, fileOffset + sectionOff, 0, memSize - sectionOff, rfs );
                if(status == bfSuccess)
                    bundles.insert(bundle);
                else
                {
                    bundles.clear();
                    return status;
                }
            }

            return bfSuccess;
        }
    }

    //No large pages involved, just allocate and load/initialize one continuous bundle of pages
    size_t pages = (memSize + (memOffset & PAGE_OFFSET_MASK) + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE;
    shared_ptr<pageBundle> bundle;
    status = singleFileBundle(bundle, pages, file, fileOffset, memOffset, memSize, fileSize);
    if(status == bfSuccess)
    {
        //bundle->print();
        bundles.insert(bundle);
    }

    return status;
}

/*! Maps the page bundle into the current processes address space
 *  \param vBase Virtual base address to map to
 *  \param flag Page configuration flags for the mapping
 *  \param region Corresponding address space region object
 *  \return True if the mapping was completed successfully, false otherwise
 */
bool pageBundle::mapCurrent(const uintptr_t vBase, uintptr_t flags, asRegion *region)
{
    switch(entryType)
    {
        case bundleRegularPages:
            virtualMemoryManager::implementMapping(pages, vBase, flags);
            break;
        case bundlePageDirectories:
        case bundleLargePages:
            //Test that the provided base address is actually aligned to a 2MB page boundary
            if((vBase + regBase) & LARGE_PAGE_MASK)
                return false;
            virtualMemoryManager::implementDirMap(pages, vBase, flags);
            break;
    }
    addDependent(region);
    return true;
}

/*! Clones a pageBundle object
 *  \return a new Copy of the pagebundle object with the necessary dependency linkages
 */
shared_ptr<pageBundle> pageBundle::clone(void)
{
    if(bundleTemplate.get() != nullptr)
        return bundleTemplate->clone();
    else
        return shared_ptr<pageBundle> (new pageBundle(this));
}

bool pageBundle::hasTemplate(void) const
{
    return bundleTemplate.get();
}

void pageBundle::print(void) const
{
    for(size_t i = 0; i < pages.length(); i++)
    {
        kprintf("Page[%u]: %X\n", i, pages[i]);
    }
}
