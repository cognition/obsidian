#include "elf_exec.h"
#include "elf_instance.h"

Task *elfExec::createInstance(elfLoadedInstance *&exec)
{
    /*
    if( !(flags & ELF_EXECUTABLE) )
        return nullptr;
        */
    exec = instantiate();
    pid_t pid;
    if( !Task::allocatePid(pid) )
        return nullptr;
    Task *execTask = new Task( &elfLoadedInstance::init, exec, 0, pid, true );

    return execTask;
}


bufferedFileStatus elfExec::instantiateSegments(void)
{
    for(int i = 0; i < segments->length(); i++)
    {
        elfSegment &seg = segments->at(i);
        bufferedFileStatus status = seg.createMapping(file);
        if(status != bfSuccess)
            return status;
    }

    segments->sort();
    return bfSuccess;
}

bool elfExec::mapSegments(uintptr_t vbase, asRegion *ar )
{
	for(int segIndex = 0; segIndex < segments->length(); segIndex++ )
	{
        elfSegment &seg = segments->at(segIndex);
        Vector< shared_ptr<pageBundle> > segBundles = seg.getMapping();
        for(int bundleIndex = 0; bundleIndex < segBundles.length(); bundleIndex++)
        {
            shared_ptr<pageBundle> &bundle = segBundles[bundleIndex];
            bundle->mapCurrent(vbase, seg.getFlags(), ar);
        }
	}
	return true;
}

bool elfExec::parseSegments(const uint64_t baseFlags)
{
    uint64_t offset = header->phOff;
    const uint64_t step = header->phEntrySize;
    segments.init(new Vector<elfSegment>() );
    for(uint64_t entryIndex = 0; entryIndex < header->phEntryCount; entryIndex++, offset += step)
    {
        elf64ProgramHeader phEntry;
        uint64_t memFlags = PAGE_PRESENT | PAGE_USER | baseFlags;

        if( file->seekAndReadAll(seekBegin, offset, &phEntry, sizeof(elf64ProgramHeader)) != fsValid )
            return false;

        switch(phEntry.type)
        {
            case PT_LOAD:
                if( !(phEntry.flags & PF_X) )
                    memFlags |= PAGE_NX;
                if( phEntry.flags & PF_W )
                    memFlags |= PAGE_COW;
                length = max(length, (phEntry.virtAddress + phEntry.memorySize + PAGE_OFFSET_MASK) & ~PAGE_OFFSET_MASK);
                base = min(base, phEntry.virtAddress);

                if( file->validateOffset(seekBegin, phEntry.offset + phEntry.fileSize) != fsValid )
                {
                    return false;
                }

                segments->insert( elfSegment(phEntry.offset, phEntry.virtAddress, phEntry.fileSize, phEntry.memorySize, phEntry.alignment, memFlags ) );

                break;
            case PT_DYNAMIC:
                if( file->validateOffset(seekBegin, phEntry.offset + phEntry.fileSize) != fsValid )
                {

                    return false;
                }

                dyn.reset( new elfSegment( phEntry.offset, phEntry.virtAddress, phEntry.fileSize, phEntry.memorySize, phEntry.alignment, memFlags));
                break;
            default:
                break;

        }

    }

    for(int seg = 0; seg < segments->length(); seg++)
    {
        elfSegment &first = segments->at(seg);
        for(int secondSeg = seg+1; secondSeg < segments->length(); secondSeg++)
        {
            elfSegment &second = segments->at(secondSeg);
            if(first == second)
            {
                segments->clear();
                kprintf("Segment overlap detected between segments %u and %u! First: %X-%X Second: %X-%X\n", seg, secondSeg,
                        first.getBase(), first.getBase()+first.getLength() - 1,
                        second.getBase(), second.getBase() + second.getLength() - 1);
                first.print();
                second.print();
                return false;
            }
        }
    }

    length -= base;
    return true;
}

elfLoadedInstance *elfExec::instantiate(void)
{
    return new elfLoadedInstance(this);
};

const String &elfExec::toString(void) const
{
    return str;
}
