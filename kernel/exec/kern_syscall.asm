[BITS 64]
[DEFAULT REL]

GLOBAL syscall64_entry, syscall32_entry, sysenter64_entry, sysenter32_entry
EXTERN syscall64_retrieve_user_rcx, syscall32_retrieve_user_ecx
EXTERN sysenter64_retrieve_user_regs, sysenter32_retrieve_user_regs
EXTERN system_call_handler

syscall64_entry:
    swapgs
    ; Save user mode stack
	mov [gs:72], rsp
	; Setup kernel mode stack
	mov rsp, [gs:64]

	sub rsp, 112

	mov [rsp+104], rbp
	mov [rsp+96], rbx
	mov [rsp+88], r15
	mov [rsp+80], r14
	mov [rsp+72], r13
	mov [rsp+64], r12
	; Save R10, we should have a 16 byte alignment on the stack anyways
	mov [rsp+56], r10
	; Save registers used by the syscall/sysret instructions
	mov [rsp+48], r11
	mov [rsp+40], rcx

	; Save registers which should be caller saved
	mov [rsp+32], rdx
	mov [rsp+24], rsi
	mov [rsp+16], rdi
	mov [rsp+8], r8
	mov [rsp], r9

	mov rdi, [gs:72]
	call syscall64_retrieve_user_rcx
	mov rcx, rax
	mov rdi, [rsp+16]
	mov rsi, [rsp+24]
	mov rdx, [rsp+32]
	mov r8, [rsp+8]
	mov r9, [rsp]

	; Actual kernel logic for decoding and processing calls
	call system_call_handler

	; Restore registers which should be caller saved
	mov r9, [rsp]
	mov r8, [rsp+8]
	mov rdi, [rsp+16]
	mov rsi, [rsp+24]
	mov rdx, [rsp+32]

    mov rbp, [rsp+104]
	mov rbx, [rsp+96]
	mov r15, [rsp+88]
	mov r14, [rsp+80]
	mov r13, [rsp+72]
	mov r12, [rsp+64]

    ; Restore R10
	mov r10, [rsp+56]

	; Restore registers used by the syscall/sysret instructions
	mov rcx, [rsp+40]
	mov r11, [rsp+48]


	; Return to user space
	mov rsp, [gs:72]
	swapgs
    o64 sysret

syscall32_entry:
	swapgs
	; Save user mode stack
	mov [gs:72], rsp
	; Setup kernel mode stack
	mov rsp, [gs:64]

	sub rsp, 32
	; Save registers used by the syscall/sysret instructions
	mov r11, [rsp+24]
	mov rcx, [rsp+16]

	; Save general purpose registers
	mov ebx, [rsp+12]
	mov edx, [rsp+8]
	mov esi, [rsp+4]
	mov edi, [rsp]

	; Attempt to get the ECX register from the user space stack
	mov rdi, [gs:72]
	call syscall32_retrieve_user_ecx
	mov ecx, eax
	mov edi, [rsp]

	; Actual kernel logic for decoding and processing calls
	call system_call_handler

	; Restore general purpose registers
	mov edi, [rsp]
	mov esi, [rsp+4]
	mov edx, [rsp+8]
	mov ebx, [rsp+12]

	; Restore registers used by the syscall/sysret instructions
	mov rcx, [rsp+16]
	mov r11, [rsp+24]

	mov rsp, [gs:72]
	swapgs
	o32 sysret

sysenter64_entry:
	swapgs

	sub rsp, 48
	; Save the registers used by the SYSENTER and SYSEXIT instructions
	mov rcx, [rsp+40]
	mov rdx, [rsp+32]

	mov rdi, [rsp+24]
	mov rsi, [rsp+16]
	mov r8, [rsp+8]
	mov r9, [rsp]

	; Retrieve the user space values of RCX and RDX
	mov rdi, rcx
	call sysenter64_retrieve_user_regs
	mov rcx, rax
	mov rdi, [rsp+24]

	; Actual kernel logic for decoding and processing calls
	call system_call_handler

	mov r9, [rsp]
	mov r8, [rsp+8]
	mov rsi, [rsp+16]
	mov rdi, [rsp+24]

	; Restore the registers used by the SYSENTER and SYSEXIT instructions
	mov rdx, [rsp+32]
	mov rcx, [rsp+40]

	swapgs
	sti
	sysexit

sysenter32:
	swapgs

	swapgs
	sti
	o32 sysexit
