#ifndef EXEC_USER_SANITY_CHECKS_H_
#define EXEC_USER_SANITY_CHECKS_H_

#include "compiler.h"

//! Structure used to perform a a 2 register return, where RAX = .rcx and RDX = .rdx
struct COMPILER_PACKED sysenter_regs
{
	//! User space RCX value
	uint64_t rcx;
	//! User space RDX value
	uint64_t rdx;
};

extern "C"
{
	//Prototypes
	uintptr_t syscall64_retrieve_user_rcx(uint64_t *userStack);
	uint32_t syscall32_retrieve_user_ecx(uint32_t *userStack);
    sysenter_regs sysenter64_retrieve_user_regs(uint64_t *userStack);
    sysenter_regs sysenter32_retrieve_user_regs(uint32_t *userStack);
    bool getStackArgs(uint32_t argCount, ...);
}

#endif
