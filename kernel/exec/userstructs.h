#ifndef OBSIDIAN_USER_STRUCTURES_H_
#define OBSIDIAN_USER_STRUCTURES_H_

#include <compiler.h>

struct obs_inode
{
    uint64_t fileSize;
    uint32_t parentDevHandle;
    uint32_t ioSize;
    uid_t userId;
    gid_t groupId;
    uint32_t linkCount;
    uint32_t deviceId;
    uint64_t blockCount;
    time_t imodified, created, modified, accessed;
};

typedef struct obs_inode inode_t;

struct dirent
{
    uint64_t fsn;
    char name[255];
};

#endif // OBSIDIAN_USER_STRUCTURES_H_
