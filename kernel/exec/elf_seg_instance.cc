#include "elf_seg_instance.h"
#include <memory/virtual.h>

elfSegInstance::elfSegInstance(const elfSegment &src, const uintptr_t base) : asRegion(src.getBase() - base, src.getLength(), asRegion::execRegion, src.getFlags() )
{
    bundles = src.getMapping();
    bundleFlags.insert(permissions);
    flags = regionHasBundle | regionGroupMapped;
    if(permissions & PAGE_COW)
        flags |= regionCOW;
}
