#ifndef ELF_MODULE_H_
#define ELF_MODULE_H_

#include "elf.h"
#include "elf_dyn_table.h"
#include <driverapi/module_allocator.h>

enum class modStatus
{
    modEntry,       //Module is ready for setup/entry
    modAllocated,   //Module's memory has been allocated
    modLoaded,      //Module has been loaded into memory
    modNotFound,    //The module was not found
    modCorrupted,   //Some form of executable or file corruption was encountered when loading the module
    modTooBig,      //The module exceeded the maximum permissible size of the kernel
    modNoMemory     //The module failed retrieve a valid block of memory from the allocator
};

const char *modStatusToString(const modStatus status);

class elfModule : public elfFile
{
    public:

        elfModule() = default;
        elfModule(const String modfile);
        modStatus allocateModule(tierModuleAllocator &alloc);
        modStatus loadModule(tierModuleAllocator &alloc);
	protected:
        modStatus processRelocations();
        bool validateDynTable(const elf64ProgramHeader *hdr);
        modStatus finalizeMemoryPermissions(void);

	    struct modSectionInfo
	    {
	        uint32_t offset, sectionNum;
	    };

        uintptr_t baseAddress, tableAddress, moduleMaxSpan;
        size_t pagesRequired;
        uint32_t symTab, stringTab;

        static uint32_t moduleMustHaveFlags, moduleCannotHaveFlags;

        shared_ptr<elfDynamicTable> edt;
};


#endif // ELF_MODULE_H_
