#include "libcache.h"
#include <io/io.h>

static const Vector<String> *findMatches(const String &soname);

class libFiles : public hashedString
{
    public:
        libFiles() : hashedString("") {};
        libFiles(const String &soname) : hashedString(soname), filePaths(){};
        ~libFiles(){};

        libFiles &operator =(const libFiles &other)
        {
            filePaths = other.filePaths;
            str = other.str;
            hash = other.hash;
            return *this;
        }

        void add(const String &path)
        {
            filePaths.insert(path);
        }

        const Vector<String> *getPaths(void) const
        {
            return &filePaths;
        }
    private:
        Vector<String> filePaths;
};

static shared_ptr< chainedHashTable<libFiles, hashedString::hasher>> table;

static const Vector<String> *findMatches(const String &soname)
{
    libFiles *ret;
    if(!table->find(soname, ret))
        return nullptr;
    else
        return ret->getPaths();
}


namespace libCache
{
    void init(void)
    {
        table.reset( new chainedHashTable<libFiles, hashedString::hasher>());
    }

    void addEntry(const String &soname, const String &path)
    {
        libFiles files(soname);
        libFiles *soFiles = table->findOrInsert(files);
        soFiles->add(path);
    }

    const String decodeSoName(const String &soname)
    {
        const Vector<String> *matches = findMatches(soname);
        if(!matches)
            return String("");
        else
            return matches->at(0);
    }

};
