#ifndef ELF_DYN_TABLE_H_
#define ELF_DYN_TABLE_H_

#include <compiler.h>
#include "elf.h"

class elfDynamicTable
{
    public:
        struct COMPILER_PACKED elf64DynEntry
		{
		    uint64_t type, value;
		};


        elfDynamicTable(const uintptr_t addr, const size_t length);

        void parse(const uintptr_t loadBase);


        const char *getString(const uintptr_t index);
        const elfFile::Elf64Sym *getSym(const uintptr_t index);
        const elf64Reloca *getReloc(const uintptr_t index);
        const elf64DynEntry *getEntry(const int index);
        uint64_t getRelaCount(void) const;
        size_t getStringTabSize(void) const;

        //Dynamic table key values
        static constexpr const int32_t DT_NULL         = 0;
        static constexpr const int32_t DT_NEEDED       = 1;
        static constexpr const int32_t DT_PLTRELSZ     = 2;
        static constexpr const int32_t DT_PLTGOT       = 3;
        static constexpr const int32_t DT_HASH         = 4;
        static constexpr const int32_t DT_STRTAB       = 5;
        static constexpr const int32_t DT_SYMTAB       = 6;
        static constexpr const int32_t DT_RELA         = 7;
        static constexpr const int32_t DT_RELASZ       = 8;
        static constexpr const int32_t DT_RELAENT      = 9;
        static constexpr const int32_t DT_STRSZ        = 10;
        static constexpr const int32_t DT_SYMENT       = 11;
        static constexpr const int32_t DT_INIT         = 12;
        static constexpr const int32_t DT_FINI         = 13;
        static constexpr const int32_t DT_SONAME       = 14;
        static constexpr const int32_t DT_RPATH        = 15;
        static constexpr const int32_t DT_SYMBOLIC     = 16;
        static constexpr const int32_t DT_REL          = 17;
        static constexpr const int32_t DT_RELSZ        = 18;
        static constexpr const int32_t DT_RELENT       = 19;
        static constexpr const int32_t DT_PLTREL       = 20;
        static constexpr const int32_t DT_DEBUG        = 21;
        static constexpr const int32_t DT_TEXTREL      = 22;
        static constexpr const int32_t DT_JMPREL       = 23;
        static constexpr const int32_t DT_BIND_NOW     = 24;
        static constexpr const int32_t DT_INIT_ARRAY   = 25;
        static constexpr const int32_t DT_FINI_ARRAY   = 26;
        static constexpr const int32_t DT_INIT_ARRAYSZ = 27;
        static constexpr const int32_t DT_FINI_ARRAYSZ = 28;

        static constexpr const int32_t DT_FLAGS        = 30;

        static constexpr const uint32_t DTF_ORIGIN     = 0x0001U;
        static constexpr const uint32_t DTF_SYMBOLIC   = 0x0002U;
        static constexpr const uint32_t DTF_TEXTREL    = 0x0004U;
        static constexpr const uint32_t DTF_BIND_NOW   = 0x0008U;
        static constexpr const uint32_t DTF_STATIC_TLS = 0x0010U;

    private:
        uintptr_t dynBase, symBase, relaBase;
        const char *strBase;
        size_t dynSegSize, strSize, relaSize;
        uint32_t symEntSize, relaEntSize;
};

#endif // ELF_DYN_TABLE_H_
