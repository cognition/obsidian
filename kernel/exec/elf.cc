#include "elf.h"
#include <cpu/localstate.h>
#include <fs/vfs.h>
#include <memory/memory.h>
#include <memory/virtual.h>
#include <addrspace/linkertab.h>

//elfFile class
elfFile::elfFile(String fileName) : hashedString(fileName), length(0), flags(0), instanceCount(0), symTabSize(0)
{
}

elfFile::elfFile() : hashedString(String("")), length(0), flags(0), instanceCount(0), symTabSize(0) {};

elfFile::~elfFile()
{
}

bool elfFile::open(void)
{
    fileHandle *execFile = Vfs::open( str, FILE_READ | FILE_EXEC );
	file.reset(execFile);
	return (execFile != nullptr);
}

bool elfFile::parse(const uint16_t objType, physaddr_t baseFlags)
{
    elf64Header *fileHeader = new elf64Header;
    header.reset(fileHeader);
    flags |= baseFlags;
    kprintf("Parsing ELF file: %s\n", str.getBuffer() );
	// Read the file
	if( file->seekAndReadAll( seekBegin, 0, fileHeader, sizeof(elf64Header) ) != fsValid )
    {
        kprintf("Failed to read file header!\n");
		return false;
    }

	// Verify the magic bytes in the header
	if( (header->ident[EI_MAG0] != 0x7F) || (header->ident[EI_MAG1] != 'E')
		|| (header->ident[EI_MAG2] != 'L') || (header->ident[EI_MAG3] != 'F') )
		{
		    kprintf("Failed to validate ELF file header!\n");
			return false;
		}

	// Verify that the file is a valid ELF file
	if ( header->ident[EI_OSABI] != ELFOSABI_SYSV )
    {
        kprintf("ELF file ABI test failed!\n");
		return false;
    }
	// x86_64 Architecture
	if ( header->machine != EM_X86_64 )
    {
        kprintf("ELF File architecture invalid!\n");
		return false;
    }
	// LSB data encoding
	if ( header->ident[EI_DATA] != ELFDATA2LSB )
    {
        kprintf("ELF File endianess mismatch!\n");
		return false;
    }

	// 64-bit file
	if ( header->ident[EI_CLASS] != ELFCLASS64 )
		return false;

	// Must be an executable or dynamic library
	if(header->type != objType)
    {
        if(objType == ET_EXEC && header->type == ET_DYN)
        {
            /* TODO: Load and verify dynamic table for PIE flags */
        }
        else
        {
            kprintf("ELF - Parse - Expected: %u Found: %u\n", objType, header->type);
            return false;
        }
    }

    kprintf("File parse complete!\n");
    return true;

}

bool elfFile::loadSection(uintptr_t start, const Elf64_SectionHeader *sh )
{
    uint8_t *ptr = reinterpret_cast<uint8_t *> (start);

    if( file->seekAndReadAll(seekBegin, sh->sh_offset, ptr, sh->sh_size) != fsValid)
        return false;
    else
        return true;
}

bool elfFile::getSymbolTable(void)
{
    off_t offset = header->shOff;
    off_t shStep = header->shEntrySize;
    for(int sectionEntry = 0; sectionEntry < header->shEntryCount; sectionEntry++, offset += shStep)
    {
        Elf64_SectionHeader sh;

        if(file->seekAndReadAll(seekBegin, offset, &sh, sizeof(Elf64_SectionHeader) ) != fsValid)
            return false;

        if(sh.sh_type == SHT_SYMTAB)
        {
            symTabSize = sh.sh_size;
            return true;
        }

    }
    return false;
}

const String &elfFile::toString(void) const
{
	return str;
}
