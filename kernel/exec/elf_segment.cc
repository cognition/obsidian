#include "elf_segment.h"
#include <memory/virtual.h>

//elfSegment class
elfSegment::elfSegment(const uintptr_t offset, const uintptr_t mOffset, const uint64_t segSize, const uint64_t mSize, const uint64_t alignment, const uint64_t segmentFlags) :
    regionObj64(mOffset, mSize), virtOffset(mOffset), fileOffset(offset), fileSize(segSize), memSize(mSize), flags(segmentFlags), align(alignment)
{
}

void elfSegment::print(void) const
{
    kprintf("Elf segment @ %X mSize %u fSize: %u fileOffset: %u\n", virtOffset, memSize, fileSize, fileOffset);
}

physaddr_t elfSegment::getFlags(void) const
{
	return flags;
}

uint64_t elfSegment::getAlign(void) const
{
    return align;
}

const Vector< shared_ptr<pageBundle> > &elfSegment::getMapping(void) const
{
	return pages;
}

bufferedFileStatus elfSegment::createMapping(shared_ptr<fileHandle> &file)
{
    const size_t pageCount = ((virtOffset & PAGE_OFFSET_MASK) + memSize + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE;
    return pageBundle::createFileBundle(pages, pageCount, file, fileOffset, virtOffset, memSize, fileSize);
}
