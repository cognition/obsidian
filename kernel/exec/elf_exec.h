#ifndef ELF_EXEC_H_
#define ELF_EXEC_H_

#include "elf.h"

class elfExec : public elfFile
{
    public:
        elfExec(const String name);
        ~elfExec();

        Task *createInstance(class elfLoadedInstance *&exec);
		const String &toString(void) const;
        bool mapSegments(uintptr_t vbase, asRegion *ar );
		static void loadInstance(void *execPointer);
        bufferedFileStatus instantiateSegments(void);
	    class elfLoadedInstance *instantiate(void);
        bool parseSegments(const uint64_t baseFlags);

        bool dependentDecrement(void);

        void dependentIncrement(void)
        {
            instanceCount++;
        }


        const shared_ptr< Vector<elfSegment> > getSegments() const
        {
            return segments;
        }

        shared_ptr<elfSegment> getDyn(void)
        {
            return dyn;
        }

        uintptr_t getBase(void) const
        {
            return base;
        }

    protected:
        shared_ptr< Vector<elfSegment> > segments;
		shared_ptr<elfSegment> dyn;
        uintptr_t base;
};


#endif // ELF_EXEC_H_
