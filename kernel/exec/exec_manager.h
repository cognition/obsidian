#ifndef EXEC_MANAGER_H_
#define EXEC_MANAGER_H_

#include "elf_exec.h"
#include "elf_instance.h"
#include <status.h>

namespace executableManager
{
    emInstanceStatus getLibInstance( const Vector<String> &paths, const char *libName, elfLoadedInstance *&instancePtr);
    emInstanceStatus getExecInstance(const char *fullPath, shared_ptr<class elfLoadedInstance> &instancePtr, Task *&instanceTask);
};

#endif // EXEC_MANAGER_H_
