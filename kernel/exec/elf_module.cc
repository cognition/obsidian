#include "elf_module.h"
#include <addrspace/kern_map.h>

extern bool getApiSymbolAddress(const char *string, const uint32_t maxLength, uintptr_t &value);

elfModule::elfModule(const String modfile) : elfFile(modfile), baseAddress(0), tableAddress(0), moduleMaxSpan(0), symTab(0), stringTab(0)
{
}

#define MODULE_MAX_SIZE      0x4000000UL
#define PT_MODINFO           0x6FFFFEEDU
#define BITFIELD(x)          (1U<<(x))

uint32_t elfModule::moduleMustHaveFlags = BITFIELD(elfDynamicTable::DT_HASH) | BITFIELD(elfDynamicTable::DT_RELA) | BITFIELD(elfDynamicTable::DT_RELASZ) |
BITFIELD(elfDynamicTable::DT_RELAENT) | BITFIELD(elfDynamicTable::DT_BIND_NOW) | BITFIELD(elfDynamicTable::DT_SYMTAB) | BITFIELD(elfDynamicTable::DT_SYMENT);

uint32_t elfModule::moduleCannotHaveFlags = BITFIELD(elfDynamicTable::DT_PLTREL) | BITFIELD(elfDynamicTable::DT_JMPREL) | BITFIELD(elfDynamicTable::DT_PLTGOT) |
 BITFIELD(elfDynamicTable::DT_PLTRELSZ) | BITFIELD(elfDynamicTable::DT_INIT) | BITFIELD(elfDynamicTable::DT_FINI) | BITFIELD(elfDynamicTable::DT_INIT_ARRAY) |
 BITFIELD(elfDynamicTable::DT_INIT_ARRAYSZ) | BITFIELD(elfDynamicTable::DT_FINI_ARRAY) | BITFIELD(elfDynamicTable::DT_FINI_ARRAYSZ);

const char *modStatusToString(const modStatus status)
{
    switch(status)
    {
        case modStatus::modAllocated:
            return "\"module allocated\"";
            break;
        case modStatus::modCorrupted:
            return "\"module corrupted\"";
            break;
        case modStatus::modEntry:
            return "\"module entry ready\"";
            break;
        case modStatus::modLoaded:
            return "\"module loaded\"";
            break;
        case modStatus::modNoMemory:
            return "\"no memory for module\"";
            break;
        case modStatus::modNotFound:
            return "\"unable to locate module\"";
            break;
        case modStatus::modTooBig:
            return "\"module exceeds maximum memory size\"";
            break;
    }
}

/*! Processes all the relocations present in the module
 * \return True if all relocations could be successfully processed, false otherwise
 */
modStatus elfModule::processRelocations()
{
    for(uint64_t relaIndex = 0; relaIndex < edt->getRelaCount(); relaIndex++)
    {
            const elf64Reloca *reloc = edt->getReloc(relaIndex);
            if(!reloc)
                return modStatus::modCorrupted;
            //Extract symbol index and type fields
            uint32_t symIndex = ELF64_R_SYM(reloc->info);
            uint32_t type = ELF64_R_TYPE(reloc->info);

            //Get the symbol information from the module's tables
            const Elf64Sym *sym = edt->getSym(symIndex);

            const char *name = sym->name ? edt->getString(sym->name):nullptr;

            union relocField
            {
                uint16_t v16;
                uint32_t v32;
                uint64_t v64;
            };

            //Validate the relocation and setup the field to modify it
            if(reloc->offset >= moduleMaxSpan)
            {
                //Invalid relocation offset, points outside module
                return modStatus::modCorrupted;
            }
            relocField *field = reinterpret_cast <relocField *> (baseAddress + reloc->offset);


            //Look for an API symbol if the symbol is in the undefined section and a name exists
            uint64_t value = 0;
            if(!sym->tableIndex && name)
            {
                if(!getApiSymbolAddress(name, edt->getStringTabSize() - sym->name, value))
                    return modStatus::modCorrupted;
            }
            else
                value = baseAddress;

            //Resolve the relocation as defined in the System V ABI
            switch(type)
            {
                case R_X86_64_GLOB_DAT:
                    field->v64 = value;
                    break;
                case R_X86_64_RELATIVE:
                    field->v64 = baseAddress + reloc->addend;
                    break;
                default:
                    //Unknown/invalid relocation type
                    return modStatus::modCorrupted;
                    break;
        }
    }

    return finalizeMemoryPermissions();
}

/*! Finalizes the memory permissions of the segments
 *  \return True if the permissions change was successful, false otherwise
 */
modStatus elfModule::finalizeMemoryPermissions(void)
{
    elf64ProgramHeader phEntry;

    off_t offset = header->phOff;
    off_t phStep = header->phEntrySize;

    for(int remainingEntries = header->phEntryCount; remainingEntries > 0; remainingEntries--, offset += phStep)
    {
        if( file->seekAndReadAll(seekBegin, offset, &phEntry, sizeof(elf64ProgramHeader) ) != fsValid)
            return modStatus::modCorrupted;

        //Loadable segments are the only ones worth worrying about by definition
        if(phEntry.type == PT_LOAD)
        {
            uintptr_t segmentBase;
            size_t spanPages;
            uint64_t segmentPerms = 0;

            //Determine the appropriate permissions based upon the segment's flags
            if(phEntry.flags & PF_R)
                segmentPerms |= PAGE_PRESENT;
            else
                return modStatus::modCorrupted;
            if(phEntry.flags & PF_W)
                segmentPerms |= PAGE_WRITE;
            if( !(phEntry.flags & PF_X))
                segmentPerms |= PAGE_NX;

            //Calculate the page-aligned base and span
            segmentBase = baseAddress + phEntry.virtAddress & ~PAGE_OFFSET_MASK;
            spanPages = (phEntry.virtAddress & PAGE_OFFSET_MASK) + phEntry.memorySize + PAGE_OFFSET_MASK;
            spanPages /= REGULAR_PAGE_SIZE;

            virtualMemoryManager::changePermissions(segmentBase, spanPages, segmentPerms);
        }

    }

    return modStatus::modLoaded;
}

/*! Sanity checks the module's segments and allocates memory for it
 *  \param alloc Module allocator for the current pass
 *  \return True if the module is valid, false otherwise
 */
modStatus elfModule::allocateModule(tierModuleAllocator &alloc)
{
    elf64ProgramHeader phEntry;

    off_t offset = header->phOff;
    off_t phStep = header->phEntrySize;

    int dynTableCount = 0;

    for(int remainingEntries = header->phEntryCount; remainingEntries > 0; remainingEntries--, offset += phStep)
    {
        if( file->seekAndReadAll(seekBegin, offset, &phEntry, sizeof(phEntry) ) != fsValid)
        {
            //Fail - Module's program headers are corrupted
            kprintf("Error! Moddule program headers corrupted!\n");
            return modStatus::modCorrupted;
        }

        if(phEntry.type == PT_LOAD)
        {
            /* Loadable segment, make sure it's sane.
               - All loadable segments must be readable
               - All writable segments must not be readable
            */
            if ( !(phEntry.flags & PF_R) )
                return modStatus::modCorrupted;
            if( (phEntry.flags & (PF_W | PF_X)) == (PF_W | PF_X))
                return modStatus::modCorrupted;

            uintptr_t programHeaderEnd = phEntry.memorySize + phEntry.virtAddress;
            if(programHeaderEnd > moduleMaxSpan)
                moduleMaxSpan = programHeaderEnd;
        }
        else if(phEntry.type == PT_DYNAMIC)
        {
            if(dynTableCount)
            {
                kprintf("Error! Multiple dynamic tables found in module file!\n");
                //There should only be a single dynamic table present
                return modStatus::modCorrupted;
            }
            else
            {
                dynTableCount++;
                if( !validateDynTable(&phEntry)  )
                {
                    kprintf("Error! Failed to validate module dynamic table!\n");
                    return modStatus::modCorrupted;
                }
            }
        }
        else if(phEntry.type == PT_MODINFO)
            tableAddress = phEntry.virtAddress;
    }

    if(!tableAddress)
    {
        //The module information table must be present
        kprintf("Error! Failed to find module information table!\n");
        return modStatus::modCorrupted;
    }

    // Module memory size sanity check
    if(moduleMaxSpan > MODULE_MAX_SIZE)
        return modStatus::modTooBig;

    uintptr_t pagesNeeded = (moduleMaxSpan + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE;
    baseAddress = alloc.reqPages(pagesNeeded);

    return modStatus::modAllocated;
}

/*! Validates the dynamic table of the module
 *  \param hdr Program header entry of type PT_DYNAMIC
 *  \return True if the table is valid, false otherwise
 */
bool elfModule::validateDynTable(const elf64ProgramHeader *hdr)
{
    off_t dynTableOffset, newPos;
    ssize_t bytesRead;
    elfDynamicTable::elf64DynEntry dynEntry;
    uint32_t flags = 0;

    if(file->seek(seekBegin, hdr->offset, &newPos) != fsValid)
        return false;
    for(dynTableOffset = 0; dynTableOffset < hdr->fileSize; dynTableOffset += sizeof(elfDynamicTable::elf64DynEntry) )
    {
        //Iterate through the entries until we find a null entry or hit the end of the segment
        if(file->read(&dynEntry, sizeof(dynEntry), bytesRead) != fsValid)
            return false;
        if(dynEntry.type != elfDynamicTable::DT_NULL)
        {
            if(dynEntry.type == elfDynamicTable::DT_FLAGS)
            {
                if(dynEntry.value & elfDynamicTable::DTF_BIND_NOW)
                    flags |= 1U<<elfDynamicTable::DT_BIND_NOW;
                if(dynEntry.value & elfDynamicTable::DTF_SYMBOLIC)
                    flags |= 1U<<elfDynamicTable::DT_SYMBOLIC;
            }
            else
            {
                //Select standardized non-OS specific entries to make the presence bitmask
                if(dynEntry.type <= elfDynamicTable::DT_FINI_ARRAYSZ)
                {
                    uint32_t mask = 1;
                    mask <<= dynEntry.type;
                    flags |= mask;
                }
            }
        }
        else
            break;
    }

    uint32_t modCannot = (moduleCannotHaveFlags & flags);
    uint32_t modMust = (moduleMustHaveFlags ^ flags) & moduleMustHaveFlags;
    //Make sure all required flags are present and all non-permissible flags are absent
    if(modCannot | modMust)
    {
        kprintf("Mismatch of module's dynamic flags! Cannot: %X Must: %X\n", modCannot, modMust);
        return false;
    }
    else
        return true;
}

/*! Loads the module into memory, processes relocations and finalizes the memory flags
 *  \param allocator Allocator for the current module tier
 *  \return A modStatus value, with modLoaded indicating success and other values indicating failure
 */
modStatus elfModule::loadModule(tierModuleAllocator &allocator)
{
    int phStep = header->phEntrySize;
    int offset = header->phOff;
    elf64ProgramHeader phEntry;
    qCastPtr loadPtr;

    //Attempt to add tier-level base address to module offset for a full base address
    if(uintptr_t aBase = allocator.getBaseAddress() )
        baseAddress += aBase;
    else
        return modStatus::modNoMemory;

    //Iterate through the program headers and load all sections with PT_LOAD type (segments with an in memory presence), also initialize the in memory dynamic table
    for(int phEntryIndex = 0; phEntryIndex < header->phEntryCount; phEntryIndex++, offset += phStep)
    {
        if(file->seekAndReadAll(seekBegin, offset, &phEntry, sizeof(elf64ProgramHeader)) != fsValid)
        {
            kprintf("[MOD ]: Failed to program header entry while loading...\n");
            return modStatus::modCorrupted;
        }

        switch(phEntry.type)
        {
            case PT_LOAD:
                loadPtr.addr = baseAddress + phEntry.virtAddress;
                if(file->seekAndReadAll(seekBegin, phEntry.offset, loadPtr.ptr, phEntry.fileSize) != fsValid)
                {
                    kprintf("[MOD ]: Failed to read segment of length %u @ offset: %u\n", phEntry.fileSize, phEntry.offset );
                    return modStatus::modCorrupted;
                }
                if(int memDiff = (phEntry.memorySize - phEntry.fileSize) )
                    Memory::set(&loadPtr.u8[phEntry.fileSize], 0, memDiff);
                break;
            case PT_DYNAMIC:
                edt.reset( new elfDynamicTable(phEntry.virtAddress + baseAddress, phEntry.memorySize) );
                break;
            case PT_NULL:
                break;
            default:
                break;
        }

    }

    //Similarly update the module info table address
    tableAddress += baseAddress;
    //Make sure a dynamic table is present, a simple smell test for something potentially malicious
    if(!edt.get() )
        return modStatus::modCorrupted;

    edt->parse(baseAddress);

    kernelSpace::registerRegion( regionObj64(baseAddress, moduleMaxSpan), toString() );

    return processRelocations();
}
