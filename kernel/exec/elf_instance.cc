#include "elf_instance.h"
#include "exec_manager.h"
#include <cpu/localstate.h>

//elfLoadedInstance class
elfLoadedInstance::elfLoadedInstance(elfExec *initFile) :
    entryPoint(initFile->getEntry()), symTabSize(initFile->getSymTabSize()), dyn(initFile->getDyn() ), baseAlign(LARGE_PAGE_BITS), baseAddress(0), file(initFile)
{
    //TODO: Add dynamic section test and copy initialization
    uintptr_t currentBase = 0;
    currentBase--;
    shared_ptr<Vector<elfSegment> > segs = initFile->getSegments();
    for(int segIndex = 0; segIndex < segs->length(); segIndex++)
    {
        elfSegment &currentSegment = segs->at(segIndex);
        elfSegInstance *si = new elfSegInstance(currentSegment, initFile->getBase() );
        if( si->getBase() < currentBase)
        {
            currentBase = si->getBase();
        }
        segInstances.insert(si);
    }
}

elfLoadedInstance::~elfLoadedInstance()
{
    if(!file->dependentDecrement())
        delete file;
}

uintptr_t elfLoadedInstance::mapSegs(addressSpace *as)
{
    baseAddress = as->findAvailableSpan(file->getLength(), asRegion::execRegion, baseAlign);
    if(!baseAddress)
    {
        kprintf("[ELF ]: Failed to find memory for executable or library: %s\n", file->toString().toBuffer() );
        return 0;
    }

    for(int segIndex = 0; segIndex < segInstances.length(); segIndex++)
    {
        elfSegInstance *si = segInstances[segIndex];
        si->augmentBase(baseAddress);
        if(!si->mapAddressSpace(as, 0 ))
        {
            kprintf("[ELF ]: Failed to map segment %u for executable or library: %s\n", segIndex, file->toString().toBuffer() );
            return 0;
        }
    }
    return baseAddress;
}

uintptr_t elfLoadedInstance::getAlign(void) const
{
    return baseAlign;
}


/* In process entry point for launching user space applications
 * \param data Pointer to the programs elfLoadedInstance class object
 */
void elfLoadedInstance::init(void *data)
{
    uintptr_t userStack, dlStack;
    savedRegs regs, *regsPtr;
    uintptr_t argCountPtr = 0;
    uintptr_t argsPtr = 0;
    uintptr_t execBase, dynBase;

    regsPtr = &regs;

    //Get the current task and its pid
    elfLoadedInstance *instance = reinterpret_cast <elfLoadedInstance *> (data);
    Task *current = localState::getSched()->getCurrentTask();
    instance->pid = current->getPid();


    //Get the address space and map the loaded file data into it along with the user stack
    shared_ptr<addressSpace> as = current->getAddrSpace();
    if( !(execBase = instance->mapSegs(as.get())))
        return;
    //Map allocate and map stacks
    userStack = as->mapUserStack();
    dlStack = as->mapUserStack();

    Vector<String> libPaths;
    libPaths.insert(String("/initrd/lib"));

    //Load and map the dynamic linker
    elfLoadedInstance *dynInstance;
    emInstanceStatus dynStatus = executableManager::getLibInstance(libPaths, "ld.so", dynInstance);
    if(dynStatus != emSuccess)
        return;


    if( !(dynBase = dynInstance->mapSegs(as.get())))
        return;

    //Create and map the user space linker table
    linkerTableRegion *linkTable = new linkerTableRegion();
    if(!linkTable->mapAddressSpace(as.get()))
        return;

    //Bind the linker table to the address space
    as->bindLinkTable(linkTable);

    //Initialize the linker table entries for the executable and dynamic linker
    linkTable->writeSlotEntry(0, instance, true);
    linkTable->writeSlotEntry(1, dynInstance, false);

    //Set up the initial user state
    Memory::set( const_cast <savedRegs *> (regsPtr), 0, sizeof(savedRegs));

    regsPtr->cs = USER_CODE_64 | 0x3;
    regsPtr->ds = USER_DATA_64 | 0x3;
    regsPtr->es = USER_DATA_64 | 0x3;
    regsPtr->fs = USER_DATA_64 | 0x3;
    regsPtr->gs = USER_DATA_64 | 0x3;

    regsPtr->rdi = argCountPtr;
    regsPtr->rsi = argsPtr;
    regsPtr->rdx = execBase + instance->entryPoint;
    regsPtr->rip = dynInstance->entryPoint + dynBase;
    regsPtr->rsp = userStack;
    regsPtr->rbp = dlStack;
    regsPtr->ss = USER_DATA_64 | 0x3;
    regsPtr->rflags = X86_FLAGS_BASE | X86_FLAGS_INTERRUPT_ENABLE;


    kprintf("Entering @ %X:%X with user stack @ %X:%X\n", regsPtr->cs, regsPtr->rip, regsPtr->ss, regsPtr->rsp);

    //Head to user mode
    asm volatile ("mov %0, %%rsp\n"
                  "jmp isrReturn\n":: "r" (&regs): "memory");

}
