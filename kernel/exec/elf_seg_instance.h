#ifndef ELF_SEG_INSTANCE_H_
#define ELF_SEG_INSTANCE_H_

#include <addrspace/asregion.h>
#include "elf_segment.h"

class elfSegInstance : public asRegion
{
    public:
        elfSegInstance(const elfSegment &src, const uintptr_t base);
};

#endif // ELF_SEG_INSTANCE_H_
