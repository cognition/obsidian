#include "exec_manager.h"
#include "libcache.h"

static avlTree<elfExec *, pointerContentsComparable<elfExec>> activeFiles;
static ticketLock execLock;

//Several elfExec functions are implemented here for static object access
elfExec::elfExec(const String execName) : elfFile(execName), segments(new Vector<elfSegment>), base(0)
{
    base--;
    activeFiles.insert(this);
}

elfExec::~elfExec()
{
    elfExec *obj = this;
    activeFiles.remove(obj);
}

bool elfExec::dependentDecrement(void)
{
    spinLockInstance lock(&execLock);
    instanceCount--;
    if(!instanceCount)
    {
        elfExec *target = this;
        return activeFiles.remove(target);
    }
    return false;
}


/*! Retrieves or creates a new task and elfLoadedInstance object for a given executable file
 *  \param fullPath Full path and filename of the executable file
 *  \param instancePtr Return shared pointer object for the loaded instance object
 *  \param instanceTask Return pointer for the new task
 *  \return \sa emInstanceStatus
 */
emInstanceStatus executableManager::getExecInstance(const char *fullPath, shared_ptr<class elfLoadedInstance> &instancePtr, Task *&instanceTask)
{
    String fpString(fullPath);
    hashedString execPath( fpString );

    spinLock lock(&execLock);
    lock.acquire();
    //Check if there's already an instance of the requested file loaded and cached
    elfExec **existing = activeFiles.find(execPath);
    elfExec *exec;
    elfLoadedInstance *instance = nullptr;

    if(!existing)
    {
        //If no existing instance exists, then attempt to load it
        exec = new elfExec(fpString);
        exec->dependentIncrement();
        lock.release();
        if(!exec->open() )
        {
            kprintf("EM - Open failed for file %s!\n", fullPath);
            delete exec;
            return emAccess;
        }
        if(!exec->parse(elfFile::ET_EXEC, elfFile::ELF_EXECUTABLE))
        {
            kprintf("EM - Parse failed for file %s!\n", fullPath);
            delete exec;
            return emCorrupt;
        }

        if(!exec->parseSegments(0))
        {
            kprintf("EM - Segment parse failed for file %s!\n", fullPath);
            delete exec;
            return emCorrupt;
        }

        bufferedFileStatus status = exec->instantiateSegments();
        if(status != bfSuccess)
        {
            kprintf("EM - Instantiate failed for file %s!\n", fullPath);
            delete exec;
            return static_cast <emInstanceStatus> (status);
        }
    }
    else
    {
        //An exist instance of the file is already loaded
        exec = *existing;
        exec->dependentIncrement();
        lock.release();
    }

    //Spawn a new task from the instance
    instanceTask = exec->createInstance(instance);

    //Copy the actual loaded instance pointer
    if(instance)
        instancePtr.reset(instance);
    else
        return emNoMemory;

    return emSuccess;
}

emInstanceStatus executableManager::getLibInstance( const Vector<String> &paths, const char *libName, elfLoadedInstance *&instancePtr)
{
    elfExec *exec;
    elfLoadedInstance *instance = nullptr;

    String libraryName(libName);
    if(libraryName.findToken('/') == -1)
    {
        //If the library has no path indicators it's a soname, check the cache for matches
        spinLockInstance lock(&execLock);

        const String libFile = libCache::decodeSoName(libraryName);
        if(!libFile.length())
            return emAccess;
        hashedString execPath( libFile );
        kprintf("Attempting to find library: %s\n", libFile.toBuffer() );

        //See if the file is loaded already
        elfExec **existing = activeFiles.find(execPath);

        if(existing)
        {
            //If so just spawn a new instance
            kprintf("Existing file found...\n");
            exec = *existing;
            exec->dependentIncrement();
            instance = exec->instantiate();
            if(!instance)
            {
                exec->dependentDecrement();
                return emNoMemory;
            }
            instancePtr = instance;
            return emSuccess;
        }
        else
        {
            //If the there's no existing instance the library needs to be loaded
            kprintf("Library %s is not yet loaded\n", libFile.toBuffer());

            exec = new elfExec(libFile);
            kprintf("Attempting to load library: %s\n", libFile.toBuffer());

            exec->dependentIncrement();
            if(!exec->open() )
            {
                delete exec;
                return emAccess;
            }
            if(!exec->parse(elfFile::ET_DYN, elfFile::ELF_LIBRARY))
            {
                delete exec;
                return emCorrupt;
            }
            if(!exec->parseSegments(0))
            {
                delete exec;
                return emCorrupt;
            }
            if(!exec->getSymbolTable())
            {
                delete exec;
                return emCorrupt;
            }


            kprintf("Library parse successful!\n");

            //Instantiate the newly loaded library's memory image
            bufferedFileStatus status = exec->instantiateSegments();
            if(status != bfSuccess)
            {
                delete exec;
                return static_cast <emInstanceStatus> (status);
            }
            kprintf("Segment instantiation successful!\n");
            //Create an new loadedInstance object
            instance = exec->instantiate();
            if(!instance)
            {
                exec->dependentDecrement();
                return emNoMemory;
            }

            instancePtr = instance;
            return emSuccess;
        }
    }
    else
    {
        //Library is an actual file
        kprintf("Relative path loading NYI!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    return emAccess;
}
