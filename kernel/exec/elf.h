#ifndef EXEC_ELF_H_
#define EXEC_ELF_H_

#include <task.h>
#include "elf_reloc.h"
#include <regionobj.h>
#include <templates/shared_ptr.cc>
#include <templates/map.cc>
#include <string.h>
#include <fs/filesystem.h>
#include <driverapi/module_allocator.h>
#include "elf_segment.h"
#include "status.h"

#define ELF64_R_SYM(x)            (x>>32)
#define ELF64_R_TYPE(x)           (x & 0xFFFFFFFFUL)

class elfFile : public hashedString
{
    protected:
        elfFile();

	public:
		elfFile(String fileName);
		elfFile(const elfFile &other) = default;
		~elfFile();
		bool open(void);
		bool parse(const uint16_t objType, const physaddr_t baseFlags = 0);
		bool getSymbolTable(void);
        const String &toString(void) const;

        //elfFile &operator =(const elfFile &other);

        uintptr_t getEntry(void) const
        {
            return header->entry;
        }

        uintptr_t getLength(void) const
        {
            return length;
        }

        uintptr_t getSymTabSize(void) const
        {
            return symTabSize;
        }

		//ELF64 Header
		struct COMPILER_PACKED elf64Header
		{
			unsigned char ident[16];
			uint16_t type;
			uint16_t machine;
			uint32_t version;
			uint64_t entry, phOff, shOff;
			uint32_t flags;
			uint16_t headerSize;
			uint16_t phEntrySize;
			uint16_t phEntryCount;
			uint16_t shEntrySize;
			uint16_t shEntryCount;
			uint16_t sectionStringIndex;
		};

		//ELF64 Program header
		struct COMPILER_PACKED elf64ProgramHeader
		{
			uint32_t type;
			uint32_t flags;
			uint64_t offset;
			uint64_t virtAddress;
			uint64_t physAddress;
			uint64_t fileSize;
			uint64_t memorySize;
			uint64_t alignment;
		};

        //Section headers
        struct COMPILER_PACKED Elf64_SectionHeader
        {
            uint32_t sh_name;
            uint32_t sh_type;
            uint64_t sh_flags;
            uint64_t sh_addr;
            uint64_t sh_offset;
            uint64_t sh_size;
            uint32_t sh_link;
            uint32_t sh_info;
            uint64_t sh_addralign;
            uint64_t sh_entsize;
        };

        //Symbol table entry
        struct COMPILER_PACKED Elf64Sym
        {
            uint32_t name;
            uint8_t info, _reserved;
            uint16_t tableIndex;
            uint64_t value, size;
        };

		//Section header values
		static const uint32_t SHT_SYMTAB = 2;
		static const uint32_t SHT_NOBITS = 8;

		//Object type values
		static const uint16_t ET_NONE = 0;
		static const uint16_t ET_REL = 1;
		static const uint16_t ET_EXEC = 2;
		static const uint16_t ET_DYN = 3;
		static const uint16_t ET_CORE = 4;

		// EI_OSABI values
		static const uint8_t ELFOSABI_SYSV = 0;
		// EI_CLASS values
		static const uint8_t ELFCLASS32 = 1;
		static const uint8_t ELFCLASS64 = 2;
		// EI_DATA values
		static const uint8_t ELFDATA2LSB = 1;

		// Machine types
		static const uint16_t EM_X86_64 = 62;

		//ELF Identity bytes
		static const uint8_t EI_MAG0       = 0;
		static const uint8_t EI_MAG1       = 1;
		static const uint8_t EI_MAG2       = 2;
		static const uint8_t EI_MAG3       = 3;
		static const uint8_t EI_CLASS      = 4;
		static const uint8_t EI_DATA       = 5;
		static const uint8_t EI_VERSION    = 6;
		static const uint8_t EI_OSABI      = 7;
		static const uint8_t EI_ABIVERSION = 8;
		static const uint8_t EI_PAD        = 9;
		static const uint8_t EI_NIDENT     = 16;

		//Program header types
		static const uint32_t PT_NULL = 0;
		static const uint32_t PT_LOAD = 1;
		static const uint32_t PT_DYNAMIC = 2;
		static const uint32_t PT_INTERP = 3;
		static const uint32_t PT_NOTE = 4;
		static const uint32_t PT_SHLIB = 5;
		static const uint32_t PT_PHDR = 6;
		static const uint32_t PT_GNU_EHFRAME = 0x6474e550;

		//Program header flags
		static const uint32_t PF_X = 0x1U;
		static const uint32_t PF_W = 0x2U;
		static const uint32_t PF_R = 0x4U;

		//ELF file types
        static const uint64_t ELF_EXECUTABLE = 0x1U;
        static const uint64_t ELF_LIBRARY    = 0x2U;
        static const uint64_t ELF_MODULE     = 0x4U;

        //Section flags
        static const uint32_t SHF_WRITE      = 0x1U;
        static const uint32_t SHF_ALLOC      = 0x2U;
        static const uint32_t SHF_EXECINSTR  = 0x4U;


        bool loadSection(uintptr_t start, const Elf64_SectionHeader *header );

    protected:
		uintptr_t length;
		shared_ptr< elf64Header > header;
		shared_ptr< fileHandle > file;
        uint64_t flags;
		uintptr_t instanceCount;
		uintptr_t symTabSize;
};


#endif
