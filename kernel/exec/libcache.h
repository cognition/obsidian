#ifndef LIBCACHE_H_
#define LIBCACHE_H_

#include <string.h>
#include <templates/hash_table.cc>

namespace libCache
{
    void init(void);
    void addEntry(const String &soname, const String &path);
    const String decodeSoName(const String &soname);
};

#endif // LIBCACHE_H_
