#include <cpu/localstate.h>
#include "syscalls.h"
#include "user_sanity_checks.h"
#include <addrspace/userheap.h>
#include <fs/vfs.h>
#include "userstructs.h"
#include "errno.h"

//Dynamic linker system calls
#define LINKER_INIT_TABLE                   0x8000001
#define LINKER_LOAD_LIB                     0x8000002
#define LINKER_PRINT_MSG                    0x8000003

//Dynamic linker error codes
#define LINK_BAD_PARAM                      -1
#define LINK_NOT_FOUND                      -2

//MMAP values
#define VM_MAP_PROTECT_READ                 0x01
#define VM_MAP_PROTECT_WRITE                0x02
#define VM_MAP_PROTECT_EXEC                 0x04
#define VM_MAP_ANONYMOUS                    0x8


//Seek values
#define SEEK_CUR 1
#define SEEK_END 2
#define SEEK_SET 3

struct COMPILER_PACKED obs_map_res_info
{
	const char *request_string;
	uint32_t request_string_length, flags;
	uintptr_t res_addr, res_length;
};



extern "C" uint64_t system_call_handler(const uint64_t func, const uintptr_t arg1, const uintptr_t arg2, const uintptr_t arg3, const uintptr_t arg4, const uintptr_t arg5);
static int userLinkTableInit(const uintptr_t tabInfoAddr);
static int linkPrintMessage(const uintptr_t stringAddr, const uint32_t stringLength);
static bool validateUserSpan(const uintptr_t addr, const uintptr_t length, const bool writeable);
static int dynLinkLoadLib(const uintptr_t sonameAddr, const uintptr_t sonameLength);
static int memoryMap(const uintptr_t hint, const size_t size, const uint32_t protect, const uint32_t flags, const int fd);
static int userSetTls(const uintptr_t address);

static int directoryOpen(const void *path, const int pathLength, void *retHandle);
static int directoryReadEntries(const int handle, void *buffer, const size_t max_size, void *bytes_read);

static int isTty(const int fd);
static int fileSeek(const int fd, const off_t offset, const int whence, off_t *retPtr);
static int fileRead(const int fd, void *buffer, const size_t count, ssize_t *bytesRead);
static int fileWrite(const int fd, void *buffer, const size_t count, ssize_t *bytesWritten);
static void doExit(const int code);

static int nativeMapRes(const uintptr_t res_info_addr, const uintptr_t res_info_size);
static int getRefAttribute(const uintptr_t path, const uint32_t path_length, uintptr_t attr_out_ptr, uintptr_t attr_out_size, uintptr_t attr_out_type);

extern "C" uint64_t system_call_handler(const uint64_t func, const uintptr_t arg1, const uintptr_t arg2, const uintptr_t arg3, const uintptr_t arg4, const uintptr_t arg5)
{
    switch(func)
	{
	    //Control related
        case SYSCALL_EXIT:
            doExit(arg1);
            break;

        //File I/O related
        case SYSCALL_FILE_STAT:
            kprintf("File - stat called on FD: %u path: %s, flags: %x ret: %x\n", arg1, arg2, arg3, arg4);
            asm volatile ("cli\n"
                          "hlt\n"::);
            break;
        case SYSCALL_SEEK:
            return fileSeek(arg1, arg2, arg3, reinterpret_cast <off_t *> (arg4));
            break;
        case SYSCALL_READ:
            return fileRead(arg1, reinterpret_cast <void *> (arg2), arg3, reinterpret_cast <ssize_t *>(arg4));

        case SYSCALL_WRITE:
                return fileWrite(arg1, reinterpret_cast<void *> (arg2), arg3, reinterpret_cast <ssize_t *> (arg4) );
            break;

        case SYSCALL_ISTTY:
            return isTty(arg1);
        //Directory related
        case SYSCALL_OPEN_DIR:
            return directoryOpen( reinterpret_cast <void *> (arg1), arg2, reinterpret_cast <void *> (arg3));
            break;

        case SYSCALL_READ_DIR_ENTRIES:
            return directoryReadEntries(arg1, reinterpret_cast <void *> (arg2), arg3, reinterpret_cast <void *> (arg4) );
            break;
        case SYSCALL_SIGACTION:
            return ENOSYS;
            break;
        //Memory related
        case SYSCALL_TLS_SET:
            return userSetTls(arg1);
            break;
        case SYSCALL_MMAP:
            return memoryMap(arg1, arg2, arg3, arg4, arg5);
            break;
        //Utility related
        case SYSCALL_LOG_PRINT:
            if(!validateUserSpan(arg1, arg2, false))
                return -1;
            else
                kprintf("Userspace error: %s\n", arg1);
            break;

        // Dynamic linker related
        case LINKER_INIT_TABLE:
            kprintf("Linker init table call detected.\n");
            return userLinkTableInit(arg1);
            break;
        case LINKER_LOAD_LIB:
            kprintf("Linker load lib call detected.\n");
            return dynLinkLoadLib(arg1, arg2);
            break;
        case LINKER_PRINT_MSG:
            return linkPrintMessage(arg1, arg2);
            break;

        // Native interfaces
        case OBS_NATIVE_MAP_RES:
            return nativeMapRes(arg1, arg2);
        case OBS_NATIVE_GET_RS_ATTR:
            return getRefAttribute(arg1, arg2, arg3, arg4, arg5);
        default:
            kprintf("Unimplemented/Not found syscall: %u\n", func);
            asm volatile ("cli\n"
                          "hlt\n"::);
			break;
	}
	return 1;
}

static void doExit(const int code)
{
    processorScheduler *sched = localState::getSched();
    kprintf("Exiting task %u with code: %u\n",  sched->getCurrentTask()->getPid(), code );
    sched->exitCurrentTask();
}

static int isTty(const int fd)
{
    auto ct = localState::getSched()->getCurrentTask();
    fileHandle *fh = ct->getFileHandle(fd);

    if(!fh)
        return EBADF;
    return fh->isTty();
}

static int fileSeek(const int fd, const off_t offset, const int whence, off_t *retPtr)
{
    seekPos requestPos;

    auto ct = localState::getSched()->getCurrentTask();
    auto as = ct->getAddrSpace();
    //Get the corresponding fileHandle object if there is one
    fileHandle *fdHandle = ct->getFileHandle(fd);
    if(!fdHandle)
        return EBADF;
    if( !as->validateSpan( reinterpret_cast <uintptr_t> (retPtr), sizeof(off_t), false) )
        return EFAULT;

    //Validate the whence value
    switch(whence)
    {
        case SEEK_CUR:
            requestPos = seekCurrent;
            break;
        case SEEK_END:
            requestPos = seekEnd;
            break;
        case SEEK_SET:
            requestPos = seekBegin;
            break;
        default:
            return EINVAL;
            break;
    }

    fsResult result = fdHandle->seek(requestPos, offset, retPtr);
    switch(result)
    {
        case fsIsPipe:
            return ESPIPE;
        case fsValid:
            return 0;
        case fsIoError:
            return EIO;
        case fsInvalidOffset:
            return EINVAL;
        default:
            return EINVAL;
            break;
    }

}

static int fileRead(const int fd, void *buffer, const size_t count, ssize_t *bytesRead)
{
    auto ct = localState::getSched()->getCurrentTask();
    auto as = ct->getAddrSpace();
    //Get the corresponding fileHandle object if there is one
    fileHandle *fdHandle = ct->getFileHandle(fd);
    if(!fdHandle)
        return EBADF;
    if( !as->validateSpan( reinterpret_cast <uintptr_t> (bytesRead), sizeof(ssize_t), true) )
        return EFAULT;
    if( !as->validateSpan( reinterpret_cast <uintptr_t> (buffer), count, false ) )
        return EFAULT;

    ssize_t read;
    fsResult result = fdHandle->read(buffer, count, read);
    *bytesRead = read;

    switch(result)
    {
        case fsWriteOnly:
            return EACCES;
            break;
        case fsInvalidCount:
        case fsValid:
            return 0;
            break;
        case fsIoError:
        default:
            return EIO;
            break;
    }

}

static int fileWrite(const int fd, void *buffer, const size_t count, ssize_t *bytesWritten)
{
    auto ct = localState::getSched()->getCurrentTask();
    auto as = ct->getAddrSpace();
    //Get the corresponding fileHandle object if there is one
    fileHandle *fdHandle = ct->getFileHandle(fd);
    if(!fdHandle)
        return EBADF;
    if( !as->validateSpan( reinterpret_cast <uintptr_t> (bytesWritten), sizeof(ssize_t), true) )
        return EFAULT;
    if( !as->validateSpan( reinterpret_cast <uintptr_t> (buffer), count, true ) )
        return EFAULT;

    ssize_t written = 0;
    fsResult result = fdHandle->write(buffer, count, written);
    *bytesWritten = written;

    switch(result)
    {
        case fsReadOnly:
            return EACCES;
            break;
        case fsInvalidCount:
        case fsValid:
            return 0;
            break;
        case fsIoError:
        default:
            return EIO;
            break;
    }
}

/*! Opens a directory FD
 * \param path Userspace pointer to the path
 * \param pathLength Length of the path string
 * \param retHandle Pointer where the file descriptor value will be placed
 * \return Zero if successful, errno appropriate value indicating failure reasoning
 */
static int directoryOpen(const void *path, const int pathLength, void *retHandle)
{
    //Validate the user space string and return pointer
    if(!validateUserSpan(reinterpret_cast <const uintptr_t> (path), pathLength, false))
        return EINVAL;
    if(!validateUserSpan( reinterpret_cast <uintptr_t> (retHandle), sizeof(int), true))
        return EINVAL;

    auto sched = localState::getSched();
    Task *currentTask = sched->getCurrentTask();
    int *retPtr = reinterpret_cast <int *> (retHandle);

    String pathString(reinterpret_cast <const char *> (path), pathLength);

    directoryHandle *dh;
    if( int errno = Vfs::openDir(pathString, dh) )
        return errno;
    else
    {
        int fd = currentTask->allocateFileDesc();
        if(fd == -1)
        {
            delete dh;
            return ENFILE;
        }
        currentTask->mapDirHandle(fd, shared_ptr<directoryHandle> (dh));
        *retPtr = fd;


        return 0;
    }
}

static int directoryReadEntries(const int handle, void *buffer, const size_t max_size, void *bytes_read)
{
    //Validate user space structures
    uintptr_t bufferStart = reinterpret_cast <uintptr_t> (buffer);
    if(!validateUserSpan( bufferStart, max_size, true))
        return EINVAL;
    if(!validateUserSpan( reinterpret_cast <uintptr_t> (bytes_read), sizeof(ssize_t), true))
        return EINVAL;

    ssize_t *bytes_out = reinterpret_cast <ssize_t *>(bytes_read);

    size_t maxEntryCount = max_size/sizeof(dirent);
    size_t readCounter = 0;
    auto sched = localState::getSched();
    Task *currentTask = sched->getCurrentTask();

    if(directoryHandle *dh = currentTask->getDirHandle(handle))
    {
        for(size_t entry = 0; entry < maxEntryCount; entry++)
        {
            directoryEntryInfo *info;
            dirent *userEntry = reinterpret_cast <dirent *> (bufferStart * sizeof(dirent));
            directoryResult res = dh->getDirectoryEntry(info);

            if(res == dirResultSuccess)
                readCounter++;
            else
                break;

            info->getName().copyToRaw(userEntry->name, 255);
            userEntry->fsn = info->getFSN();

        }
        *bytes_out = readCounter*sizeof(dirent);
        return 0;
    }
    else
        return EBADF;
}

/*! Sets the userspace TLS structure pointer
 *  \param address Address of the user space TCB
 *  \return Zero if successful, EINVAL if the requested address was not mapped to user space
 */
static int userSetTls(const uintptr_t address)
{
    if( !validateUserSpan(address, 200, true) )
        return EINVAL;

    Task *current = localState::getSched()->getCurrentTask();
    current->setUserTLS(address);
    current->loadUserTLS();

    return 0;
}

static int memoryMap(const uintptr_t hint, const size_t size, const uint32_t protect, const uint32_t flags, const int fd)
{
    uint64_t offset, *retPtr;
    if( getStackArgs(2, &offset, &retPtr) )
    {
        if(!validateUserSpan( reinterpret_cast<uintptr_t> (retPtr), sizeof(uintptr_t), true) )
        {
            kprintf("MMAP: Unable to validate return pointer @ %X\n", retPtr);

            return EINVAL;
        }
        if(flags & VM_MAP_ANONYMOUS)
        {
            if(hint)
            {
                kprintf("Hint based MMAP NYI!\n");
            }
            else
            {
                uint64_t pageFlags = PAGE_USER;
                if(protect & VM_MAP_PROTECT_READ)
                    pageFlags |= PAGE_PRESENT;
                if(protect & VM_MAP_PROTECT_WRITE)
                    pageFlags |= PAGE_WRITE;
                if(protect & VM_MAP_PROTECT_EXEC || !protect)
                    return EPERM;

                if( (pageFlags & (PAGE_PRESENT | PAGE_WRITE)) == (PAGE_PRESENT | PAGE_WRITE))
                {

                    Task *ct = localState::getSched()->getCurrentTask();
                    auto as = ct->getAddrSpace();
                    userHeapRegion *heap = new userHeapRegion(0, (size + PAGE_OFFSET_MASK) & ~PAGE_OFFSET_MASK );
                    if(!heap->mapAddressSpace(as.get()))
                    {
                        kprintf("Error failed to map heap region to address space!\n");

                        return ENOMEM;
                    }
                    else
                    {
                        uintptr_t baseAddress = heap->getBase();
                        *retPtr = baseAddress;
                        Memory::set( reinterpret_cast <void *> (heap->getBase()), 0, REGULAR_PAGE_SIZE);
                        return 0;
                    }
                }
                else
                {
                    kprintf("Invalid MMAP protection request!\n");
                }
            }
            kprintf("Shouldn't be here...\n");
            asm volatile ("cli\n"
                          "hlt\n"::);
        }
        else
        {
            kprintf("File memory mapping NYI!\n");
            asm volatile ("cli\n"
                          "hlt\n"::);
        }
    }
    else
        return EINVAL;

    kprintf("MMAP Invalid value!\n");
    return EINVAL;
}

int nativeMapRes(const uintptr_t res_info_addr, const uintptr_t res_info_size)
{
    obs_map_res_info *res_info = reinterpret_cast <obs_map_res_info *> (res_info_addr);

    auto sched = localState::getSched();
    auto currentTask = sched->getCurrentTask();

    shared_ptr<addressSpace> addr = currentTask->getAddrSpace();

    if(res_info_size < sizeof(obs_map_res_info))
        return EINVAL;

    if(!addr->validateSpan(res_info_addr, res_info_size, true) )
        return EINVAL;

    if(addr->validateSpan( reinterpret_cast <uintptr_t> (res_info->request_string), res_info->request_string_length, false))
    {
        String resPath(res_info->request_string, res_info->request_string_length);

        refSpaceResource *rsRes = refSpace::findResource(resPath);

        hardwareRegion *resRegion = rsRes->generateMappingRegion();

        if( !addr->mapRegion(resRegion, asRegion::stackSystem, 0) )
            return ENOMEM;
        else
            return 0;
    }
    else
        return EINVAL;

    return 0;
}

int getRefAttribute(const uintptr_t path, const uint32_t path_length, uintptr_t attr_out_ptr, uintptr_t attr_out_size, uintptr_t attr_out_type)
{
    auto sched = localState::getSched();
    auto currentTask = sched->getCurrentTask();

    shared_ptr<addressSpace> addr = currentTask->getAddrSpace();

    if(!addr->validateSpan(path, path_length, false) || !addr->validateSpan(attr_out_size, sizeof(uint32_t), true) ||
       !addr->validateSpan(attr_out_type, sizeof(int), true) || !path_length )
        return EINVAL;

    String pathString(reinterpret_cast <const char *> (path), path_length );
    uint32_t *attrSize = reinterpret_cast <uint32_t *> (attr_out_size);
    int *attrType = reinterpret_cast <int *> (attr_out_type);

    //int reqType = *attrType;
    uint32_t sizeVal = *attrSize;

    if(!addr->validateSpan(attr_out_ptr, sizeVal, true) )
        return EINVAL;

    refSpaceAttribute *attr = refSpace::findAttribute(pathString);

    if(!attr)
        return ENXIO;

    refSpaceAttribute::rsAttributeType actualType = attr->getType();
    *attrType = actualType;

    qCastPtr attrOut;
    attrOut.addr = attr_out_ptr;

    switch(actualType)
    {
        case refSpaceAttribute::rsAttributeType::RS_ATTR_TYPE_UNINIT:
            break;
        case refSpaceAttribute::rsAttributeType::RS_ATTR_TYPE_UINT32:
            if(sizeVal >= sizeof(uint32_t))
                *attrOut.u32 = attr->getValue().u32;
            *attrSize = sizeof(uint32_t);
            break;
        case refSpaceAttribute::rsAttributeType::RS_ATTR_TYPE_UINT64:
            if(sizeVal >= sizeof(uint64_t))
                *attrOut.u64 = attr->getValue().u64;
            *attrSize = sizeof(uint64_t);
            break;
        case refSpaceAttribute::rsAttributeType::RS_ATTR_TYPE_INT32:
            if(sizeVal >= sizeof(int32_t))
                *attrOut.i32 = attr->getValue().i32;
            *attrSize = sizeof(int32_t);
            break;
        case refSpaceAttribute::rsAttributeType::RS_ATTR_TYPE_INT64:
            if(sizeVal >= sizeof(int64_t))
                *attrOut.i64 = attr->getValue().i64;
            *attrSize = sizeof(int64_t);
            break;
        case refSpaceAttribute::rsAttributeType::RS_ATTR_TYPE_BINARY:
            {
                Vector<uint8_t> *buffer = attr->getValue().binary;
                if(sizeVal < buffer->length() )
                    return ENOBUFS;
                Memory::copy(attrOut.ptr, buffer->getDataPtr(), buffer->length() );
            }
            break;
        case refSpaceAttribute::rsAttributeType::RS_ATTR_TYPE_STRING:
            {
                String *str = attr->getValue().str;
                if(sizeVal <= str->length())
                    return ENOBUFS;
                Memory::copy(attrOut.ptr, str->toBuffer(),  str->length() + 1);
            }
            break;
    }

    return 0;
}


int dynLinkLoadLib(const uintptr_t sonameAddr, const uintptr_t sonameLength)
{
    kprintf("Load lib called...\n");
    //asm volatile ("xchg %%bx, %%bx\n"::);
    auto sched = localState::getSched();
    auto currentTask = sched->getCurrentTask();
    shared_ptr<addressSpace> addr = currentTask->getAddrSpace();

    if(!addr->validateSpan(sonameAddr, sonameLength, false) )
        return LINK_BAD_PARAM;
    Vector<String> libPaths;
    String libName((const char *)sonameAddr, sonameLength);

    auto lt = addr->getLinkerTable();
    return lt->getLibrary(libName, libPaths);
}

static bool validateUserSpan(const uintptr_t addr, const uintptr_t length, const bool writeable)
{
    auto sched = localState::getSched();
    auto currentTask = sched->getCurrentTask();
    shared_ptr<addressSpace> as = currentTask->getAddrSpace();


    return as->validateSpan(addr, length, writeable);
}

int linkPrintMessage(const uintptr_t stringAddr, const uint32_t stringLength)
{
    if(stringLength > 256)
        return LINK_BAD_PARAM;

    if(!validateUserSpan(stringAddr, stringLength, false))
        return LINK_BAD_PARAM;

    const char *stringPtr = reinterpret_cast<const char *>(stringAddr);
    String stringObj(stringPtr, stringLength);
    kprintf("DYNLINK: %s\n", stringObj.toBuffer() );
    return 0;
}

int userLinkTableInit(const uintptr_t tabInfoAddr)
{
    auto sched = localState::getSched();
    auto currentTask = sched->getCurrentTask();
    shared_ptr<addressSpace> addr = currentTask->getAddrSpace();

    if(!addr->validateSpan(tabInfoAddr, sizeof(linkerTableRegion::kDynInfo), false))
    {
        kprintf("ULT: Bad param!\n");
        return LINK_BAD_PARAM;
    }

    addr->getLinkerTable()->bindUserInfo(tabInfoAddr);

    return 0;
}
