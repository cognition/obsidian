#include <cpu/localstate.h>
#include "user_sanity_checks.h"

extern "C"
{

	/*! Attempts to retrieve the the value of user mode value of RCX from the user mode stack after a SYSCALL instruction
	 *  \param userStack Pointer to the RSP value at the time of SYSCALL
	 *  \return Value of the RCX register in user space
	 */
	uintptr_t syscall64_retrieve_user_rcx(uint64_t *userStack)
	{
		processorScheduler *sched = localState::getSched();
		Task *currentTask = sched->getCurrentTask();
		if( !currentTask->verifyUserStack( reinterpret_cast <uintptr_t> (userStack), 8) )
		{
			//! \todo Add code to kill the task for violating syscall semantics
		}

		return userStack[1];
	}

	/*! Attempts to retrieve the the value of user mode value of ECX from the user mode stack after a SYSCALL instruction
	 *  \param userStack Pointer to the ESP value at the time of SYSCALL
	 *  \return Value of the ECX register in user space
	 */
	uint32_t syscall32_retrieve_user_ecx(uint32_t *userStack)
	{
		processorScheduler *sched = localState::getSched();
		Task *currentTask = sched->getCurrentTask();
		if( !currentTask->verifyUserStack(reinterpret_cast <uintptr_t> (userStack), 4) )
		{
			//! \todo Add code to kill the task for violating syscall semantics
		}
		return *userStack;
	}

	/*! Retrieves RCX and RDX registers from the user space stack from a SYSENTER call
	 *  \param userStack Pointer to the user space value of RCX upon SYSENTER
	 *  \return RAX = Userspace RCX, RDX = Userspace RDX
	 */
    sysenter_regs sysenter64_retrieve_user_regs(uint64_t *userStack)
    {
		sysenter_regs ret;
		processorScheduler *sched = localState::getSched();
		Task *currentTask = sched->getCurrentTask();
		if( !currentTask->verifyUserStack( reinterpret_cast <uintptr_t> (userStack), 16) )
		{
			//! \todo Add code to kill the task for violating syscall semantics
		}

		ret.rcx = *userStack;
		ret.rdx = userStack[1];

		return ret;
    }

	/*! Retrieves ECX and EDX registers from the user space stack from a SYSENTER call
	 *  \param userStack Pointer to the user space value of RCX upon SYSENTER
	 *  \return EAX = Userspace RCX, RDX = Userspace EDX
	 */
    sysenter_regs sysenter32_retrieve_user_regs(uint32_t *userStack)
    {
		sysenter_regs ret;
		processorScheduler *sched = localState::getSched();
		Task *currentTask = sched->getCurrentTask();
		if( !currentTask->verifyUserStack( reinterpret_cast <uintptr_t> (userStack), 16) )
		{
			//! \todo Add code to kill the task for violating syscall semantics
		}

		ret.rcx = *userStack;
		ret.rdx = userStack[1];

		return ret;
    }

    bool getStackArgs(uint32_t argCount, ...)
    {
        va_list args;
        va_start(args, argCount);

        uintptr_t uStack = localState::getUserStackPtr();
		processorScheduler *sched = localState::getSched();
		Task *currentTask = sched->getCurrentTask();
        if( !currentTask->verifyUserStack(uStack, argCount*sizeof(uintptr_t)) )
        {
            va_end(args);
            return false;
        }
        for(uint32_t i = 0; i < argCount; i++)
        {
            const uint64_t *stackPtr = reinterpret_cast <const uint64_t *> (uStack + sizeof(uint64_t)*(i+3));
            uint64_t *output = va_arg(args, uint64_t *);
            *output = *stackPtr;
        }
        va_end(args);

        return true;
    }

}
