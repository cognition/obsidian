#include "elf_dyn_table.h"

/*! ELF dynamic table utility class constructor
 *  \param addr Address of the dynamic table
 *  \param length Length of the table in bytes
 */
elfDynamicTable::elfDynamicTable(const uintptr_t addr, const size_t length) :
    dynBase(addr), symBase(0), relaBase(0), strBase(nullptr), dynSegSize(length), strSize(0),
    relaSize(0), symEntSize(0), relaEntSize(0)
{

}

/*! Parses the dynamic table
 *  \param loadBase Base address of the module executable
 */
void elfDynamicTable::parse(const uintptr_t loadBase)
{
    const elf64DynEntry *entry = reinterpret_cast<const elf64DynEntry *> (dynBase);
    int maxEntry = dynSegSize/sizeof(elf64DynEntry);
    //Scan the dynamic table for entries for the symbol, string and relocation tables and update the internal variables for those structures
    for(int entryNum = 0; entryNum < maxEntry; entryNum++, entry++)
    {
        switch(entry->type)
        {
            case DT_NULL:
                entryNum = maxEntry;
                break;
            case DT_STRTAB:
                strBase = reinterpret_cast <const char *> (loadBase + entry->value);
                break;
            case DT_STRSZ:
                strSize = entry->value;
                break;
            case DT_SYMTAB:
                symBase = loadBase + entry->value;
                break;
            case DT_SYMENT:
                symEntSize = entry->value;
                break;
            case DT_RELA:
                relaBase = loadBase + entry->value;
                break;
            case DT_RELASZ:
                relaSize = entry->value;
                break;
            case DT_RELAENT:
                relaEntSize = entry->value;
                break;
        }
    }
}

/*! Retrieves a string from the string table
 *  \param index Index of the string in the table
 *  \return A pointer to the string if one was valid, nullptr otherwise
 */
const char *elfDynamicTable::getString(const uintptr_t index)
{
    if(index < strSize)
        return &strBase[index];
    else
        return nullptr;
}

/*! Gets a symbol entry from the symbol table
 *  \param index Index of the symbol in the table
 *  \return A pointer to the symbol's entry
 */
const elfFile::Elf64Sym *elfDynamicTable::getSym(const uintptr_t index)
{
    uintptr_t address = symBase + index*symEntSize;
    return reinterpret_cast <const elfFile::Elf64Sym *> (address);
}

/*! Gets a relocation entry from the relocation table
 *  \param index Index of the requested relocation entry
 *  \return A pointer to a RELA type relocation entry if the index is valid, nullptr otherwise
 */
const elf64Reloca *elfDynamicTable::getReloc(const uintptr_t index)
{
    uintptr_t relaOffset = index*relaEntSize;
    if(relaOffset <= (relaSize - sizeof(elf64Reloca)) )
        return reinterpret_cast <elf64Reloca *> (relaBase + relaOffset);
    else
        return nullptr;
}

//! Returns the number of relocations in the relocation table
uint64_t elfDynamicTable::getRelaCount(void) const
{
    return (relaSize/relaEntSize);
}

//! Returns the size of the string table
size_t elfDynamicTable::getStringTabSize(void) const
{
    return strSize;
}
