#ifndef OBSIDIAN_SYSCALL_H_
#define OBSIDIAN_SYSCALL_H_

/* Obsidian posix system call slots */

//Call 0 is always reserved for retrieving a specific system call table
#define SYSCALL_GET_TABLE                0

//POSIX specific call table slots

//Execution state/flow change syscalls
#define SYSCALL_EXIT                     1
#define SYSCALL_TEXIT                    2
#define SYSCALL_YIELD                    3
#define SYSCALL_SLEEP                    4
#define SYSCALL_NANOSLEEP                5
#define SYSCALL_WAIT_PID                 6

#define SYSCALL_FORK                     7
#define SYSCALL_CLONE                    8
#define SYSCALL_EXECVE                   9
#define SYSCALL_KILL                     10

//Signals
#define SYSCALL_SET_SIGMASK              11
#define SYSCALL_SIGACTION                12

//Running process permissions/ids related syscalls
#define SYSCALL_GET_GID                  13
#define SYSCALL_GET_EGID                 14
#define SYSCALL_GET_UID                  15
#define SYSCALL_GET_EUID                 16
#define SYSCALL_GET_PID                  17
#define SYSCALL_GET_PGRP                 18
#define SYSCALL_GET_PPID                 19

#define SYSCALL_SET_UID                  20
#define SYSCALL_SET_EUID                 21
#define SYSCALL_SET_GID                  22
#define SYSCALL_SET_EGID                 23

//System information/misc. functions
#define SYSCALL_GET_ENTROPY              24
#define SYSCALL_GET_RUSAGE               25
#define SYSCALL_GET_RLIMIT               26

//Directory related syscalls
#define SYSCALL_ACCESS                   27
#define SYSCALL_FACCESS_AT               28
#define SYSCALL_OPEN_DIR                 29
#define SYSCALL_READ_DIR_ENTRIES         30
#define SYSCALL_RENAME                   31
#define SYSCALL_RENAME_AT                32
#define SYSCALL_RMDIR                    33
#define SYSCALL_UNLINK                   34
#define SYSCALL_GET_CWD                  35
#define SYSCALL_CHDIR                    36
#define SYSCALL_FCHDIR                   37
#define SYSCALL_CHROOT                   38
#define SYSCALL_MKDIR                    39
#define SYSCALL_MKDIR_AT                 40
#define SYSCALL_LINK                     41
#define SYSCALL_LINK_AT                  42
#define SYSCALL_SYMLINK                  43
#define SYSCALL_SYMLINK_AT               44
#define SYSCALL_READLINK                 45
#define SYSCALL_CHMOD                    46
#define SYSCALL_FCHMOD                   47
#define SYSCALL_FCHMOD_AT                48

//Terminal/Console related syscalls
#define SYSCALL_TTY_NAME                 49
#define SYSCALL_ISTTY                    50
#define SYSCALL_TC_GETATTR               51
#define SYSCALL_TC_SETATTR               52
#define SYSCALL_TC_FLOW                  53
#define SYSCALL_SET_SID                  54
//Memory related syscalls
#define SYSCALL_MMAP                     55
#define SYSCALL_REMAP                    56
#define SYSCALL_PROTECT                  57
#define SYSCALL_UNMAP                    58

#define SYSCALL_TLS_SET                  59

//Special IPC descriptor related
#define SYSCALL_PIPE                     60
#define SYSCALL_MAKE_FIFO_AT             61
#define SYSCALL_SOCKET                   62

//Network descriptor related
#define SYSCALL_MSG_SEND                 63
#define SYSCALL_MSG_RECV                 64
#define SYSCALL_GET_SOCK_OPT             65
#define SYSCALL_SET_SOCK_OPT             66
#define SYSCALL_ACCEPT                   67
#define SYSCALL_BIND                     68
#define SYSCALL_CONNECT                  69
#define SYSCALL_LISTEN                   70

//File and special I/O related
#define SYSCALL_FILE_OPEN                71
#define SYSCALL_DUP                      72
#define SYSCALL_DUP2                     73
#define SYSCALL_FLOCK                    74
#define SYSCALL_READ                     75
#define SYSCALL_WRITE                    76
#define SYSCALL_PREAD                    77
#define SYSCALL_SEEK                     78
#define SYSCALL_CLOSE                    79

#define SYSCALL_FCNTL                    80
#define SYSCALL_FILE_ADVISE              81
#define SYSCALL_FILE_SYNC                82

#define SYSCALL_FTRUNCATE                83
#define SYSCALL_FILE_ALLOCATE            84
#define SYSCALL_FILE_STAT                85
#define SYSCALL_UTIMENSAT                86

#define SYSCALL_POLL                     87
#define SYSCALL_SELECT                   88
#define SYSCALL_IOCTL                    89

//Futex related
#define SYSCALL_FUTEX_WAKE               90
#define SYSCALL_FUTEX_WAIT               91

//Time related
#define SYSCALL_CLOCK_GET                92

#define SYSCALL_LOG_PRINT                93

//Native system calls
#define OBS_NATIVE_MAP_RES         0x80000000U
#define OBS_NATIVE_GET_RS_ATTR     0x80000001U
#define OBS_NATIVE_GET_CPU_INFO    0x80000800U


#endif // OBSIDIAN_SYSCALLS_H_
