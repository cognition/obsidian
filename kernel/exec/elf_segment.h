#ifndef ELF_SEGMENT_H_
#define ELF_SEGMENT_H_

#include <regionobj.h>
#include <fs/filehandle.h>
#include <status.h>
#include <addrspace/page_bundle.h>

class elfSegment : public regionObj64
{
	public:
	    elfSegment(void) : virtOffset(0), fileOffset(0), fileSize(0), memSize(0), flags(0) {};
		explicit elfSegment(const uintptr_t offset, const uintptr_t mOffset, const uint64_t segSize, const uint64_t mSize, const uint64_t alignment, const uint64_t segmentFlags);

		elfSegment(const elfSegment &other) : virtOffset(other.virtOffset), fileOffset(other.fileOffset), fileSize(other.fileSize),
            memSize(other.fileSize), flags(other.flags), align(other.align), pages(other.pages) {};

        void print(void) const;

		elfSegment &operator =(const elfSegment &other)
		{
		    virtOffset = other.virtOffset;
		    fileOffset = other.fileOffset;
		    fileSize = other.fileSize;
		    memSize = other.memSize;
		    flags = other.flags;
		    pages = other.pages;

		    regLength = other.regLength;
		    regBase = other.regBase;
		    align = other.align;
		    return *this;
		}

		physaddr_t getFlags(void) const;
        uint64_t getAlign(void) const;

		bufferedFileStatus createMapping(shared_ptr<fileHandle> &file);

		const Vector< shared_ptr<pageBundle> > &getMapping(void) const;
	private:
		uintptr_t virtOffset;
		uintptr_t fileOffset;
		uintptr_t fileSize, memSize;
		uint64_t flags, align;

		Vector< shared_ptr<pageBundle> > pages;
};


#endif // ELF_SEGMENT_H_
