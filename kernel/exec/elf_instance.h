#ifndef ELF_INSTANCE_H_
#define ELF_INSTANCE_H_

#include "elf_exec.h"
#include "elf_seg_instance.h"
#include <addrspace/asregion.h>


class elfLoadedInstance
{
    public:
        elfLoadedInstance(elfExec *initFile);
        virtual ~elfLoadedInstance();
        static void init(void *data);

        shared_ptr<elfSegment> getDyn(void) const
        {
            return dyn;
        }

        uintptr_t getEntryAddr(void) const
        {
            return entryPoint;
        }

        uintptr_t getSymTabSize(void) const
        {
            return symTabSize;
        }

        uintptr_t getBase(void) const
        {
            return baseAddress;
        }

        uintptr_t getAlign(void) const;

        uintptr_t mapSegs(addressSpace *as);

    private:

        uintptr_t entryPoint, symTabSize;
        Vector<elfSegInstance *> segInstances;
        shared_ptr<elfSegment> dyn;
        uintptr_t baseAlign, baseAddress;
        elfExec *file;
        pid_t pid;
};


#endif // ELF_INSTANCE_H_
