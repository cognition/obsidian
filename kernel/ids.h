#ifndef OBSIDIAN_IDS_H_
#define OBSIDIAN_IDS_H_

#include <compiler.h>
#include <regionobj.h>
#include <templates/avltree.cc>

class idPool
{
public:
    idPool(const uint32_t base, const uint32_t span);
    bool getSingle(uint32_t &value);
    void freeSingle(const uint32_t value);
private:

    avlTree<regionObj32> pool;
};

#endif // OBSIDIAN_IDS_H_
