/* Task class header
 * Author: Joseph Kinzel */

#ifndef TASK_H_
#define TASK_H_

#include <cpu/registers.h>
#include <cpu/lock.h>
#include <memory/virtual.h>
#include <memory/phys.h>
#include <ids.h>
#include <addrspace/addrspace.h>
#include <templates/map.cc>
#include <fs/filehandle.h>
#include <templates/variant.cc>
#include <templates/unique_ptr.cc>
#include <templates/fifoq.cc>

typedef Variant<shared_ptr<fileHandle>, shared_ptr<directoryHandle>> fdHandleTypes;

#define TASK_FLAG_SAVE_USER				0x1

const uint32_t MAX_PID_VALUE = 65536;

//! Base class representing a generic event hook
class eventListener
{
	public:
		virtual bool trigger(const uint32_t generation) = 0;
		virtual ~eventListener();
        void bumpGeneration(void);
        uint32_t getGeneration(void) const;
        bool matchGeneration(const uint32_t targetGeneration);

        bool hasObtained(void);
        void updateOwner(class Task *o)
        {
            owner = o;
        }

	    static const constexpr uint32_t EL_VALID      = 0x0001U;
	    static const constexpr uint32_t EL_OBTAINED   = 0x0002U;
	    static const constexpr uint32_t EL_SERVICING  = 0x0004U;
	protected:
	    eventListener(class Task *eventOwner, const uint32_t flagsInit);

	    bool validToServicing(void);
        bool validToObtained(void);
        bool servicingToObtained(void);
        bool servicingToValid(void);
        bool invalidate(void);
        bool reset(void);


		friend class Task;
		friend class Event;
		class Task *owner;
		volatile uint32_t generation;
        //! Flags indicating status of the object
        volatile uint32_t flags;

};

class Event
{
    public:
        class eqEntry
        {
            public:
                eqEntry( shared_ptr<eventListener> &&e );
                eqEntry( eqEntry &other);
                eqEntry();
                shared_ptr<eventListener> split(uint32_t &generationRet);
            private:
                shared_ptr<eventListener> listener;
                const uint32_t targetGeneration;
        };


        Event();
        Event(Event &other);
        virtual ~Event();

        Event(const Event &other) = delete;
        Event &operator =(const Event &other) = delete;
        bool hasEvents(void) const;
        Event &operator =(Event &other);


        void pushListener( Event::eqEntry &&e );
        shared_ptr<eventListener> popListener(uint32_t &generation);
        void signalListeners(void);


    protected:

        fifoQueue< eqEntry > listeners;
};

class waitListener : public eventListener
{
    public:
        waitListener(Task *eventOwner);
        virtual ~waitListener();
        virtual bool trigger(const uint32_t generation);
};


//! Object representing a simple task
class Task
{
	public:
		Task();
		explicit Task(const uint64_t kStackStart, const uint64_t kStackEnd, const uint32_t prio, const pid_t tPid);
		Task( void *ip, const pid_t tPid );
		Task( void ( *ip )( void * ), void *context, uint32_t prio, const pid_t tPid, bool isUser = false );
		~Task();

		void execute();
		void save( savedRegs *regPtr, const void *topCaller = nullptr );
		uint32_t getPriority() const;

        void waitForEvent(Event *e);
		bool triggerWaitListener(const waitListener *listener, const uint32_t generation);

		bool verifyUserStack(uintptr_t ptr, uintptr_t length);
		void saveVectorState(void);
		void loadVectorState(void);

		void setQuantum(const uint32_t qValue);
		uint32_t getQuantum(void) const;


		shared_ptr<class addressSpace> getAddrSpace(void)
		{
		    return addrSpace;
		}

		uint64_t getPid(void) const
		{
		    return pid;
		}

		void setKernStack(const uintptr_t kStackVal)
		{
		    kernelStack = kStackVal;
		}

		inline void setUserTLS(const uintptr_t newTls)
		{
            userTLS = newTls;
		}

		inline void loadUserTLS(void)
		{
		    Processor::writeMSR(MSR_FS_BASE, userTLS);
		}

		int allocateFileDesc(void)
		{
            uint32_t fd;
            if(!fdBank.getSingle(fd))
               return -1;
            else
                return fd;
		}

		template <typename T> void mapFileHandle(const int fd, T fh)
		{
		    fdHandleTypes fht;
		    //fdHandleTypes gotVariant;
		    fht.set< shared_ptr<fileHandle> >(fh);
		    //kprintf("Mapping file descriptor %u variant type: %u\n", fd, fht.getTypeIndex() );

		    fdMap.insert(fd, fht);
		    //fdMap.get(fd, gotVariant);
		    //kprintf("Got file descriptor %u variant type: %u\n", fd, gotVariant.getTypeIndex() );
		}

		void mapDirHandle(const int fd, shared_ptr<directoryHandle> &&dh)
		{
            fdHandleTypes dht;
            dht.set< shared_ptr<directoryHandle> >(dh);
            fdMap.insert(fd, dht);
		}

		fileHandle *getFileHandle(const int fd)
		{

            //kprintf("Initial typeIndex: %X\n", fh.getTypeIndex() );
		    if(fdHandleTypes *fh = fdMap.get(fd))
            {
                if( shared_ptr<fileHandle> *ptr = fh->get_if< shared_ptr<fileHandle> >())
                {
                    fileHandle *rawFH = ptr->get();
                    return rawFH;
                }
                else
                {
                    kprintf("FD is not a file handle! TypeIndex: %X\n", fh->getTypeIndex() );
                    return nullptr;
                }
            }
            else
            {
                kprintf("Unable to find mapping for FD: %u\n", fd);

                return nullptr;
            }


		}

		directoryHandle *getDirHandle(const int fd)
		{
		    if(fdHandleTypes *fh = fdMap.get(fd))
            {
                if( shared_ptr<directoryHandle> *ptr = fh->get_if< shared_ptr<directoryHandle> >())
                {
                    directoryHandle *rawDH = ptr->get();
                    return rawDH;
                }
                else
                {
                    kprintf("FD is not a directory handle! TypeIndex: %X\n", fh->getTypeIndex() );
                    return nullptr;
                }
            }
            else
            {
                kprintf("Unable to find mapping for FD: %u\n", fd);

                return nullptr;
            }

		}

		bool assertInterruptsEnabled(void) const
		{
		    return (regs->rflags & 0x200);
		}
		bool kStackValidate(const void *base, const uint64_t size);

		void destoryAddressSpace(void);
        void printSavedRegs(void);
        void printInstructionPointer(void);

        static bool allocatePid(pid_t &pid);
        static void freePid(pid_t p);

        class taskPrioEntry
        {
            public:
                taskPrioEntry() = default;
                taskPrioEntry(Task *t) : attached(t), prio(attached->priority) {};

                uint64_t getPriority(void) const
                {
                    return prio;
                }

                Task *getTask(void)
                {
                    return attached;
                }
            private:
                Task *attached;
                uint64_t prio;
        };

        class highestPrioEntry
        {
            public:
                highestPrioEntry() = default;

                bool compare(const taskPrioEntry &first, const taskPrioEntry &second)
                {
                    return (first.getPriority() > second.getPriority());
                }
        };


        taskPrioEntry toTPE(void)
        {
            return taskPrioEntry(this);
        }
	protected:
        void bindScheduler(class processorScheduler *s);

		friend class processorScheduler;

		class Task *next, *prev;
        shared_ptr<waitListener> taskWaitListener;
		regionObj64 kernelStackRange;
		uintptr_t kernelStack;
		uintptr_t userStackSave;
		regionObj64 userStack;
		uint64_t userTLS;

		uint32_t priority, quantum, flags;
		volatile savedRegs *regs;
		vectorRegs *vectorState;
		uint64_t pml4;
		shared_ptr<class addressSpace> addrSpace;
		class processorScheduler *sched;
		uint64_t pid, uid;
		idPool fdBank;
		Map<int, fdHandleTypes> fdMap;
		ticketLock taskLock;
		bool destroyed;


		static idPool pidBank;

};

#endif
