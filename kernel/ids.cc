#include "ids.h"

idPool::idPool(const uint32_t base, const uint32_t span)
{
    pool.insert( regionObj32(base, span) );
}

bool idPool::getSingle(uint32_t &value)
{
    regionObj32 reg;

    if(! pool.retrieveMinimum(reg) )
        return false;

    if(reg.getLength() > 1)
    {
        value = reg.getBase();
        reg.setBase(value+1);
        reg.setLength( reg.getLength() - 1);
        pool.insert(reg);
        return true;
    }
    else
    {
        value = reg.getBase();
        return true;
    }

}

void idPool::freeSingle(const uint32_t value)
{
    regionObj32 postSearch(value+1, 1);
    regionObj32 preSearch(value-1, 1);


    regionObj32 postResult(value, 1);
    if(pool.retrieve(postSearch, postResult))
    {
        postResult.augmentLength(1);
        postResult.setBase(value);
    }

    if(regionObj32 *preReg = pool.find(preSearch) )
    {
        preReg->augmentLength(postResult.getLength());
    }
    else
        pool.insert(postResult);
}

