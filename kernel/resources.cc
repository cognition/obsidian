#include "resources.h"
#include <io/io.h>

//! Constructs an empty systemResource object
systemResource::systemResource() : type( systemResourceNone ), base( 0 ), length( 0 )
{
}

/*! Constructs a systemResource object with the provided properties
 *  \param resType Type of resource this object represents
 *  \param start Start address of this resource's range
 *  \param size Length of the resource's range
 */
systemResource::systemResource( systemResourceType resType, uint64_t start, uint64_t size ) : type( resType ), base( start ), length( size )
{
}

systemResource::systemResource(const systemResource &other) : type(other.type), base(other.base), length(other.length) {}

systemResource &systemResource::operator=(const systemResource &other)
{
    type = other.type;
    base = other.base;
    length = other.length;
    return *this;
}

/*! Checks if a resource object overlaps with another
 *  \param res Resource to check against for any overlaps
 *  \return True if the resources overlap, or false if they do not
 */
bool systemResource::operator ==( const systemResource &res ) const
{
	if( type != res.type )
		return false;
	uint64_t end = base + length;
	if( base >= res.base )                             // base >= res.base
		return ( base < ( res.base + res.length) );    // base < res.end
	else if(end > res.base)                            // base < res.base, end > res.base
        return (end <= (res.base + res.length));       // end <= resEnd
    else
		return false;
}

bool systemResource::operator <(const systemResource &res) const
{
	if( type == res.type )
		return ( (base+length) <= res.base);
	else
		return (type < res.type);
}

/*! Checks if a resource object's range is past that of another's
 *  \param res Resource range to check against
 *  \return True if the resource object's range is past the other's end boundry, false otherwise
 */
bool systemResource::operator >( const systemResource &res ) const
{
	if( type == res.type )
		return ( base >= ( res.base+res.length ) );
	else
		return ( type > res.type );
}

/*! Checks if a given resource contains the entirety of another
 *  \param other Resource that should be contained within this region
 *  \return True if other is completely contained, false if not
 */

bool systemResource::encases(const systemResource &other) const
{
    return ((type == other.type) && (base <= other.base) && ((base+length) >= (other.base + other.length)));
}


/*! Returns the base address value of the resource range
 *  \return Base address value of the resource range
 */
uint64_t systemResource::getBase(void) const
{
	return base;
}

/*! Returns the length of a resource range
 *  \return Length of the resource range
 */
uint64_t systemResource::getLength(void) const
{
	return length;
}

/*! Returns the type of the resource range
 *  \return systemResourceType Value indicating the type of resource
 */
systemResourceType systemResource::getType(void) const
{
	return type;
}

/*! Returns a string representation of the resource
 * \return String object describing the resource
 */
String systemResource::toString(void) const
{
    String typeStr, baseStr, rangeStr;
    switch(type)
    {
        case systemResourceMemory:
            typeStr = "MEM ";
            baseStr = String::stringFromHex(base);
            rangeStr = String("-") + String::stringFromHex(base + length - 1);
            break;
        case systemResourceInterrupt:
            typeStr = "INT ";
            baseStr = String::stringFromDec(base);
            break;
        case systemResourceIrq:
            typeStr = "IRQ";
            baseStr = String::stringFromDec(base);
            break;
        case systemResourceInterruptRoute:
            typeStr = "ROUTE ";
            baseStr = String::stringFromDec(base);
            rangeStr = String("-") + String::stringFromDec(base + length - 1);
            break;
        case systemResourceIo:
            typeStr = "I/O ";
            baseStr = String::stringFromHex(base);
            rangeStr = String("-") + String::stringFromHex(base + length - 1);
            break;
        case systemResourceDma:
            typeStr = "DMA ";
            baseStr = String::stringFromDec(base);
            break;
        case systemResourcePci:
            typeStr = "PCI Address ";
            baseStr = String::stringFromHex(base);
            break;
        default:
            typeStr = "Invalid";
            break;
    }

    return (typeStr + baseStr + rangeStr);
}


/*! Identity test */
bool systemResource::isExactly(const systemResource &other) const
{
    return ((type == other.type) && (base == other.base) && (length == other.length));
}


/*! Partitions a resource, if necessary
 *  \param resources Resource Tree to add leading and trailing sections of the resource range to
 *  \param res Target resource range
 */
void systemResource::partition( avlTree<systemResource> &resources, systemResource &res )
{
	uintptr_t resEnd = res.base+res.length;
	uintptr_t pre = res.base - base;
	// Check for resources preceding the requested range
	if( pre )
    {
        systemResource presource( type, base, pre );
		resources.insert(presource);
    }

	uintptr_t post = (base+length) - resEnd;
	// Check for trailing resources in the range
	if( post )
    {
        systemResource tailResource(type, resEnd, post);
		resources.insert(tailResource);

    }
}

/*! Extends the resource's range while leaving the base fixed
 * \param additionalLength Additional length to truncate onto the resource region
 */
void systemResource::extend(const uint64_t additionalLength)
{
    length += additionalLength;
}
