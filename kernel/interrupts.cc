#include <io/io.h>
#include <addrspace/kern_map.h>
#include <memory/memory.h>
#include <cpu/localstate.h>
#include <interrupts.h>
#include <debug.h>

#define HALT()        asm volatile ("cli\n"          \
                           "hlt\n"::)
#define BOCHS_BREAK()  asm volatile ("xchg %%bx, %%bx\n"::)

extern "C" void io_debug_print(const char *format, ...);

static const constexpr uint64_t USER_SPACE_SIZE = 0x800000000000UL;

extern "C"
{

	static void printStack( uintptr_t rsp, int count )
	{
		uint64_t *stack = reinterpret_cast <uint64_t *> ( rsp );
        for(int index = 0; index < count; index++)
		{
		    uint64_t value = stack[index];
		    if(value >= KERNEL_VSPACE_START)
            {
                uintptr_t rbase, roffset;
                String regionName = kernelSpace::decodeAddress(value, rbase, roffset);
                kprintf("Stack [%x] - Section [%s]+%X Base: %p Processor: %u\n", index,  regionName.toBuffer(), roffset, rbase, localState::getProcId() );
            }
		    else
                kprintf( "Stack [%x] - %x\n", index, value );

		}
	}

	static void printRegs( const savedRegs *regs )
	{
		kprintf( "RAX=%016X RBX=%016X RCX=%016X\n", regs->rax, regs->rbx, regs->rcx );
		kprintf( "RDX=%016X RDI=%016X RSI=%016X\n", regs->rdx, regs->rdi, regs->rsi );
		kprintf( "RBP=%016X  R8=%016X  R9=%016X\n", regs->rbp, regs->r8, regs->r9 );
		kprintf( "R10=%016X R11=%016X R12=%016X\n", regs->r10, regs->r11, regs->r12 );
		kprintf( "R13=%016X R14=%016X R15=%016X\n", regs->r13, regs->r14, regs->r15 );
	}

	//! Interrupt 0: Divide by zero
	void divideErrorException( savedRegs *regs )
	{
		kprintf( "ERROR: Divide by zero exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 1: Debug exception
	void debugException( savedRegs *regs )
	{
	    uint64_t debugStatus;
		//io_debug_print( "DEBUG: Exception caught @ %X:%X\n", regs->cs, regs->rip );
        asm volatile ("mov %%dr6, %0\n": "=r" (debugStatus):);
        if(debugStatus & DEBUG_STATUS_BP0)
            examineBp(0, regs);
        if(debugStatus & DEBUG_STATUS_BP1)
            examineBp(1, regs);
        if(debugStatus & DEBUG_STATUS_BP2)
            examineBp(2, regs);
        if(debugStatus & DEBUG_STATUS_BP3)
            examineBp(3, regs);
	}

	//! Interrupt 2: NMI
	void nmiInterrupt( savedRegs *regs )
	{
		kprintf( "ERROR: NMI caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 3: Breakpoint Exception
	void breakpointException( savedRegs *regs )
	{
		kprintf( "DEBUG: Breakpoint exception caught @ %X:%X\n", regs->cs, regs->rip );
		printStack( regs->rsp, 8 );
		HALT();
	}

	//! Interrupt 4: Overflow Exception
	void overflowException( savedRegs *regs )
	{
		kprintf( "ERROR: Overflow exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 5: Bound range exceeded Exception
	void boundRangeExceededException( savedRegs *regs )
	{
		kprintf( "ERROR: Bounds range exceeded exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 6: Invalid opcode exception
	void invalidOpcodeException( savedRegs *regs )
	{
		kprintf( "ERROR: Invalid opcode exception caught @ %X:%X\n", regs->cs, regs->rip );
		printRegs( regs );
		HALT();
	}

	//! Interrupt 7: Device not available exception
	void deviceNotAvailableExeption( savedRegs COMPILER_UNUSED *regs )
	{
		processorScheduler *sched = localState::getSched();

		Task *current = sched->getCurrentTask();
		Task *last = sched->getLastActiveVectorTask();

		sched->unmaskVectorCatch();
		if(last)
			last->saveVectorState();
		sched->setLastActiveVectorTask(current);
		current->loadVectorState();
	}

	//! Interrupt 8: Double fault exception
	void doubleFaultException( savedRegs *regs )
	{
		kprintf( "ERROR: Double fault exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 9: Coprocessor segment overrun
	void coprocessorSegmentOverrun( savedRegs *regs )
	{
		kprintf( "ERROR: Coprocessor segment overrun caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 10: Invalid TSS Exception
	void invalidTssException( savedRegs *regs )
	{
		kprintf( "ERROR: Invalid TSS Exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 11: Segment not present
	void segmentNotPresent( savedRegs *regs )
	{
		kprintf( "ERROR: Segment not present error caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 12: Stack fault exception
	void stackFaultException( savedRegs *regs )
	{
		kprintf("ERROR: Stack fault exception caught @ %X:%X\n", regs->cs, regs->rip );
		kprintf("RSP - %X RBP - %X SS - %X\n", regs->rsp, regs->rbp, regs->ss);
		HALT();
	}

	//! Interrupt 13: General Protection exception
	void generalProtectionException( savedRegs *regs )
	{
	    if(regs->rip < USER_SPACE_SIZE)
            kprintf( "ERROR: General Protection fault caught @ %X:%X on CPU#%u\n", regs->cs, regs->rip, localState::getProcId() );
        else
        {
            uintptr_t rbase, roffset;
            String regionName = kernelSpace::decodeAddress(regs->rip, rbase, roffset);
            kprintf( "ERROR: General proectation fault exception caught in section [%s]+%X Base: %p CPU#%u\n", regionName.toBuffer(), roffset, rbase, localState::getProcId() );
        }
        printRegs( regs );
        printStack( regs->rsp, 18);
        //printWatchSaves();
		HALT();
	}

	//! Interrupt 14: Page fault exception
	void pageFaultException( savedRegs *regs )
	{
		uint64_t faddr;
		asm volatile ( "mov %%cr2, %0\n" :"=r"( faddr ): );
		// Lazy mapping for higher level paging structures
		if( faddr >= SELF_REF_PAGE_DIR )
        {
            if(faddr >= SELF_REF_PDPTR)
            {
                kprintf("Error, attempted to access PDPTR region directly!\n");
            }
			else if( ! ( regs->error & ( PAGE_ERROR_PROTECTION | PAGE_ERROR_USER | PAGE_PRESENT) ) )
			{
			    const uintptr_t higherHalfStart = 1<<29;
			    bool isUser = ((faddr - SELF_REF_PAGE_DIR) < higherHalfStart) ? true:false;
			    virtualMemoryManager::mapStructures( faddr, isUser );
				return;
			}
        }
        else if(faddr < USER_SPACE_SIZE)
        {
            if(faddr >= LARGE_PAGE_SIZE)
            {
                if(!(regs->error & PAGE_PRESENT) || (regs->error & PAGE_WRITE))
                {
                    //Attempt to demand map the region or service COW request
                    auto sched = localState::getSched();
                    auto ct = sched->getCurrentTask();
                    auto as = ct->getAddrSpace();
                    if( as->requestDemandMap(faddr, regs->error) )
                        return;
                }
            }
        }

        processorScheduler *sched = localState::getSched();
        pid_t pid = 0;
        if(sched)
            pid = sched->getCurrentTask()->getPid();

        if(regs->rip < USER_SPACE_SIZE)
            kprintf( "ERROR: Page fault exception caught @ %X:%X Processor: %u Pid: %u\n", regs->cs, regs->rip, localState::getProcId(), pid );
        else
        {
            uintptr_t rbase, roffset;
            String regionName = kernelSpace::decodeAddress(regs->rip, rbase, roffset);
            kprintf( "ERROR: Page fault exception caught in section [%s]+%X Base: %p Processor: %u\n", regionName.toBuffer(), roffset, rbase, localState::getProcId() );

        }
        //We've encountered an unexpected fault, output some information
		kprintf( "Faulting access address: %X Error code: %X\n", faddr, regs->error );
        printRegs(regs);
        virtualMemoryManager::printMapping( faddr );
        printStack( regs->rsp, 18);
        //printWatchSaves();
		HALT();
	}

	//! Interrupt 16: x87 FPU Error
	void fpuError( savedRegs *regs )
	{
		kprintf( "ERROR: FPU Error encountered @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 17: Alignment check exception
	void alignmentCheckException( savedRegs *regs )
	{
		kprintf( "ERROR: Alignment check exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 18: Machine check exception
	void machineCheckException( savedRegs *regs )
	{
		kprintf( "ERROR: Machine check exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Interrupt 19: SIMD Floating point exception
	void simdException( savedRegs *regs )
	{
		kprintf( "ERROR: SIMD Floating point exception caught @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	//! Undefined, reserved interrupt handler
	void unknownReservedHandler( savedRegs *regs )
	{
		kprintf( "ERROR: Encountered an unknown processor exception @ %X:%X\n", regs->cs, regs->rip );
		kprintf( "Offending vector: %X\n", regs->vector );
		HALT();
	}

	//! Software based interrupt handler
	void softwareGuidedInterrupt( savedRegs *regs )
	{
        auto sched = localState::getSched();
        auto enterTask = sched->getCurrentTask();
	    Processor::serviceSoftIntVector(regs->vector);
        Processor *proc = localState::getProc();
        proc->apicSendEOI();
        sched->processPotentialPreempt(enterTask, regs);

        if(sched->getCurrentTask() != enterTask)
        {
            kprintf("[SCHD]: SGI - Error uncaught change in current task! Entry task: %p Current Task: %p during SGI\n", enterTask, sched->getCurrentTask() );
            asm volatile ("cli\n"
                          "hlt\n"::);
        }

	}

	/********************************
	 *  Local APIC Vector Handlers  *
	 ********************************/

	void lapicSpuriousHandler( savedRegs *regs )
	{
		kprintf( "LAPIC Spurious interrupt encountered @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	void lapicThermalHandler( savedRegs *regs )
	{
		kprintf( "LAPIC Thermal interrupt encountered @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	void lapicTimerHandler( savedRegs *regs )
	{
	    bool timerExpired;

		Processor *proc = localState::getProc();
		processorScheduler *sched = localState::getSched();
		Task *et = sched->getCurrentTask();
		if(!et->kStackValidate( reinterpret_cast <void *> (regs->rsp), 8))
        {
            kprintf("[SCHD]: LAPIC timer entered with invalid saved stack pointer of %p\n", regs->rsp);
        }

        if(!et->kStackValidate(regs, sizeof(savedRegs)))
        {
            kprintf("[SCHD]: LAPIC timer entered with invalid state!\n");
            asm volatile ("cli\n"
                          "hlt\n"::);
        }
        //kprintf("LAPIC Trigger!\n");
        timerExpired = sched->signalCurrentTimerEvent(regs);
		proc->apicSendEOI();

		sched->processPotentialPreempt(et, regs, timerExpired);
		if(sched->getCurrentTask() != et)
        {
            kprintf("[SCHD]: Error uncaught change in current task! Entry task: %p Current Task: %p during LAPIC return\n", et, sched->getCurrentTask() );
            asm volatile ("cli\n"
                          "hlt\n"::);
        }
	}

	void lapicErrorHandler( savedRegs *regs )
	{
		kprintf( "LAPIC Spurious interrupt encountered @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	void lapicPerfHandler( savedRegs *regs )
	{
		kprintf( "LAPIC Spurious interrupt encountered @ %X:%X\n", regs->cs, regs->rip );
		HALT();
	}

	void schedRemotePreempt( savedRegs *regs )
	{
	    Processor *proc = localState::getProc();
		processorScheduler *sched = localState::getSched();
        auto enterTask = sched->getCurrentTask();
	    if(!enterTask->kStackValidate(regs, sizeof(savedRegs)))
        {
            kprintf("[SCHD]: Sched remote preempt entered with invalid state!\n");
            asm volatile ("cli\n"
                          "hlt\n"::);
        }

		sched->executeRemotePreempt();
		proc->apicSendEOI();
		sched->processPotentialPreempt(enterTask, regs);
        if(sched->getCurrentTask() != enterTask)
        {
            kprintf("[SCHD]: Error uncaught change in current task! Entry task: %p Current Task: %p\n", enterTask, sched->getCurrentTask() );
            asm volatile ("cli\n"
                          "hlt\n"::);
        }

	}

}
