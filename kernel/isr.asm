[BITS 64]
[DEFAULT REL]

%include "cpu_state.inc"

; IDT Entry macro
%macro idtEntry 2
    dq isr%1, %2
%endmacro

; Fills a range of IDT entries with their proper ISR handlers
; First param = Start vector, Second param = End vector
%macro idtFill 2
    %assign i %1
    %rep ( %2-%1+1 )
        idtEntry i, 0
        %assign i i+1
    %endrep
%endmacro

; Creates a test IDT entry
%macro testIdtEntry 1
    dq tIsr%1
    dq 0
%endmacro

%macro testIdtFill 2
    %assign i %1
    %rep (%2-%1+ 1)
        testIdtEntry i
        %assign i i+1
    %endrep
%endmacro

; Creates a specific test ISR
%macro testIsr 1
tIsr%1:
    push %1
    cli
    hlt
%endmacro

; Creates a range of test ISRs
%macro fillTestIsr 2
    %assign i %1
    %rep %2-%1
        testIsr i
        %assign i i+1
    %endrep
%endmacro



; Creates an ISR that does not push an error code
; First param = ISR number, Second Param = Handler function

%macro createIsr 2

isr%1:
    push %1
    push rdx
    lea rdx, [rel %2]
    jmp isrMain

%endmacro

; Creates an ISR which pushes a dummy error code of zero
; First param = ISR number, Second Param = Handler function
%macro createIsrError 2
    isr%1:
        push 0
        push %1
        push rdx
        lea rdx, [rel %2]
        jmp isrMain
%endmacro

; Fills a certain range of ISR handlers with the same code, except for the vector value that they push
%macro fillIsrs 3
    %assign i %1
    %rep %2-%1
        createIsrError i, %3
        %assign i i+1
    %endrep
%endmacro

[SECTION .text]

	GLOBAL loadIDT, isrReturn, mainIDTR, loadTestIDT

	EXTERN divideErrorException, debugException, nmiInterrupt, breakpointException, overflowException, boundRangeExceededException, invalidOpcodeException
	EXTERN deviceNotAvailableExeption, doubleFaultException, coprocessorSegmentOverrun, invalidTssException, segmentNotPresent, stackFaultException
	EXTERN generalProtectionException, pageFaultException, fpuError, alignmentCheckException, machineCheckException, simdException, unknownReservedHandler
	EXTERN softwareGuidedInterrupt, lapicSpuriousHandler, lapicThermalHandler, lapicTimerHandler, lapicErrorHandler, lapicPerfHandler, schedRemotePreempt

%ifdef IDT_TEST
	fillTestIsr 0, 256
%endif


	createIsrError 0, divideErrorException
	createIsrError 1, debugException
	createIsrError 2, nmiInterrupt
	createIsrError 3, breakpointException
	createIsrError 4, overflowException
	createIsrError 5, boundRangeExceededException
	createIsrError 6, invalidOpcodeException
	createIsrError 7, deviceNotAvailableExeption
	createIsr 8, doubleFaultException
	createIsrError 9, coprocessorSegmentOverrun
	createIsr 10, invalidTssException
	createIsr 11, segmentNotPresent
	createIsr 12, stackFaultException
	createIsr 13, generalProtectionException
	createIsr 14, pageFaultException
	createIsrError 15, unknownReservedHandler
	createIsrError 16, fpuError
	createIsrError 17, alignmentCheckException
	createIsrError 18, machineCheckException
	createIsrError 19, simdException
	fillIsrs 20, 32, unknownReservedHandler
	createIsrError 32, lapicSpuriousHandler
	createIsrError 33, lapicThermalHandler
	createIsrError 34, lapicTimerHandler
	createIsrError 35, lapicErrorHandler
	createIsrError 36, lapicPerfHandler
	createIsrError 37, schedRemotePreempt
	fillIsrs 38, 256, softwareGuidedInterrupt

isrMain:
; Save all our GP registers
sub rsp, RDX_SAVE
mov [rsp+RCX_SAVE], rcx
mov [rsp+RBX_SAVE], rbx
mov [rsp+RAX_SAVE], rax
mov [rsp+RSI_SAVE], rsi
mov [rsp+RDI_SAVE], rdi
mov [rsp+RBP_SAVE], rbp

mov [rsp+R15_SAVE], r15
mov [rsp+R14_SAVE], r14
mov [rsp+R13_SAVE], r13
mov [rsp+R12_SAVE], r12
mov [rsp+R11_SAVE], r11
mov [rsp+R10_SAVE], r10
mov [rsp+R9_SAVE], r9
mov [rsp+R8_SAVE], r8

; Save the segment registers
mov ax, gs
mov [rsp+GS_SAVE], ax
mov ax, fs
mov [rsp+FS_SAVE], ax
mov ax, es
mov [rsp+ES_SAVE], ax
mov ax, ds
mov [rsp+DS_SAVE], ax

; Update DS and ES to our kernel values
mov ax, KERNEL_DATA_SEG
mov ds, ax
mov es, ax

mov ax, [rsp+CS_SAVE]
cmp ax, USER_CODE_SEG_32
jl .skipGs
swapgs

.skipGs:

; Call the actual handling logic for the interrupt
mov rdi, rsp
call rdx


isrReturn:



; Restore DS and ES from the stack
xor eax, eax
mov ax, word [rsp+DS_SAVE]
mov ds, ax
mov ax, word [rsp+ES_SAVE]
mov es, ax

mov ax, [rsp+CS_SAVE]
cmp ax, USER_CODE_SEG_32
jl .skipGsRestore
swapgs

.skipGsRestore:

; Restore the GP registers
mov r8, [rsp+R8_SAVE]
mov r9, [rsp+R9_SAVE]
mov r10, [rsp+R10_SAVE]
mov r11, [rsp+R11_SAVE]
mov r12, [rsp+R12_SAVE]
mov r13, [rsp+R13_SAVE]
mov r14, [rsp+R14_SAVE]
mov r15, [rsp+R15_SAVE]

mov rbp, [rsp+RBP_SAVE]
mov rdi, [rsp+RDI_SAVE]
mov rsi, [rsp+RSI_SAVE]
mov rax, [rsp+RAX_SAVE]
mov rbx, [rsp+RBX_SAVE]
mov rcx, [rsp+RCX_SAVE]
mov rdx, [rsp+RDX_SAVE]
; Adjust the stack, lop off the vector number and error code
add rsp, RIP_SAVE

iretq


loadIDT:
push rdi
push rcx
push rax

lea rdi, [rel mainIDT]

mov ecx, 255
.formatLoop:
mov rax, [rdi]
mov word [rdi+2], KERNEL_CODE_SEG
shr rax, 16
mov [rdi+6], ax
shr rax, 16
xchg eax, [rdi+8]
mov ah, 0x8E
mov [rdi+4], ax
add rdi, 16
dec ecx
jnz .formatLoop

lidt [mainIDTR]

lea rdi, [rel mainIDT]

pop rax
pop rcx
pop rdi

ret

%ifdef IDT_TEST
loadTestIDT:
push rbx
push rcx
push rax
lea rbx, [rel testIDT]
mov ecx, 256
.updateEntry:
mov eax, [rbx+4]
mov [rbx+8], ebx
mov ax, KERNEL_CODE_SEG
xchg [rbx+2], ax
mov [rbx+6], ax
mov ah, 0x8E
mov al, 0
mov [rbx+4], ax
add rbx, 16
dec ecx
jnz .updateEntry

mov rbx, testIDTR
lidt [rbx]

pop rax
pop rcx
pop rbx
ret
%endif

[SECTION .data]
mainIDT:
idtFill 0, 7
idtEntry 8, 1
idtEntry 9, 0
idtEntry 10, 2
idtEntry 11, 0
idtEntry 12, 3
idtEntry 13, 0
idtEntry 14, 4
idtFill 15, 255

mainIDTR:
dw $-mainIDT
dq mainIDT

%ifdef IDT_TEST
testIDT:
testIdtFill 0, 255

testIDTR:
dw $-testIDT
dq testIDT

%endif
