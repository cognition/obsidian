#include <io/io.h>
#include "cppruntime.h"

static destructorCall destructorStack[512];
static destructorCall *destructorStackPtr = destructorStack;

extern "C"
{

	void *__dso_handle = NULL;

	int __cxa_atexit( funcPtr f, void *obj, void *dso )
	{
		destructorCall destruct = {f, obj, dso};
		if( destructorStackPtr == &destructorStack[512] )
			return -1;
		*destructorStackPtr = destruct;
		destructorStackPtr++;
		return 0;
	}

	void __cxa_finalize( funcPtr f )
	{
		if( !f )
		{
			if( destructorStackPtr != destructorStack )
			{
				do
				{
					destructorStackPtr--;
					if( destructorStackPtr->destructorFunc )
						destructorStackPtr->destructorFunc( destructorStackPtr->objPtr );
				} while( destructorStackPtr != destructorStack );
			}
			else
			{
				destructorCall *cmpPtr = destructorStack;
				while( cmpPtr != destructorStackPtr )
				{
					if( cmpPtr->destructorFunc == f )
					{
						cmpPtr->destructorFunc( cmpPtr->objPtr );
						cmpPtr->destructorFunc = NULL;
						break;
					}
					cmpPtr++;
				}
			}
		}
	}

	void __cxa_pure_virtual()
	{
		kprintf( "ERROR: Attempt was made to call a pure virtual function!" );
		asm ( "cli\n"
		      "hlt\n":: );
	}

}
