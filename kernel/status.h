#ifndef STATUS_H_
#define STATUS_H_

enum bufferedFileStatus
{
    bfSuccess = 0,
    bfIoError,
    bfAccessError,
    bfNoMemoryError
};


enum emInstanceStatus
{
    emSuccess = bfSuccess,
    emAccess = bfAccessError,
    emIoError = bfIoError,
    emNoMemory = bfNoMemoryError,
    emCorrupt,
    emNoDep
};


#endif // STATUS_H_
