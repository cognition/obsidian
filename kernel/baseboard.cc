#include "baseboard.h"

baseBoard::baseBoard(refSpaceObj *parentRef) : Device("Motherboard", "mb", parentRef )
{
	resources.insert( systemResource( systemResourceMemory, 0, 0xFFFFFFFFFFFFFFFFUL ) );
	resources.insert( systemResource( systemResourceInterrupt, 0, 0xFFFFFFFF ) );
	resources.insert( systemResource( systemResourceInterruptRoute, 0, 0xFFFFFFFF ) );
	resources.insert( systemResource( systemResourceIo, 0, 0xFFFF ) );
	resources.insert( systemResource( systemResourceIrq, 0, 16 ) );
	resources.insert( systemResource( systemResourceDma, 0, 8 ) );
	resources.insert( systemResource( systemResourcePci, 0, 0xFFFFFFFFFFFFFFFFUL ) );
}

bool baseBoard::init( void COMPILER_UNUSED *context )
{
	return true;
}
