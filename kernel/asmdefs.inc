; x86-64 General assembly macros and definitions file

%define KERN_PHYS                   0x200000
%define KERN_VADDR                  0xFFFF800000000000
%define PADJUST(x)                  ( (x-KERN_VADDR)+KERN_PHYS)

%define MSR_EFER                    0xC0000080
%define MSR_KERN_GS_BASE            0xC0000102
%define MSR_GS_BASE                 0xC0000101

%define KERNEL_CODE_64              0x8
%define KERNEL_DATA_SEG             0x10
%define KERNEL_LDT_SEL              0x40
%define KERNEL_TSS_SEL              0x50

