#ifndef INTERRUPT_VECTORS_H_
#define INTERRUPT_VECTORS_H_

#include <cpu/registers.h>

//Prototypes
extern "C" {
	void divideErrorException( savedRegs *regs );
	void debugException( savedRegs *regs );
	void nmiInterrupt( savedRegs *regs );
	void breakpointException( savedRegs *regs );
	void overflowException( savedRegs *regs );
	void boundRangeExceededException( savedRegs *regs );
	void invalidOpcodeException( savedRegs *regs );
	void deviceNotAvailableExeption( savedRegs *regs );
	void doubleFaultException( savedRegs *regs );
	void coprocessorSegmentOverrun( savedRegs *regs );
	void invalidTssException( savedRegs *regs );
	void segmentNotPresent( savedRegs *regs );
	void stackFaultException( savedRegs *regs );
	void generalProtectionException( savedRegs *regs );
	void pageFaultException( savedRegs *regs );
	void fpuError( savedRegs *regs );
	void machineCheckException( savedRegs *regs );
	void alignmentCheckException( savedRegs *regs );
	void simdException( savedRegs *regs );
	void unknownReservedHandler( savedRegs *regs );
	void softwareGuidedInterrupt( savedRegs *regs );
	//LAPIC function related prototypes
	void lapicSpuriousHandler( savedRegs *regs );
	void lapicThermalHandler( savedRegs *regs );
	void lapicTimerHandler( savedRegs *regs );
	void lapicErrorHandler( savedRegs *regs );
	void lapicPerfHandler( savedRegs *regs );

	//Scheduler related IPIs
	void schedRemotePreempt( savedRegs *regs );
}

#endif
