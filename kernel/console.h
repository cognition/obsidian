#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <io/textoutdev.h>

//! Object which represents a 80x25 char CGA textmode adapter
class bootConsole : public textOutputDevice
{
	public:
		bootConsole();
		virtual ioDevResult writeBuffer( const void *buffer, uintptr_t length );
		virtual ioDevResult preformOp( const streamOp o );
		virtual ioDevResult echoChar(const char value);
		virtual void getDims(int &rows, int &cols);
		void relocate();
		void init();
	private:
		void shiftBuffer( uint32_t lines );
		//! Pointer to the hardware's output buffer
		uint16_t *hwBuffer;
		//! Pointer to the memory buffer
		static uint16_t memBuffer[];
		//! Color mask, upper 8 bits represent the current color scheme
		uint16_t colorMask;
		//! Active flag
		bool active;
};

#endif
