#include <compiler.h>
#include <memory/memory.h>
#include "string.h"
#include <io/ioiface.h>

#define STRING_MAX              255
#define UINT64_DEC_STR_LEN_MAX  19
#define UINT64_HEX_STR_LEN_MAX  16


//! Default empty constructor
String::String() : stringLength(0), buffer(zeroLengthString)
{
}

//! Copy constructor
String::String(const String &other) : stringLength(other.stringLength), buffer(other.buffer)
{
}

/*! \brief Buffer based initializer
 *  \param src Source buffer
 */
String::String( const char *src ) : stringLength( strlen(src) ), buffer(new Vector<char>(stringLength+1) )
{
    if(stringLength)
        buffer->insert(src, stringLength);
    buffer->insert(0);
}

/*! \brief Size limited buffer based initialization
 *  \param src Source buffer
 *  \param length Length of the string
 */
String::String( const char *src, int length ) : stringLength(length), buffer(new Vector<char> (stringLength+1))
{
    buffer->insert(src, length);
    buffer->insert(0);
}

/*! \brief Copy operator
 *  \param src Source string for copy operation
 *  \return Reference to the destination string
 */
String &String::operator =( const String &src )
{
	stringLength = src.stringLength;
	buffer = src.buffer;
	return *this;
}

/*! \brief Copy operator for an ascii buffer
 *  \param src Source buffer to copy the string from
 *  \return Reference to the destination string
 */
String &String::operator =( const char *src )
{
	stringLength = strlen( src );
	buffer.reset( new Vector<char>(stringLength+1) );

    if(stringLength)
        buffer->insert(src, stringLength);

    buffer->insert(0);

	return *this;
}

/*! \brief Lexical equality operation
 *  \param t Ascii string to test against
 *  \return True if the strings are lexically equivalent, false otherwise
 */
bool String::operator ==( const char *t ) const
{
	int l2 = strlen( t );
	if( stringLength != l2 )
	{
		return false;
	}
	for( int counter = 0; counter < stringLength; counter++ )
	{
		if( t[counter] != buffer->at(counter) )
			return false;
	}
	return true;
}

bool String::operator ==(const String &other) const
{
    if( length() != other.length() )
        return false;
    for(int index = 0; index < length(); index++)
    {
        if( buffer->at(index) != other.buffer->at(index) )
            return false;
    }

    return true;
}

bool String::operator <(const String &other) const
{
    int result, minLength;

    minLength = min(length(), other.length() );
    result = strncmp( buffer->getBuffer(), other.buffer->getBuffer(), minLength);
    if(result < 0)
        return true;
    else if( (result == 0) && (length() < other.length()) )
        return true;
    else
        return false;
}

bool String::operator >(const String &other) const
{
    int result, minLength;

    minLength = min(length(), other.length() );
    result = strncmp( buffer->getBuffer(), other.buffer->getBuffer(), minLength);
    if(result > 0)
        return true;
    else if( (result == 0) && (length() > other.length()) )
        return true;
    else
        return false;

}

/*! Concatenation operation between two String objects
  * \param other String on the right side of the operator
  * \return A new String object containing the concatenation of both strings
  */
String String::operator +(const String &other) const
{
    int newLength = other.stringLength + stringLength;
    Vector<char> *retVect = new Vector<char>(newLength + 1);

    retVect->insert( buffer->getBuffer(), stringLength );
    retVect->insert( other.buffer->getBuffer(), other.stringLength );
    retVect->insert(0);

    return String(retVect);
}

/*! Concatenation operation between a String and string literal
  *
  * \param other String literal on the right side of the operator
  * \return A new String object containing the concatenation of the String object and string literal
  */
String String::operator +(const char *other) const
{
    int otherLength = strlen(other);
    int newLength = otherLength + stringLength;
    Vector<char> *retVect = new Vector<char>(newLength + 1);

    retVect->insert( buffer->getBuffer(), stringLength );
    retVect->insert(other, otherLength);
    retVect->insert(0);

    return String(retVect);
}

/*! \brief Copies the contents of a string to a raw character buffer
 *  \param rawDest Target buffer
 *  \param maxSize Maximum size of potential buffer contents
 *  \return True if the string contents were successfully copied, false if the string contents would exceed the buffer's maximum length
 */
bool String::copyToRaw(char *rawDest, const size_t maxSize)
{
    if(maxSize <= stringLength)
        return false;

   Memory::copy(rawDest, buffer->getDataPtr(), stringLength+1);
   return true;
}


/*! \brief String length accessor
 *  \return Length of the string in characters
 */
int String::length() const
{
	return stringLength;
}

/*! \brief Raw string buffer accessor
 *
 *  \return A writeable point to the string's buffer
 */
char *String::getBuffer()
{
    return buffer->getBuffer();
}

/*! \brief Returns a pointer to the string's buffer
 *  \return Pointer to the String's buffer
 */
const char *String::toBuffer() const
{
	return const_cast <const char *> ( buffer->getBuffer() );
}

/*! Gets the character at a specific point in the string
 *  \param index Location to get the character from
 *  \return The requested character if available or -1 if the index is invalid
 */
const char String::at(const int index) const
{
    if(index < buffer->length())
        return buffer->at(index);
    else
        return -1;
}

/*! Returns the shared_ptr object which contains the string's buffer
 *  \return Const reference to the string's shared_ptr
 */
shared_ptr<Vector<char>> const &String::getSharedPtr(void) const
{
	return buffer;
}

/*! Generic string length function
 *
 *  \param s Input string buffer
 *
 *  \return Length of the string buffer
 */
int String::strlen( const char *s )
{
	int l = 0;
	do
	{
		if( !*s )
			break;
		s++;
		l++;
	} while( l < STRING_MAX );
	return l;
}

/*! Bounded string length function
 *
 *  \param s Source raw string buffer
 *  \param maxLength Maximum size of the string's buffer
 *
 *  \return Size of the string or maximum size of the buffer
 */
int String::strnlen( const char *s, const int maxLength)
{
    int length;
    const char *ptr = s;
    for(length = 0; length < maxLength; length++, ptr++)
    {
        if(*ptr == 0)
            break;
    }

    return length;
}

/*! Bounded string compare
 *
 *  \param first First of the string buffer to compare
 *  \param second Second string buffer to compare
 *  \param totalLength Upper bounds for the compare operation
 *
 *  \return Zero if the strings are equal, or the index of the first differing character, negative if the first string's character is lexographically smaller than the second's or positive otherwise.
 */
int String::strncmp(const char *first, const char *second, const int totalLength)
{
    const char *fPtr = first;
    const char *sPtr = second;
    for(int index = 0; index < totalLength; index++, sPtr++, fPtr++)
    {
        if(*fPtr < *sPtr)
            return -1*(index+1);
        else if(*sPtr < *fPtr)
            return (index+1);
    }

    return 0;
}

/*! Searches a buffer for a specific token
 *  \param str Input buffer
 *  \param maxLength Maximum buffer size
 *  \param token Character to find
 *  \return Index of the character or -1 if the character could not be located
 */
int String::strntok(const char *str, const int maxLength, const char token)
{
    for(int index = 0; index < maxLength; index++)
    {
        if(str[index] == token)
            return index;
    }

    return -1;
}

/*! Finds a substring within a string
 *  \param s Substring to search for
 *  \return Index that the substring can be found at, or -1 if it was not found
 */
int String::findSubstring( const char *s ) const
{
	int l = strlen(s);
	int index = 0;
	uint32_t rem = l;
	uint32_t tIndex = 0;

	while((stringLength-index) >= rem)
	{
		if(buffer->at(index) != s[tIndex])
		{
			tIndex = 0;
			rem = l;
			continue;
		}
		else
		{
			tIndex++;
			if(!--rem)
				return (index-l+1);
		}
		index++;
	}
	return -1;
}

/*! Searches for a specific character in a substring
 *  \param t Character to search for
 *  \param start Index to begin searching at
 *  \return Index of the first token found, or -1 if no token character was found
 */
int String::findToken(const char t, const int start) const
{
	int index = start;
	while(index < stringLength)
	{
		if(buffer->at(index) == t)
			return index;
		index++;
	}
	return -1;
}

/*! Reverse token search
 *
 *  \param token Token to search a string for
 *  \param start Starting index
 *
 *  \return Index of the first token found or -1 if no token could be found in the specified substring
 */
int String::findTokenRev(const char token, const int start) const
{
    int index = start;
    if(index < 0)
        index = stringLength-2;

    for(; index >= 0; index--)
    {
        if(buffer->at(index) == token)
            return index;
    }

    return -1;
}

String String::substr(int start = 0, int size = STRING_MAX) const
{
	if( (start >= stringLength) )
		return String();
	else if( (size >= stringLength))
	{
		if(size == STRING_MAX)
			return String(&buffer->getBuffer()[start], stringLength - start);
		else
			return String();
	}
	else
	{
		return String(&buffer->getBuffer()[start], size);
	}
}

Vector<String> String::splitToken(const char token) const
{
	Vector<String> results;
	int last = findToken(token, 0);

	if(last == -1)
	{
		results.insert(*this);
		return results;
	}
	else if(last)
	{
		results.insert( String(buffer->getBuffer(), last));
	}
	last++;

	for(int next = findToken(token, last); next != -1; next = findToken(token, last))
	{
		if(last != next)
			results.insert( substr(last, next - last) );
		last = next+1;
	}

	if(last < stringLength)
		results.insert( substr(last) );

	return results;
}

String String::stringFromHex(const uint64_t value, const int digits)
{
    char buffer[17];
    buffer[16] = 0;
    uint64_t remValue = value;
    String ret;
    int currentOffset = 15;
    int digitsRem = digits;

    for(; currentOffset >= 0; currentOffset--)
    {
        if(!remValue)
        {
            buffer[currentOffset] = '0';
            digitsRem--;
            if(digitsRem <= 0)
            {
                ret = &buffer[currentOffset];
                break;
            }
        }
        else
        {
            char digit = remValue & 0xF;
            remValue >>= 4;
            buffer[currentOffset] = digit > 9 ? (digit +  55):(digit + 0x30);
            digitsRem--;
            if(!remValue)
            {
                if(digitsRem <= 0)
                {
                    ret = &buffer[currentOffset];
                    break;
                }
            }
        }
    }

    if(currentOffset < 0)
        ret = buffer;

    return ret;
}

String String::stringFromDec(const uint64_t value, const int digits)
{
    char buffer[21];
    buffer[20] = 0;
    uint64_t remValue = value;
    int currentOffset = 19;
    int digitsRem = digits;

    for(; currentOffset >= 0; currentOffset--)
    {
        if(!remValue)
        {
            buffer[currentOffset] = '0';
            digitsRem--;
            if(digitsRem <= 0)
            {
                return String(&buffer[currentOffset], 20 - currentOffset);
            }
        }
        else
        {
            char digit = remValue % 10;
            remValue /= 10;
            buffer[currentOffset] = (digit + 0x30);
            digitsRem--;
            if(!remValue)
            {
                if(digitsRem <= 0)
                {
                    return String(&buffer[currentOffset], 20 - currentOffset);
                }
            }
        }
    }

    if(currentOffset < 0)
        return String(buffer, 19);

    return String();


}

bool String::hexToUnsigned(uint64_t &value) const
{
    uint64_t currentVal, topMask;

    currentVal = 0;
    topMask = 0xF;
    topMask <<= 60;

    for(int index = 0; index < stringLength; index++)
    {
        uint64_t charValue = 0;
        const char currentChar = buffer->at(index);
        switch(currentChar)
        {
            case '0':
                charValue = 0;
                break;
            case '1':
                charValue = 1;
                break;
            case '2':
                charValue = 2;
                break;
            case '3':
                charValue = 3;
                break;
            case '4':
                charValue = 4;
                break;
            case '5':
                charValue = 5;
                break;
            case '6':
                charValue = 6;
                break;
            case '7':
                charValue = 7;
                break;
            case '8':
                charValue = 8;
                break;
            case '9':
                charValue = 9;
                break;
            case 'a':
            case 'A':
                charValue = 10;
                break;
            case 'b':
            case 'B':
                charValue = 11;
                break;
            case 'c':
            case 'C':
                charValue = 12;
                break;
            case 'd':
            case 'D':
                charValue = 13;
                break;
            case 'e':
            case 'E':
                charValue = 14;
                break;
            case 'f':
            case 'F':
                charValue = 15;
                break;
            default:
                kprintf("String::hexToUnsigned - Found unexpected character with value %u\n", currentChar);
                return false;
                break;
        }

        if(currentVal & topMask)
            return false;

        currentVal <<= 4;
        currentVal |= charValue;
    }

    value = currentVal;

    return true;
}

bool String::decToUnsigned(uint64_t &value) const
{
    uint64_t currentVal, lastVal;

    currentVal = 0;
    lastVal = 0;

    for(int index = 0; index < stringLength; index++)
    {
        const char currentChar = buffer->at(index);
        if((currentChar >= '0') && (currentChar <= '9'))
        {
            currentVal *= 10;

            currentVal += (currentChar - '0');
            if(currentVal < lastVal)
                return false;

            lastVal = currentVal;
        }
        else
            return false;
    }

    value = currentVal;
    return true;
}

uint64_t String::hash(void) const
{
    static const uint64_t oBasis = 14695981039346656037U;
    static const uint64_t prime = 1099511628211U;
    uint64_t hValue = oBasis;
    for(int i = 0; i < stringLength; i++)
    {
        hValue ^= buffer->at(i);
        hValue *= prime;
    }

    return hValue;
}

// hashedString class
hashedString::hashedString(const String &init) : hash(init.hash()), str(init)
{
}

hashedString::hashedString(const hashedString &other) : hash(other.hash), str(other.str)
{

}

bool hashedString::operator ==(const hashedString &other) const
{
    if(hash == other.hash)
        return (str == other.str);

    return false;
}

bool hashedString::operator <(const hashedString &other) const
{
    return (hash < other.hash);
}

bool hashedString::operator >(const hashedString &other) const
{
    return (hash > other.hash);
}

hashedString &hashedString::operator =(const hashedString &other)
{
    hash = other.hash;
    str = other.str;
    return *this;
}
