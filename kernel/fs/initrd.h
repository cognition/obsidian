#ifndef INITRD_H_
#define INITRD_H_

#include <compiler.h>
#include "filesystem.h"
#include "filesession.h"
#include "filehandle.h"

#define RAW_ENTRY_DIRECTORY            0x8000

class COMPILER_PACKED rawEntry
{
	public:
		inline uint32_t getLength(void) const
		{
			return length;
		}

		inline uint32_t getNameOffset(void) const
		{
			return nameOffset;
		}

		inline uint32_t getOffset(void) const
		{
			return offset;
		}

		inline bool isDir(void) const
		{
		    return (flags & RAW_ENTRY_DIRECTORY);
		}

		inline bool isFile(void) const
		{
		    return !isDir();
		}

		inline uint32_t getMaxSubEntry(void) const
		{
		  return (length/sizeof(rawEntry));
		}
	protected:
		rawEntry(){}
		uint32_t length;
		uint32_t flags;
		uint32_t offset;
		uint32_t nameOffset;
};

class initRdDirHandle : public directoryHandle
{
    public:
        initRdDirHandle(const stringFragment &initPath, class initRamdiskFs *initFs, const rawEntry *entry);
        rawEntry *getRawEntry(void) const;
};

struct COMPILER_PACKED imageFileHeader
{
	const uint64_t magic;
	const uint32_t imageSize;
	const uint32_t stringTableSize;
	const uint32_t rootDirSize;
};

/*! Initial RAM Disk file system */
class initRamdiskFs : public fileSystem
{
	public:
		static initRamdiskFs *mapFromMem(const uint32_t physicalAddress, const uint32_t length);
		const rawEntry *findEntry(const char *path) const;
		//Required interface operations
		virtual fileHandle *open(const String &path, const uint8_t permissions);
		virtual fsResult close(fileSession *session);
		virtual fsResult read(void *data, const uint64_t offset, const uint64_t count, void *buffer, ssize_t &bytesRead);
		virtual fsResult write(void *data, const uint64_t offset, const uint64_t count, const void *buffer, ssize_t &bytesWritten);
		//Directory related functions
		virtual int openDirectory(const stringFragment &path, directoryHandle *&retHandle);
		virtual directoryResult getDirectoryEntry(directoryHandle *dir, const uint64_t index, directoryEntryInfo *&info);

	private:
		initRamdiskFs(const uintptr_t imageVirtualAddress, const uint32_t memLength, const imageFileHeader *imgHeader);

		const char *getStringFromTable(const uint32_t index) const;
		const rawEntry *matchEntry(const rawEntry *start, const uint32_t entryCount, String &entryName) const;


		const uintptr_t virt;
		const uint32_t imageLength;
		const imageFileHeader *header;
		const rawEntry *rootEntries;
		const char *stringTable;
		const uint32_t rootEntryCount;
};

namespace initRd
{
	bool initialize(const uint32_t physAddress, const uint32_t length);
	bool fileExists(const char *name);
	fileHandle *open(String &path, uint8_t permissions);
}

#endif
