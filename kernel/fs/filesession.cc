#include "filesession.h"
#include <cpu/localstate.h>
#include <io/io.h>

#define PIPE_DEFAULT_SIZE               65536

static String anonPipeString("Anonymous Pipe.");

//fileSession class
fileSession::fileSession(const String &path, fileSystem *parentFs) : fileName(path), flags(sessionFsFile), typeSpecific(new fileTypeSpecific)
{
    typeSpecific->fsData.init(parentFs);
}

fileSession::fileSession(textOutputDevice *dev, const uint32_t typeFlags) : fileName(""), flags(typeFlags | sessionPipe | sessionTerm | sessionPipeDevOut), typeSpecific(new fileTypeSpecific)
{
    typeSpecific->pipeData.init(dev);
}

fileSession::fileSession(shared_ptr<fileTypeSpecific> &ts, const uint32_t pipeFlags) : flags(pipeFlags | sessionPipe), typeSpecific(ts)
{
}

//Destructor
fileSession::~fileSession()
{
    //Cleanup if necessary
    if((flags & (sessionPipe | sessionTerm)) == sessionPipe)
        typeSpecific->pipeData.cleanup();
    else if(flags & sessionFsFile)
        typeSpecific->fsData.cleanup(this);
}

void fileSession::setEOF(uint64_t value)
{
    kprintf("File: %s has size %u\n", fileName.toBuffer(), value);
    typeSpecific->fsData.setEOF(value);
}

/*! Creats an anonymous pipe with a single reader and writer
 *  \param reader Return reference for the created reader
 *  \param writer Return reference for the created writer
 *  \param readerFlags Attribute flags for the reader
 *  \param writerFlags Attribute flags for the writer
 *  \param pipeSize Size of the pipe's buffer in bytes
 *  \return True if the creation was successful and the return pointers are valid, false otherwise
 */
bool fileSession::createPipe(shared_ptr<fileSession> &reader, const uint32_t readerFlags, shared_ptr<fileSession> &writer, const uint32_t writerFlags, const uint32_t pipeSize = PIPE_DEFAULT_SIZE)
{
    shared_ptr<fileTypeSpecific> sharedPipeState(new fileTypeSpecific);
    sharedPipeState->pipeData.init(pipeSize);
    reader.reset(new fileSession(sharedPipeState, sessionReadOnly | readerFlags) );
    writer.reset(new fileSession(sharedPipeState, sessionWriteOnly | writerFlags) );
    return true;
}

const String &fileSession::toString(void) const
{
    if(flags & sessionFsFile)
        return fileName;
    else
        return anonPipeString;
}

uint64_t fileSession::getGeneration(void) const
{
	return generation;
}

void *fileSession::getData(void) const
{
	return typeSpecific->fsData.getData();
}

void fileSession::setData(void *value)
{
	typeSpecific->fsData.setData(value);
}

/*! Closes the file
 *  \sa fsResult for possible return values
 */
fsResult fileSession::close(void)
{
	spinLockInstance lock(&writeLock);
	uint64_t val = __sync_fetch_and_sub(&activeInstances, 1);

	generation++;
	if(val == 1)
		return typeSpecific->fsData.close(this);
	else
		return fsValid;
}

/*! Reads from the file
 *  \param buffer Destination buffer for the data
 *  \param offset Offset into the file
 *  \param count Number of bytes to attempt to read
 *  \param gen Generation of the file
 *  \param bytesRead Return reference for the number of bytes actually read
 *  \sa fsResult for potential error codes
 */
fsResult fileSession::read(void *buffer, const size_t offset, const size_t count, const uint64_t gen, ssize_t &bytesRead)
{
    if(flags & sessionWriteOnly)
        return fsWriteOnly;

    if(flags & sessionFsFile)
    {
        //Alter the caller that the file has changed
        if(gen < generation)
        {
            kprintf("File session gen mismatch!\n");
            return fsOldGeneration;
        }
        else if( offset >= typeSpecific->fsData.getEOF() )
        {
            //Obviously there's nothing past the EOF
            kprintf("VFS - Read - Invalid offset! Requested: %u File size: %u\n", offset+count, typeSpecific->fsData.getEOF() );
            return fsInvalidOffset;
        }
        else
            return typeSpecific->fsData.read(buffer, offset, count, bytesRead);
    }
    else if(flags & sessionPipe)
        return typeSpecific->pipeData.read(flags, buffer, count, bytesRead);

    return fsStateError;
}

/*! Attempts to write to a file object
 *  \param buffer Data to write to the file
 *  \param offset Offset in the file to perform the write to
 *  \param amount Number of bytes to write
 *  \param gen Generation of the file possessed by the writer
 *  \param bytesWritten Value return for the number of bytes actually written
 *
 *  \sa fsResult for return values
 */

fsResult fileSession::write(const void *buffer, const size_t offset, const size_t amount, const uint64_t gen, ssize_t &bytesWritten )
{
	if(flags & sessionReadOnly)
        return fsReadOnly;

	spinLockInstance lock(&writeLock);

	if(flags & sessionFsFile)
    {
        uint64_t newGen = gen + 1;
        if( offset > typeSpecific->fsData.getEOF() )
            return fsInvalidOffset;
        //Update the generation counter to indicate the file has changed
        else if( !__sync_bool_compare_and_swap(&generation, gen, newGen) )
            return fsOldGeneration;
        else
        {
            //Update the EOF if the write alters it
            const uint64_t endPoint = offset + amount;
            if(endPoint > typeSpecific->fsData.getEOF() )
                typeSpecific->fsData.setEOF(endPoint);

            return typeSpecific->fsData.write(buffer, offset, amount, bytesWritten);
        }
    }
    else if(flags & sessionPipe)
        return typeSpecific->pipeData.write(flags, buffer, amount, bytesWritten);
    else
        return fsStateError;
}

/*! Verifies if the requested seek operation is valid and updates the internal offset pointer
 *  \param pos Base location of the seek operations
 *  \param current Current offset in the file
 *  \param absOffset Resulting absolute offset
 *  \return fsValid if successful, fsIsPipe if the seek was requested on a pipe and fsStateError if the fileSession object is malformed
 */
fsResult fileSession::verifySeek(const seekPos pos, const off_t current, const off_t offset, off_t &absOffset)
{
    //Pipes can't seek, files can
    if(flags & sessionPipe)
       return fsIsPipe;
    else if(flags & sessionFsFile)
        return typeSpecific->fsData.verifySeek(pos, current, offset, absOffset);
    else
        return fsStateError;
}

int fileSession::isTty(void)
{
    if(flags & sessionTerm)
        return 0;
    else
        return 1;
}

/* pipeDataObj internal class */

//Non-terminal initialization function
void fileSession::pipeDataObj::init(const uint32_t initPipeCapacity)
{
    pipeSpecific.buffer = new unsigned char[initPipeCapacity];
    pipeCapacity = initPipeCapacity;
    pipeOffset = 0;
    pipeSize = 0;
}

//Terminal targeted pipe initialization function
void fileSession::pipeDataObj::init(textOutputDevice *dev)
{
    pipeSpecific.dev = dev;
}

//Cleanup for non-terminal pipe
void fileSession::pipeDataObj::cleanup(void)
{
    delete[] pipeSpecific.buffer;
}

/*! Read from a pipe object
 *  \param sflags Session flags indicating pipe type
 *  \param tBuffer Target buffer to read data into
 *  \param count Number of bytes to attempt to read
 *  \param bytesRead Returns number of bytes actually read
 *  \return fsValid if bytes were read or the pipe was empty, fsInvalidOffset if the provided offset was a non-zero number
 */
fsResult fileSession::pipeDataObj::read(const uint32_t sflags, void *tBuffer, const size_t count, ssize_t &bytesRead)
{
    //Wait for data if the pipe is empty and connected to a terminal
    while(!pipeSize && (sflags & sessionTerm))
    {
        localState::getSched()->sleepCurrent(300);
    }

    ssize_t readSize = min(count, pipeSize);

    ssize_t firstCopySize, firstOffset;

    uint8_t *destBuffer = reinterpret_cast <uint8_t *> (tBuffer);
    if(pipeSize > pipeOffset)
    {
        //The read starts at the back of the pipe
        firstCopySize = min(pipeSize - pipeOffset, readSize);
        firstOffset = pipeCapacity - firstCopySize;

        if(firstCopySize < readSize)
        {
            //Read any bytes that have wrapped around the end of the buffer into the back of the target buffer
            ssize_t diff = readSize - firstCopySize;
            Memory::copy(&destBuffer[firstCopySize], pipeSpecific.buffer, diff );
            pipeSize -= diff;
        }
    }
    else
    {
        //No wrap, just one read behind the offset
        firstCopySize = readSize;
        firstOffset = pipeOffset - pipeSize;
    }

    //Read from the begging of the pipe
    pipeSize -= firstCopySize;
    Memory::copy(destBuffer, &pipeSpecific.buffer[firstOffset], firstCopySize);

    bytesRead = readSize;

    return fsValid;
}

/*! Writes data to a pipe object
 *  \param sflags Flags of the file session
 *  \param buffer Buffer to output
 *  \param count Number of bytes to attempt to write
 *  \param bytesWritten Returns the number of bytes actually written
 *  \return fsValid if the call is successful, fsInvalidOffset if a non-zero offset is provided
 */
fsResult fileSession::pipeDataObj::write(const uint32_t sflags, const void *buffer, const size_t count, ssize_t &bytesWritten)
{
    const uint8_t *byteBuffer = reinterpret_cast <const uint8_t *> (buffer);

    if((sflags & (sessionTerm | sessionPipeDevOut)) == (sessionTerm | sessionPipeDevOut))
    {
        //Terminals get special treatment
        size_t spanStart = 0;
        size_t spanLength = 0;

        for(size_t index = 0; index < count; index++)
        {
            if(!byteBuffer[index])
                break;
            if((' ' <= byteBuffer[index]) && (byteBuffer[index] <= 126))
                spanLength++;
            else
            {
                //Handle escape characters
                if(spanLength)
                    pipeSpecific.dev->writeBuffer(&byteBuffer[spanStart], spanLength);
                switch(byteBuffer[index])
                {
                    case 0xD:
                        pipeSpecific.dev->preformOp(streamOpCarriageReturn);
                        break;
                    case 0xA:
                        pipeSpecific.dev->preformOp(streamOpNewLine);
                        break;
                    case 0x9:
                        pipeSpecific.dev->preformOp(streamOpTab);
                        break;
                    case 0x8:
                        pipeSpecific.dev->preformOp(streamOpBackspace);
                        break;
                    default:
                        break;
                }

                spanLength = 0;
                spanStart = index+1;
            }
        }

        //Write any visible characters
        if(spanLength)
            pipeSpecific.dev->writeBuffer(&byteBuffer[spanStart], spanLength);


        pipeSpecific.dev->preformOp(streamOpFlush);
        bytesWritten = count;
        return fsValid;
    }
    else
    {
        //Calculate the bytes to write after and before the current pipe offset
        ssize_t writeBytes = min(count, pipeCapacity - pipeSize );
        size_t trailing = min(writeBytes, pipeCapacity - pipeOffset );
        size_t wrap = writeBytes - trailing;

        //Write bytes after the current offset
        Memory::copy( &pipeSpecific.buffer[pipeOffset], byteBuffer, trailing);
        pipeOffset += trailing;
        pipeOffset %= PIPE_DEFAULT_SIZE;

        if(wrap)
        {
            //Copy any remaining bytes to the beginning of the pipe
            Memory::copy( pipeSpecific.buffer, &byteBuffer[trailing], wrap );
            pipeOffset += wrap;
            pipeOffset %= PIPE_DEFAULT_SIZE;
        }

        pipeSize += writeBytes;
        bytesWritten = writeBytes;
        return fsValid;
    }
}

/*! Closes the pipe
 *
 * \return fsValid always
 */
fsResult fileSession::pipeDataObj::close(void)
{
    return fsValid;
}

/* fsDataObj internal class */

void fileSession::fsDataObj::init(fileSystem *initFs)
{
    // Attach the filesystem
    fs = initFs;
}

void fileSession::fsDataObj::cleanup(fileSession *sess)
{
    //Close the file if necessary
    fs->close(sess);
}

/*! Verifies the validity of the seek option
 *  \param pos Seek base indicator
 *  \param current Current absolute offset
 *  \param offset Offset from the base
 *  \param absOffset Absolute offset return
 *  \return fsValid if the provided offset was valid, fsInvalidOffset if it was not
 */
fsResult fileSession::fsDataObj::verifySeek(const seekPos pos, const off_t current, const off_t offset, off_t &absOffset)
{
    off_t newOffset = 0;
    //Calculate the requested new offset
    switch(pos)
    {
        case seekBegin:
            newOffset = offset;
            break;
        case seekEnd:
            newOffset = eof + offset;
            break;
        case seekCurrent:
            newOffset = current + offset;
            break;
    }

    //Check if the newly calculated offset is an invalid value
    if((newOffset > eof) || (newOffset < 0))
    {
        kprintf("Invalid seek request encountered!\n");
        absOffset = current;
        return fsInvalidOffset;
    }
    else
        absOffset = newOffset;

    return fsValid;
}

/*! Reads from the attached filesystem object
 *  \param buffer Buffer to read bytes into
 *  \param offset Offset in the file for the requested read
 *  \param count Number of bytes to attempt to read
 *  \param bytesRead Number of bytes that were actually read
 *  \return fsValid if successful, other errors if the filesystem encountered an error \sa fsResult
 */
fsResult fileSession::fsDataObj::read(void *buffer, const size_t offset, const size_t count, ssize_t &bytesRead)
{
    fsResult result = fs->read(data, offset, count, buffer, bytesRead);
    return result;
}

/*! Writes to the attached filesystem object
 *  \param buffer Buffer to write out
 *  \param offset Offset into the file to write to
 *  \param count Number of bytes to attempt to write
 *  \param bytesWritten Returns the number of bytes actually written
 *  \param fsValid if successful, other values if an error is encountered \sa fsResult
 */
fsResult fileSession::fsDataObj::write(const void *buffer, const size_t offset, const size_t count, ssize_t &bytesWritten)
{
    return fs->write(data, offset, count, buffer, bytesWritten);
}

/*! Closes the file
 *  \param sess File session object to be closed
 *  \return fsVAlid if successful, other values if an error is encountered \sa fsResult
 */
fsResult fileSession::fsDataObj::close(fileSession *sess)
{
    return fs->close(sess);
}
