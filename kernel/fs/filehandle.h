#ifndef FILE_HANDLE_H_
#define FILE_HANDLE_H_

#include "filesession.h"

class fileHandle
{
	public:

		fileHandle(shared_ptr<fileSession> &initSession);
        ~fileHandle() = default;

		fileHandle(const fileHandle &other);
		fileHandle &operator =(const fileHandle &other);

		fsResult close(void);
		fsResult read(void *buffer, size_t amount, ssize_t &bytesRead);
		fsResult write(const void *buffer, size_t amount, ssize_t &bytesWritten);
		fsResult seek(const seekPos pos, size_t offset, off_t *ret = nullptr);
		fsResult validateOffset(const seekPos pos, size_t offset);

		fsResult seekAndReadAll(const seekPos pos, const off_t offset, void *buffer, size_t amount);
		fsResult seekAndWriteAll(const seekPos pos, const off_t offset, void *buffer, size_t amount);

		int isTty(void)
		{
		    return session->isTty();
		}
	private:
		uint64_t currentOffset;
		uint64_t generation;
		shared_ptr<fileSession> session;
};


#endif // FILE_HANDLE_H_
