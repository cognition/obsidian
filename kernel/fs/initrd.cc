#include "initrd.h"
#include <memory/memory.h>
#include <io/io.h>
#include <exec/errno.h>
#include "vfs.h"

static initRamdiskFs *initrd = NULL;

static const union
{
	char characters[8];
	uint64_t value;
} initRdMagicU = { {'O', 'B', 'S', ' ', 'I', 'M', 'G', ' '} };

//initRdDirhandle class methods
initRdDirHandle::initRdDirHandle(const stringFragment &initPath, class initRamdiskFs *initFs, const rawEntry *entry) : directoryHandle(initPath, initFs)
{
    data = const_cast <rawEntry *> (entry);
}

rawEntry *initRdDirHandle::getRawEntry(void) const
{
    return reinterpret_cast <rawEntry *>(data);
}

//initRamdiskFs class methods

initRamdiskFs::initRamdiskFs(const uintptr_t imageVirtualAddress, const uint32_t memLength, const imageFileHeader *imgHeader) :
	virt(imageVirtualAddress), imageLength(memLength), header(imgHeader),
	rootEntries(reinterpret_cast <rawEntry *> (virt + sizeof(imageFileHeader) + imgHeader->stringTableSize) ),
	stringTable(reinterpret_cast <char *> (virt + sizeof(imageFileHeader))),
	rootEntryCount(imgHeader->rootDirSize / sizeof(rawEntry))
{
}

initRamdiskFs *initRamdiskFs::mapFromMem(const uint32_t physicalAddress, const uint32_t length)
{
    kprintf("Mapping INITRD...\n");
	const uintptr_t initRdVirtAddress = virtualMemoryManager::mapRegion(physicalAddress, length, PAGE_PRESENT);
	//Check if the mapping failed
	if(!initRdVirtAddress)
		return NULL;

	//Verify the initrd image header
	const imageFileHeader *initRdHeader = reinterpret_cast <imageFileHeader *> (initRdVirtAddress);
	if( (initRdHeader->magic != initRdMagicU.value) || (initRdHeader->imageSize != length) )
	{
		kprintf("INIT: Error - Initial ramdisk image is corrupted or unusable!\n");
		return NULL;
	}

	/*
	asm volatile ("cli\n"
                  "hlt\n"::);
    */
	initRamdiskFs *rd = new initRamdiskFs(initRdVirtAddress, length, initRdHeader);
	return rd;
}

/*!
 * Retrieves a string pointer from the string table of the image
 * \param index Index of strings first character
 * \return Pointer to the string, or NULL if an invalid offset was passed
 */
const char *initRamdiskFs::getStringFromTable(const uint32_t index) const
{
	if(index >= header->stringTableSize)
		return NULL;
	else
		return &stringTable[index];
}

const rawEntry *initRamdiskFs::matchEntry(const rawEntry *start, const uint32_t entryCount, String &entryName) const
{
	const rawEntry *ptr = start;
	uint32_t count = entryCount;

	while(count)
	{
		const char *name = getStringFromTable( ptr->getNameOffset() );

		if(!name)
		{

			return NULL;
		}
		else if(entryName == name)
			return ptr;
		ptr++;
		count--;
	}

	return NULL;
}

const rawEntry *initRamdiskFs::findEntry(const char *path) const
{
	String pathString(path);
	const rawEntry *entries = rootEntries;
	uint32_t count = rootEntryCount;
	Vector<String> names = pathString.splitToken('/');

	//Crawl the directory tree
	for(uint32_t i = 0; i < names.length();)
	{
		const rawEntry *result = matchEntry(entries, count, names[i]);
		if(!result)
			break;
		count = result->getLength() / sizeof(rawEntry);
		i++;
		//Check if we've actually found the entry, if not continue crawling
		if(i == names.length())
			return result;
		else
			entries = reinterpret_cast <const rawEntry *> (virt + result->getOffset() );
	}

	return NULL;
}

fileHandle *initRamdiskFs::open(const String &path, uint8_t COMPILER_UNUSED permissions)
{
	fileSession *initSession = new fileSession(path, this);

	fileSession *session = activeFiles.findOrInsert( initSession );
    //fileSession *session = initSession;

	if(session != initSession)
		delete initSession;

	if( !session->getData() )
	{
		const rawEntry *dirEntry = findEntry(path.toBuffer());
		if(!dirEntry)
		{
			kprintf("Error: Unable to find directory entry for file: %s\n", path.toBuffer() );
			return NULL;
		}
		else if(dirEntry->isFile() )
		{
			const uint32_t totalOffset = dirEntry->getOffset() + dirEntry->getLength();
			if( totalOffset > imageLength )
				return NULL;
			session->setData( const_cast <rawEntry *> (dirEntry) );
			session->setEOF(dirEntry->getLength());

		}
		else
		{
            //Entry is not a file
            return NULL;
		}
	}
	shared_ptr<fileSession> sessionShared(session);
	fileHandle *handle = new fileHandle(sessionShared );
	return handle;
}

fsResult initRamdiskFs::read(void *data, const uint64_t offset, const uint64_t count, void *buffer, ssize_t &bytesRead)
{
    const rawEntry *entry = reinterpret_cast <const rawEntry *> ( data );
	uintptr_t addr = virt + entry->getOffset() + offset;
	if( (offset+count) > entry->getLength() )
    {
        ssize_t maximumRead = entry->getLength() - offset;
        if(maximumRead >= 0)
            bytesRead = maximumRead;
        else
        {
            bytesRead = 0;
            return fsInvalidCount;
        }

        Memory::copy(buffer, reinterpret_cast <const void *> (addr), maximumRead);

        return fsValid;
    }

    //kprintf("INITRD - Read - SRC: %X DEST: %X Size: %u\n", addr, buffer, count);
    Memory::copy(buffer, reinterpret_cast <const void *> (addr), count );
    bytesRead = count;
	return fsValid;
}

fsResult initRamdiskFs::close(fileSession *session)
{
	activeFiles.remove(*session);
	return fsValid;
}

fsResult initRamdiskFs::write(COMPILER_UNUSED void *data, COMPILER_UNUSED const uint64_t offset,
                                           COMPILER_UNUSED const uint64_t count, COMPILER_UNUSED const void *buffer, COMPILER_UNUSED ssize_t &bytesWritten)
{
	return fsReadOnly;
}

int initRamdiskFs::openDirectory(const stringFragment &path, directoryHandle *&retHandle)
{
    const rawEntry *entry = findEntry(path.toBuffer() );
    if(!entry)
        return ENOENT;
    else if(entry->isDir())
    {
        retHandle = new initRdDirHandle(path, this, entry);
        return 0;
    }
    else
        return ENOTDIR;
}

directoryResult initRamdiskFs::getDirectoryEntry(directoryHandle *dir, const uint64_t index, directoryEntryInfo *&info)
{
    initRdDirHandle *dh = static_cast <initRdDirHandle *> (dir);

    const rawEntry *dirEntry = dh->getRawEntry();
    uint32_t maxSubEntry = dirEntry->getMaxSubEntry();

    if(index >= maxSubEntry)
        return dirResultNotFound;

    const char *rawEntryString = getStringFromTable(dirEntry->getNameOffset() );

    if(!rawEntryString )
        return dirResultCorrupt;

    info = new directoryEntryInfo(String(rawEntryString), 0);

    return dirResultSuccess;
}

namespace initRd
{
	bool initialize(const uint32_t physicalAddress, const uint32_t length)
	{
		initrd = initRamdiskFs::mapFromMem(physicalAddress, length);
		String initRdId("/initrd");
		if(!initrd)
			return false;
		else
			return (Vfs::mount(initrd, initRdId)  == Vfs::mountSuccess);
	}

	bool fileExists(const char *name)
	{
		const rawEntry *entry = initrd->findEntry(name);
		return (entry != NULL);
	}

	fileHandle *open(String &path, uint8_t permissions)
	{
		return initrd->open(path, permissions);
	}
}
