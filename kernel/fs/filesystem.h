#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include <templates/radixtree.cc>
#include <cpu/lock.h>
#include <io/io.h>

//File access flags
#define FILE_READ           0x1
#define FILE_WRITE          0x2
#define FILE_EXEC           0x4


enum seekPos
{
    seekCurrent,
    seekEnd,
    seekBegin
};


enum fsResult
{
    fsValid = 0,
    fsIoError,
    fsStateError,
    fsAccessError,
    fsInvalidOffset,
    fsInvalidCount,
    fsOldGeneration,
    fsReadOnly,
    fsWriteOnly,
    fsIsPipe
};

enum directoryResult
{
    dirResultSuccess = 0,
    dirResultNotFound,
    dirResultAccess,
    dirResultCorrupt,
    dirResultUpdated
};


class directoryEntryInfo
{
    public:
        directoryEntryInfo(const String &initName, const uint64_t entryFSN);
        ~directoryEntryInfo();

        String getName(void) const
        {
            return name;
        }

        uint64_t getFSN(void) const
        {
            return fsn;
        }
    protected:
        uint64_t fsn;
        String name;
};

class directoryHandle
{
    public:
        virtual ~directoryHandle() = default;
        directoryResult getDirectoryEntry(directoryEntryInfo *&info);
        void rewind(void);
        void reset(void);
    protected:
        directoryHandle(const stringFragment &initPath, class fileSystem *initFs);

        void *data;
        class fileSystem *fs;
        const stringFragment path;
        off_t entryNum;
        int64_t version;
};

class fileSystem
{
	public:
		fileSystem(void);
		virtual ~fileSystem();
		void setMountPoint( const String &mountPath);
		const String &toString(void) const;
		virtual class fileHandle *open(const String &path, const uint8_t permissions) = 0;
		virtual int openDirectory(const stringFragment &path, directoryHandle *&retHandle) = 0;
		virtual directoryResult getDirectoryEntry(directoryHandle *dir, const uint64_t index, directoryEntryInfo *&info) = 0;
		virtual fsResult read(void *data, const uint64_t offset, const uint64_t count, void *buffer, ssize_t &bytesRead) = 0;
		virtual fsResult write(void *data, const uint64_t offset, const uint64_t count, const void *buffer, ssize_t &bytesWritten) = 0;
		virtual fsResult close(class fileSession *session) = 0;
	protected:
		String mountPoint;
		radixTree<fileSession> activeFiles;
};

#endif
