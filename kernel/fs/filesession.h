#ifndef FILE_SESSION_H_
#define FILE_SESSION_H_

#include "filesystem.h"
#include <io/textoutdev.h>

class fileSession
{
    private:
        class fsDataObj
        {
            public:
                void init(fileSystem *initFs);
                void cleanup(fileSession *sess);

                void setEOF(const uint64_t eofValue)
                {
                    eof = eofValue;
                }

                uint64_t getEOF(void) const
                {
                    return eof;
                }


                void *getData(void)
                {
                    return data;
                }

                void setData(void *d)
                {
                    data = d;
                }

                fsResult read(void *buffer, const size_t offset, const size_t count, ssize_t &bytesRead);
                fsResult write(const void *buffer, const size_t offset, const size_t count, ssize_t &bytesWritten);
                fsResult close(fileSession *sess);
                fsResult verifySeek(const seekPos pos, const off_t current, const off_t offset, off_t &absOffset);
            private:
                class fileSystem *fs;
                void *data;
                uint64_t eof;
        };

        class pipeDataObj
        {
            public:

                void init(textOutputDevice *dev);
                void init(const uint32_t initPipeCapacity);
                void cleanup(void);

                fsResult read(const uint32_t sflags, void *buffer, const size_t count, ssize_t &bytesRead);
                fsResult write(const uint32_t sflags, const void *buffer, const size_t count, ssize_t &bytesWritten);
                fsResult close(void);

            private:
                union
                {
                    textOutputDevice *dev;
                    unsigned char *buffer;
                } pipeSpecific;
                uint32_t pipeCapacity;
                volatile uint32_t pipeSize, pipeOffset;

        };

        union fileTypeSpecific
		{
		    pipeDataObj pipeData;
		    fsDataObj fsData;
		};


	public:
        static const uint32_t fileRead          = 0x01U;
        static const uint32_t fileWrite         = 0x02U;
        static const uint32_t fileAppend        = 0x04U;
        static const uint32_t fileTruncate      = 0x08U;
        static const uint32_t fileCreate        = 0x10U;


	    static const uint32_t sessionFsFile     = 0x1U;
	    static const uint32_t sessionPipe       = 0x2U;
        static const uint32_t sessionReadOnly   = 0x4U;
        static const uint32_t sessionWriteOnly  = 0x8U;
        static const uint32_t sessionEcho       = 0x10U;

	    static const uint32_t sessionPipeDevOut = 0x100U;
	    static const uint32_t sessionPipeDevIn = 0x100U;
	    static const uint32_t sessionTerm = 0x10000U;

		fileSession(shared_ptr<fileTypeSpecific> &ts, const uint32_t pipeFlags);
		fileSession(textOutputDevice *dev, const uint32_t typeFlags);
		fileSession(const String &path, class fileSystem *parentFs);
		~fileSession();
		const String &toString(void) const;
		uint64_t getGeneration(void) const;
		void *getData(void) const;
		void setData(void *value);

		fsResult close(void);
		fsResult read(void *buffer, const size_t offset, const size_t count, const uint64_t gen, ssize_t &bytesRead);
		fsResult write(const void *buffer, const size_t offset, const size_t amount, const uint64_t gen, ssize_t &bytesWritten );
		fsResult verifySeek(const seekPos pos, const off_t current, const off_t offset, off_t &absOffset);
        void setEOF(uint64_t value);

		int isTty(void);

		static bool createPipe(shared_ptr<fileSession> &reader, const uint32_t readerFlags, shared_ptr<fileSession> &writer, const uint32_t writerFlags, const uint32_t pipeSize);
	private:
        String fileName;
		ticketLock writeLock;
		uint32_t activeInstances, flags;
		uint64_t generation;
        shared_ptr<fileTypeSpecific> typeSpecific;
};


#endif // FILE_SESSION_H_
