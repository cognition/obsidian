#include "filehandle.h"

/*! Constructor.  Attaches a fileHandle to a fileSession
 *  \param initSession Session to attach to the fileHandle object
 */
fileHandle::fileHandle(shared_ptr<fileSession> &initSession) : generation( initSession->getGeneration() ), session(initSession)
{
}

/*! Close operation.  Attempts to close a fileHandle object
 *  \return fsResult value indicating success or a failure reason for the operation
 */
fsResult fileHandle::close(void)
{
	return session->close();
}

/*! Read operation.  Reads bytes from an active file session and write them to a buffer of a specified size
 *  \param buffer Target buffer for data
 *  \param amount Number of bytes to attempt to read
 *  \param bytesRead Return reference for the number of bytes actually read
 *  \return A fsResult value indicating success or a failure reason for the operation
 */
fsResult fileHandle::read(void *buffer, size_t amount, ssize_t &bytesRead)
{
    fsResult res = session->read(buffer, currentOffset, amount, generation, bytesRead);
    if(res == fsValid)
        currentOffset += bytesRead;
    return res;
}

/*! Write operation.  Writes bytes out to a file given at the current position
 *  \param buffer Data to be output to the file
 *  \param amount Size of the output buffer
 *  \return A fsResult value indicating success or the failure reason for the operation
 */

fsResult fileHandle::write(const void *buffer, size_t amount, ssize_t &bytesWritten)
{
	fsResult res = session->write(buffer, currentOffset, amount, generation, bytesWritten);
    if(res == fsValid)
        currentOffset += bytesWritten;
    return res;
}

/*! Seek operation.  Updates the current file position pointer of the file handle
 *  \param pos Relative position for seek operation
 *  \param offset Offset from relative position in bytes
 *  \return fsResult value indicating success of the failure reason for the operation
 */
fsResult fileHandle::seek(const seekPos pos, size_t offset, off_t *retPtr)
{
	fsResult result = session->verifySeek(pos, currentOffset, offset, currentOffset);
	if(retPtr)
        *retPtr = currentOffset;
    return result;
}

fsResult fileHandle::validateOffset(const seekPos pos, size_t offset)
{
    off_t discard = 0;
    return session->verifySeek(pos, currentOffset, offset, discard);
}

fsResult fileHandle::seekAndReadAll(const seekPos pos, const off_t offset, void *buffer, size_t amount)
{
    fsResult result = seek(pos, offset);

    ssize_t bytesRead;
    if(result != fsValid)
        return result;
    result = read(buffer, amount, bytesRead);

    if(result != fsValid)
        return result;

    if(bytesRead != amount)
        return fsIoError;

    return fsValid;
}

fsResult fileHandle::seekAndWriteAll(const seekPos pos, const off_t offset, void *buffer, size_t amount)
{
    fsResult result = seek(pos, offset);

    ssize_t bytesWritten;

    if(result != fsValid)
        return result;
    result = write(buffer, amount, bytesWritten);

    if(result != fsValid)
        return result;

    if(bytesWritten != amount)
        return fsIoError;

    return fsValid;

}
