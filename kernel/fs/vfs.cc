#include "vfs.h"
#include <templates/radixtree.cc>
#include <io/io.h>
#include <exec/errno.h>

static radixTree<fileSystem> mountTree;

namespace Vfs
{
    int openDir(const String &path, directoryHandle *&retHandle)
    {
        int index = 0;
        stringFragment search(path);
		fileSystem *fs = mountTree.findBestMatch(search, '/', index);
		if(!fs)
            return ENOENT;
        else
            return fs->openDirectory(stringFragment(path, index), retHandle);

    }

	/*! Attempts to open a file
	 *  \param path Absolute file path
	 *  \param permissions Requested file handle permissions (read, write, etc.)
	 *  \return A pointer to a valid fileHandle object, or NULL if the file could not be opened
	 */
	fileHandle *open(const String &path, const uint8_t permissions)
	{
		int index = 0;
		stringFragment search(path);
		fileSystem *fs = mountTree.findBestMatch(search, '/', index);
		if(!fs)
        {
            kprintf("Unable to find root file system!\n");
			return nullptr;
        }
		else
		{
		    String relPath = path.substr(index, path.length() - index);
			return fs->open(relPath, permissions);
		}
	}

	/*! Attempts to mount a file system
	 *  \param fs A pointer to the file system that is attempting to be mounted
	 *  \param mountPath Absolute path that the fileSystem will be mounted at
	 */
	mountResult mount(fileSystem *fs, const String &mountPath)
	{
		int index = 0;
		fs->setMountPoint(mountPath);
		fileSystem *present = mountTree.findBestMatch(stringFragment(mountPath), '/', index);
		if(present)
		{
				//! \todo Implement nested fs mounting
				kprintf("Error: Nested mounting NYI!\n");
				return mountExists;
		}
		else
		{
			mountTree.findOrInsert(fs);
			return mountSuccess;
		}
	}

	/*! Attempts to unmount the file system behind a given path
	 *  \param mountPath Path the pointing the mounted file system
	 *  \return Pointer to the fileSystem object that unmounted, or NULL if the file system could not be unmounted
	 */
	fileSystem *unmount(const String &mountPath)
	{
		return mountTree.remove(mountPath);
	}
}
