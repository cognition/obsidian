#ifndef FS_VFS_H_
#define FS_VFS_H_

#include "filesystem.h"
#include "filehandle.h"

namespace Vfs
{
	enum mountResult
	{
		mountSuccess,
		mountPermission,
		mountUnreachable,
		mountExists
	};

	int openDir(const String &path, directoryHandle *&retHandle);
	fileHandle *open(const String &path, const uint8_t permissions);
	mountResult mount(fileSystem *fs, const String &mountPath);
	fileSystem *unmount(const String &mountPath);
}

#endif

