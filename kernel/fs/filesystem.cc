#include "filesystem.h"

//directoryEntryInfo class methods
directoryEntryInfo::directoryEntryInfo(const String &initName, const uint64_t entryFSN) : fsn(entryFSN), name(initName)
{
}

//directoryHandle class methods
directoryHandle::directoryHandle(const stringFragment &initPath, class fileSystem *initFs) : fs(initFs), path(initPath), entryNum(0)
{
}

directoryResult directoryHandle::getDirectoryEntry(directoryEntryInfo *&info)
{
    return fs->getDirectoryEntry(this, entryNum, info);
}

void directoryHandle::rewind(void)
{
    if(entryNum)
        entryNum--;
}

void directoryHandle::reset(void)
{
    entryNum = 0;
}


//fileSystem class methods
fileSystem::fileSystem()
{
}

fileSystem::~fileSystem()
{
}

void fileSystem::setMountPoint( const String &mountPath)
{
	mountPoint = mountPath;
}

const String &fileSystem::toString(void) const
{
	return mountPoint;
}
