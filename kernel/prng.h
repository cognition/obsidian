#ifndef PRNG_H_
#define PRNG_H_

#include <compiler.h>

class prngInstance
{
    public:
        prngInstance();
        void seed(const uint64_t value);
        uint64_t get(void);
    private:
        uint64_t state[4];
};

namespace random
{
    void seed(uint64_t value);
    uintptr_t get(void);
};

#endif // PRNG_H_
