#ifndef BASE_BOARD_H_
#define BASE_BOARD_H_

#include "device.h"

//! Object representing a generic root device, really only serves such a purpose
class baseBoard : public Device
{
	public:
		baseBoard(refSpaceObj *parentRef);
		virtual bool init( void *context);
};

#endif

