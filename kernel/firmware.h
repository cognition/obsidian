#ifndef SYSTEM_FIRMWARE_H_
#define SYSTEM_FIRMWARE_H_

#include "compiler.h"
#include "bootloader.h"

namespace Firmware
{
    bool doChecksum(void *data, uint32_t size);
    uintptr_t getAcpiRSDP(bootLoaderInfo *bli);
}

#endif
