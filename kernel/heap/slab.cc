#include "slab.h"
#include "cache.h"

//! Placement new
inline void *operator new(uintptr_t COMPILER_UNUSED size, uintptr_t addr)
{
	return reinterpret_cast <void *> (addr);
}

//heapSlab class

/*! Initial slab constructor
 *  \param slabOwner heapProcessorCache object which manages this slab
 */
heapSlab::heapSlab(class heapProcessorCache *slabOwner, const void *topCaller) : next(nullptr), prev(nullptr), blockCount(1),
    freeSpace(HEAP_SLAB_SIZE - (sizeof(heapSlab) + sizeof(heapBlock))), owner(slabOwner)
{
    uintptr_t addr = reinterpret_cast <uintptr_t> (this);
    hash = ~addr;
    heapBlock *initBlock = new (addr + sizeof(heapSlab)) heapBlock(freeSpace, 0, sizeof(heapSlab), HEAP_BLOCK_LAST);
    owner->registerSlab(this, initBlock, topCaller);
}


/*! Constructor
 *  \param blockSize Size of the requested block
 *  \param slabOwner heapProcessorCache object which will own the heapSlab object
 *  \param retBlock Return reference to a block which will be allocated upon slab creation
 */
heapSlab::heapSlab(const uint32_t blockSize, heapProcessorCache *slabOwner, heapBlock *&retBlock, const void *topCaller) : next(nullptr), prev(nullptr), blockCount(2),
	freeSpace(HEAP_SLAB_SIZE - (sizeof(heapSlab) + sizeof(heapBlock)*2 + blockSize)), owner(slabOwner)
{
	uintptr_t addr = reinterpret_cast <uintptr_t> (this);
	uintptr_t offset = sizeof(heapSlab) + sizeof(heapBlock) + blockSize;
	//Create the hash
	hash = ~addr;

	retBlock = new ( addr + sizeof(heapSlab) ) heapBlock(blockSize, 0, sizeof(heapSlab), 0);

	//Create the block for the remaining space in the slab
	heapBlock *block = new (addr + offset) heapBlock(freeSpace, blockSize + sizeof(heapBlock), offset, HEAP_BLOCK_LAST);
	//Register the slab and block with the cache
	owner->registerSlab(this, block, topCaller);
}

/*! Verifies that a slab object is valid
 *  \return True if the slab is valid, false if it is not
 */
bool heapSlab::verify()
{
    uintptr_t testHash = reinterpret_cast <uintptr_t> (this);
    testHash = ~testHash;
    return (hash == testHash);
}

/*! Retrieves a pointer to the cache which owns this block
 *  \return A pointer to the cache that owns this block
 */
heapProcessorCache *heapSlab::getOwner()
{
    return owner;
}
