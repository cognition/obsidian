#ifndef HEAP_PROCESSOR_CACHE_
#define HEAP_PROCESSOR_CACHE_

#include "block.h"
#include "slab.h"
#include "region.h"
#include "availabilityfield.h"

#define HEAP_CACHE_MINOR_FIELD_SIZE           (32)
#define HEAP_CACHE_MAJOR_FIELD_COUNT          (32)

#define HEAP_CACHE_MINOR_INDEX_MASK           0x1F

#define HEAP_CACHE_BLOCK_BINS                 (HEAP_CACHE_MAJOR_FIELD_COUNT*HEAP_CACHE_MINOR_FIELD_SIZE)

#define HEAP_CACHE_BLOCK_GRANULARITY          16
#define HEAP_CACHE_MIN_BLOCK_SIZE             HEAP_CACHE_BLOCK_GRANULARITY

#define HEAP_CACHE_BLOCK_SIZE_CEILING         (HEAP_CACHE_MINOR_FIELD_SIZE*HEAP_CACHE_MAJOR_FIELD_COUNT*HEAP_CACHE_BLOCK_GRANULARITY)

#define HEAP_FREE_SUCCESS                     1
#define HEAP_FREE_FAIL_INVALID                0

class heapProcessorCache
{
	public:
		heapProcessorCache();
		heapProcessorCache(void *initSlab);
		void *allocate(uintptr_t size, const void *topCaller = nullptr);
		int free(void *ptr, const void *topCaller = nullptr);
		void registerSlab(heapSlab *slab, heapBlock *block, const void *topCaller);
		void registerFreeBlock(heapBlock *block, const void *topCaller);

		static void setRedZoneEnd(const uintptr_t newEnd);

		uint64_t getSlabCount(void) const;
	private:
		//Type specific allocation functions
		heapBlock *allocateHeapBlock(uint32_t size, uint32_t index, const void *topCaller);
		void *allocateRegion(uintptr_t size, const void *topCaller);

		//Insertion/sorting functions
        void insertBlock(heapBlock *block, const void *topCaller);

		//Removal functions
		void removeBlock(heapBlock *block, uint32_t index, const void *topCaller);
        void blindRemoveBlock(heapBlock *block, const void *topCaller);

		// Subordinate object allocation functions
		heapBlock *allocateNewSlab(uint32_t size, const void *topCaller);

		//Internal utility functions
        void attemptBlockMerge(heapBlock *&block, heapSlab *slab, const void *topCaller);

		//! List of slabs owned by this cache
		heapSlab *slabs;
		uint64_t slabCount;
		//! A tree of active regions owned by this cache
		static avlTree<regionObj64> activeRegions;
		//! Lock for larger regions
		static ticketLock cacheRegionLock;

		//! Lock field for this heap cache object
		ticketLock cacheLock;
		//! Major bit field
		uint32_t majorField;
		//! Minor fields
		bitAvailabilityField32 minorFields[HEAP_CACHE_MAJOR_FIELD_COUNT];

		//! Array of pointers to blocks
		heapBlock *blocks[HEAP_CACHE_BLOCK_BINS];
};

#endif

