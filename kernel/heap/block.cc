#include "block.h"
#include <io/io.h>

//! Testing definition
#ifdef HEAP_DEEP_VALIDATE

#define assert(x)           if( !(x)) { \
                            kprintf("Assert \"%s\"failed @ %s:%s\n", #x, __FILE__, __LINE__);\
                            asm volatile ("cli\n" \
                                          "hlt\n"::); \
                             }

#else

#define assert(x)

#endif

//Placement new operator
inline void *operator new(uintptr_t COMPILER_UNUSED size, uintptr_t ptr)
{
	return reinterpret_cast <void *> (ptr);
}

//heapBlock class

//! Empty constructor
heapBlock::heapBlock()
{
}

/*! Default constructor
 *  \param blockSize Size of the block in bytes
 *  \param leftOffset Offset, from the slab base, of the block to the immediate left of this one
 *  \param slabOffset Offset of this block to the slab's base
 *  \param flagInit Initial flags

 */
heapBlock::heapBlock(const uint32_t blockSize, const uint32_t leftOffset, const uint32_t slabOffset, const uint32_t flagInit) :
	slab(slabOffset), size(blockSize), left(leftOffset)
{
	hash = ~reinterpret_cast <uintptr_t> (this);
	hash &= ~0xF;
	hash |= HEAP_BLOCK_FREE | flagInit;

	link[HEAP_BLOCK_LINK_PREV] = nullptr;
	link[HEAP_BLOCK_LINK_NEXT] = nullptr;
}

/*! Splits an existing block into two smaller ones
 *  \param newSize New size for the trailing block
 *  \return A pointer to the newly created block
 */
heapBlock *heapBlock::split(const uint32_t newSize)
{
    COMPILER_UNUSED uint32_t oldSize = size;
	uintptr_t addr = reinterpret_cast <uintptr_t> (this);
	uint32_t rem = size - (sizeof(heapBlock) + newSize);
	uintptr_t offset = rem + sizeof(heapBlock);

	//Sanity checks
	assert(rem >= 16);
	assert(newSize < size);


    //kprintf("S: %u NS: %u Hash: %X\n", size, newSize, hash);
	heapBlock *newBlock = new (addr + offset) heapBlock(newSize, offset, offset + slab, hash & HEAP_BLOCK_LAST);
	hash &= ~HEAP_BLOCK_LAST;
    if(!newBlock->isLast())
        newBlock->updateRightLink();

    size = rem;

	//Validation checks
	assert(newBlock->size == newSize);
	assert(newBlock->isFree());
	assert(isFree() );
	assert((newBlock->size + sizeof(heapBlock) + rem) == oldSize);



	return newBlock;
}

//! Updates the link between a block's immediate right neighbor and itself, if a right neighbor exists
void heapBlock::updateRightLink()
{

        heapBlock *rb = getRight();
        rb->left = size + sizeof(heapBlock);
}

/*! Verifies that this block's data is valid
 *  \return True if the block is valid, false if it is not
 */
bool heapBlock::verify()
{
    uint32_t testHash = reinterpret_cast <uintptr_t> (this);
    testHash = ~testHash;
    testHash ^= hash;
    testHash &= ~0xFUL;
    if(!testHash)
        return true;
    else
    {
        kprintf("Error hash mismatch - Found: %X expected: %X Block: %p\n", hash & ~0xFUL, testHash, this);
        return false;
    }
}

/*! Retrieves a pointer to the slab which contains this block
 *  \return A pointer to the slab that contains this block
 */
heapSlab *heapBlock::getSlab()
{
    uintptr_t addr = reinterpret_cast <uintptr_t> (this);
    addr -= slab;
    return reinterpret_cast <heapSlab *> (addr);
}
