#ifndef HEAP_BLOCK_H_
#define HEAP_BLOCK_H_

#include "compiler.h"
#include "slab.h"
#include <io/io.h>

#define HEAP_BLOCK_FREE             0x08
#define HEAP_BLOCK_LAST             0x04

#define HEAP_BLOCK_LINK_PREV		0
#define HEAP_BLOCK_LINK_NEXT		1

class COMPILER_PACKED heapBlock
{
	public:
		heapBlock();
		heapBlock(const uint32_t blockSize, const uint32_t leftOffset, const uint32_t slabOffset, const uint32_t flagInit);
		heapBlock *split(const uint32_t newSize);
		void updateRightLink();

        bool verify();
        heapSlab *getSlab();
		/*! Returns a pointer to the data area of the block
		 *  \return A pointer to the memory area of the block
		 */
		inline void *getPtr()
		{
            return reinterpret_cast <void *> (&link[0]);
		}

		//! Marks the block as in use
		inline void markUsed(void)
		{
			hash &= ~HEAP_BLOCK_FREE;
		}

        //! Marks the block as free
        inline void markFree(void)
        {
            hash |= HEAP_BLOCK_FREE;
            link[HEAP_BLOCK_LINK_PREV] = nullptr;
            link[HEAP_BLOCK_LINK_NEXT] = nullptr;
        }

        /*! Checks if a block is currently free
         *  \return True if the block is not in use, false if it is in use
         */
        inline bool isFree(void) const
        {
            return (hash & HEAP_BLOCK_FREE);
        }

        inline bool isLast(void) const
        {
            return (hash & HEAP_BLOCK_LAST);
        }

        //! Trashes the hash code and locality information for the block
        inline void trash(void)
        {
            hash = 0;
            size = 0;
            left = 0;
            slab = 0;
            link[HEAP_BLOCK_LINK_NEXT] = nullptr;
            link[HEAP_BLOCK_LINK_PREV] = nullptr;
        }

        inline uintptr_t getHash(void) const
        {
            return hash;
        }

        inline void flagLast(void)
        {
            hash |= HEAP_BLOCK_LAST;
        }

        heapBlock *getLeft(void)
        {
            uintptr_t leftAddr;
            leftAddr = reinterpret_cast<uintptr_t> (this) - left;
            return reinterpret_cast <heapBlock *> (leftAddr);
        }

        heapBlock *getRight(void)
        {
            uintptr_t rightAddr;
            rightAddr = reinterpret_cast <uintptr_t> (this) + size + sizeof(heapBlock);
            return reinterpret_cast <heapBlock *> (rightAddr);
        }

	protected:
		uint32_t hash, slab;
	public:
		//! Size of the block in bytes
		uint32_t size, left;
		heapBlock *link[];
};

#endif

