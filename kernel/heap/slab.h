#ifndef HEAP_SLAB_H_
#define HEAP_SLAB_H_

#include "compiler.h"

#define HEAP_SLAB_SIZE		0x200000
#define HEAP_SLAB_BINS		8

class COMPILER_PACKED heapSlab
{
	public:
        heapSlab(class heapProcessorCache *slabOwner, const void *topCaller);
		heapSlab(const uint32_t blockSize, class heapProcessorCache *slabOwner, class heapBlock *&retBlock, const void *topCaller);
		bool verify();
		class heapProcessorCache *getOwner();

		heapSlab *next, *prev;
	private:
		uint32_t blockCount, freeSpace, freeCount, usedCount;
		uintptr_t hash;
		class heapProcessorCache *owner;
};

#endif
