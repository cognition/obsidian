#ifndef BIT_AVAILABILITY_FIELD_H_
#define BIT_AVAILABILITY_FIELD_H_

#include <compiler.h>

class bitAvailabilityField32
{
	public:
		bitAvailabilityField32();
		bool findBestIndex(uint8_t &index);
		void addMask(const uint32_t mask);
		bool removeMask(const uint32_t mask);

	private:
		uint32_t availabilityField;
};

#endif
