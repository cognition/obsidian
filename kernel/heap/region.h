#ifndef HEAP_REGION_H_
#define HEAP_REGION_H_

#include <regionobj.h>
#include <templates/avltree.cc>

class heapRegion : public avlBaseNode<regionObj64>
{
    public:
        heapRegion(uintptr_t regionBase, uintptr_t regionLength);
};

#endif
