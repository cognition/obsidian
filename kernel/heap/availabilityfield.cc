#include "availabilityfield.h"
#include <cpu/bits.h>

//bitAvailabilityField32 class
bitAvailabilityField32::bitAvailabilityField32() : availabilityField(0)
{

}

bool bitAvailabilityField32::findBestIndex(uint8_t &index)
{
	uint32_t mask = 1;
	mask <<= index;
	if(mask & availabilityField)
		return true;
	else
	{
		uint32_t narrowMask = (~mask);
		narrowMask -= (mask - 1);
		uint32_t narrowField = availabilityField & narrowMask;
		if(narrowField)
			index = Bits::getLsb32(narrowField);
		else
			return false;
	}
	return true;
}

void bitAvailabilityField32::addMask(const uint32_t mask)
{
	availabilityField |= mask;
}

bool bitAvailabilityField32::removeMask(const uint32_t mask)
{
	availabilityField &= ~mask;
	return (!availabilityField);
}
