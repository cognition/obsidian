#include "operators.h"
#include <cpu/localstate.h>
#include <io/io.h>

static bool heapDbg = false;

void setHeapDebug(const bool value)
{
    heapDbg = value;
}

//! Global new and delete operator overloads
void *operator new(uintptr_t size)
{
    if(heapDbg)
    {
        kprintf("New called for %u bytes from %X\n", size, __builtin_return_address(0));
        asm volatile ("xchg %%bx, %%bx\n":: "a" (&heapDbg));
    }
    /*
    if(!size)
    {
        kprintf("New called for %u bytes from %X\n", size, __builtin_return_address(0));
        asm volatile ("cli\n"
                      "hlt\n"::);
    }
    */
    heapProcessorCache *cache = localState::getProcessorCache();
    void *ptr = cache->allocate(size, COMPILER_FUNC_CALLER);
    return ptr;
}

void *operator new[](uintptr_t size)
{
    if(heapDbg)
    {
        kprintf("New[] called for %u bytes from %X\n", size, COMPILER_FUNC_CALLER);
        asm volatile ("xchg %%bx, %%bx\n":: "a" (&heapDbg));
    }
    /*
    if(!size)
    {
        kprintf("New[] called for %u bytes from %X\n", size, COMPILER_FUNC_CALLER);
        asm volatile ("cli\n"
                      "hlt\n"::);
    }
    */
    heapProcessorCache *cache = localState::getProcessorCache();
    void *ptr = cache->allocate(size, COMPILER_FUNC_CALLER);
    return ptr;
}

void operator delete(void *ptr)
{
    heapProcessorCache *cache = localState::getProcessorCache();
    if( cache->free(ptr, COMPILER_FUNC_CALLER) == HEAP_FREE_FAIL_INVALID)
    {
        kprintf("Error freeing memory @ %X Caller: %X\n", ptr, COMPILER_FUNC_CALLER );
        asm volatile ("xchg %%bx, %%bx\n"::);
        asm volatile ("cli\n"
                      "hlt\n"::);
    }
}

void operator delete[](void *ptr)
{
    heapProcessorCache *cache = localState::getProcessorCache();
    if( cache->free(ptr, COMPILER_FUNC_CALLER) == HEAP_FREE_FAIL_INVALID)
    {
		kprintf("Error freeing memory @ %X Caller: %p\n", ptr, COMPILER_FUNC_CALLER );
        asm volatile ("xchg %%bx, %%bx\n"::);
        asm volatile ("cli\n"
                      "hlt\n"::);

    }
  //  else
       // kprintf("Delete[] called on %X by %X\n", ptr, __builtin_return_address(0));
}
