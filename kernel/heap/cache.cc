#include <memory/memory.h>
#include <cpu/bits.h>
#include "cache.h"
#include <io/io.h>

#define CALCULATE_HEAPBLOCK_INDEX(x)        (((x)/HEAP_CACHE_BLOCK_GRANULARITY)-1)
#define CALCULATE_MAJOR_INDEX(x)            ((x)/HEAP_CACHE_MINOR_FIELD_SIZE)
#define CALCULATE_MINOR_INDEX(x)            ((x)%HEAP_CACHE_MINOR_FIELD_SIZE)

#define HEAP_REGION_ROUNDED_SIZE            ((sizeof(heapRegion) + 0xFU) & ~0xFU)
#define HEAP_REGION_BLOCK_INDEX             (CALCULATE_HEAPBLOCK_INDEX(HEAP_REGION_ROUNDED_SIZE))


//! Testing definition
#ifdef HEAP_DEEP_VALIDATE

#define assert(x, y, z)           if( !(x)) { \
                            kprintf("Assert \"%s\"failed @ %s:%u Caller: %p Value: %X\n", #x, __FILE__, __LINE__, (z), (y));\
                            asm volatile ("cli\n" \
                                          "hlt\n"::); \
                             }

#else

#define assert(x, y, z)

#endif

extern "C" void *_end;
COMPILER_UNUSED static uintptr_t heapRedZoneBase = reinterpret_cast <uintptr_t> (&_end);
static uintptr_t heapRedZoneEnd = reinterpret_cast <uintptr_t> (&_end);


avlTree<regionObj64> heapProcessorCache::activeRegions;
ticketLock heapProcessorCache::cacheRegionLock;


void heapProcessorCache::setRedZoneEnd(const uintptr_t newEnd)
{
    heapRedZoneEnd = newEnd;
}

//heapProcessorCache class

//! Processor cache constructor
heapProcessorCache::heapProcessorCache() : slabs(nullptr), slabCount(0), majorField(0)
{
	//Clear the list arrays
	Memory::set(blocks, 0, HEAP_CACHE_BLOCK_BINS*sizeof(heapBlock *));
}

/*! Processor cache BSP constructor
 *  \param initSlab Memory area for the initial heapSlab object
 */
heapProcessorCache::heapProcessorCache(void *initSlab) : slabs(nullptr), slabCount(0), majorField(0)
{
	//Clear the list arrays
	Memory::set(blocks, 0, HEAP_CACHE_BLOCK_BINS*sizeof(heapBlock *));
	//Creat the initial slab
 	new (initSlab) heapSlab(this, this);
}

/*! Allocates a region of memory
 *  \param size Size of the region in bytes
 *  \return A pointer to the region of memory, or NULL if no memory is available
 */
void *heapProcessorCache::allocate(uintptr_t size, const void *topCaller)
{

	uintptr_t blockIndex;
	//Round up to the nearest 16 byte multiple
	size += 0xF;
	size &= ~0xFUL;

	assert( (heapRedZoneBase > reinterpret_cast<uintptr_t> (this)) || (heapRedZoneEnd <= reinterpret_cast<uintptr_t> (this)), this, topCaller );

	if(size < HEAP_CACHE_BLOCK_GRANULARITY)
    {
		heapBlock *block = allocateHeapBlock(size, 0, topCaller);

		void *ptr = block->getPtr();
        assert( block->size >= size, this, topCaller);
        assert( block->verify(), block, topCaller );
        assert( !block->isFree(), block, topCaller );
        assert( block->getSlab()->verify(), block, topCaller );
        //kprintf("HEAP - Allocated %u bytes @ %p\n", size, ptr);


        return ptr;
    }
	blockIndex = CALCULATE_HEAPBLOCK_INDEX(size);

    if(blockIndex >= HEAP_CACHE_BLOCK_BINS)
	{
        //Use the region allocator
		return allocateRegion(size, topCaller);
	}
	else
	{
		//Use the block allocator
		heapBlock *block = allocateHeapBlock(size, blockIndex, topCaller);

		void *ptr = block->getPtr();
        assert( block->size >= size, this, topCaller);
        assert( block->verify(), block, topCaller );
        assert( !block->isFree(), block, topCaller );
        assert( block->getSlab()->verify(), block, topCaller );
        //kprintf("HEAP - Allocated %u bytes @ %p\n", size, ptr);
        return ptr;
	}
}

/*! Frees an area of memory
 *  \param ptr Pointer to the area being freed
 *  \return HEAP_FREE_SUCCESS if successful or HEAP_FREE_FAIL_INVALID if the free request was invalid
 */
int heapProcessorCache::free(void *ptr, const void *topCaller)
{
    uintptr_t addr = reinterpret_cast <uintptr_t> (ptr);
	//Seems redundant but it's not suppose to be a incorrect to call delete with a null pointer
	if(addr < REGULAR_PAGE_SIZE)
    {
        kprintf("[DRVD]: Error bad free call from %p with value %p\n", COMPILER_FUNC_CALLER, ptr);
		return HEAP_FREE_SUCCESS;
    }

    assert( (heapRedZoneBase > addr) || (heapRedZoneEnd <= addr), this, topCaller );
    assert( addr > KERNEL_VSPACE_START, addr, topCaller );

    if((addr & PAGE_OFFSET_MASK) == 0)
    {
        //Check if the pointer is to active memory region
        regionObj64 testRegion(addr, 1);
        spinLockInstance regionLock(&cacheRegionLock);
        avlBaseNode<regionObj64> *node = activeRegions.retrieveNode(testRegion);
        if(node)
        {
            addr = reinterpret_cast <uintptr_t> (node);

            virtualMemoryManager::unmapRegion( node->data.getBase(), node->data.getLength(), true, true );
        }
    }

	//It should be just a normal heapBlock, verify it
	heapBlock *block = reinterpret_cast <heapBlock *> (addr - sizeof(heapBlock));
	if(block->verify())
	{
        assert(block->size >= HEAP_CACHE_BLOCK_GRANULARITY, block, topCaller);

        // Double free test
        assert( !block->isFree(), block, topCaller );

		heapSlab *blockSlab = block->getSlab();
		//Make sure the slab is valid
		if(!blockSlab->verify())
			return HEAP_FREE_FAIL_INVALID;

		heapProcessorCache *cache = blockSlab->getOwner();
        spinLockInstance lockInstance(&cache->cacheLock);

		block->markFree();
		//Attempt to merge the block with it's neighbors
		cache->attemptBlockMerge(block, blockSlab, topCaller);
		assert(block, blockSlab, topCaller);
		//Register the block with it's cache
		cache->registerFreeBlock(block, topCaller);
		return HEAP_FREE_SUCCESS;
	}
	else
	{
		kprintf("Failed to free block @ %X\n", block);
		return HEAP_FREE_FAIL_INVALID;
	}
}

/*! Registers a new slab with a heapProcessorCache object
 *  \param newSlab Slab to register
 *  \param block Free block
 */
void heapProcessorCache::registerSlab(heapSlab *newSlab, heapBlock *block, const void *topCaller)
{
    assert( (heapRedZoneBase > reinterpret_cast<uintptr_t> (newSlab)) || (heapRedZoneEnd <= reinterpret_cast<uintptr_t> (newSlab)), newSlab, topCaller );
	//Insert the new slab into the slab list
	newSlab->next = slabs;
    slabCount++;
	if(slabs)
		slabs->prev = newSlab;
	slabs = newSlab;

    while(block->size > HEAP_CACHE_BLOCK_SIZE_CEILING)
    {
        heapBlock *rem = block->split(HEAP_CACHE_BLOCK_SIZE_CEILING);
        insertBlock(rem, topCaller);
        //block = rem;
    }

    //Insert the new block
	insertBlock(block, topCaller);
}

/*! Registers a free block with the cache that should own it
 *  \param block Block to be registered
 */
void heapProcessorCache::registerFreeBlock(heapBlock *block, const void *topCaller)
{
    //kprintf("Freed %u bytes @ %p Caller %p\n", block->size, block->getPtr(), topCaller );
    insertBlock(block, topCaller);
}

/*! Allocates a heap block of a given size
 *  \param size Size of the block in bytes
 *  \param index Initial array index location to check
 *  \return Pointer to a heap block of the requested size
 */
heapBlock *heapProcessorCache::allocateHeapBlock(uint32_t size, uint32_t index, const void *topCaller)
{
	//Calculate initial index values
	uint8_t majorIndex, minorIndex;
	majorIndex = index/HEAP_CACHE_MINOR_FIELD_SIZE;
	minorIndex = index & HEAP_CACHE_MINOR_INDEX_MASK;

	//Create the initial major test mask
	uint32_t majorMask = 1;
	majorMask <<= majorIndex;
	spinLockInstance lockInstance(&cacheLock);

	//Check if the best fit minor field has any blocks in it
	if(majorField & majorMask)
	{
	    //If so, determine the best fit index, if there is one
		if( minorFields[majorIndex].findBestIndex(minorIndex) )
		{
			uint32_t netIndex = majorIndex*HEAP_CACHE_MINOR_FIELD_SIZE + minorIndex;
			heapBlock *block = blocks[netIndex];
            assert(block, &blocks[netIndex], topCaller);
            assert( (heapRedZoneBase > reinterpret_cast<uintptr_t> (block)) || (heapRedZoneEnd <= reinterpret_cast<uintptr_t> (block)), &blocks[netIndex], topCaller );
			// Test that the block is in the right bin, is free and isn't corrupted
			assert( CALCULATE_HEAPBLOCK_INDEX(block->size) == netIndex, netIndex, topCaller );
            assert( block->verify(), block, topCaller );
			assert( block->isFree(), block, topCaller );
			assert( block->getSlab()->verify(), block, topCaller );

			//asm volatile ("nop\n"::: "memory");
			removeBlock(block, netIndex, topCaller);

            //If there's enough excess space, split it off into a new block
			if( (block->size - size) >= (HEAP_CACHE_BLOCK_GRANULARITY + sizeof(heapBlock)) )
			{
				heapBlock *rem = block->split(size);
				insertBlock(block, topCaller);
				block = rem;
			}

            block->markUsed();
			return block;
		}
	}

	uint32_t narrowField = majorField;
	uint32_t narrowMask = ~majorMask - (majorMask - 1);

	narrowField &= narrowMask;
	if(narrowField)
	{
	    majorIndex = Bits::getLsb32(narrowField);
		minorIndex = 0;

		if(!minorFields[majorIndex].findBestIndex(minorIndex) )
        {
            kprintf("Error unable to find minor index!\n");
            asm volatile ("cli\n"
                          "hlt\n"::);
        }

		uint32_t netIndex = majorIndex*HEAP_CACHE_MINOR_FIELD_SIZE + minorIndex;
		heapBlock *block = blocks[netIndex];

		assert( (heapRedZoneBase > reinterpret_cast<uintptr_t> (block)) || (heapRedZoneEnd <= reinterpret_cast<uintptr_t> (block)), &blocks[netIndex], topCaller );
		removeBlock(block, netIndex, topCaller);

		if( (block->size - size) >= (HEAP_CACHE_BLOCK_GRANULARITY + sizeof(heapBlock)) )
		{

			heapBlock *rem = block->split(size);
			insertBlock(block, topCaller);
			block = rem;
		}

		//Sanity checks
		assert(block->verify(), block, topCaller);
		assert(block->isFree(), block, topCaller);
		assert(block->size >= size, block, topCaller);
        assert( block->getSlab()->verify(), block->getSlab(), topCaller );

        block->markUsed();
		return block;
	}
	else
		return allocateNewSlab(size, topCaller);
}

/*! Allocates an area of memory that is some multiple of the system's page size
 *  \param size Size of the requested memory area in bytes
 *  \return A pointer to the memory area requested
 */
void *heapProcessorCache::allocateRegion(uintptr_t size, const void *topCaller)
{
    uintptr_t regionPages;
    uintptr_t regionSize = size;
    regionSize += PAGE_OFFSET_MASK;
    regionSize &= ~PAGE_OFFSET_MASK;
    regionPages = regionSize/REGULAR_PAGE_SIZE;

    uintptr_t base = virtualMemoryManager::allocateRegion(regionPages, PAGE_PRESENT | PAGE_WRITE);

    heapBlock *regionBlock = allocateHeapBlock(HEAP_REGION_ROUNDED_SIZE, HEAP_REGION_BLOCK_INDEX, topCaller);
    heapRegion *region = new ( regionBlock->getPtr() ) heapRegion(base, regionSize);

    spinLockInstance lock(&cacheRegionLock);
    activeRegions.insertNode( region );

    return reinterpret_cast <void *> (base);
}


/*! Inserts a heapBlock into the appropriate segregated list
 *  \param block Block to be inserted
 */
COMPILER_NOINLINE void heapProcessorCache::insertBlock(heapBlock *block, const void *topCaller)
{
	//Calculate all the relevant indexes
	uint32_t fullIndex = CALCULATE_HEAPBLOCK_INDEX(block->size);
	uint32_t majorIndex = CALCULATE_MAJOR_INDEX(fullIndex);
	uint8_t minorIndex = CALCULATE_MINOR_INDEX(fullIndex);

	//Sanity checks
	assert(majorIndex < HEAP_CACHE_MAJOR_FIELD_COUNT, majorIndex, topCaller);
	assert(minorIndex < HEAP_CACHE_MINOR_FIELD_SIZE, majorIndex, topCaller);
	assert(fullIndex < HEAP_CACHE_BLOCK_BINS, majorIndex, topCaller);
	assert(block, fullIndex, topCaller);

	//Calculate destination minor and major masks
	uint32_t minorMask = 1;
	minorMask <<= minorIndex;

	uint32_t majorMask = 1;
	majorMask <<= majorIndex;

	//Retrieve the root pointer for the destination block list
    heapBlock *ptr = blocks[fullIndex];

    block->link[HEAP_BLOCK_LINK_NEXT] = ptr;

	//There is at least one block in the existing block list, link the new block with it
	if(ptr)
	{
		ptr->link[HEAP_BLOCK_LINK_PREV] = block;
	}
	assert(block, fullIndex, topCaller);
	//Place the new block at the head of the list
	blocks[fullIndex] = block;

	//Update the presence bitmaps
	minorFields[majorIndex].addMask(minorMask);
	majorField |= majorMask;
}

/*! Removes a heapBlock from the segregated list array
 *  \param block Block to be removed
 *  \param index Net index of the list in the list array
 */
COMPILER_NOINLINE void heapProcessorCache::removeBlock(heapBlock *block, uint32_t index, const void *topCaller)
{
	uint8_t majorIndex = CALCULATE_MAJOR_INDEX(index);
	uint8_t minorIndex = CALCULATE_MINOR_INDEX(index);
	//References, just to make things look cleaner
	heapBlock *prev = block->link[HEAP_BLOCK_LINK_PREV];
	heapBlock *next = block->link[HEAP_BLOCK_LINK_NEXT];

	//Circular linkage test
    assert(prev != block, block, topCaller);
    assert(next != block, block, topCaller);

    //Check if there's a block ahead of this one in the list
	if(prev)
	{
		//If there is link it to the block after this one
		prev->link[HEAP_BLOCK_LINK_NEXT] = next;
	}
	else
	{
        //Cache and block consistency test
        assert(block == blocks[index], block, topCaller);

		//If there is not, we have to update the list pointer in the array itself
		blocks[index] = next;
		//Check if the list is empty, if it is clear it's bit in the availability mask
		if(!next)
		{
			uint32_t minorMask = 1;
			minorMask <<= minorIndex;
			if( minorFields[majorIndex].removeMask(minorMask) )
			{
				uint32_t majorMask = 1;
				majorMask <<= majorIndex;
				majorMask = ~majorMask;
				majorField &= majorMask;
			}
		}
	}

	//Link the next block to the previous one, if there is a next block
	if(next)
		next->link[HEAP_BLOCK_LINK_PREV] = prev;

	//Clear the link fields
	block->link[HEAP_BLOCK_LINK_PREV] = nullptr;
	block->link[HEAP_BLOCK_LINK_NEXT] = nullptr;
}

/*! Allocates a new slab and allocates a heapBlock from it
 *  \param size Size of the block which will be allocated from the new slab
 *  \return Pointer to a heapBlock object of the requested size
 */
heapBlock *heapProcessorCache::allocateNewSlab(uint32_t size, const void *topCaller)
{
	heapBlock *block;
	void *space = reinterpret_cast <void *> (virtualMemoryManager::allocateRegion(HEAP_SLAB_SIZE/REGULAR_PAGE_SIZE, PAGE_PRESENT | PAGE_WRITE));
	new (space) heapSlab(size, this, block, topCaller);
	block->markUsed();
	return block;
}

/*! Attempts to merge a heapBlock with it's neighbors
 *  \param block The heap block to attempt the merge on
 *  \param slab The slab which the block belongs to
 */
COMPILER_NOINLINE void heapProcessorCache::attemptBlockMerge(heapBlock *&block, heapSlab *slab, const void *topCaller)
{
    //Check if there's an adjacent block after this one
    if(!block->isLast())
    {
        heapBlock *rb = block->getRight();

        //Check if the block is free
        if( rb->isFree() )
        {
            //Test for right block header corruptions
            assert( rb->verify(), rb, topCaller );

        	uint32_t nSize = block->size + rb->size + sizeof(heapBlock);
        	if(nSize <= HEAP_CACHE_BLOCK_SIZE_CEILING)
			{
				//Roll the right block's space into the block we're merging
				block->size = nSize;

                if(rb->isLast() )
                    block->flagLast();
                else
                {
                    //Update the block's locality links
                    block->updateRightLink();
                }
				//Unlink the right block
				blindRemoveBlock(rb, topCaller);

				rb->trash();
			}
        }
    }
    //Check if there's an adjacent block before this one
    if(block->left)
    {
        //Test for left block header corruptions
        heapBlock *lb = block->getLeft();

        //Check if the block is free
        if ( lb->isFree() )
        {
            assert( lb->verify(), lb, topCaller );

        	uint32_t nSize = block->size + lb->size + sizeof(heapBlock);
        	if(nSize <= HEAP_CACHE_BLOCK_SIZE_CEILING)
			{
				//Unlink the left block
				blindRemoveBlock(lb, topCaller);

                //Roll the merge block's space into the left block
				lb->size = nSize;

				if(block->isLast())
                    lb->flagLast();
                else
                {
                    //Update the block's locality links
                    lb->updateRightLink();
                }


				block->trash();

				block = lb;
			}
        }
    }
}

/*! Removes a block from the cache without initially knowing it's size
 * \param block Block to be removed
 */
void heapProcessorCache::blindRemoveBlock(heapBlock *block, const void *topCaller)
{
    assert(block->size >= HEAP_CACHE_BLOCK_GRANULARITY, block, topCaller);
    uint32_t index = CALCULATE_HEAPBLOCK_INDEX(block->size);

    removeBlock(block, index, topCaller);
}

uint64_t heapProcessorCache::getSlabCount(void) const
{
    return slabCount;
}
