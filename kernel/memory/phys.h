#ifndef PHYS_MEMORY_H_
#define PHYS_MEMORY_H_

#include <bootloader.h>
#include <device.h>
#include "region_allocator.h"
#include "memorybin.h"

#define MEMORY_BIN_COUNT        24

/*! Object which describes a bank of physical memory and provides physical memory operations including: \n
 *  \n
 *  Allocation of cache color specific pages. \n
 *  Allocation of least used page colors. \n
 *  Allocation of large pages. \n
 *  Allocation of large contiguous memory buffers. \n
 *  Freeing of pages. \n
 *  Partition and management physical memory. \n
 */
class physicalMemory : public Device, public memoryRegionAllocator
{
	public:
		physicalMemory( Device *p );
		bool init( void *context );

		static uintptr_t allocatePage();
		static uintptr_t allocateLargePage();
		static uintptr_t allocatePageBin( const uint32_t bin );
		static uintptr_t allocateRegion(size_t size, uint32_t align );
		static void reclaimPage( uintptr_t addr );
		static void reclaimLargePage( uintptr_t addr);
		static void reclaimRegion(uintptr_t base, size_t length);
		static void printUsageInfo(void);
        static uint64_t getMaxAddress(void);

	private:
		void registerRamRegion( uintptr_t base, uintptr_t length );

		uintptr_t instanceAllocateRegion( uintptr_t size, uint32_t align );
		uintptr_t instanceAllocatePageBin( const uint32_t bin );
		uintptr_t instanceAllocatePage();
		uintptr_t instanceAllocateLargePage();

		void instanceReclaimPage( uintptr_t addr );
        void instanceReclaimLargePage( uintptr_t addr);

		void registerBin( memoryBin *bin );
        void partitionAndInsertRegion(uintptr_t base, uintptr_t length);

		memoryBin *mapPageBins( uintptr_t &currentPhysEnd, uintptr_t &currentVirtEnd );

        void instancePrintUsageInfo(void);

        ticketLock physMemLock;
		//! Array of pageBin objects for each potential page color
		memoryBin *pageBins;
		//! Ordered list of memory bins for random allocation
		memoryBin *mostEligible;
		//! List of currently allocated contiguous memory buffers
		memoryRegionBin activeBuffers;

        uintptr_t allocatedPages;

        uint32_t largePageFixedMask;
        uint8_t largePageFixedIndex;
};

#endif

