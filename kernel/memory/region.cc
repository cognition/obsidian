#include "region.h"
#include "objcache.h"

//Canonical boundary
#define KERNEL_VSPACE_START     0xFFFF800000000000UL

//! Testing definition
#ifdef MEM_REGION_VALIDATE

#define assert(x)           if( !(x)) { \
                            kprintf("Assert \"%s\"failed @ %s:%u Caller: %p\n", #x, __FILE__, __LINE__, __builtin_return_address(0));\
                            asm volatile ("cli\n" \
                                          "hlt\n"::); \
                             }

#else

#define assert(x)

#endif

//memorySpan class
memorySpan::memorySpan(const uint64_t spanBase, const uint64_t spanBytes): base(spanBase), filler(0)
{
    uint64_t lengthBits = Bits::getMsb64(spanBytes);
    base |= lengthBits;
}

/*! Copy constructor
 *  \param other Other span object to copy
 */
memorySpan::memorySpan(const memorySpan &other) : base(other.base), filler(0)
{
}

memorySpan &memorySpan::operator =(const memorySpan &other)
{
    base = other.base;
    filler = 0;
    return *this;
}

bool memorySpan::operator <(const memorySpan &other) const
{
    return (base < other.base);
}

bool memorySpan::operator >(const memorySpan &other) const
{
    return (base > other.base);
}

bool memorySpan::operator ==(const memorySpan &other) const
{
    return (base == other.base);
}

//memoryRegion class
memoryRegion::memoryRegion(const uint64_t base, const uint64_t length) : avlBaseNode<memorySpan>(memorySpan(base, length))
{
    assert((base & 0x7FU) == 0);
}

//Halves the size of the region
memoryRegion *memoryRegion::halve(void)
{
    uint64_t lengthBits = data.base & 0x7FU;

    assert(lengthBits != 0);

    lengthBits--;
    data.base &= ~0x7FUL;
    uint64_t trueBase = data.base;
    data.base |= lengthBits;

    memoryRegion *tail = memoryCache::allocate(trueBase + getLength(), getLength());

    return tail;
}

/*  Initializes the regions state
 *  \param base Base address of the region
 *  \param length Length of the region in bytes
 */
void memoryRegion::init(const uint64_t base, const uint64_t length)
{
    assert(length && (length < (1UL<<48) ));
    //Placement new, reinitializes the AVL node's fields
    new (this) memoryRegion(base, length);
}

//Returns the region's length in bytes
uint64_t memoryRegion::getLength(void) const
{
    uint64_t length = 1;
    length <<= (data.base & 0x7FU);

    assert(length > 0);

    return length;
}

//Returns the region's base address
uint64_t memoryRegion::getBase(void) const
{
    uint64_t base = ~0x7FUL;
    base &= data.base;
    return base;
}

//Doubles the size of the region
void memoryRegion::stepAugmentLength(void)
{
    data.base++;
}

/*! Updates the link field
 */
void memoryRegion::setLink(memoryRegion *next)
{
    left = next;
}

//! Returns the value of the link field
memoryRegion *memoryRegion::getLink(void) const
{
    return static_cast <memoryRegion *> (left);
}

//Unlinks and resets the node's linkages while preserving the data fields
void memoryRegion::delink(void)
{
    left = nullptr;
    right = nullptr;
    reset();
}

//Completely resets all region data
void memoryRegion::clear(void)
{
    reset();
    data.base = 0;
    delink();
}

//memoryRegionBin class
memoryRegionBin::memoryRegionBin() : root(nullptr)
{
}

/*! Inserts a region into the bin
 *  \param span A unique region to insert into the bin
*/
void memoryRegionBin::insert(memoryRegion *span)
{
    if(!root)
        root = span;
    else
    {
        avlBaseNode<memorySpan> *rProxy = root;
        rProxy->insert(rProxy, span);
        root = static_cast <memoryRegion *> (rProxy);
    }
}

/*! Attempts to find and remove a specific region from the bin
 *  \param request Requested region
 *  \returns A pointer to the region that was retrieved or nullptr if a matching region was not in the bin
 */
memoryRegion *memoryRegionBin::retrieve(const memorySpan &request)
{
    if(!root)
        return nullptr;
    else
    {
        avlBaseNode<memorySpan> *ret = nullptr;
        avlBaseNode<memorySpan> *rProxy = root;
        root->retrieve(rProxy, request, ret);
        if(rProxy)
            root = static_cast <memoryRegion *> (rProxy);
        else
            root = nullptr;

        memoryRegion *retVal = static_cast <memoryRegion *> (ret);
        if(retVal)
            retVal->delink();
        return retVal;
    }
}

/*! Attempts to retrieve the minimum valued, by base address, region from the bin
 *  \return The region with the lowest base address if one is present or a nullptr if the bin is empty
 */
memoryRegion *memoryRegionBin::retrieveMinimum(void)
{
    if(!root)
        return nullptr;
    else
    {
        avlBaseNode<memorySpan> *ret = nullptr;
        avlBaseNode<memorySpan> *rProxy = root;

        root->getLeftMost(rProxy, ret);
        if(rProxy)
            root = static_cast <memoryRegion *> (rProxy);
        else
            root = nullptr;
        //kprintf("MRB - RM - RET: %p ROOT: %p rProxy: %p\n", ret, root, rProxy);

        memoryRegion *retVal = static_cast <memoryRegion *> (ret);
        if(retVal)
            retVal->delink();
        return retVal;
    }
}

/*! Indicates if the bin is empty
 *  \return True if there are no regions in the bin, false otherwise
 */
bool memoryRegionBin::empty(void) const
{
    return (!root);
}

