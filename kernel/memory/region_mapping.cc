#include "region_mapping.h"

//! Empty constructor
regionMapping::regionMapping() : physBase( 0 ), virtualBase( 0 ), regionLength( 0 ), dependents(0)
{
}

//! Constructs a mapping with the provided values
regionMapping::regionMapping( uintptr_t phys, uintptr_t virt, uintptr_t length ) : physBase( phys ), virtualBase( virt ), regionLength( length )
{
}

/*! Equality operator
 *  \param m Reference to the other mapping used in the comparison
 *  \return True if they regions physical space overlap, false otherwise
 */
bool regionMapping::operator ==(const regionMapping &m ) const
{
	if( physBase <= m.physBase )
		if( ( physBase+regionLength ) >= ( m.physBase+regionLength ) )
			return true;
	return false;
}

/*! Greater than operator
 *  \param m Reference to the other mapping used in the comparison
 *  \return True if region's physical base is greater address is greater then the one it's compared to, false otherwise
 */
bool regionMapping::operator >(const regionMapping &m ) const
{
	return ( physBase > m.physBase );
}

bool regionMapping::operator <(const regionMapping &m ) const
{
	return ( (physBase + regionLength) <= m.physBase );
}

void regionMapping::addDependent() const
{
	dependents++;
}

bool regionMapping::removeDependent() const
{
	return !(--dependents);
}
