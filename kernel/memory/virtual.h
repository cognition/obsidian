#ifndef VIRTUAL_MEMORY_H_
#define VIRTUAL_MEMORY_H_

#include <compiler.h>
#include "fixed_region_allocator.h"
#include "region_mapping.h"
#include <templates/avltree.cc>
#include <templates/vector.cc>
#include <fs/filesystem.h>
#include <templates/shared_ptr.cc>


//Page metrics
#define REGULAR_PAGE_SIZE       0x1000U
#define LARGE_PAGE_SIZE         0x200000U

#define LARGE_PAGE_MASK         0x1FFFFFUL
#define PAGE_OFFSET_MASK        0xFFFUL
#define PAGE_STRUCTURE_MASK     0x1FF

#define PAGE_OFFSET_BITS        12
#define LARGE_PAGE_BITS         21


// Self referenced paging structures
#define SELF_REF_PML4           0xFFFFFFFFFFFFF000UL
#define SELF_REF_PDPTR          0xFFFFFFFFFFE00000UL
#define SELF_REF_PAGE_DIR       0xFFFFFFFFC0000000UL
#define SELF_REF_PAGE_TABLE     0xFFFFFF8000000000UL

// Guest referenced paging structures
#define GUEST_REF_PML4          0xFFFFFF7FFFFFF000UL
#define GUEST_REF_PDPTR         0xFFFFFF7FFFE00000UL
#define GUEST_REF_PAGE_DIR      0xFFFFFF7FC0000000UL
#define GUEST_REF_PAGE_TABLE    0xFFFFFF0000000000UL

//Canonical boundary
#define KERNEL_VSPACE_START     0xFFFF800000000000UL

//Page masks
#define PAGE_PRESENT            0x1U
#define PAGE_WRITE              0x2U
#define PAGE_USER               0x4U
#define PAGE_CACHE_WT           0x8U
#define PAGE_CACHE_DISABLE      0x10U
#define PAGE_ACCESSED           0x20U
#define PAGE_DIRTY              0x40U
#define PAGE_CACHE_PAT          0x80U

#define PAGE_PAT0               0x00U
#define PAGE_PAT1               (PAGE_CACHE_WT)
#define PAGE_PAT2               (PAGE_CACHE_DISABLE)
#define PAGE_PAT3               (PAGE_CACHE_WT | PAGE_CACHE_DISABLE)
#define PAGE_PAT4               (PAGE_CACHE_PAT)
#define PAGE_PAT5               (PAGE_CACHE_PAT | PAGE_CACHE_WT)
#define PAGE_PAT6               (PAGE_CACHE_PAT | PAGE_CACHE_DISABLE)
#define PAGE_PAT7               (PAGE_CACHE_PAT | PAGE_CACHE_DISABLE | PAGE_CACHE_WT)

#define PAGE_CACHE_WC           (PAGE_PAT4)

#define PAGE_GLOBAL             0x100U

#define PAGE_LARGE              0x80U
#define PAGE_LARGE_GLOBAL       0x200U
#define PAGE_LARGE_PAT          0x1000U

#define LARGE_PERMISSION_FLAGS  (PAGE_NX | 0x011FU)
#define REG_PERMISSION_FLAGS    (PAGE_NX | 0x019FU)

#define PAGE_COW                0x200U

#define PAGE_NX                 0x8000000000000000UL

#define LARGE_PAGE_ADDRESS_MASK (~(PAGE_NX | LARGE_PAGE_MASK))
#define PAGE_ADDRESS_MASK       (~(PAGE_NX | PAGE_OFFSET_MASK))

//Page fault error code masks
#define PAGE_ERROR_PROTECTION   0x1
#define PAGE_ERROR_WRITE        0x2
#define PAGE_ERROR_USER         0x4
#define PAGE_ERROR_RESERVED     0x8
#define PAGE_ERROR_IF           0x10

//Stack related values
#define DEFAULT_KSTACK_PAGES        4U
#define DEFAULT_KSTACK_SIZE         (DEFAULT_KSTACK_PAGES*REGULAR_PAGE_SIZE)

typedef uintptr_t pmmEntry;

/*! Object which provides virtual memory management functions including: \n
 *  \n
 *  Kernel mode virtual space allocation. \n
 *  Physical to virtual memory mapping. \n
 *  Cache optimized virtual memory region allocations. \n
 *  Partition and management of virtual space. \n
 */
class virtualMemoryManager : public fixedRegionAllocator
{
	public:
		virtualMemoryManager( uintptr_t vSpaceStart );

		//Functionality accessors
		static void init( uintptr_t vSpaceStart );
		static uintptr_t allocateRegion( size_t pages, uint64_t flags );
		static uintptr_t mapRegion( uintptr_t phys, uintptr_t length, uint64_t flags );

        static void mapPages( const uintptr_t phys, const uintptr_t linear, const size_t pages, const uint64_t flags, const bool allocate );
		static void mapPages( const Vector<pmmEntry> &pages, const uintptr_t linear, const uintptr_t length, const uint64_t flags, bool allocate );

		static bool unmapPhysRegion( uintptr_t phys, uintptr_t length );
		static bool unmapRegion( uintptr_t virt, uintptr_t length, bool save, bool virtSave);
		//Physical to Virtual address translation
		static uintptr_t getVirtualAddress( const uintptr_t phys, const uintptr_t accessLength );

		static void changePermissions(const uintptr_t vBase, const size_t pageCount, const uint64_t newPermissions);
		//Lazy mapping handler
		static void mapStructures( uintptr_t laddr, bool isUser = false );

		//Kernel stack mapping function
		static regionObj64 allocateKernelStack( uint32_t pages );

		//Unmaps the first 2MB of identity mapped memory
		static void unmapIdentity();

		//Verifies the mapping attributes of a range of virtual memory
		static bool verifyAccess( uintptr_t address, uint64_t length, uint64_t attributes );
		//Gets the address of the primary kernel virtual mapping structure
		static uintptr_t getKernelPrimaryVirtualStructure();
		//Clones a new primary virtual structure and copies kernel space entries
		static physaddr_t cloneKPVS(void);
		//Gets the physical address mapping of a virtual page
		static bool getPhysicalAddress( const uintptr_t virt, uintptr_t &phys );
		static uintptr_t registerMmioMapping( uintptr_t phys, uint32_t accessLength);

		static void printMapping( const uintptr_t virt );
		static uintptr_t createMapping( Vector<physaddr_t> &mapping, const size_t pagesNeeded);
		static uintptr_t createLargeMapping( Vector<physaddr_t> &mapping, const size_t largePagesNeeded);
		static void implementMapping( const Vector<physaddr_t> &mapping, const virtaddr_t virtAddress, const physaddr_t flags );
		static void implementDirMap( const Vector<physaddr_t> &mapping, const virtaddr_t virtAddress, const physaddr_t flags );

		static uint32_t maxAddressBits(void);
		static void unmapUserStructures(void);
		static void copySwap(const uintptr_t addr, physaddr_t flags, bool savePage);

		static void *quickMapPage(const uintptr_t addr);
		static void quickMapClear(void);

		static void *quickMapLargePage(const uintptr_t addr);
		static void quickMapClearLarge(void);

		static bool allocateQuickMapEntries(uintptr_t &qmapAddr, physaddr_t *&qmapEntry, uintptr_t &lqmapAddr, physaddr_t *&lqmapEntry);

		static inline uintptr_t getMappingOffset(const uintptr_t addr);
	private:
	    void attemptUnmapHigher(const uint32_t pdpIndex, const uint32_t l4Index, const uintptr_t pageCount);

		uintptr_t allocateSpace( size_t pages, const uintptr_t align = 0 );
		uintptr_t instanceAllocateRegion( size_t pages, uint64_t flags );
		uintptr_t instanceMapRegion( uintptr_t phys, uintptr_t length, uint64_t flags );

		void mapPageArray( const Vector<physaddr_t> &pages, const virtaddr_t addr, const uint64_t flags );
		bool instanceUnmapPhysRegion( uintptr_t phys, uintptr_t length );
		bool instanceUnmapRegion(uintptr_t virt, const uintptr_t length, const bool save, const bool virtSave);
        void partitionAndInsertRegion(uintptr_t base, uintptr_t length);

		//! Tree holding physical to virtual mappings of contiguous regions of physical memory, usually for MMIO or buffers
		static avlTree <regionMapping> mappingTranslations;

		//! Memory region cache maintenance call, updates swap structures
		friend void memoryCache::maintainCache();
		//! Bitmask used in page color operations
		static uintptr_t colorMask;
		//! Bitmask used to obtain the 'color' information from a given address
		static uintptr_t colorFieldMask;
};


#endif
