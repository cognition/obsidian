#include "memorybin.h"
#include "memory.h"

#include <io/io.h>

//memoryBin class

//! Constructor
memoryBin::memoryBin() : prev( NULL ), next( NULL ), usedPages( 0 ), availablePages( 0 )
{
}

//! Initializes a the object
void memoryBin::init()
{
	usedPages = 0;
	availablePages = 0;
	prev = NULL;
	next = NULL;
}

//! Returns a ranking value based on the total available pages to this bin
uint32_t COMPILER_INLINE memoryBin::getRank()
{
	return ( availablePages - usedPages );
}

void COMPILER_INLINE memoryBin::addAvailablePages(uint32_t value)
{
    availablePages += value;
}

void COMPILER_INLINE memoryBin::incUsed()
{
    usedPages++;
}

void COMPILER_INLINE memoryBin::decUsed()
{
    usedPages--;
}
