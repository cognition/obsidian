#ifndef REGION_MAPPING_H_
#define REGION_MAPPING_H_

#include <compiler.h>

//! Object representing a physical to virtual mapping of a physically contiguous region of memory
class regionMapping
{
	public:
		regionMapping();
		regionMapping( uintptr_t phys, uintptr_t virt, uintptr_t length );
		void addDependent() const;
		bool removeDependent() const;
		bool operator ==(const regionMapping &m ) const;
		bool operator >(const regionMapping &m ) const;
		bool operator <(const regionMapping &m ) const;
		//! Physical base address of the mapping
		const uintptr_t physBase;
		//! Virtual base address of the mapping
		const uintptr_t virtualBase;
		//! Length of the mapping
		const uintptr_t regionLength;
	private:
		//! Number of object dependent on this mapping
		mutable uint32_t dependents;
};


#endif
