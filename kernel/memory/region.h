#ifndef MEMORY_REGION_H_
#define MEMORY_REGION_H_

#include <templates/avltree.cc>
#include <cpu/arch.h>
#include <regionobj.h>

COMPILER_PACKED struct memorySpan
{
      memorySpan() = default;
      memorySpan(const uint64_t spanBase, const uint64_t spanPages);
      memorySpan(const memorySpan &other);


      memorySpan &operator =(const memorySpan &other);
      bool operator <(const memorySpan &other) const;
      bool operator >(const memorySpan &other) const;
      bool operator ==(const memorySpan &other) const;

      memorySpan &split(const uint64_t targeBytes, memorySpan &tail);

      uint64_t base;
      uint32_t filler;
};

COMPILER_PACKED class memoryRegion : public avlBaseNode<memorySpan>
{
    public:
        memoryRegion() = default;
        memoryRegion(const uint64_t base, const uint64_t length);

        memoryRegion *halve(void);
        void init(const uint64_t base, const uint64_t length);
        uint64_t getLength(void) const;
        uint64_t getBase(void) const;

        void stepAugmentLength(void);

        void setLink(memoryRegion *next);
        memoryRegion *getLink(void) const;

        void delink(void);
        void clear(void);
};

COMPILER_PACKED class memoryRegionBin
{
    public:
        memoryRegionBin();
        void insert(memoryRegion *span);
        memoryRegion *retrieve(const memorySpan &request);
        memoryRegion *retrieveMinimum(void);
        bool empty(void) const;
    private:
        memoryRegion *root;
};

#endif // MEMORY_REGION_H_
