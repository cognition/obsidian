#ifndef MEMORY_FIXED_REGION_ALLOCATOR_H_
#define MEMORY_FIXED_REGION_ALLOCATOR_H_

#include "objcache.h"

#define ARCH_MIN_PAGE_BITS              12

class fixedRegionAllocator
{
    public:
        fixedRegionAllocator( uint8_t maxAddressBits, uint8_t fixedStartBits );
    protected:
        memoryRegion *allocateFixedRegion(uint32_t depth);
        void insertFixedRegion(uint32_t index, memoryRegion *region, uintptr_t mask);
    private:
        memoryRegion *retrieveFixedRegion(const uint8_t index, const uintptr_t mask);
        //Fixed region pointer storage array
        memoryRegionBin *fixedRegions;
        uintptr_t availableMask;
        uint8_t largestFixedIndex;
        uintptr_t availableMemory;
    protected:
        const uint8_t fixedTotalBits, fixedRegionCount;
};

#endif
