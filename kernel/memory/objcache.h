#ifndef MEMORY_OBJECT_CACHE_H_
#define MEMORY_OBJECT_CACHE_H_

#include <compiler.h>
#include <cpu/processor.h>
#include "region.h"


#define CACHE_REGION_COUNT          ((ARCH_PAGE_SIZE/sizeof(memoryRegion))-1)

/*! Object containing a REGULAR_PAGE_SIZE sized storage cache of memory region objects and provides functions including: \n
 *  \n
 *  Allocation of an object from a specific cache
 *  Freeing of an object to a specific cache
 *  Global allocation of memoryRegion objects
 *  Global free request handling of memoryRegion objects
 *  Maitenence of cache structures
 */
class COMPILER_PACKED memoryCache
{
	public:
		memoryCache();
		static void free( memoryRegion *region );
		static memoryRegion *allocate( const uint64_t base, uint64_t length );
		static void maintainCache();
	private:
		memoryRegion *allocateLocal( const uint64_t base, const uint64_t length );
		void freeLocal( uint32_t index );

		//! Next list object pointer
		class memoryCache *next;
		//! Free region pointer
		memoryRegion *freePtr;
		//! Packing
		void *packed[2];
		//! Array of regions held by the cache
		memoryRegion regions[CACHE_REGION_COUNT];
		//! Temporary swap caches
		static class memoryCache *swap[4];
		//! Caches that have blocks available for allocations
		static class memoryCache *active;
};



#endif

