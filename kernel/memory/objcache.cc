#include "objcache.h"
#include <io/io.h>
#include <cpu/bits.h>
#include <cpu/lock.h>
#include "ops.h"

static ticketLock mCacheLock;
static __attribute__ ( ( aligned ( 4096 ) ) ) memoryCache initCache;
static __attribute__ ( ( aligned ( 4096 ) ) ) memoryCache swapCache1;
static __attribute__ ( ( aligned ( 4096 ) ) ) memoryCache swapCache2;
static __attribute__ ( ( aligned ( 4096 ) ) ) memoryCache swapCache3;
static __attribute__ ( ( aligned ( 4096 ) ) ) memoryCache swapCache4;


memoryCache *memoryCache::active = &initCache;
memoryCache *memoryCache::swap[4] = { &swapCache1, &swapCache2, &swapCache3, &swapCache4 };

// memoryCache class
//! Constructor for a memoryCache region
memoryCache::memoryCache() : next( NULL ), freePtr(regions)
{
    Memory::set(&regions[0], 0, CACHE_REGION_COUNT*sizeof(memoryRegion));
    for(uint32_t i = 0; i < (CACHE_REGION_COUNT-1); i++)
        regions[i].setLink( &regions[i+1] );
}

/*! \brief Allocates and initializes a memoryRegion object
 *  \param base Base address of the region
 *  \param length length of the region in bytes
 *  \return Pointer to the new memoryRegion object
 */
memoryRegion *memoryCache::allocate( const uint64_t base, uint64_t length )
{
    spinLockInstance cacheLock(&mCacheLock);
	if( !active )
	{
		for( int i = 0; i < 4; i++ )
			if( swap[i] )
			{
				active = swap[i];
				swap[i] = NULL;
				break;
			}
		if( !active )
		{
			kprintf( "Error! memoryCache objects exhausted!\n" );
			asm ( "hlt\n":: );
		}
	}

	//kprintf("OC: Allocating from cache %x\n", active);
	memoryRegion *ret = active->allocateLocal( base, length );
	//kprintf("CACHEA:%X @%X:%X\n", ret, baseAddr, regionLength );
	return ret;
}

/*! \brief Allocates and initializes a memoryRegion object from a specific cache
 *  \param base Base address of the region
 *  \param length Length of the region in bytes
 *  \return Pointer to the new memoryRegion object
 */
memoryRegion *memoryCache::allocateLocal( const uint64_t base, const uint64_t length )
{
	memoryRegion *ret = freePtr;
	if(!ret)
    {
        kprintf("Error allocation attempted from full memory cache: %x", this);
        asm volatile ("cli\n"
                      "hlt\n"::);
        return nullptr;
    }
    else
    {
        freePtr = ret->getLink();

        // If this cache is out of free objects switch the active cache over to the next one in the list
        if(!freePtr)
        {
            active = next;
            next = nullptr;
        }
        ret->setLink(nullptr);
        // Initialize region to the provided values
        ret->init( base, length );
        return ret;
    }
}

/*! \brief Frees a memoryRegion object
 *  \param region memoryRegion object to be freed
 */
void memoryCache::free( memoryRegion *region )
{
    spinLockInstance cacheLock(&mCacheLock);
	uintptr_t address = reinterpret_cast <uintptr_t> ( region );
    uint32_t index = address & 0xFFF;
    address -= index;
    index -= 32;
    index /= sizeof(memoryRegion);
	memoryCache *cache = reinterpret_cast <memoryCache *> ( address );
    cache->freeLocal( index );
}

/*! \brief Frees a memoryRegion object specific to a cache object
 *  \param index Index of the memoryRegion object to be freed
 */
void memoryCache::freeLocal( uint32_t index )
{
    memoryRegion *reg = &regions[index];
    reg->clear();

    if(!freePtr)
    {
        next = active;
        active = this;
    }
    else
        reg->setLink(freePtr);
    freePtr = reg;
}
