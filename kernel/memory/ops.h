#ifndef MEMORY_OPS_H_
#define MEMORY_OPS_H_

#include <compiler.h>

namespace Memory
{
void copy( void *dest, const void *src, size_t size );
void set( void *dest, uint8_t value, size_t size );
int free(void *dest);
}


extern "C"
{
	void kmemcpy( void *dest, const void *src, size_t size );
	void kmemset( void *dest, const uint8_t value, size_t size );
}


#endif
