#include "ops.h"
#include "phys.h"
#include "virtual.h"
#include <cpu/localstate.h>
#include <cpu/bits.h>
#include <io/io.h>

static const uint64_t *selfPageDir = reinterpret_cast <const uint64_t *> ( SELF_REF_PAGE_DIR );
static const uint64_t *selfPageTable = reinterpret_cast <const uint64_t *> ( SELF_REF_PAGE_TABLE );

static physicalMemory *localMemory = NULL;

extern "C" void *_end, *_start;

static const uintptr_t kernPhysStart = LARGE_PAGE_SIZE - 2*REGULAR_PAGE_SIZE;
static uint64_t maxPhysAddress = 0;


//physicalMemory class
/*! Constructor
 *  \param p Parent device
 */
physicalMemory::physicalMemory( Device *p ) : Device( "System RAM", "mem", p ),
	memoryRegionAllocator( localState::getProc()->getColorBits() ), pageBins( NULL ), mostEligible( NULL ), allocatedPages(0)
{
    largePageFixedMask = LARGE_PAGE_SIZE>>colorBits;
    largePageFixedIndex = LARGE_PAGE_BITS - colorBits;
}

/*! \brief Initializes a physicalMemory object with values from the bootloader
 *  \param localInfo Pointer to the bootloader's information structure
 *  \param physEnd Physical end of memory based on the kernel's paging structures
 *  \return True if successful, false otherwise
 */
bool physicalMemory::init( void *context )
{
	localMemory = this;
	bootLoaderInfo *localInfo = reinterpret_cast <bootLoaderInfo *> ( context );
	uintptr_t virtEnd = localInfo->virtEnd;

	uintptr_t physEnd = localInfo->physEnd;

	if(!virtEnd)
		virtEnd = (uintptr_t)(&_end);

	kprintf("PMEM: Physical memory end - %X Virtual memory end - %X\n", physEnd, virtEnd);
	kprintf("PMEM: Boot memory map @ %X Size: %u\n", localInfo->memoryMapPtr, localInfo->memoryMapLength);
	pageBins = mapPageBins( physEnd, virtEnd );

	//Initialize the page bins
	for( uint32_t count = 0; count < colorCount; count++ )
		pageBins[count].init();

	//Sort through the memory map for available RAM regions
	uint32_t mmapArraySize = localInfo->memoryMapLength;
	memoryMap *mmapPtr = localInfo->memoryMapPtr;

	do
	{
		switch( mmapPtr->type )
		{
			case MMAP_RANGE_RAM:
				if( !acquireResource( systemResource( systemResourceMemory, mmapPtr->base, mmapPtr->length ) ) )
				{
					kprintf( "Error unable to acquire memory region @ %X-%X\n", mmapPtr->base, ( mmapPtr->base + mmapPtr->length - 1 ) );
					return false;
				}
				if( (mmapPtr->base + mmapPtr->length) > maxPhysAddress)
                    maxPhysAddress = mmapPtr->base + mmapPtr->length;
				if( mmapPtr->base < physEnd )
				{
					uint64_t post = ( mmapPtr->base + mmapPtr->length );
					if(post > kernPhysStart)
					{
						uint64_t pre = kernPhysStart - mmapPtr->base;
						if( mmapPtr->base < kernPhysStart )
							registerRamRegion( mmapPtr->base, pre );
						if( post > physEnd)
							registerRamRegion( physEnd, post - physEnd );
						break;
					}
                    else if(mmapPtr->base < ARCH_RESERVED_MEMORY_SIZE)
                    {
                        //Reserve the RAM region required by the platform
                        registerRamRegion( ARCH_RESERVED_MEMORY_SIZE, post - (ARCH_RESERVED_MEMORY_SIZE - mmapPtr->base) );
                        break;
                    }
				}

				registerRamRegion( mmapPtr->base, mmapPtr->length );
				break;
			default:
				break;
		}
		mmapArraySize -= mmapPtr->size;
		mmapPtr = reinterpret_cast <memoryMap *> ( reinterpret_cast <uintptr_t> ( mmapPtr ) + mmapPtr->size );
	} while( mmapArraySize );

	for( uint32_t count = 0; count < colorCount; count++ )
		registerBin( &pageBins[count] );

    //Initialize the virtual memory management subsystem
	virtualMemoryManager::init( virtEnd );
	return true;
}

//! Returns the maximum RAM address of the system
uint64_t physicalMemory::getMaxAddress(void)
{
    return maxPhysAddress;
}


/*! A simple stack allocator and virtual memory mapping function
 *  \param phys Reference to the current physical address
 *  \param virt Reference to the current virtual address
 *  \param page Number of pages to map from the initial \a phys value to the initial \a virt value
 *  \param flags Architecture specific flags describing attributes and permissions of the mapping
 */
static void faultlessMap( uintptr_t &phys, uintptr_t &virt, uint32_t pages, uint32_t flags )
{
	uint64_t *pageDir, *pageTable, index, physPtr, virtPtr, remaining;
	uint32_t tabCount;
	// Initialize our counters
	virtPtr = virt;
	physPtr = phys;
	remaining = pages;

	// Trim off the sign extended bits of the address
	index = virt;
	index <<= 16;
	index >>= 28;
	// Calculate initial page table entry count
	if( index & 0x1FF )
	{
		tabCount = ~index;
		tabCount &= 0x1FF;
	}
	else
		tabCount = 512;
	// Initialize our paging structure pointers
	pageTable = const_cast <uint64_t *> ( &selfPageTable[index] );
	index >>= 9;
	pageDir = const_cast <uint64_t *> ( &selfPageDir[index] );

	// Update mapping pointers
	uint64_t mapSize = REGULAR_PAGE_SIZE*pages;
	phys += mapSize;
	virt += mapSize;

	if( tabCount > pages )
		tabCount = pages;


	do
	{
		if( !*pageDir )
		{
			uint64_t *tmpTabPtr = const_cast <uint64_t *> ( &selfPageTable[512*index] );
			*pageDir = phys | PAGE_PRESENT | PAGE_WRITE;
			phys += REGULAR_PAGE_SIZE;

			asm ( "invlpg (%0)\n":: "r" ( tmpTabPtr ) );
			Memory::set( tmpTabPtr, 0, REGULAR_PAGE_SIZE );
			index++;
		}

		for( uint32_t count = 0; count < tabCount; count++ )
		{
			*pageTable = physPtr | flags;
			physPtr += REGULAR_PAGE_SIZE;
			pageTable++;
			asm ( "invlpg (%0)\n":: "r" ( virtPtr) );
		}

		remaining -= tabCount;

		if( remaining >= 512 )
			tabCount = 512;
		else
			tabCount = remaining;
	}
	while( remaining );
}

/*! \brief Maps memory for the page allocation bins
 *  \param currentPhysEnd Variable holding the current end point for physical memory
 *  \param currentVirtEnd Variable containing the current end point for the kernel's virtual memory
 *  \return Pointer to the memoryBin array that was mapped
 */
memoryBin *physicalMemory::mapPageBins( uintptr_t &currentPhysEnd, uintptr_t &currentVirtEnd )
{
	const uint32_t totalSize = sizeof( memoryBin )*colorCount;
	uint32_t pagesNeeded = totalSize/REGULAR_PAGE_SIZE;
	if(totalSize % REGULAR_PAGE_SIZE)
        pagesNeeded++;
	memoryBin *ret = reinterpret_cast <memoryBin *> ( currentVirtEnd );
	faultlessMap( currentPhysEnd, currentVirtEnd, pagesNeeded, PAGE_PRESENT | PAGE_GLOBAL | PAGE_WRITE );
	return ret;
}

/*! Allocates a page corresponding to a specific page color value from a physicalMemory object
 *  \param bin Color value
 *  \return Physical address of the allocated paged
 */
uintptr_t physicalMemory::instanceAllocatePageBin( const uint32_t bin )
{
    spinLock lock(&physMemLock);
    lock.acquire();

	memoryBin &specificBin = pageBins[bin];
    allocatedPages++;

    memoryRegion *region = buddyVariableAllocate(0, bin, REGULAR_PAGE_SIZE, bin<<ARCH_MIN_PAGE_BITS );
	uintptr_t ret = region->getBase();

	memoryCache::free(region);

    specificBin.incUsed();

    memoryBin *ptr = specificBin.next;

	if( ptr )
	{
		uint32_t rank = specificBin.getRank();
		if( ptr->getRank() > rank )
		{
			ptr->prev = specificBin.prev;

			if( specificBin.prev )
				specificBin.prev->next = ptr;
			else
				mostEligible = ptr;

			specificBin.next = NULL;
			specificBin.prev = NULL;

			while( ptr->next )
			{
				if( ptr->next->getRank() <= rank )
					break;
				ptr = ptr->next;
			}

			memoryBin *currentBinPtr = &pageBins[bin];
			if( ptr->next )
				ptr->next->prev = currentBinPtr;
			specificBin.next = ptr->next;
			ptr->next = currentBinPtr;
			specificBin.prev = ptr;
		}
	}
    lock.release();
    memoryCache::maintainCache();
    //instancePrintUsageInfo();
    //kprintf("PMEM - Allocated page %X from bin %u\n", ret, bin);
	return ret;
}

/*! Allocates a region of physical memory from a physicalMemory object
 *  \param size Size of the region in bytes
 *  \param align Alignment value
 *  \param alignMask Alignment mask
 *  \return Base address of the region
 */
uintptr_t physicalMemory::instanceAllocateRegion( uintptr_t size, uint32_t align )
{

	uint8_t bin = Bits::getMsb64( size );
	uint8_t subIndex = ( align & colorMask )>>bin;
	uintptr_t tSize = 1;
	size_t pages = (size + PAGE_OFFSET_MASK)/REGULAR_PAGE_SIZE;

	tSize <<= bin;
	bin -= ARCH_MIN_PAGE_BITS;

	if(tSize < size)
    {
        bin++;
        subIndex >>= 1;
    }
    spinLock lock(&physMemLock);
    lock.acquire();

	memoryRegion *reg = NULL;
	if( bin >= colorBits )
	{
		bin -= colorBits;
		reg = allocateFixedRegion(bin);
	}
	else
		reg = buddyVariableAllocate( bin, subIndex, size, align );

    lock.release();
	memoryCache::maintainCache();
	if( !reg )
		return 0;
	else
	{
	    allocatedPages += pages;
	    activeBuffers.insert(reg);
		return reg->getBase();
	}
}

//! Allocates a page of physical memory from the best qualifying memory bin
uintptr_t physicalMemory::instanceAllocatePage()
{
	uint8_t bin = ( reinterpret_cast <uintptr_t> ( mostEligible ) - reinterpret_cast <uintptr_t> ( pageBins ) )/sizeof( memoryBin );
	uintptr_t page = instanceAllocatePageBin( bin );
	return page;
}

//! Allocates a large page of physical memory from the object
uintptr_t physicalMemory::instanceAllocateLargePage()
{
	uint8_t index = (LARGE_PAGE_BITS - PAGE_OFFSET_BITS);
	if(index > colorBits)
        index -= colorBits;
    spinLock lock(&physMemLock);
    lock.acquire();
	memoryRegion *reg = allocateFixedRegion( index );
	uintptr_t addr = reg->getBase();
	memoryCache::free( reg );
	allocatedPages += LARGE_PAGE_SIZE/REGULAR_PAGE_SIZE;
	lock.release();
	memoryCache::maintainCache();
	return addr;
}


/*! Preforms the setup options necessary when a region of usable ram is registered:
 *  - Updates any applicable memoryBin objects available page count.
 *  - Inserts and, if applicable, partitions the ram region in a manner compatible with our memory organization heiarchy
 *  \param base Base address of the RAM region
 *  \param length Length of the RAM region
 */
void physicalMemory::registerRamRegion( uintptr_t base, uintptr_t length )
{
	length &= ~0xFFF;
	kprintf("Adding RAM region @ %X-%X\n", base, base+length - 1);

	if( !length )
		return;

	uint32_t allBins, remBins;
	uint8_t baseIndex = ( base & colorMask ) / REGULAR_PAGE_SIZE;
	uintptr_t pages = length/REGULAR_PAGE_SIZE;
	allBins = pages/colorCount;
	remBins = pages%colorCount;


	if( allBins )
	{
		for( uint32_t i = 0; i < colorCount; i++ )
			pageBins[i].addAvailablePages(allBins);
	}
	if( remBins )
	{
		uint8_t index = baseIndex;
		do
		{
			pageBins[index].addAvailablePages(1);
			index++;
			index &= colorFieldMask;
			remBins--;
		} while( remBins );
	}
	partitionAndInsertRegion( base, length );
}

/*! Partitions and inserts a range of memory based on the alignment and length rules
 *  enforced by the region allocator.
 *  \param base Base address of the region to be inserted
 *  \param length Length of the region to be inserted
 */
void physicalMemory::partitionAndInsertRegion( uintptr_t base, uintptr_t length )
{
    uintptr_t end = base + length;

    uintptr_t sizeMask = REGULAR_PAGE_SIZE;
    uint8_t depth = 0;

    // Top down fit
    while(sizeMask <= length)
    {
        if( end & sizeMask )
        {
            uintptr_t addr = end - sizeMask;
            memoryRegion *region = memoryCache::allocate( addr, sizeMask);
            if(depth > colorBits)
            {
                insertFixedRegion(depth - colorBits, region, sizeMask>>fixedTotalBits);
            }
            else
            {
                variableInsertRegionBin( depth, region, true);
            }
            end -= sizeMask;
            length -= sizeMask;
        }
        depth++;
        sizeMask <<= 1;
    }

    sizeMask = REGULAR_PAGE_SIZE;
    depth = 0;
    uintptr_t addr = base;

    // Bottom up fit
    while(sizeMask <= length)
    {
        if( length & sizeMask )
        {
            memoryRegion *region = memoryCache::allocate( addr, sizeMask);
            if(depth > colorBits)
            {
                insertFixedRegion(depth - colorBits, region, sizeMask>>fixedTotalBits);
            }
            else
            {
                variableInsertRegionBin( depth, region, true);
            }
            addr += sizeMask;
            length -= sizeMask;
        }
        depth++;
        sizeMask <<= 1;
    }


}

/*! Inserts an memoryBin object into the mostEligible list based on it's eligibility ranking
 *  \param bin Memory bin to be inserted
 */
void physicalMemory::registerBin( memoryBin *bin )
{
	memoryBin *ptr = mostEligible;
	uint32_t rank = bin->getRank();

	if( !ptr )
		mostEligible = bin;
	else if( rank >= ptr->getRank() )
	{
		bin->next = ptr;
		ptr->prev = bin;
		mostEligible = bin;
	}
	else
	{
		while( ptr->next )
		{
			if( ptr->next->getRank() <= rank )
				break;
			ptr = ptr->next;
		}
		bin->next = ptr->next;
		if( bin->next )
			bin->next->prev = bin;
		bin->prev = ptr;
		ptr->next = bin;
	}
}

/*! Returns a page of physical memory to the physical memory manager
 *  \param addr Address of the page being returned
 */
void physicalMemory::instanceReclaimPage( uintptr_t addr )
{
	uint32_t bin = ( addr & colorMask ) >> 12;
	memoryBin &binObj = pageBins[bin];

    //kprintf("PMEM - Reclaiming page %X\n", addr);
    spinLock lock(&physMemLock);
    lock.acquire();
    allocatedPages--;

    memoryRegion *region = memoryCache::allocate( addr, REGULAR_PAGE_SIZE );
    variableInsertRegionBin( 0, region, true );
    binObj.decUsed();
    memoryBin *binPtr = binObj.prev;
    while(binPtr)
    {
        if(binPtr->getRank() > binObj.getRank() )
            break;
        else
            binPtr = binPtr->prev;
    }
    if(binPtr != binObj.prev)
    {
        if(binPtr)
        {
            memoryBin *prevPtr = binObj.prev;
            if(binObj.next)
                binObj.next->prev = prevPtr;
            prevPtr->next = binObj.next;

            binObj.next = binPtr->next;
            binObj.next->prev = &binObj;
            binObj.prev = binPtr;
            binPtr->next = &binObj;
        }
        else
        {
            if(binObj.next)
                binObj.next->prev = binObj.prev;
            if(binObj.prev)
                binObj.prev->next = binObj.next;

            binObj.next = mostEligible;
            binObj.prev = NULL;
            mostEligible->prev = &binObj;
            mostEligible = &binObj;
        }
    }

    lock.release();
    memoryCache::maintainCache();
}

/*! Returns a large page of physical memory to the physical memory manager
 *  \param addr Address of the large page to return to the PMM
 */
void physicalMemory::instanceReclaimLargePage( uintptr_t addr)
{
   spinLock lock(&physMemLock);
   lock.acquire();
   memoryRegion *region = memoryCache::allocate( addr, LARGE_PAGE_SIZE );
   insertFixedRegion( largePageFixedIndex, region, largePageFixedMask );
   lock.release();
   memoryCache::maintainCache();
}

void physicalMemory::instancePrintUsageInfo(void)
{
    kprintf("Number of pages used: %u - Bytes: %u\n", allocatedPages, allocatedPages*REGULAR_PAGE_SIZE);
}

void physicalMemory::printUsageInfo(void)
{
    localMemory->instancePrintUsageInfo();
}

/********************************
 *  Static feature accessors    *
 ********************************/

/*! Allocates a non-specific page of physical memory
 *  \return Physical address of the allocated page
 */
uintptr_t physicalMemory::allocatePage()
{
	uintptr_t addr = localMemory->instanceAllocatePage();
	//kprintf("PMEM - Allocate: %X\n", addr);
	return addr;
}

/*! Allocates a non-specific large page of physical memory
 *  \return Physical address of the allocated lage page
 */
uintptr_t physicalMemory::allocateLargePage()
{
	uintptr_t addr = localMemory->instanceAllocateLargePage();
	memoryCache::maintainCache();
	return addr;
}

/*! Allocates a page from a specific page coloring bin
 *  \return Physical address of the allocated page
 */
uintptr_t physicalMemory::allocatePageBin( const uint32_t bin )
{
	uintptr_t addr = localMemory->instanceAllocatePageBin( bin );
	//kprintf("PMEM - Allocate Bin - %u - Frame: %X\n", bin, addr);
	return addr;
}

uintptr_t physicalMemory::allocateRegion(size_t size, uint32_t align )
{
    return localMemory->instanceAllocateRegion(size, align);
}

/*! Reclaims a page of physical memory that was previous in use
 *  \param addr Address of the page being reclaimed
 */
void physicalMemory::reclaimPage( uintptr_t addr )
{
    //kprintf("PMEM - Reclaim: %X\n", addr);
	localMemory->instanceReclaimPage( addr );
}

/*! Reclaims a large page of physical memory that was previously in use
 *  \sa void physicalMemoryManager::reclaimLargePage( uintptr_t addr )
 *  \param add Address of the large page that is to be returned to the PMM
 */
void physicalMemory::reclaimLargePage( uintptr_t addr )
{
    localMemory->instanceReclaimLargePage( addr );
}

void physicalMemory::reclaimRegion(uintptr_t base, size_t length)
{
    localMemory->partitionAndInsertRegion(base, length);
}
