[BITS 64]
[DEFAULT rel]

GLOBAL sse2_memset, sse2_memsetw, sse2_memcpy

sse2_memsetw:
push rcx
cmp rdx, 16
jl .wordFill
movzx eax, si
shl eax, 16
or eax, esi
mov esi, eax
shr rax, 32
or rax, rsi
cmp rdx, 256
jl .std_memsetw

movq xmm0, rax
pshufd xmm0, xmm0, 0
movzx ecx, dil
and cl, 0xF
jz .setupSSE2
not cl
and cl, 0xE
shr cl, 1
sub rdx, rcx
rep stosw

.setupSSE2:
mov rcx, rdx
and rcx, byte -128
sub rdx, rcx
shr rcx, 7
.sse2fill:
movntdq [rdi], xmm0
movntdq [rdi+16], xmm0
movntdq [rdi+32], xmm0
movntdq [rdi+48], xmm0
movntdq [rdi+64], xmm0
movntdq [rdi+80], xmm0
movntdq [rdi+96], xmm0
movntdq [rdi+112], xmm0
add rdi, 128
dec rcx
jnz .sse2fill
.std_memsetw:
movzx ecx, dx
and ecx, byte -4
jz .wordFill
sub edx, ecx
shr ecx, 2
rep stosq
.wordFill:
cmp dx, 0
jz .end
mov ecx, edx
rep stosw
.end:
pop rcx
ret

sse2_memset:
push rcx
cmp rdx, 32
jl .finish

mov al, sil
mov ah, al
mov si, ax
shl eax, 16
or eax, esi
mov esi, eax
shl rax, 32
or rax, rsi
cmp rdx, 256
jl .std_memset

movq xmm0, rax
pshufd xmm0, xmm0, 0
mov cl, dil
and cl, 0xF
jz .sse2_setup
not cl
and ecx, 0xF
sub rdx, rcx
rep stosb

.sse2_setup:
mov rcx, rdx
and rcx, byte -128
sub rdx, rcx
shr rcx, 7

.sse2_loop:
movntdq [rdi], xmm0
movntdq [rdi+16], xmm0
movntdq [rdi+32], xmm0
movntdq [rdi+48], xmm0
movntdq [rdi+64], xmm0
movntdq [rdi+80], xmm0
movntdq [rdi+96], xmm0
movntdq [rdi+112], xmm0
add rdi, 128
dec rcx
jnz .sse2_loop
.std_memset:
movzx ecx, dx
and ecx, byte -8
jz .finish
sub dx, cx
shr ecx, 3
rep stosq
.finish:
cmp dx, 0
jz .end
movzx ecx, dx
rep stosb
.end:
pop rcx
ret

