#ifndef MEMORY_PLACEMENT_NEW_H_
#define MEMORY_PLACEMENT_NEW_H_

#include <compiler.h>

inline void *operator new( uintptr_t COMPILER_UNUSED size, void *ptr )
{
	return ptr;
}

#endif // MEMORY_PLACEMENT_NEW_H_
