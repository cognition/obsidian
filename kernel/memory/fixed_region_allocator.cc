#include <cpu/bits.h>
#include <cpu/arch.h>
#include <io/io.h>
#include "fixed_region_allocator.h"
#include "phys.h"

#define REGULAR_PAGE_SIZE           4096
#define LARGE_PAGE_SIZE             0x200000
#define LARGE_PAGE_MASK             0x1FFFFF

//! Testing definition
#ifdef FRALLOC_VALIDATE

#define assert(x)           if( !(x)) { \
                            kprintf("Assert \"%s\"failed @ %s:%u\n", #x, __FILE__, __LINE__);\
                            asm volatile ("cli\n" \
                                          "hlt\n"::); \
                             }

#else

#define assert(x)

#endif



/*! Constructor
 *  \param maxAddressBits Maximum addressable number of bits
 *  \param fixedStartBits Bits reserved for other region allocators
 */
fixedRegionAllocator::fixedRegionAllocator( const uint8_t maxAddressBits, const uint8_t fixedStartBits ) :
    availableMask(0), largestFixedIndex(0), availableMemory(0), fixedTotalBits(fixedStartBits + ARCH_MIN_PAGE_BITS), fixedRegionCount(maxAddressBits - fixedTotalBits)
{
    fixedRegions = new memoryRegionBin[fixedRegionCount];
}

/*! Actual allocator logic
 *  \param depth Index into the array we're requesting, which holds regions of size 2^(depth + fixedTotalBits)
 */
memoryRegion *fixedRegionAllocator::allocateFixedRegion(uint32_t depth)
{
    //Create the test mask
	uintptr_t testMask = 1;
	testMask <<= depth;
    //Check that we can actually service the request
    if(availableMask < testMask)
    {
        kprintf("Error out of memory, depth requested %u Available memory: %X Caller: %X\n", depth, availableMemory, __builtin_return_address(0));
        physicalMemory::printUsageInfo();
        asm volatile ("cli\n"
                      "hlt\n"::);
        return NULL;
    }

    //Check if a region meeting our criteria is available.  If one is retrieve and return it
	if(availableMask & testMask)
        return retrieveFixedRegion(depth, testMask);
	else
	{
	    //We need to subdivide a larger region to meet the requested size
		memoryRegion *region;
		uint8_t index;
        uintptr_t mask;
		//Create a temporary availability mask which represents only available regions > our requested size
		uintptr_t tmpMask = availableMask & ~(testMask-1);

        //Determine the next smallest region via LSB of the temporary mask
		index = Bits::getLsb64(tmpMask);
		//Initialize our mask and size field
		mask = 1;
		mask <<= index;
        //kprintf("Attempting to allocate oversized region @ depth: %u Index: %u AM: %X TM: %X Mask: %X\n", depth, index, availableMask, tmpMask, mask);
        //Retrieve the region
        region = retrieveFixedRegion(index, mask);
        //Do the actual subdivision
		while(index > depth)
		{
			index--;
            mask >>= 1;

            memoryRegion *split = region->halve();
			insertFixedRegion(index, split, mask);
		}
        return region;
	}
}

/*! Inserts a region into the appropriate array index and updates the availability mask
 *  \param index Index in the region array to place the region pointer at
 *  \param region Pointer to the region which is being inserted
 *  \param mask Test mask for the array index
 */
void fixedRegionAllocator::insertFixedRegion(uint32_t index, memoryRegion *region, uintptr_t mask)
{
    availableMemory += region->getLength();

    assert(index < fixedRegionCount );

    if((fixedRegionCount - index) > 1)
    {
        if( region->getBase() & region->getLength() )
        {
            memorySpan span(region->getBase() - region->getLength(), region->getLength() );

            if( memoryRegion *buddy = fixedRegions[index].retrieve(span) )
            {
                buddy->stepAugmentLength();
                region->clear();
                memoryCache::free(region);
                if(fixedRegions[index].empty())
                    availableMask &= ~mask;
                insertFixedRegion(index + 1, buddy, mask << 1);
                return;
            }
        }
        else
        {
            memorySpan span(region->getBase() + region->getLength(), region->getLength() );

            if( memoryRegion *buddy = fixedRegions[index].retrieve(span) )
            {
                region->stepAugmentLength();
                buddy->clear();
                memoryCache::free(buddy);
                if(fixedRegions[index].empty())
                    availableMask &= ~mask;
                insertFixedRegion(index + 1, region, mask << 1);
                return;
            }

        }
    }

    fixedRegions[index].insert(region);
    //Update availability mask and largest index indicator
    availableMask |= mask;
    if(index > largestFixedIndex)
        largestFixedIndex = index;
}

/*! Retrieve a region from the array at a provided index
 *  \param index Index into the array
 *  \param mask Test mask for the index
 *  \return A pointer to a valid region
 */
memoryRegion *fixedRegionAllocator::retrieveFixedRegion(const uint8_t index, const uintptr_t mask)
{
    assert(index < fixedRegionCount);

    //Initialize our return pointer
    memoryRegion *ret = fixedRegions[index].retrieveMinimum();
    //kprintf("FRA - Retrieving region @ Index: %u with mask %X Reg: %p Caller: %p\n", index, mask, ret, COMPILER_FUNC_CALLER);
    //Update linkages if necessary, this also checks if there are no additional free regions for this index
    if( fixedRegions[index].empty() )
    {
        //No more regions at this index.  Update availability mask and largest fixed region
        availableMask &= ~mask;
        if(largestFixedIndex == index)
        {
            if(availableMask)
                largestFixedIndex = Bits::getMsb64(availableMask);
        }
    }

    availableMemory -= ret->getLength();
    return ret;
}
