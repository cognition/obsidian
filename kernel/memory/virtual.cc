#include <cpu/bits.h>
#include "virtual.h"
#include "phys.h"
#include <io/io.h>
#include "memory.h"
#include <cpu/lock.h>
#include <cpu/localstate.h>

static uint64_t * const selfPageL4 = reinterpret_cast <uint64_t * const> ( SELF_REF_PML4 );
static uint64_t * const selfPageDirPtr = reinterpret_cast <uint64_t * const> ( SELF_REF_PDPTR );
static uint64_t * const selfPageDir = reinterpret_cast <uint64_t * const> ( SELF_REF_PAGE_DIR );
static uint64_t * const selfPageTable = reinterpret_cast <uint64_t * const> ( SELF_REF_PAGE_TABLE );

#define PAGE_INVALIDATE(addr)           asm ("invlpg (%0)\n":: "r" (addr))
#define SHARED_FLAGS_MASK               (PAGE_PRESENT | PAGE_WRITE | PAGE_USER)

#define VMM_MAX_REGION_SIZE             (1UL<<48)


static uintptr_t kpvs = 0;
//Virtual memory manager lock
static ticketLock vmmLock;
//Higher level paging structure allocation lock
static ticketLock hlaLock;
static uint64_t kernL4[256];

static virtualMemoryManager *vmm = NULL;

uintptr_t virtualMemoryManager::colorFieldMask = 0;
uintptr_t virtualMemoryManager::colorMask = 0;

avlTree <regionMapping> virtualMemoryManager::mappingTranslations;

//virtualSpaceManager class

/*! Constructor for the VMM
 *  \param vSpaceStart Start of virtual space available for kernel data
 */
virtualMemoryManager::virtualMemoryManager( uintptr_t vSpaceStart ) : fixedRegionAllocator(48, 0)
{
    uint8_t colorBits = localState::getProc()->getColorBits();
    colorFieldMask = 1;
    colorFieldMask <<= colorBits;
    colorFieldMask--;
    colorMask = colorFieldMask<<ARCH_MIN_PAGE_BITS;
	uintptr_t vSpaceLength = GUEST_REF_PAGE_TABLE - vSpaceStart;
	kprintf("[VMM ]: Initial virt region @ %X Length: %X\n", vSpaceStart, vSpaceLength);

	uintptr_t mapOffset = getMappingOffset(vSpaceStart);

	if(vSpaceStart & LARGE_PAGE_MASK)
    {
        //There's at least one page sized block of virtual address space, take it for quick mapping
        localState::setQuickMapAddr(vSpaceStart);
        localState::setQuickMapEntry(&selfPageTable[mapOffset]);

        vSpaceStart += REGULAR_PAGE_SIZE;
        vSpaceLength -= REGULAR_PAGE_SIZE;

        if(vSpaceStart & LARGE_PAGE_MASK)
        {
            //Still not page aligned, kick the interim region to the virtual space allocator
            uintptr_t interimLength = LARGE_PAGE_SIZE - (vSpaceStart & LARGE_PAGE_MASK);
            partitionAndInsertRegion(vSpaceStart, interimLength);

            vSpaceStart += interimLength;
            vSpaceLength -= interimLength;

        }

        //Grab the space for a large page
        localState::setQuickMapLargeAddr(vSpaceStart);
        localState::setQuickMapLargeEntry( &selfPageDir[getMappingOffset(vSpaceStart)>>9] );

        vSpaceStart += LARGE_PAGE_SIZE;
        vSpaceLength -= LARGE_PAGE_SIZE;
    }
    else
    {
        //How fortuitous, we're already aligned for a large page mapping
        localState::setQuickMapLargeAddr(vSpaceStart);
        localState::setQuickMapLargeEntry(&selfPageDir[mapOffset>>9]);
        vSpaceStart += LARGE_PAGE_SIZE;
        vSpaceLength -= LARGE_PAGE_SIZE;
        //Tack the regular page on the end
        localState::setQuickMapAddr(vSpaceStart);
        localState::setQuickMapEntry(&selfPageTable[mapOffset + 512]);
        vSpaceStart += REGULAR_PAGE_SIZE;
        vSpaceLength -= REGULAR_PAGE_SIZE;
    }

    //Everything else belongs the virtual region allocator
 	partitionAndInsertRegion( vSpaceStart, vSpaceLength );
}

/*! Allocates a region of virtual space
 *  \param pages Size of the region in pages
 *  \return Base address of the virtual space
 */
uintptr_t virtualMemoryManager::allocateSpace( size_t pages, const uintptr_t align )
{
	memoryRegion *reg;
	uintptr_t addr;
	uintptr_t size = 1;
	uint8_t bin = Bits::getMsb64( pages );
	size <<= bin;

	if(size < pages)
        bin++;

    reg = allocateFixedRegion(bin);
    if(reg->getLength() < pages*REGULAR_PAGE_SIZE)
    {
        kprintf("[VMM ]: Error allocated region is smaller than requested size! Region length: %X Requested: %X Bin: %u\n", reg->getLength(), pages*REGULAR_PAGE_SIZE, bin );
        asm volatile ("xchg %%bx, %%bx\n"::);
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    regionObj64 space(reg->getBase(), reg->getLength() );

    //Free the region object
    memoryCache::free( reg );

    if(align)
    {
        size_t toLarge = (LARGE_PAGE_SIZE - align)/REGULAR_PAGE_SIZE;
        if( (toLarge < pages) && ((pages - toLarge) >= (LARGE_PAGE_SIZE/REGULAR_PAGE_SIZE) ) && (space.getLength() >= (align + pages*REGULAR_PAGE_SIZE)) )
        {
            partitionAndInsertRegion(space.getBase(), align);
            space.augmentBase(align);
            space.decrementLength(align);
        }
    }


    addr = space.getBase();
    uintptr_t tail = space.getLength() - pages*REGULAR_PAGE_SIZE;

    if(tail)
        partitionAndInsertRegion(space.getBase() + pages*REGULAR_PAGE_SIZE, tail);

    return addr;
}

uint32_t virtualMemoryManager::maxAddressBits(void)
{
    return ARCH_VIRTUAL_NON_FIXED_BITS;
}

/*! Creates a new kernel primary virtual structure (ex. PML4 structure on an x86-64) and clones all kernel level entries
 *  \return The physical address of the new virtual address structure
 */
physaddr_t virtualMemoryManager::cloneKPVS(void)
{

    static constexpr const uint32_t midEntry = 256;
    //Allocate the physical memory for the new structure and map it into kernel space
    physaddr_t newKPVS = physicalMemory::allocatePage();
    uintptr_t vaddr = mapRegion(newKPVS, REGULAR_PAGE_SIZE, PAGE_PRESENT | PAGE_WRITE );

    //Copy all entries except for the self reflected entry
    physaddr_t *newRef = reinterpret_cast <physaddr_t *>(vaddr);
    physaddr_t *selfRef = &newRef[midEntry*2 - 1];
    //uint32_t halfSize = midEntry*sizeof(uint64_t);


    Memory::copy( &newRef[midEntry], kernL4, REGULAR_PAGE_SIZE/2);
    Memory::set(newRef, 0, REGULAR_PAGE_SIZE/2);

    *selfRef = newKPVS | PAGE_PRESENT | PAGE_WRITE | PAGE_NX;

    //Remove the temporary kernel virtual mapping
    unmapRegion(vaddr, REGULAR_PAGE_SIZE, false, true);

    return newKPVS;
}

/*! Allocates a region of virtual memory
 *  \param pages Number of pages to be allocated
 *  \param flags Flags describing the attributes of the allocated region
 *  \return Virtual address of the newly allocated region
 */
uintptr_t virtualMemoryManager::instanceAllocateRegion( size_t pages, uint64_t flags )
{
    if(!pages)
    {
        kprintf("Error: Call to allocate region does not meet minimum length!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

	//Get a chunk of virtual space
	uintptr_t address = allocateSpace( pages );

	//Allocate and map physical pages into the region
	mapPages( 0, address, pages, flags, true );
	return address;
}

/*! Maps a physical region of memory to an available virtual address
 *  \param phys Physical address to map
 *  \param length Length of the region in bytes
 *  \param flags Page flags describing the properties of the desired mapping
 *  \return Address of the virtual region
 */
uintptr_t virtualMemoryManager::instanceMapRegion( uintptr_t phys, uintptr_t length, uint64_t flags )
{
	uintptr_t pages = (length + PAGE_OFFSET_MASK) / REGULAR_PAGE_SIZE;
	spinLock lockInstance( &vmmLock );
    lockInstance.acquire();

	if(!pages)
    {
        kprintf("Error: Call to map region does not meet minimum length!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    //Allocate space like we normally would if the address is not aligned to a large page boundary already
	uintptr_t address = allocateSpace( pages, phys & LARGE_PAGE_MASK );


	// Map the region into memory at the address we've acquired
	mapPages( phys, address, pages, flags, false );
    lockInstance.release();

    regionMapping mapping( phys, address, length );

    mappingTranslations.insert( mapping );
	return address;
}

/*! Unmaps a previously mapped, physically contiguous region of memory
 *  \param phys Physical base address of the region
 *  \param length Length of the region in bytes
 *  \return True if the region was unmapped successfully, false if provided mapping data was invalid
 */
bool virtualMemoryManager::instanceUnmapPhysRegion( uintptr_t phys, uintptr_t length )
{
	spinLockInstance lockInstance( &vmmLock );
	//Construct a comparison object to make sure the region is actually mapped
	regionMapping searchMapping( phys, 0, length );

	const regionMapping *foundMapping = mappingTranslations.find(searchMapping);
	if( !foundMapping )
		return false;
	else if( foundMapping->removeDependent() )
	{
		regionMapping obj(*foundMapping);
		mappingTranslations.remove(searchMapping);
		return instanceUnmapRegion( obj.virtualBase, obj.regionLength, false, true );
	}
	else
		return true;
}

/*! Unmaps a region of virtual memory
 *  \param virt Virtual address to begin unmapping from
 *  \param length Length of the region in bytes
 *  \param save Save the physical pages unmapped by the operation
 *  \param saveVirt Save the virtual range associated with the region
 *  \return True if operation is successful, false otherwise
 */
bool virtualMemoryManager::instanceUnmapRegion(uintptr_t virt, const uintptr_t length, const bool save, const bool saveVirt)
{
	const uintptr_t virtSave = virt;
	uintptr_t tabIndex = getMappingOffset(virt);
	uintptr_t tabOffset = tabIndex & 0x1FFU;
	uint64_t dirIndex = tabIndex>>9;
	//uint32_t pdpIndex = dirIndex>>9;
	//uint32_t pml4Index = pdpIndex>>9;

    //Set up the relevant paging structures
	uint64_t * __restrict__ tab = const_cast <uint64_t *> ( &selfPageTable[tabIndex] );
	uint64_t * __restrict__ tabBase = const_cast <uint64_t *> (&selfPageTable[tabIndex - tabOffset]);
	uint64_t * __restrict__ dir = const_cast <uint64_t *> ( &selfPageDir[dirIndex] );
	uint32_t tc = 512U - tabOffset;

    uint32_t preEmptyCount = 0;
    // Count the number of empty precursor entries in the page directory
    while(tabOffset)
    {
        if(*tabBase == 0)
            preEmptyCount++;
        tabBase++;
        tabOffset--;
    }


	uintptr_t remaining = length/REGULAR_PAGE_SIZE;

	do
	{
        // If there's a 2MB page remove it
		if( (*dir & PAGE_LARGE) && (remaining >= 512) && !(tabIndex & 0x1FFU) )
		{
		    if(save)
            {
                uint64_t page = *dir & ~(PAGE_NX | PAGE_OFFSET_MASK);
                physicalMemory::reclaimLargePage(page);
            }
			*dir = 0;
			dir++;
			remaining -= 512;
			PAGE_INVALIDATE( virt );
			virt += LARGE_PAGE_SIZE;
			tab = &tab[512];
		}
		else
		{
		    if( tc > remaining )
				tc = remaining;
            uint32_t tcSave = tc;

		    remaining -= tc;
            // Remove pages from the page table
			while(tc)
			{
				if(save)
                {
                    uint64_t page = *tab & PAGE_ADDRESS_MASK;
                    physicalMemory::reclaimPage(page);
                }
				*tab = 0;
				tab++;
                PAGE_INVALIDATE( virt );
				virt += REGULAR_PAGE_SIZE;
				tc--;
			}

			uint32_t tabRem = (512 - (reinterpret_cast <uintptr_t> (tab) & PAGE_OFFSET_MASK)/sizeof(uint64_t)) & 0x1FFU;
			//If the number of page table entries removed and empty precursor entries are equal to a full directory remove the directory entry too
			if(preEmptyCount)
                tcSave += preEmptyCount;

            while(tabRem)
            {
               if(*tab)
                break;
               else
               {
                   tab++;
                   tcSave++;
                   tabRem--;
                }
            }

            if(tcSave == 512)
            {
               uint64_t page = *dir & PAGE_ADDRESS_MASK;
               *dir = 0;
               physicalMemory::reclaimPage(page);
            }

            dir++;

			tc = 512;
			preEmptyCount = 0;
            tabIndex = 0;

		}
	} while( remaining );

    if( (virtSave >= KERNEL_VSPACE_START) && saveVirt)
        partitionAndInsertRegion(virtSave, length);

    return true;
}

void virtualMemoryManager::attemptUnmapHigher(const uint32_t pdpIndex, const uint32_t l4Index, const uintptr_t pageCount)
{
    uint32_t pdpSpan = ((pageCount>>18) + pdpIndex%512)>>18;
    uint32_t l4Span = (pdpSpan + pdpIndex%512)>>9;

    kprintf("UnmapHigh - pdpSpan: %u l4Span: %u PC: %u\n", pdpSpan, l4Span, pageCount);
    if(!pdpSpan)
    {
        uint64_t *pd = reinterpret_cast <uint64_t *> (SELF_REF_PAGE_DIR + pdpIndex*REGULAR_PAGE_SIZE);
        uint32_t entryCount = 0;
        for(int i = 0; i < 512; i++)
        {
            if(pd[i] & PAGE_PRESENT)
                entryCount++;
        }
        if(!entryCount)
        {
            kprintf("No entries in pdp, reclaiming...\n");
            uint64_t *pdpEntry = reinterpret_cast<uint64_t *> (SELF_REF_PDPTR + pdpIndex*sizeof(uint64_t));
            uint64_t page = *pdpEntry;
            page &= ~PAGE_OFFSET_MASK;
            *pdpEntry = 0;
            physicalMemory::reclaimPage(page);
            PAGE_INVALIDATE(pd);
        }
    }
    else
    {
        kprintf("ERROR: Multi-entry PDP unmap NYI!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

    if(!l4Span)
    {
        uint64_t *pdp = reinterpret_cast <uint64_t *> (SELF_REF_PDPTR + l4Index*REGULAR_PAGE_SIZE);
        uint32_t entryCount = 0;
        for(int i = 0; i < 512; i++)
        {
            if(pdp[i] & PAGE_PRESENT)
                entryCount++;
        }
        if(!entryCount)
        {
            kprintf("No entries in l4, reclaiming...\n");
            uint64_t *l4Entry = reinterpret_cast<uint64_t *> (SELF_REF_PML4 + l4Index*sizeof(uint64_t));
            uint64_t page = *l4Entry;
            page &= ~PAGE_OFFSET_MASK;
            *l4Entry = 0;
            physicalMemory::reclaimPage(page);
            PAGE_INVALIDATE(pdp);
        }
    }
    else
    {
        kprintf("ERROR: Multi-entry L4 unmap NYI!\n");
        asm volatile ("cli\n"
                      "hlt\n"::);
    }

}

/*! Initializes the virtual memory subsystem
 *  \param vSpaceStart Current start address of virtual space in the kernel
 */
void virtualMemoryManager::init( uintptr_t vSpaceStart )
{
    kpvs = selfPageL4[511] & ~(PAGE_OFFSET_MASK | PAGE_NX);
    Memory::copy(kernL4, &selfPageL4[256], REGULAR_PAGE_SIZE/2);
	vmm = new virtualMemoryManager( vSpaceStart );
}

/*! Partitions and inserts a range of memory based on the alignment and length rules
 *  enforced by the region allocator.
 *  \param base Base address of the region to be inserted
 *  \param length Length of the region to be inserted
 */
void virtualMemoryManager::partitionAndInsertRegion( uintptr_t base, uintptr_t length )
{
    uintptr_t sizeMask = REGULAR_PAGE_SIZE;
    uint8_t index = 0;
    while(sizeMask <= length)
    {
        if( base & sizeMask )
        {
            memoryRegion *region = memoryCache::allocate( base, sizeMask );
            base += sizeMask;
            insertFixedRegion(index, region, sizeMask>>fixedTotalBits);
            length -= sizeMask;
        }
        index++;
        sizeMask <<= 1;
    }

    while(length)
    {
        if(length & sizeMask)
        {
            length -= sizeMask;
            memoryRegion *region = memoryCache::allocate( base, sizeMask );
            insertFixedRegion(index, region, sizeMask>>fixedTotalBits);
            base += sizeMask;
        }
        index--;
        sizeMask >>= 1;
    }
}

/*! Maps pages into virtual memory at a given address
 *  \param phys Physical start address, unnecessary if allocating
 *  \param linear Virtual address to map pages into
 *  \param pages Number of pages to be mapped into virtual memory
 *  \param flags Page flags describing the attributes of the region
 *  \param allocate Allocation flags, true if memory should be allocated for the region.  False if a contiguous physical region is being mapped
 */
void virtualMemoryManager::mapPages( const uintptr_t phys, const uintptr_t linear, const size_t pages, const uint64_t flags, const bool allocate )
{
	uint64_t *tab, *dir, offset, remaining, physPtr, lPtr;

    remaining = pages;
	physPtr = phys;
	lPtr = linear;

	//Trim off the sign extended bits
	offset = getMappingOffset(linear);
	uint32_t tabCount = 512U - ( offset & 0x1ffU );

	//Initialize the pointers for our page directory and page table entries
	tab = &selfPageTable[offset];
	offset >>= 9;
	dir = &selfPageDir[offset];

	while( remaining )
	{
		//Sanity check for the directory entry
		if( !*dir )
		{
			if( ( remaining >= 512 ) && !( (lPtr | physPtr) & LARGE_PAGE_MASK ) )
			{
			    //Map large page if requested
				if( allocate )
					*dir = physicalMemory::allocateLargePage() | flags | PAGE_LARGE;
				else
				{
                    *dir = physPtr | flags | PAGE_LARGE;
					physPtr += LARGE_PAGE_SIZE;
				}
				dir++;
				tab = &tab[512];
				remaining -= 512;
				PAGE_INVALIDATE( lPtr );
				lPtr += LARGE_PAGE_SIZE;
				tabCount = 512;
				continue;
			}
			else
			{
			    //Allocate a new page for the page table structure
			    uintptr_t newPage = physicalMemory::allocatePage();
			    *dir = newPage | PAGE_PRESENT | PAGE_WRITE | (flags & PAGE_USER);
				PAGE_INVALIDATE( tab );
				uintptr_t tabBase = reinterpret_cast <uintptr_t> ( tab );
				tabBase &= ~PAGE_OFFSET_MASK;
				Memory::set( reinterpret_cast <void *> ( tabBase ), 0, REGULAR_PAGE_SIZE );
			}
		}
        dir++;


		//Update our page counter
		if( tabCount > remaining )
			tabCount = remaining;
		remaining -= tabCount;

		if( allocate )
		{
			//Create a counter for page color values
			uint32_t currentColor = ( lPtr & colorMask )>>12;
			while( tabCount )
            {
                /*
                if(*tab)
                {
                    kprintf("Error table entry already populated! Entry: %X Value: %X\n", tab, *tab);
                    asm volatile ("cli\n"
                                  "hlt\n"::);
                }
                */
				uintptr_t page = physicalMemory::allocatePageBin( currentColor );
				//Allocate a page corresponding to the proper color and insert it into the page table with the proper flags
				*tab = page | flags;
				//kprintf("Tab entry: %X\n", *tab);
				tab++;
				//Update our color counter
				currentColor++;
				currentColor &= colorFieldMask;
				PAGE_INVALIDATE( lPtr );
				lPtr += REGULAR_PAGE_SIZE;
				tabCount--;
			}

		}
		else
		{
			while(tabCount)
			{
				//Map the proper physical address into the page table with the correct flags
				*tab = physPtr | flags;
				tab++;
				physPtr += REGULAR_PAGE_SIZE;
				PAGE_INVALIDATE( lPtr );
				lPtr += REGULAR_PAGE_SIZE;
				tabCount--;
			}
		}
		tabCount = 512;
	}

}

/*! Maps an array of pages into the current virtual address space
 *
 * \param pages Physical pages to map
 * \param addr  Target virtual base address
 * \param flags Page control flags
 */
void virtualMemoryManager::mapPageArray( const Vector<pmmEntry> &pages, const uintptr_t addr, const uint64_t flags )
{
	uint64_t * __restrict__ tab, * __restrict__ dir, offset, remaining, lPtr;
    uintptr_t pageEntry = 0;


    remaining = pages.length();
	lPtr = addr;

	//Trim off the sign extended bits
	offset = getMappingOffset(addr);
	uint32_t tabCount = 512 - ( offset & 0x1ff );
    tabCount = min(tabCount, remaining);

	//Initialize the pointers for our page directory and page table entries
	tab = &selfPageTable[offset];
	offset >>= 9;
	dir = &selfPageDir[offset];

	do
	{
	    // Map the page directory if it's not present
		if(!*dir)
		{
			const uint8_t dirColor = (reinterpret_cast <uintptr_t> (dir) / sizeof(uint64_t)) & colorFieldMask;
			*dir = physicalMemory::allocatePageBin(dirColor) | (PAGE_PRESENT | PAGE_WRITE | (flags & PAGE_USER));
			PAGE_INVALIDATE(tab);
			dir++;
		}
		remaining -= tabCount;
		do
		{
		    //Map the actual page entry
            *tab = pages[pageEntry] | flags;
            PAGE_INVALIDATE(lPtr);
            tab++;
            tabCount--;
            pageEntry++;
            lPtr += REGULAR_PAGE_SIZE;
		} while(tabCount);
        tabCount = min(remaining, 512);
	} while( remaining );
}

/********************************
 *  Static feature accessors    *
 ********************************/

/*! Accessor for the instanceAllocateRegion function
 *  \param pages Number of pages to be allocated and mapped
 *  \param flags Flags describing the attributes of the region being mapped
 *  \return Virtual address of the allocated region
 *  \sa uintptr_t virtualMemoryManager::instanceAllocateRegion(size_t pages, uint64_t flags)
 */
uintptr_t virtualMemoryManager::allocateRegion( size_t pages, uint64_t flags )
{
    spinLockInstance instanceLock(&vmmLock);
	uintptr_t addr = vmm->instanceAllocateRegion( pages, flags );
	memoryCache::maintainCache();
	return addr;
}

/*!  Accessor for the instanceMapRegion function
 *  \param phys Physical address to map
 *  \param length Length of the region to map, in bytes
 *  \param flags Flags describing the attributes of the region
 *  \return Virtual address that the region has been mapped to
 *  \sa uintptr_t virtualMemoryManager::instanceMapRegion(uintptr_t phys, uintptr_t length, uint64_t flags)
 */
uintptr_t virtualMemoryManager::mapRegion( uintptr_t phys, uintptr_t length, uint64_t flags )
{
    uintptr_t addr = vmm->instanceMapRegion( phys, length, flags );
	memoryCache::maintainCache();
	return addr;
}

/*! Accessor for the instanceUnmapPhysRegion function
 *  \param phys Physical base address of the region
 *  \param length Length of the region in bytes
 *  \return True if the region was unmapped successfully, false if the provided mapping information was invalid
 */
bool virtualMemoryManager::unmapPhysRegion( uintptr_t phys, uintptr_t length )
{
	bool result = vmm->instanceUnmapPhysRegion( phys, length );
	memoryCache::maintainCache();
	return result;
}

/*! Accessor for the instanceUnmapRegion function
 *  \sa virtualMemoryManager::instanceUnmapRegion( uintptr_t virt, uintptr_t length)
 *  \param virt Virtual base address of the region to be unmapped
 *  \param length Length of the region in bytes
 *  \param save Indicates to the virtual memory manager if the pages freed from this operation should be returned to the PMM or not
 *  \param virtSave Indicates that the virtual range is to be saved
 *  \return True if the operations is successful, false otherwise
 */
bool virtualMemoryManager::unmapRegion( uintptr_t virt, uintptr_t length, bool save, bool virtSave )
{
    spinLockInstance instanceLock(&vmmLock);

    uintptr_t roundedLength = (length + PAGE_OFFSET_MASK) & ~PAGE_OFFSET_MASK;

    bool result = vmm->instanceUnmapRegion(virt, roundedLength, save, virtSave);
	memoryCache::maintainCache();
	return result;
}

static void freeStructEntry(uint64_t &entry)
{
    uintptr_t frame = entry;
    entry = 0;
    frame &= PAGE_ADDRESS_MASK;
    physicalMemory::reclaimPage(frame);
}

void virtualMemoryManager::unmapUserStructures(void)
{
    for(uint32_t pml4Index = 0; pml4Index < 256; pml4Index++)
    {
        if(selfPageL4[pml4Index])
        {
            uintptr_t offset = pml4Index*512;
            for(uint32_t pdpIndex = 0; pdpIndex < 512; pdpIndex++)
            {
                if(selfPageDirPtr[offset + pdpIndex])
                {
                    uintptr_t dirBase = (offset + pdpIndex)*512;
                    for(uint32_t dirIndex = 0; dirIndex < 512; dirIndex++)
                    {
                        if(selfPageDir[dirBase + dirIndex])
                        {
                            uintptr_t frame = selfPageDir[dirBase + dirIndex];
                            if(frame & PAGE_LARGE)
                                continue;

                            freeStructEntry(selfPageDir[dirBase + dirIndex]);
                        }
                    }
                    freeStructEntry(selfPageDirPtr[offset + pdpIndex]);
                }
            }
            freeStructEntry(selfPageL4[pml4Index]);
        }
    }
}

/*! Maps higher level paging structures into memory (PDPTR and PML4 entries)
 *  \param laddr Linear address of the page directory entry which needs it's structures mapped
 */
void virtualMemoryManager::mapStructures( uintptr_t laddr, bool isUser )
{
	uintptr_t offset = laddr;
    //If this function is being called the faulting address is somewhere within the kernel side page directory span
	offset -= SELF_REF_PAGE_DIR;
    // Offset should be specific page directory entry * 8, the size of a page table entry.  We want the page directory pointer index so we need to divide by 512*8 or 12 bits
	offset >>= 12;

	//Make everything present and writable, permissions will be enforced in lower page directory structures
	uintptr_t flags = PAGE_PRESENT | PAGE_WRITE;
	if(isUser)
        flags |= PAGE_USER;

	//Lock up
	spinLockInstance lockInstance( &hlaLock );

    uint64_t * const pdptr = &selfPageDirPtr[offset];
	offset >>= 9;
	uint64_t * const pml4 = &selfPageL4[offset];

	if( !*pml4 )
	{
	    uintptr_t pdpBaseAddr = reinterpret_cast<uintptr_t> (pdptr);
        pdpBaseAddr &= ~PAGE_OFFSET_MASK;

	    if(offset >= 256)
        {
            uint32_t kl4Off = offset - 256;
            if(kernL4[kl4Off])
            {
                *pml4 = kernL4[kl4Off];
                PAGE_INVALIDATE( pdpBaseAddr );
            }
            else
            {
                uintptr_t newL4Page = physicalMemory::allocatePage();
                *pml4 =  newL4Page | flags;
                PAGE_INVALIDATE( pdpBaseAddr );
                Memory::set( reinterpret_cast<void *>(pdpBaseAddr), 0, REGULAR_PAGE_SIZE );
                kernL4[kl4Off] = newL4Page | flags;
            }
        }
        else
        {
            uintptr_t newL4Page = physicalMemory::allocatePage();
            *pml4 =  newL4Page | flags;
            PAGE_INVALIDATE( pdpBaseAddr );
            Memory::set( reinterpret_cast<void *>(pdpBaseAddr), 0, REGULAR_PAGE_SIZE );
        }
	}
	//Verify the PDPTR entry
	if( !*pdptr )
	{
        uintptr_t pdBaseAddr = laddr;
        pdBaseAddr &= ~PAGE_OFFSET_MASK;

        uintptr_t newPage = physicalMemory::allocatePage();
		*pdptr = newPage | flags;
		PAGE_INVALIDATE( pdBaseAddr );
		Memory::set( reinterpret_cast<void *>(pdBaseAddr), 0, REGULAR_PAGE_SIZE );
	}
}

/*! Attempts to retrieve the virtual address of a mapped physical address
 *  \param phys Physical address to translate into a virtual address
 *  \param accessLength Length of the access request desired
 *  \return Linear address corresponding to the physical address if a mapping exists, zero otherwise
 */
uintptr_t virtualMemoryManager::getVirtualAddress( uintptr_t phys, uintptr_t accessLength )
{
	//Construct a regionMapping object representing the base address requested and the potential length of access requests upon it
	regionMapping request( phys, 0, accessLength );

	//Search our mapping tree for the object
	const regionMapping *mapping = mappingTranslations.find( request );

	//If no mapping is found then request is invalid
	if( !mapping )
		return 0;

	//Calculate and return the virtual address corresponding to the physical address requested
	uintptr_t offset = phys - mapping->physBase;
	return ( mapping->virtualBase + offset );
}

uintptr_t virtualMemoryManager::registerMmioMapping( const uintptr_t phys, const uint32_t accessLength)
{
	uintptr_t pOffset = phys & PAGE_OFFSET_MASK;
	//Construct a regionMapping object representing the base address requested and the potential length of access requests upon it
	regionMapping request( phys, 0, accessLength );
	//Search our mapping tree for the object
	const regionMapping *mapping = mappingTranslations.find( request );
	if( !mapping )
	{
		uintptr_t pBase = phys - pOffset;
		uintptr_t pages = pOffset + accessLength + PAGE_OFFSET_MASK;
		pages /= REGULAR_PAGE_SIZE;
		return vmm->instanceMapRegion(pBase, pages*REGULAR_PAGE_SIZE, PAGE_PRESENT | PAGE_WRITE | PAGE_CACHE_DISABLE) + pOffset;
	}
	else
	{
		mapping->addDependent();
		return ( mapping->virtualBase + pOffset);
	}
}

/*! Allocates a new kernel stack
 *  \param pages Size of the stack in pages
 *  \return Pointer to the new stack space that's been allocated
 */
regionObj64 virtualMemoryManager::allocateKernelStack( uint32_t pages )
{
	//Lock up
	spinLockInstance lockInstance( &vmmLock );
	uintptr_t space = vmm->allocateSpace( pages + 2 );

	vmm->mapPages( 0, space, 1, 0, false );
	space += REGULAR_PAGE_SIZE;
	uintptr_t bottom = space+REGULAR_PAGE_SIZE;
	vmm->mapPages( 0, bottom, pages, PAGE_WRITE | PAGE_PRESENT, true );
	vmm->mapPages( 0, space + (pages+1)*REGULAR_PAGE_SIZE, 1, 0, false );

	Memory::set( reinterpret_cast<void *>(bottom), 0, pages*REGULAR_PAGE_SIZE);
	return regionObj64(bottom, pages*REGULAR_PAGE_SIZE);
}

/*! Retrieves the physical address of the kernel's master virtual memory mapping structure,
 *  e.g PML4 for the x86_64 architecture.
 *  \return Address of the structure
 */
uintptr_t virtualMemoryManager::getKernelPrimaryVirtualStructure()
{
	return kpvs;
}

//! Unmaps the identity mapping of the first 2MB of memory
void virtualMemoryManager::unmapIdentity()
{
    selfPageL4[0] = 0;

    for(uintptr_t i = 0; i < 1024; i++)
        PAGE_INVALIDATE(i*REGULAR_PAGE_SIZE);
}

/*! Verifies the access rights of a set of page addresses
 *  \param address Base virtual address to start verification at
 *  \param length Length of the verification in bytes
 *  \param attributes Attribute mask to compare page mappings against
 *  \return True if the mapping attributes are available across the provided range, false otherwise
 */
bool virtualMemoryManager::verifyAccess( uintptr_t address, uint64_t length, uint64_t attributes )
{
	uint64_t offset = getMappingOffset(address);

	uint64_t pages = (address & PAGE_OFFSET_MASK) + PAGE_OFFSET_MASK;
	pages += length;
	pages /= REGULAR_PAGE_SIZE;

	if( !( selfPageL4[offset>>27] & PAGE_PRESENT ) )
		return false;
	else if( !( selfPageDirPtr[offset>>18] & PAGE_PRESENT ) )
		return false;
	else if( !( selfPageDir[offset>>9] & PAGE_PRESENT ) )
		return false;

	uint32_t tcOffset = 512;
	tcOffset -= offset & PAGE_STRUCTURE_MASK;

	while( pages )
	{
		//Verify the page directory entry
		if( !( offset & PAGE_STRUCTURE_MASK ) )
		{
			uintptr_t dirPtrOff = offset >> 9;
			//Verify the page directory pointer entry, if we're on a boundry
			if( !( dirPtrOff & PAGE_STRUCTURE_MASK ) )
			{
				uint32_t l4Off = dirPtrOff >> 9;
				//Verify the PML4 entry if we're on a boundry
				if( !( l4Off & PAGE_STRUCTURE_MASK ) )
					if( !( selfPageL4[l4Off>>9] & PAGE_PRESENT ) )
						return false;
				if( !( selfPageDirPtr[l4Off] & PAGE_PRESENT ) )
					return false;
			}
			if( !( selfPageDir[dirPtrOff] & PAGE_PRESENT ) )
				return false;
			else if( ( selfPageDir[dirPtrOff] & ( PAGE_LARGE | attributes ) ) == ( PAGE_LARGE | attributes ) )
			{
				if( pages >= 512 )
				{
					pages -= 512;
					continue;
				}
				else
					break;
			}

		}
		if( tcOffset > pages )
			tcOffset = pages;
		pages -= tcOffset;
		//Verify individual page table entries
		while( tcOffset )
		{
			if( ( selfPageTable[offset] & attributes ) != attributes )
				return false;
			offset++;
			tcOffset--;
		}
		tcOffset = 512;
	}
	return true;
}

/*! Retrieves a physical address from a provided virtual address value
 *  \param virt Virtual address to obtain a coressponding physical address for
 *  \param phys Reference used to return the physical address
 *  \return True if a physical address exists for the requested virtual address, false if there is no page present
 */
bool virtualMemoryManager::getPhysicalAddress( const uintptr_t virt, uintptr_t &phys )
{
	uint64_t offset = getMappingOffset(virt);

	phys = 0;

	if( !( selfPageL4[offset>>27] & PAGE_PRESENT ) )
		return false;
	else if( !( selfPageDirPtr[offset>>18] & PAGE_PRESENT ) )
		return false;
	else if( !( selfPageDir[offset>>9] & PAGE_PRESENT ) )
		return false;
	else if( selfPageDir[offset>>9] & PAGE_LARGE )
	{
		phys = ( selfPageDir[offset>>9] & LARGE_PAGE_ADDRESS_MASK ) + ( virt & LARGE_PAGE_MASK );
		return true;
	}
	else if( selfPageTable[offset] & PAGE_PRESENT )
	{
		phys = ( selfPageTable[offset] & PAGE_ADDRESS_MASK ) + ( virt & PAGE_OFFSET_MASK );
		return true;
	}
	else
		return false;
}

void virtualMemoryManager::implementMapping( const Vector<pmmEntry> &mapping, const uintptr_t linearAddress, const physaddr_t flags )
{
	vmm->mapPageArray( mapping, linearAddress, flags );
}

void virtualMemoryManager::implementDirMap( const Vector<physaddr_t> &mapping, const virtaddr_t virtAddress, const physaddr_t flags )
{
    uintptr_t tabOffset = getMappingOffset(virtAddress);

    uint64_t *dir = &selfPageDir[tabOffset>>9];

    for(size_t entry = 0; entry < mapping.length(); entry++)
        dir[entry] = mapping[entry] | flags;
}

uintptr_t virtualMemoryManager::createMapping( Vector<physaddr_t> &mapping, const size_t pagesNeeded )
{
    uintptr_t addr = vmm->allocateSpace(pagesNeeded);
	uintptr_t tabOffset = getMappingOffset(addr);
	uint32_t colorCount = tabOffset & colorFieldMask;

	for(size_t i = 0; i < pagesNeeded; i++)
    {
        uintptr_t page = physicalMemory::allocatePageBin(colorCount);
        mapping.insert(page);
        colorCount++;
        colorCount &= colorFieldMask;
    }

    vmm->mapPageArray(mapping, addr, PAGE_PRESENT | PAGE_WRITE);
    return addr;
}

uintptr_t virtualMemoryManager::createLargeMapping( Vector<physaddr_t> &mapping, const size_t largePagesNeeded)
{
    uintptr_t addr = vmm->allocateSpace(largePagesNeeded*512);

    uintptr_t tabOffset = getMappingOffset(addr);

    uintptr_t dirOffset = tabOffset>>9;

    uint64_t *dir = &selfPageDir[dirOffset];

    if(dirOffset)
        mapStructures(addr);
    for(size_t index = 0; index < largePagesNeeded; index++)
    {
        if(((dirOffset + index) & 0x1FFU) == 0)
            mapStructures(addr + LARGE_PAGE_SIZE*index);
        dir[index] = physicalMemory::allocateLargePage() | PAGE_LARGE | PAGE_PRESENT | PAGE_WRITE;
    }


    return addr;
}

void virtualMemoryManager::changePermissions(const uintptr_t vBase, const size_t pageCount, const uint64_t newPermissions)
{
    uintptr_t vPtr = vBase;
    size_t pagesRem = pageCount;

    /* Trim the highest 16 bits to deal with sign extension and then divide by the page bits in order for a page index value*/
    uintptr_t tabOff = (vBase<<16)>>(16 + PAGE_OFFSET_BITS);
    uintptr_t dirOff = tabOff/512;

    while(pagesRem)
    {

        if( selfPageDir[dirOff] & PAGE_LARGE )
        {
            /* Invalid remap request */
            if((vPtr & LARGE_PAGE_ADDRESS_MASK ) || (pagesRem < 512) )
                return;
            /* Update the large page entry with the new permissions */
            uint64_t entry = selfPageDir[dirOff];
            entry &= ~LARGE_PERMISSION_FLAGS;
            entry |= newPermissions;
            selfPageDir[dirOff] = entry;
            dirOff++;
            tabOff += 512;
            pagesRem -= 512;
            PAGE_INVALIDATE(vPtr);
            vPtr += LARGE_PAGE_SIZE;
        }
        else
        {
            size_t tabCount = min(512, pagesRem);
            pagesRem -= tabCount;
            do
            {
                uint64_t entry = selfPageTable[tabOff];
                entry &= ~REG_PERMISSION_FLAGS;
                entry |= newPermissions;
                selfPageTable[tabOff] = entry;
                tabOff++;
                tabCount--;
                PAGE_INVALIDATE(vPtr);
                vPtr += REGULAR_PAGE_SIZE;
            } while(tabCount);
        }
    }
}

/*! Perform a quick mapping of a normal page
 *  \param addr Physical address of the page to map
 *  \return Pointer to the quick mapped page
 */
void *virtualMemoryManager::quickMapPage(const uintptr_t addr)
{
    uintptr_t qaddr = localState::getQuickMapAddr();
    physaddr_t *entry = localState::getQuickMapEntry();

    *entry = addr | PAGE_PRESENT | PAGE_WRITE;
    PAGE_INVALIDATE(qaddr);

    return reinterpret_cast<void *> (qaddr);
}

//! Clears the regular quick mapping for the current processor
void virtualMemoryManager::quickMapClear(void)
{
    uintptr_t qaddr = localState::getQuickMapAddr();
    physaddr_t *entry = localState::getQuickMapEntry();

    *entry = 0;
    PAGE_INVALIDATE(qaddr);
}

/*! Perform a quick mapping of a large page
 *  \param addr Physical address of the page to map
 *  \return Pointer to the quick mapped page
 */
void *virtualMemoryManager::quickMapLargePage(const uintptr_t addr)
{
    uintptr_t qaddr = localState::getQuickMapLargeAddr();
    physaddr_t *entry = localState::getQuickMapLargeEntry();

    *entry = addr | PAGE_PRESENT | PAGE_WRITE | PAGE_LARGE;
    PAGE_INVALIDATE(qaddr);

    return reinterpret_cast<void *> (qaddr);
}

//! Clears the large quick mapping for the current processor
void virtualMemoryManager::quickMapClearLarge(void)
{
    uintptr_t qaddr = localState::getQuickMapLargeAddr();
    physaddr_t *entry = localState::getQuickMapLargeEntry();

    *entry = 0;
    PAGE_INVALIDATE(qaddr);
}

/*! Allocates the quick mapping entries for a processor
 *  \param qmapAddr Return virtual address for regular page sized mapping
 *  \param qmapEntry Return pointer for the mapping entry
 *  \param lqmapAddr Return lqmapAddr Return virtual address of the large page quick mapping
 *  \param lqmapEntry Return entry pointer for the large quick mapping
 *  \return True if all mappings could be successfully allocated, false otherwise
 */
bool virtualMemoryManager::allocateQuickMapEntries(uintptr_t &qmapAddr, physaddr_t *&qmapEntry, uintptr_t &lqmapAddr, physaddr_t *&lqmapEntry)
{
    qmapAddr = vmm->allocateSpace(1);
    if(!qmapAddr)
        return false;
    qmapEntry = &selfPageTable[getMappingOffset(qmapAddr)];

    lqmapAddr = vmm->allocateSpace(512, LARGE_PAGE_SIZE);
    if(!lqmapAddr)
        return false;
    lqmapEntry = &selfPageDir[getMappingOffset(lqmapAddr)>>9];

    return true;
}

/*! Swaps a page mapping by copying it to a new page mapping
 *  \param addr Virtual address of the page to be updated
 *  \param flags Mapping flags
 *  \param savePage Returns the prior page to the allocator if true
 */
void virtualMemoryManager::copySwap(const uintptr_t addr, const physaddr_t flags, const bool savePage)
{
    uintptr_t addrBase = addr & ~PAGE_OFFSET_MASK;
    uintptr_t offset = getMappingOffset(addr);

    uintptr_t ptOffset = offset;
    uintptr_t pdOffset = offset>>9;

    physaddr_t *pdEntry = &selfPageDir[pdOffset];
    physaddr_t *ptEntry = &selfPageTable[ptOffset];

    if(*pdEntry & PAGE_LARGE)
    {
        //Allocate the page, if there's a delay here it's no big deal
        physaddr_t page = physicalMemory::allocateLargePage();
        physaddr_t oldPage = *pdEntry & LARGE_PAGE_ADDRESS_MASK;

        //No interruptions to keep the quick mappings sane
        uint64_t istate = Processor::interruptDisable();
        //Map & copy
        void *tmp = quickMapLargePage(page);
        Memory::copy(tmp, reinterpret_cast <void *> (addrBase), LARGE_PAGE_SIZE);
        quickMapClearLarge();
        Processor::interruptRestore(istate);

        if(savePage)
            physicalMemory::reclaimLargePage(oldPage);
        //Update the mapping
        *pdEntry = page | flags | PAGE_LARGE;
    }
    else
    {
        //Allocate a new page to be copied over
        physaddr_t page = physicalMemory::allocatePageBin((addrBase>>PAGE_OFFSET_BITS) & colorFieldMask);
        physaddr_t oldEntry = *ptEntry;
        physaddr_t oldPage = oldEntry & PAGE_ADDRESS_MASK;

        //Keep the quick mapping exclusive by disabling interrupts for the mapping process
        uint64_t istate = Processor::interruptDisable();
        void *tmp = quickMapPage(page);
        Memory::copy(tmp, reinterpret_cast <void *> (addrBase), REGULAR_PAGE_SIZE);
        quickMapClear();
        Processor::interruptRestore(istate);

        if(savePage)
            physicalMemory::reclaimPage(oldPage);
        //Update the mapping
        *ptEntry = page | flags;
    }
    PAGE_INVALIDATE(addr);
}

/*! Converts a linear address into an absolute paging table offset
 *  \param addr Linear address to convert
 *  \return Absolute offset into the paging table
 */
inline uintptr_t virtualMemoryManager::getMappingOffset(const uintptr_t addr)
{
	uintptr_t offset = addr;
	offset <<= 16;
	offset >>= 28;

    return offset;
}

/*! Function used to crawl and dump the page tables for debugging purposes
 *  \param virt Virtual address to retrieving page table information for
 */
void virtualMemoryManager::printMapping( const uintptr_t virt )
{
	uintptr_t l4Entry, pdpEntry, pdEntry, maxAddr;
	uintptr_t offset = getMappingOffset(virt);

	maxAddr = physicalMemory::getMaxAddress();
	kprintf("Dumping page mappings for page @ %p - Max physical address: %X\n", virt, maxAddr);

	l4Entry = selfPageL4[offset>>27];
	kprintf( "L4: %X", l4Entry);
	if( !(l4Entry & PAGE_PRESENT ) || (l4Entry >= maxAddr ) )
    {
        kprintf("\n");
		return;
    }

	pdpEntry = selfPageDirPtr[offset>>18];
	kprintf( " PDPTR: %X", pdpEntry );

	if( !( pdpEntry & PAGE_PRESENT ) || (pdpEntry >= maxAddr) )
    {
        kprintf("\n");
        return;
    }

	pdEntry = selfPageDir[offset>>9];
	kprintf( "DIR: %X", pdEntry);
	if( !( pdEntry & PAGE_PRESENT ) && (pdEntry >= maxAddr) )
    {
        kprintf("\n");
		return;
    }

	if( pdEntry & PAGE_LARGE )
		return;
	else
	{
		kprintf( " TAB: %X\n", selfPageTable[offset] );
	}
}

static volatile bool maintainFlag = false;

inline void *operator new( size_t COMPILER_UNUSED size, uintptr_t addr )
{
	return reinterpret_cast <void *> ( addr );
}

static int cacheCount = 0;

//! Memory region cache maintenance call, updates swap structures
void memoryCache::maintainCache()
{
	if( !maintainFlag )
	{
		maintainFlag = true;
		for( int i = 0; i < 4; i++ )
			if( !swap[i] )
            {
                uintptr_t cacheAddr = vmm->instanceAllocateRegion( 1, PAGE_WRITE | PAGE_PRESENT );
				swap[i] = new (cacheAddr) memoryCache;
				cacheCount++;
            }
		maintainFlag = false;
	}
}

