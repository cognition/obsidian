#ifndef MEMORY_REGION_ALLOCATOR_H_
#define MEMORY_REGION_ALLOCATOR_H_

#include "objcache.h"
#include "fixed_region_allocator.h"

#define MEMORY_REGION_BIN_COUNT         28

/*! Abstract buddy allocator class for regions of memory.
 *  Progressively subdivides memory regions into sizes of REGULAR_PAGE_SIZE*(2^n), where n is the absolute depth
 *  of a bin list. \n
 *  \n
 *  General terminology definitions: \n
 *  \n
 *  Absolute depth: An index value representing the size of the region, essentially equal to [MSB(Size) - Bits reserved for page offsets] \n
 *  Variable Bins: A variable number of bins based upon the number of bits used for page coloring \n
 *  Fixed Bins: Bins used to hold regions which are too large to be affected by page coloring \n
 */
class memoryRegionAllocator : public fixedRegionAllocator
{
	public:
		memoryRegionAllocator(const uint8_t colorBitWidth);
		virtual ~memoryRegionAllocator();
	private:
        memoryRegion *carve(memoryRegion *reg, uint32_t tSize, uint32_t tIndex, uint32_t diff, const uint32_t size, const uint32_t align, const uint32_t index);

	protected:

		memoryRegion *buddyVariableAllocate( const uint32_t level, uint32_t subIndex, const uint32_t size, const uint32_t align );
        memoryRegion *variableRemoveRegion(const uint32_t bin, const uint32_t expectedSize);
		void variableInsertRegionBin( const uint32_t level, memoryRegion *region, bool attemptCombine = false );
		//! Number of bits used to store page color information
		const uint8_t colorBits;
		//! Number of colors
		const uint32_t colorCount;
		//! Number of 'variable' allocation bins
		const uint32_t variableBinCount;
		//! Mask representing the all possible bits of page color information
		const uint8_t colorFieldMask;
		//! Mask representing all the possible bits of page color information contained in an address
		const uint32_t colorMask;
		//! Number of 'fixed' bins
		const uint8_t fixedBinCount;
		//! Array holding memoryRegion lists, based on the number page colors used by a particular system
		memoryRegionBin *variableBins;
		uint64_t *bottomBinMap;
		uint64_t topBinMap;
};

#endif

