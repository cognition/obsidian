#ifndef MEMORY_BIN_H_
#define MEMORY_BIN_H_

#include "objcache.h"

#define PHYS_MEMORY_ARRAY_SIZE              124

#define DEFAULT_MEMORY_BIN_FILL_AMOUNT      1

/*! Describes a 'bin' of memory which contains pages of memory for a specific page color and provides functions including: \n
 *  \n
 *  A page stack allocator. \n
 *  Allocation of pages of memory from either the stack or physicalMemory objects. \n
 *  Doubly linked list pointers. \n
 */
class COMPILER_PACKED memoryBin
{
	public:
		memoryBin();
		void init();
		uint32_t getRank();
		void addAvailablePages(uint32_t value);
		void incUsed();
		void decUsed();
        //! Pointer to the previous memoryBin object in the eligibility list
		class memoryBin *prev;
		//! Pointer to the next memoryBin object in the eligibility list
		class memoryBin *next;
    private:
		//! Total number of pages currently in use for this bin
		uint32_t usedPages;
		//! Total number of possible available pages for this bin
		uint32_t availablePages;
};

#endif
