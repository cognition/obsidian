#include "region_allocator.h"
#include <cpu/bits.h>
#include "memory.h"
#include <io/io.h>
#include <cpu/localstate.h>
#include "virtual.h"

#define ARCH_MAX_PHYS_ADDR_BITS         48

//! Testing definition
#ifdef RALLOC_VALIDATE

#define assert(x)           if( !(x)) { \
                            kprintf("Assert \"%s\"failed @ %s:%d\n", #x, __FILE__, __LINE__);\
                            asm volatile ("cli\n" \
                                          "hlt\n"::); \
                             }

#else

#define assert(x)

#endif


//memoryRegionAllocator class

/*! Default constructor
 *  \param colorBitWidth Numbers of bits used to store color information
 */
memoryRegionAllocator::memoryRegionAllocator(const uint8_t colorBitWidth) : fixedRegionAllocator( ARCH_MAX_PHYS_ADDR_BITS, colorBitWidth ),
	colorBits( colorBitWidth ),
	colorCount( 1<<colorBits ),
	variableBinCount( colorCount<<1 ),
	colorFieldMask( colorCount-1 ),
	colorMask( ((uint32_t)colorFieldMask)<<12 ),
	fixedBinCount( MEMORY_REGION_BIN_COUNT - colorBits ),
	variableBins( new memoryRegionBin[variableBinCount] ),
	bottomBinMap( new uint64_t[colorCount]),
	topBinMap(0)
	{
        //Create the bin masks for each page sized bin
        for(int bin = 0; bin < colorCount; bin++)
        {
            uint32_t initIndex = (colorCount-1) + bin;

            uint64_t mask = 1;
            while(initIndex)
            {
                if(initIndex < 64)
                {
                    uint64_t lMask = 1U;
                    lMask <<= initIndex;
                    mask |= lMask;
                }
                initIndex--;
                initIndex >>= 1;
            }

            bottomBinMap[bin] = mask;
        }
	}



//! Destructor
memoryRegionAllocator::~memoryRegionAllocator()
{
	delete[] variableBins;
	delete[] bottomBinMap;
}

/*! Insersts a memoryRegion object into a list
 *  \param level Depth of the memory in the tree
 *  \param region memoryRegion object to be inserted
 *  \param attemptCombine True if the allocator should attempt to combine the returned region with larger ones
 */
void memoryRegionAllocator::variableInsertRegionBin( const uint32_t level, memoryRegion *region, const bool attemptCombine )
{
    //Starting level based index
    uint32_t depthIndex = (colorCount >> level) - 1;
    //Index into that level, based upon page color bits
    uint32_t subIndex = (region->getBase() & colorMask) >> (level + PAGE_OFFSET_BITS);

    uint32_t totalIndex = depthIndex + subIndex;

    //If we exceed the size of the tree there's a problem....
    assert(totalIndex < (colorCount*2 - 1));

    //Attempt to find border region for combination if it's requested
    if(attemptCombine)
    {
        if(depthIndex)
        {
            uint64_t regBase = region->getBase();
            uint32_t targetSubIndex = subIndex;
            uint64_t targetBase = region->getBase();
            uint64_t targetSize = 1;
            targetSize <<= (level + PAGE_OFFSET_BITS);

            //Determine if the region being inserted is the start or trailing region so we can request the proper pair
            if(subIndex & 0x1)
            {
                targetSubIndex--;
                targetBase -= region->getLength();
            }
            else
            {
                targetSubIndex++;
                targetBase += region->getLength();
            }

            uint32_t targetIndex = targetSubIndex + depthIndex;

            memoryRegionBin &tBin = variableBins[targetIndex];
            memoryRegion *target = tBin.retrieve(memorySpan(targetBase, targetSize) );

            if(target)
            {
                //Update the mask if a the bin was covered by and it emptied
                if(tBin.empty() && (targetIndex < 64))
                {
                    uint64_t mask = 1;
                    mask <<= targetIndex;

                    topBinMap &= ~mask;
                }

                //Determine and free the trailing region
                if(targetBase < regBase)
                {
                    memoryCache::free(region);
                    region = target;
                }
                else
                    memoryCache::free(target);

                region->stepAugmentLength();
                //Recursively insert the newly combined region
                variableInsertRegionBin(level+1, region, true);
                return;
            }
        }
        else
        {
            //Attempt combine & insertFixedRegion since we're in the top level bin

            uint64_t regBase = region->getBase();
            uint64_t regLength = region->getLength();
            uint64_t targetBase = regBase;

            if(regBase & regLength )
                targetBase -= regLength;
            else
                targetBase += regLength;

            memoryRegion *target = variableBins[0].retrieve(memorySpan(targetBase, regLength ) );
            if(target)
            {
                //Modify the mask if the last entry was removed
                if(variableBins[0].empty())
                {
                    uint64_t mask = 1;
                    topBinMap &= ~mask;
                }

                //Find which region is the base and free the trailing region
                if(targetBase < regBase)
                {
                    memoryCache::free(region);
                    region = target;
                }
                else
                    memoryCache::free(target);

                region->stepAugmentLength();
                insertFixedRegion(0, region, 0x01U);
                return;
            }
        }
    }

    //Combining was either not requested or did not succeed, put the region in the appropriate bin
    variableBins[totalIndex].insert(region);
    if(totalIndex < 64)
    {
        //If the bin is covered by the mask we know for sure there's something in it now
        uint64_t mask = 1;
        mask <<= totalIndex;
        topBinMap |= mask;
    }
}

/*! Removes a region from a provided bin
 *  \param bin Bin to remove the region from
 *  \param expectedSize Size of the region, really just for debugging purposes
 *  \return Pointer to the region retrieved or nullptr if none was found
 */
memoryRegion *memoryRegionAllocator::variableRemoveRegion(const uint32_t bin, const uint32_t expectedSize)
{
    //Make sure the requested index is a sane value
    assert(bin < (colorCount*2-1) );

    memoryRegion *reg = variableBins[bin].retrieveMinimum();
    if(reg)
    {
        //Check for corruption
        assert(reg->getLength() == expectedSize);
        //Update the mask if necessary
        if( (bin < 64) && variableBins[bin].empty() )
        {
            uint64_t mask = 1;
            mask <<= bin;
            mask = ~mask;
            topBinMap &= mask;
        }

    }
    return reg;
}

/*! Carves a larger region down to a desired size and alignment
 *  \param reg Initial oversized region
 *  \param tSize Initial split size, should be equal to half of the region size
 *  \param diff Level of the oversized region
 *  \param size Target region size
 *  \param align Desired alignment
 *  \param index Bin index of the target region
 *  \return A pointer to the region of the correct size and alignment
 */
memoryRegion *memoryRegionAllocator::carve(memoryRegion *reg, uint32_t tSize, uint32_t tIndex, uint32_t diff, const uint32_t size, const uint32_t align, const uint32_t index)
{
    assert(reg->getLength() == tSize*2);
    assert(reg->getLength() > size);

    while(tSize >= size)
    {
        tIndex = index>>diff;

        memoryRegion *split = reg->halve();
        if(align & tSize)
        {
            variableInsertRegionBin(diff, reg);
            reg = split;
        }
        else
        {
            variableInsertRegionBin(diff, split);
        }

        assert(tSize);
        diff--;
        tSize >>= 1;
    }

    assert(reg != nullptr);
    return reg;
}

/*! Cache-coloring aware buddy allocation function for variable bins
 *  \param depth Inverse B-Tree level to start at
 *  \param subIndex B-Tree level secondary index, specifies color affinity for a tree level
 *  \param size Size of the region to be allocated in bytes
 *  \param align Alignment value, used for finding offsets
 *  \param alignMask Alignment mask, used for finding offsets
 *  \return Pointer to the memoryRegion object that has been allocated
 */
memoryRegion *memoryRegionAllocator::buddyVariableAllocate( const uint32_t depth, const uint32_t subIndex, const uint32_t size, const uint32_t align )
{
	uint32_t depthIndex = (colorCount>>depth) - 1;
	uint32_t index = depthIndex + subIndex;

    //Make sure the requested size is sane
    assert(subIndex <= depthIndex);
    assert(depthIndex < colorCount);

    //Attempt to a find a region that fits the target index
    memoryRegion * __restrict__ ptr = variableRemoveRegion(index, size);

	if(ptr)
        return ptr;

    //If there wasn't such a region recursively split larger regions until we have one
    memoryRegion * reg;
    uint64_t tSize;
    uint32_t tIndex, diff;

    if(depthIndex)
    {
        tSize = size;
        tIndex = (index - 1)>>1;
        diff = depth;

        //If the region requested is beyond the map deal with all regions between it and the map
        while(tIndex >= 64)
        {
            reg = variableRemoveRegion(tIndex, tSize*2);
            if(reg)
                return carve(reg, tSize, tIndex, diff, size, align, index);

            diff++;
            tSize <<= 1;
            tIndex--;
            tIndex >>= 1;
        }

        //Check the map for the best region
        const uint64_t maskedValue = topBinMap & bottomBinMap[subIndex];
        if(maskedValue)
        {
            int bin = Bits::getMsb64(maskedValue);
            tSize = 1;
            diff = colorBits - Bits::getMsb64(bin+1) - 1;
            tSize <<= (diff + PAGE_OFFSET_BITS);
            reg = variableRemoveRegion(bin, tSize*2);

            assert(reg != nullptr);

            return carve(reg, tSize, tIndex, diff, size, align, index);
        }
    }
    else
        tIndex = 0;

    //No eligible region found in the variable portion, full from the fixed region allocator
    reg = allocateFixedRegion(0);

    tSize = reg->getLength();
    tSize >>= 1;
    diff = Bits::getLsb64(reg->getLength()/size);
    diff--;

    return carve(reg, tSize, tIndex, diff, size, align, index);
}
