#include "ops.h"
#include <cpu/localstate.h>
#include <io/io.h>

extern "C" void sse2_memset( void *dest, uint8_t v, size_t amt );

extern "C" void kmemcpy( void *dest, const void *src, size_t size )
{
    Memory::copy( dest, src, size );
}


int Memory::free(void *ptr)
{
    auto hpc = localState::getProcessorCache();
    return hpc->free(ptr);
}

void Memory::copy( void *dest, const void *src, size_t size )
{
	uint64_t *dBuffer8 = reinterpret_cast <uint64_t *> (const_cast <void *> (dest));
	const uint64_t *sBuffer8 = reinterpret_cast <const uint64_t *> (src);

	while(size >= 64)
	{
        *dBuffer8 = *sBuffer8;
        dBuffer8[1] = sBuffer8[1];
        dBuffer8[2] = sBuffer8[2];
        dBuffer8[3] = sBuffer8[3];
        dBuffer8[4] = sBuffer8[4];
        dBuffer8[5] = sBuffer8[5];
        dBuffer8[6] = sBuffer8[6];
        dBuffer8[7] = sBuffer8[7];
        dBuffer8 = &dBuffer8[8];
        sBuffer8 = &sBuffer8[8];
        size -= 64;
	}

	if(size >= 8)
	{
        *dBuffer8 = *sBuffer8;
        dBuffer8++;
        sBuffer8++;
        size -= 8;
	}

	uint8_t *dBuffer = reinterpret_cast <uint8_t *> ( dBuffer8 );
	const uint8_t *sBuffer = reinterpret_cast <const uint8_t *> ( sBuffer8 );

	while( size )
	{
		*dBuffer = *sBuffer;
		dBuffer++;
		sBuffer++;
		size--;
	}
}

extern "C" void kmemset( void *dest, const uint8_t value, size_t size)
{
    Memory::set(dest, value, size);
}

void Memory::set( void *dest, const uint8_t value, size_t size )
{
	uint8_t *buffer = reinterpret_cast <uint8_t *> ( dest );

	while(size)
	{
	    *buffer = value;
	    buffer++;
	    size--;
	}

	//sse2_memset( dest, value, size );
}
