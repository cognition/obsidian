#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "task.h"
#include "events.h"
#include <cpu/processor.h>
#include <cpu/registers.h>
#include <templates/priority_queue.cc>

#define VECTOR_SCHED_REMOTE_PREEMPT                      0x25

extern "C" void taskWaitCurrent(Event *waitEvent, uint32_t mSec);

//! Possible operational states of a processor
enum processorExecutionState
{
    //! The processor is executing tasks normally
    PROCESSOR_EXEC_STATE_RUNNING,
    //! The processor is in a requested stall state
    PROCESSOR_EXEC_STATE_STALL,
    //! The processor is halted
    PROCESSOR_EXEC_STATE_HALT,
    //! The processor is in a low power sleep state
    PROCESSOR_EXEC_STATE_SLEEP
};

//! Object representing a logical processor specific scheduler
class processorScheduler
{
	public:
		processorScheduler( Processor *schedProc, Task *execTask, const pid_t idlePid, const pid_t cleanupPid );
		void scheduleTasklet( Task *task );
		bool signalCurrentTimerEvent( savedRegs *regs );
		void updateLocalTicks(void);
		Task *getCurrentTask(void);
		//Task preemption routines
		void attemptTaskPreempt( Task *task );
		void attemptRemotePreempt( Task *task );
		void executeRemotePreempt(void);
		void processPotentialPreempt(Task *enterTask, savedRegs *enterRegs, const bool alwaysRunNextTimer = false);

		void invokeCurrentSchedulerEvent(void *caller);
		bool validateSchedEventCaller(Task *t) const
		{
		    return ((t == currentTask) || (currentTask == idleTask));
		}

		//Execution control functions
		void exitCurrentTask(void);
		void stall( uint32_t usec );
		void sleepCurrent( uint32_t mSec );
		void waitCurrent(Event *waitEvent, uint32_t mSec, savedRegs *regs);
		void unwaitTask(processorScheduler *caller, Task *t);
		void executeCurrent(void);
		void cleanDestroyed(void);

		bool operator ==(const processorScheduler &sched ) const;
		bool operator >(const processorScheduler &sched ) const;
        bool operator <(const processorScheduler &sched ) const;

		Task *getLastActiveVectorTask(void);
		void setLastActiveVectorTask(Task *t);
		void unmaskVectorCatch(void);
		void maskVectorCatch(void);

		uint32_t getProcId(void) const
		{
		    return proc->getId();
		}

		Task *getIdle(void)
		{
		    return idleTask;
		}

	private:
	    void processSchedTimerImminent(void);
	    void invalidateSchedTimer(void);

	    void storeTimer(timedEventInstance &timer);

	    void processPreempt(Task *enterTask, savedRegs *enterRegs);

	    void cycleTimerInit(void);
        void queueSchedTimer(uint64_t delayTicks);
		uint64_t calculateUsecDelay( uint64_t usec );
		uint64_t calculateMsecDelay( uint64_t msec );
		void runCurrentTimer(void);
		void triggerCurrentTimer(void);
		void runNextTimer(void);

		void pushWaitTask(Task *t);
		void pushRetiredTask(Task *t);
		void pushRunnableTask(Task *t);
		Task *popRetiredTask(void);
		Task *popRunnable(void);
		void removeWaitTask(Task *t);

		void incTaskCount(void);
		void decTaskCount(void);

		void runIdle(void);
        void setCurrentTask(Task *t);

		uint32_t calcActiveCount(void) const
		{
		    uint32_t value = ((currentTask != idleTask) && (currentTask != cleanupTask) && currentTask) ? 1:0;
		    return value;
		}

        bool validateTaskCounts(void) const
        {
            return (taskCount == (runnable.size() + calcActiveCount() + waitTaskCount + retiredTaskCount) );
        }

		void needWork(void);
		void schedNextCycle(void);

        //! Pointer to the processor object tied to this scheduler
		Processor *proc;
		shared_ptr<schedulerEvent> schedSignalListener;
		//! Currently executing task field
		Task * volatile currentTask;
		//! \todo Replace these pointers with a good queue object
		priorityQueue<Task::taskPrioEntry, Task::highestPrioEntry> runnable;
		Task * volatile waitList, * volatile retiredList, * volatile destroyedList;
		//! Idle task instance for this scheduler
		Task *idleTask;
		//! Cleanup task instance for this scheduler
		Task *cleanupTask;
		//! Remote preemption request pointer
		volatile Task *remotePreempt;
		//! Last active task to use the processor's vector math extensions
		Task *lastVector;
		//! Control register cache
		uintptr_t controlCache;
		//! Active scheduler triggering event
		schedulerEvent *currentSchedEvent, *currentCycleEvent;
		//! Cycle timer object
		timedEventInstance cycleTimer;
		//! Scheduler timer object
		timedEventInstance schedTimer;
		//! Current executing timer object
        timedEventInstance currentTimer;
        //! Timer tree
		priorityQueue<timedEventInstance, timedEventInstance::mostImminentTEI> timers;

        //! Local APIC ticks field
		uint64_t localTicks;
		//! Timer remaining count
		uint64_t currentTimerRemaining;
		//! Cycle end count
		uint64_t cycleEnd;

		uint32_t hardLoad, softLoad;
		uint32_t idleTicks, loadTicks;
		uint32_t volatile initTimerTicks, destroyedCount;

		//! Lock field for the processor scheduler
		ticketLock procSchedLock;
        ticketLock procTimerLock;
		//! \todo Roll this into the retired task queue
		uint32_t retiredTaskCount;
		uint32_t taskCount;
		uint32_t waitTaskCount;
		uint32_t schedState;


		static const constexpr uint32_t SCHED_PREEMPT_FLAG  = 0x0001U;
        static const constexpr uint32_t SCHED_CSE_FLAG      = 0x0002U;
        static const constexpr uint32_t SCHED_SAVED_FLAG    = 0x0004U;

		enum
		{
		    schedDiscard = 0x00,
		    schedSaveRunnable,
		    schedSaveRetired,
		    schedSaveWait
		} schedSaveState;


		//! Default task execution period
		uint64_t taskQuantum;

		/*! Minimum time slice a task can have
		 *  \note Used for performance purposes
		 */
		const uint32_t taskMinimum;
		//! Processor execution state
		volatile processorExecutionState execState;
};

//! Possible results of a task scheduling request
enum schedResult
{
    //! No logical processors were available to handle the scheduling request
    schedNoneAvailable = 0,
    //! The task was scheduled successfully
    schedSuccess
};

//! Global scheduler namespace
namespace Scheduler
{
    void registerProcessorScheduler( processorScheduler *sched );
    Task *getCurrentTask(void);
    void waitCurrent( Event *waitEvent, uint32_t mSec);
    schedResult registerTask( Task *task );
    void sleep(const uint32_t mSec);
}

#endif
