#ifndef ACPI_SUBSYSTEM_H_
#define ACPI_SUBSYSTEM_H_

#include <baseboard.h>
#include <cpu/processor.h>
#include <task.h>
#include <templates/vector.cc>
#include "acpica.h"

class acpiServices
{
	public:
		acpiServices( baseBoard &board );
		~acpiServices();
		bool initialize(Task *initTask);
		bool wakeAp( uint32_t apicId, uint32_t acpiId, uintptr_t apicBase );

		static bool getDeviceResources( ACPI_HANDLE dev, Vector<systemResource> &resources);
		baseBoard *getBaseBoard(void)
		{
		    return &systemBaseBoard;
		}
	private:
		bool initIoApic( void *ptr );
		void registerIntOverride( void *ptr );
		void calibrateBspTimer( Processor *bsp );
		void calibrateBspTsc();
		void loadApTrampoline();
		bool parseMADT( void *madt, Task *initTask );
		baseBoard &systemBaseBoard;
		bool useIoApic;
};

extern volatile char *acpiDbCmdBuffer;
void AcpiGenericRemoveHandler(ACPI_HANDLE ptr, void *data);

#endif
