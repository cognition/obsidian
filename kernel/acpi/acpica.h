#ifndef ACPICA_DIST_H_
#define ACPICA_DIST_H_

#define OBSIDIAN64              1
#define ACPI_USE_LOCAL_

extern "C"
{
    #include "local_include/platform/acenv.h"
    #include "local_include/actypes.h"
    #include "local_include/acexcep.h"
}

#endif

