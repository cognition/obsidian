#ifndef ACPI_MADT_H_
#define ACPI_MADT_H_

#include "acpitabledef.h"

//MADT Flags
#define MADT_PCAT_COMPAT                0x1

//Local APIC flags
#define MADT_LAPIC_ENABLED              0x1

#define MADT_LOCAL_APIC                 0x0
#define MADT_IO_APIC                    0x1
#define MADT_INTERRUPT_OVERRIDE         0x2
#define MADT_NMI_SOURCE                 0x3
#define MADT_LOCAL_NMI                  0x4
#define MADT_LOCAL_ADDR_OVERRIDE        0x5
#define MADT_IO_SAPIC                   0x6
#define MADT_LOCAL_SAPIC                0x7
#define MADT_PLATFORM_INTERRUPT_SOURCE  0x8
#define MADT_LOCALX2_APIC               0x9
#define MADT_LOCALx2_NMI                0xA

struct COMPILER_PACKED acpiMADT
{
	acpiStandardHeader standard;
	uint32_t lapicAddress, flags;
	uint8_t entries[];
};

struct COMPILER_PACKED madtLocalApic
{
	uint8_t type, length, acpiId, apicId;
	uint32_t flags;
};

struct COMPILER_PACKED madtIoApic
{
	uint8_t type, length, apicId, _reserved;
	uint32_t physAddress, globalBase;
};

struct COMPILER_PACKED madtIntOverride
{
	uint8_t type, length, bus, source;
	uint32_t line;
	uint16_t flags;
};

#endif
