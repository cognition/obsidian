#ifndef ACPI_TABLE_DEF_H_
#define ACPI_TABLE_DEF_H_

#define FADT_IAPC_BOOT_LEGACY                 0x01U
#define FADT_IAPC_BOOT_PS2                    0x02U
#define FADT_IAPC_BOOT_NOVGA                  0x04U
#define FADT_IAPC_BOOT_NOMSI                  0x08U
#define FADT_IAPC_BOOT_NOASPM                 0x10U
#define FADT_IAPC_BOOT_NORTC                  0x20U

struct COMPILER_PACKED acpiGAS
{
    uint8_t addressSpaceId, registerBitWidth, registerBitOffset, accessSize;
    uint64_t address;
};

struct COMPILER_PACKED acpiStandardHeader
{
	uint32_t signature, length;
	uint8_t revision, checkSum, oemId[6], oemTableId[8], oemRevision[4], creatorId[4], creatorRev[4];
};

struct COMPILER_PACKED acpiFADT
{

    acpiStandardHeader header;
    uint32_t firmwareControl, dsdt;
    uint8_t _reserved, pmProfile;
    uint16_t sciVector;
    uint32_t sciCmd;
    uint8_t acpiEnable, acpiDisable, s4bios, pstateControl;
    uint32_t pm1aEventBlock, pm1bEventBlock, pm1aControlBlock, pm1bControlBlock;
    uint32_t pm2ControlBlock, pmTimerBlock, gpe0Block, gpe1Block;
    uint8_t pm1EventLen, pm1ControlLen, pm2ControlLen, pmTimerLen, gpe0BlockLen, gpe1BlockLen, gpe1Base;
    uint8_t osCstateSupportVal;
    uint16_t p2Latency, p3Latency, flushSize, flushStride;
    uint8_t dutyOffset, dutyWidth, rtcDayAlarm, rtcMonthAlarm, rtcCenturyIndex;
    uint16_t iaPcBootFlags;
    uint8_t _reserve2;
    uint32_t fixedFeatureFlags;
    acpiGAS resetReg;
    uint8_t resetValue;
    uint16_t armBootFlags;
    uint8_t fadtMinor;
    uint64_t xFirmwareControl, xDsdt;
    acpiGAS xPm1aEvtBlock, xPm1bEvtBlock, xPm1aControlBlock, xPm1bControlBlock;
    acpiGAS xPm2ControlBlock, xPmTimerBlock, xGPe0Block, xGpe1Block;
    acpiGAS sleepControlReg, sleepStatusReg;
    uint64_t hyperVisorId;
};


#endif // ACPI_TABLE_DEF_H_
