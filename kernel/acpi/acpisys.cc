#include <io/io.h>
#include "acpisys.h"
#include <bus/pci.h>
#include <bus/ecbus.h>
#include "madt.h"
#include <cpu/localstate.h>
#include <memory/memory.h>
#include <mbdevs/pic.h>
#include <mbdevs/ioapic.h>
#include <timer.h>
#include <bios32.h>
#include <io/ps2.h>
#include <prng.h>

extern "C" {
    #include "local_include/acpi.h"
}

extern "C" void *mpTrampolineLinkageStart, *mpTrampolineLinkageEnd, *mpTrampInfoStart, *mpTrampInfoEnd;
extern "C" volatile uint8_t apActiveFlag, apEarlyFlag;

//Method arguments
#define ACPI_PIC_USE_LEGACY                 0x0
#define ACPI_PIC_USE_IOAPIC                 0x1
#define ACPI_PIC_USE_SAPIC                  0x2
//AP Trampoline code values
#define AP_TRAMPOLINE_DEST                  0x1000U
#define AP_TRAMPOLINE_START                 &mpTrampolineLinkageStart

//BSP Calibration values
#define OSPM_TIMER_CALIBRATE_USEC           25011
#define OSPM_TIMER_CALIBRATE_FLOAT          25011
#define OSPM_TSC_CALIBRATE_FLOAT            250110
#define OSPM_LAPIC_CALIBRATE_INIT           0xFFFFFFFFU

struct COMPILER_PACKED apInitState
{
	dtReg gdtr;
	localState *apState;
	uintptr_t apicBase;
	uintptr_t stack;
};

//Device _STA Method return flags
#define ACPI_DEV_STA_PRESENT               0x1
#define ACPI_DEV_STA_ENABLED               0x2
#define ACPI_DEV_STA_SHOWGUI               0x4
#define ACPI_DEV_STA_WORKING               0x8
#define ACPI_DEV_STA_BATTERY               0x10

//Resource interpretation values
#define ACPI_DEV_RES_TYPE                  0x80U

#define GET_SMALL_RES_TYPE(x)              ((x >> 3) & 0xF)
#define GET_SMALL_RES_LENGTH(x)            (x & 0x7)

#define SMALL_RES_TYPE_IRQ                 0x4
#define SMALL_RES_TYPE_DMA                 0x5
#define SMALL_RES_TYPE_START_DEP           0x6
#define SMALL_RES_TYPE_END_DEP             0x7
#define SMALL_RES_TYPE_IO                  0x8
#define SMALL_RES_TYPE_FIXED_IO            0x9
#define SMALL_RES_TYPE_FIXED_DMA           0xA

//static ACPI_TABLE_DESC initTables[32];

extern "C" apInitState apInitInfo;

static const char *madtString = "APIC";
static const char *fadtString = "FACP";
static const char *pciString = "MCFG";
static const void *apTrampolineDestination = reinterpret_cast <void *> ( AP_TRAMPOLINE_DEST );

static bool ps2Found = false;

acpiServices::acpiServices( baseBoard &board ) : systemBaseBoard( board ), useIoApic(false)
{
}

acpiServices::~acpiServices()
{

}

bool acpiServices::wakeAp( uint32_t apicId, uint32_t acpiId, uintptr_t apicBase )
{
	//Get the processor object for the currently executing processor
	Processor *proc = localState::getProc();
	//Get the scheduler object for the currently executing processor
	processorScheduler *sched = localState::getSched();
	//Initialize the AP's state information
	apInitInfo.apState = new localState( apicId, acpiId );

	uintptr_t tss = Processor::createTss();
	Processor::createGdt( apInitInfo.gdtr, tss );
	apInitInfo.stack = virtualMemoryManager::allocateKernelStack( DEFAULT_KSTACK_PAGES ).limit() - 8;
	apInitInfo.apicBase = apicBase;

    asm volatile ("movb $0, (%0)\n"
                  "sfence\n":: "r" (&apActiveFlag):"memory");


	//Send INIT IPI, wait 10ms
	proc->sendIPI( apicId, 0, LAPIC_DELIVERY_INIT | LAPIC_DESTINATION_PHYSICAL | LAPIC_DELIVERY_ASSERT | LAPIC_TRIGGER_LEVEL );
	sched->stall(10000);
    //Clear the AP Status flag


	//Send startup IPI with a destination value of AP_TRAMPOLINE_DEST, wait 200us
	proc->sendIPI( apicId, AP_TRAMPOLINE_DEST/REGULAR_PAGE_SIZE, LAPIC_DELIVERY_STARTUP | LAPIC_DESTINATION_PHYSICAL | LAPIC_DELIVERY_ASSERT );
	sched->stall( 500 );
    //Check the AP Status flag
	if( !apActiveFlag )
	{
		//If the processor has not yet started, send another startup IPI, wait another 200us

		proc->sendIPI( apicId, 1, LAPIC_DELIVERY_STARTUP | LAPIC_DESTINATION_PHYSICAL | LAPIC_DELIVERY_ASSERT );
		sched->stall(25000);
		//Check if the AP is active again, if it's not we've failed to boot up
		if( !apActiveFlag )
        {
            kprintf("Failed to wake - Early flag value: %u\n", apEarlyFlag);
			return false;
        }
	}

	return true;
}

//! Uses the ACPI PM Timer to calibrate the BSP processors LAPIC timer
void acpiServices::calibrateBspTimer( Processor *bsp )
{
	uint32_t initValue, currentValue, apicEnd, usec;
	uint64_t calc, rem;

	//Get an initial value for the timer
	AcpiGetTimer( &initValue );

	bsp->apicMaskTimer();

	//Spin until right after the timer rolls over, so we have minimal rounding errors
	do
	{
		AcpiGetTimer( &currentValue );
	} while( currentValue == initValue );

	//Save that value as our initial timer tick value
	initValue = currentValue;

	//Initialize the APIC Timer
	bsp->apicSetDiv(LAPIC_TIMER_DIV_1);
	bsp->apicStartTimer( OSPM_LAPIC_CALIBRATE_INIT );

	do
	{
		//Retrieve the current value from the timer
		AcpiGetTimer( &currentValue );
		//Calculate the duration of time that our calibration test has been running
		AcpiGetTimerDuration( initValue, currentValue, &usec );
		//Loop until we hit our predefined value
	} while( usec < OSPM_TIMER_CALIBRATE_USEC );

	//Get our ending APIC timer value
	apicEnd = bsp->getApicTimerVal();
	bsp->apicStopTimer();
	bsp->apicUnmaskTimer();

	//Calculate the number of cycles per microsecond
	calc = OSPM_LAPIC_CALIBRATE_INIT-apicEnd;
	rem = calc%OSPM_TIMER_CALIBRATE_USEC;
    calc /= OSPM_TIMER_CALIBRATE_USEC;
    if(rem >= (OSPM_TIMER_CALIBRATE_USEC/2) )
        calc++;

    kprintf( "TIME: BSP's APIC Timer runs @ approximately %u MHz\n", calc );

	//Update our timer scale variables
	localState::setApicScaleUsec( calc );
}

//! Calibrates the processor's time stamp counter relative to a value of 100ns
void acpiServices::calibrateBspTsc()
{
	uint32_t startTSC, endTSC, initValue, currentValue, usec;
	uint64_t calc, rem;

	//Get an initial value for the timer
	AcpiGetTimer( &initValue );

	//Spin until right after the timer rolls over, so we have minimal rounding errors
	do
	{
		AcpiGetTimer( &currentValue );
	} while( currentValue == initValue );

	//Save that value as our initial timer tick value
	initValue = currentValue;

	startTSC = Timer::readTSC();
	do
	{
		//Retrieve the current value from the timer
		AcpiGetTimer( &currentValue );
		//Calculate the duration of time that our calibration test has been running
		AcpiGetTimerDuration( initValue, currentValue, &usec );
		//Loop until we hit our predefined value
	} while( usec < OSPM_TIMER_CALIBRATE_USEC );
	endTSC = Timer::readTSC();

	calc = endTSC-startTSC;
	rem = calc%OSPM_TSC_CALIBRATE_FLOAT;
    calc /= OSPM_TSC_CALIBRATE_FLOAT;
    if(rem >= (OSPM_TIMER_CALIBRATE_FLOAT/2))
        calc++;
    kprintf( "TIME: BSP runs @ approximately %u cycles/100nanosec\n", calc );

	localState::setTscScaleHundredNano( calc );
}

//! Loads the AP trampoline into physical memory at the address specified by apTrampolineDestination
void acpiServices::loadApTrampoline()
{
    const uintptr_t codeStart = reinterpret_cast <uintptr_t> (AP_TRAMPOLINE_START);
    const uintptr_t codeEnd = reinterpret_cast <uintptr_t> (&mpTrampolineLinkageEnd);

    const uintptr_t apTrampCodeLength = codeEnd - codeStart;

    const uintptr_t infoStart = reinterpret_cast <uintptr_t> (&mpTrampInfoStart);
    const uintptr_t infoEnd = reinterpret_cast <uintptr_t> (&mpTrampInfoEnd);
    const uintptr_t apTrampInfoLength =  infoEnd - infoStart;

    const uintptr_t apTrampTotalSize = apTrampCodeLength + apTrampInfoLength;


    kprintf("Load AP Trampoline - ATCL: %X ATIL: %X TS: %X IS: %X IE: %X CS: %X CE: %X\n", apTrampCodeLength, apTrampInfoLength, apTrampTotalSize, &mpTrampInfoStart, &mpTrampInfoEnd, codeStart, codeEnd);
    uint64_t apTrampVirtAddr = virtualMemoryManager::mapRegion((uintptr_t)apTrampolineDestination, apTrampTotalSize, PAGE_PRESENT | PAGE_WRITE);

    Memory::copy( (void *)apTrampVirtAddr, AP_TRAMPOLINE_START, apTrampCodeLength );
	Memory::copy( (void *)(apTrampVirtAddr + apTrampCodeLength), &mpTrampInfoStart, apTrampInfoLength );

	virtualMemoryManager::unmapRegion(apTrampVirtAddr, apTrampTotalSize, false, true);
}

/*! Initializes an IOAPIC from described in the MADT
 *  \param ptr Pointer to the MADT's IOAPIC entry
 *  \return True if the IOAPIC was successfully initialized, false if it was not
 */
bool acpiServices::initIoApic( void *ptr )
{
	madtIoApic *ioApicInfo = reinterpret_cast <madtIoApic *> ( ptr );
	return interruptManager::installIoApic( &systemBaseBoard, ioApicInfo->physAddress, ioApicInfo->globalBase );
}

void acpiServices::registerIntOverride( void *ptr )
{
	madtIntOverride *override = reinterpret_cast <madtIntOverride *> ( ptr );
	interruptManager::registerIrqRedirect( override->source, override->line, override->flags );
}

/** \brief Converts an ACPI_RESOURCE object into a native systemResource object
 *
 * \param resPtr ACPI_RESOURCE* Input ACPICA resource object
 * \param context void* A vector of systemResource objects
 * \return ACPI_STATUS AE_OK is successful, AE_NOT_SUPPORTED if an unknown resource type is provided
 *
 */
static ACPI_STATUS acpiBuildResourceCallback(ACPI_RESOURCE *resPtr, void *context)
{
    Vector<systemResource> *resources = reinterpret_cast<Vector<systemResource> *> (context);
	ACPI_RESOURCE_ADDRESS64 addr64;
    uint16_t irqEntry;


	switch(resPtr->Type)
	{
		case ACPI_RESOURCE_TYPE_IRQ:
		    irqEntry = resPtr->Data.Irq.Interrupts[1];
		    irqEntry <<= 8;
		    irqEntry |= resPtr->Data.Irq.Interrupts[0];
		    resources->insert( systemResource(systemResourceIrq, irqEntry, 1) );
            break;
		case ACPI_RESOURCE_TYPE_IO:
			resources->insert( systemResource(systemResourceIo, resPtr->Data.Io.Minimum, resPtr->Data.Io.AddressLength ) );
			break;
		case ACPI_RESOURCE_TYPE_FIXED_IO:
			resources->insert ( systemResource(systemResourceIo, resPtr->Data.FixedIo.Address, resPtr->Data.FixedIo.AddressLength) );
			break;
		case ACPI_RESOURCE_TYPE_DMA:
			for(uint8_t i = 0; i < resPtr->Data.Dma.ChannelCount; i++)
				resources->insert ( systemResource(systemResourceDma, resPtr->Data.Dma.Channels[i], 1));
			break;
		case ACPI_RESOURCE_TYPE_FIXED_DMA:
			resources->insert ( systemResource(systemResourceDma, resPtr->Data.FixedDma.Channels, 1));
			break;
		case ACPI_RESOURCE_TYPE_MEMORY24:
		    kprintf("Found Memory24 resource - Address: %X Length: %X\n", resPtr->Data.Memory24.Minimum, resPtr->Data.Memory24.AddressLength);
			resources->insert ( systemResource(systemResourceMemory, resPtr->Data.Memory24.Minimum, resPtr->Data.Memory24.AddressLength) );
			break;
		case ACPI_RESOURCE_TYPE_FIXED_MEMORY32:
		    kprintf("Found fixed memory32 resource - Address: %X Length: %X\n", resPtr->Data.FixedMemory32.Address, resPtr->Data.FixedMemory32.AddressLength);
			resources->insert ( systemResource(systemResourceMemory, resPtr->Data.FixedMemory32.Address, resPtr->Data.FixedMemory32.AddressLength ) );
			break;
		case ACPI_RESOURCE_TYPE_MEMORY32:
			kprintf("Found memory32 resource - Address: %X Length: %X\n", resPtr->Data.Memory32.Minimum, resPtr->Data.Memory32.AddressLength);
			resources->insert ( systemResource(systemResourceMemory, resPtr->Data.Memory32.Minimum, resPtr->Data.Memory32.AddressLength) );
			break;
		case ACPI_RESOURCE_TYPE_ADDRESS16:
		case ACPI_RESOURCE_TYPE_ADDRESS32:
		case ACPI_RESOURCE_TYPE_ADDRESS64:
            AcpiResourceToAddress64(resPtr, &addr64);
            if(!addr64.Address.AddressLength)
                break;
            switch(addr64.ResourceType)
            {
                case ACPI_MEMORY_RANGE:
                    kprintf("Found memory address range - Minimum: %X Maximum: %X Length: %X\n", addr64.Address.Minimum, addr64.Address.Maximum, addr64.Address.AddressLength);

                    resources->insert ( systemResource(systemResourceMemory, addr64.Address.Minimum, addr64.Address.AddressLength ? addr64.Address.AddressLength:(addr64.Address.Maximum - addr64.Address.Minimum + 1) ) );
                    break;
                case ACPI_IO_RANGE:

                    resources->insert ( systemResource(systemResourceIo, addr64.Address.Minimum, addr64.Address.AddressLength) );
                    break;
                case ACPI_BUS_NUMBER_RANGE:
                    break;
            }
			break;
		case ACPI_RESOURCE_TYPE_EXTENDED_ADDRESS64:
			break;
		case ACPI_RESOURCE_TYPE_EXTENDED_IRQ:
		    kprintf("Found extended IRQ...\n");
			resources->insert( systemResource(systemResourceInterrupt, resPtr->Data.ExtendedIrq.Interrupts[0], 1));
			break;
        case ACPI_RESOURCE_TYPE_END_TAG:
            break;
		case ACPI_RESOURCE_TYPE_GENERIC_REGISTER:
			kprintf("ACPI - NYI - Generic register support!\n");
			break;
        default:
            kprintf("NYI resource type %u\n", resPtr->Type);
            break;
	}

    return AE_OK;
}

bool acpiServices::getDeviceResources( ACPI_HANDLE dev, Vector<systemResource> &resources)
{
    String methodString = "_CRS";
    ACPI_STATUS result;
    result = AcpiWalkResources(dev, methodString.getBuffer(), &acpiBuildResourceCallback, &resources);

    if(result != AE_OK)
    {
        if(result != AE_NOT_FOUND)
        {
            kprintf("ACPI: Failed to get resources %s\n", AcpiFormatException(result));
            return false;
        }
        else
            return true;
    }

    return true;
}


/*! Parses ACPI Multiple APIC description table
 *  \param ptr Pointer to the MADT
 *  \param initTask Initialization task of the BSP
 *  \return True if the MADT is present and parsed successfully, false otherwise
 */
bool acpiServices::parseMADT( void *ptr, Task *initTask )
{
	acpiMADT *madt = reinterpret_cast <acpiMADT *> ( ptr );
	uint8_t *subPtr = madt->entries;
	uint32_t length = madt->standard.length - sizeof( acpiMADT );
	uintptr_t apicBase = madt->lapicAddress;
	uint32_t apCount = 1;
	madtLocalApic *lapic;
	Processor *bsp = localState::getProc();

	if( madt->flags & MADT_PCAT_COMPAT )
		if( !interruptManager::installDualPic( &systemBaseBoard ) )
			return false;

    //Attempt to initialize the BSP's local APIC
    if( !bsp->init( &apicBase ) )
        return false;
    else
    {
        //Update our LAPIC base remapping pointer and load in
        apicBase += REGULAR_PAGE_SIZE;
        loadApTrampoline();
    }
    bsp->setId( bsp->readAPICID() );

    kprintf("[ACPI] BSP APIC ID: %u\n", bsp->getId() );

    //Calibrate our processor specific timers
    calibrateBspTimer( bsp );
    calibrateBspTsc();

    uint32_t acpiTimerVal;
    uint64_t tscVal;
    AcpiGetTimer(&acpiTimerVal);
    tscVal = Timer::readTSC();
    tscVal ^= acpiTimerVal;
    kprintf("PRNG seed: %X\n", tscVal);
    asm volatile ("xchg %%bx, %%bx\n"::);
    random::seed(tscVal);

    //Set up the processor scheduler
    pid_t idle, cleanup;
    if(!Task::allocatePid(idle) || !Task::allocatePid(cleanup) )
    {
        return false;
    }
    else
    {
        new processorScheduler( bsp, initTask, idle, cleanup);
    }



    while( length )
	{
		uint8_t subLength = subPtr[1];
		switch( *subPtr )
		{
		    // A local APIC (logical processor)
			case MADT_LOCAL_APIC:
				lapic = reinterpret_cast <madtLocalApic *> ( subPtr );
				if( lapic->flags & MADT_LAPIC_ENABLED )
				{
					if( lapic->apicId != bsp->getId() )
					{
					    /*
					    //Wake up the application processor
						if(!wakeAp( lapic->apicId, lapic->acpiId, apicBase ))
                        {
                            kprintf("Failed to wake processor %u\n", lapic->apicId);
                            return false;
                        }
						//Update the LAPIC remapping address for the next processor
						apicBase += REGULAR_PAGE_SIZE;
                        apCount++;
                        */
					}
				}
				break;
            //An IOAPIC
			case MADT_IO_APIC:
				if( !initIoApic( subPtr ) )
					return false;
				else
					useIoApic = true;
				break;
            //An interrupt override
			case MADT_INTERRUPT_OVERRIDE:
				registerIntOverride( subPtr );
				break;
            //! \todo Add other valid table entry types (NMIs, local NMI pins, etc..)
			default:
				break;
		}
		subPtr = &subPtr[subLength];
		length -= subLength;
	}

	kprintf( "SCHD: Found %u active logical processors.\n", apCount );
	asm volatile ( "sti\n"::: );
	return true;
}

static ACPI_STATUS acpiDoNothing(ACPI_HANDLE COMPILER_UNUSED Device, UINT32 COMPILER_UNUSED Level, void COMPILER_UNUSED *context, void COMPILER_UNUSED **returnValue)
{
    return AE_OK;
}


static ACPI_STATUS acpiFoundBusDevice(ACPI_HANDLE Device, UINT32 COMPILER_UNUSED Level, void COMPILER_UNUSED *context, void COMPILER_UNUSED **returnValue)
{
    ACPI_STATUS status;
    ACPI_DEVICE_INFO *devInfoPtr;

    status = AcpiGetObjectInfo(Device, &devInfoPtr);
    if(ACPI_FAILURE(status))
    {
        return status;
    }

	if(devInfoPtr->Flags & ACPI_PCI_ROOT_BRIDGE)
	{
		ACPI_PNP_DEVICE_ID_LIST *cids = &(devInfoPtr->CompatibleIdList);
		ACPI_PNP_DEVICE_ID *devId = &(devInfoPtr->HardwareId);

		uint32_t address = 0;
		if(devInfoPtr->Valid & ACPI_VALID_ADR)
            address = (devInfoPtr->Address & 0x7U) | ((devInfoPtr->Address>>16)<<3);

		kprintf("Found PCI Root bridge %s @ %X\n", devId->String, address);

		for(uint32_t ids = 0; ids < cids->Count; ids++ )
		{
			ACPI_PNP_DEVICE_ID *cid = &(cids->Ids[ids]);
			kprintf("ID #%u: %s\n", ids, cid->String );
		}

		uint32_t segment, pciBusNum;
		ACPI_OBJECT integerObject;
		ACPI_BUFFER integerBuffer = {sizeof(integerObject), &integerObject};

		//Determine the PCI segment group number
		status = AcpiEvaluateObjectTyped(Device, "_SEG", NULL, &integerBuffer, ACPI_TYPE_INTEGER);
		if(!ACPI_FAILURE(status))
		{
            //Potentially a multisegment system
			segment = integerObject.Integer.Value & 0xFFFFU;
        }
		else
		{
		    //Single segment system
            segment = 0;
        }

        segment <<= 16;

        integerBuffer = {sizeof(integerObject), &integerObject};
        //Determine the base bus number of this root bridge
        status = AcpiEvaluateObjectTyped(Device, "_BBN", NULL, &integerBuffer, ACPI_TYPE_INTEGER);
        if(!ACPI_FAILURE(status))
            pciBusNum = integerObject.Integer.Value & 0xFFU;
        else
            pciBusNum = 0;


        uint32_t pciDeviceNum = address;
        segment |= pciBusNum<<8;
        segment |= pciDeviceNum;
        segment |= address;

        if( !pciBus::enumeratePciRootBus(segment, Device) )
        {
            kprintf("Unable to validate host bridge!\n");
        }
	}

	ACPI_FREE(devInfoPtr);

    return AE_OK;

}

static ACPI_STATUS acpiFoundDevice(ACPI_HANDLE Device, UINT32 COMPILER_UNUSED Level, void COMPILER_UNUSED *context, void COMPILER_UNUSED **returnValue)
{
    ACPI_STATUS status;
    ACPI_DEVICE_INFO *devInfoPtr;
    acpiServices *servicesObj = reinterpret_cast<acpiServices *>(context);


    status = AcpiGetObjectInfo(Device, &devInfoPtr);
    if(ACPI_FAILURE(status))
    {
        return status;
    }

	    /*
        Vector<systemResource> devResources;
        if(!servicesObj->getDeviceResources(Device, devResources))
        {
            kprintf("Failed to retrieve device resources!\n");

        }
        */
    ACPI_PNP_DEVICE_ID *devId = &(devInfoPtr->HardwareId);
    if(devId)
    {
        if(devId->String)
        {
            String devIdString(devId->String);


            String psKbId("PNP0303");
            String psMouseId("PNP0F03");
            if(psKbId == devId->String)
            {
                ACPI_STATUS ret;
                ACPI_OBJECT integerObj;
                ACPI_BUFFER integerBuffer = {sizeof(ACPI_OBJECT), &integerObj};
                ret = AcpiEvaluateObjectTyped(Device, "_STA", NULL, &integerBuffer, ACPI_TYPE_INTEGER);
                if(ret == AE_OK)
                {
                    if((integerObj.Integer.Value & (ACPI_DEV_STA_PRESENT | ACPI_DEV_STA_ENABLED | ACPI_DEV_STA_WORKING)) == (ACPI_DEV_STA_PRESENT | ACPI_DEV_STA_ENABLED | ACPI_DEV_STA_WORKING))
                        kprintf("Found PS/2 Keyboard.  Status: present\n");
                }
                else if(ret == AE_NOT_FOUND)
                {
                    kprintf("Found PS/2 Keyboard.  Status: unknown");
                }

                if(!ps2Found)
                {
                    ps2Found = true;

                    ps2Controller *ps2 = new ps2Controller(servicesObj->getBaseBoard());
                    kprintf("Initializing PS/2 Driver...\n");
                    ps2->init(NULL);
                }
            }
            else if(psMouseId == devId->String)
            {
                ACPI_STATUS ret;
                ACPI_OBJECT integerObj;
                ACPI_BUFFER integerBuffer = {sizeof(ACPI_OBJECT), &integerObj};
                ret = AcpiEvaluateObjectTyped(Device, "_STA", NULL, &integerBuffer, ACPI_TYPE_INTEGER);
                if(ret == AE_OK)
                {
                    if((integerObj.Integer.Value & (ACPI_DEV_STA_PRESENT | ACPI_DEV_STA_ENABLED | ACPI_DEV_STA_WORKING)) == (ACPI_DEV_STA_PRESENT | ACPI_DEV_STA_ENABLED | ACPI_DEV_STA_WORKING))
                    {
                        kprintf("Found PS/2 Mouse.  Status: present\n");
                    }
                }
                else if(ret == AE_NOT_FOUND)
                {
                    kprintf("Found PS/2 Mouse.  Status: unknown");
                }

            }

            //kprintf("Found device with id %s @ %X\n", devId->String, devInfoPtr->Address);


        }
        else
        {
           // if(devInfoPtr->Valid & ACPI_VALID_ADR)
                //kprintf("No device id. Address: %X\n", devInfoPtr->Address);

        }
    }

	ACPI_FREE(devInfoPtr);

    return AE_OK;
}

/*! Initializes ACPI Services for the kernel
 *  \param initTask Kernel Initialization task currently executing on the BSP
 */
bool acpiServices::initialize(Task *initTask)
{
	//Initialize basic table support
	switch( AcpiInitializeTables( NULL, 32, TRUE ) )
	{
		case AE_OK:
			break;
		case AE_NOT_FOUND:
			kprintf( "ERROR - ACPI: Unable to locate RSDP!\n" );
			return false;
			break;
		case AE_NO_MEMORY:
			kprintf( "ERROR - ACPI: Insufficient memory to access tables!\n" );
			return false;
			break;
	}

	ACPI_TABLE_HEADER *table;

	//Load in the MADT so we figure out the LAPIC, PIC, and IOAPIC setup, as well as the configuration for any NMI's
	if( AcpiGetTable( const_cast <char *> ( madtString ), 1, &table ) != AE_OK )
	{
		kprintf( "ERROR - ACPI: Unable to locate the MADT Table!\n" );
		return false;
	}
	else if( !parseMADT( table, initTask ) )
		return false;

	//Attempt to load in and initialize the PCI MMIO Configuraiton table
	if( AcpiGetTable( const_cast <char *> ( pciString ), 1, &table ) == AE_OK )
	{
	    kprintf("[ACPI]: Processing MCFG Table...\n");
		pciMcfg *mcfg = reinterpret_cast <pciMcfg *> ( table );
		uint32_t tableCount = mcfg->length - sizeof( pciMcfg );
		tableCount /= sizeof( pciMmioEntry );
		pciMmioEntry *busEntry = mcfg->entries;
		while( tableCount )
		{
		    //! Register the MMIO interfaces for each segment+bus range specified
			if( !pciBus::registerMmioRange( &systemBaseBoard, busEntry->base, busEntry->segment, busEntry->startBus, busEntry->endBus ) )
				return false;
			busEntry++;
			tableCount--;
		}
	}
	else
	{
	    //PCI MMIO configuration space was not available, attempt to get information from the PCI BIOS via BIOS32
        if( bios32::init() )
        {
            if( !pciBus::registerTraditional(&systemBaseBoard) )
                return false;
        }
        else
        {
            kprintf("Unable to initialize BIOS32 Service for PCI BIOS detection!\n");
            return false;
        }
        kprintf("Port based PCI enabled!\n");
	}
    //Load the FADT for the arch specific boot information
	if( AcpiGetTable( const_cast <char *> (fadtString), 1, &table) == AE_OK )
	{
        acpiFADT *fadt = reinterpret_cast <acpiFADT *> (table);

        if(fadt->iaPcBootFlags & FADT_IAPC_BOOT_PS2)
        {
            ps2Found = true;
            kprintf("Initializing PS/2 Driver...\n");
            ps2Controller *ps2 = new ps2Controller(&systemBaseBoard);
            ps2->init(NULL);
        }
    }

    //Initialize the subsystem
	ACPI_STATUS initStatus = AcpiInitializeSubsystem();

	if( ACPI_FAILURE ( initStatus ) )
    {
        kprintf("ACPICA - Error: Failed to initialize ACPI subsystem! Code: %Xh\n", initStatus);
        return false;
    }

    //Finish ACPI Table initialization
    initStatus = AcpiLoadTables();
    if( ACPI_FAILURE( initStatus) )
    {
        kprintf("ACPICA - Error: Failed to load and initialize ACPI Tables! Code: %Xh\n", initStatus);
        return false;
    }

    //Initialize ACPI Hardware
    initStatus = AcpiEnableSubsystem(ACPI_FULL_INITIALIZATION);
    if( ACPI_FAILURE( initStatus) )
    {
        kprintf("ACPICA - Error: Failed to initialize ACPI hardware! Code: %Xh\n", initStatus);
        return false;
    }

	ACPI_STATUS *statusPointer = &initStatus;
	//Complete initialization of ACPI namespace objects
    initStatus = AcpiInitializeObjects(ACPI_FULL_INITIALIZATION);
    if( ACPI_FAILURE( initStatus) )
    {
        kprintf("ACPICA - Error: Failed to initialize ACPI Namespace! Code: %Xh\n", initStatus);
        return false;
    }

	//There are IOAPIC devices present on this system
	if(useIoApic)
	{
		//Basically we create an Integer object usable by ACPICA and add it to a list containing one item that will be passed to the _PIC_ method
		ACPI_OBJECT integerObject;
		integerObject.Type = ACPI_TYPE_INTEGER;
		integerObject.Integer.Value = ACPI_PIC_USE_IOAPIC;
		ACPI_OBJECT_LIST picMethodArgList = {1, &integerObject};

		ACPI_STATUS picStatus = AcpiEvaluateObject(NULL, "\\_PIC", &picMethodArgList, NULL);
		if(picStatus == AE_NOT_FOUND)
		{
			kprintf("Warning: Unable to find ACPI \\_PIC method.\n");
		}
		else if(picStatus != AE_OK)
		{
			kprintf("ACPICA Encountered an while executing the \\_PIC method.  Status: %x\n", picStatus);
		}

	}

	ACPI_HANDLE acpiSystemBusNs;
	ACPI_STATUS *statusPtr;

	initStatus = AcpiGetHandle(NULL, "\\_SB", &acpiSystemBusNs);
	if(initStatus == AE_NOT_FOUND)
    {
        kprintf("ACPI: Could not find \\_SB namespace...\n");
        return false;
    }
	else if(ACPI_FAILURE(initStatus))
    {
        kprintf("ACPI: Error locating \\_SB namespace...\n");
        return false;
    }

	initStatus = AcpiWalkNamespace(ACPI_TYPE_DEVICE, acpiSystemBusNs, 1, &acpiFoundBusDevice, &acpiDoNothing, this, reinterpret_cast <void **> (&statusPtr));
	if ( ACPI_FAILURE( initStatus) )
    {
        kprintf("ACPI: Error traversing bus devices...\n");
        return false;
    }

    initStatus = AcpiGetDevices(NULL, &acpiFoundDevice, this, reinterpret_cast <void **> (&statusPtr) );
	if( ACPI_FAILURE( initStatus ) )
    {
        kprintf("ACPI: Failed to register devices!\n");
        return false;
    }

    AcpiGetDevices("PNP0C09", &ecDeviceFound, NULL, reinterpret_cast <void **> (&statusPointer) );


	return true;
}

/*! Generic handler for dealing with device removal from the ACPI Namespace due to an unload event
 *  \param ptr Not used
 *  \param data Pointer to the OS's device object for the device being removed
 */
void AcpiGenericRemoveHandler(ACPI_HANDLE COMPILER_UNUSED ptr, void *data)
{
	Device *dev = reinterpret_cast <Device *> (data);
	delete dev;
}
