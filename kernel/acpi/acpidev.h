#ifndef ACPI_DEV_H_
#define ACPI_DEV_H_

#include "acpica.h"
#include <device.h>

class acpiDevice : public Device
{
	public:
		acpiDevice(const char *devName, const char *fp);
		acpiDevice(const char *devName, ACPI_HANDLE handle, const char *fp);
		acpiDevice(Device *parent, const char *devName, ACPI_HANDLE handle, const char *fp);
		virtual bool init(void *context) = 0;
		const String &getFullAcpiPath(void) const;
	protected:
		String fullAcpiPath;
		ACPI_HANDLE devHandle;
};

#endif
