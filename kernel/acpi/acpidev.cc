#include "acpidev.h"

//acpiDevice class

acpiDevice::acpiDevice(const char *devName, const char *fp) : Device(devName), fullAcpiPath(fp), devHandle(NULL)
{
}

acpiDevice::acpiDevice(const char *devName, ACPI_HANDLE handle, const char *fp) : Device(devName), fullAcpiPath(fp), devHandle(handle)
{

}

acpiDevice::acpiDevice(Device *parent, const char *devName, ACPI_HANDLE handle, const char *fp) : Device(devName, devName, parent), fullAcpiPath(fp), devHandle(handle)
{
}

const String &acpiDevice::getFullAcpiPath(void) const
{
	return fullAcpiPath;
}

