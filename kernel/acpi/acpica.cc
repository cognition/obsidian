#include <memory/memory.h>
#include <firmware.h>
#include <cpu/lock.h>
#include <io/io.h>
#include <cpu/localstate.h>
#include <cpu/semaphore.h>
#include <bus/pci.h>
#include <timer.h>
#include <scheduler.h>
#include "acpisys.h"
#include <introute.h>
#include <string.h>

extern "C"
{
#include "local_include/acpi.h"
}

static bool systemFullInit = false;

class tempMmioRegister
{
	public:
		tempMmioRegister( uintptr_t physAddress, uint32_t Width )
		{
			uintptr_t offset = physAddress & PAGE_OFFSET_MASK;
			addressBase = physAddress - offset;
			size = ( physAddress + Width/8 ) - addressBase;
			size &= REGULAR_PAGE_SIZE;
			size += REGULAR_PAGE_SIZE;
			vaddr = offset + virtualMemoryManager::mapRegion( addressBase, size, PAGE_PRESENT | PAGE_CACHE_DISABLE );
		}

		~tempMmioRegister()
		{
			virtualMemoryManager::unmapPhysRegion( addressBase, size );
		}

		template <typename T> inline T read()
		{
			volatile T *reg = reinterpret_cast <volatile T *> ( vaddr );
			return *reg;
		}

		template <typename T>  inline void write( uint32_t value )
		{
			volatile T *reg = reinterpret_cast <volatile T *> ( vaddr );
			*reg = value;
		}
	private:
		uintptr_t addressBase, vaddr;
		uint32_t size;
};

volatile char *acpiDbCmdBuffer = NULL;

extern "C" {

	/*! Allows the OS to preform certain functions prior to the initialization of the ACPICA subsystem
	 *  \return AE_OK Always
	 */
	ACPI_STATUS AcpiOsInitialize()
	{
        systemFullInit = true;
		/*
		AcpiDbgLevel = ACPI_DEBUG_DEFAULT;
		AcpiDbgLayer = ACPI_TABLES | ACPI_PARSER | ACPI_RESOURCES | ACPI_DISPATCHER | ACPI_EVENTS | ACPI_NAMESPACE | ACPI_DRIVER | ACPI_UTILITIES | ACPI_OS_SERVICES;
        */
        return AE_OK;
	}

	/*! Allows the OS to preform certain functions prior to the termination of the ACPICA subsystem
	 *  \return AE_OK Always
	 */
	ACPI_STATUS AcpiOsTerminate()
	{
		return AE_OK;
	}

	/*! Returns a physical address pointing to the location of the RSDP
	 *  \return Physical address where the RSDP is located
	 */
	ACPI_PHYSICAL_ADDRESS AcpiOsGetRootPointer()
	{
		return Firmware::getAcpiRSDP(NULL);
	}

	/*! Allows the OS to override the value of specific predefined objects
	 *  \param PredefinedObject Pointer to the predefined object that is eligible to be overwritten
	 *  \param NewValue Pointer to the location where a new value for the predefined object is written, or NULL if it cannot be overwritten
	 *  \return AE_OK always
	 */
	ACPI_STATUS AcpiOsPredefinedOverride( const ACPI_PREDEFINED_NAMES COMPILER_UNUSED *PredefinedObject, ACPI_STRING *NewValue )
	{
        *NewValue = NULL;
		return AE_OK;
	}

	/*! Allows the OS to override a hardware provided ACPI table
	 *  \param ExistingTable Pointer to the existing table
	 *  \param NewTable Pointer that allows the table pointer to be updated
	 *  \return Always AE_OK
	 */
	ACPI_STATUS AcpiOsTableOverride( ACPI_TABLE_HEADER COMPILER_UNUSED *ExistingTable, ACPI_TABLE_HEADER **NewTable )
	{
		*NewTable = NULL;
		return AE_OK;
	}

	/*! Allows the OS to override a hardware provided table with another stored at a specific physical address
	 *  \param Existing Table Pointer to the existing table
	 *  \param NewAddress Pointer to return the new physical address of a table
	 *  \param NewTableLength Pointer to return the length of the new table at
	 */
	ACPI_STATUS AcpiOsPhysicalTableOverride (ACPI_TABLE_HEADER COMPILER_UNUSED *ExistingTable, ACPI_PHYSICAL_ADDRESS *NewAddress, UINT32 *NewTableLength)
	{
		*NewAddress = NULL;
		*NewTableLength = 0;
		return AE_OK;
	}


//Memory management functions

	/*! Maps a region of physical memory to an appropriate virtual address for the ACPICA
	 *  \param PhysicalAddress Physical address to be mapped
	 *  \param Length Length of the region in bytes
	 *  \return Pointer to the virtual mapping of the region, or NULL if the mapping failed
	 */
	void *AcpiOsMapMemory( ACPI_PHYSICAL_ADDRESS PhysicalAddress, ACPI_SIZE Length )
	{
		//Make sure we're aligned to a page boundry
		ACPI_PHYSICAL_ADDRESS base = PhysicalAddress & ~PAGE_OFFSET_MASK;
		ACPI_PHYSICAL_ADDRESS offset = PhysicalAddress - base;
		Length += offset;
		//Map the pages
		uintptr_t vbase = virtualMemoryManager::mapRegion( base, Length, PAGE_WRITE | PAGE_PRESENT );
		//Return a null value if the mapping failed, or return the appropriate virtual address
		if( !vbase )
			return NULL;
		else
			return reinterpret_cast <void *> ( vbase + offset );
	}

	/*! Unmaps a previously mapped region of memory
	 *  \param LogicalAddress Logical/Virtual address of the region to be unmapped
	 *  \param Length Length of the region to be unmapped in bytes
	 */
	void AcpiOsUnmapMemory( void *LogicalAddress, ACPI_SIZE Length )
	{
		Length += PAGE_OFFSET_MASK;
		Length &= ~PAGE_OFFSET_MASK;
		uintptr_t virt = reinterpret_cast <uintptr_t> ( LogicalAddress ) & ~PAGE_OFFSET_MASK;
		virtualMemoryManager::unmapRegion( virt, Length, false, true );
	}

	/*! Translates a logical address into a physical one
	 *  \param LogicalAddress Pointer representing the logical address
	 *  \param PhysicalAddress Pointer to the value that will contain the physical address translation
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	ACPI_STATUS AcpiOsGetPhysicalAddress( void *LogicalAddress, ACPI_PHYSICAL_ADDRESS *PhysicalAddress )
	{
		uintptr_t virt = reinterpret_cast <uintptr_t> ( LogicalAddress );
		ACPI_PHYSICAL_ADDRESS phys;

		if( !LogicalAddress || !PhysicalAddress )
			return AE_BAD_PARAMETER;
		if( !virtualMemoryManager::getPhysicalAddress( virt, phys ) )
		{
			kprintf("ACPI: Error getting physical address for logical address: %p\n", LogicalAddress);
			return AE_ERROR;
		}
		*PhysicalAddress = phys;
		return AE_OK;
	}

	/*! Allocates dynamic memory for the ACPICA
	 *  \param Size Size of the allocation in bytes
	 *  \return Address to the requested block of memory
	 */
	void *AcpiOsAllocate( ACPI_SIZE Size )
	{
		void *ptr = reinterpret_cast <void *> ( new uint8_t[Size] );
        if(!ptr)
            kprintf("ACPICA - Error kernel alloc failed!\n");
        return ptr;
	}

	/*! Frees a portion of previously allocated memory
	 *  \param Memory Memory block to be freed
	 */
	void AcpiOsFree( void *Memory )
	{
		delete[] reinterpret_cast <uint8_t *> ( Memory );
	}

	/*! Verifies a region of memory read access
	 *  \param Memory Logical address pointer of the region to verify
	 *  \param Length Length of the region in bytes
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	extern "C" BOOLEAN AcpiOsReadable( void *Memory, ACPI_SIZE Length )
	{
		uintptr_t address = reinterpret_cast <uintptr_t> ( Memory );
		if( !virtualMemoryManager::verifyAccess( address, Length, PAGE_PRESENT ) )
		{
			kprintf("AcpiOSReadable failed for region @ %X-%X\n", address, address + Length - 1);
			return false;
		}
		else
			return true;
	}

	/*! Verifies a region of memory for both read and write access
	 *  \param Memory Logical address pointer of the region to verify
	 *  \param Length Length of the region in bytes
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	BOOLEAN AcpiOsWritable( void *Memory, ACPI_SIZE Length )
	{
		uintptr_t address = reinterpret_cast <uintptr_t> ( Memory );
		if( !virtualMemoryManager::verifyAccess( address, Length, PAGE_PRESENT | PAGE_WRITE ) )
		{
			kprintf("AcpiOSWritable failed for region @ %X-%X\n", address, address + Length - 1);
			return FALSE;
		}
		else
			return TRUE;
	}


	//Spin lock functions

	/*! Creates a new spinlock object for use by the ACPICA subsystem
	 *  \param OutHandle Pointer to the destination object
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	ACPI_STATUS AcpiOsCreateLock( ACPI_SPINLOCK *OutHandle )
	{
		if( !OutHandle )
			return AE_BAD_PARAMETER;
		spinLockObj *lock = new spinLockObj;
		*OutHandle = reinterpret_cast <ACPI_SPINLOCK> ( lock );
		return AE_OK;
	}

	/*! Deletes a previously created spinlock object for the ACPICA subsystem
	 *  \param Handle Handle specifying the lock to be deleted
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	void AcpiOsDeleteLock( ACPI_SPINLOCK Handle )
	{
		if( !Handle )
			return;
        spinLockObj *lock = reinterpret_cast <spinLockObj *> ( Handle );
		delete lock;
	}

	/*! Acquires ownership of a spinlock for the ACPICA subsystem
	 *  \param Handle Spinlock object to acquire
	 *  \return Flags which are suppose to represent which registers are to be restored
	 */
	ACPI_CPU_FLAGS AcpiOsAcquireLock( ACPI_SPINLOCK Handle )
	{
		if( !Handle )
        {
            kprintf("[ACPI]: Error bad lock handle!: %p\n", Handle);

			return 0;
        }
		spinLockObj *lock = reinterpret_cast <spinLockObj *> ( Handle );
		lock->acquire();
		return 0;
	}

	/*! Releases ownership of a spinlock for the ACPICA subsystem
	 *  \param Handle Spinlock object to release
	 *  \param Flags Cpu flags to be restored
	 */
	void AcpiOsReleaseLock( ACPI_SPINLOCK Handle, ACPI_CPU_FLAGS COMPILER_UNUSED Flags )
	{
		if( !Handle )
			return;
		spinLockObj *lock = reinterpret_cast <spinLockObj *> ( Handle );
		lock->release();
	}

	//MMIO access

	/*! Reads a value from MMIO
	 *  \param Address Physical address to read data from
	 *  \param Value Return pointer for the data
	 *  \param Width Width of the read operation in bites
	 */
	ACPI_STATUS AcpiOsReadMemory( ACPI_PHYSICAL_ADDRESS Address, UINT64 *Value, UINT32 Width )
	{
		if( !Value )
			return AE_BAD_PARAMETER;

		tempMmioRegister reg( Address, Width );
		switch( Width )
		{
			case 8:
				*Value = reg.read<uint8_t> ();
				break;
			case 16:
				*Value = reg.read<uint16_t> ();
				break;
			case 32:
				*Value = reg.read<uint32_t> ();
				break;
			case 64:
				*Value = reg.read<uint64_t> ();
				break;
			default:
				return AE_BAD_PARAMETER;
				break;
		}

		return AE_OK;
	}

	/*! Writes data to a MMIO Register
	 *  \param Address Physical address of the port
	 *  \param Value Value to be written
	 *  \param Width Width of the write operation in bits
	 *  \return AE_OK If successful.  AE_BAD_PARAMETER if the value pointer or width size given are invalid
	 */
	ACPI_STATUS AcpiOsWriteMemory( ACPI_PHYSICAL_ADDRESS Address, UINT64 Value, UINT32 Width )
	{
		if( !Value )
			return AE_BAD_PARAMETER;

		tempMmioRegister reg( Address, Width );
		switch( Width )
		{
			case 8:
				reg.write<uint8_t> ( Value & 0xFF );
				break;
			case 16:
				reg.write<uint16_t> ( Value & 0xFFFF );
				break;
			case 32:
				reg.write<uint32_t> ( Value & 0xFFFFFFFFU);
				break;
			case 64:
				reg.write<uint64_t> ( Value );
				break;
			default:
				return AE_BAD_PARAMETER;
				break;
		}

		return AE_OK;
	}



	//IO Port access

	/*! Reads data from an IO port
	 *  \param Address IO port to read data from
	 *  \param Value Return pointer for the data
	 *  \param Width Width of the read in bits
	 *  \return Always AE_OK if the Width and Value fields are valid. AE_BAD_PARAMETER if either is invalid
	 */
	ACPI_STATUS  AcpiOsReadPort( ACPI_IO_ADDRESS Address, UINT32 *Value, UINT32 Width )
	{
        uint8_t bValue;
        uint16_t wValue;
        uint32_t dValue;
        dValue = 0;
		if( !Value )
			return AE_BAD_PARAMETER;
		switch( Width )
		{
			case 8:
				asm volatile ( "inb %%dx, %%al\n": "=a" ( bValue ) : "d" ( Address ) );
				dValue = bValue;
				break;
			case 16:
				asm volatile ( "inw %%dx, %%ax\n": "=a" ( wValue ) : "d" ( Address ) );
				dValue = wValue;
				break;
			case 32:
				asm volatile ( "inl %%dx, %%eax\n": "=a" ( dValue) : "d" ( Address ) );
				break;
			default:
				return AE_BAD_PARAMETER;
				break;
		}
		*Value = dValue;
		return AE_OK;
	}

	/*! Writes data to an IO Port
	 *  \param Address IO Port to write data to
	 *  \param Value Value to be written to port
	 *  \param Width Width of the write in bits
	 *  \return Always AE_OK if the Value field is valid. AE_BAD_PARAMETER if either is invalid
	 */
	ACPI_STATUS AcpiOsWritePort( ACPI_IO_ADDRESS Address, UINT32 Value, UINT32 Width )
	{
		switch( Width )
		{
			case 8:
				asm volatile ( "outb %%al, %%dx\n":: "d" ( Address ), "a" ( Value & 0xFFU ) );
				break;
			case 16:
				asm volatile ( "outw %%ax, %%dx\n":: "d" ( Address ), "a" ( Value & 0xFFFFU) );
				break;
			case 32:
				asm volatile ( "outl %%eax, %%dx\n":: "d" ( Address ), "a" ( Value ) );
				break;
			default:
				return AE_BAD_PARAMETER;
				break;
		}
		return AE_OK;
	}

	//Output handlers

	/*! Printf function ACPICA uses to output messages
	 *  \param format Standard printf style format line
	 */
	void ACPI_PRINTF_LIKE( 1 ) ACPI_INTERNAL_XFACE AcpiOsPrintf( const char *format, ... )
	{
		va_list args;
		va_start( args, format );
		AcpiOsVprintf( format, args );
		va_end( args );
	}

	/*! Vprintf style function ACPICA requires for message output
	 *  \param format Standard printf style format line
	 *  \param args Variable argument list containing addition arguments
	 */
	void ACPI_PRINTF_LIKE( 1 ) AcpiOsVprintf( const char *format, va_list args )
	{
		kvprintf( format, args );
	}

	//Timer support

	/*! Gets the value of the cpu timer in 100 nanosecond increments
	 *  \return Value of the system timer in units of 100 nanoseconds
	 */
	UINT64 AcpiOsGetTimer()
	{
		uint64_t val = Timer::readTSC();
		val /= localState::getTscScaleHundredNano();
		return val;
	}

	void AcpiOsSleep( UINT64 Milliseconds )
	{
	    processorScheduler *sched = localState::getSched();
		sched->sleepCurrent( Milliseconds );
	}

	void AcpiOsStall( UINT32 Microseconds )
	{
	    processorScheduler *sched = localState::getSched();
		sched->stall( Microseconds );
	}

    static ACPI_OSD_HANDLER acpicaSCIHandler = NULL;

    static int acpiSCIHandler(void *context, COMPILER_UNUSED const int index)
    {
    	if(acpicaSCIHandler)
            return ( acpicaSCIHandler(context) == ACPI_INTERRUPT_HANDLED);
        else
            return false;
    }

	//Interrupt handler functions
	ACPI_STATUS AcpiOsInstallInterruptHandler( UINT32 InterruptLevel, ACPI_OSD_HANDLER Handler, void *Context )
	{
		if( !Handler )
			return AE_BAD_PARAMETER;
        if(acpicaSCIHandler)
            return AE_ALREADY_EXISTS;

        acpicaSCIHandler = Handler;
        if(!interruptManager::installIrqHandler(InterruptLevel, &acpiSCIHandler, Context, INT_FLAGS_DELIVERY_FIXED |
                                                  INT_FLAGS_DELIVERY_PHYS | INT_FLAGS_TRIGGER_LEVEL | INT_FLAGS_POLARITY_LOW))
			kprintf("Failed to find the appropriate interrupt router for IRQ%u\n", InterruptLevel);
		return AE_OK;
	}

	ACPI_STATUS AcpiOsRemoveInterruptHandler( UINT32 COMPILER_UNUSED InterruptNumber, ACPI_OSD_HANDLER Handler )
	{
		if( !Handler )
			return AE_BAD_PARAMETER;
		if( acpicaSCIHandler == Handler)
        {
            acpicaSCIHandler = NULL;
            return AE_OK;
        }
        else
            return AE_NOT_EXIST;
	}

	//Task management
	/*! Gets an ID value for the currently executing thread
	 *  \return ID Value of the currently executing thread
	 */
	ACPI_THREAD_ID AcpiOsGetThreadId()
	{
		ACPI_THREAD_ID id = reinterpret_cast <ACPI_THREAD_ID> ( localState::getSched()->getCurrentTask() );
		return id;
	}



	ACPI_STATUS AcpiOsExecute( ACPI_EXECUTE_TYPE Type, ACPI_OSD_EXEC_CALLBACK Function, void *Context )
	{
		Task *task = NULL;
		if( !Function )
			return AE_BAD_PARAMETER;

        pid_t tPid;
        if(!Task::allocatePid(tPid) )
            return AE_LIMIT;
		switch( Type )
		{
			case OSL_GLOBAL_LOCK_HANDLER:
				task = new Task( Function, Context, 1, tPid );
				break;
			case OSL_NOTIFY_HANDLER:
				task = new Task( Function, Context, 1, tPid );
				break;
			case OSL_GPE_HANDLER:
				task = new Task( Function, Context, 50, tPid );
				break;
			case OSL_EC_POLL_HANDLER:
				task = new Task( Function, Context, 1, tPid );
				break;
			case OSL_EC_BURST_HANDLER:
				task = new Task( Function, Context, 40, tPid );
				break;
            case OSL_DEBUGGER_MAIN_THREAD:
                task = new Task( Function, Context, 1, tPid );
                break;
            case OSL_DEBUGGER_EXEC_THREAD:
                task = new Task( Function, Context, 1, tPid );
                break;
			default:
			    Task::freePid(tPid);
				return AE_BAD_PARAMETER;
				break;
		}
		Scheduler::registerTask( task );
		return AE_OK;
	}

	//Semaphore support

	/*! Creates a new semaphore for use by the ACPICA
	 *  \param maxUnits Maximum number of units the semaphore can contain
	 *  \param initialUnits Initial count of units the semaphore contains
	 *  \param OutHandle Pointer to the destination variable which will hold the semaphore handle
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	ACPI_STATUS AcpiOsCreateSemaphore( UINT32 maxUnits, UINT32 initialUnits, ACPI_SEMAPHORE *OutHandle )
	{
		if( !OutHandle )
			return AE_BAD_PARAMETER;
		else if( initialUnits > maxUnits )
			return AE_BAD_PARAMETER;

		semaphoreObj *semaphore = new semaphoreObj( maxUnits, initialUnits );
		*OutHandle = reinterpret_cast <ACPI_SEMAPHORE> ( semaphore );
		return AE_OK;
	}

	/*! Deletes a previously created semaphore object
	 *  \param Handle Handle representing the semaphore object to be deleted
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	ACPI_STATUS AcpiOsDeleteSemaphore( ACPI_SEMAPHORE Handle )
	{
		if( !Handle )
			return AE_BAD_PARAMETER;
		semaphoreObj *semaphore = reinterpret_cast <semaphoreObj *> ( Handle );
		delete semaphore;
		return AE_OK;
	}

	/*! Attempt to acquire and wait for a semaphore
	 *  \param Handle Handle object representing to the semaphore
	 *  \param Units Number of units to be acquired from the semaphore
	 *  \param Timeout Time to wait for the semaphore in milliseconds, a value of 0xFFFF represents an infinite wait time
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	ACPI_STATUS AcpiOsWaitSemaphore( ACPI_SEMAPHORE Handle, UINT32 Units, UINT16 Timeout )
	{
		if( !Handle)
		{
		    /* This is pretty ugly hack, for whatever reason ACPICA doesn't do mutex initialization when early table initialization
		       happens but still attempts to use table mutexs on subsequent table access functions.

		       As a result it will provide a NULL value to Handle which breaks the documented required of Handle being a value created by AcpiOsCreateSemaphore.
		       Once the subsystem is fully initialized this will no longer occur, but until that point a NULL value has to be accepted.  This also was not the behavior in
		       prior versions of ACPICA, someone apparently felt it was okay to introduce this behavior, probably because only a single table is loaded at one time.
            */
		    if(systemFullInit)
            {
                kprintf("Error bad semaphore handle!\n");
                return AE_BAD_PARAMETER;
            }
            else
                return AE_OK;
		}

		//kprintf("AcpiOsWaitSemaphore called!\n");
		semaphoreObj *semaphore = reinterpret_cast <semaphoreObj *> ( Handle );
		if( !semaphore->wait( Units, Timeout ) )
		{
			kprintf("Error: Semaphore timed out!\n");
			return AE_TIME;
		}
		else
			return AE_OK;
	}

	/*! Signal a semaphore to return units
	 *  \param Handle Handle representing the semaphore
	 *  \param Units Units to return to the semaphore
	 *  \return Return values and conditions are specified in the ACPICA Reference
	 */
	ACPI_STATUS AcpiOsSignalSemaphore( ACPI_SEMAPHORE Handle, UINT32 Units )
	{
		if( !Handle )
			return AE_BAD_PARAMETER;

		//kprintf("AcpiOsSignalSemaphore called!\n");
		semaphoreObj *semaphore = reinterpret_cast <semaphoreObj *> ( Handle );
		if( !semaphore->signal( Units ) )
			return AE_LIMIT;
		else
			return AE_OK;
	}

	//Signal function

	/*! Processes a signal sent from ACPICA to the host OS
	 *  \param Function Function number indicating the type of signal that was sent
	 *  \param Info Pointer to the signal information structure
	 *  \return Always AE_OK for now, proper termination might be necessary down the line
	 */
	ACPI_STATUS AcpiOsSignal( UINT32 Function, void *Info )
	{
		ACPI_SIGNAL_FATAL_INFO *fatal;
		switch( Function )
		{
			case ACPI_SIGNAL_FATAL:
				fatal = reinterpret_cast <ACPI_SIGNAL_FATAL_INFO *> ( Info );
				kprintf( "ACPI - ERROR: Encountered FatalOpcode! Type: %X Code - %X Arg - %X\n", fatal->Type, fatal->Code, fatal->Argument );
				break;
			default:
				break;
		}
		return AE_OK;
	}

	//PCI Bus access functions

	static uint32_t decodePciId( ACPI_PCI_ID *PciId )
	{
		uint32_t id = PciId->Segment;
		id <<= 8;
		id |= PciId->Bus & 0xFF;
		id <<= 5;
		id |= PciId->Device & 0x1F;
		id <<= 3;
		id |= PciId->Function & 0x7;

        return id;
	}

	/*! Reads data from the PCI Configuration space of a specific device
	 *  \param PciId Address of the device and function that the configuration space will be read for
	 *  \param Register Register in the device's configuration space to read from
	 *  \param Value Pointer indicating where the value in the register to be returned
	 *  \param Width Width of the read operation in bytes
	 *  \return AE_OK if the read completed successfully,
	 *  AE_BAD_ADDRESS if either the device or Register values were invalid, AE_BAD_PARAMETER if the Value or PciId fields are invalid
	 */
	ACPI_STATUS AcpiOsReadPciConfiguration( ACPI_PCI_ID *PciId, UINT32 Register, UINT64 *Value, UINT32 Width )
	{
		uint32_t id, dataDWord;
		uint16_t dataWord;
		uint8_t dataByte;

		if( !PciId )
			return AE_BAD_PARAMETER;
		else if( !Value )
			return AE_BAD_PARAMETER;

		//Convert the PCI Id value into a native value
		id = decodePciId( PciId );
        switch( Width )
		{
			case 8:
				if( pciBus::readByte( id, Register, dataByte ) )
				{
					*Value = dataByte;
					return AE_OK;
				}
				else
					return AE_BAD_ADDRESS;
				break;
			case 16:
				if( pciBus::readWord( id, Register, dataWord ) )
				{
					*Value = dataWord;
					return AE_OK;
				}
				else
					return AE_BAD_ADDRESS;
				break;
			case 32:
				if( pciBus::readDWord( id, Register, dataDWord ) )
				{
					*Value = dataDWord;
					return AE_OK;
				}
				else
					return AE_BAD_ADDRESS;
				break;
			case 64:
				if( pciBus::readQWord( id, Register, *Value ) )
					return AE_OK;
				else
					return AE_BAD_ADDRESS;
				break;
			default:
				return AE_BAD_PARAMETER;
				break;
		}
	}

	/*! Reads data to the PCI Configuration space of a specific device
	 *  \param PciId Address of the device and function that the configuration space will be read for
	 *  \param Register Register in the device's configuration space to write to
	 *  \param Value Value to be written to configuration space
	 *  \param Width Width of the write operation
	 *  \return AE_OK if the read completed successfully,
	 *  AE_BAD_ADDRESS if either the device or Register values were invalid, AE_BAD_PARAMETER if the Value or PciId fields are invalid
	 */
	ACPI_STATUS AcpiOsWritePciConfiguration( ACPI_PCI_ID *PciId, UINT32 Register, UINT64 Value, UINT32 Width )
	{
		if( !PciId )
			return AE_BAD_PARAMETER;
		else if( !Value )
			return AE_BAD_PARAMETER;

		//Convert the PCI Id value into a native value
		uint32_t id = decodePciId( PciId );

		//Actually preform the operation based upon with Width value we're given, or error out if it's a bogus value
		switch( Width )
		{
			case 8:
				if( pciBus::writeByte( id, Register, Value ) )
					return AE_OK;
				else
					return AE_BAD_PARAMETER;
				break;
			case 16:
				if( pciBus::writeWord( id, Register, Value ) )
					return AE_OK;
				else
					return AE_BAD_PARAMETER;
				break;
			case 32:
				if( pciBus::writeDWord( id, Register, Value ) )
					return AE_OK;
				else
					return AE_BAD_PARAMETER;
				break;
			case 64:
				if( pciBus::writeQWord( id, Register, Value ) )
					return AE_OK;
				else
					return AE_BAD_PARAMETER;
				break;
			default:
				return AE_BAD_PARAMETER;
				break;
		}


	}

        ACPI_STATUS AcpiOsGetLine(char *Buffer, UINT32 BufferLength, UINT32 *BytesRead)
        {
            while(!acpiDbCmdBuffer)
            {
                asm("pause\n"::);
            }
            char *cmdline = const_cast <char *> (acpiDbCmdBuffer);
            uint32_t l = String::strlen(cmdline);
            if(l > BufferLength )
                return AE_BUFFER_OVERFLOW;
            else
            {
                kprintf("Copying buffer: %s\n", cmdline);
                Memory::copy(Buffer, cmdline, l);
                *BytesRead = l;
                acpiDbCmdBuffer = NULL;
                return AE_OK;
            }
        }

        void AcpiOsWaitEventsComplete (void)
        {
            //TODO: Wait on all
        }

        ACPI_STATUS AcpiOsEnterSleep ( UINT8 SleepState, UINT32 RegaValue, UINT32 RegbValue)
        {
            //TODO: Actually write values to the sleep registers to put the system to sleep
        }

}
