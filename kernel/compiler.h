#ifndef COMPILER_H_
#define COMPILER_H_

#define NULL                   0

#define COMPILER_FUNC_CALLER   __builtin_return_address(0)

#define COMPILER_PACKED        __attribute__ ((packed))
#define COMPILER_UNUSED        __attribute__ ((unused))
#define COMPILER_INLINE        __attribute__ ((always_inline))
#define COMPILER_NOINLINE      __attribute__ ((noinline))
#define COMPILER_NORETURN      __attribute__ ((noreturn))
#define COMPILER_UNREACHABLE()   __builtin_unreachable()

#ifndef va_arg

#define va_arg(x, y)           __builtin_va_arg((x), y)
#define va_start(x, y)         __builtin_va_start((x), y)
#define va_end(x)              __builtin_va_end((x))

#endif

#define UINT8_MAX              0xFF
#define UINT16_MAX             0xFFFF
#define UINT32_MAX             0xFFFFFFFF

//! Atomics

#define atomicInc(x)           __sync_fetch_and_add((x), 1)
#define atomicDec(x)           __sync_fetch_and_sub((x), 1)
#define atomicAdd(x, y)        __sync_fetch_and_add((x), (y))
#define atomicSub(x, y)        __sync_fetch_and_sub((x), (y))
#define atomicOr(x, y)         __sync_fetch_and_or((x), (y))
#define atomicAnd(x, y)        __sync_fetch_and_and((x), (y))

#define atomicIncRes(x)        __sync_add_and_fetch((x), 1)
#define atomicDecRes(x)        __sync_sub_and_fetch((x), 1)
#define atomicAddRes(x, y)     __sync_add_and_fetch((x), (y))
#define atomicSubRes(x, y)     __sync_sub_and_fetch((x), (y))
#define atomicOrRes(x, y)      __sync_or_and_fetch((x), (y))
#define atomicAndRes(x, y)     __sync_and_and_fetch((x), (y))

#define atomicBoolCAS(var, expected, altered)  (__sync_bool_compare_and_swap((var), (expected), (altered)))


//! Bit twiddling
#define leftRotate64(x, y)      __builtin_rotateleft64((x), (y))


typedef __builtin_va_list   va_list;

//Variable lengths

typedef unsigned long int size_t;
typedef long int ssize_t;

typedef char int8_t;
typedef short int int16_t;
typedef int int32_t;
typedef long int int64_t;

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long int uint64_t;

typedef long int intptr_t;
typedef unsigned long int uintptr_t;

typedef unsigned long int physaddr_t;
typedef unsigned long int virtaddr_t;

typedef size_t off_t;
typedef uint32_t pid_t;
typedef uint32_t uid_t;
typedef uint32_t gid_t;
typedef int64_t time_t;

union qCastPtr
{
    uintptr_t addr;
    uint8_t *u8;
    uint16_t *u16;
    uint32_t *u32;
    uint64_t *u64;
    int8_t *i8;
    int16_t *i16;
    int32_t *i32;
    int64_t *i64;
    void *ptr;
};

#endif

