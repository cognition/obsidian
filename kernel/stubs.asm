%include "cpu_state.inc"

EXTERN procAvailable, procSchedCleanup
GLOBAL idleLoop, cleanupLoop



idleLoop:
    cli
    mov rax, rsp
    push qword KERNEL_DATA_SEG
    push rax
    pushf
    push qword KERNEL_CODE_SEG
    lea rax, [rel idleLoop]
    push rax
    push qword 0
    push qword 0
    ; GP Register adjustment
    sub rsp, RDX_SAVE+8

    ; Segment register saves
    mov word [rsp + DS_SAVE], KERNEL_DATA_SEG
    mov word [rsp + ES_SAVE], KERNEL_DATA_SEG
    mov word [rsp + FS_SAVE], KERNEL_DATA_SEG
    mov word [rsp + GS_SAVE], KERNEL_DATA_SEG

    mov rdi, rsp
    sti
    call procAvailable

    add rsp, SS_AUTOSAVE+8
    hlt
    jmp idleLoop

cleanupLoop:
    call procSchedCleanup
    jmp cleanupLoop
