#ifndef KERNEL_MAIN_H_
#define KERNEL_MAIN_H_

#include "bootloader.h"


extern "C"
{
	int apKernMain( uintptr_t apicBase )
	int kernelMain( bootLoaderInfo *bootInfo );
}

#endif

