#include <cpu/processor.h>
#include "firmware.h"
#include "bios32.h"
#include <io/io.h>

#define BIOS32_SERVICE_SEARCH_START             0xE0000
#define BIOS32_SERVICE_SEARCH_END               0xFFFF0

static uint32_t bios32ServiceSignature = 0x5F32335F;

struct bios32ServiceDirectory
{
    uint32_t signature;
    uint32_t entry32;
    uint8_t revision, length, checksum, _reserved[5];
};

struct COMPILER_PACKED
{
    uint16_t cs, ds;
    uint32_t entry;
} bios32ServiceCall;


bios32service::bios32service(uint32_t baseAddress, uint32_t serviceLength, uint32_t serviceEntry) : entry(serviceEntry)
{
    cs = Processor::createLdtEntry(baseAddress, serviceLength, DT_TYPE_CODE | DT_TYPE_32BIT | DT_TYPE_PRESENT );
    ds = Processor::createLdtEntry(baseAddress, serviceLength, DT_TYPE_DATA | DT_TYPE_32BIT | DT_TYPE_PRESENT );
}

void bios32service::execute(regs32 *callInfo)
{
    farCall32 serviceCall;
    serviceCall.eip = entry;
    serviceCall.cs = cs;
    serviceCall.regs = *callInfo;
    serviceCall.regs.ds = ds;
    serviceCall.regs.es = ds;
    callFar32(&serviceCall);
    *callInfo = serviceCall.regs;
}

namespace bios32
{
    bool init(void)
    {
        uintptr_t addr = BIOS32_SERVICE_SEARCH_START;

        do
        {
            uint32_t *test = reinterpret_cast <uint32_t *> (addr);
            if(*test == bios32ServiceSignature)
            {
                bios32ServiceDirectory *dir = reinterpret_cast <bios32ServiceDirectory *> (addr);
                if( Firmware::doChecksum( dir, dir->length*16 ) )
                {
                    uintptr_t base = dir->entry32;
                    uintptr_t offset =  base & 0xFFF;
                    base -= offset;

                    bios32ServiceCall.entry = offset;
                    bios32ServiceCall.cs = Processor::createLdtEntry(base, 0x1FFF, DT_TYPE_CODE | DT_TYPE_PRESENT | DT_TYPE_32BIT );
                    bios32ServiceCall.ds = Processor::createLdtEntry(base, 0x1FFF, DT_TYPE_DATA | DT_TYPE_PRESENT | DT_TYPE_32BIT );
                    return true;
                }
            }

            addr += 16;
        } while(addr <= BIOS32_SERVICE_SEARCH_END);

        return false;
    }

    bios32service *requestService(uint32_t id)
    {
        farCall32 b32Call;
        b32Call.cs = bios32ServiceCall.cs;
        b32Call.regs.ds = bios32ServiceCall.ds;
        b32Call.regs.es = bios32ServiceCall.ds;
        b32Call.eip = bios32ServiceCall.entry;
        b32Call.regs.eax = id;
        b32Call.regs.ebx = 0;
        callFar32(&b32Call);
        if(b32Call.regs.eax & 0xFF)
            return NULL;
        else
        {
            if(!b32Call.regs.ecx)
                b32Call.regs.ecx = 0x10000;
            bios32service *service = new bios32service(b32Call.regs.ebx, b32Call.regs.ecx, b32Call.regs.edx );
            return service;
        }

    }
}
