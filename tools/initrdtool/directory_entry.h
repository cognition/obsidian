#ifndef DIRECTORY_ENTRY_H_
#define DIRECTORY_ENTRY_H_

#include "file_entry.h"
#include <string>
#include <vector>
#include <fstream>
#include <memory>

using namespace std;

class directoryEntry : public rawEntry
{
	public:
		directoryEntry(string entryName, const uint32_t nOffset);
		void addFile(shared_ptr<fileEntry> &entry);
		void addSubdir(shared_ptr<directoryEntry> &entry);
		const string &getName(void) const;
		void registerStrings(void);
		bool outputEntry(char *sTable, shared_ptr<ofstream> &outputFile);
	private:
		const string name;
		vector< shared_ptr<directoryEntry> > subdirs;
		vector< shared_ptr<fileEntry> > files;
		uint32_t totalStringSize;
};

#endif // DIRECTORY_ENTRY_H_
