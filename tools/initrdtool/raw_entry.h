#ifndef RAW_ENTRY_H_INCLUDED
#define RAW_ENTRY_H_INCLUDED

#include <cstdint>

#define RAW_ENTRY_DIRECTORY            0x8000

class __attribute__ ((packed)) rawEntry
{
	public:
		rawEntry(const uint32_t nOffset);
		uint32_t getLength(void) const;
	protected:
		uint32_t length;
		uint32_t flags;
		uint32_t offset;
		uint32_t nameOffset;
};

#endif // RAW_ENTRY_H_INCLUDED
