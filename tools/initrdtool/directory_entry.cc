#include "directory_entry.h"
#include <cstring>

directoryEntry::directoryEntry(string entryName, const uint32_t nOffset) : rawEntry(nOffset), name(entryName), totalStringSize(0)
{
	flags = RAW_ENTRY_DIRECTORY;
}

void directoryEntry::addFile(shared_ptr<fileEntry> &entry)
{
	const string &fName = entry->getName();
	totalStringSize += fName.length() + 1;
	files.push_back(entry);
	length += sizeof(rawEntry);
}

void directoryEntry::addSubdir(shared_ptr<directoryEntry> &entry)
{
	const string &dName = entry->getName();
	totalStringSize += dName.length() + 1;
	subdirs.push_back(entry);
	length += sizeof(rawEntry);
}

const string &directoryEntry::getName(void) const
{
	return name;
}

bool directoryEntry::outputEntry(char *sTable, shared_ptr<ofstream> &outputFile)
{
	strcpy( &sTable[nameOffset], name.c_str() );
	offset = outputFile->tellp();
	outputFile->seekp(length, ios_base::cur);

	for(uint32_t i = 0; i < files.size(); i++)
		if(!files[i]->outputEntry(sTable, outputFile))
			return false;
	for(uint32_t i = 0; i < subdirs.size(); i++)
		if(!subdirs[i]->outputEntry(sTable, outputFile))
			return false;

	uint32_t endOffset = outputFile->tellp();
	outputFile->seekp(offset, ios_base::beg);

	for(uint32_t i = 0; i < subdirs.size(); i++)
	{
		rawEntry *rawData = static_cast <rawEntry *> (&*subdirs[i]);
		outputFile->write( reinterpret_cast <const char *> (rawData), sizeof(rawEntry) );
	}

	for(uint32_t i = 0 ; i < files.size(); i++)
	{
		rawEntry *rawData = static_cast <rawEntry *> (&*files[i]);
		outputFile->write( reinterpret_cast <const char *> (rawData), sizeof(rawEntry) );
	}

	outputFile->seekp(endOffset, ios_base::beg);
	return true;
}
