#ifndef FILE_ENTRY_H_
#define FILE_ENTRY_H_

#include <memory>
#include <fstream>
#include <string>
#include "raw_entry.h"

using namespace std;

class fileEntry : public rawEntry
{
	public:
		fileEntry(string entryName, string filePath, const uint32_t nOffset);
		const string &getName(void) const;
		bool outputEntry(char *sTable, shared_ptr<ofstream> &outputFile);
	private:
		const string name;
		const string path;
};

#endif
