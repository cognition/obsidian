#include <iostream>
#include "image_file.h"

static constexpr union
{
	char chars[8];
	uint64_t val;
} imageMagicU = {  {'O', 'B', 'S', ' ', 'I', 'M', 'G', ' '} };

imageFile::imageFile(string imageFilename) : fileName(imageFilename), rootDirEntry( new directoryEntry("", 0) ), stringTableSize(0), stringTable(NULL)
{
}

imageFile::~imageFile()
{
	delete stringTable;
}

uint32_t imageFile::allocateStringTableSpace(const string &s)
{
	return stringTableSize.fetch_add(s.length() + 1);
}

shared_ptr <directoryEntry> &imageFile::getRootDirEntry(void)
{
	return rootDirEntry;
}

const char *imageFile::getString(uint32_t index) const
{
	if(index < stringTableSize)
		return &stringTable[index];
	else
		return NULL;
}

bool imageFile::writeFile(void)
{
	stringTable = new char[stringTableSize.load()];

	shared_ptr<ofstream> outFile( new ofstream(fileName, ios_base::out | ios_base::binary | ios_base::trunc) );
	if( outFile->fail() )
		return false;

	imageFileHeader header = { imageMagicU.val, 0, stringTableSize.load(), rootDirEntry->getLength() };

	outFile->seekp( sizeof(imageFileHeader) + stringTableSize.load(), ios_base::beg );

	stringTable = new char[stringTableSize.load()];
	cout << "Writing directory structure..." << endl;
	if(!rootDirEntry->outputEntry(stringTable, outFile))
		return false;

	header.imageSize = outFile->tellp();
	outFile->seekp( 0, ios_base::beg );
	outFile->write( reinterpret_cast <const char *> (&header), sizeof(imageFileHeader) );
	outFile->write( stringTable, stringTableSize.load() );
	cout << "Current location: " << outFile->tellp() << endl;
	outFile->flush();
	outFile->close();

	cout << "Header size: " << sizeof(imageFileHeader) << endl;
	cout << "String table size: " << stringTableSize.load() << endl;
	return true;
}
