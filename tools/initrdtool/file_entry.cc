#include "file_entry.h"
#include <cstring>

fileEntry::fileEntry(string entryName, string filePath, const uint32_t nOffset) : rawEntry(nOffset), name(entryName), path(filePath)
{

}

const string &fileEntry::getName(void) const
{
	return name;
}

bool fileEntry::outputEntry(char *sTable, shared_ptr<ofstream> &outputFile)
{
	//Copy the string content into the string table buffer
	strcpy( &sTable[nameOffset], name.c_str() );

	ifstream inFile(path, ios_base::in | ios_base::binary);
	if(inFile.fail())
		return false;

	inFile.seekg(0, ios_base::end);
	length = inFile.tellg();
	inFile.seekg(0, ios_base::beg);
	offset = outputFile->tellp();
	char *buffer = new char[length];

	inFile.read(buffer, length);
	outputFile->write(buffer, length);

	bool ret = !inFile.fail() || !outputFile->fail();
	inFile.close();

	return ret;
}
