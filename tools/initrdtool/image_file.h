#ifndef IMAGE_FILE_H_
#define IMAGE_FILE_H_

#include "directory_entry.h"
#include <atomic>

using namespace std;

struct __attribute__ ((packed)) imageFileHeader
{
	uint64_t magic;
	uint32_t imageSize;
	uint32_t stringTableSize;
	uint32_t rootDirSize;
};

class imageFile
{
	public:
		imageFile(string imageFilename);
		~imageFile();
		uint32_t allocateStringTableSpace(const string &s);
		void allocateStringTable(void);
		shared_ptr <directoryEntry> &getRootDirEntry(void);
		bool writeFile(void);
	private:
		const char *getString(const uint32_t index) const;

		const string fileName;
		shared_ptr <directoryEntry> rootDirEntry;
		atomic<uint32_t> stringTableSize;
		char *stringTable;
};

#endif
