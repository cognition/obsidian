#include <cstdint>
#include <cstdlib>
#include <memory.h>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include "image_file.h"


using namespace std;

static const char *helpMsg[] = {
	"Obsidian initial ramdisk utility.",
	"",
	"Usage: initrdutil initrd-image image-root",
	""
};

static const uint32_t helpMsgLines = sizeof(helpMsg)/sizeof(const char *);

static void printCommandUsage(void)
{
	cout << endl;
	for(uint32_t i = 0; i < helpMsgLines; i++)
		cout << helpMsg[i] << endl;
}


static bool addDirectory(string path, DIR *directory, shared_ptr <directoryEntry> &directoryBlock, shared_ptr <imageFile> &outImg)
{
	dirent64 data;
	dirent64 *entry = &data;
	memset(entry, 0, sizeof(dirent64));
	while( readdir64_r(directory, entry, &entry) == 0)
	{
		if(!entry)
			break;
		switch(entry->d_type)
		{
			case DT_DIR:
				{
					string dName(entry->d_name);
					if( dName.compare("..") == 0)
						break;
					else if(dName.compare(".") == 0)
						break;
					string subpath(path);
					subpath += '/';
					subpath += dName;

					DIR *dirPtr = opendir( subpath.c_str() );
					if(!dirPtr)
					{
						cerr << "Error: Unable to open subdirectory \"" << subpath << '\"' << endl;
						return false;
					}

					const uint32_t nameOffset = outImg->allocateStringTableSpace(dName);
					shared_ptr <directoryEntry> subEntry( new directoryEntry(entry->d_name, nameOffset) );
					directoryBlock->addSubdir(subEntry);

					if( !addDirectory(subpath, dirPtr, subEntry, outImg) )
						return false;
				}
				break;
			case DT_LNK:
			case DT_REG:
				{
					string fName(entry->d_name);
					string fPath(path);
					fPath += '/';
					fPath += fName;

					const uint32_t nameOffset = outImg->allocateStringTableSpace(fName);
					shared_ptr<fileEntry> entry( new fileEntry(fName, fPath, nameOffset) );
					directoryBlock->addFile(entry);
				}
				break;
			default:
				return false;
				break;
		}
	}

	return true;
}


int main(int argc, const char **argv)
{
	if(argc < 3)
	{
		cerr << "Error: Incorrect command usage!" << endl;
		printCommandUsage();
		return EXIT_FAILURE;
	}

	string imageFilename = argv[1];
	string baseDirectoryName = argv[2];
	cout << "Image file: " << imageFilename << endl;
	cout << "Source directory: " << baseDirectoryName << endl;
	DIR *baseDir = opendir( baseDirectoryName.c_str() );
	if(!baseDir)
	{
		cerr << "Error: Unable to open image directory \"" << baseDirectoryName << '\"' << endl;
		return EXIT_FAILURE;
	}

	shared_ptr<imageFile> imagePtr( new imageFile(imageFilename) );
	shared_ptr<directoryEntry> rootEntry = imagePtr->getRootDirEntry();
	if(!addDirectory(baseDirectoryName, baseDir, rootEntry, imagePtr))
		return EXIT_FAILURE;

	if(imagePtr->writeFile())
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}
