#include "display_config.h"
#include <efi.h>
#include <efilib.h>

#define EDID_DDT_ENTRY_SIZE          18

#define EDID_PREF_MODE_OFF           0x36
#define EDID_ALT_DDT_START           0x48
#define EDID_EXT_COUNT               0x7E
#define EDID_DDT_CVT3                0xF8
#define EDID_DDT_DRL                 0xFD


typedef struct __attribute__ ((packed))
{
  UINT16 pixelClock;
  UINT8 horizontalPixelsLow;
  UINT8 horizontalBlankLow;
  UINT8 horizontalInfoHigh;
  UINT8 verticalLinesLow;
  UINT8 verticalBlankLow;
  UINT8 verticalInfoHigh;
  UINT8 horizontalFrontPorch;
  UINT8 horizontalSyncPulseWidth;
  UINT8 verticalExtInfoLow;
  UINT8 extInfoHigh;
  UINT8 horizontalPhysLow;
  UINT8 verticalPhysLow;
  UINT8 physHigh;
  UINT8 horizontalBorder;
  UINT8 verticalBorder;
} detailTimingDef;

typedef struct __attribute__ ((packed))
{
  UINT16 generalType;
  UINT8 _reserved;
  UINT8 tag;
  UINT8 _reserved2;
} genericDDT;

typedef struct __attribute__ ((packed))
{
  genericDDT ddtHeader;
} cvtEntry;

EFI_STATUS retrieveBestMode(EFI_GRAPHICS_OUTPUT_PROTOCOL *gop, EFI_PHYSICAL_ADDRESS *fbAddr, UINTN *modeBytesReq )
{
  EFI_STATUS status;
  UINT32 maxMode;
  UINTN infoSize;
  EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE *protMode = gop->Mode;
  EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *modeInfo;
  *fbAddr = protMode->FrameBufferBase;
  *modeBytesReq = protMode->FrameBufferSize;
  maxMode = protMode->MaxMode;

  for(UINT32 mode = 0; mode < maxMode; mode++)
  {
    status = uefi_call_wrapper(gop->QueryMode, 4, gop, mode, &infoSize, &modeInfo);
    if(status != EFI_SUCCESS)
    {
      Print(L"Unable to retrieve info for mode %i\n", mode);
      Pause();
      return status;
    }

    Print(L"Found available graphics mode[%u]: %ux%u\n", mode, modeInfo->HorizontalResolution, modeInfo->VerticalResolution);
  }

  return EFI_SUCCESS;
}

int doChecksum(UINT8 *bytes, UINTN size)
{
  UINT8 runningSum = 0;
  for(UINTN index = 0; index < size; index++)
    runningSum += bytes[index];

  return runningSum;
}

cvtEntry *getCVTSupport(const UINT8 *edid, UINTN edidSize)
{
  UINTN offset = EDID_ALT_DDT_START;

  for(UINTN entry = 0; entry < 3; entry++)
  {
    genericDDT *ddt = (genericDDT *)&edid[offset];
    if(ddt->generalType == 0)
    {
      Print(L"Found DDT of type: %X\n", ddt->tag);
      if(ddt->tag == EDID_DDT_CVT3)
      {
        Print(L"Found suitable display info via CVT3\n");
      }
      else if(ddt->tag == EDID_DDT_DRL)
      {
        Print(L"Found suitable display info via DRL\n");
      }
    }

    offset += EDID_DDT_ENTRY_SIZE;
  }


  return 0;
}

EFI_STATUS getBestMode(EFI_GRAPHICS_OUTPUT_PROTOCOL *gfx, UINT32 *modeNumOut, frameBufferInfo *outInfo, const UINT32 width, const UINT32 height)
{
	EFI_STATUS status;
	EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *info;
	UINTN infoSize;
	UINT32 bestMode = gfx->Mode->MaxMode;
	UINT32 maxDepth = 0;
	

	if(width && height)
	{
		for(UINT32 mode = 0; mode < gfx->Mode->MaxMode; mode++)
		{
			UINT32 totalMask = 0;
			status = uefi_call_wrapper(gfx->QueryMode, 4, gfx, mode, &infoSize, &info);

			if(status != EFI_SUCCESS)
			{
				Print(L"Error getting graphics mode information for mode %i\n", mode);
				return status;
			}
			if(info->HorizontalResolution == width && info->VerticalResolution == height)
			{
				UINT32 depth;
				outInfo->width = width;
				outInfo->height = height;
				outInfo->pixPerScanLine = info->PixelsPerScanLine;
				switch(info->PixelFormat)
				{
					case PixelRedGreenBlueReserved8BitPerColor:
						outInfo->bytesPerPixel = 4;
						outInfo->redMask = 0xFFU;
						outInfo->greenMask = 0xFF00U;
						outInfo->blueMask = 0xFF0000U;
						outInfo->rsvdMask = 0U;
						*modeNumOut = mode;
						return EFI_SUCCESS;

						break;
					case PixelBlueGreenRedReserved8BitPerColor:
						outInfo->bytesPerPixel = 4;
						outInfo->redMask = 0xFF0000U;
						outInfo->greenMask = 0xFF00U;
						outInfo->blueMask = 0xFFU;
						outInfo->rsvdMask = 0U;
						*modeNumOut = mode;
						return EFI_SUCCESS;

						break;
					case PixelBitMask:

						totalMask = info->PixelInformation.RedMask;
						totalMask |= info->PixelInformation.BlueMask;
						totalMask |= info->PixelInformation.GreenMask;
						totalMask |= info->PixelInformation.ReservedMask;
						depth = __builtin_popcount(totalMask);
						maxDepth = depth <= maxDepth ? maxDepth:depth;
						if(maxDepth == depth)
						{
							bestMode = mode;
							outInfo->bytesPerPixel = (maxDepth + 7)/8;
							outInfo->redMask = info->PixelInformation.RedMask;
							outInfo->blueMask = info->PixelInformation.BlueMask;
							outInfo->greenMask = info->PixelInformation.GreenMask;
							outInfo->rsvdMask = info->PixelInformation.ReservedMask;					
						}
						if(maxDepth == 32)
						{
							*modeNumOut = mode;
							return EFI_SUCCESS;
						}

						break;
					default:
						break;
				}
			}
		}

	}
	else
	{
		bestMode = gfx->Mode->MaxMode;		
		UINT32 maxArea = 0;
		for(UINT32 mode = 0; mode < gfx->Mode->MaxMode; mode++)
		{
			UINT32 currentArea, currentDepth, totalMask;
			status = uefi_call_wrapper(gfx->QueryMode, 4, gfx, mode, &infoSize, &info);
			if(status != EFI_SUCCESS)
			{
				Print(L"Error getting graphics mode information for mode %i\n", mode);
				return status;
			}
			currentArea = info->HorizontalResolution*info->VerticalResolution;

			switch(info->PixelFormat)
			{
				case PixelRedGreenBlueReserved8BitPerColor:
					currentDepth = 32;
					break;
				case PixelBlueGreenRedReserved8BitPerColor:
					currentDepth = 32;
					break;
				case PixelBitMask:
					totalMask = info->PixelInformation.RedMask;
					totalMask |= info->PixelInformation.BlueMask;
					totalMask |= info->PixelInformation.GreenMask;
					totalMask |= info->PixelInformation.ReservedMask;
					currentDepth = __builtin_popcount(totalMask);
					break;
				default:
					break;
			}

			if(currentArea > maxArea)
			{				
				maxDepth = currentDepth;
				maxArea = currentArea;
				bestMode = mode;
				outInfo->bytesPerPixel = (maxDepth + 7)/8;
				outInfo->redMask = info->PixelInformation.RedMask;
				outInfo->blueMask = info->PixelInformation.BlueMask;
				outInfo->greenMask = info->PixelInformation.GreenMask;
				outInfo->rsvdMask = info->PixelInformation.ReservedMask;
				outInfo->pixPerScanLine = info->PixelsPerScanLine;
				outInfo->height = info->VerticalResolution;
				outInfo->width = info->HorizontalResolution;					
			}
			else if(currentArea == maxArea && currentDepth > maxDepth)
			{
				maxDepth = currentDepth;
				bestMode = mode;
				outInfo->bytesPerPixel = (maxDepth + 7)/8;
				outInfo->redMask = info->PixelInformation.RedMask;
				outInfo->blueMask = info->PixelInformation.BlueMask;
				outInfo->greenMask = info->PixelInformation.GreenMask;
				outInfo->rsvdMask = info->PixelInformation.ReservedMask;
				outInfo->pixPerScanLine = info->PixelsPerScanLine;
				outInfo->height = info->VerticalResolution;
				outInfo->width = info->HorizontalResolution;					
			}
		}

	}

	if(bestMode == gfx->Mode->MaxMode)
		return EFI_NOT_FOUND;
	else
	{
		*modeNumOut = bestMode;
		return EFI_SUCCESS;
	}

}

EFI_STATUS retrieveAllEDID(EFI_GRAPHICS_OUTPUT_PROTOCOL **gfxOut, UINT32 *modeNumOut, frameBufferInfo *fbInfoOut)
{
  EFI_HANDLE gfxHandle;
  EFI_DEVICE_PATH *gfxPath;
  EFI_GRAPHICS_OUTPUT_PROTOCOL *gfxProto;
  EFI_HANDLE *handles;
  UINT32 prefHorizontalRes, prefVerticalRes;
		
  EFI_STATUS status;
  UINTN bufferSize = 0;
  UINT32 targetHres, targetVres;
  
  gfxProto = 0;

  status = uefi_call_wrapper(BS->LocateHandle, 5, ByProtocol, &EdidDiscoveredProtocol, 0, &bufferSize, handles);

  handles = (EFI_HANDLE *)AllocateZeroPool(bufferSize);
  status = uefi_call_wrapper(BS->LocateHandle, 5, ByProtocol, &EdidDiscoveredProtocol, 0, &bufferSize, handles);

  if(status != EFI_SUCCESS)
  {
    Print(L"Error: Could not find any EDID protocols.  Code: %u\n", status);
    Pause();
    return status;
  }

  Print(L"Found %u handles with EDID information.\n", bufferSize/sizeof(EFI_HANDLE));

  for(UINTN handleNum = 0; handleNum < bufferSize/sizeof(EFI_HANDLE); handleNum++)
  {

    EFI_EDID_ACTIVE_PROTOCOL *edidProto;
    detailTimingDef *prefTiming;
    status = uefi_call_wrapper(BS->HandleProtocol, 3, handles[handleNum], &EdidActiveProtocol, &edidProto);
    if(status != EFI_SUCCESS)
      continue;

    if(!edidProto->SizeOfEdid)
    {
	 /* This is basically just a hack for VirtualBox since it will gladly hand the bootloader
       * A junk structure of size zero and pretty much every resolution in the book regardless of what
       * root system's resolution is.
       */

	 /* TODO: Eventually add settings file for this */
      prefHorizontalRes = 1920;
      prefVerticalRes = 1080;
    }
    else if(doChecksum(edidProto->Edid, edidProto->SizeOfEdid))
      return EFI_CRC_ERROR;
    else
    {



      Print(L"Found EDID version %u.%u\n", edidProto->Edid[0x12], edidProto->Edid[0x13]);

      prefTiming = (detailTimingDef *)&(edidProto->Edid[EDID_PREF_MODE_OFF]);

      prefHorizontalRes = prefTiming->horizontalInfoHigh & 0xF0;
      prefHorizontalRes <<= 4;
      prefHorizontalRes |= prefTiming->horizontalPixelsLow;

      prefVerticalRes = prefTiming->verticalInfoHigh & 0xF0;
      prefVerticalRes <<= 4;
      prefVerticalRes |= prefTiming->verticalLinesLow;
   }
	gfxPath = DuplicateDevicePath( DevicePathFromHandle(handles[handleNum]) );
	status = uefi_call_wrapper(BS->LocateDevicePath, 3, &GraphicsOutputProtocol, &gfxPath, &gfxHandle);

	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to find graphics cards! Status: %u\n", status);
		if(status == EFI_NOT_FOUND)
		{
		}
		else
		{
		}
	}
	else
	{
		status = uefi_call_wrapper(BS->HandleProtocol, 3, gfxHandle, &GraphicsOutputProtocol, &gfxProto);
		if(status != EFI_SUCCESS)
		{
			*gfxOut = NULL;
			Print(L"Unable to find matching graphics output procotol for the display.\n");
		}
		else
		{
			*gfxOut = gfxProto;
		}
	}

  }



	if(gfxProto)
	{
		status = getBestMode(gfxProto, modeNumOut, fbInfoOut, prefHorizontalRes, prefVerticalRes);
		if(status != EFI_SUCCESS)
		{
			Print(L"Unable to find graphics mode! Status: %u\n", status);
		}
		else
		{
			Print(L"Best graphics mode: %u\n", *modeNumOut);
		}
	}
	else
	{
		Print(L"Unable to located graphics card protocol!\n");

	}


edidDone:
  FreePool(handles);
  return EFI_SUCCESS;
}


