#ifndef DISPLAY_CONFIG_H_
#define DISPLAY_CONFIG_H_

#include <efi.h>


typedef struct __attribute__ ((packed))
{
	UINT64 fbVirtAddr, bbVirtAddr;
	UINT32 width, height, bytesPerPixel, pixPerScanLine, totalSize;
	UINT32 redMask, greenMask, blueMask, rsvdMask;
} frameBufferInfo;


EFI_STATUS retrieveAllEDID(EFI_GRAPHICS_OUTPUT_PROTOCOL **gfxOut, UINT32 *modeNumOut, frameBufferInfo *fbInfoOut);

#endif
