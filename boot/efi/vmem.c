#include "vmem.h"

/* Maps pages in a continous range @ physAddr to virtAddr with permissions access
 \param physAddr Base of the physical address range
 \param virtAddr Base of the virtual address range
 \param count Number of 4K pages to map
 \param access Access flags
 \param l4 Pointer to physical address of the L4 page table
 \param freeAddr Current physical address for zero-initialized pages for higher level page table structures
 \param printMsg Verbose information flag
 */
void mapPages(const UINT64 physAddr, const UINT64 virtAddr, const UINT64 count, const UINT64 access, UINT64 *l4, UINT64 *freeAddr, const int printMsg)
{
	UINT64 virtPtr, physPtr, pagesRem, l4Off, pdpOff, pdOff, ptOff, freeOff, hlAccess;

	physPtr = physAddr;
	virtPtr = virtAddr;
	pagesRem = count;

	freeOff = *freeAddr;
	/* Get the initial offsets into each level of the page table */
	l4Off = (virtAddr>>39)%512;
	pdpOff = (virtAddr>>30)%512;
	pdOff = (virtAddr>>21)%512;
	ptOff = (virtAddr>>12)%512;

	/* Only certain access flags are universally applicable through all levels of the page table, adjust for them */
	hlAccess = access & (PAGE_PRESENT | PAGE_WRITE | PAGE_USER);

	if(printMsg)
	{
		Print(L"Mapping %u pages from %lX to %lX\n", count, physAddr, virtAddr);
		Print(L"Init offsets - L4: %u PDP: %u PD: %u PT: %u\n", l4Off, pdpOff, pdOff, ptOff);
	}

	for(;l4Off < 512; l4Off++)
	{
		UINT64 *pdpPtr;
		 if(!l4[l4Off])
		 {
			/* Allocate a fresh new PDP table */
			 l4[l4Off] = freeOff | hlAccess;
			 pdpPtr = (UINT64 *)freeOff;
			 freeOff += PAGE_SIZE;
			 if(printMsg)
			 	Print(L"Created L4 Entry: %lX\n", l4[l4Off]);
		 }
		 else
		 {
			/* Use the existing PDP table */
			 l4[l4Off] |= hlAccess;
			 pdpPtr = (UINT64 *)(l4[l4Off] & ~PAGE_CONFIG_BITS);
			 if(printMsg)
			 	Print(L"Found L4 Entry: %lX\n", l4[l4Off]);
		 }

		for(; pdpOff < 512; pdpOff++)
		{
			 UINT64 * volatile pdPtr;
			 if(!pdpPtr[pdpOff])
			 {
				/* Allocate a new page directory table */
				 pdpPtr[pdpOff] = freeOff | hlAccess;
				 pdPtr = (UINT64 * volatile)freeOff;
				 freeOff += PAGE_SIZE;
				 if(printMsg)
				 	Print(L"Created PDP Entry: %lX\n", pdpPtr[pdpOff]);

			 }
			 else
			 {
				/* Use the existing page directory table */
				 pdpPtr[pdpOff] |= hlAccess;
				 pdPtr = (UINT64 * volatile )(pdpPtr[pdpOff] & ~PAGE_CONFIG_BITS);
				 if(printMsg)
				 	Print(L"Found PDP Entry: %lX\n", pdpPtr[pdpOff]);
			 }
			for(; pdOff < 512; pdOff++)
			{
				 UINT64 * volatile ptPtr;
				 if(!pdPtr[pdOff])
				 {
					/* Allocate a new page table */
					 pdPtr[pdOff] = freeOff | hlAccess;
					 ptPtr = (UINT64 * volatile)freeOff;
					 freeOff += PAGE_SIZE;
					 if(printMsg)
					 	Print(L"Created PD[%u] entry: %lX\n", pdOff, pdPtr[pdOff]);

				 }
				 else
				 {
					/* Use the existing table */
					 pdPtr[pdOff] |= hlAccess;
					 ptPtr = (UINT64 * volatile)(pdPtr[pdOff] & ~PAGE_CONFIG_BITS);
					 if(printMsg)
					 	Print(L"Found PD entry: %lX\n", pdPtr[pdOff]);
				 }

				for(; ptOff < 512; ptOff++, physPtr += PAGE_SIZE)
				{
					/* Set the invidual page table entries */
					ptPtr[ptOff] = physPtr | access;
					pagesRem--;					
					if(!pagesRem)
					{
						*freeAddr = freeOff;
						return;
					}
				}
				ptOff = 0;
			}
			pdOff = 0;
		}
		pdpOff = 0;
	}

	Print(L"Out of VSPACE!\n");
	Pause();
	return;
}

int checkMapped(const UINT64 vaddr, const UINT64 *l4)
{
	UINT32 l4Off, pdpOff, pdOff, ptOff;
	UINT64 *pdp, *pd, *pt;
	/* Get the offsets into each level of the page table */
	l4Off = (vaddr>>39)%512;
	pdpOff = (vaddr>>30)%512;
	pdOff = (vaddr>>21)%512;
	ptOff = (vaddr>>12)%512;

	Print(L"VADDR: %lX Offsets - L4: %u PDP: %u PD: %u PT: %u\n", vaddr, l4Off, pdpOff, pdOff, ptOff);
	if(l4[l4Off] & PAGE_PRESENT)
	{
		pdp = (UINT64 *)(l4[l4Off] & ~PAGE_CONFIG_BITS);
		Print(L"Found PDP %lX\n", pdp);
		if(pdp[pdpOff] & PAGE_PRESENT)
		{
			pd = (UINT64 *)(pdp[pdpOff] & ~PAGE_CONFIG_BITS);
			Print(L"Found PD %lX\n", pd);
			if(pd[pdOff] & PAGE_PRESENT)
			{
				pt = (UINT64 *)(pd[pdOff] & ~PAGE_CONFIG_BITS);
				Print(L"Found PT %lX\n", pt);
				if(pt[ptOff] & PAGE_PRESENT)
					return 1;
				else
					Print(L"Address %lX is missing PT entry %u Found: %lx\n", vaddr, ptOff, pt[ptOff]);
			}
			else
				Print(L"Address %lX is missing PD entry %u Found: %lx\n", vaddr, pdOff, pd[pdOff]);
		
		}
		else
			Print(L"Missing PDP entry %u Found: %lx\n", pdpOff, pdp[pdpOff]);
			
	}
	else
		Print(L"Missing L4 entry %u Found: %lx\n", l4Off, l4[l4Off]);
			

	return 0;
}

