#include "iso_fs.h"
#include <efilib.h>

/* Directory entry match function
 * @entry Loaded directory entry
 * @target Target string
 * @targetLength Length of the target sequence

 @return Zero is successful, a positive integer otherwise
*/
static UINTN isoMatchDirEntry(const isoDirEntry *entry, const CHAR8 *target, const UINTN targetLength, const UINTN adjust)
{
	/* Verify entry length, +2 due to the mandatory ;1 required by the ISO9660 spec */
	if( (entry->nameLength - adjust) != targetLength)
		return 1;
	else
		return strncmpa(entry->fileId, target, targetLength);
}

/* String search function
 * @str String to be searched
 * @target Target character to find the next instance of
 *
 * @return -1 if no character is found, zero or a positive integer otherwise
 */
static int strFind(const CHAR8 *str, const CHAR8 target)
{
	for(int counter = 0; str[counter]; counter++)
		if(str[counter] == target)
			return counter;

	return -1;
}

/* Directory traversal function
 *
 * @trav Directory traversal structure pointer
 * @fs ISO9660 Filesystem structure pointer for the file system containing the desired file
 * @entry Pointer to the output directory entry structure
 * @entryMaxSize Maximum size of the directory entry buffer

 @return EFI_SUCCESS if a directory entry was loaded.
 		EFI_NOT_FOUND if there are no additional directory entries. All other codes are I/O related errors.
 */
static EFI_STATUS isoTraverseDir(isoDirTraversalState *trav, isoFS *fs, isoDirEntry *entry, const UINTN entryMaxSize)
{
	EFI_STATUS status;
	EFI_DISK_IO_PROTOCOL *disc;
	UINT32 mediaId;
	disc = fs->disc;
	mediaId = fs->mediaId;
	/* Exit with a code if there's no directory entries left */
	if(trav->currentDirOffset >= trav->dirTotalSize)
		return EFI_NOT_FOUND;

	/* Read the base directory entry structure in order to get the total entry length */
	status = uefi_call_wrapper(disc->ReadDisk, 5, disc, mediaId, trav->currentDirOffset + trav->dirLbaByteOffset, sizeof(isoDirEntry), entry);
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to read ISO9660 Directory entry @ %lu from disc.  Code: %u\n", trav->currentDirOffset + trav->dirLbaByteOffset, status);
		Pause();
		return status;
	}
	else if(!entry->length)
		return EFI_NOT_FOUND;

	/* Read the rest of the file name */
	status = uefi_call_wrapper(disc->ReadDisk, 5, disc, mediaId, trav->currentDirOffset + trav->dirLbaByteOffset + sizeof(isoDirEntry), entry->nameLength, entry->fileId );

	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to read ISO9660 Directory entry string @ %lu from disc.  Code: %u\n", trav->currentDirOffset + trav->dirLbaByteOffset, status);
		Pause();
		return status;
	}

	/* Update the traversal function */
	trav->currentDirOffset += entry->length;
	return EFI_SUCCESS;
}

/* File system session creation function
 *
 * @disc EFI Disk protocol for the desired drive containing the FS
 * @discBlock EFI Block protocol for the desired drive containing the FS
 * @fs Pointer to the destination filesystem session structure pointer
 */

EFI_STATUS isoOpenFS(EFI_DISK_IO_PROTOCOL *disc, EFI_BLOCK_IO_PROTOCOL *discBlock, isoFS **fs)
{
	EFI_STATUS status;
	UINT32 mediaId, pvdOffset;
	isoPVD *pvd;
	isoFS *fsObj;

	/* The Primary volume descriptor has to start 32KiB per ECMA 119 spec */
	pvdOffset = ISO_SECTOR_SIZE*0x10;
	/* Save the media ID of the disc and allocate the PVD storage structure */
	mediaId = discBlock->Media->MediaId;
	pvd = (isoPVD *)AllocateZeroPool(ISO_SECTOR_SIZE);

	do
	{

		/* Read the PVD from disc */
		status = uefi_call_wrapper(disc->ReadDisk, 5, disc, mediaId, pvdOffset, ISO_SECTOR_SIZE, pvd);

		if(status != EFI_SUCCESS)
		{
			Print(L"Error reading PVD from CD\n");
			Pause();
			return status;
		}

		/* Verify the volume identifier */
		if( strncmpa(pvd->id, "CD001", 5) )
		{
			FreePool(pvd);
			return EFI_VOLUME_CORRUPTED;
		}

		if(pvd->type == ISO_VD_PVD)
			break;

	} while(pvd->type != ISO_VD_TERM);

	/* Create the filesystem object structure and return it */
	fsObj = (isoFS *)AllocateZeroPool(sizeof(isoFS));
	fsObj->pvd = pvd;
	fsObj->mediaId = mediaId;
	fsObj->disc = disc;

	*fs = fsObj;

	return EFI_SUCCESS;

}

/* Filesystem session cleanup function
 * @fs Pointer to the file system session object to close
 */
void isoCloseFS(isoFS *fs)
{
	FreePool(fs->pvd);
	FreePool(fs);
}

/* File open function
 *
 * @fs Filesystem session pointer to a FS containing the desired file
 * @path Full path to the file
 * @file Return pointer to the file information structure
 *
 * @return EFI_SUCCESS if successful, EFI_NOT_FOUND if the file could not be located and other misc. I/O errors
 */
EFI_STATUS isoOpenFile(isoFS *fs, const CHAR8 *path, isoFile **file)
{
	EFI_STATUS status;
	isoDirEntry *dirEntry;
	isoFile *fHandle;
	isoDirTraversalState trav;
	UINTN lastOff, currentOff, pLength, adjust;

	status = EFI_SUCCESS;
	dirEntry = (isoDirEntry *)AllocateZeroPool(ISO_DIR_RECORD_BASE_LENGTH + 255);

	/* Start the traversal at the root directory of the filesystem */
	trav.dirLbaByteOffset = fs->pvd->rootEntry.location[ISO_ENDIAN_LITTLE] * fs->pvd->logicBlockSize[ISO_ENDIAN_LITTLE];
	trav.dirTotalSize = fs->pvd->rootEntry.dataSize[ISO_ENDIAN_LITTLE];
	trav.currentDirOffset = 0;

	/* Initialize the path length, first substring search start point, and current offset values */
	pLength = strlena(path);
	lastOff = path[0] == '\\' ? 1:0;
	currentOff = lastOff;

	while(currentOff < pLength)
	{
		UINT8 isoExpectedFlags;
		UINTN limit, nextOff;
		//Find the next directory delimiter
		UINTN nextDivide = strFind(&path[currentOff], '\\');
		if(nextDivide == -1)
		{
			//If there's no more delimiters we're looking for a file
			isoExpectedFlags = 0;
			limit = pLength - currentOff;
			nextOff = pLength;
			adjust = 2;
		}
		else
		{
			/* A delimiter exists and we're searching for a directory */
			isoExpectedFlags = ISO_DIRENT_FLAG_DIR;
			limit = nextDivide;
			nextOff = currentOff + nextDivide + 1;
			adjust = 0;
		}

		/* Iterate through all directory entries */
		while(status == EFI_SUCCESS)
		{
			if( !isoMatchDirEntry(dirEntry, &path[currentOff], limit, adjust) )
			{
				/* If we find a match entry make sure it's of the proper type */
				if((dirEntry->flags & isoExpectedFlags) == isoExpectedFlags)
					break;
				else
				{
					Print(L"Found directory entry but flags were %X instead of %X expected.\n", dirEntry->flags, isoExpectedFlags);
					/* If not exit out with a not found code */
					status = EFI_NOT_FOUND;
					goto openCleanup;
				}
			}

			/* Get the next directory entry */
			status = isoTraverseDir(&trav, fs, dirEntry, ISO_DIR_RECORD_BASE_LENGTH + 255);
		}

		if(status != EFI_SUCCESS)
		{
			Print(L"Failed to find open file! Code: %u\n", status);
			Pause();
			goto openCleanup;
		}

		/* Update the traversal structure with the next directory or file location */
		trav.dirLbaByteOffset = fs->pvd->logicBlockSize[ISO_ENDIAN_LITTLE]*dirEntry->location[ISO_ENDIAN_LITTLE];
		trav.dirTotalSize = dirEntry->dataSize[ISO_ENDIAN_LITTLE];
		trav.currentDirOffset = 0;
		currentOff = nextOff;
	}

	/* Create and return the file access structure */
	fHandle = (isoFile *)AllocateZeroPool(sizeof(isoFile));

	fHandle->diskOffset = trav.dirLbaByteOffset;
	fHandle->fileSize = trav.dirTotalSize;

	*file = fHandle;

openCleanup:
	FreePool(dirEntry);

	return status;
}

//Cleanup function for the file information structure
void isoCloseFile(isoFile *file)
{
	FreePool(file);
}

/* ISO9660 FS Read file function
 *
 * @param fs Filesystem session for the FS being accessed
 * @param file File information structure
 * @param offset Offset into the file where the requested read operation will begin
 * @param buffer Destination buffer for the data being read
 * @param readSize Size of the requested read operation
 *
 * @return EFI_SUCCESS if the read completed, EFI_INVALID_PARAMETER if the requested read passed the EOF, and other potential I/O errors
 */
EFI_STATUS isoReadFile(isoFS *fs, const isoFile *file, const UINT64 offset, void *buffer, UINTN readSize)
{
	EFI_STATUS status;
	EFI_DISK_IO_PROTOCOL *disc = fs->disc;

	/* Sanity check, make sure the requested read doesn't go past the end of the file */
	if(file->fileSize <= (file->diskOffset + offset + readSize) )
	{
		/* Perform the read */
		status = uefi_call_wrapper(disc->ReadDisk, 5, disc, fs->mediaId, file->diskOffset + offset, readSize, buffer );
		if(status != EFI_SUCCESS)
		{
			Print(L"Error reading file from CD! Status: %u\n", status);
			Pause();
			return status;;
		}
		else
			return EFI_SUCCESS;
	}
	else
		return EFI_INVALID_PARAMETER;
}
