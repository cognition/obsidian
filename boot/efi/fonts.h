#ifndef EFI_FONTS_H_
#define EFI_FONTS_H_

#include <efi.h>
#include <efilib.h>
#include "iso_fs.h"

typedef struct __attribute__ ((packed))
{
	union
	{
		UINT8 *pixelBuffer;
		UINT32 *dwordBuffer;
	} pixels;
	UINT32 charWidth, charHeight;
	UINT8 bytesPerPixel;
} glyphBuffer;

EFI_STATUS loadPCFFont(isoFS *fs, isoFile *fontFile, UINT32 whiteMask, glyphBuffer *glyphOut, EFI_PHYSICAL_ADDRESS *currentPhys);

#endif
