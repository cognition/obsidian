#ifndef VMEM_H_
#define VMEM_H_

#include <efi.h>
#include <efilib.h>

#define PAGE_SIZE                0x1000

#define PAGE_PRESENT             0x1U
#define PAGE_WRITE               0x2U
#define PAGE_USER                0x4U
#define PAGE_WT                  0x8U
#define PAGE_CD                  0x10U
#define PAGE_PAT                 0x80U
#define PAGE_GLOBAL              0x100U
#define PAGE_NX                  0x8000000000000000UL
#define PAGE_LARGE               0x80U

#define PAGE_CONFIG_BITS         (PAGE_PRESENT | PAGE_WRITE | PAGE_USER | PAGE_WT | PAGE_CD | PAGE_GLOBAL | PAGE_PAT | PAGE_NX)

void mapPages(const UINT64 physAddr, const UINT64 virtAddr, const UINT64 count, const UINT64 access, UINT64 *l4, UINT64 *freeAddr, const int printMsg);
int checkMapped(const UINT64 vaddr, const UINT64 *l4);

#endif
