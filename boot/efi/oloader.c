/* Obsidian UEFI Loader
 * Author: Joseph Kinzel
 */

#include "iso_fs.h"
#include "display_config.h"
#include "fonts.h"
#include "vmem.h"
#include <efilib.h>

#define EFER_MSR                 0xC0000080U

#define KERN_PHYS_BASE           0x200000
#define MMAP_DEFAULT_ENTRIES     128

/* ELF64 Data types */
typedef UINT64 Elf64_Addr;
typedef UINT16 Elf64_Half;
typedef UINT64 Elf64_Off;
typedef INT32  Elf64_Sword;
typedef INT64  Elf64_Sxword;
typedef UINT32 Elf64_Word;
typedef UINT64 Elf64_Xword;
typedef UINT8 Elf64_Byte;
typedef UINT16 Elf64_Section;

/* ELF header field values */
#define EI_NIDENT     16

#define ELFCLASSNONE    0
#define ELFCLASS32      1
#define ELFCLASS64      2

#define ELFDATANONE     0
#define ELFDATA2LSB     1
#define ELFDATA2MSB     2

#define EM_X86_64     62

enum
{
	AddressRangeMemory = 1,
	AddressRangeReserved,
	AddressRangeACPI,
	AddressRangeNVS,
	AddressRangeUnusable,
	AddressRangeDisabled,
	AddressRangePersistentMemory,
	AddressRangeEfiCode = 0xF0U,
	AddressRangeEfiData,
	AddressRangeEfiMMIO 
} AcpiMemoryType;

union ElfSig
{
	UINT8 EI_MAG_BYTES[4];
	UINT32 EI_MAG_WORD;
};

static union ElfSig ElfSigWord = {0x7f, 'E', 'L', 'F'};

/* ELF64 File header */
struct __attribute__ ((packed)) Elf64_Header
{
	struct __attribute__ ((packed))
	{
		union ElfSig EI_MAG;

		UINT8 EI_CLASS;
		UINT8 EI_DATA;
		UINT8 EI_VERSION;
		UINT8 EI_PAD[9];
	} e_ident;

	Elf64_Half e_type;
	Elf64_Half e_machine;
	Elf64_Word e_version;
	Elf64_Addr e_entry;
	Elf64_Off ph_off;
	Elf64_Off sh_off;

	Elf64_Word e_flags;
	Elf64_Half e_ehsize;
	Elf64_Half e_phentsize;
	Elf64_Half e_phnum;
	Elf64_Half e_shentsize;
	Elf64_Half e_shnum;
	Elf64_Half eshstrndx;
};

/* ELF64 Program header entry */
struct __attribute__((packed)) Elf64_Phdr
{
	Elf64_Word p_type;
	Elf64_Word p_flags;
	Elf64_Off p_offset;
	Elf64_Addr p_vaddr;
	Elf64_Addr p_paddr;
	Elf64_Xword p_filesz;
	Elf64_Xword p_memsz;
	Elf64_Xword p_align;
};


/* Program header flags bitfield masks */
#define ELF_PF_X  0x1
#define ELF_PF_W  0x2
#define ELF_PF_R  0x4

/* Simplified virtual mapping information */
struct segmentMappingInfo
{
	UINT64 physBase, virtBase;
	UINT64 spanPages, attr;
};

//! Structure describing a Int 15/AX = E820h memory map entry
struct __attribute__ ((packed)) memoryMap
{
	//! Size of this memory map entry
	UINT32 size;
	//! Base address of the region
	UINT64 base;
	//! Length of the region
	UINT64 length;
	//! Entry type
	UINT32 type;
	//! Extra attributes describing region
	UINT32 extendedAttributes;

};

//! Information structure passed to the kernel by the bootloader
struct __attribute__ ((packed)) bootLoaderInfo
{
	//! Physical address of the initial ramdisk
	UINT32 initrdAddress;
	//! Size of the initial ramdisk
	UINT32 initrdLength;
	//! Pointer to the start of the memory map
	struct memoryMap *memoryMapPtr;
	//! Length of the memory map in bytes
	UINT16 memoryMapLength;
	//! Offset to the PNP Bios information structure
	UINT16 pnpOffset;
	//! Segment of the PNP Bios information structure
	UINT16 pnpSeg;
	//! End of physical memory used by kernel, ramdisk and initial paging structures
	UINT32 physEnd;
	//! End of virtual memory required by the kernel and EFI runtime services
	UINT64 virtEnd;
	//! Physical address of the ACPI RSDP
	UINT64 acpiRSDP;
	//! Frame buffer information
	frameBufferInfo fbInfo;	
	//! Font glyphs
	glyphBuffer glyphs;
	//! Bios boot drive number
	UINT8 bootDriveNumber;
};

static EFI_STATUS loadInitRdFile(isoFS *discFS, isoFile *rdFile, UINT64 *initRdBase, UINT64 *initRdSize)
{
	EFI_STATUS status;
	UINT64 rdOnDiskSize, rdPages, rdPhysAddr;

	/* Get the file size and calculate the number of pages needed, update physical start address */
	rdOnDiskSize = rdFile->fileSize;
	rdPages = (rdOnDiskSize + 0xFFF)/0x1000;
	rdPhysAddr = *initRdBase;

	/* Allocate the pages needed */
	Print(L"INITRD on disk size: %lu bytes Physical load address @ %lx\n", rdOnDiskSize, rdPhysAddr);
	status = uefi_call_wrapper(BS->AllocatePages, 4, AllocateAddress, EfiLoaderData, rdPages, &rdPhysAddr);

	if(status != EFI_SUCCESS)
	{
		Print(L"Error unable to allocate %d pages @ %lx Code: %d\n", rdPages, rdPhysAddr, status);
		Pause();
		return status;
	}

	/* Load the file from disk into memory */
	status = isoReadFile(discFS, rdFile, 0, (void *)rdPhysAddr, rdOnDiskSize);

	if(status != EFI_SUCCESS)
	{
		Print(L"Error unable to read INITRD image from disc.  Error: %u\n", status);
		Pause();
		return status;
	}

	*initRdSize = rdOnDiskSize;

	return status;
}

static EFI_STATUS loadKernelFile(isoFS *discFS, isoFile *kernFile, UINT64 *kernEntry, UINT64 *totalPages,
	struct segmentMappingInfo **segInfo, int *segmentMappings, UINT64 *kernEndAddr) {

	EFI_STATUS status;
	struct Elf64_Header kernelHeader;
	struct Elf64_Phdr progHeader;
	UINT64 kernPhysOffset, allPages;
	UINTN bufferSize = sizeof(kernelHeader);
	struct segmentMappingInfo *kMappingInfo;
	int mappedSegmentCount = 0;

	kernPhysOffset = KERN_PHYS_BASE;

	/* Load the kernel header into memory */
	status = isoReadFile(discFS, kernFile, 0, &kernelHeader, bufferSize);

	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to read kernel file header\n");
		return status;
	}

	/* Identify the ELF file via its header */
	if(kernelHeader.e_ident.EI_MAG.EI_MAG_WORD != ElfSigWord.EI_MAG_WORD ||
		kernelHeader.e_ident.EI_CLASS != ELFCLASS64 ||
		kernelHeader.e_ident.EI_DATA != ELFDATA2LSB ||
		kernelHeader.e_machine != EM_X86_64) {

		Print(L"Kernel header corrupted!\n");
		return EFI_VOLUME_CORRUPTED;
	}

	Print(L"Program header offset: %d Program Header Entries: %d\n", kernelHeader.ph_off, kernelHeader.e_phnum);
	Print(L"Kernel entry address: %.16lx\n", kernelHeader.e_entry);

	*kernEntry = kernelHeader.e_entry;

	allPages = 0;
	/* Notable assumptions about the structure of the ELF file's segments Handle
	 * file load post conditions:
	 * - Each segment will begin and end at a page aligned address.
	 * - Because of the afforementioned assumption the effective in memory size of each
	 *   segment is some multiple of a page size.
	 * - All loadable segments in aggregate occupy a single continguous span of memory
	 * - Any data not in the executable file is initialized to zero
	 */

	kMappingInfo = AllocateZeroPool(kernelHeader.e_phnum*sizeof(struct segmentMappingInfo));
	for(UINT64 header = 0; header < kernelHeader.e_phnum; header++)
	{
		UINT64 pageCount;
		Print(L"Processing program segment %lu\n", header);

		/* Load the segment header into memory */
		bufferSize = sizeof(progHeader);

		status = isoReadFile(discFS, kernFile, kernelHeader.ph_off + header*kernelHeader.e_phentsize, &progHeader, bufferSize);

		if(status != EFI_SUCCESS)
			return status;



		/* Calculate the number of pages needed by the segment */
		pageCount = (progHeader.p_memsz + 0xFFFUL) / 0x1000;

		if(progHeader.p_memsz)
		{
			/* Load the segment if it actually has a place in memory */
			struct segmentMappingInfo *segMapInfo = &kMappingInfo[mappedSegmentCount++];

			UINT64 segMapFlags = PAGE_PRESENT;
			UINT64 segAddr = kernPhysOffset;
			UINTN segSize = progHeader.p_filesz;
			UINT64 memSize = pageCount*PAGE_SIZE;
			UINTN rem = memSize - segSize;
			EFI_MEMORY_TYPE segType = progHeader.p_flags & ELF_PF_X ?
				EfiLoaderCode:EfiLoaderData;

			/* Set the page mapping flags */
			 if(!(progHeader.p_flags & ELF_PF_X))
			 	segMapFlags |= PAGE_NX;
			 if(progHeader.p_flags & ELF_PF_W)
			 	segMapFlags |= PAGE_WRITE;


			status = uefi_call_wrapper(BS->AllocatePages, 4, AllocateAddress, segType, pageCount, &segAddr);
			if(status != EFI_SUCCESS)
			{
				Print(L"Error unable to allocate %d pages @ %lx Code: %d\n", pageCount, segAddr, status);
				Pause();
				return status;
			}
			/* Do actual loading from the file if required */
			if(segSize)
			{
				status = isoReadFile(discFS, kernFile, progHeader.p_offset, (void *)segAddr, segSize);

				if(status != EFI_SUCCESS)
				{
					Print(L"Failed to load segment data! Code: %d\n", status);
					Pause();
					return status;
				}

				segAddr = kernPhysOffset+progHeader.p_filesz;
			}

			/* Zero any memory that was not loaded from the file */
			if(rem)
				SetMem((void *)segAddr, rem, 0);

			/* Save physical -> virtual mapping info for updating by runtime service later */
			segMapInfo->physBase = kernPhysOffset;
			segMapInfo->virtBase = progHeader.p_vaddr;
			segMapInfo->spanPages = pageCount;
			segMapInfo->attr = segMapFlags;

			kernPhysOffset += memSize;

		}
	
		Print(L"Segment vaddr: %.16lx Mem size: %.8ld File size: %ld Pages: %d\n", progHeader.p_vaddr, progHeader.p_memsz, progHeader.p_filesz, pageCount);
	}

	*totalPages = allPages;
	*segmentMappings = mappedSegmentCount;
	*segInfo = kMappingInfo;
	*kernEndAddr = kernPhysOffset;
	Print(L"Kernel physical end address: %lX\n", kernPhysOffset);

	return EFI_SUCCESS;
}

static void transformMemoryMap(struct memoryMap *osMap, uintptr_t entryAddress, const UINTN mmapEntryCount, const UINTN descSize)
{
	EFI_MEMORY_DESCRIPTOR *mmapPtr;
	struct memoryMap *oMapPtr = osMap;

	/* Transform the EFI style memory map to an INT 15/e820h style memory map */
	for(UINT64 entry = 0; entry < mmapEntryCount; entry++, oMapPtr++)
	{
		mmapPtr = (EFI_MEMORY_DESCRIPTOR *)entryAddress;

		oMapPtr->size = sizeof(struct memoryMap);
		oMapPtr->length = mmapPtr->NumberOfPages * PAGE_SIZE;
		oMapPtr->base = mmapPtr->PhysicalStart;
		oMapPtr->extendedAttributes = 0;

		switch(mmapPtr->Type)
		{
			case EfiRuntimeServicesCode:
				oMapPtr->type = AddressRangeEfiCode;
				break;
			case EfiRuntimeServicesData:
				oMapPtr->type = AddressRangeEfiData;
				break;
			case EfiMemoryMappedIO:
			case EfiMemoryMappedIOPortSpace:
				oMapPtr->type = AddressRangeEfiMMIO;
				break;
			case EfiReservedMemoryType:
			case EfiPalCode:
			case EfiUnusableMemory:
				oMapPtr->type = AddressRangeReserved;
				break;
			case EfiBootServicesCode:
			case EfiBootServicesData:
			case EfiConventionalMemory:
			case EfiLoaderCode:
			case EfiLoaderData:
				oMapPtr->type = AddressRangeMemory;
				break;
			case EfiACPIReclaimMemory:
				oMapPtr->type = AddressRangeACPI;
				break;
			case EfiACPIMemoryNVS:
				oMapPtr->type = AddressRangeNVS;
				break;
			/*
			case EfiPersistentMemory:
				oMapPtr->type = AddressRangePersistentMemory;
				break;
			*/
			default:
				oMapPtr->type = AddressRangeReserved;
				break;
		}
		entryAddress += descSize;
	}
}

/* Initialize the boot information structure
 * @param initRdEnd Physical end address of the initial RAM disk
 * @param physEnd Pointer to the end of physical addresses that have been allocated so far
 * @param BIout Return point for the boot information structure
 * @param vpNeeded Number of virtual pages needed for the memory map and boot information structure
 */
static EFI_STATUS buildBootInfo(const UINT64 initRdEnd, UINT64 *physEnd, struct bootLoaderInfo **BIout, UINT32 *bliNeededOut)
{
	EFI_STATUS status;
	UINTN mmapSize, mapKey, descSize;
	UINT64 bliPagesNeeded, bootInfoAddr, mmapEntryCount, entryAddress;
	UINT32 descVersion, efiVirtPagesRequired;
	EFI_MEMORY_DESCRIPTOR *mmap, *mmapPtr;
	struct memoryMap *osMap, *oMapPtr;
	struct bootLoaderInfo *bootInfo;
	EFI_CONFIGURATION_TABLE *ctPtr;

	efiVirtPagesRequired = 0;
	/* Retrieve information on the size of the system's memory map */
	mmapSize = 0;
	status = uefi_call_wrapper(BS->GetMemoryMap, 5, &mmapSize, mmap, &mapKey, &descSize, &descVersion);

	mmapEntryCount = mmapSize/descSize;
	bliPagesNeeded = (mmapEntryCount*sizeof(struct memoryMap) + sizeof(struct bootLoaderInfo) + mmapEntryCount*sizeof(struct memoryMap) + 0xFFFU)/PAGE_SIZE;
	Print(L"Memory map size: %u Descriptor size: %u Pages needed: %u\n", mmapSize, descSize, bliPagesNeeded);

	/* Allocate pages for the memory map and boot information structure */
	bootInfoAddr = initRdEnd;
	status = uefi_call_wrapper(BS->AllocatePages, 4, AllocateAddress, EfiLoaderData, bliPagesNeeded, &bootInfoAddr);
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to allocate pages for OS boot information structure! Error: %u\n", status);
		Pause();
		return status;
	}

	bootInfo = (struct bootLoaderInfo *)bootInfoAddr;
	bootInfo->memoryMapPtr = (struct memoryMap *)(bootInfoAddr + sizeof(struct bootLoaderInfo));



	/* Get the configuration table and search it for the ACPI RSDP entry */
	ctPtr = ST->ConfigurationTable;
	Print(L"Configuration table: %lX Number of CT entries: %u\n", ctPtr, ST->NumberOfTableEntries);

	for(UINTN entry = 0; entry < ST->NumberOfTableEntries; entry++ )
	{
		if( !CompareGuid(&(ctPtr->VendorGuid), &AcpiTableGuid) )
		{
			Print(L"Found ACPI Table @ %lX\n", ctPtr->VendorTable);
			bootInfo->acpiRSDP = (UINT64)ctPtr->VendorTable;
			//break;
		}
		ctPtr++;
	}



	*physEnd = bootInfoAddr + PAGE_SIZE*bliPagesNeeded;

	*BIout = bootInfo;
	*bliNeededOut = bliPagesNeeded;

	return EFI_SUCCESS;
}

/* ! Attempts to obtain a final memory map, exit boot services and jump into the kernel with the identity mapping still on and the new page table
 * @param bInfo Boot loader information structure
 * @param ImageHandle Handle for the currently executing bootloader image
 * @param vBootInfo Virtual address of the boot loader information structure
 * @param firstFreePage Next free physical page, represents the end of the physical pages actually used by the kernel and any required boot information
 * @param l4 The physical address of the PML4 structure
 * @param virtAddrPtr Highest virtual address used by the kernel initially
 * @param textOut Text output protocol interface, needed for reestablishing text output capabilities should a failure to exit boot services occur.
 * @param identityKernEntry Identity mapped kernel entry point
 */

static EFI_STATUS attemptKernJump(struct bootLoaderInfo *bInfo, EFI_HANDLE ImageHandle, const EFI_VIRTUAL_ADDRESS vBootInfo, 
	const EFI_PHYSICAL_ADDRESS firstFreePage, const UINT64 *l4, const EFI_VIRTUAL_ADDRESS virtAddrPtr, SIMPLE_TEXT_OUTPUT_INTERFACE *textOut, const UINT64 identityKernEntry) {

	EFI_STATUS status;
	UINT64 mmapSize, mapKey, descSize, mmapAddr;
	UINT32 descVersion, mmapEntryCount;
	EFI_MEMORY_DESCRIPTOR *efiMapPtr, *mmap;

	mmap = NULL;
	/* Retrieve information on the size of the system's memory map */
	mmapSize = 0;
	status = uefi_call_wrapper(BS->GetMemoryMap, 5, &mmapSize, mmap, &mapKey, &descSize, &descVersion);
	if(status != EFI_SUCCESS && status != EFI_BUFFER_TOO_SMALL)
	{
		Print(L"Unable to retrieve memory map size! Status: %u\n", status);
		Pause();
		return status;
	}

	while(status == EFI_BUFFER_TOO_SMALL)
	{
		if(mmap)
		{
			FreePool(mmap);
		}
	
		/* Allocate the memory map and retrieve it */
		mmapAddr = (UINT64)AllocateZeroPool(mmapSize);
		mmap = (EFI_MEMORY_DESCRIPTOR *)mmapAddr;
		status = uefi_call_wrapper(BS->GetMemoryMap, 5, &mmapSize, mmap, &mapKey, &descSize, &descVersion);
	}
	
	if(status != EFI_SUCCESS)
	{
		
		Print(L"Failed to retrieve EFI memory map! Error: %u\n", status);
		Pause();
		return status;
	}

	/* Attempt to exit UEFI boot services with the map */
	status = uefi_call_wrapper(BS->ExitBootServices, 2, ImageHandle, mapKey);
	int bsFailCounter = 0;

	while(status != EFI_SUCCESS)
	{
		/* If the initial exit attempt fails keep on try again a few more times before reseting the text output and erroring out */
		status = uefi_call_wrapper(BS->GetMemoryMap, 5, &mmapSize, mmap, &mapKey, &descSize, &descVersion);
		if(status != EFI_SUCCESS)
		{
			uefi_call_wrapper(textOut->Reset, 2, textOut, TRUE);
		
			Print(L"Failed to retrieve EFI memory map during ExitBootServices! Error: %u\n", status);
			Pause();
			return status;
		}

		status = uefi_call_wrapper(BS->ExitBootServices, 2, ImageHandle, mapKey);
		if(status != EFI_SUCCESS)
		{
			bsFailCounter++;
			if(bsFailCounter == 15)
			{
				uefi_call_wrapper(textOut->Reset, 2, textOut, TRUE);
				Print(L"Warning! ExitBootServices call failed with code: %u\n", status);
				Pause();

				return status;
			}
		}
		else
			break;
	}

	/* Transform the UEFI style memory map with the extended Int 15h/E820h format */
	mmapEntryCount = mmapSize/descSize;

	transformMemoryMap(bInfo->memoryMapPtr, mmapAddr, mmapEntryCount, descSize);
	/* Update the boot information structure memory map fields */
	bInfo->memoryMapPtr = (struct memoryMap *)(vBootInfo + sizeof(struct bootLoaderInfo));	
	bInfo->memoryMapLength = mmapEntryCount*sizeof(struct memoryMap);

	/* Update the physical and virtual extent values */
	bInfo->virtEnd = virtAddrPtr;
	bInfo->physEnd = firstFreePage;

	/* Jump into the identity mapped kernel code */
	asm volatile("cli\n"
		        "jmp *%%rax\n":: "a" (identityKernEntry), "b" (l4), "D" (vBootInfo), "S" (firstFreePage) );

	// Should never actually reach this point
	return EFI_SUCCESS;
}


EFI_STATUS createKernelVirtMapping(EFI_GRAPHICS_OUTPUT_PROTOCOL *gfx, const UINT32 gfxModeNum, const UINT32 bliNeeded, struct bootLoaderInfo *bInfo, const struct segmentMappingInfo *kSegs, const int kSegCount,
	EFI_LOADED_IMAGE *lip, EFI_HANDLE ImageHandle, SIMPLE_TEXT_OUTPUT_INTERFACE *textOut, const UINT64 kernEntry, EFI_PHYSICAL_ADDRESS *physEnd)
{
	EFI_STATUS status;
	struct memoryMap *mmPtr;
	UINT32 kVirtPagesNeeded, kpdpNeeded, kpdNeeded, kptNeeded;
	UINT32 initRdPages, mapPagesReq, fbPagesNeeded, glyphPagesNeeded;
	UINT64 *l4;
	EFI_MEMORY_DESCRIPTOR *mmap;
	UINT64 vBootInfo, rsdpAddr;
	EFI_VIRTUAL_ADDRESS kVirtEnd, kVirtStart, virtAddrPtr;
	EFI_PHYSICAL_ADDRESS mapPagesStart, mapPagesPtr, fbBackBuffer;

	/* Initialize kernel virtual address span vars */
	kVirtPagesNeeded = 0;
	kVirtEnd = 0;
	kVirtStart = 0;
	kVirtStart -= PAGE_SIZE;

	/* Frame buffer virtual pages needed */
	fbPagesNeeded = bInfo->fbInfo.bytesPerPixel * bInfo->fbInfo.height * bInfo->fbInfo.pixPerScanLine;
	fbPagesNeeded += PAGE_SIZE - 1;
	fbPagesNeeded /= PAGE_SIZE;

	Print(L"FB Pages needed: %u\n", fbPagesNeeded);

	/* Allocate physical memory for the framebuffer's back buffer */
	fbBackBuffer = *physEnd;
	status = uefi_call_wrapper(BS->AllocatePages, 4, AllocateAddress, EfiLoaderData, fbPagesNeeded, &fbBackBuffer);
	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to allocate back buffer for frame buffer! Code: %u\n", status);
		Pause();
		return status;
	}
	else
	{
		*physEnd += fbPagesNeeded*PAGE_SIZE;
		Print(L"Allocated FB back buffer @ %lX\n", fbBackBuffer);
	}

	/* Calculate the number of pages needed for the glyph pixel buffer */
	glyphPagesNeeded = ((bInfo->glyphs.charWidth*0x80U)*bInfo->glyphs.charHeight)*bInfo->glyphs.bytesPerPixel + PAGE_SIZE-1;
	glyphPagesNeeded /= PAGE_SIZE;

	/* Find the virtual address limits of the actual kernel executable */
	for(int segCounter = 0; segCounter < kSegCount; segCounter++)
	{
		EFI_VIRTUAL_ADDRESS segEnd;
		const struct segmentMappingInfo *seg = &kSegs[segCounter];
		segEnd = seg->virtBase + PAGE_SIZE*seg->spanPages;
		if(seg->virtBase < kVirtStart)
			kVirtStart = seg->virtBase;
		if(segEnd > kVirtEnd)
			kVirtEnd = segEnd;

		kVirtPagesNeeded += seg->spanPages;
	}

	Print(L"Kernel spans %lX-%lX Pages required: %u",
	kVirtStart, kVirtEnd, kVirtPagesNeeded);

	/* Calculate the number of pages needed by the INITRD */
	initRdPages = (bInfo->initrdLength + PAGE_SIZE-1)/PAGE_SIZE;
	kptNeeded = (kVirtPagesNeeded + fbPagesNeeded*2 + glyphPagesNeeded + 511)/512;
	kpdNeeded = kptNeeded/512;
	kpdpNeeded = kpdNeeded/512;

	if(!kptNeeded)
		kptNeeded = 1;
	if(!kpdNeeded)
		kpdNeeded = 1;
	if(!kpdpNeeded)
		kpdpNeeded = 1;

	mapPagesReq = kpdpNeeded + kpdNeeded + kptNeeded + 7;
	mapPagesStart = *physEnd;
	*physEnd += mapPagesReq*PAGE_SIZE;

	/* Set up the Level 4 page table pointer and allocate + zero all the higher level mapping pages */
	l4 = (UINT64 *)mapPagesStart;

	status = uefi_call_wrapper(BS->AllocatePages, 4, AllocateAddress, EfiLoaderData, mapPagesReq, &mapPagesStart);

	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to allocate %u mapping structure pages! Status: %u\n", mapPagesReq, status);
		Pause();
		return status;
	}
	
	ZeroMem(l4, mapPagesReq*PAGE_SIZE);

	Print(L"Physical memory end: %lX\n", mapPagesStart + PAGE_SIZE*mapPagesReq);

	/* Recursive mapping trick */
	l4[511] = mapPagesStart | PAGE_PRESENT | PAGE_WRITE;
	mapPagesPtr = mapPagesStart + PAGE_SIZE;

	/* Map all the kernel's segments to their virtual addresses in the new page tables */
	for(int segCounter = 0; segCounter < kSegCount; segCounter++)
	{
		EFI_VIRTUAL_ADDRESS segEnd;
		const struct segmentMappingInfo *seg = &kSegs[segCounter];
		Print(L"Mapping virtual address range %lX-%lX with access: %lX\n", seg->virtBase, seg->virtBase + PAGE_SIZE*seg->spanPages, seg->attr);
		mapPages(seg->physBase, seg->virtBase, seg->spanPages, seg->attr, l4, &mapPagesPtr, 0);
	}

	/* Map the boot information structure into kernel VSPACE */
	Print(L"Mapping boot information page from %lX to %lX..\n", bInfo, kVirtEnd);
	mapPages((UINT64)bInfo, kVirtEnd, bliNeeded, PAGE_PRESENT, l4, &mapPagesPtr, 0);
	vBootInfo = kVirtEnd;

	virtAddrPtr = kVirtEnd + bliNeeded*PAGE_SIZE;
	/* Update the memory map pointer */
	mapPages(KERN_PHYS_BASE, KERN_PHYS_BASE, (kVirtEnd - kVirtStart)/PAGE_SIZE, PAGE_PRESENT | PAGE_WRITE, l4, &mapPagesPtr, 0);

	/* Map the glyph buffer into memory */
	mapPages((EFI_PHYSICAL_ADDRESS)bInfo->glyphs.pixels.pixelBuffer, virtAddrPtr, glyphPagesNeeded, PAGE_PRESENT, l4, &mapPagesPtr, 0);
	bInfo->glyphs.pixels.pixelBuffer = (UINT8 *)virtAddrPtr;
	virtAddrPtr += glyphPagesNeeded*PAGE_SIZE;

	/* Map the graphics back buffer into memory */
	mapPages(fbBackBuffer, virtAddrPtr, fbPagesNeeded, PAGE_PRESENT | PAGE_WRITE, l4, &mapPagesPtr, 0);
	bInfo->fbInfo.bbVirtAddr = virtAddrPtr;

	virtAddrPtr += fbPagesNeeded*PAGE_SIZE;


	/* Set the NXE bit */
	asm volatile ("rdmsr\n"
                   "orl $0x800, %%eax\n"
	              "wrmsr\n"
				"movq %%cr4, %%rax\n"
				"orl $0x6B0, %%eax\n"
				"movq %%rax, %%cr4\n":: "c" (EFER_MSR): "eax", "edx");

	Print(L"Hit any key to jump to the kernel...\n");
	Pause();	

	/* Set the graphics mode to the preferred mode */	
	status = uefi_call_wrapper(gfx->SetMode, 2, gfx, gfxModeNum);
	if(status != EFI_SUCCESS)
	{
		uefi_call_wrapper(textOut->Reset, 2, textOut, TRUE);
		Print(L"Failed to set graphics mode! Error: %u\n", status);
		Pause();
		return status;
	}
	else
		bInfo->fbInfo.totalSize = gfx->Mode->FrameBufferSize;
	

	/* Map the frame buffer into memory */
	mapPages(gfx->Mode->FrameBufferBase, virtAddrPtr, fbPagesNeeded, PAGE_PRESENT | PAGE_WRITE | PAGE_PAT, l4, &mapPagesPtr, 0);
	bInfo->fbInfo.fbVirtAddr = virtAddrPtr;
	virtAddrPtr += fbPagesNeeded*PAGE_SIZE;


	/* Try to actually exit boot services and jump into the identity mapped portion of the kernel code */
	return attemptKernJump(bInfo, ImageHandle, vBootInfo, mapPagesPtr, l4, virtAddrPtr, textOut, kernEntry - kVirtStart + KERN_PHYS_BASE);
}

/* Locates and mounts the ISO9660 filesystem associated with the boot image/device
* \param discFS Return pointer for the ISO9660 filesystem object
* \param lipInterface Loaded image protocol pointer for this OS bootloader
*/
EFI_STATUS mountIsoFS(isoFS **discFS, EFI_LOADED_IMAGE *lipInterface)
{
	EFI_STATUS status;
	EFI_DEVICE_PATH *cdDevPath, *node, *isoPath;
	EFI_DISK_IO_PROTOCOL *discIoIface;
	EFI_BLOCK_IO_PROTOCOL *blockIoIface;
	EFI_HANDLE bootDeviceHandle, cdHandle;
	UINT16 *imageDevString;


	/* Get the device path of the device handle, ignore the device path
  	   from the loaded image protocol since it's simply the file path */
	bootDeviceHandle = lipInterface->DeviceHandle;

	cdDevPath = DevicePathFromHandle(bootDeviceHandle);
	node = cdDevPath;

	
	Print(L"Looking for disc device path...\n");
	/* Locate the actual CD-ROM hardware device path */
	while(!IsDevicePathEndType(node))
	{
		EFI_DEVICE_PATH *next = NextDevicePathNode(node);

		/* Note: Media CD-ROM entry actually represents El Torito boot
		   information, as such the actual device is the prior entry. */
		if((DevicePathType(next) == MEDIA_DEVICE_PATH) &&
			(DevicePathSubType(next) == MEDIA_CDROM_DP)) {
			SetDevicePathEndNode(next);
			break;
		}

		node = next;
	}

	Print(L"Found potential device path!\n");
	if(IsDevicePathEndType(node))
	{
		Print(L"Unable to locate boot CD-ROM device!\n");
		Pause();
		return EFI_UNSUPPORTED;
	}

	Print(L"Duplicating device path..\n");
	isoPath = DuplicateDevicePath(cdDevPath);

	Print(L"Forming device path string..\n");

	imageDevString = DevicePathToStr(isoPath);
	Print(L"Boot CD device path: %s\n", imageDevString);

	/* Get the handle associated with the CD-ROM device */
	status = uefi_call_wrapper(BS->LocateDevicePath, 3, &DiskIoProtocol, &isoPath, &cdHandle);
	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to locate boot CD-ROM Device Handle\n", status);
		Pause();
		return EFI_UNSUPPORTED;
	}

	/* Get the block and disc I/O protocols */
	status = uefi_call_wrapper(BS->HandleProtocol, 3, cdHandle, &DiskIoProtocol, &discIoIface);
	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to open disk I/O Protocol on boot device\n");
		Pause();
		return EFI_UNSUPPORTED;
	}

	status = uefi_call_wrapper(BS->HandleProtocol, 3, cdHandle, &BlockIoProtocol, &blockIoIface);
	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to open block I/O Protocol on boot device\n");
		Pause();
		return EFI_UNSUPPORTED;
	}

	/* Initialize the ISO9660 file system object */
 	status = isoOpenFS(discIoIface, blockIoIface, discFS);
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to mount CD ISO9660 filesystem.\n");
		Pause();
		return status;
	}
	else
		Print(L"Boot disc filesystem mounted.\n");

	return EFI_SUCCESS;
}


EFI_STATUS EFIAPI efi_main(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
	EFI_STATUS status;
	EFI_BOOT_SERVICES *bs;
	EFI_GRAPHICS_OUTPUT_PROTOCOL *gfx;
	SIMPLE_TEXT_OUTPUT_INTERFACE *stoInterface;
	EFI_HANDLE *stoHandles;
	isoFile *kernFile, *rdFile, *pcfFile;
	EFI_LOADED_IMAGE *lipInterface;
	isoFS *discFS;
	UINT64 kernelEntryPoint, totalKernPages, initRdBase, initRdSize, physEnd;
	UINT32 bliNeeded, gfxModeNum;
	UINTN stoSizeNeeded;
	struct segmentMappingInfo *kSegInfo;
	struct bootLoaderInfo *kernBootInfo;
	int kSegmentCount;

	bs = SystemTable->BootServices;
	InitializeLib(ImageHandle, SystemTable);

	stoSizeNeeded = 0;
	Print(L"Obsidian bootloader active...\n");
	status = LibLocateHandle(ByProtocol, &TextOutProtocol, 0, &stoSizeNeeded, &stoHandles);
	if(status != EFI_BUFFER_TOO_SMALL && status != EFI_SUCCESS)
	{
		Print(L"Failed to get array size for text output protocol! Status: %u\n", status);
		Pause();
		return status;
	}

	stoHandles = (EFI_HANDLE *)AllocateZeroPool(stoSizeNeeded);
	status = LibLocateHandle( ByProtocol, &TextOutProtocol, 0, &stoSizeNeeded, &stoHandles);
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to locate text output handle! Status: %u\n", status);
		Pause();
		return status;
	}


	status = uefi_call_wrapper(BS->HandleProtocol, 3, stoHandles[0], &TextOutProtocol, &stoInterface);
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to get text output protocol! Status: %u\n", status);
		Pause();
		return status;
	}

	FreePool(stoHandles);
	
	status = uefi_call_wrapper(stoInterface->ClearScreen, 1, stoInterface);
	if(status != EFI_SUCCESS)
		return status;
	/* Initialize the Loaded Image Protocol to retrieve image information,
	   including boot device */
	status = uefi_call_wrapper(bs->OpenProtocol, 6, ImageHandle, &LoadedImageProtocol, &lipInterface, ImageHandle, NULL, EFI_OPEN_PROTOCOL_EXCLUSIVE);
	if(status != EFI_SUCCESS)
	{
		Print(L"Error Loaded image protocol not available - Status: %u.\n", status);
		Pause();
		return EFI_UNSUPPORTED;
	}
	else
		Print(L"Image handle protocol retrieved.\n");

	status = mountIsoFS(&discFS, lipInterface);
	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to mount ISO filesystem! Status: %u\n", status);
		Pause();
		return status;
	}
	else
		Print(L"ISOFS Mounted.\n");

	/* Open the kernel file */
	status = isoOpenFile(discFS, "\\BOOT\\K_AMD64.ELF", &kernFile);

	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to open kernel file! Error: %u\n", status);
		Pause();
		return EFI_NOT_FOUND;
	}
	else
		Print(L"Kernel file located on disc.\n");


	/* Load the kernel object and segment information structures */
	status = loadKernelFile(discFS, kernFile, &kernelEntryPoint, &totalKernPages, &kSegInfo, &kSegmentCount, &initRdBase);

	if(status != EFI_SUCCESS)
	{
		Print(L"Error loading kernel file!\n");
		Pause();
		return status;
	}

	/* Load the initial ram disk image into memory */
	status = isoOpenFile(discFS, "\\BOOT\\IR_AMD64.IMG", &rdFile);

	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to open INITRD file! Error: %u\n", status);
		Pause();
		return EFI_NOT_FOUND;
	}

	status = loadInitRdFile(discFS, rdFile, &initRdBase, &initRdSize);

	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to load initrd file! Error: %u\n", status);
		Pause();
		return EFI_UNSUPPORTED;
	}

	UINT64 initRdEnd = (initRdBase + initRdSize + PAGE_SIZE - 1) & ~0xFFFUL;

	Print(L"InitRD physical end addr: %lX\n", initRdEnd);

	/* Initialize and allocate the kernel boot information structure */
	status = buildBootInfo(initRdEnd, &physEnd, &kernBootInfo, &bliNeeded);
	kernBootInfo->initrdAddress = initRdBase;
	kernBootInfo->initrdLength = initRdSize;


	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to complete boot information structure.  Error: %u\n", status);
		Pause();
		return EFI_UNSUPPORTED;
	}

	/* Locate the currently active graphics output protocol and ascertain the best display mode */
	status = retrieveAllEDID(&gfx, &gfxModeNum, &kernBootInfo->fbInfo);

	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to initial graphics output! Code: %u\n", status);
		Pause();
		return status;
	}

	/* Load the bitmap font for the graphical console */
	status = isoOpenFile(discFS, "\\BOOT\\FONTS\\10X20.PCF", &pcfFile);
	if(status != EFI_SUCCESS)
	{
		Print(L"Unable to load console font file! Error: %u\n", status);
		Pause();
		return EFI_UNSUPPORTED;
	}

	status = loadPCFFont(discFS, pcfFile,  kernBootInfo->fbInfo.redMask | kernBootInfo->fbInfo.blueMask | kernBootInfo->fbInfo.greenMask, &kernBootInfo->glyphs, &physEnd);
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to load console font file! Status: %u\n", status);
		Pause();
		return status;
	}
	
	/* NOTE: All ISO9660 FS operations should be completed by this point */
	isoCloseFS(discFS);


	
	/* Create the virtual mappings for the framebuffer, gfx back buffer, kernel segment, initial ramdisk and memory map, then jump into the actual kernel */
	status = createKernelVirtMapping(gfx, gfxModeNum, bliNeeded, kernBootInfo, kSegInfo, kSegmentCount, lipInterface, ImageHandle, stoInterface, kernelEntryPoint, &physEnd);

	/* NOTE: Execution should never reach this point if the jump into the kernel is successful */
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to enter kernel: %u\n", status);
		Pause();
		return status;
	}

	return EFI_SUCCESS;
}
