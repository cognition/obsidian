#include "fonts.h"
#include "vmem.h"

#define PCF_DEFAULT_FORMAT          0x0000U
#define PCF_INKBOUNDS               0x0200U
#define PCF_ACCEL_INKBOUNDS         0x0100U
#define PCF_COMP_METRICS            0x0100U
#define PCF_GLYPH_PAD               0x0003U
#define PCF_BYTE_MASK               0x0004U
#define PCF_BIT_MASK                0x0008U
#define PCF_SCAN_UNIT_MASK          0x0030U

#define UNICODE_SCRIPT_PAGE_SIZE    0x80U

#define GET_PCF_SCAN_MASK(x)        ((x>>4) & 0x3)
#define GET_PCF_GLYPH_PAD(x)        (x & 0x3)

typedef struct __attribute__ ((packed))
{
  //All stored in LSB format
  INT32 type;
  INT32 format;
  INT32 size;
  INT32 offset;
} pcfTableEntry;

typedef struct __attribute__ ((packed))
{
  char header[4];
  INT32 tableCount;
} pcfHeader;

enum
{
  pcfProperties = 1,
  pcfAccel = 2,
  pcfMetrics = 4,
  pcfBitmaps = 8,
  pcfInkMetrics = 16,
  pcfBdfEncodings = 32,
  pcfSWidths = 64,
  pcfGlyphNames = 128,
  pcfBdfAccel = 256
} pcfTableType;

typedef struct __attribute__ ((packed))
{
  UINT8 leftSideBearing;
  UINT8 rightSideBearing;
  UINT8 charWidth;
  UINT8 charAscent;
  UINT8 charDescent;
} compressedMetrics;

typedef struct __attribute__ ((packed))
{
  INT16 leftSideBearing;
  INT16 rightSideBearing;
  INT16 charWidth;
  INT16 charAscent;
  INT16 charDescent;
  UINT16 charAttributes;
} uncompressedMetrics;

typedef struct __attribute__ ((packed))
{
  INT32 count;
  uncompressedMetrics um[];
} uncompressedMetricsTable;

typedef struct __attribute__ ((packed))
{
  INT16 count;
  compressedMetrics cm[];
} compressedMetricsTable;


typedef union
{
  compressedMetricsTable compressed;
  uncompressedMetricsTable uncompressed;
} pcfMetricsEntry;

typedef struct __attribute__ ((packed))
{
  INT32 format;
  UINT8 noOverlap;
  UINT8 constantMetrics;
  UINT8 terminalFont;
  UINT8 constantWidth;
  UINT8 inkInside;
  UINT8 inkMetrics;
  UINT8 drawDirection;
  UINT8 padding;
  INT32 fontAscent;
  INT32 fontDescent;
  INT32 maxOverlap;
  uncompressedMetrics minBounds;
  uncompressedMetrics maxBounds;

  uncompressedMetrics inkMinBounds;
  uncompressedMetrics inkMaxBounds;
} acceleratorTable;

typedef struct __attribute__ ((packed))
{
  INT32 format;
  pcfMetricsEntry entries;
} pcfMetricsTable;

typedef struct __attribute__ ((packed))
{
  INT32 format;
  INT32 glyphCount;
  INT32 offsets[];
} pcfBitmapTable;

typedef struct __attribute__ ((packed))
{
  INT32 format;
  INT16 minCharOrByte2;
  INT16 maxCharOrByte2;
  INT16 minByte1;
  INT16 maxByte1;
  INT16 defaultChar;
  INT16 glyphIndeces[];
} pcfEncodingTable;

typedef struct __attribute__ ((packed))
{
	INT32 nameOffset;
	INT8 isString;
	INT32 value;
} pcfPropsEntry;

typedef struct __attribute__ ((packed))
{
	INT32 stringSize;
	char strings[];
} pcfPropsFooter;

typedef struct __attribute__ ((packed))
{
	INT32 format;
	INT32 numProperties;
	pcfPropsEntry properties[];
} pcfPropertiesTableHeader;

typedef struct
{
	const char *propName;
	INT8 isString;
	union
	{
		const char *stringValue;
		INT32 numValue;
	} values;
} propertyMatch;

typedef struct
{
	pcfMetricsTable *metrics, *inkMetrics;
	acceleratorTable *accel;
	pcfBitmapTable *bitmaps;
	pcfEncodingTable *encoding;
} fontTableState;

static inline UINT8 compressedExtract(const UINT8 input)
{
	UINT8 value = input;
	value -= 0x80U;

	return value;
}

static EFI_STATUS loadPcfProperties(isoFS *fs, isoFile *pcfFile, const pcfTableEntry *tableInfo, pcfPropertiesTableHeader **propHeader, pcfPropsFooter **propsFooter)
{
	EFI_STATUS status;
	int err;
	UINT8 *ptr;
	INT32 entryCount, propPadding;
	pcfPropertiesTableHeader *props;
	pcfPropsFooter *footer;

	/* Allocate the properties table and read it into memory */
	props = (pcfPropertiesTableHeader *)AllocateZeroPool(tableInfo->size);

	status = isoReadFile(fs, pcfFile, tableInfo->offset, props, tableInfo->size);
	if(status != EFI_SUCCESS)
	{
		Print(L"Error reading font file properties! Status: %u\n", status);
		Pause();
		return status;
	}	

	entryCount = props->numProperties;

	/* Calculate padding for the footer and set the footer */
	propPadding = (4 - entryCount%4)%4;

	ptr = (UINT8 *)&(props->properties[entryCount]);
	ptr = &ptr[propPadding];
	footer = (pcfPropsFooter *)ptr;


	*propsFooter = footer;
	*propHeader = props;

	return EFI_SUCCESS;
}

static int attemptMatch(const pcfPropsEntry *entry, const pcfPropsFooter *propsFooter, const propertyMatch *target)
{
	const char *propName;

	propName = &(propsFooter->strings[entry->nameOffset]);
	/* Attempt to match the property name with the target name */
	if(strlena(propName) != strlena(target->propName))
		return 0;

	if(strcmpa(propName, target->propName) )
		return 0;

	/* Make sure the value and target type match */
	if(target->isString != entry->isString)
		return 0;

	/* Do the actual value comparisson */
	if(!target->isString)
		return (target->values.numValue == entry->value);
	else
		return !strcmpa(target->values.stringValue, &propsFooter->strings[entry->value]);
}

static int verifyEncoding(const pcfPropertiesTableHeader *propsHeader, const pcfPropsFooter *propsFooter)
{
	const int desiredCount = 2;
	propertyMatch desired[desiredCount];
	int propsMatched = 0;
	
	/* Look for a 16-bit unicode character set encoding */
	desired[0].propName = "CHARSET_REGISTRY";
	desired[0].isString = 1;
	desired[0].values.stringValue = "ISO10646";

	desired[1].propName = "CHARSET_ENCODING";
	desired[1].isString = 1;
	desired[1].values.stringValue = "1";


	for(INT32 target = 0; target < desiredCount; target++)
	{
		const pcfPropsEntry *entry = &propsHeader->properties[0];
		for(INT32 prop = 0; prop < propsHeader->numProperties; prop++, entry++)
		{
			if(attemptMatch(entry, propsFooter, &desired[target]) )
			{
				propsMatched++;
				break;
			}
		}

	}

	return (propsMatched == desiredCount);
}

static void plotPixel(UINT32 *pixBuffer, const UINT32 lineSize, const UINT8 bytesPerPixel, const UINT32 x, const UINT32 y, const UINT32 whiteMask)
{

	int offset = (y*lineSize + x);

	pixBuffer[offset] = whiteMask;
}

static EFI_STATUS drawGlyph(UINT32 *pixBuffer, const INT32 lineSize, const INT32 glyphHorizOffset, const pcfMetricsTable *metrics, const pcfBitmapTable *bitmaps, 
	const INT16 offset, const INT32 bytesPerPixel, const UINT32 whiteMask)
{
	
	INT32 totalBits, charWidth, charHeight, bitmapSize, padding, byteWidth;
	UINT8 bmFormat, *bitmapData, *glyphPtr;
	UINT32 bmMapSize, bits, rem, padAdjust;

	bmFormat = GET_PCF_GLYPH_PAD(bitmaps->format);
	bitmapSize = bitmaps->offsets[bmFormat + bitmaps->glyphCount];
	bitmapData = (UINT8 *)&bitmaps->offsets[4 + bitmaps->glyphCount];

	/* Extract metrics for the character */
	if(metrics->format & PCF_COMP_METRICS)
	{
		/* Compressed metrics, use the adjustment function */
		charWidth = compressedExtract(metrics->entries.compressed.cm[offset].charWidth);
		charHeight = compressedExtract(metrics->entries.compressed.cm[offset].charAscent) + compressedExtract(metrics->entries.compressed.cm[offset].charDescent);
	}
	else
	{
		/* Full metrics. Just load them directly */
		charWidth = metrics->entries.uncompressed.um[offset].charWidth;
		charHeight = metrics->entries.uncompressed.um[offset].charAscent + metrics->entries.uncompressed.um[offset].charDescent;
	}

	/* Get the bitmap's read size (byte, short, word) */
	bmMapSize = GET_PCF_SCAN_MASK(bitmaps->format);
	UINT32 bytesPerRead = 1;
	bytesPerRead <<= bmMapSize;
	glyphPtr = &bitmapData[bitmaps->offsets[offset]];

	/* Padding and read size sanity check */
	if(GET_PCF_GLYPH_PAD(bitmaps->format) < bmMapSize)
		return -1;

	/* Calculate the adjustment the padding requires */
	padding = 1U << bmFormat;
	byteWidth = ((charWidth+0x7)/8);
	padAdjust = (padding - byteWidth%(padding))%padding - (bytesPerRead - byteWidth%(bytesPerRead))%bytesPerRead;

	for(int row = 0; row < charHeight; row++)
	{
		int column;
		for(column = 0; column < charWidth; )
		{
			int colBase = column;
			UINT32 drawMask = 0;
			/* Read the raw bits in based upon the read size */
			switch(bmMapSize)
			{
				case 1:
					bits = *(UINT16 *)glyphPtr;
					glyphPtr += 2;
					column += 2*8;
					break;
				case 2:
					bits = *(UINT32 *)glyphPtr;
					glyphPtr += 4;
					column += 4*8;
					break;
				default:
					bits = *glyphPtr;
					glyphPtr++;
					column += 8;
					break;
			}

			/* Reverse the bytes if the MSB byte order flag is set */
			if(bitmaps->format & PCF_BYTE_MASK)
			{
				switch(bmMapSize)
				{
					case 1:
						bits = __builtin_bswap16(bits);
						break;
					case 2:
						bits = __builtin_bswap32(bits);
						break;
					default:
						bits = bits;
						break;
				}
			}

			UINT32 mask = 1;

			/* Check the bit order flag */
			if( !(bitmaps->format & PCF_BIT_MASK) )
				drawMask = bits;
			else
			{
				/* If it's set to MSB then reverse the bit order */
				UINT32 revMask = 1;
				for(mask <<= (bytesPerRead*8 - 1); mask; mask >>= 1, revMask <<= 1)
				{
					if(mask & bits)
						drawMask |= revMask;
				}
			}


			while(drawMask)
			{
				/* Draw a pixel if the corresponding bit is set */
				if(drawMask & 0x1)
					plotPixel(pixBuffer, lineSize, bytesPerPixel, glyphHorizOffset + colBase, row, whiteMask);

				colBase++;
				drawMask >>= 1;
			}

		}
		glyphPtr += padAdjust;
	}

	return EFI_SUCCESS;
}


static EFI_STATUS renderScript(glyphBuffer *buffer, const UINT16 scriptBase, const fontTableState *ts, const INT32 bytesPerPixel, const UINT32 whiteMask, EFI_PHYSICAL_ADDRESS *currentPhys)
{
	INT32 charWidth, charHeight, pixPerChar, lineSize, glyphHorizOffset;
	UINT32 *pixBuffer;
	UINT8 scriptHigh, scriptLow;
	UINT16 encodingAdjust;
	UINT32 glyphPagesNeeded;
	uncompressedMetrics *maxCharSize;
	EFI_PHYSICAL_ADDRESS addr;
	EFI_STATUS status;

	UINT16 scriptValue = scriptBase;

	glyphHorizOffset = 0;

	scriptHigh = scriptBase >> 8;
	scriptLow = scriptBase & 0xFF;

	encodingAdjust = ts->encoding->minByte1;
	encodingAdjust <<= 8;
	encodingAdjust |= ts->encoding->minCharOrByte2;

	/* Verify that the encoding table includes the desired script page */
	if( ts->encoding->minByte1 > scriptHigh && scriptHigh > ts->encoding->maxByte1 )
		return -1;

	if( ts->encoding->minCharOrByte2 > scriptLow && (scriptLow + UNICODE_SCRIPT_PAGE_SIZE-1) > ts->encoding->maxCharOrByte2)
		return -1;
	
	maxCharSize = &ts->accel->minBounds;

	/* Get the size of each character's bitmap and use it to allocate the pixel buffer */

	lineSize = maxCharSize->charWidth;
	lineSize *= UNICODE_SCRIPT_PAGE_SIZE;

	/* Allocate all the pages needed for the glyph pixel buffer and zero initialize it */
	glyphPagesNeeded = (maxCharSize->charAscent + maxCharSize->charDescent)*lineSize*bytesPerPixel;
	glyphPagesNeeded += (PAGE_SIZE - 1);
	glyphPagesNeeded /= PAGE_SIZE;

	addr = *currentPhys;	
	status = uefi_call_wrapper(BS->AllocatePages, 4, AllocateAddress, EfiLoaderData, glyphPagesNeeded, &addr);
	if(status != EFI_SUCCESS)
	{
		Print(L"Failed to allocate %u pages for glyph buffer! Status: %u\n", glyphPagesNeeded, status);
		Pause();
		return status;
	}

	Print(L"Allocated %u pages for glyph buffer @%lx\n", glyphPagesNeeded, *currentPhys);
	pixBuffer = (UINT32 *)addr;
	ZeroMem(pixBuffer, glyphPagesNeeded*PAGE_SIZE);
	*currentPhys = addr + PAGE_SIZE*glyphPagesNeeded;

	/* Render each individual glyph */
	for(INT32 glyphIndex = scriptBase ; glyphIndex < (scriptBase + UNICODE_SCRIPT_PAGE_SIZE); glyphIndex++)
	{
		INT16 offset;
		int index = glyphIndex - encodingAdjust;
		offset = ts->encoding->glyphIndeces[index];
		/* Check if there's no encoding entry */
		if(offset == 0xFFFF)
		{
			/* Use the default character if available, if not print nothing */
			offset = ts->encoding->glyphIndeces[ts->encoding->defaultChar - encodingAdjust];
			if(offset == 0xFFFF)
				continue;		
		}
		/* Render the actual glyph to the buffer */
		drawGlyph(pixBuffer, lineSize, glyphHorizOffset, ts->metrics, ts->bitmaps, offset, bytesPerPixel, whiteMask);
		glyphHorizOffset += maxCharSize->charWidth;
	}

	/* Update the glyph buffer structure and output */
	buffer->pixels.dwordBuffer = pixBuffer;
	buffer->charWidth = maxCharSize->charWidth;
	buffer->charHeight = maxCharSize->charAscent + maxCharSize->charDescent;
	buffer->bytesPerPixel = bytesPerPixel;

	return EFI_SUCCESS;
}

EFI_STATUS loadPCFFont(isoFS *fs, isoFile *fontFile, const UINT32 whiteMask, glyphBuffer *out, EFI_PHYSICAL_ADDRESS *currentPhys)
{
	EFI_STATUS status;
	pcfHeader fileHeader;
	pcfTableEntry *tables;
	fontTableState ts;
	pcfPropertiesTableHeader *pHeader;
	pcfPropsFooter *pFooter;
	UINT32 foundTablesMask;
	/* Tables required to actually decode and render the font's glyphs */
	static const int neededTables = pcfMetrics | pcfBitmaps | pcfBdfAccel | pcfProperties | pcfBdfEncodings;
	UINTN allTablesSize, readSize;

	/* Load the PCF file's header */
	status = isoReadFile(fs, fontFile, 0, &fileHeader, sizeof(fileHeader));

	/* Allocate and load the table entries */
	allTablesSize = sizeof(pcfTableEntry)*fileHeader.tableCount;
	tables = AllocateZeroPool(allTablesSize);

	status = isoReadFile(fs, fontFile, sizeof(fileHeader), tables, allTablesSize);

	/* Initialize the table state structure */
	ts.accel = 0;
	ts.inkMetrics = 0;
	ts.metrics = 0;
	ts.bitmaps = 0;

	for(int i = 0; i < fileHeader.tableCount ; i++)
	{

		switch(tables[i].type)
		{
			/* Glyph encodings, maps the character set to specific glpyh offsets */
			case pcfBdfEncodings:
				foundTablesMask |= pcfBdfEncodings;
				ts.encoding = (pcfEncodingTable *)AllocateZeroPool(tables[i].size);
				status = isoReadFile(fs, fontFile, tables[i].offset, ts.encoding, tables[i].size);
				break;
			/* Accelerator table, general information about the font */
			case pcfBdfAccel:
				foundTablesMask |= pcfBdfAccel;
				ts.accel = (acceleratorTable *)AllocateZeroPool(tables[i].size);

				status = isoReadFile(fs, fontFile, tables[i].offset, ts.accel, tables[i].size);
				if(!ts.accel->terminalFont)
				{
					FreePool(ts.accel);
					ts.accel = 0;
				}
				break;
			/* Properties table, includes specific named properties including the encoding format */
			case pcfProperties:
				foundTablesMask |= pcfProperties;

				loadPcfProperties(fs, fontFile, &tables[i], &pHeader, &pFooter);
				if( verifyEncoding(pHeader, pFooter) )
					Print(L"Font encoding scheme verified!\n");
				FreePool(pHeader);
				FreePool(pFooter);
				break;
			/* Metrics for the glyphs in the font, should be the same for a monospaced font by definition */
			case pcfMetrics:
				foundTablesMask |= pcfMetrics;

				ts.metrics = (pcfMetricsTable *)AllocateZeroPool(tables[i].size);
				status = isoReadFile(fs, fontFile, tables[i].offset, ts.metrics, tables[i].size);

				break;
			/* Metrics for the minimum bounding box size of each character */
			case pcfInkMetrics:
				foundTablesMask |= pcfInkMetrics;
				ts.inkMetrics = (pcfMetricsTable *)AllocateZeroPool(tables[i].size);

				status = isoReadFile(fs, fontFile, tables[i].offset, ts.inkMetrics, tables[i].size);
				break;
			/* Bitmap data for each glyph */
			case pcfBitmaps:
				foundTablesMask |= pcfBitmaps;
				ts.bitmaps = (pcfBitmapTable *)AllocateZeroPool(tables[i].size);				
				status = isoReadFile(fs, fontFile, tables[i].offset, ts.bitmaps, tables[i].size);
				break;
			default:
				break;
		}	
	}

	if((foundTablesMask & neededTables) == neededTables)
	{

		if( renderScript(out, 0, &ts, 4, whiteMask, currentPhys) != EFI_SUCCESS)
		{
			Print(L"ERROR: Failed to render  glyphs!\n");		
		}

		/* Free the required tables */
		FreePool(ts.encoding);
		FreePool(ts.bitmaps);
		FreePool(ts.metrics);
		FreePool(ts.inkMetrics);
		FreePool(ts.accel);

		return EFI_SUCCESS;
	}
	else
	{
		if(ts.encoding)
			FreePool(ts.encoding);
		if(ts.bitmaps);
			FreePool(ts.bitmaps);
		if(ts.metrics)
			FreePool(ts.metrics);
		if(ts.inkMetrics)
			FreePool(ts.inkMetrics);
		if(ts.accel)
			FreePool(ts.accel);
	}

	FreePool(tables);

	return EFI_NOT_FOUND;	
}

