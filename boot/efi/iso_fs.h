#ifndef BOOT_EFI_ISO_H_
#define BOOT_EFI_ISO_H_

#include <efi.h>

#define ISO_SECTOR_SIZE             2048
#define ISO_DIRENT_FLAG_DIR         0x2
#define ISO_ENDIAN_LITTLE           0
#define ISO_DIR_RECORD_BASE_LENGTH  34

#define ISO_VD_PVD                  1
#define ISO_VD_TERM                 255

//ISO9660 directory entry descriptor, see ECMA 119
typedef struct __attribute__ ((packed))
{
	UINT8 length;
	UINT8 extLength;
	UINT32 location[2];
	UINT32 dataSize[2];
	UINT8 dateTime[7], flags, unitSize, interGap;
	UINT16 volSeqNum[2];
	UINT8 nameLength;
	UINT8 fileId[];
} isoDirEntry;

//Structure describing the Primary Volume Descriptor, see ECMA-119
typedef struct __attribute__ ((packed))
{
	INT8 type;
	UINT8 id[5];
	INT8 version;
	UINT8 _unused;
	UINT8 sysId[32];
	UINT8 volId[32];
	UINT8 _unused2[8];
	UINT32 volSpace[2];
	UINT8 _unused3[32];
	UINT16 volSetSize[2];
	UINT16 volSeqNumber[2];
	UINT16 logicBlockSize[2];
	UINT32 pathTableSize[2];
	INT32 typeLPath;
	INT32 optLPath;
	INT32 typeMPath;
	INT32 optMPath;
	isoDirEntry rootEntry;
} isoPVD;

//Simple filesystem descriptor structure
typedef struct
{
	//Removable media id
	UINT32 mediaId;
	//Disk interface
	EFI_DISK_IO_PROTOCOL *disc;
	//Copy of the primary volume descriptor for the filesystem
	isoPVD *pvd;
} isoFS;

//Simple file descriptor
typedef struct
{
	//Offset from the start of the media in bytes
	UINT64 diskOffset;
	//Total size of the file in bytes
	UINT64 fileSize;
} isoFile;

//Directory traversal support structure
typedef struct
{
	UINT32 dirLbaByteOffset;
	UINT32 dirTotalSize;
	UINT32 currentDirOffset;
} isoDirTraversalState;

EFI_STATUS isoOpenFS(EFI_DISK_IO_PROTOCOL *disc, EFI_BLOCK_IO_PROTOCOL *discBlock, isoFS **fs);
void isoCloseFS(isoFS *fs);
EFI_STATUS isoOpenFile(isoFS *fs, const CHAR8 *path, isoFile **file);
void isoCloseFile(isoFile *file);
EFI_STATUS isoReadFile(isoFS *fs, const isoFile *file, const UINT64 offset, void *buffer, UINTN readSize);

#endif
