; First stage bootloader ISO9660 Traversal and loading functions

; Attempts to locate a matching directory entry given  a file name
; NOTE: Will not traverse sub directories, only finds a directory entry in a single directory listing block

findDirectoryEntry:
	; AX	 = Directory entry flags expected for entry
	; EBX = LBA address
	; ECX = Length in bytes
	; DL = Length of the file name string in bytes
	; DS:SI = File/directory name to search for

	push es
	push di
	
	; Save the string length
	push dx
	; Save the expected directory flags
	push ax
	; Save the file string segment
	push ds
	; Save the pointer to the file string
	push si
	; Update bp to our stack frame
	mov bp, sp
	; Load the LBA filed of the EDD Disk address packet
	xor edx, edx
	mov ds, dx
	mov [eddDap.lba], ebx

.readSectors:
	; DX = Additional offset from the last read
	push ecx
	push dx
	add edx, ecx
	jz .findFail
	; Max read size for a real mode segment
	cmp edx, 0x10000
	jl .lastSectorRead
	
	mov bl, 0x20
	test cx, 0x7FF
	jz .updateBlockCount
	dec bl
	jmp .updateBlockCount
.lastSectorRead:
	; Last read command
	mov ebx, edx
	shr ebx, 11
	test cx, 0x7FF
	jz .updateBlockCount
	inc bl
.updateBlockCount:
	; Save the block count on the stack
	push bx
	mov dx, 0
	mov ds, dx
	; Update the block count filed of the EDD disk address packet
	mov [eddDap.blockCount], bl
	mov [eddDap.segment], word DISK_BUFFER_SEG
	mov [eddDap.offset], word 0
.doEddRead:
	; Issue the EDD extended read command
	mov ah, 0x42
	mov dl, [bootDrive]
	mov si, eddDap
	int 0x13
	jc eddReadError

	mov ax, DISK_BUFFER_SEG
	mov es, ax

.searchDirEntries:
	xor edx, edx
	pop cx
	pop dx
	push cx
	mov di, 0
	
	; ES:DI = DISK_BUFFER_SEG:0
	; CX = Block read count
	; DX = Bytes remaining from the last read
	shl cx, 11
	add cx, dx

	mov ax, [bp+4]
.searchLoop:

	movzx bx, byte [es:di]
	test bx, bx
	jz .findFail
	cmp cx, bx
	jl .searchDone

	mov dl, [es:di+25]
	mov si, [bp]
	mov ds, [bp+2]
	and dl, al
	cmp dl, al
	jne .nextEntry

	mov si, [bp]
	
	test byte [bp+4], 0x2
	jnz .directoryCompare
	
	mov dl, [es:di+32]
	sub dl, [bp+6]	
	jle .nextEntry
	cmp dl, 2
	jne .nextEntry
	push bx
	movzx bx, [bp+6]
	cmp word [es:di+bx+33], ';1'
	pop bx
	jne .nextEntry
	mov dl, [bp+6]
	jmp .compareNames
	
.directoryCompare:
	mov dl, [bp+6]
	cmp dl, [es:di+32]
	jne .nextEntry

.compareNames:
	push di
	push cx

	movzx cx, dl
	add di, 33

	repe cmpsb
	pop cx
	pop di
	jz .entryFound

.nextEntry:
	add di, bx
	sub cx, bx
	jnz .searchLoop
.searchDone:	
	mov [eddDap.offset], cx
	movzx edx, cx

	test cx, cx
	jz .doNextRead

	push ds
	push di
	mov ax, es
	mov ds, ax
	mov si, di
	mov di, 0
	rep movsb
	pop di
	pop ds

.doNextRead:
	pop bx
	pop ecx
	movzx edi, di
	movzx ebx, bx
	sub ecx, edi
	jmp .readSectors

	; On return:
	; CF = Set if file not found
	; EBX = LBA address of entry
	; ECX = Length of entry in bytes
.entryFound:
	add sp, 14
	mov ebx, [es:di+2]
	mov ecx, [es:di+10]
	pop di
	pop es
	clc
	ret

.findFail:
	add sp, 14
	pop di
	pop es
	stc
	ret

; Loads a file using EDD extensions to ES:DI
rmLoadFile:
	; EBX = LBA
	; ECX = File size in bytes
	; ES:DI = File destination buffer

	push ds
	push si
	
	mov ax, 0
	mov ds, ax

	mov [eddDap.lba], ebx
	mov ax, es
	xchg [eddDap.segment], ax
	push ax
	mov [eddDap.offset], di
	mov edx, ecx
	shr edx, 11
	test cx, 0x7FF
	jz .doRead
	inc dl

.doRead:
	
	mov [eddDap.blockCount], dl
	mov si, eddDap
	mov ah, 0x42
	mov dl, [bootDrive]
	int 0x13
	jc eddReadError

	pop dx
	pop si
	pop ds	
	
	ret


