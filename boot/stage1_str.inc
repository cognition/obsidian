
strlen:
	; DS:SI = String to compare against

	push si
	push cx
	push ax

	mov ax, 0xFF00
	mov cx, 0xFF
	repe scasb
	sub ah, cl

	pop cx
	mov al, cl
	pop cx

	; AH = Length

	ret

str_find_token:
	; ES:DI = String
	; AL = Character
	; CX = String length

	push bp
	mov bp, sp
	push ax
	push cx

	push es
	push di
	repne scasb
	pop di
	pop es

	je .foundString	
	add sp, 2
	pop ax
	pop bp
	stc
	ret

.foundString:
	mov ax, [bp-4]
	inc cx
	sub ax, cx
	mov cx, ax
	add sp, 2
	pop ax
	pop bp
	clc
	ret

; Compares equality of two strings for the first n characters
strncmp:
	; DS:SI Formatted compare string
	; ES:DI Compare buffer
	push dx
	push ds
	push si
	push es
	push di
	
	movzx dx, byte [ds:si]
	inc si
	cmp cx, dx
	jl .stateRestore
	
	push cx
	mov cx, dx
	repe cmpsb
	pop cx

.stateRestore:
	pop di
	pop es
	pop si
	pop ds
	pop dx
	ret	

