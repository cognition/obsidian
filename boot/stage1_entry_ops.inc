
build_entry_info:
	; ES:DI = Current file offset
	; CX = Bytes remaining in file

	cmp byte [es:di], '['
	jne errorEntryContents

	; Setup local variables frame
	push bp
	push di
	push cx
	mov bp, sp

	; Get the end of the first line
	mov al, EOL_TOKEN
	call str_find_token
	jc errorEntryContents
	push cx

	; Get the nameplate ending	
	mov al, ']'
	call str_find_token
	jc errorEntryContents
	
	; Make sure it occurs before the EOL
	cmp [bp-2], cx
	jl errorEntryContents

	; ES:DI and Length CX will be the name operating system name string
	dec cx
	inc di
	; DX = next line
	pop dx

	; Save length and address on the stack for later use
	mov [es:bx], cx
	mov [es:bx+2], es
	mov [es:bx+4], di
	add bx, 6
	
	; ES:DI = Data after EOL
	add di, dx
	
	; CX = Remaining bytes in file
	mov cx, [bp]
	sub cx, dx
	sub cx, 3
	
	; Save the kernel's filename string
	mov si, kernelCmd
	call parseCommandLine
	; Save the kernel's initrd string
	mov si, initrdCmd
	call parseCommandLine
	; Save the kernel's argument string
	mov si, argsCmd
	call parseCommandLine

	mov sp, bp
	add sp, 4
	pop bp

	ret
	
parseCommandLine:
	; Find the next EOL
	mov al, EOL_TOKEN
	call str_find_token
	jc errorEntryContents
	push cx

	call strncmp
	jnz errorEntryContents
	
	; Calculate the size and offset of the actual kernel specifier string
	pop dx
	movzx cx, byte [ds:si]
	sub dx, cx
	add di, cx

	mov [es:bx], dx
	mov [es:bx+2], es
	mov [es:bx+4], di
	add bx, 6
	inc dx
	add di, dx
	mov cx, [bp]
	
	mov dx, di
	sub dx, [bp+2]
	sub cx, dx
	
	ret

