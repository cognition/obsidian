[ORG 0x7c00]

; Memory map starting location
%define MMAP_SEG_START		0x2000
%define MMAP_OFF_START		0

; Disk read buffer
%define DISK_BUFFER_SEG	        0x3000
%define DISK_BUFFER_BASE        (DISK_BUFFER_SEG*0x10)

; Kernel base address
%define KERNEL_PHYS_BASE        0x200000

; Special in file token values
%define EOL_TOKEN               0xA

%define ENTRY_INFO_SIZE         24
%define ENTRIES_TAG             0xEEEE

; Selectors
%define LOADER_CODE16           0x08
%define LOADER_DATA_SEG         0x10
%define LOADER_CODE32           0x18
%define LOADER_CODE64           0x20

%define MSR_EFER                0xC0000080

_start:
	jmp entry

	; Pad to the boot info table
	times 8-($-$$) db 0

	BI_isoPvdLba             dd   0
	BI_isoBootFileLba        dd   0
	BI_isoBootFileLength     dd   0
	BI_checksum              dd   0
	BI_reserved     times 40 db   0

entry:
	jmp 0x0000:boot
boot:
	cli
	; Set up the stack
	mov ax, 0x4000
	mov ss, ax
	mov sp, 0xFFF8
	; Initialize the segment registers
	mov ax, 0
	mov ds, ax
	mov [bootDrive], dl
	mov [bootInfo.pnpSeg], es
	mov es, ax
	mov [bootInfo.pnpOff], di

check_cpuid:
	; CPUID availability is determined by the processors ability to set
	; the ID flag (Bit 21 in EFLAGS)
	pushfd
	mov bp, sp
	mov eax, [bp]
	btc dword [bp], 21
	popfd
	pushfd
	pop ebx
	xor eax, ebx
	bt eax, 21
	jnc errorProcNotSupported

checkLongModeSupported:
	; Make sure function 0x80000001 is supported and call it
	mov eax, 0x80000000
	cpuid
	mov ebx, eax
	mov eax, 0x80000001
	cmp ebx, eax
	jl errorProcNotSupported
	cpuid

	; Bit 29 in EDX should be set if long mode is supported
	bt edx, 29
	jnc errorProcNotSupported

enable_A20:
	; Check if the A20 line is already enabled
	push es
	mov si, 0x600
	mov di, si
	add di, 16
	mov ax, 0xFFFF
	mov es, ax
	mov bl, [ds:si]
.test_A20:
	mov al, [ds:si]
	cmp [es:di], al
	jne .a20enabled

	not al
	mov [ds:si], al
	cmp [es:di], al
	jne .a20enabled

	; Ask BIOS to enable the A20 line for us
.bios_enable_A20:
	push bx
	mov ax, 0x2401
	int 0x15
	pop bx
	jc .kb_enable_a20
	test ah, ah
	je .a20enabled

	; Enable A20 Line via 8042 Keyboard controller
.kb_enable_a20:
	call .kb_wait
	mov al, 0xAD
	out 0x64, al

	call .kb_wait
	mov al, 0xD0
	out 0x64, al

	call .kb_wait2
	in al, 0x60
	push ax

	call .kb_wait
	mov al, 0xD1
	out 0x64, al

	call .kb_wait
	pop ax
	or al, 2
	out 0x60, al

	call .kb_wait
	mov al, 0xAE
	out 0x64, al

	call .kb_wait
	jmp .a20enabled

.kb_wait:
	in al, 0x64
	test al, 0x2
	jnz .kb_wait
	ret

.kb_wait2:
	in al, 0x64
	test al, 0x1
	jz .kb_wait2
	ret

.a20enabled:
	mov [ds:si], bl
	pop es

get_memory_map:
	mov ax, MMAP_SEG_START
	mov es, ax
	mov di, MMAP_OFF_START+4
	mov ebx, 0

.get_mmap_entry:
	; Execute successive calls to INT=15h/EAX=E820h to retrieve memory
	; map elements
	mov eax, 0xe820
	mov edx, 0x534D4150
	mov ecx, 24
	int 0x15
	jc errorMemoryMap

	add cl, 4
	mov dword [es:di-4], ecx
	add di, cx
	cmp ebx, 0
	jne .get_mmap_entry
	sub di, 4
	mov [bootInfo.mmapLength], di
check_edd_present:
    mov ah, 0x41
    mov bx, 0x55AA
    mov dl, [bootDrive]
    int 0x13
    jc eddNotPresent


get_iso_pvd:
	mov ebx, [BI_isoPvdLba]
	mov [eddDap.lba], ebx
	mov si, eddDap
	mov dl, [bootDrive]
	mov ah, 0x42
	int 0x13
	jnc .pvdReadSuccess

	call eddReadError

.pvdReadSuccess:
	lfs bx, [eddDap.offset]
	add bx, 156

	; Retrieve the length of the PVD's root directory
	mov ecx, [fs:bx+10]
	; Retrieve the LBA address of the PVD's root directory entry
	mov ebx, [fs:bx+2]

	; Save ES:DI
	push es
	push di

find_boot_dir:
	mov si, bootDirStr
	mov dl, [bootDirStrL]
	; Make sure the directory flag is set
	mov ax, 0x2
	call findDirectoryEntry
	jc errorNoBootDir

	; Save the boot directory information
	mov [bootDir.lba], ebx
	mov [bootDir.length], ecx

find_loader_dir:
	mov si, loaderDirStr
	mov dl, [loaderDirStrL]
	mov ax, 0x2
	call findDirectoryEntry
	jc errorNoLoaderInfo

	; Save the loader directory information
	mov [loaderDir.lba], ebx
	mov [loaderDir.length], ecx

find_entry_file:
	mov si, entryFileStr
	mov dl, [entryFileStrL]
	mov ax, 0
	call findDirectoryEntry
	jc errorNoLoaderInfo

	cmp ecx, 4096
	jg errorEntryFileSize
	test ecx, ecx
	jz errorEntryFileSize

load_entry_file:
	pop di
	pop es

	push es
	push di
	mov [es:di], word ENTRIES_TAG

	add di, 4
	push ecx

	call rmLoadFile
	jnc .entryLoadSuccess
	call eddReadError
.entryLoadSuccess:
	pop ecx

parse_entry_file:
	mov bx, cx
	lea bx, [es:di+bx]
	mov [es:di-2], bx
.readEntries:
	call build_entry_info
	test ecx, ecx
	jz .entryTableComplete
	cmp [es:di], byte EOL_TOKEN
	jne errorEntryContents
	inc di
	dec ecx
	test ecx, ecx
	jnz .readEntries

.entryTableComplete:
	pop di
	pop es

	mov ax, bx
	sub ax, [es:di+2]
	mov cl, ENTRY_INFO_SIZE
	div cl

	push bx
	mov bx, [es:di+2]
	mov [entryTableSeg], es
	mov [entryTableOff], bx
	mov [entryTableCount], al

	mov al, 0

loadKernelEntry:
	; AL = Entry number to load
	; ES:BX = Entry table
	; Calculate the offset of the entry
	mov cl, ENTRY_INFO_SIZE
	mul cl

	mov di, bx
	; AX = Entry offset into the table
	add di, ax
	; ES:DI = Entry data
	;|-----------------------------------------|
	;|ES:DI+0 = Entry name string length       |
	;|ES:DI+2 = Entry name string segment      |
	;|ES:DI+4 = Entry name string offset       |
	;|-----------------------------------------|
	;|ES:DI+6 = Kernel filename length         |
	;|ES:DI+8 = Kernel filename string segment |
	;|ES:DI+10 = Kernel filename string offset |
	;|-----------------------------------------|
	;|ES:DI+12 = Initrd filename length        |
	;|ES:DI+14 = Initrd filename string segment|
	;|ES:DI+16 = Initrd filename string offset |
	;|-----------------------------------------|
	;|ES:DI+18 = Args string length            |
	;|ES:DI+20 = Args string segment           |
	;|ES:DI+22 = Args string offset            |
	;|-----------------------------------------|

	; Attempt to find the kernel's image

	mov ebx, [bootDir.lba]
	mov ecx, [bootDir.length]
	mov ds, [es:di+8]
	mov si, [es:di+10]
	mov dl, [es:di+6]
	mov ax, 0

	call findDirectoryEntry
	jc errorNoKernel

	; EBX = Kernel LBA
	; ECX = Kernel size in bytes

	; Save the entry pointer
	push es
	push di

	; Save the LBA and File size on stack
	push ebx
	push ecx

	; Pointer ES:DI to the default disk buffer
	mov ax, DISK_BUFFER_SEG
	mov es, ax
	mov di, 0

	cmp ecx, 0x10000
	jle .noAdjust
	mov ecx, 0x10000

.noAdjust:
	push ebx
	push ecx
	mov bp, sp

	; [BP]    (DWord) Last load size
	; [BP+4]  (DWord) Last LBA
	; [BP+8]  (DWord) Full file size
	; [BP+12] (DWord) Base LBA

	call rmLoadFile

	; ELF Magic number identification
	cmp dword [es:di], `\x7FELF`
	jne errorKernelFormat

	; 64-bit LSB File
	cmp word [es:di+4], 0x0102
	jne errorKernelFormat

	; Executable file
	cmp word [es:di+16], 2
	jne errorKernelFormat

	; Machine field, denotes the x86_64 architecture
	cmp word [es:di+18], 62
	jne errorKernelFormat

	; Save the entry point address on the stack
	push dword [es:di+28]
	push dword [es:di+24]


	; Put the program header file offset on the stack
	push dword [es:di+32]

	; BX = Section header entry size, CX = Section header count
	mov bx, [es:di+54]
	mov cx, [es:di+56]

	mov ax, bx
	mul cx
	jo errorKernelFormat
	jc errorKernelFormat

	pop edi
	test edi, 0xFFFF0000
	jnz errorKernelFormat

	add ax, di
	jc errorKernelFormat

.readSegmentInfo:
	; Make sure we have a loadable segment
	cmp dword [es:di], 1
	jne .nextSegment

	; Save the file offset on the stack
	push dword [es:di+8]

	; Save the virtual memory address of the segment on stack
	push dword [es:di+16]

	; Save the in file size of the segment
	push dword [es:di+32]
	; Save the in memory size of the segment
	push dword [es:di+40]
	inc dx

.nextSegment:
	add di, bx
	dec cx
	jnz .readSegmentInfo

	mov bx, dx
.loadSegment:
	pop edx
	pop ecx
	pop edi
	pop esi
	push bx

	sub edx, ecx
	; EDX = Trailing byte count
	; ECX = File size of segment
	; EDI = Virtual address of segment
	; ESI = File offset of segment

	; Get the LBA offset from the base LBA of the segment
	mov eax, esi
	shr eax, 11
	add eax, [bp+12]

	; Get the in sector offset of the segment
	and esi, 0x7FF

	; Calculate the maximum copy size in bytes
	mov ebx, 0x10000
	sub ebx, esi
	; Test if we can copy the entire segment in one go
	cmp ebx, ecx
	jle .prepCopy
	; If we can set EBX == ECX
	; Otherwise EBX == Max copy size
	mov ebx, ecx
.prepCopy:
	; Update ECX to reflect the number of bytes left to copy
	; After this operation
	sub ecx, ebx

	push ecx
	mov ecx, ebx
	add edi, KERNEL_PHYS_BASE
.sectorLoad:
	add esi, DISK_BUFFER_BASE
	push edx
	push ecx
	push esi
	push edi

     ; Get the lower 16-bits of the read address, containing sector offset
     mov bx, si
	mov si, 0
	mov ds, si
	mov si, eddDap
	mov [eddDap.lba], eax
	; Calculate the number of sectors,
	mov ax, bx
	and ax, 0x7FF
	add ax, 0x7FF
	add eax, ecx
	shr eax, 11
	mov [eddDap.blockCount], al
	mov ah, 0x42
	mov dl, [bootDrive]
	int 0x13
	jnc .loadSuccess
	call eddReadError

.loadSuccess:
	pop edi
	pop esi
	pop ecx
	pop edx
	pop ebx

.doPmodeCopy:
	cli
	lgdt [loaderGdtr]
	mov eax, cr0
	or al, 1
	mov cr0, eax
	mov ax, LOADER_DATA_SEG
	mov ds, ax
	mov es, ax
    o32 a32 rep movsb
	test ebx, ebx
	jnz .returnRealMode
	test edx, edx
	jz .returnRealMode
	mov ecx, edx
	mov al, 0
	o32 a32 rep stosb
.returnRealMode:
	mov eax, cr0
	and al, 0xFE
	mov cr0, eax
	mov ax, 0
	mov ds, ax
	mov es, ax

	jmp 0x0000:.enterRealMode
.enterRealMode:
	sti
	test ebx, ebx
	jz .loadNextSegment

	; Extra bytes need to copied over still
	mov eax, 0x10000
	mov ecx, ebx
	cmp eax, ecx
	jle .calculateRemaining
	mov eax, ecx
.calculateRemaining:
	sub ecx, eax
	push ecx
	mov ecx, eax
	mov eax, 0x20
	test esi, 0x7FF
	jz .updateLba
	dec eax
.updateLba:
	add eax, [eddDap.lba]

	mov esi, 0
	jmp .sectorLoad

.loadNextSegment:
	cmp edi, [kernPhysEnd]
	jl .adjustSegCounter
	mov [kernPhysEnd], edi
.adjustSegCounter:
	pop bx
	dec bx
	jnz .loadSegment

	mov edi, [kernPhysEnd]
	add edi, 0xFFF
	and edi, 0xFFFFF000
	mov [kernPhysEnd], edi

loadRamDisk:
	; Get ES:DI to point to the entry information table
	mov di, [bp+16]
	mov es, [bp+18]

	mov ebx, [bootDir.lba]
	mov ecx, [bootDir.length]
	mov ds, [es:di+14]
	mov si, [es:di+16]
	mov dl, [es:di+12]
	mov ax, 0

	call findDirectoryEntry
	jc errorNoInitrd

	mov ax, 0
	mov ds, ax

	mov edi, [kernPhysEnd]

	mov [bootInfo.initrdAddress], edi
	mov [bootInfo.initrdLength], ecx
	mov [eddDap.lba], ebx
.doNextLoad:
	push ecx
	test ecx, 0xFFFF0000
	jz .lastLoad
	mov edx, 0x20
	mov ecx, 0x10000
	push ecx
	jmp .rmLoadPart
.lastLoad:
	push ecx
	mov dx, cx
	shr dx, 11
	test cx, 0x7FF
	jz .rmLoadPart
.doShift:
	inc dl
.rmLoadPart:
	push edx
	mov [eddDap.blockCount], dl
	mov si, eddDap


	mov ah, 0x42
	mov dl, [bootDrive]
	int 0x13
	jc eddReadError

	pop edx
	pop ecx
	push ecx
	mov esi, DISK_BUFFER_BASE

	cli
	lgdt [loaderGdtr]
	mov eax, cr0
	or al, 1
	mov cr0, eax

	mov ax, LOADER_DATA_SEG
	mov ds, ax
	mov es, ax
	a32 o32 rep movsb

	mov eax, cr0
	and eax, 0xFFFFFFFE
	mov cr0, eax

	mov ax, 0
	mov ds, ax
	mov es, ax
	jmp 0x0000:.returnRealMode

.returnRealMode:
	sti
	pop eax
	pop ecx
	add [eddDap.lba], edx
	sub ecx, eax
	jnz .doNextLoad



goPmode:
	mov [kernPhysEnd], edi
	cli
	mov ax, 0
	mov ds, ax
	lgdt [loaderGdtr]
	mov eax, cr0
	or al, 0x1
	mov cr0, eax

	mov eax, ss
	shl eax, 4
	movzx esp, sp
	add esp, eax

	mov ax, LOADER_DATA_SEG
	mov ds, ax
	mov es, ax
	mov ss, ax

	jmp LOADER_CODE32:mapPageTables


; Modules
%include "stage1_iso.inc"
%include "stage1_str.inc"
%include "stage1_entry_ops.inc"

[BITS 32]
mapPageTables:

	push edi
	mov edi, 0xB8000
	mov byte [edi], byte 'J'
	mov byte [edi+1], byte 0x7
	mov byte [edi+2], byte 'U'
	mov byte [edi+3], byte 0x7
	mov byte [edi+4], byte 'M'
	mov byte [edi+5], byte 0x7
	mov byte [edi+6], byte 'P'
	mov byte [edi+7], byte 0x7

	pop edi


	mov edi, [kernPhysEnd]
	add edi, 0xFFF
	and edi, 0xFFFFF000
	; Set CR3 to our PML4
	mov cr3, edi
	mov ecx, edi
	; Get page entry count
	shr ecx, 12
	; First entry in the page directory will be a large page identity mapping the lower 2 megs of memory
	sub ecx, 512
	mov edx, ecx
	; Get the page directory entry count
	add edx, 0x1FF
	and edx, 0xFFFFFE00
	shr edx, 9

	mov ebx, edi
	add ebx, 0x1003


	xor eax, eax
	; Store the page directory pointer table address and flags as the first entry
	mov [edi], ebx
	; Store the kernel page directory pointer table address and flags
	add ebx, 0x1000
	mov [edi+2048], ebx
	; Recursively map the PML4
	mov [edi+4088], edi
	add edi, 4

	; Save the page count
	push ecx
	; Fill the upper half of the first entry with zeros, and all the entries leading up to the kernel PDPTR
	mov ecx, 511
	rep stosd
	add edi, 4
	; Fill all the entries leading to the last one with zeros
	mov ecx, 509
	rep stosd
	; Set the present and writeable flags for our recursively mapped entry
	or [edi], byte 0x3
	; Zero the upper half of the entry
	mov [edi+4], dword 0
	add edi, 8

	; Point the identity page directory entry to the identity page directory
	add ebx, 0x1000
	mov [edi], ebx
	add edi, 4
	; Zero everything else
	mov ecx, 1023
	rep stosd
	; Point the kernel page directory pointer entry to the kernel's page directory
	add ebx, 0x1000
	mov [edi], ebx
	add edi, 4
	mov ecx, 1023
	rep stosd

	; Identity map the first 16 megabytes with a single large page
	mov eax, 0x83
	mov ecx, 8
.doIdentityMap:
	stosd
	mov [edi], dword 0
	add edi, 4
	add eax, 0x200000
	dec ecx
	jnz .doIdentityMap

	mov ecx, 1008
	rep stosd
	xor eax, eax

	; Map the page directory entries for the kernel
.mapPageDirectory:
	add ebx, 0x1000
	mov [edi], ebx
	add edi, 4
	stosd
	dec edx
	jnz .mapPageDirectory

	test edi, 0xFFF
	jz .mapPageTables

	mov ecx, 4096
	sub ecx, edi
	and ecx, 0xFFF
	shr ecx, 2
	rep stosd

	pop ecx
	mov eax, KERNEL_PHYS_BASE | 0x103
.mapPageTables:
	stosd
	mov [edi], dword 0
	add eax, 0x1000
	add edi, 4
	dec ecx
	jnz .mapPageTables

	mov ecx, 4096
	sub ecx, edi
	and ecx, 0xFFF
	shr ecx, 2
	rep stosd

	mov eax, cr4
	or eax, 0xB0
	mov cr4, eax

	mov ecx, MSR_EFER
	rdmsr
	or eax, 0x900

	wrmsr

	mov eax, cr0
	bts eax, 31
	mov cr0, eax

.testLME:
	rdmsr
	bt eax, 10
	jc .longModeActive
	pause
	jmp .testLME
.longModeActive:
	mov [kernPhysEnd], edi
	mov edi, bootInfo
	jmp LOADER_CODE64:enter64

[BITS 64]
enter64:
	pop rax
	mov rbx, cr3
	jmp rax


[BITS 16]
; Various possible boot time error handlers

breakpoint:
	mov si, breakpointMsg
	jmp outputFatalError

errorProcNotSupported:
	mov si, badProcMsg
	jmp outputFatalError

errorMemoryMap:
	mov si, mmapErrMsg
	jmp outputFatalError

eddNotPresent:
    mov si, noEddMsg
    jmp outputFatalError

eddReadError:
	mov si, eddReadErrMsg
	jmp outputMsgWithError

errorNoBootDir:
	mov si, noBootDirMsg
	jmp outputFatalError

errorNoLoaderInfo:
	mov si, noLoaderInfoMsg
	jmp outputFatalError

errorEntryFileSize:
	mov si, entryFileSizeMsg
	jmp outputFatalError

errorEntryContents:
	mov si, entryContentsMsg
	jmp outputFatalError

errorNoKernel:
	mov si, noKernelMsg
	jmp outputFatalError

errorKernelFormat:
	mov si, kernelFormatMsg
	jmp outputFatalError

errorNoInitrd:
	mov si, noInitrdMsg
	jmp outputFatalError

printHex:
    push ax
    mov al, ah
    shr al, 4
    and al, 0xF
    add al, 0x30
    mov ah, 0x0E
    mov bx, 0x07
    int 0x10
    pop ax
    mov al, ah
    and al, 0x0F
    add al, 0x30
    mov ah, 0x0E
    mov bx, 0x07
    int 0x10

    ret

outputMsgWithError:
    push ax
	mov ax, 0
	mov ds, ax
	mov ah, 0x0E
	mov bx, 0x07
	push si
	; Output the standard error message
	mov si, errorMsg
	call outputMsg
	; Output event error message
	pop si
	call outputMsg

    mov si, codeMsg
    call outputMsg
    pop ax
    call printHex
    mov si, addressMsg
    call outputMsg
    mov bp, sp
    mov ax, [ss:bp]
    call printHex
    mov ax, [ss:bp]
    mov ah, al
    call printHex
    mov si, haltMsg
    call outputMsg
    cli
    hlt

outputFatalError:
	mov ax, 0
	mov ds, ax
	mov ah, 0x0E
	mov bx, 0x07
	push si
	; Output the standard error message
	mov si, errorMsg
	call outputMsg
	; Output event error message
	pop si
	call outputMsg
	; Output the halt message
	mov si, haltMsg
	call outputMsg

	; Halt the system
.haltSystem:
	hlt
	jmp .haltSystem

outputMsg:
	lodsb
	test al, al
	jz .outputComplete
	int 0x10
	jmp outputMsg
.outputComplete:
	ret

loaderGdt:
	dq 0
	.loaderCode16 db 0xFF, 0xFF, 0, 0, 0, 0x9C, 0x8F, 0
	.loaderData   db 0xFF, 0xFF, 0, 0, 0, 0x92, 0xCF, 0
	.loaderCode32 db 0xFF, 0xFF, 0, 0, 0, 0x9C, 0xCF, 0
	.loaderCode64 db 0xFF, 0xFF, 0, 0, 0, 0x9C, 0xAF, 0

loaderGdtr:
	dw (loaderGdtr-loaderGdt)-1
	dd loaderGdt


eddDap:
	.dapSize       db 16, 0
	.blockCount    db 1, 0
	.offset        dw 0
	.segment       dw DISK_BUFFER_SEG
	.lba           dq 0

bootDir:
	.lba           dd 0
	.length        dd 0

loaderDir:
	.lba           dd 0
	.length        dd 0

entryTableSeg       dw 0
entryTableOff       dw 0
entryTableCount     db 0

bootInfo:
.initrdAddress      dd 0
.initrdLength       dd 0
.mmapAddress        dq (MMAP_SEG_START*0x10 + MMAP_OFF_START)
.mmapLength         dw 0
.pnpOff             dw 0
.pnpSeg             dw 0
kernPhysEnd         dd 0
kernVirtEnd         dq 0
kernAcpiRSDP        dq 0
fbInfo              times 52 db 0
glyphInfo           times 13 db 0
bootDrive           db 0

bootDirStr          db 'BOOT',0
bootDirStrL         db 4
loaderDirStr        db 'CDLOAD64',0
loaderDirStrL       db 8
entryFileStr        db 'ENTRIES.LST',0
entryFileStrL       db 11

kernelCmd           db 7,'kernel='
initrdCmd           db 7,'initrd='
argsCmd             db 5,'args='

badProcMsg          db `A processor which supports long mode is required`,0
mmapErrMsg          db `Unable to retrieve BIOS memory map`,0

addressMsg          db ` @ `,0
codeMsg             db ` Error code: `,0
kernelFormatMsg     db `Kernel file format invalid!`,0
noKernelMsg         db `Unable to locate requested kernel file!`,0
noInitrdMsg         db `Unable to locate requested initrd image!`,0
noEddMsg            db `Boot drive does not support EDD extensions!`,0
eddReadErrMsg       db `Unable to read sectors using EDD!`,0
noBootDirMsg        db `Unable to locate boot directory!`,0
noLoaderInfoMsg     db `Unable to locate CDLOAD64\'s configuration file`,0
entryFileSizeMsg    db `Entry file must be <= 4096 bytes in size`,0
entryContentsMsg    db `Entry file contents format invalid`,0
breakpointMsg       db `Breakpoint`,0
haltMsg             db `\n\rSystem halted.\n\r`,0
errorMsg            db `CDLOAD64 - Error: `,0
