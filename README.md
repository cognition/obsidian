# README #

This project currently requires LLVM + Clang, NASM and mkisofs.  The output file is a BIOS bootable ISO image under the 'images' directory.
Currently the kernel is not interactive but should boot properly and run a heap allocation speed test.

# Project organization(subdirectories) #

bin - Kernel and tool binaries
boot - All bootloader code
crosstools - All tools and libraries needed for the userspace build chain (cross compiler, binary tools, c library, dynamic linker)
drivers - Any loadable kernel side drivers
initrd - Files that will end up on the initial ram disk
isofiles - Files that will end up on ISO9660 images
kernel - All the kernel code
tools - Custom build chain tool code (currently only for the initial ram disk)
userspace - Userspace programs and their code


# License Information #

Project is currently public domain, with the following stipulations:

This software implements components of the Intel ACPI Component Architecture and which requires the following licensing provisions:

1. COPYRIGHT NOTICE
Some or all of this work - Copyright © 1999-2011, Intel Corp. All rights reserved.

4. DISCLAIMER AND EXPORT COMPLIANCE

4.1. INTEL MAKES NO WARRANTY OF ANY KIND REGARDING ANY SOFTWARE PROVIDED HERE. ANY SOFTWARE ORIGINATING FROM INTEL
OR DERIVED FROM INTEL SOFTWARE IS PROVIDED "AS IS," AND INTEL WILL NOT PROVIDE ANY SUPPORT, ASSISTANCE, INSTALLATION,
TRAINING OR OTHER SERVICES. INTEL WILL NOT PROVIDE ANY UPDATES, ENHANCEMENTS OR EXTENSIONS. INTEL SPECIFICALLY
DISCLAIMS ANY IMPLIED WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.

4.2. IN NO EVENT SHALL INTEL HAVE ANY LIABILITY TO LICENSEE, ITS LICENSEES OR ANY OTHER THIRD PARTY, FOR ANY LOST PROFITS,
LOST DATA, LOSS OF USE OR COSTS OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, OR FOR ANY INDIRECT, SPECIAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THIS AGREEMENT, UNDER ANY CAUSE OF ACTION OR THEORY OF LIABILITY, AND
IRRESPECTIVE OF WHETHER INTEL HAS ADVANCE NOTICE OF THE POSSIBILITY OF SUCH DAMAGES. THESE LIMITATIONS SHALL APPLY
NOTWITHSTANDING THE FAILURE OF THE ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.
